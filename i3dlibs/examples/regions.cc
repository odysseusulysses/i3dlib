/*
 * i3dlib - image manipulation library
 *
 * Copyright (C) 2000-2006   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 * FILE: regions.cc
 * 
 * Labeling of Image3d
 *
 * Pavel Matula, pam@fi.muni.cz, 2001
 */

#include <iostream>
#include <vector>
#include <valarray>
#include <stdlib.h>
#include "threshold.h"
#include "neighbours.h"
#include "regions.h"
#include "image3d.h"
#include "imgfiles.h"

using namespace std;
using namespace i3d;

void usage(const char *name)
{
   cerr << name << " ... image labeling algorithms\n" 
	"Copyright (C) 2006 Centre for Biomedical Image Analysis \n"
	"This is free software; see the source for copying conditions. "
	"There is NO\nwarranty; not even for MERCHANTABILITY or FITNESS FOR "
	"A PARTICULAR PURPOSE.\n\n";
  cerr << "Usage: "<< name << " [f] input output threshold min_volume" << endl;
  cerr << "  input  - input image to load can be specified by" << endl;
  cerr << "           a) regular expression: <regexp>.tga" << endl;
  cerr << "           b) file name in i3d format: <file_name>.i3d" << endl;
  cerr << "  output - name of output file (only i3d format supported)" << endl;
  cerr << "  threshold" << endl;
  cerr << "  min_volume - minimal volume of components" << endl;
  exit(1);
}

int main(int argc, char* argv[]) 
{
  bool useff = false;
  size_t shift = 0;

  if (argc != 5 && argc != 6) {
    usage(argv[0]);
  }
  
  if ((argc == 6)) {
    if (argv[shift + 1] == string("f")) {
      cout << "Flood fill version will be used." << endl;
      useff = true;
      ++shift;
    } else {
      usage(argv[0]);
    }
  }
  try  {
    // zjisteni typu vstupniho souboru
    // !!! Nefunguje zjisteni souboru z regularniho vyrazu !!!
    cerr << "Reading image Type" << endl;
    ImgVoxelType vt = ReadImageType(argv[shift + 1]);
    cerr << "Voxel=" << VoxelTypeToString(vt) << endl;
    
    switch (vt)
      {
      case Gray8Voxel: 
        {
          cout << "Reading GRAY8 image..." << argv[shift + 1] << endl;
          Image3d<GRAY8> i(argv[shift + 1]);
          cout << "Doing Thresholding..." << endl;
          Threshold(i,byte(atoi(argv[shift + 3])));
          cout << "Labeling..." << endl;
          LabeledImage3d<size_t, GRAY8> li;
          if (useff)
            li.CreateRegionsFF(i);
          else
            li.CreateRegions(i, nb3D_6);
          cout << "Number of components: " << li.NumberOfComponents() << endl;

          cout << "Region sizes: " << endl;
          ComponentInfo<GRAY8>::Container::const_iterator 
            it = li.components.begin(); 
          for(; it != li.components.end(); ++it) {
//              cout << "===============================" << endl;
//              cout << "region #" << it->first << endl;
//              cout << "VOI offset = " << it->second.voi.offset << endl;
//              cout << "VOI size = " << it->second.voi.size << endl;
//              cout << "border = " << it->second.border << endl;
//              cout << "voxel_value = " << (int)it->second.voxel_value << endl;
            cout << 
//                "volume = " << 
              it->second.volume << " ";
//              cout << endl;
          }

			 	cout << endl << "min volume: " << argv[5] << endl;
	          cout << "Merging small components with the first..." << endl;
			   li.MergeComponents(li.components.rbegin()->first, bind2nd(component_volume_less<GRAY8>(), int(atoi(argv[4]))));

			 
         // cout << "Removing small components.." << endl;
         /* Remove(li.components, bind2nd(component_volume_less<GRAY8>(), int(atoi(argv[shift + 4]))));*/
			/* li.MergeComponents(10, bind2nd(component_volume_less<GRAY8>(), int(atoi(argv[shift + 4]))));*/
          cout << "Number of components: " << li.NumberOfComponents() << endl;
          cout << "Saving components to image..." << endl;
          if (li.Convert(i))
            i.SaveImage(argv[shift + 2]);
          else
            cout << "Can't save too many components!" << endl;
          break;
        }
      case RGBVoxel:
        {
          cout << "Labeling of RGB image is not implemented..." << argv[shift + 1] << endl;
          break;
        }
      default:
        cerr << "Other voxel types not supported" << endl;
      }
  }
  catch (IOException& e) {
    cerr << e <<endl;
    exit(1);
  }
  catch (InternalException& e) {
    cerr << e <<endl;
    exit(1);
  }
  catch (bad_alloc&) {
    cerr << "Not enough memory." << endl;
    exit(1);
  }
  catch (...) {
    cerr << "Regions: Unknown exception." << endl;
    exit(1);
  }
  
  return 0;
}

