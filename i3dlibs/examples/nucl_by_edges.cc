/*
 * i3dlib - image manipulation library
 *
 * Copyright (C) 2000-2006   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
* FILE: nucl_by_edges.cc
*
* an example of nucleus segmentaion by edge based algorithm
*
* Pavel Matula, pam@fi.muni.cz, 2009
* 
*/

#include <iostream>
#include "image3d.h"
#include "imgfiles.h"
#include "filters.h"
#include "regions.h"
#include "morphology.h"
#include "gradient.h"
#include "DistanceTransform.h"
#include "threshold.h"
#include "histogram.h"
#include <time.h>

using namespace std;
using namespace i3d;

double diffclock(clock_t clock1,clock_t clock2)
{
	double diffticks=clock1-clock2;
	double diffms=(diffticks)/CLOCKS_PER_SEC;
	return diffms;
}

#define INIT_CLOCK() clock_t clock_begin, clock_end;
#define START_CLOCK() clock_begin = clock()
#define END_CLOCK() clock_end=clock(); cout << "Time elapsed: " << double(diffclock(clock_end,clock_begin)) << " s"<< endl;


template <class Voxel> void DoAlgo(const char *in_file,
								   const char *out_file)
{
	INIT_CLOCK();

	START_CLOCK();
	cout << "Reading image..." << endl;
	Image3d<Voxel> img_in(in_file);
	END_CLOCK();

	START_CLOCK();
	Image3d<float> gimg;
	GrayToFloat(img_in, gimg);
	float sigma_xy = 2.0 /*/ 4*/;
	float sigma_z = sigma_xy * img_in.GetResolution().GetZ() / img_in.GetResolution().GetX();

	cout << "Gauss filtering with sigma (" << sigma_xy << ", " << sigma_xy << ", " << sigma_z << ")" << endl;
	GaussIIR(gimg, sigma_xy, sigma_xy, sigma_z); 
	END_CLOCK();
//	gimg.SaveImage("gaussed1.ics");

	START_CLOCK();
	Image3d<float> grad_img_f;
	cout << "Square of gradient magnitude..." << endl;
	i3d::GradientMagnitude2(gimg, grad_img_f);
	END_CLOCK();
//	grad_img_f.SaveImage("gradient.ics");

	START_CLOCK();
	cout << "Laplacian..." << endl;
	Image3d<float> laplace_img_f;
	i3d::Laplace(gimg, laplace_img_f);
	END_CLOCK();
	//laplace_img_f.SaveImage("laplace.ics");

	START_CLOCK();
	cout << "computing mask..." << endl;
	float *p = grad_img_f.GetFirstVoxelAddr();
	float max_val = *p; ++p;
	for (int i = 1; i < grad_img_f.GetImageSize(); ++i) {
		max_val = std::max(max_val, *p); ++p;
	}

	float g_thres = max_val * 0.01 * 0.1;

	i3d::Image3d<BINARY> img_bin;
	img_bin.Align(gimg, false);
	//img_bin.MakeRoom(gimg.GetSize());
	//img_bin.SetOffset(gimg.GetOffset());
	//img_bin.SetResolution(gimg.GetResolution());

	p = grad_img_f.GetFirstVoxelAddr();
	float *q = laplace_img_f.GetFirstVoxelAddr();
	BINARY *r = img_bin.GetFirstVoxelAddr();
//	float *m = gimg.GetFirstVoxelAddr();
	for (int i = 0; i < img_bin.GetImageSize(); ++i) {
		if (*p > g_thres && *q < 0) {
			*r = i3d::BINARY(1);
			//*m = 0.0; 
		} else {
			*r = i3d::BINARY(0);
//			*m = std::numeric_limits<float>::max(); 
		}
		++r; ++p; ++q;// ++m;
	}
	END_CLOCK();
	img_bin.SaveImage("markers.ics");

	i3d::Image3d<BINARY> img_neg_lapl;
	i3d::Threshold(laplace_img_f, img_neg_lapl, -100000.0f, 0.1f);

	i3d::Image3d<GRAY16> out_img;
	i3d::BinaryToGray16(img_bin, out_img);
	out_img.SaveImage(out_file);

	img_neg_lapl.SaveImage("mask.ics");


//	return;
	START_CLOCK();
	i3d::Image3d<BINARY> img_rec;
	cout << "reconstruction..." << endl;
	i3d::Reconstruction_by_dilation(img_bin, img_neg_lapl, img_rec);
	END_CLOCK();

	img_rec.SaveImage("recontructedlaplace.ics");

//	return;
	START_CLOCK();
	cout << "Thresolding..." << endl;
	i3d::Threshold(grad_img_f, img_bin, 0.0f);
	END_CLOCK();

	img_bin.SaveImage(out_file);
//	return;


	START_CLOCK();
	cout << "Labelling..." << endl;
	i3d::LabeledImage3d<GRAY16, BINARY> l_img;
	l_img.CreateRegionsFF (img_bin);
	cout << "# of components: " << l_img.NumberOfComponents() << endl;
	END_CLOCK();


//	l_img.SaveImage("labeled.ics");

	START_CLOCK();
	cout << "taking components with large VOI only..." << endl;

	size_t min_vol = 100000/*/16*/;
	i3d::ComponentToRemove<BINARY> pred (BINARY(0), true, true, false, false, false, 0, 0, 
		min_vol > 0, false, min_vol, 0, false, true, true);
	i3d::EraseComponents (img_bin, l_img, pred);
	cout << "# of components: " << l_img.NumberOfComponents() << endl;
	l_img.SaveImage("filtered_labeled.ics");

	char str[1000];
	cout << "Component closing ..." << endl;
	int j = 0;
	size_t bdr_sz = 30;
	typename ComponentInfo<BINARY>::Container::const_iterator i;
	for (i = l_img.components.begin(); i != l_img.components.end(); i++) 
	{
		cout << "component #" << ++j << "with label " << i->first << endl;
		l_img.GetComponentMask(i->first, img_bin);
		gimg.SetResolution(img_in.GetResolution());
		gimg.MakeRoom(img_bin.GetSizeX() + 2 * bdr_sz, img_bin.GetSizeY() + 2 * bdr_sz, img_bin.GetSizeZ() + 2 * bdr_sz);
		memset(gimg.GetFirstVoxelAddr(), 0, gimg.GetImageSize() * sizeof(float)); // .Clear()

		for (size_t z = 0; z < img_bin.GetSizeZ(); ++z) {
			for (size_t y = 0; y < img_bin.GetSizeY(); ++y) {
				for (size_t x = 0; x < img_bin.GetSizeX(); ++x) {
					gimg.SetVoxel(x + bdr_sz, y + bdr_sz, z + bdr_sz, img_bin.GetVoxel(x,y,z) > 0 ? 1.0f : 0.0f);
				}
			}
		}

		sprintf(str, "image%d.ics", j);
		gimg.SaveImage(str);
		cout << "closing..." << endl;
		float disk_size = 0.5;
		i3d::Resolution res = gimg.GetResolution();
		i3d::Resolution new_res = gimg.GetResolution();
		new_res.SetZ(res.GetZ() / 1);
		gimg.SetResolution(new_res);
		i3d::HybridSaito(gimg, 0, 0.0f);
		for (size_t k = 0; k < gimg.GetImageSize(); ++k) {
			gimg.SetVoxel(k, gimg.GetVoxel(k) < disk_size ? 1.0f : 0.0f);
		}
		sprintf(str, "dil_image%d.ics", j);
		gimg.SetResolution(res);
		gimg.SaveImage(str);

		gimg.SetResolution(new_res);
		i3d::HybridSaito(gimg, 0, 1.0f);
		for (size_t k = 0; k < gimg.GetImageSize(); ++k) {
			gimg.SetVoxel(k, gimg.GetVoxel(k) < disk_size ? 0.0f : 1.0f);
		}

		sprintf(str, "clo_image%d.ics", j);
		gimg.SetResolution(res);
		gimg.SaveImage(str);

		size_t c_vol = 0;
		for (size_t z = 0; z < img_bin.GetSizeZ(); ++z) {
			for (size_t y = 0; y < img_bin.GetSizeY(); ++y) {
				for (size_t x = 0; x < img_bin.GetSizeX(); ++x) {
					if (gimg.GetVoxel(x + bdr_sz,y + bdr_sz,z + bdr_sz) > 0) {
						img_bin.SetVoxel(x, y, z, BINARY(1));
						++c_vol;
					}
				}
			}
		}

		// update mask in labeled image

		VOI<size_t> block = i->second.voi;
		Offset r_offset = PixelsToMicrons(block.offset, l_img.GetResolution());

		for (size_t z=0; z<block.size.z; z++) {
			for (size_t y=0; y<block.size.y; y++) {
				for (size_t x=0; x<block.size.x; x++)
				{
					if (img_bin.GetVoxel(x,y,z) == BINARY(1)) {
						l_img.SetVoxel(block.offset.x + x,
							block.offset.y + y,
							block.offset.z + z, i->first);
					}
				}
			}
		}

//		i->second.volume = c_vol;
	}



	//START_CLOCK();
	//i3d::HybridSaito(fimg, 0, 1.0f);
	//END_CLOCK();
	//fimg.SaveImage("dt.ics");

	//START_CLOCK();
	//cout << "Labelling..." << endl;
	//i3d::LabeledImage3d<GRAY16, BINARY> l_img;
	//l_img.CreateRegionsFF (img_bin);
	//cout << "# of components: " << l_img.NumberOfComponents() << endl;
	//END_CLOCK();

	//START_CLOCK();
	//cout << "removing small components..." << endl;

	//size_t min_vol = 30;
	//i3d::ComponentToRemove<BINARY> pred (BINARY(0), true, true, true, false, false, 0, 0, 
	//	min_vol > 0, false, min_vol, 0, false, true);
	//i3d::EraseComponents (img_bin, l_img, pred);
	//cout << "# of components: " << l_img.NumberOfComponents() << endl;

	//img_bin.SaveImage("markers.ics");

	START_CLOCK();
	cout << "Saving image..." << endl;
	l_img.SaveImage(out_file);
	END_CLOCK();
}

int main(int argc, char* argv[]) 
{
  if (argc < 3) {
   cerr << argv[0] << " ... Nucleus segmenation by watershed\n" 
	"Copyright (C) 2009 Centre for Biomedical Image Analysis \n"
	"This is free software; see the source for copying conditions. "
	"There is NO\nwarranty; not even for MERCHANTABILITY or FITNESS FOR "
	"A PARTICULAR PURPOSE.\n\n";
    cerr << "Usage: "<< argv[0] << " input output" << endl;
    cerr << "  input  - input image" << endl;
    cerr << "  output - output file name" << endl;
   exit(1);
  }
  
  try  {
    // zjisteni typu vstupniho souboru
    // !!! Nefunguje zjisteni souboru z regularniho vyrazu !!!
    cerr << "Reading image Type" << endl;
    ImgVoxelType vt = ReadImageType(argv[1]);
    cerr << "Voxel=" << VoxelTypeToString(vt) << endl;
    
    switch (vt)
      {
      case Gray16Voxel: 
        {
          DoAlgo<GRAY16>(argv[1],argv[2]);
          break;
        }
      default:
        cerr << "Other voxel types not supported" << endl;
      }
  }
  catch (IOException& e) {
    cerr << e <<endl;
    exit(1);
  }
  catch (InternalException& e) {
    cerr << e <<endl;
    exit(1);
  }
  catch (bad_alloc&) {
    cerr << "Not enough memory." << endl;
    exit(1);
  }
  catch (...) {
    cerr << "Unknown exception." << endl;
    exit(1);
  }
  
  return 0;
}
