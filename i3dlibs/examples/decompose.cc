/*
 * i3dlib - image manipulation library
 *
 * Copyright (C) 2000-2006   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 * FILE: decompose.cc
 *
 * The decomposition of true color (rgb) image into three grey
 * scale images - red, green, and blue channel.
 *
 * Petr Matula, pem@fi.muni.cz, 2001
 * 
 */

#include <iostream>
#include "image3d.h"
#include "toolbox.h"

using namespace std;
using namespace i3d;

template <class FULL_COLOR, class GRAY_CHANNEL> 
	void decompose(const char *in_file, const char *chan, const char *out_file)
	{
		 cout << "opening" << endl;
	 Image3d<FULL_COLOR> img(in_file);
  	 cout << "Done." << endl;

	 Image3d<GRAY_CHANNEL> r,g,b;

    cout << "Doing decomposition..." << endl;
    RGBtoGray(img,r,g,b);
    cout << "Done." << endl;

    string fn = out_file;
    string channels = chan;
    string path,name,ext;
    SplitFilename(fn,path,name,ext);

    name = name + "." + ext;
    
    if (channels.find("r") != string::npos)
      {
        cout << "Saving red channel..." << endl;
        r.SaveImage((path + "red_" + name).c_str());
        cout << "Done." << endl;
      }
    
    if (channels.find("g") != string::npos)
      {
        cout << "Saving green channel..." << endl;
        g.SaveImage((path + "green_" + name).c_str());
        cout << "Done." << endl;
      }
    
    if (channels.find("b") != string::npos)
      {
        cout << "Saving blue channel... "  << endl;
        b.SaveImage((path + "blue_" + name).c_str());
        cout << "Done." << endl;
      }
	}


int main(int argc, char* argv[]) 
{
  if (argc < 4) {
   cerr << argv[0] << " ... truecolor (rgb) image decomposition\n" 
	"Copyright (C) 2006 Centre for Biomedical Image Analysis \n"
	"This is free software; see the source for copying conditions. "
	"There is NO\nwarranty; not even for MERCHANTABILITY or FITNESS FOR "
	"A PARTICULAR PURPOSE.\n\n";
    cerr << "Usage: "<< argv[0] << " input [r][g][b] output" << endl;
    cerr << "  Reads slices of 3D image specified by input" << endl;
    cerr << "         a) regular expression: <regexp>.tga can be used" << endl;
    cerr << "         b) or file name in i3d format: <file_name>.i3d" << endl;
    cerr << endl;
    cerr << "  Writes r,g or b channel to output file red_output.i3d " << endl;
    cerr << "  green_output.i3d, or blue_output.i3d" << endl;
    cerr << "  At least one of r g b must be specified" << endl;
    cerr << endl << "Example:" << endl;
    cerr << "  " << argv[0] << " \"0001_[0-9][0-9].tga\" rg 0001" << endl;
    cerr << "  saves red, green channel of 3d image to red_0001.i3d" <<endl;
    cerr << "  green_0001.i3d, respectively" <<endl;
      
   exit(1);
  }
  
  try  {
    /* Nacteni obrazu z disku */
    cout << "Reading image " << argv[1] << "..." << endl;

	 ImgVoxelType vt = ReadImageType(argv[1]);
	 cerr << "Voxel=" << VoxelTypeToString(vt) << endl;
	
	switch (vt)
		{
		case RGBVoxel:
			{
			decompose<RGB, GRAY8>(argv[1], argv[2], argv[3]);
			break;
			}
		case RGB16Voxel:
			{
			decompose<RGB16, GRAY16>(argv[1], argv[2], argv[3]);
			break;
			}
		default:
			cerr << "Just only RGB a RGB16 image" << 
					" decomposition is supported." << endl;
		}
	
  }
  catch (IOException& e) {
    cerr << e <<endl;
    exit(1);
  }
  catch (InternalException& e) {
    cerr << e <<endl;
    exit(1);
  }
  catch (bad_alloc&) {
    cerr << "Not enough memory." << endl;
    exit(1);
  }
  catch (...) {
    cerr << "Decompose: Unknown exception." << endl;
    exit(1);
  }
  
  return 0;
}
