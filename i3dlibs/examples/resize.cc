/*
 * i3dlib - image manipulation library
 *
 * Copyright (C) 2000-2006   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 * FILE: resize.cc
 * 
 * Resize image
 *
 * Petr Matula, pem@fi.muni.cz, 2001
 */

#include <iostream>
#include <stdlib.h>

#include "image3d.h"
#include "imgfiles.h"
#include "transform.h"
using namespace std;
using namespace i3d;

template <class Voxel> void DoResize(const char *in_file,
                                     const char *out_file,
                                     size_t sx,
                                     size_t sy,
                                     size_t sz)
{
  cout << "Reading image..." << endl;
  Image3d<Voxel> i(in_file);

  Image3d<Voxel> j;
  
  cout << "Resizing..." << endl;
  Resize(i,j,sx,sy,sz);
  
  cout << "Saving image..." << endl;
  j.SaveImage(out_file);
}

int main(int argc, char* argv[]) 
{
  if (argc < 6) {
   cerr << argv[0] << " ... image resampling\n" 
	"Copyright (C) 2006 Centre for Biomedical Image Analysis \n"
	"This is free software; see the source for copying conditions. "
	"There is NO\nwarranty; not even for MERCHANTABILITY or FITNESS FOR "
	"A PARTICULAR PURPOSE.\n\n";
    cerr << "Usage: "<< argv[0] << " input output sx sy sz" << endl;
    cerr << "  resizes input image so that output image has size sx x sy x sz" << endl;
   exit(1);
  }
  
  try  {
    cerr << "Reading image Type" << endl;
    ImgVoxelType vt = ReadImageType(argv[1]);
    
    switch (vt)
      {
      case Gray16Voxel: 
        {
          DoResize<GRAY16>(argv[1],argv[2],
                          atoi(argv[3]),atoi(argv[4]),atoi(argv[5]));
          break;
        }
      case Gray8Voxel: 
        {
          DoResize<GRAY8>(argv[1],argv[2],
                          atoi(argv[3]),atoi(argv[4]),atoi(argv[5]));
          break;
        }
      case RGBVoxel:
        {
          DoResize<RGB>(argv[1],argv[2],
                        atoi(argv[3]),atoi(argv[4]),atoi(argv[5]));
          break;
        }
      default:
        cerr << "Other voxel types not supported" << endl;
      }
  }
  catch (IOException& e) {
    cerr << e <<endl;
    exit(1);
  }
  catch (InternalException& e) {
    cerr << e <<endl;
    exit(1);
  }
  catch (bad_alloc&) {
    cerr << "Not enough memory." << endl;
    exit(1);
  }
  catch (...) {
    cerr << "Resize: Unknown exception." << endl;
    exit(1);
  }
  
  return 0;
}
