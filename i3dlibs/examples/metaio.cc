/*
 * i3dlib - image manipulation library
 *
 * Copyright (C) 2000-2006   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

  /*
	*
	* FILE: metaio.cc
   *
   * Example of reading and writing images in METAIO i3d, tiff, jpeg file 
   * formats.
   * It is a raw format used to save image of the memory to the disk
   * it can save float, gray8, gray6, rgb8 and rgb16 voxels.
   *
   *
   * Honza Huben� xhubeny@fi.muni.cz 2004
   */
#include "image3d.h"
#include "imgfiles.h"
#include <iostream>
#include <map>
#include <string>
#include <vector>

using namespace std;
using namespace i3d;

template <class Voxel> void ReadWriteRawImage(const char *in_file,
                                    const char *out_file)
{
  Image3d<Voxel> i(in_file);
  
  /* Save the image to raw with one raw file per slice -- compression 
	* set to None
	*/
  //i.SaveImage(out_file, IMG_METAIO, false);
  
  /* Save the image to raw with one raw file per all slices 
	* compression set to DefaultComp
	*/
  cout << "Saving image..." << endl;
  cout << in_file << " " <<out_file << endl;
  i.SaveImage(out_file);
}

int main(int argc, char* argv[]) 
{
  if (argc < 3) {
   cerr << argv[0] << " ... an example of MetaIO image format manipulation\n" 
	"Copyright (C) 2006 Centre for Biomedical Image Analysis \n"
	"This is free software; see the source for copying conditions. "
	"There is NO\nwarranty; not even for MERCHANTABILITY or FITNESS FOR "
	"A PARTICULAR PURPOSE.\n\n";
    cerr << "Usage: "<< argv[0] << "input output" << endl;
    cerr << "  input  - input  filename (*.jpeg, *.jpg, *.tif, *.tga, *.mha)" << endl;
    cerr << "  output - output  filename (*.jpeg, *.jpg, *.tif, *.tga, *.mha)" << endl;
   exit(1);
  }
  
  try  {
    // zjisteni typu vstupniho souboru
    // !!! Nefunguje zjisteni souboru z regularniho vyrazu !!!
    cerr << "Reading image Type" << endl;
    ImgVoxelType vt = ReadImageType(argv[1]);
    cerr << "Voxel=" << VoxelTypeToString(vt) << endl;
    
    switch (vt)
      {
      case Gray8Voxel: 
          ReadWriteRawImage<GRAY8>(argv[1],argv[2]);
          break;
      case RGBVoxel:
          ReadWriteRawImage<RGB>(argv[1],argv[2]);
          break;
      default:
        cerr << "Other voxel types not supported" << endl;
      }
  }
  catch (IOException& e) {
    cerr << e <<endl;
    exit(1);
  }
  catch (InternalException& e) {
    cerr << e <<endl;
    exit(1);
  }
  catch (bad_alloc&) {
    cerr << "Not enough memory." << endl;
    exit(1);
  }
  catch (...) {
    cerr << "Unknown exception." << endl;
    exit(1);
  }
  
  return 0;
}

