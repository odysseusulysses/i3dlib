/*
 * i3dlib - image manipulation library
 *
 * Copyright (C) 2000-2006   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 * FILE: Canny2D.cc
 * 
 * Canny Edge Detector
 *
 * Marek Kasik, xkasik1@fi.muni.cz, 2003
 */

#include <iostream>
#include "image3d.h"
#include "imgfiles.h"
#include "edge.h"

using namespace std;
using namespace i3d;

template <class Voxel> void DoCanny2D(const char *in_file,const char *out_file,float sigma,float scale)
{
#ifdef WITH_FFTW
  cout << "Reading image..." << endl;
  Image3d<Voxel> i(in_file);
  Image3d<Voxel> o;
  o.MakeRoom(i.GetSize());
	
  cout << "Doing Canny3D..." << endl;

  Canny3D(i,o,sigma,scale);
  
  cout << "Saving image..." << endl;

  o.SaveImage(out_file, IMG_UNKNOWN, false);
#else
#warning "Canny support disabled. You should enable it and recompile i3dlib."
#endif
}

int main(int argc, char* argv[]){
  if(argc<4){
   cerr << argv[0] << " ... canny edge detection \n" 
	"Copyright (C) 2006 Centre for Biomedical Image Analysis \n"
	"This is free software; see the source for copying conditions. "
	"There is NO\nwarranty; not even for MERCHANTABILITY or FITNESS FOR "
	"A PARTICULAR PURPOSE.\n\n";
    cerr << "Usage: "<< argv[0] << " input output sigma [scale]" << endl;
    cerr << "  input  - input image to load can be specified by" << endl;
    cerr << "           a) regular expression: <regexp>.tga" << endl;
    cerr << "           b) file name in i3d format: <file_name>.i3d" << endl;
    cerr << "  output - name of output file " << endl;
    cerr << "  sigma - used sigma " << endl;
    cerr << "  scale - scale gradient magnitudes to do better histogram " << endl;    
   exit(1);
  }
  
  float	sigma,scale;
  
  try{
    cerr<<"Reading image Type"<<endl;
    ImgVoxelType vt = ReadImageType(argv[1]);
    cerr<<"Voxel="<<VoxelTypeToString(vt)<<endl;
    
    sigma=atof(argv[3]);
    scale=(argc>=5)?(atof(argv[4])):(1.0);
    
    switch(vt){
      case Gray8Voxel:{
          DoCanny2D<GRAY8>(argv[1],argv[2],sigma,scale);
          break;
      }
      case RGBVoxel:{
          DoCanny2D<RGB>(argv[1],argv[2],sigma,scale);
          break;
      }
      default:
        cerr<<"Other voxel types not supported"<<endl;
    }

  }
  catch(IOException& e){
    cerr<<e<<endl;
    exit(1);
  }
  catch(InternalException& e){
    cerr<<e<<endl;
    exit(1);
  }
  catch(bad_alloc&){
    cerr<<"Not enough memory."<<endl;
    exit(1);
  }
  catch(...) {
    cerr<<"Canny2D: Unknown exception."<<endl;
    exit(1);
  }
  
  return 0;
}
