/*
 * i3dlib - image manipulation library
 *
 * Copyright (C) 2000-2006   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 * FILE: emax.cc
 *
 * an example of EMAX transform applied on an image
 *
 * Pavel Matula, pam@fi.muni.cz, 2009
 * 
 */

#include <iostream>
#include "image3d.h"
#include "imgfiles.h"
#include "filters.h"
#include "regions.h"
#include "morphology.h"
#include <time.h>

using namespace std;
using namespace i3d;

double diffclock(clock_t clock1,clock_t clock2)
{
	double diffticks=clock1-clock2;
	double diffms=(diffticks)/CLOCKS_PER_SEC;
	return diffms;
}

#define INIT_CLOCK() clock_t clock_begin, clock_end;
#define START_CLOCK() clock_begin = clock()
#define END_CLOCK() clock_end=clock(); cout << "Time elapsed: " << double(diffclock(clock_end,clock_begin)) << " s"<< endl;


template <class Voxel> void DoEMAX(const char *in_file,
                                   const char *out_file)
{
	INIT_CLOCK();
  
  char out_file1[1000];
  char out_file2[1000];

  sprintf(out_file1, "emax_sweep_%s", out_file);
  sprintf(out_file2, "emax_%s", out_file);


  i3d::Image3d<float> img_float;
  float sigmax = 2.0, sigmay = 2.0, sigmaz = 2.0;
  Voxel emax_h = 20;

  START_CLOCK();
  cout << "Reading image..." << endl;
  Image3d<Voxel> img_in(in_file);
  END_CLOCK();

  Image3d<Voxel> img_out;
  Image3d<Voxel> tmp;
  LabeledImage3d<size_t, Voxel> limg;
  

  START_CLOCK();
  cout << "Doing Gauss..." << endl;
  i3d::GrayToFloat (img_in, img_float);
  i3d::GaussIIR (img_float, sigmax, sigmay, sigmaz);
  i3d::FloatToGrayNoWeight (img_float, img_in);
  END_CLOCK();

  //START_CLOCK();
  //cout << "labeling..." << endl;
  //limg.CreateRegions (img_in);
  //END_CLOCK();

  cout << "Doing EMAX..." << endl;

  START_CLOCK();
  cout << "Doing hMAX..." << endl;
  h_Max(img_in, emax_h, tmp);
  END_CLOCK();

  START_CLOCK();
  cout << "Doing rMAX by sweeping..." << endl;
  r_Max_bysweeping(tmp, img_out);
  END_CLOCK();

  START_CLOCK();
  cout << "Saving image..." << endl;
  img_out.SaveImage(out_file1);
  END_CLOCK();

  START_CLOCK();
  cout << "Doing rMAX..." << endl;
  r_Max(tmp, img_out, i3d::nb3D_6);
  END_CLOCK();


  START_CLOCK();
  cout << "Saving image..." << endl;
  img_out.SaveImage(out_file2);
  END_CLOCK();
}

int main(int argc, char* argv[]) 
{
  if (argc < 3) {
   cerr << argv[0] << " ... EMAX transform\n" 
	"Copyright (C) 2009 Centre for Biomedical Image Analysis \n"
	"This is free software; see the source for copying conditions. "
	"There is NO\nwarranty; not even for MERCHANTABILITY or FITNESS FOR "
	"A PARTICULAR PURPOSE.\n\n";
    cerr << "Usage: "<< argv[0] << " input output" << endl;
    cerr << "  input  - input image to load can be specified by" << endl;
    cerr << "           a) regular expression: <regexp>.tga" << endl;
    cerr << "           b) file name in i3d format: <file_name>.i3d" << endl;
    cerr << "  output - name of output file " << endl;
   exit(1);
  }
  
  try  {
    // zjisteni typu vstupniho souboru
    // !!! Nefunguje zjisteni souboru z regularniho vyrazu !!!
    cerr << "Reading image Type" << endl;
    ImgVoxelType vt = ReadImageType(argv[1]);
    cerr << "Voxel=" << VoxelTypeToString(vt) << endl;
    
    switch (vt)
      {
      case Gray16Voxel: 
        {
          DoEMAX<GRAY16>(argv[1],argv[2]);
          break;
        }
      //case RGBVoxel:
      //  {
      //    DoEMAX<RGB>(argv[1],argv[2]);
      //    break;
      //  }
      default:
        cerr << "Other voxel types not supported" << endl;
      }
  }
  catch (IOException& e) {
    cerr << e <<endl;
    exit(1);
  }
  catch (InternalException& e) {
    cerr << e <<endl;
    exit(1);
  }
  catch (bad_alloc&) {
    cerr << "Not enough memory." << endl;
    exit(1);
  }
  catch (...) {
    cerr << "Gauss: Unknown exception." << endl;
    exit(1);
  }
  
  return 0;
}
