/*
 * i3dlib - image manipulation library
 *
 * Copyright (C) 2000-2006   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 * FILE: histogram.cc
 * 
 * Computing of histogram of a grayscale image.
 *
 * Petr Mejzl�k, mejzlik@fi.muni.cz, 2002
 */

#include <iostream>
#include <iomanip>
#include "image3d.h"
#include "imgfiles.h"
#include "histogram.h"

using namespace std;
using namespace i3d;

int main(int argc, char* argv[]) 
{

  if (argc != 5 ) {
   cerr << argv[0] << " ... computing of histogram of a greyscale image\n" 
	"Copyright (C) 2006 Centre for Biomedical Image Analysis \n"
	"This is free software; see the source for copying conditions. "
	"There is NO\nwarranty; not even for MERCHANTABILITY or FITNESS FOR "
	"A PARTICULAR PURPOSE.\n\n";
    cerr << "Usage: "<< argv[0] << " input output LM_factor histo_smooth" << endl;
    cerr << "  input  - input image to load can be specified by" << endl;
    cerr << "           a) regular expression: <regexp>.tga" << endl;
    cerr << "           b) file name in i3d format: <file_name>.i3d" << endl;
    cerr << "  output - name of output file " << endl;
   exit(1);
  }
  
  try  {
    // zjisteni typu vstupniho souboru
    // !!! Nefunguje zjisteni souboru z regularniho vyrazu !!!
    cerr << "Reading image Type" << endl;
    ImgVoxelType vt = ReadImageType(argv[1]);
    cerr << "Voxel=" << VoxelTypeToString(vt) << endl;
    
    switch (vt)
      {
      case Gray8Voxel: 
        {
	  byte LMfactor = atoi(argv[3]);
	  byte histo_smooth = atoi(argv[4]);

          cout << "Reading GRAY8 image..." << argv[1] << endl;
          Image3d<GRAY8> img(argv[1]);


          cout << "Computing the histogram..." << endl;
          // Compute histogram from all voxels, except of those located on the border:
          Vector3d < size_t > off (0), sz (1);
          if (img.GetSizeX() > 2) {
		off.x = 1; sz.x = img.GetSizeX() - 2;
          }
          if (img.GetSizeY() > 2) {
		off.y = 1; sz.y = img.GetSizeY() - 2;
          }
          if (img.GetSizeZ() > 2) {
		off.z = 1; sz.z = img.GetSizeZ() - 2;
          }

	  Histogram hist;
          VOI < PIXELS > inside (off, sz);
          IntensityHist (img, hist, VOItoGslice (inside, img.GetSize()));

          for (int i = 0; i < histo_smooth; i++) {
          		Histogram h;
			SmoothHist (hist, h);
			hist = h;
          }


          cout << "Computing the histogram's properties..." << endl;
	  HistInfo histInfo; 
          ComputeHistInfo (hist, histInfo, LMfactor);

          cout << "The histogram is:" << endl;
	  cout << "size = " << hist.size() << endl;
	  cout << "mean1 = " << histInfo.mean1 << endl;
	  cout << "stdDev1 = " << histInfo.stdDev1 << endl;
	  cout << "mean2 = " << histInfo.mean2 << endl;
	  cout << "stdDev2 = " << histInfo.stdDev2 << endl;
	  cout << "threshold = " << histInfo.threshold << endl;

	  float imgsz = img.GetImageSize();
	  for (size_t i = 0; i < hist.size(); i++) {
	    cout << setw(3) << i;
	    cout << setw(6) << hist[i];
	    cout << setw((int)(hist[i]/imgsz*hist.size()*5)) << 'x';
	    cout << endl;
	  }

          break;
        }
      case RGBVoxel:
        {
          cout << "Histogram implemented for grayscale only..." << argv[1] << endl;
          break;
        }
      default:
        cerr << "Other voxel types not supported" << endl;
      }
  }
  catch (IOException& e) {
    cerr << e <<endl;
    exit(1);
  }
  catch (InternalException& e) {
    cerr << e <<endl;
    exit(1);
  }
  catch (bad_alloc&) {
    cerr << "Not enough memory." << endl;
    exit(1);
  }
  catch (...) {
    cerr << "Histogram: Unknown exception." << endl;
    exit(1);
  }
  
  return 0;
}

