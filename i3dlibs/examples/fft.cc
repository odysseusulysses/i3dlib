/*
 * i3dlib - image manipulation library
 *
 * Copyright (C) 2000-2009   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 * FILE: fft.cc
 *
 * an example of fast Fourier transform
 *
 * David Svoboda <svoboda@fi.muni.cz> 2008
 * 
 */

#include <iostream>
#include "image3d.h"
#include "toolbox.h"
#include "fourier.h"

using namespace std;
using namespace i3d;

//typedef double TYP;
typedef float TYP;


int main(int argc, char* argv[]) 
{
  if (argc != 4) {
   cerr << argv[0] << " ... a simple example for fast Fourier transform\n\n" 
	"Copyright (C) 2008,2009 Centre for Biomedical Image Analysis \n"
	"This is free software; see the source for copying conditions. "
	"There is NO\nwarranty; not even for MERCHANTABILITY or FITNESS FOR "
	"A PARTICULAR PURPOSE.\n\n";
    cerr << "Usage: "<< argv[0] << 
				" <x-size> <y-size> <z-size>" << endl << endl;
   exit(1);
  }
  

	 try {
	 Image3d<TYP> img, img2;
	 img.MakeRoom(atoi(argv[1]),atoi(argv[2]), atoi(argv[3]));

	 for (size_t i=0; i<img.GetImageSize(); i++)
	 {
		  img.SetVoxel(i, (TYP) (65000 * ((TYP) i) / img.GetImageSize()));
		  //	img.SetVoxel(i, (TYP) i);
	 }

	 img.SaveImage("F600.ics");
	 exit(-1);

	Image3d<complex<TYP> > fimg;

    //time_t start, end;
    //time (&start);
	FFT(img, fimg, false);
    //time (&end);

    //double dif = difftime (end, start);
    //cout << "FFT took: " << human_readable(dif,2) << " seconds" << endl;

	 /*img.SaveImage("input.ics");
	 fimg.SaveImage("inputFFT.ics");

	 IFFT(fimg, img2, true);

	 // find the difference:
	 TYP diff = 0;

	 for (size_t i=0; i<img2.GetImageSize(); i++)
	 {
		  TYP value = img2.GetVoxel(i) - img.GetVoxel(i);
		  diff = std::max(diff, abs(value));
		  //img2.SetVoxel(i, value);
	 }
	 cout << "max error: " << diff << endl;

	 img2.SaveImage("output.ics");*/
	 }

  catch (IOException& e) {
    cerr << e <<endl;
    exit(1);
  }
  catch (InternalException& e) {
    cerr << e <<endl;
    exit(1);
  }
  catch (bad_alloc&) {
    cerr << "Not enough memory." << endl;
    exit(1);
  }
  catch (...) {
    cerr << "Unknown exception." << endl;
    exit(1);
  }
  
  
  return 0;
}
