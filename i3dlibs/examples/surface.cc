/*
 * i3dlib - image manipulation library
 *
 * Copyright (C) 2000-2006   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 * FILE: surface.cc
 * 
 * Computation of surface (3D) or perimeter (2D).
 *
 * Petr Mejzl�k, mejzlik@fi.muni.cz, 2002
 *
 */


#include <iostream>
#include <vector>
#include <valarray>
#include "threshold.h"
#include "neighbours.h"
#include "regions.h"
#include "image3d.h"
#include "print.h"

using namespace std;
using namespace i3d;

const int one_voxel_off[] = {0, 0, 0};
const Neighbourhood one_voxel_nbh(one_voxel_off, 1);

const int square2x2_off[] = {0, 0, 0,  1, 0, 0,
                             0, 1, 0,  1, 1, 0};
const Neighbourhood square2x2_nbh(square2x2_off, 4);

const int oval_off[] = {0, 0, 0,  1, 0, 0,
                        0, 1, 0,  1, 1, 0,
		       -1, 0, 0,  2, 0, 0,
		        0,-1, 0,  1,-1, 0};
const Neighbourhood oval_nbh(oval_off, 8);

const int cube2x2x2_off[] = {0, 0, 0,  1, 0, 0,  0, 1, 0,  1, 1, 0,
                             0, 0, 1,  1, 0, 1,  0, 1, 1,  1, 1, 1};
const Neighbourhood cube2x2x2_nbh(cube2x2x2_off, 8);

const int br_cube2x2x2_off[] = {0, 0, 0,  1, 0, 0,  0, 1, 0,  1, 1, 0,
                                0, 0, 1,  1, 0, 1,  0, 1, 1};
const Neighbourhood br_cube2x2x2_nbh(cube2x2x2_off, 7);

const Neighbourhood sphere10_nbh(10.0, 10.0, 10.0);

// set_win(): set all voxels to 0, except of voxels of an object given 
// by vector v, which are set to 255.

void set_win(Image3d<GRAY8>& img, vector<GRAY8*> v)
{
	valarray<GRAY8>& data = img.GetVoxelData();
	data = 0;
	vector<GRAY8*>::iterator p;
	for (p = v.begin(); p != v.end(); ++p) **p = (GRAY8) 255;
}

// compute_surface() computes surface/perimeter of an object given
// by pointers pointing into img. 
void compute_surface(const vector<GRAY8*>& object, Image3d<GRAY8>& img)
{
    Image3d<BINARY> bimg;
    LabeledImage3d<size_t, BINARY> limg;
    const unsigned char thr = 128;
    size_t volume;
  
    set_win(img, object);
    Threshold(img, bimg, thr);
    if (img.GetNumSlices() > 1) {
        cout << "Labeling in 3D.." << endl;
    	limg.CreateRegions(bimg, nb3D_6);
    }
    else {
        cout << "Labeling in 2D.." << endl;
    	limg.CreateRegions(bimg, nb2D_4);
    }

    cout << "Removing black components.." << endl;
    RemoveBlack(limg.components); // remove background components
  
    cout << "ORIG. IMAGE:" << endl; PrintImage3d(img);
    cout << "BIMG:" << endl; PrintImage3d(bimg);
    cout << "LIMG:" << endl; PrintImage3d(limg);

    cout << "Number of components: " << limg.NumberOfComponents() << endl;
//    Surface(limg, (size_t) 1, volume);
    size_t label = limg.components.begin()->first;
    cout << "Surface of the 1st component (label=" << label << "): " \
         << Surface(limg, label, volume) << endl;
}


int main(int argc, char* argv[]) 
{
   cout << argv[0] << " ... evaluation of object surface(3D) / perimeter(2D)\n"
	"Copyright (C) 2006 Centre for Biomedical Image Analysis \n"
	"This is free software; see the source for copying conditions. "
	"There is NO\nwarranty; not even for MERCHANTABILITY or FITNESS FOR "
	"A PARTICULAR PURPOSE.\n\n";

  try {
    vector<GRAY8*> one_voxel, square_2x2, oval, \
    	 	   cube2x2x2, br_cube2x2x2, sphere10;
    Image3d<GRAY8> img2d, img3d;
  
    img2d.MakeRoom(4, 4, 1);
    GetWindow(img2d, 1, 1, 0, one_voxel_nbh, one_voxel);
    cout << endl << endl << "\t\t ONE VOXEL" << endl << endl;
    compute_surface(one_voxel, img2d);

    GetWindow(img2d, 1, 1, 0, square2x2_nbh, square_2x2);
    cout << endl << endl << "\t\t SQUARE 2x2" << endl << endl;
    compute_surface(square_2x2, img2d);

    img2d.MakeRoom(8, 8, 1);
    GetWindow(img2d, 3, 3, 0, oval_nbh, oval);
    cout << endl << endl << "\t\t OVAL" << endl << endl;
    compute_surface(oval, img2d);

    img3d.MakeRoom(8, 8, 8);
    GetWindow(img3d, 3, 3, 3, cube2x2x2_nbh, cube2x2x2);
    cout << endl << endl << "\t\t CUBE 2x2x2" << endl << endl;
    compute_surface(cube2x2x2, img3d);

    img3d.MakeRoom(8, 8, 8);
    GetWindow(img3d, 3, 3, 3, br_cube2x2x2_nbh, br_cube2x2x2);
    cout << endl << endl << "\t\t CUBE 2x2x2 WITHOUT ONE CORNER" \
         << endl << endl;
    compute_surface(br_cube2x2x2, img3d);

    img3d.MakeRoom(30, 30, 30);
    GetWindow(img3d, 15, 15, 15, sphere10_nbh, sphere10);
    cout << endl << endl << "\t\t SPHERE: RADIUS = 10" << endl << endl;
    compute_surface(sphere10, img3d);

    Neighbourhood circle_nbh;
    vector<GRAY8*> circle;
    vector<Vector3d<int> >::const_iterator i;
    for (i = sphere10_nbh.offset.begin(); i < sphere10_nbh.offset.end(); i++)
    {
      if (i->z == 0)
        circle_nbh.offset.push_back(Vector3d<int>(i->x, i->y, i->z));
    }
    img2d.MakeRoom(30, 30, 1);
    GetWindow(img2d, 15, 15, 0, circle_nbh, circle);
    cout << endl << endl << "\t\t CIRCLE: RADIUS = 10" << endl << endl;
    compute_surface(circle, img2d);

  }
  catch (IOException& e) {
    cerr << e <<endl;
    exit(1);
  }
  catch (InternalException& e) {
    cerr << e <<endl;
    exit(1);
  }
  catch (bad_alloc&) {
    cerr << "Not enough memory." << endl;
    exit(1);
  }
  catch (...) {
    cerr << "Surface: Unknown exception." << endl;
    exit(1);
  }
  
  return 0;
}

