############################################################################
#
#  i3dlib - image manipulation library
#
#  Copyright (C) 2000-2006   Centre for Biomedical Image Analysis (CBIA)
# 
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Library General Public
#  License as published by the Free Software Foundation; either
#  version 2 of the License, or (at your option) any later version.
# 
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Library General Public License for more details.
# 
#  You should have received a copy of the GNU Library General Public
#  License along with this library; if not, write to the Free
#  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#
#############################################################################
#
# FILE: cross platform CMake configure script
#
# David Svoboda <svoboda@fi.muni.cz>
#
#############################################################################
cmake_minimum_required(VERSION 2.6)

# set the compilation way
SET(CMAKE_VERBOSE_MAKEFILE ON CACHE BOOL
    "Tracing the compilation process." FORCE)

# test for presence of STD
INCLUDE(${CMAKE_ROOT}/Modules/TestForSTDNamespace.cmake)

# include the necessary macros
INCLUDE(${CMAKE_ROOT}/Modules/CheckIncludeFileCXX.cmake)

# find some necessary files - e.g. sstream
CHECK_INCLUDE_FILE_CXX("sstream" HAVE_SSTREAM)


# windows specific
IF (WIN32)
	SET(CMAKE_EXE_LINKER_FLAGS_DEBUG
		"/NODEFAULTLIB:LIBCD /NODEFAULTLIB:MSVCRT" CACHE STRING "" FORCE)
	SET(CMAKE_EXE_LINKER_FLAGS_RELEASE
		"/NODEFAULTLIB:LIBC /NODEFAULTLIB:LIBCD" CACHE STRING "" FORCE)
ENDIF (WIN32)

# unix specific
IF (UNIX AND CMAKE_COMPILER_IS_GNUCXX AND ALGO_WITH_LAPACK)
	SET(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -Xlinker -defsym -Xlinker MAIN__=main")
ENDIF (UNIX AND CMAKE_COMPILER_IS_GNUCXX AND ALGO_WITH_LAPACK)


# use the i3dcore and i3dalgo directories and libraries
LINK_DIRECTORIES(${CMAKE_BIN_DIR}/src)
LINK_DIRECTORIES(${CMAKE_BIN_DIR}/src-core)
INCLUDE_DIRECTORIES(${CMAKE_SOURCE_DIR}/src)
INCLUDE_DIRECTORIES(${CMAKE_SOURCE_DIR}/src-core)
INCLUDE_DIRECTORIES(${CORE_BINARY_DIR})
LINK_LIBRARIES(${ALGO_LIBRARY_NAME} ${CORE_LIBRARY_NAME} ${LIBS})


cmake_policy(SET CMP0003 OLD)
ADD_EXECUTABLE(decompose decompose.cc)
ADD_EXECUTABLE(Canny2D Canny2D.cc)
ADD_EXECUTABLE(gauss gauss.cc)
ADD_EXECUTABLE(neighbours neighbours.cc)
ADD_EXECUTABLE(histogram histogram.cc)
ADD_EXECUTABLE(regions regions.cc)
ADD_EXECUTABLE(resize resize.cc)
ADD_EXECUTABLE(metaio metaio.cc)
ADD_EXECUTABLE(emax emax.cc)
ADD_EXECUTABLE(fmm fmm.cc)
ADD_EXECUTABLE(bfb bfb.cc)
ADD_EXECUTABLE(surface surface.cc)
ADD_EXECUTABLE(shapetest shapetest.cc)
#ADD_EXECUTABLE(nucl_by_watershed nucl_by_watershed.cc)
#ADD_EXECUTABLE(nucl_by_edges nucl_by_edges.cc)
ADD_EXECUTABLE(fft fft.cc)
ADD_EXECUTABLE(conv conv.cc)

IF (WIN32 AND NOT ALGO_BUILD_SHARED_LIBS)
	GET_FILENAME_COMPONENT(BLAS_LIB_DIR ${ALGO_BLAS_LIB} PATH)
	SET_TARGET_PROPERTIES(shapetest PROPERTIES LINK_FLAGS /LIBPATH:"${BLAS_LIB_DIR}")
ENDIF (WIN32 AND NOT ALGO_BUILD_SHARED_LIBS)
