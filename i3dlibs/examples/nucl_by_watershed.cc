/*
 * i3dlib - image manipulation library
 *
 * Copyright (C) 2000-2006   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
* FILE: nucl_by_watershed.cc
*
* an example of nucleus segmentaion by watershed algorithm
*
* Pavel Matula, pam@fi.muni.cz, 2009
* 
*/

#include <iostream>
#include "image3d.h"
#include "imgfiles.h"
#include "filters.h"
#include "regions.h"
#include "morphology.h"
#include "gradient.h"
#include "DistanceTransform.h"
#include "threshold.h"
#include "histogram.h"
#include <time.h>

using namespace std;
using namespace i3d;

double diffclock(clock_t clock1,clock_t clock2)
{
	double diffticks=clock1-clock2;
	double diffms=(diffticks)/CLOCKS_PER_SEC;
	return diffms;
}

#define INIT_CLOCK() clock_t clock_begin, clock_end;
#define START_CLOCK() clock_begin = clock()
#define END_CLOCK() clock_end=clock(); cout << "Time elapsed: " << double(diffclock(clock_end,clock_begin)) << " s"<< endl;


template <class Voxel> void DoAlgo(const char *in_file,
								   const char *out_file)
{
	INIT_CLOCK();

	START_CLOCK();
	cout << "Reading image..." << endl;
	Image3d<Voxel> img_in(in_file);
	END_CLOCK();

	i3d::Image3d<float> img_float;
	GrayToFloat(img_in, img_float);

	START_CLOCK();
	Image3d<float> gimg;
	gimg = img_float;
	float sigmaXsmall = 2;
	float sigmaYsmall = 2;
	float sigmaZsmall = 1;

	cout << "Gauss filtering with small sigma..." << endl;
	GaussIIR(gimg, sigmaXsmall, sigmaYsmall, sigmaZsmall); 
	END_CLOCK();
	gimg.SaveImage("gaussed1.ics");

	START_CLOCK();
	Image3d<float> grad_img_f;
	cout << "Square of gradient magnitude..." << endl;
	i3d::GradientMagnitude2(gimg, grad_img_f);
	END_CLOCK();
	grad_img_f.SaveImage("gradient.ics");
	Image3d<GRAY16> grad_img;
	FloatToGray(grad_img_f, grad_img);

	START_CLOCK();
	float sigmaX = 5;
	float sigmaY = 5;
	float sigmaZ = 2;
	cout << "Gauss filtering with large sigma..." << endl;
	GaussIIR(img_float, sigmaX, sigmaY, sigmaZ); 
	END_CLOCK();

	FloatToGray(img_float, img_in);

	img_in.SaveImage("gaussed2.ics");



	START_CLOCK();
	cout << "Thresholding..." << endl;

	i3d::Histogram hist;
	i3d::IntensityHist(img_in, hist);

	Voxel threshold = i3d::FindIsoDataThreshold(hist);

	Threshold(img_in, threshold);
	END_CLOCK();
	img_in.SaveImage("thresholded.ics");

	Image3d<float> fimg;
	fimg.MakeRoom (img_in.GetSize());
	fimg.SetOffset (img_in.GetOffset());
	fimg.SetResolution (img_in.GetResolution());

	for (size_t i = fimg.GetImageSize(); i > 0; i--)
		fimg.SetVoxel (i - 1, (img_in.GetVoxel(i-1) > 0) ? 1.0f : 0.0f);

	// vymazani krajnich rezu.. hybrid saito nevhodne funguje na okraji
	for (size_t x = 0; x < fimg.GetSizeX(); ++x) {
		for (size_t y = 0; y < fimg.GetSizeY(); ++y) {
			if (fimg.GetVoxel(x, y, 0) > 0.0)
				fimg.SetVoxel(x, y, 0, 0.0);
			if (fimg.GetVoxel(x, y, fimg.GetSizeZ() - 1) > 0.0)
				fimg.SetVoxel(x, y, fimg.GetSizeZ() - 1, 0.0);
		}
	}


	START_CLOCK();
	cout << "Distance transform..." << endl;
	i3d::HybridSaito(fimg, 0, 1.0f);
	END_CLOCK();
	fimg.SaveImage("dt.ics");

	START_CLOCK();
	cout << "EMAX..." << endl;
	Voxel h = 5;
	i3d::Image3d<Voxel> img_out;
	FloatToGray(fimg, img_in);
	e_Max(img_in, h, img_out, i3d::nb3D_6);
	END_CLOCK();

	i3d::Image3d<BINARY> img_bin;
	i3d::Gray16ToBinary(img_out, img_bin);

	START_CLOCK();
	cout << "Labelling..." << endl;
	i3d::LabeledImage3d<GRAY16, BINARY> l_img;
	l_img.CreateRegionsFF (img_bin);
	cout << "# of components: " << l_img.NumberOfComponents() << endl;
	END_CLOCK();

	START_CLOCK();
	cout << "removing small components..." << endl;

	size_t min_vol = 30;
	i3d::ComponentToRemove<BINARY> pred (BINARY(0), true, true, true, false, false, 0, 0, 
		min_vol > 0, false, min_vol, 0, false, true);
	i3d::EraseComponents (img_bin, l_img, pred);
	cout << "# of components: " << l_img.NumberOfComponents() << endl;

	img_bin.SaveImage("markers.ics");

	START_CLOCK();
	cout << "Finding background pixel..." << endl;
	END_CLOCK();

	START_CLOCK();
	cout << "Minima imposition..." << endl;
	for (size_t i = 0; i < l_img.GetImageSize(); ++i) {
		img_bin.SetVoxel(i, l_img.GetVoxel(i) > 0 ? i3d::BINARY(1) : i3d::BINARY(0));
	} 

	img_bin.SetVoxel(0, i3d::BINARY(1));


	img_bin.SaveImage("mask.ics");
	grad_img.SaveImage("gradimg_before.ics");
	impose_minima(grad_img, img_bin);
	grad_img.SaveImage("gradimg_after.ics");
	END_CLOCK();

	START_CLOCK();
	Image3d<int> out;
	cout << "Watershed..." << endl;
	WatershedLin(grad_img, out, 6);
	END_CLOCK();

	for (int i = 0; i < img_out.GetImageSize(); ++i) {
		img_out.SetVoxel(i, out.GetVoxel(i) + 10);
	}

	START_CLOCK();
	cout << "Saving image..." << endl;
	img_out.SaveImage(out_file);
	END_CLOCK();
}

int main(int argc, char* argv[]) 
{
  if (argc < 3) {
   cerr << argv[0] << " ... Nucleus segmenation by watershed\n" 
	"Copyright (C) 2009 Centre for Biomedical Image Analysis \n"
	"This is free software; see the source for copying conditions. "
	"There is NO\nwarranty; not even for MERCHANTABILITY or FITNESS FOR "
	"A PARTICULAR PURPOSE.\n\n";
    cerr << "Usage: "<< argv[0] << " input output" << endl;
    cerr << "  input  - input image" << endl;
    cerr << "  output - output file name" << endl;
   exit(1);
  }
  
  try  {
    // zjisteni typu vstupniho souboru
    // !!! Nefunguje zjisteni souboru z regularniho vyrazu !!!
    cerr << "Reading image Type" << endl;
    ImgVoxelType vt = ReadImageType(argv[1]);
    cerr << "Voxel=" << VoxelTypeToString(vt) << endl;
    
    switch (vt)
      {
      case Gray16Voxel: 
        {
          DoAlgo<GRAY16>(argv[1],argv[2]);
          break;
        }
      default:
        cerr << "Other voxel types not supported" << endl;
      }
  }
  catch (IOException& e) {
    cerr << e <<endl;
    exit(1);
  }
  catch (InternalException& e) {
    cerr << e <<endl;
    exit(1);
  }
  catch (bad_alloc&) {
    cerr << "Not enough memory." << endl;
    exit(1);
  }
  catch (...) {
    cerr << "Unknown exception." << endl;
    exit(1);
  }
  
  return 0;
}
