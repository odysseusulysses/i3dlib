/*
 * i3dlib - image manipulation library
 *
 * Copyright (C) 2000-2006   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 * FILE: neighbours.cc
 *
 * Example of using neighbours in Image3d
 *
 * Pavel Matula, pam@fi.muni.cz, 2001
 */

#include <iostream>
#include <vector>
#include <valarray>
#include "vector3d.h"
#include "image3d.h"
#include "neighbours.h"
#include "print.h"
using namespace std;
using namespace i3d;

int main(int argc, char* argv[]) 
{
   cout << argv[0] << " ... an example of using 'neighbours'"
		" structure in I3DLIB\n" 
	"Copyright (C) 2006 Centre for Biomedical Image Analysis \n"
	"This is free software; see the source for copying conditions. "
	"There is NO\nwarranty; not even for MERCHANTABILITY or FITNESS FOR "
	"A PARTICULAR PURPOSE.\n\n";
	
  Image3d<GRAY8> img;
  img.MakeRoom(5, 4, 3);
  GRAY8 b[] = {0, 1, 2, 3, 4, 
             5, 6, 7, 8, 9,
             1, 2, 1, 2, 1,
             2, 3, 3, 1, 2,

             0, 0, 0, 0, 0,
             1, 1, 1, 1, 1, 
             2, 2, 2, 2, 2,
             3, 3, 3, 3, 3,
             
             9, 8, 7, 6, 5,
             4, 3, 2, 1, 0,
             8, 8, 8, 8, 8,
             4, 4, 4, 4, 4};

  const valarray<GRAY8> va(b, 5*4*3);
  img.GetVoxelData() = va;

  cout << "Image3d" << endl;
  PrintImage3d(img);

  cout << "Neighbourhood of all voxels:" << endl;
  vector<GRAY8 *> win;
  size_t i, j;
  for (i = 0, j = 0; i < img.GetImageSize(); ++i) {
    if (j == 0) { // the window has to be recomputed 
      cout << endl;
      j = GetWindow(img, img.GetX(i), img.GetY(i), img.GetZ(i), nb3D_6, win);
    }
    else {
      MoveWindow(win);
      j--;
    }
    cout << " ";
    print(win);
  } // for

  cout << endl;
}
