/*
 * i3dlib - image manipulation library
 *
 * Copyright (C) 2000-2009   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 * FILE: conv.cc
 *
 * an example of image convolution
 *
 * David Svoboda <svoboda@fi.muni.cz> 2008
 * 
 */

#include <iostream>
#include "image3d.h"
#include "convolution.h"
#include "filters.h"
#include "transform.h"
#include "toolbox.h"
#include <time.h>
#include <valarray>

using namespace std;
using namespace i3d;

template <class T_in, class T_out> void DoConv(const char *in_file,
															  const char *kernel_file,
															  const char *out_file)
{
  cout << "Reading input image ..." << endl;
  Image3d<T_in> input_image(in_file);

  cout << "Reading kernel ..." << endl;
  Image3d<T_in> kernel(kernel_file);

  Image3d<T_out> solution;


  Vector3d<float> res_img, res_psf;
  res_img = input_image.GetResolution().GetRes();
  res_psf = kernel.GetResolution().GetRes();

  if (res_img != res_psf)
  {
		cout << "Resampling required ... I'll do it myself :)" << endl;
		ResampleToDesiredResolution(kernel, res_img, LANCZOS);
  }

  cout << "Performing convolution ..." << endl;

  time_t start, end;

  time (&start);
  Convolution<double>(input_image, kernel, solution);
  time (&end);

  double dif = difftime (end, start);

  cout << "Convolution took: " << human_readable(dif,2) << " seconds" << endl;

  cout << "Saving the result ..." << endl;
  solution.SaveImage(out_file);
}

int main(int argc, char* argv[]) 
{
  if (argc < 4) {
   cerr << argv[0] << " ... a simple image convolution\n\n" 
	"Copyright (C) 2008,2009 Centre for Biomedical Image Analysis \n"
	"This is free software; see the source for copying conditions. "
	"There is NO\nwarranty; not even for MERCHANTABILITY or FITNESS FOR "
	"A PARTICULAR PURPOSE.\n\n";
    cerr << "Usage: "<< argv[0] << 
			" <input image> <kernel> <output image> " << endl <<
			"- both input image and kernel must be of the same bit-depth.\n\n";
	 			
   exit(1);
  }

  try  {
    cerr << "Reading image Type" << endl;
    ImgVoxelType vt = ReadImageType(argv[1]);
    cerr << "Voxel=" << VoxelTypeToString(vt) << endl;
    
    switch (vt)
      {
      case BinaryVoxel: 
        {
          DoConv<BINARY,float>(argv[1],argv[2],argv[3]);
          break;
        }
      case FloatVoxel: 
        {
          DoConv<float,float>(argv[1],argv[2],argv[3]);
          break;
        }
      case Gray8Voxel: 
        {
          DoConv<GRAY8, GRAY8>(argv[1],argv[2],argv[3]);
          break;
        }
      case Gray16Voxel: 
        {
          DoConv<GRAY16, GRAY16>(argv[1],argv[2],argv[3]);
          break;
        }
      default:
        cerr << "A strange voxel type." << endl;
      }
  }
  catch (IOException& e) {
    cerr << argv[0] << ": " << e << endl;
    exit(1);
  }
  catch (InternalException& e) {
    cerr << argv[0] << ": " << e <<endl;
    exit(1);
  }
  catch (bad_alloc&) {
    cerr << argv[0] << ": Not enough memory." << endl;
    exit(1);
  }
  catch (const char *s) {
    cerr << argv[0] << ": " << s << endl;
    exit(1);
  }
  catch (string &s) {
    cerr << argv[0] << ": " << s << endl;
    exit(1);
  }
  
  return 0;
}
