/*
 * i3dlib - image manipulation library
 *
 * Copyright (C) 2000-2006   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 * FILE: bfb.cc
 *
 * an example of balanced forward backward diffusion applied on an image
 *
 * Pavel Matula, pam@fi.muni.cz, 2009
 * 
 */

#include <iostream>
#include "image3d.h"
#include "imgfiles.h"
//#include "filters.h"
//#include "regions.h"
#include "LevelSet.h"
#include <time.h>

using namespace std;
using namespace i3d;

double diffclock(clock_t clock1,clock_t clock2)
{
	double diffticks=clock1-clock2;
	double diffms=(diffticks)/CLOCKS_PER_SEC;
	return diffms;
}

#define INIT_CLOCK() clock_t clock_begin, clock_end;
#define START_CLOCK() clock_begin = clock()
#define END_CLOCK() clock_end=clock(); cout << "Time elapsed: " << double(diffclock(clock_end,clock_begin)) << " s"<< endl;


template <class Voxel> void DoBFB(const char *in_file,
								  const char *out_file)
{
#ifdef WITH_LAPACK
	INIT_CLOCK();

	START_CLOCK();
	cout << "Reading image..." << endl;
	Image3d<Voxel> img_in(in_file);
	END_CLOCK();

	i3d::Image3d<float> img_float;

	START_CLOCK();
	cout << "BFB computation..." << endl;

	size_t iterations = 8;
	float timestep = 20.0;
	i3d::AOSTVFilter <Voxel, float> TV (img_in, img_float);  
	TV.SetMaximumNumberOfIterations (iterations); 
	TV.SetExponent (2.0f); 
	TV.SetGlobalTimeStep (timestep);
	TV.Execute ();                
	END_CLOCK();

	START_CLOCK();
	cout << "Saving image..." << endl;
	img_float.SaveImage(out_file);
	END_CLOCK();
#else
	#ifdef _MSC_VER
		#pragma message("Unless you enable WITH_LAPACK, the application 'bfb' won't work.")
	#else
		#warning "Unless you enable WITH_LAPACK, the application 'bfb' won't work."
	#endif
#endif
}

int main(int argc, char* argv[]) 
{
  if (argc < 3) {
   cerr << argv[0] << " ... Fast marching method\n" 
	"Copyright (C) 2009 Centre for Biomedical Image Analysis \n"
	"This is free software; see the source for copying conditions. "
	"There is NO\nwarranty; not even for MERCHANTABILITY or FITNESS FOR "
	"A PARTICULAR PURPOSE.\n\n";
    cerr << "Usage: "<< argv[0] << " input output" << endl;
    cerr << "  input  - input image" << endl;
    cerr << "  output - output file name" << endl;
   exit(1);
  }
  
  try  {
    // zjisteni typu vstupniho souboru
    // !!! Nefunguje zjisteni souboru z regularniho vyrazu !!!
    cerr << "Reading image Type" << endl;
    ImgVoxelType vt = ReadImageType(argv[1]);
    cerr << "Voxel=" << VoxelTypeToString(vt) << endl;
    
    switch (vt)
      {
      case Gray16Voxel: 
        {
          DoBFB<GRAY16>(argv[1],argv[2]);
          break;
        }
      default:
        cerr << "Other voxel types not supported" << endl;
      }
  }
  catch (IOException& e) {
    cerr << e <<endl;
    exit(1);
  }
  catch (InternalException& e) {
    cerr << e <<endl;
    exit(1);
  }
  catch (bad_alloc&) {
    cerr << "Not enough memory." << endl;
    exit(1);
  }
  catch (...) {
    cerr << "Unknown exception." << endl;
    exit(1);
  }
  
  return 0;
}
