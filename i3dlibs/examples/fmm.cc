/*
 * i3dlib - image manipulation library
 *
 * Copyright (C) 2000-2006   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 * FILE: fmm.cc
 *
 * an example of fast marching method applied on an image
 *
 * Pavel Matula, pam@fi.muni.cz, 2009
 * 
 */

#include <iostream>
#include "image3d.h"
#include "imgfiles.h"
//#include "filters.h"
//#include "regions.h"
#include "LevelSet.h"
#include <time.h>

using namespace std;
using namespace i3d;

double diffclock(clock_t clock1,clock_t clock2)
{
	double diffticks=clock1-clock2;
	double diffms=(diffticks)/CLOCKS_PER_SEC;
	return diffms;
}

#define INIT_CLOCK() clock_t clock_begin, clock_end;
#define START_CLOCK() clock_begin = clock()
#define END_CLOCK() clock_end=clock(); cout << "Time elapsed: " << double(diffclock(clock_end,clock_begin)) << " s"<< endl;


template <class Voxel> void DoFMM(const char *in_file,
								  const char *out_file)
{
	INIT_CLOCK();

	START_CLOCK();
	cout << "Reading image..." << endl;
	Image3d<Voxel> img_in(in_file);
	END_CLOCK();

	double sigmax = 1.2;
	double sigmay = 1.2;
	double sigmaz = 1.2; 
	double width = 1.5;

	i3d::Image3d<float> img_float;
	i3d::Image3d<float> img_float_out;

	Image3d<Voxel> img_out;
	img_out = img_in;

	START_CLOCK();
	cout << "Speed function computation..." << endl;
	SpeedFunctionStandard(img_in, img_float, sigmax, sigmay, sigmaz, width);
	END_CLOCK();

	SeedPointsVector SeedPoints;
	SeedPoints.push_back(i3d::Vector3d<size_t>(0, 0, 0));
	SeedPoints.push_back(i3d::Vector3d<size_t>(0, 0, img_in.GetSizeZ() - 1));
	SeedPoints.push_back(i3d::Vector3d<size_t>(0, img_in.GetSizeY() - 1, 0));
	SeedPoints.push_back(i3d::Vector3d<size_t>(0, img_in.GetSizeY() - 1, img_in.GetSizeZ() - 1));
	SeedPoints.push_back(i3d::Vector3d<size_t>(img_in.GetSizeX() - 1, 0, 0));
	SeedPoints.push_back(i3d::Vector3d<size_t>(img_in.GetSizeX() - 1, 0, img_in.GetSizeZ() - 1));
	SeedPoints.push_back(i3d::Vector3d<size_t>(img_in.GetSizeX() - 1, img_in.GetSizeY() - 1, 0));
	SeedPoints.push_back(i3d::Vector3d<size_t>(img_in.GetSizeX() - 1, img_in.GetSizeY() - 1, img_in.GetSizeZ() - 1));

	START_CLOCK();
	cout << "Fast marching method..." << endl;
	FastMarchingMethod (img_float, img_float_out, SeedPoints);
	END_CLOCK();

	float value;
	size_t Start = 3;
	float Threshold = 2.0;
	size_t Bins = 100;
	size_t Step = 1;
	double Sigma = 1.0;
	double Radius = 3.0;

	img_float_out.SaveImage("speedfunction.ics");


	START_CLOCK();
	cout << "Finding contour level..." << endl;
	FindContourLevel(img_float_out, value, Start, Threshold, Bins, Step, Sigma, Radius);
	END_CLOCK();

	Voxel *out_ptr = img_out.GetFirstVoxelAddr();
	float *out_float_ptr = img_float_out.GetFirstVoxelAddr();
	for (size_t i = 0; i < img_out.GetImageSize(); ++i) {
		if (*out_float_ptr > value) {
			*out_ptr = std::numeric_limits<Voxel>::max();
		} else {
			*out_ptr = (Voxel)0;
		}
		out_ptr++; out_float_ptr++;
	}

  START_CLOCK();
  cout << "Saving image..." << endl;
  img_out.SaveImage(out_file);
  END_CLOCK();
}

int main(int argc, char* argv[]) 
{
  if (argc < 3) {
   cerr << argv[0] << " ... Fast marching method\n" 
	"Copyright (C) 2009 Centre for Biomedical Image Analysis \n"
	"This is free software; see the source for copying conditions. "
	"There is NO\nwarranty; not even for MERCHANTABILITY or FITNESS FOR "
	"A PARTICULAR PURPOSE.\n\n";
    cerr << "Usage: "<< argv[0] << " input output" << endl;
    cerr << "  input  - input image" << endl;
    cerr << "  output - output file name" << endl;
   exit(1);
  }
  
  try  {
    // zjisteni typu vstupniho souboru
    // !!! Nefunguje zjisteni souboru z regularniho vyrazu !!!
    cerr << "Reading image Type" << endl;
    ImgVoxelType vt = ReadImageType(argv[1]);
    cerr << "Voxel=" << VoxelTypeToString(vt) << endl;
    
    switch (vt)
      {
      case Gray16Voxel: 
        {
          DoFMM<GRAY16>(argv[1],argv[2]);
          break;
        }
      default:
        cerr << "Other voxel types not supported" << endl;
      }
  }
  catch (IOException& e) {
    cerr << e <<endl;
    exit(1);
  }
  catch (InternalException& e) {
    cerr << e <<endl;
    exit(1);
  }
  catch (bad_alloc&) {
    cerr << "Not enough memory." << endl;
    exit(1);
  }
  catch (...) {
    cerr << "Unknown exception." << endl;
    exit(1);
  }
  
  return 0;
}
