/*
 * i3dlib - image manipulation library
 *
 * Copyright (C) 2000-2006   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/* 
 * FILE: print.h
 *
 * Output functions for images and vectors. 
 *
 * */

#include <iostream>
#include <iomanip>
#include <vector>
#include <valarray>
#include "image3d.h"
using namespace std;
using namespace i3d;


template <class T> void print(const vector<T*>& v) {
  cout << "[ ";
  for (size_t i = 0; i < v.size(); ++i) 
    cout << (int)*(v[i]) << " ";
  cout << "]";
}

template <class T> void print(const Vector3d<T>& v) {
	  cout << v.x << ", " << v.y << ", " << v.z << endl ;
}

template <class T> void print(const valarray<T>& v) {
  for (size_t i=0; i<v.size(); ++i) 
    cout << v[i] << " ";
  cout << endl;
};

template <class T> void PrintImage3d(const Image3d<T>& img) {
  size_t dx = img.GetSizeX();
  size_t dy = img.GetSizeY();
  size_t dz = img.GetSizeZ();
  size_t x, y, z;

  cout << "Size: " << dx << " x " << dy << " x " << dz << endl;
  for (z = 0; z < dz; ++z) {
    for (y = 0; y < dy; ++y) {
      for (x = 0; x < dx; ++x) {
        cout << setw(3) << (int) img.GetVoxel(x, y, z) << " ";
      }
      cout << endl;
    }
    cout << endl;
  }
};


