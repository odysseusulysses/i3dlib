/*
 * i3dlib - image manipulation library
 *
 * Copyright (C) 2000-2006   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 *  FILE: shapetest.cc
 *
 *  testing of functions for shape characterization on binary images
 *
 *  Zdenek Hrncir, 2004 (60514@mail.muni.cz)
 */


#include <iostream>
#include <iomanip>
#ifndef _MSC_VER
#include <cmath>
#endif
#include "image3d.h"
#include "imgfiles.h"
#include "shape.h"
#include "print.h"

using namespace std;
using namespace i3d;

int main(int argc, char* argv[])
{

  if (argc < 2 ) {
   cerr << argv[0] << " ... evaluation of shape characteristics"
		" of binary images\n" 
	"Copyright (C) 2006 Centre for Biomedical Image Analysis \n"
	"This is free software; see the source for copying conditions. "
	"There is NO\nwarranty; not even for MERCHANTABILITY or FITNESS FOR "
	"A PARTICULAR PURPOSE.\n\n";
    cerr << "Usage: "<< argv[0] << " input [y,n]*"<< endl;
    cerr << "  input  -  input image can be specified by" << endl;
    cerr << "            a) regular expression: <regexp>.tga, <regexp>.tif" << endl;
    cerr << "            b) file name in i3d format: <file_name>.i3d" << endl;
    cerr << "  [y,n]* -  sequence of test switchers divided by space" << endl;
    cerr << "            e.g. \"y n n\" will switch off (only) 2. and 3. test." << endl;
    cerr << "  test1  -  volume/area" << endl;
    cerr << "  test2  -  surface/perimeter" << endl;
    cerr << "  test3  -  roundness" << endl;
    cerr << "  test4  -  centroid" << endl;
    cerr << "  test5  -  radius" << endl;
    cerr << "  test6  -  eigenvectors, elongation" << endl;
    cerr << "  test7  -  symmetry" << endl;
    cerr << "  test8  -  rectangularity" << endl;
    cerr << "  test9  -  curvature measures" << endl;
    cerr << "  test10 -  low pass energy" << endl;
    cerr << "  test11 -  Fourier descriptors" << endl;
   exit(1);
  }

  try  {
    // zjisteni typu vstupniho souboru
    cerr << "Reading image Type" << endl;
    ImgVoxelType vt = ReadImageType(argv[1]);
    cerr << "Voxel=" << VoxelTypeToString(vt) << endl;

	//declarations for using Shape with labeled image version:
    LabeledImage3d<size_t, GRAY8> img;  
	Shape<size_t> shape;  
	//declarations for using Shape with binary image version
    Image3d<BINARY> imgB;	
	Shape<BINARY> shapeB;
	bool binVersion=true; // to know further which version to use
    switch (vt)          //voxel type testing
      {
      case BinaryVoxel:
        {
          cout << "Reading BINARY image..." << argv[1] << endl;
		  imgB.ReadImage(argv[1]);
          cout << "Image read. "  << endl;
		  //Image3d can be used with Shape
		  shapeB.SetImage(&imgB);
		  shapeB.SetLabel(BINARY(1)); //setting object voxels to be measured
		  break;
        }
      case Gray8Voxel:
        {
          cout << "Reading GRAY8 image..." << argv[1] << endl;
          Image3d<GRAY8> imgG(argv[1]);
          cout << "Labeling..." << endl;
          img.CreateRegionsFF(imgG);
		  // for easier component sellection:
		  cout << "Number of components: " << img.NumberOfComponents() << endl;
		  cout << "Number:  Size(pixel/voxel):" << endl;
		  shape.SetImage(&img);			  
		  for (size_t i=1; i <= img.NumberOfComponents(); i++){
			  shape.SetLabel(i);
			  cout << i << "              " << shape.Volume() << endl;
		  }
		  size_t component;
		  cout << "Enter number of component you are interrested in (1-" <<
            img.NumberOfComponents() << "):" << endl;
		  cin >> component;
		  //LabeledImage3d can be used with Shape as well 
		  shape.SetImage(&img);	
		  shape.SetLabel(component);    
		  binVersion=false;
          break;
        }
      default:
        cerr << "Other voxel types (than BINARY and GRAY8)are not supported in this test" << endl;
        exit(1);
      }

    float resX,resY,resZ;		  
    cout << endl << "Enter image resolution (X,Y,Z):" << endl;
    cin >> resX >> resY >> resZ;
    img.SetResolution(Resolution(resX,resY,resZ));
	imgB.SetResolution(Resolution(resX,resY,resZ));

	//further I'm working with 'shape' connected with LabeledImage3d 'img' or
	//with 'shapeB' connected with binary Image3d 'imgB'. Choice depends on previous imput. 
	int argp=2; // argument pointer (nastaven na treti argument)
    if ( argp >= argc || *argv[argp]=='y' || *argv[argp]=='Y') {
      cout << "**********"<< endl;
      cout << "* TEST 1 *"<< endl;
      cout << "**********"<< endl;
      cout << "Picture size: "<< img.GetImageSize() << endl;
      cout << "counting volume/area..." << endl;
	  size_t vol= binVersion?shapeB.Volume():shape.Volume(); 
	  float volUm= binVersion?shapeB.VolumeUm():shape.VolumeUm();
      cout << "volume/area in voxels: " << vol << endl;
      cout << "volume/area in microns: " << volUm << endl << endl;
    }
      argp++;

    if ( argp >= argc || *argv[argp]=='y' || *argv[argp]=='Y') {
      cout << "**********"<< endl;
      cout << "* TEST 2 *"<< endl;
      cout << "**********"<< endl;
      cout << "counting surface/perimeter..." << endl;
	  float surf=binVersion?shapeB.Surface():shape.Surface();
	  float surfUm=binVersion?shapeB.SurfaceUm():shape.SurfaceUm();
      cout << "surface/perimeter in voxels: " << surf << endl;
      cout << "surface/perimeter in microns: " << surfUm << endl<< endl;
    }
    argp++;

    if ( argp >= argc || *argv[argp]=='y' || *argv[argp]=='Y') {
      cout << "**********"<< endl;
      cout << "* TEST 3 *"<< endl;
      cout << "**********"<< endl;
      cout << "counting roundness..." << endl;
	  float round=binVersion?shapeB.Roundness():shape.Roundness();
      cout << "roundness: " << round << endl << endl;
    }
    argp++;

    if ( argp >= argc || *argv[argp]=='y' || *argv[argp]=='Y') {
      cout << "**********"<< endl;
      cout << "* TEST 4 *"<< endl;
      cout << "**********"<< endl;
      //testuj
      cout << "counting centroid..." << endl;
	  Vector3d<size_t> center = binVersion?shapeB.Centroid():shape.Centroid();
      cout << "centroid (x, y, z ):  " << endl;
      print<size_t>(center);
      cout << endl;
    }
    argp++;

    if ( argp >= argc || *argv[argp]=='y' || *argv[argp]=='Y') {
      cout << "**********"<< endl;
      cout << "* TEST 5 *"<< endl;
      cout << "**********"<< endl;
      //testuj
      cout << "counting radius..." << endl;
      float radMin, radMax, radMean;
	  binVersion?shapeB.Radius(radMin, radMax, radMean):shape.Radius(radMin, radMax, radMean);
      cout << "radius min:  " << radMin <<  endl;
      cout << "radius max:  " << radMax << endl;
      cout << "radius mean: " << radMean << endl << endl;
    }
    argp++;

if ( argp >= argc || *argv[argp]=='y' || *argv[argp]=='Y') {
      cout << "**********"<< endl;
      cout << "* TEST 6 *"<< endl;
      cout << "**********"<< endl;
      cout << "calculating eigenvectors..." << endl;
	  PrincipalAxes pas = binVersion?shapeB.GetPrincipalAxes():shape.GetPrincipalAxes();

      cout << "Eigen values: Eigen vectors (x,y,z):" << endl;
      cout << pas.val1 << ": " ;
      print<float>(pas.axe1);
      cout << pas.val2 << ": " ;
      print<float>(pas.axe2);
      cout << pas.val2 << ": " ;
      print<float>(pas.axe3);

	  bool is3D = binVersion? (imgB.GetNumSlices() > 1) : (img.GetNumSlices() > 1 );
	  if (is3D) {
          float e1, e2;
		  binVersion?shapeB.Elongation3d( e1, e2):shape.Elongation3d( e1, e2);
		  cout << "Major elongation: " << e1 << endl << "Minor elongation: " << e2 << endl << endl;
	  }else {
          float e1;
		  binVersion?shapeB.Elongation2d( e1 ):shape.Elongation2d( e1 );
          cout << "Elongation: " << e1 <<  endl << endl;
	  }
    }
    argp++;

if ( argp >= argc || *argv[argp]=='y' || *argv[argp]=='Y') {
      cout << "**********"<< endl;
      cout << "* TEST 7 *"<< endl;
      cout << "**********"<< endl;

      cout << "calculating principal symmetry..." << endl;
      float sym1, sym2, sym3;
	  binVersion?shapeB.PrincipalSymmetry(sym1 , sym2, sym3 ):shape.PrincipalSymmetry(sym1 , sym2, sym3 );


      cout << "principal symmetry with normal vector as: " << endl;
      cout << "axe1 : " << sym1 << endl;
      cout << "axe2 : " << sym2 << endl;
      cout << "axe3 : " << sym3 << endl << endl;
    }
    argp++;

    if ( argp >= argc || *argv[argp]=='y' || *argv[argp]=='Y') {
      cout << "**********"<< endl;
      cout << "* TEST 8 *"<< endl;
      cout << "**********"<< endl;
      cout << "calculating rectangularity..." << endl;
	  float rect=binVersion?shapeB.Rectangularity( ):shape.Rectangularity( );
      cout << "rectangularity : " << rect << endl << endl;
    }
    argp++;



    if ( argp >= argc || *argv[argp]=='y' || *argv[argp]=='Y') {
      cout << "**********"<< endl;
      cout << "* TEST 9 *"<< endl;
      cout << "**********"<< endl;

      if (img.GetNumSlices() > 1) {
        cout << "curvature and bending energy is not implemented for 3D images, "
	        "input one slice only." << endl;
      } else {
        cout << "counting curvature and bending energy: " << endl;
		double be = binVersion?shapeB.BendingEnergy2d():shape.BendingEnergy2d();
        cout << "Bending energy: " << be << endl;
      }
    }
    argp++;

  if ( argp >= argc || *argv[argp]=='y' || *argv[argp]=='Y') {
      cout << "***********"<< endl;
      cout << "* TEST 10 *"<< endl;
      cout << "***********"<< endl;
        unsigned int in;
        cout << " How many coeficients use for Low Pass Energy? : ";
        cin >> in;
        cout << "counting  LP Energy..." << endl;
        float lpe;
		lpe = binVersion?shapeB.LPEnergy2d(in):shape.LPEnergy2d(in);
        cout << "LP Energy: " << lpe << endl << endl;
    }
    argp++;

  if ( argp >= argc || *argv[argp]=='y' || *argv[argp]=='Y') {
      cout << "***********"<< endl;
      cout << "* TEST 11 *"<< endl;
      cout << "***********"<< endl;
      cout << "counting normalized Fourier descriptors..." << endl;
        float ff, NE;
		binVersion?shapeB.NFDs2d(ff, NE):shape.NFDs2d(ff, NE);
        cout << "Fourier descriptor FF: " << ff << endl;
        cout << "normalized energy: " << NE<< endl <<endl;
    }
    argp++;

   /*if ( argp >= argc || *argv[argp]=='y' || *argv[argp]=='Y') {
      cout << "***********"<< endl;
      cout << "* TEST 11 *"<< endl;
      cout << "***********"<< endl;

      cout << "counting statistical moments..." << endl;
      cout << "center - x, y, z  : " << shape.Centroid().x << ", "<< shape.Centroid().y
           << ", " << shape.Centroid().z <<endl;
      cout << "M 2,0,0 - M 1,1,0 - M: 1,0,1" << shape.StatMoment( 2,0,0) << ", "
           << shape.StatMoment( 1,1,0) << ", " << shape.StatMoment( 1,0,1)<<endl;
      cout << "M 1,1,0 - M 0,2,0 - M: 0,1,1" << shape.StatMoment( 1,1,0) << ", "
           << shape.StatMoment( 0,2,0) << ", " << shape.StatMoment( 0,1,1)<<endl;
      cout << "M 2,0,0 - M 1,1,0 - M: 1,0,1" << shape.StatMoment( 1,0,1) << ", "
           << shape.StatMoment( 0,1,1) << ", " << shape.StatMoment( 0,0,2)<<endl;


    }
      argp++;*/



  }
  catch (IOException& e) {
    cerr << e <<endl;
    exit(1);
  }
  catch (InternalException& e) {
    cerr << e <<endl;
    exit(1);
  }
  catch (bad_alloc&) {
    cerr << "Not enough memory." << endl;
    exit(1);
  }
  catch (...) {
    cerr << "shapetest: Unknown exception." << endl;
    exit(1);
  }

  return 0;
}
