/*
 * i3dlib - image manipulation library
 *
 * Copyright (C) 2000-2006   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 * FILE: gauss.cc
 *
 * an example of gaussian smoothing applied on an image
 *
 * Petr Matula, pem@fi.muni.cz, 2001
 * 
 */

#include <iostream>
#include "image3d.h"
#include "imgfiles.h"
#include "filters.h"
using namespace std;
using namespace i3d;

template <class Voxel> void DoGauss(const char *in_file,
                                    const char *out_file,
                                    double sigmax = 0.5,
                                    double sigmay = 0.5,
                                    double sigmaz = 0.5,
                                    double widthFactor = 2.0)
{
  cout << "Reading image..." << endl;
  Image3d<Voxel> i(in_file);
	
  cout << "Doing Gauss..." << endl;
  Gauss(i,sigmax,sigmay,sigmaz,widthFactor);
  
  cout << "Saving image..." << endl;
  i.SaveImage(out_file);
}

int main(int argc, char* argv[]) 
{
  if (argc < 3) {
   cerr << argv[0] << " ... simple image filtering: gaussian smoothing\n" 
	"Copyright (C) 2006 Centre for Biomedical Image Analysis \n"
	"This is free software; see the source for copying conditions. "
	"There is NO\nwarranty; not even for MERCHANTABILITY or FITNESS FOR "
	"A PARTICULAR PURPOSE.\n\n";
    cerr << "Usage: "<< argv[0] << " input output" << endl;
    cerr << "  input  - input image to load can be specified by" << endl;
    cerr << "           a) regular expression: <regexp>.tga" << endl;
    cerr << "           b) file name in i3d format: <file_name>.i3d" << endl;
    cerr << "  output - name of output file " << endl;
   exit(1);
  }
  
  try  {
    // zjisteni typu vstupniho souboru
    // !!! Nefunguje zjisteni souboru z regularniho vyrazu !!!
    cerr << "Reading image Type" << endl;
    ImgVoxelType vt = ReadImageType(argv[1]);
    cerr << "Voxel=" << VoxelTypeToString(vt) << endl;
    
    switch (vt)
      {
      case Gray16Voxel: 
        {
          DoGauss<GRAY16>(argv[1],argv[2], 0.3, 0.3, 0.3);
          break;
        }
      case RGBVoxel:
        {
          DoGauss<RGB>(argv[1],argv[2]);
          break;
        }
      default:
        cerr << "Other voxel types not supported" << endl;
      }


      // Test with a white 3D image:
/*
      cerr << "Testing a white 3D image:" << endl;

      Image3d<GRAY8> img;
      img.MakeRoom(100, 100, 100);
      GRAY8* data = img.GetFirstVoxelAddr();
      size_t imgsize = img.GetImageSize();

      for (size_t i = 0; i < imgsize; i++) data[i] = 255;

      cerr << "Sigma = 1.0, window size = 7:" << endl;

      Gauss(img, 1.0, 1.0, 1.0, 3.0);

      cerr << "Sigma = 1.0, window size = 11:" << endl;

      Gauss(img, 5.0, 5.0, 5.0, 1.0);
*/
  }
  catch (IOException& e) {
    cerr << e <<endl;
    exit(1);
  }
  catch (InternalException& e) {
    cerr << e <<endl;
    exit(1);
  }
  catch (bad_alloc&) {
    cerr << "Not enough memory." << endl;
    exit(1);
  }
  catch (...) {
    cerr << "Gauss: Unknown exception." << endl;
    exit(1);
  }
  
  return 0;
}
