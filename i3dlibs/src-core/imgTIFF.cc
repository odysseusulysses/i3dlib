/*
 * i3dlib - image manipulation library
 *
 * Copyright (C) 2000-2006   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 * FILE: imgTIFF.cc
 *
 * I/O routines for TIFF images
 *
 * David Svoboda <svoboda@fi.muni.cz> 2002
 *
 */

/* Suppression of Deprecated warning for Visual studio 8 */
#ifdef _MSC_VER
 #if _MSC_VER == 1400
  #define _CRT_SECURE_NO_DEPRECATE 1
 #endif
#endif

#include "i3d_config.h"

#ifdef WITH_TIFF

#include <string.h>
#include <iostream>
#include "toolbox.h"
#include "imgTIFF.h"
#include <tiffio.h>
#include <fstream>

#ifdef _MSC_VER
   #include <time.h>
#else
   #include <sys/time.h>
   #include <sys/times.h>
#endif

using namespace std;

namespace i3d {

  //---------------------------------------------------------------------------
  // New TIFF tags specification
  //---------------------------------------------------------------------------
  /* LIBTIFF manages own tags by TIFFFieldInfo structure. It contains following
   * elements:
   * - field_tag ... the tag number
   * - field_readcount ... how many values can be read from the tag, specially:
   *   TIFF_VARIABLE (-1) if the tag has variable number of values
   *   TIFFTAG_SPP (-2) if the tag has one value for each sample
   * - field_writecount ... how many values can be written to the tag
   * - field_type ... type of the tag values, it must be one of these:
   *   TIFF_BYTE, TIFF_ASCII, TIFF_SHORT, TIFF_LONG, TIFF_RATIONAL, TIFF_SBYTE,
   *   TIFF_UNDEFINED, TIFF_SSHORT, TIFF_SLONG, TIFF_SRATIONAL, TIFF_FLOAT, 
   *   TIFF_DOUBLE or TIFF_IFD
   * - field_bit ... special field number, new tags use FIELD_CUSTOM
   * - field_oktochange ... TRUE or FALSE, indicates if the tag value can be
   *   changed while image is being written
   * - field_passcount ... TRUE or FALSE, indicates if the count value must be
   *   passed in TIFFSetField() and TIFFGetField() functions
   * - field_name ... ASCII tag name */
   
  /* Definition of TIFFFieldInfo structures for new TIFF tags */
  static const TIFFFieldInfo xtiffFieldInfo[] = {
    { I3DTAG_ZPOSITION, 1, 1, TIFF_RATIONAL, FIELD_CUSTOM, 0, 0, "ZPosition" },
    { I3DTAG_ZRESOLUTION, 1, 1, TIFF_RATIONAL, FIELD_CUSTOM, 0, 0, "ZResolution" }
  };

  /**********************************************************************/
  /* Registration of new TIFF tags in libtiff                           */
  /**********************************************************************/
  /* Following TIFF tag registration mechanism was assumed from the
   * <a href="http://remotesensing.org/libtiff/addingtags.html">Defining New TIFF Tags</a>.
   * New TIFF tags can be defined outside the core TIFF specification. LIBTIFF
   * manage it by TIFFMergeFieldInfo() function. New tags need to be defined
   * before reading. For this reason it is necessary to register 
   * TIFFMergeFieldInfo() function as an extender callback with LIBTIFF. This is
   * done by TIFFSetTagExtender() function. */
   
  static TIFFExtendProc _ParentExtender = NULL;

  static void _XTIFFDefaultDirectory(TIFF *tif)
  {
    /* Definition of new TIFF tags */
    TIFFMergeFieldInfo(tif, xtiffFieldInfo, 2);

    /* If there is another extender */
    if (_ParentExtender) 
        (*_ParentExtender)(tif);
  }

  /* Initialization function */
  static void _XTIFFInitialize(void)
  {
    static int first_time=1;
	
    if (! first_time) return; /* Been there. Done that. */
    first_time = 0;
	
    /* Grab the inherited method and install */
    _ParentExtender = TIFFSetTagExtender(_XTIFFDefaultDirectory);
  }

  /**********************************************************************/
  /* End of TIFF tags registration                                      */
  /**********************************************************************/

  /**********************************************************************/
  /* PixelUnpacking: from one byte to the array "UnpackedOctet' */
  /**********************************************************************/
  void PixelUnpacking(unsigned char PackedByte, BINARY* Unpacked, 
                      unsigned count = CHAR_BIT)
  {
    //	unsigned char *Unpacked = (unsigned char *) UnpackedOctet;
    unsigned char bit = 1 << (CHAR_BIT - 1);

    for (unsigned i=0; i<count; i++)
      {
        Unpacked[i] = ((PackedByte & bit) != 0);
        bit >>= 1;
      }
  }

  /**********************************************************************/
  /* PixelPacking: of the array 'UnpackedOctet' to one byte */
  /**********************************************************************/
  void PixelPacking(const BINARY* Unpacked, unsigned char &PackedByte,
                    unsigned count = CHAR_BIT)
  {
    //	unsigned char *Unpacked = (unsigned char *) UnpackedOctet;
    unsigned char bit = 1 << (CHAR_BIT - 1);
    PackedByte = 0;

    for (unsigned i=0; i<count; i++)
      {
        PackedByte |= (Unpacked[i]) ? bit : 0;
        bit >>= 1;
      }	
  }
  
  //---------------------------------------------------------------------------
  // Constructors.
  //---------------------------------------------------------------------------
  /* Image reader constructor.
  */
  TIFFReader::TIFFReader(const char *fname, const VOI<PIXELS> *voi): ImageReader(fname, voi) 
  { 
    /* Call new TIFF tags registration */
    _XTIFFInitialize();

    if ((tif = TIFFOpen(fname,"r")) == NULL)
      throw IOException("TIFF: Unable to open "+ std::string(fname) + ".");
    if (voi) 
	 {
		  tifVOI = *voi;
	 } 
	 else 
	 {
		  tifVOI=VOI<PIXELS>(0,0,0,0,0,0);
	 }
  };

  /* Image writer constructor.
  */
  TIFFWriter::TIFFWriter(const char *fname): ImageWriter(fname) 
  {
    /* Call new TIFF tags registration */
    _XTIFFInitialize();

    if ((tif = TIFFOpen(fname,"w")) == NULL)
      throw IOException("TIFF: Unable to create "+ std::string(fname) + ".");
  };

  /**********************************************************************/
  /* LoadImageInfo */
  /**********************************************************************/
  void TIFFReader::LoadImageInfo()
  {
    uint16 orientation, bps, spp, photometric, planar, res_unit, res_arg;
    uint16 check_orientation, check_bps, check_spp, sample_format;
	 uint16 check_photometric, check_planar; 
    size_t check_header_size_x, check_header_size_y;
    float xres, yres, zres;
	 bool palette = false;

    /// Read necessary info from image file
    TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &header.size.x);
    TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &header.size.y);
    header.size.z = 1;

    TIFFGetFieldDefaulted(tif, TIFFTAG_XPOSITION, &header.offset.x);
    TIFFGetFieldDefaulted(tif, TIFFTAG_YPOSITION, &header.offset.y);
    TIFFGetFieldDefaulted(tif, I3DTAG_ZPOSITION, &header.offset.z);
	 TIFFGetFieldDefaulted(tif, TIFFTAG_SAMPLEFORMAT, &sample_format);

	 /* The resolution is mandatory tag in tiff structure. In spite of it
	  * there are software packages that does not follow this standard. */
    if (!TIFFGetField(tif, TIFFTAG_XRESOLUTION, &xres))
		  xres = 0.0f;

    if (!TIFFGetField(tif, TIFFTAG_YRESOLUTION, &yres))
		  yres = 0.0f;
		  
    if (!TIFFGetField(tif, I3DTAG_ZRESOLUTION, &zres))
		  zres = 0.0f;

	 // If the resolution unit is not set, do not use implicit value.
	 // We need micrometers but implicit resolution unit is inch.
    if (!TIFFGetField(tif, TIFFTAG_RESOLUTIONUNIT, &res_unit))
		  res_unit = 1;

	 TIFFGetFieldDefaulted(tif, TIFFTAG_SAMPLESPERPIXEL, &spp);
    TIFFGetFieldDefaulted(tif, TIFFTAG_BITSPERSAMPLE, &bps);
    TIFFGetFieldDefaulted(tif, TIFFTAG_PLANARCONFIG, &planar);
    TIFFGetFieldDefaulted(tif, TIFFTAG_ORIENTATION, &orientation);

    TIFFGetField(tif, TIFFTAG_PHOTOMETRIC, &photometric);

#ifdef I3D_DEBUG
	 std::cerr << "next image size: " << header.size << std::endl;
	 std::cerr << "x-res: " << xres << std::endl;
	 std::cerr << "y-res: " << yres << std::endl;
	 std::cerr << "z-res: " << zres << std::endl;
	 std::cerr << "units: " << res_unit << std::endl;
	 std::cerr << "orientation: " << orientation << std::endl;
	 std::cerr << "bps: " << bps << std::endl;
	 std::cerr << "photometric: " << photometric << std::endl;
	 std::cerr << "planar: " << planar << std::endl;
	 std::cerr << "spp: " << spp << std::endl;
	 std::cerr << "sample format: " << sample_format << std::endl;
#endif


    /// If there is more than one layer in TIFF file, read image header 
    /// information for each one
    while(TIFFReadDirectory(tif))
    {
      header.size.z += 1;
      
      /* Reading image header values from the following layers */
      TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &check_header_size_x);
      TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &check_header_size_y);
      TIFFGetField(tif, TIFFTAG_BITSPERSAMPLE, &check_bps);
      TIFFGetField(tif, TIFFTAG_PHOTOMETRIC, &check_photometric);
      TIFFGetField(tif, TIFFTAG_PLANARCONFIG, &check_planar);
      TIFFGetField(tif, TIFFTAG_SAMPLESPERPIXEL, &check_spp);
      TIFFGetFieldDefaulted(tif, TIFFTAG_ORIENTATION, &check_orientation);
		
#ifdef I3D_DEBUG
	 std::cerr << "-----------------" << std::endl;
	 std::cerr << "next image size: " << check_header_size_x << " x " 
				<< check_header_size_y << std::endl;
	 std::cerr << "next orientation: " << check_orientation << std::endl;
	 std::cerr << "next bps: " << check_bps << std::endl;
	 std::cerr << "next photometric: " << check_photometric << std::endl;
	 std::cerr << "next planar: " << check_planar << std::endl;
	 std::cerr << "next spp: " << check_spp << std::endl;
#endif

      /// Check consistency of image header values.
      /// Some must be equal in each image.
      if((check_header_size_x!=header.size.x) || 
         (check_header_size_y!=header.size.y) || 
			(check_bps!=bps) || 
         (check_photometric!=photometric) || 
			(check_planar!=planar) || 
         (check_spp!=spp) || 
			(check_orientation!=orientation))
        throw IOException("TIFF: 3D image doesn't have same header information in each layer.");
    }
	 // come back to the first directory (if there are more directories)
    TIFFSetDirectory(tif,0);

	 // Finish the image resolution evaluation
	 // if no resolution (x and y) is defined, do nothing
    if ((xres != 0.0) && (yres != 0.0))
	 {
		  // if the image is planar, x and y resolution is sufficient,
		  // if the image is spatial z-resolution must be defined as well
		  if ((header.size.z == 1) || ((header.size.z > 1) && (zres != 0.0)))
		  {
	        header.resolution = new Vector3d<float>;
      
	        /* Choose right conversion argument depending on resolution unit */
  	      switch(res_unit)
			   { 
  	        case RESUNIT_INCH:
  	          res_arg = 25400;
					break;
  	        case RESUNIT_CENTIMETER:
  	          res_arg = 10000;
					break;
  	        default:
  	          res_arg = 1; // no or implicit measure
  		     	}

      	  header.resolution->x = xres/res_arg; 
			  header.resolution->y = yres/res_arg;
	        header.resolution->z = (header.size.z == 1) ? 1 :  zres/res_arg;
		  }
    }

    /* If VOI is empty, it will be set to the size of the whole image */
    if (tifVOI.is_empty())
    {
      tifVOI.size.x = header.size.x;
      tifVOI.size.y = header.size.y;
      tifVOI.size.z = header.size.z; 
    }
      
    /* If colors are not saved in standard ordering scheme */
    if (planar != 1)
      throw IOException("TIFF: unsupported organization of colors.");

    /* Decide for correct orientation */
    switch (orientation)
    {
      case 1: 
        header.zero_at_the_top = true;
        header.zero_on_the_left = true;
        break;
      case 2: 
        header.zero_at_the_top = true; 
        header.zero_on_the_left = false;
        break;
      case 3: 
        header.zero_at_the_top = false; 
        header.zero_on_the_left = false; 
        break;
      case 4: 
        header.zero_at_the_top = false; 
        header.zero_on_the_left = true;  
        break;
      default:
        throw IOException("TIFFReader: unsupported orientation of image," \
                              "size.y and size.x are exchanged.");
    }
      
    switch (photometric)
      {
      case 0:
        header.color_inversion = true;
        break;
      case 1: // black is 0, white is 1 or 255
        break;
      case 2: // true color 
        break;
      case 3: // palette image
		  palette = true;
        break;
      default:
        throw IOException("I3D::TIFFReader: "\
								  "transparent images not supported.");
      };

	 /* Manipulation with sample format: fixed versus floating point */
	 switch (sample_format)
	 {
		  case 1: // standard unsigned fixed point data types
					 break;
		  case 3: // floating point numbers - IEEE standard
					 break;
		  default: 
				throw IOException("I3D::TIFFReader: unsupported sample format.");
	 }

    /* Manipulation with bits per pixel and samples per pixel */
    header.bpp = bps*spp;

	 // if the image contains color palette let it looks like a RGB16 image
	 if (palette)
	 {
      header.itype=RGB16Voxel;
	 }
	 else
	 {
	    /// Choose right color type from the bits per pixel value.
	    switch (header.bpp)
		 { 
			  case 1: header.itype = BinaryVoxel; break; 
			  case 8: header.itype = Gray8Voxel; break; 
			  case 16: header.itype = Gray16Voxel; break;
			  case 24: header.itype = RGBVoxel; break;
			  case 32: header.itype = FloatVoxel; break;
			  case 48: header.itype = RGB16Voxel; break;
			  default: throw IOException("I3D::TIFFReader: "\
												  "unsupported color type.");
		 }
	 }
  }

  /**************************************************************************/
  /* LoadData */
  /**************************************************************************/
  void TIFFReader::LoadBinData(BINARY *data)
  {
    /* First of all (before reading data from image file), we have to recognize
     * appropriate order of bits stored in the image file. */
    uint16 fillorder;

    if (TIFFGetField(tif, TIFFTAG_FILLORDER, &fillorder) && (fillorder == 2))
      throw IOException("I3D::TIFFReader: unsupported bits order.");
	
    /// Read the image data from file
    tsize_t bytes_per_row = TIFFScanlineSize(tif) / sizeof(BINARY);
    uint32 tif_line = 0;
    tdir_t tif_layer = 0;

    BINARY *origin = data;

    /* Reading data from bilevel images is different from grayscale and
     * fullcolor images. */
    tsize_t shift;
    /* Allocate temporary buffer aligned for the whole of bytes. */
    unsigned char *row = (unsigned char *)_TIFFmalloc(bytes_per_row);
    
    BINARY *unpacked_row = (BINARY *)_TIFFmalloc(tifVOI.size.x*sizeof(BINARY));
    /* Number of fully used bytes in buffer 'row' */
    tsize_t full_bytes_per_row = header.size.x / CHAR_BIT;
    /* Number of bits remaing in buffer 'row' */
    tsize_t remain_bits_in_row = header.size.x % CHAR_BIT;

    /* Read image data layer by layer */
    for(tif_layer=0; tif_layer<header.size.z; tif_layer++)
    {
      if(tif_layer >= tifVOI.offset.z && tif_layer < tifVOI.offset.z + tifVOI.size.z)
      {
        TIFFSetDirectory(tif,tif_layer);

        /* Read the image data line by line */
        for (tif_line=0; tif_line<header.size.y; tif_line++)
        {
          /* Check if the line is in the VOI */
          if (((int)tif_line >= tifVOI.offset.y) && 
				  ((int)tif_line < tifVOI.offset.y + (int)tifVOI.size.y))
          {
            /* Read image data to buffer 'row' */
            TIFFReadScanline(tif, row, tif_line);

            /// Unpack pixels from the bytes
            /* Note some bits in the last byte in buffer 'row' may be padding
             * we must not write them to memory - there is no place for them! */
            for (shift=0; shift<full_bytes_per_row; shift++)
              PixelUnpacking(row[shift], unpacked_row + CHAR_BIT*shift);

            PixelUnpacking(row[shift], 
									unpacked_row + CHAR_BIT*shift, 
									remain_bits_in_row);
           
            memcpy(data, 
						 &unpacked_row[tifVOI.offset.x], 
						 tifVOI.size.x*sizeof(BINARY));

            data += tifVOI.size.x;
          }
        }
      }
    }

    _TIFFfree(row);
    _TIFFfree(unpacked_row);

    data = origin;

    /// Swap gray gradient if it is necessary
    if (header.color_inversion){
      // swap for each layer
      for(size_t i=0; i<tifVOI.size.z; i++)
      {
        ReverseColors(tifVOI.size.x, tifVOI.size.y, data);
        data += tifVOI.size.x*tifVOI.size.y;
      }
      data = origin;
    }

    /// Rotate the image if the origin is different from I3D specification
    // origin in TIFF can be anywhere
    if (!header.zero_at_the_top){
      // change of origin for each layer
      for(size_t j=0; j<tifVOI.size.z; j++)
      {
        SwapVert(tifVOI.size.x, tifVOI.size.y, data);
        data += tifVOI.size.x*tifVOI.size.y;
      }
      data = origin;
    }

    if (!header.zero_on_the_left)
    {
      // change of origin for each layer
      for(size_t k=0; k<tifVOI.size.z; k++)
      {
        SwapHoriz(tifVOI.size.x, tifVOI.size.y, data);
        data += tifVOI.size.x*tifVOI.size.y;
      }
      data = origin;
    }
  }

  /**************************************************************************/
  template <class T> void TIFFReader::LoadData(T *data){
    /* First of all (before reading data from image file), we have to recognize
     * appropriate order of bits stored in the image file. */
    uint16 fillorder;

    if (TIFFGetField(tif, TIFFTAG_FILLORDER, &fillorder) && (fillorder == 2))
      throw IOException("I3D::TIFFReader: unsupported bits order.");

    /* Read the image data from file */
    uint32 tif_line = 0;
    tdir_t tif_layer = 0;
    T *line_buffer = (T *)_TIFFmalloc(TIFFScanlineSize(tif));

    T *origin = data;

    /* Read image data layer by layer */
    for(tif_layer=0; tif_layer<header.size.z; tif_layer++)
    {
      /* Check if the layer is in VOI */
      if ((tif_layer >= tifVOI.offset.z) && 
			 (tif_layer < tifVOI.offset.z + tifVOI.size.z))
      {
        TIFFSetDirectory(tif,tif_layer);

        /* If some compression is used then the Libtif doesnt support random
           access. It is necessary to read the image line by line even it
           is not in VOI */
        uint16 tiff_comp = 1;
        TIFFGetField(tif, TIFFTAG_COMPRESSION, &tiff_comp);
                  
        /// Read image data line by line
        for(tif_line=0; tif_line<header.size.y; tif_line++)
        {
          /* Check if the line is in the VOI */
          if (((int)tif_line >= tifVOI.offset.y) && 
				  ((int)tif_line < tifVOI.offset.y + (int)tifVOI.size.y))
          {
            TIFFReadScanline(tif, line_buffer, tif_line);

            /* Copy image data from buffer and choose only pixels 
				 * which are in VOI */
            memcpy(data, 
						 &line_buffer[tifVOI.offset.x], 
						 tifVOI.size.x*sizeof(T));

            data += tifVOI.size.x;
          }
          /* Due to some compression */
          else if (tiff_comp != 1)
          {
            TIFFReadScanline(tif, line_buffer, tif_line);
          }
        }
      }
    }

    data = origin;
    _TIFFfree(line_buffer);

    /// Swap gray gradient if it is necessary
    if (header.color_inversion){
      // swap for each layer
      for(size_t i=0; i<tifVOI.size.z; i++)
      {
        ReverseColors(tifVOI.size.x, tifVOI.size.y, data);
        data += tifVOI.size.x*tifVOI.size.y;
      }
      data = origin;
    }

    /// Rotate the image if the origin is different from I3D specification
    // origin in TIFF can be anywhere
    if (!header.zero_at_the_top){
      // change of origin for each layer
      for(size_t j=0; j<tifVOI.size.z; j++)
      {
        SwapVert(tifVOI.size.x, tifVOI.size.y, data);
        data += tifVOI.size.x*tifVOI.size.y;
      }
      data = origin;
    }

    if (!header.zero_on_the_left){
      // change of origin for each layer
      for(size_t k=0; k<tifVOI.size.z; k++)
      {
        SwapHoriz(tifVOI.size.x, tifVOI.size.y, data);
        data += tifVOI.size.x*tifVOI.size.y;
      }
      data = origin;
    }
  }
  
/**************************************************************************/
template <class T> void TIFFReader::LoadRGB16Data(T *data)
  {
    /* First of all (before reading data from image file), we have to recognize
     * appropriate order of bits stored in the image file. */
    uint16 fillorder;

    if (TIFFGetField(tif, TIFFTAG_FILLORDER, &fillorder) && (fillorder == 2))
      throw IOException("I3D::TIFFReader: unsupported bits order.");

    /* Read the data from file */
    uint32 tif_line = 0;
    tdir_t tif_layer = 0;
    T *line_buffer = (T *)_TIFFmalloc(TIFFScanlineSize(tif));
    T *origin = data;
        
    if ((header.itype == RGB16Voxel) && (header.bpp == 48)) // ordinary RGB16
	 { 
    
      /* Read image data layer by layer */
      for(tif_layer=0; tif_layer<header.size.z; tif_layer++)
      {
        /* Check if the layer is in VOI */
        if(tif_layer >= tifVOI.offset.z && tif_layer < tifVOI.offset.z + tifVOI.size.z)
        {
          TIFFSetDirectory(tif,tif_layer);
      
          /* If some compression is used then the Libtiff doesn't support random
             access. It is necessary to read the image line by line even it
             is not in VOI */
          uint16 tiff_comp = 1;
          TIFFGetField(tif, TIFFTAG_COMPRESSION, &tiff_comp);
          
          /* Read image data line by line */
          for(tif_line=0; tif_line<header.size.y; tif_line++)
          {
            /* Check if the line is in the VOI */
            if((int) tif_line >= tifVOI.offset.y && 
					(int) tif_line < tifVOI.offset.y + (int)tifVOI.size.y)
            {
              TIFFReadScanline(tif, line_buffer, tif_line);

              /* Copy image data from buffer and choose only pixels which are in VOI */
              memcpy(data, &line_buffer[tifVOI.offset.x], tifVOI.size.x*sizeof(T));
              data += tifVOI.size.x;
            }
            /* Due to some compression */
            else if(tiff_comp != 1)
            {
              TIFFReadScanline(tif, line_buffer, tif_line);
            }
          }
        }
      }
    
      data = origin;
      _TIFFfree(line_buffer);
    
    }
	 else // palette based RGB16
	 { 
      T sample_color;
      tsize_t bytes_per_row = TIFFScanlineSize(tif) / sizeof(BINARY);
      unsigned char *palette_indexes = 
				  (unsigned char *)_TIFFmalloc(bytes_per_row);
      uint16 *palette_red = 
				 (uint16 *)_TIFFmalloc((size_t)((1 << header.bpp)*sizeof(uint16)));
      uint16 *palette_green = 
				 (uint16 *)_TIFFmalloc((size_t)((1 << header.bpp)*sizeof(uint16)));
      uint16 *palette_blue = 
				 (uint16 *)_TIFFmalloc((size_t)((1 << header.bpp)*sizeof(uint16)));

      /* Read image data layer by layer */
      for(tif_layer=0; tif_layer<header.size.z; tif_layer++)
      {
        if ((tif_layer >= tifVOI.offset.z) && 
				(tif_layer < tifVOI.offset.z + tifVOI.size.z))
        {
          TIFFSetDirectory(tif,tif_layer);

          // load palette colors into palette_red, palette_green 
			 // and palette_blue arrays
          TIFFGetField(tif, TIFFTAG_COLORMAP, 
							  &palette_red, 
							  &palette_green, 
							  &palette_blue);
	
          /// Read image data line by line
          for(tif_line=0; tif_line<header.size.y; tif_line++)
          {
            if (((int) tif_line >= tifVOI.offset.y) && 
					 ((int) tif_line < tifVOI.offset.y + (int)tifVOI.size.y))
            {
              TIFFReadScanline(tif, palette_indexes, tif_line);

              /* Read image data pixel by pixel and get color values 
					* from palette */
              for (unsigned int k=0; k<header.size.x; k++)
              {
                if (((int)k >= tifVOI.offset.x) && 
						  ((int)k < tifVOI.offset.x + (int)tifVOI.size.x))
                {
                  sample_color.red = palette_red[palette_indexes[k]];
                  sample_color.green = palette_green[palette_indexes[k]];
                  sample_color.blue = palette_blue[palette_indexes[k]];
	  
                  *data = sample_color;
                  data += 1;
                }
              }
            }
          }
        }
      }
      data=origin;
      _TIFFfree(palette_indexes);
    }

    /// Swap gray gradient if it is necessary
    if (header.color_inversion){
      // swap for each layer
      for(size_t i=0; i<tifVOI.size.z; i++)
      {
        ReverseColors(tifVOI.size.x, tifVOI.size.y, data);
        data += tifVOI.size.x*tifVOI.size.y;
      }
      data = origin;
    }

    /// Rotate the image if the origin is different from I3D specification
    // origin in TIFF can be anywhere
    if (!header.zero_at_the_top){
      // change of origin for each layer
      for(size_t j=0; j<tifVOI.size.z; j++)
      {
        SwapVert(tifVOI.size.x, tifVOI.size.y, data);
        data += tifVOI.size.x*tifVOI.size.y;
      }
      data = origin;
    }

    if (!header.zero_on_the_left){
      // change of origin for each layer
      for(size_t k=0; k<tifVOI.size.z; k++)
      {
        SwapHoriz(tifVOI.size.x, tifVOI.size.y, data);
        data += tifVOI.size.x*tifVOI.size.y;
      }
      data = origin;
    }
  }

  /**************************************************************************/
  /* ~TIFFReader */
  /**************************************************************************/
  TIFFReader::~TIFFReader()
  {
    TIFFClose(tif);
  }

  /**************************************************************************/
  /* SaveImageInfo */
  /**************************************************************************/
  void TIFFWriter::SaveImageInfo()
  {
    uint16 tiff_comp = 5, // LZW
			  bps, spp, photometric, 
			  sample_format = 1; // fixed point data type
    uint32 rowsperstrip;

    /// Set the appropriate image header file values
    switch (header.itype)
      {
      case RGBVoxel:
        bps = 8; // bit per sample
        photometric = 2; 
        spp = 3; // samples per pixel
        break;
      case Gray8Voxel:
        bps = 8;
        spp = 1;
        photometric = PHOTOMETRIC_MINISBLACK;
        break;
      case BinaryVoxel:
        bps = 1;
        spp = 1;
        photometric = PHOTOMETRIC_MINISBLACK;
        tiff_comp = 4; // CCITT Group 4
        break;
      case RGB16Voxel:
        bps = 16; // bit per sample
        photometric = 2; 
        spp = 3; // samples per pixel
        break;
      case Gray16Voxel:
        bps = 16;
        spp = 1;
        photometric = PHOTOMETRIC_MINISBLACK;
        break;
		case FloatVoxel:
		  bps = 32;
		  spp = 1;
        photometric = PHOTOMETRIC_MINISBLACK;
		  sample_format = 3; // IEEE float
		  break;
      default:
        std::string msg;
        msg = std::string("TIFF::SaveImageInfo: Incompatible output file type");
        throw IOException(msg);
      }

    if (header.compression == false)
      tiff_comp = 1; // Without any compression

    TIFFSetField(tif, TIFFTAG_BITSPERSAMPLE, bps);
    TIFFSetField(tif, TIFFTAG_SAMPLESPERPIXEL, spp);
    TIFFSetField(tif, TIFFTAG_PHOTOMETRIC, photometric);
    TIFFSetField(tif, TIFFTAG_FILLORDER, FILLORDER_MSB2LSB);
	 TIFFSetField(tif, TIFFTAG_SAMPLEFORMAT, sample_format);

    TIFFSetField(tif, TIFFTAG_IMAGEWIDTH, header.size.x);
    TIFFSetField(tif, TIFFTAG_IMAGELENGTH, header.size.y);
    TIFFSetField(tif, TIFFTAG_PAGENUMBER, 
					  TIFFCurrentDirectory(tif)+1, header.size.z);

    TIFFSetField(tif, TIFFTAG_XPOSITION, header.offset.x);
    TIFFSetField(tif, TIFFTAG_YPOSITION, header.offset.y);

    /* If image is only planar, we dont save information related to Z axis */
    if (header.size.z > 1)
	 {
      TIFFSetField(tif, TIFFTAG_SUBFILETYPE, FILETYPE_PAGE);     
      TIFFSetField(tif, I3DTAG_ZPOSITION, header.offset.z);  
    }
	 else
	 {
      TIFFSetField(tif, TIFFTAG_SUBFILETYPE, 0);
    }

    /* Number '10000' is due to conversion between 
	  * micrometers and centimeters */
    if (header.resolution)
	 {
      TIFFSetField(tif, TIFFTAG_RESOLUTIONUNIT, RESUNIT_CENTIMETER);
      TIFFSetField(tif, TIFFTAG_XRESOLUTION, header.resolution->x*10000);
      TIFFSetField(tif, TIFFTAG_YRESOLUTION, header.resolution->y*10000);

      if (header.size.z > 1)      
        TIFFSetField(tif, I3DTAG_ZRESOLUTION, header.resolution->z*10000);
    }

    TIFFSetField(tif, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
    TIFFSetField(tif, TIFFTAG_ORIENTATION, ORIENTATION_TOPLEFT);
    TIFFSetField(tif, TIFFTAG_COMPRESSION, tiff_comp);
	
    rowsperstrip = TIFFDefaultStripSize(tif, 0);
    TIFFSetField(tif, TIFFTAG_ROWSPERSTRIP, rowsperstrip);

    /************************************************************************/
    /* DateTime */
    /************************************************************************/

    /*time_t my_time;
    struct tm *pmy_time;

    my_time=time(NULL);
    pmy_time=localtime(&my_time);
   
    string pre_time=asctime(pmy_time);
    string tag_date_time=pre_time.substr(4,3)+pre_time.substr(8,2)+pre_time.substr(19,5)+pre_time.substr(10,9);
    
    TIFFSetField(tif,TIFFTAG_DATETIME, tag_date_time.c_str());*/
  }

  /**************************************************************************/
  /* SaveData */
  /**************************************************************************/

  void TIFFWriter::SaveBinData(const BINARY *data)
  {
    /* Read the similar help text in 'LoadData' method */
    tsize_t bytes_per_row = TIFFScanlineSize(tif) / sizeof(BINARY);
    uint32 tif_line = 0;
    tdir_t tif_layer = 0;
    tsize_t full_bytes_per_row = header.size.x / CHAR_BIT;
    tsize_t remain_bits_in_row = header.size.x % CHAR_BIT;

    tsize_t shift;
    byte *row = (byte *)_TIFFmalloc(bytes_per_row);

    for (tif_layer=0; tif_layer<header.size.z; tif_layer++)
    {
      if(tif_layer > 0){
        /* Create new diretory and set its tags */
        TIFFWriteDirectory(tif);
        SaveImageInfo();
      }

      /// Pack pixels into the bytes
      for (tif_line=0; tif_line<header.size.y; tif_line++)
      {
        for (shift=0; shift<full_bytes_per_row; shift++)
          PixelPacking(data + CHAR_BIT*shift, row[shift]);

        if (remain_bits_in_row)
          PixelPacking(data + CHAR_BIT*shift, row[shift], remain_bits_in_row);

        /// Write image data
        TIFFWriteScanline(tif, row, tif_line);

        data += header.size.x; 
      }
    }

    _TIFFfree(row);
  }

  template <class T> void TIFFWriter::SaveData(const T *data)
  {
    /* Read the similar help text in 'LoadData' method */
    uint32 tif_line = 0;
    tdir_t tif_layer = 0;

    /* Write images layer by layer */
    for(tif_layer=0; tif_layer<header.size.z; tif_layer++)
    {
      if(tif_layer > 0)
      {
        /* Create new directory and set its tags */
        TIFFWriteDirectory(tif);
        SaveImageInfo();
      }

      /// Write image data line by line
      for(tif_line=0; tif_line<header.size.y; tif_line++)
      {
        TIFFWriteScanline(tif, const_cast<T*>(data), tif_line);
        data += header.size.x;
      }
    }
  }

  /**************************************************************************/
  /* ~TIFFWriter */
  /**************************************************************************/
  TIFFWriter::~TIFFWriter()
  {
    TIFFClose(tif);
  }
}

#endif

