/*
 * i3dlib - image manipulation library
 *
 * Copyright (C) 2000-2006   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** \file toolbox.cc
 \brief General purpose functions - implementation.

 \author Petr MejzlÌk (mejzlik@fi.muni.cz) 2000
 \author Petr Matula (pem@fi.muni.cz) 2001
 */
/*Suppression of Deprecated warning for Visual studio 8*/
#ifdef _MSC_VER
#if _MSC_VER == 1400
#define _CRT_SECURE_NO_DEPRECATE 1
#endif
#endif


#include <iostream>
#include <iomanip>
#include <string>
#include <stack>
#include <fstream>
#include <string.h>
#include <regex.h>

#if defined(__unix) || defined(__APPLE__)
#	include <unistd.h>
#	include <errno.h>
#  include <sys/sysinfo.h>
#elif defined(_WIN32)
#	include <time.h>
#	include <windows.h>
#	include <direct.h>
#	define getcwd _getcwd
#	define chdir _chdir
#endif

#ifdef __GNUG__
#	pragma implementation
#endif

#include "basic.h"
#include "toolbox.h"
#include <algorithm>

#ifdef HAVE_SSTREAM
#	include <sstream>
#else
#	include <strstream>
#endif

#ifdef _MSC_VER
	#include <windows.h>
#else
	#include <fstream>
	#include <string>
#endif

#if defined(__unix) || defined(__APPLE__)
#	include <dirent.h>
#elif defined(_WIN32) 
#	include "msdir.h"
#else
#	error "You must include header file with dirent function."
#endif


using namespace std;

namespace i3d {
  
	/***************************************************************************\
	*
	*                           String related functions
	*
	\***************************************************************************/
	//--------------------------------------------------------------------------
	// itos
	//--------------------------------------------------------------------------
	string itos(int n)
	{
#		ifdef HAVE_SSTREAM
			ostringstream ost;
			ost << n;
			return ost.str();
#		else
			char str[80];
			ostrstream ost(str, sizeof(str));
			ost << n << ends;
			return string(str);
#		endif
	} // itos

	//--------------------------------------------------------------------------
	// StrToInt
	//--------------------------------------------------------------------------
	bool StrToInt (const string & s, int &i)
	{
#		ifdef HAVE_SSTREAM
			istringstream myStream (s);
			if (myStream >> i) {
				return true;
			}
			else {
				return false;
			}
#		else
#			error "StrToInt function currently defined only using sstream"
#		endif
	} // StrToInt

	//--------------------------------------------------------------------------
	// StrToFloat
	//--------------------------------------------------------------------------
	bool StrToFloat (const string & s, float &f)
	{
#		ifdef HAVE_SSTREAM
			istringstream myStream (s);
			if (myStream >> f) {
				return true;
			} else {
				return false;
			}
#		else
#			error "StrToFloat function currently defined only using sstream"
#		endif
	} // StrToFloat

	//--------------------------------------------------------------------------
	// Tokenize
	//--------------------------------------------------------------------------
	void Tokenize(
		const string & str, 
		vector < string > &tokens, 
		const string & delimiters )
	{
		// Skip delimiters at beginning.
		string::size_type lastPos = str.find_first_not_of (delimiters, 0);

		// Find first "non-delimiter".
		string::size_type pos = str.find_first_of (delimiters, lastPos);

		while (string::npos != pos || string::npos != lastPos) {
			// Found a token, add it to the vector.
			tokens.push_back (str.substr (lastPos, pos - lastPos));

			// Skip delimiters.  Note the "not_of"
			lastPos = str.find_first_not_of (delimiters, pos);

			// Find next "non-delimiter"
			pos = str.find_first_of (delimiters, lastPos);
		}
	} // Tokenize

	//--------------------------------------------------------------------------
	// Trim
	//--------------------------------------------------------------------------
	string Trim(const string &s)
	{
		if (s.length() == 0)
			return s;

		size_t i1=0;
		size_t i2=s.length()-1;

#		ifdef _MSC_VER
#			define isspace_namespace(x) ::x
#		else
#			define isspace_namespace(x) x
#		endif

		// find left trim position
		while (isspace_namespace(isspace(s[i1])) && i1 < i2) {
			i1++;
		}

		// find right trim position
		while (isspace_namespace(isspace(s[i2])) && i2 >= i1) {
			i2--;
		}

		return s.substr(i1,i2-i1+1);
	} // Trim

	//--------------------------------------------------------------------------
	// AfterLast
	//--------------------------------------------------------------------------
	string AfterLast(const string &s, char sep)
	{
		size_t pos = s.rfind(sep);

		if (pos == string::npos) {
			return "";
		} 
		else {
			return s.substr(pos+1,s.length());
		}
	} // AfterLast

	//--------------------------------------------------------------------------
	// BeforeLast
	//--------------------------------------------------------------------------
	string BeforeLast(const string &s, char sep)
	{
		size_t pos = s.rfind(sep);

		if (pos == string::npos) {
			return "";
		} 
		else {
			return s.substr(0,pos);
		}
	} // BeforeLast

	//--------------------------------------------------------------------------
	// cmp_nocase
	//--------------------------------------------------------------------------
	int cmp_nocase(const string &s1, const string &s2)
	{
		string::const_iterator p1=s1.begin();
		string::const_iterator p2=s2.begin();

		while (p1 != s1.end() && p2 != s2.end()) {
			if (toupper(*p1) != toupper(*p2)) {
				return (toupper(*p1) < toupper(*p2)) ? -1 : 1;
			}
			++p1;
			++p2;
		}
		return s2.size() - s1.size();
	} // cmp_nocase

	//--------------------------------------------------------------------------
	// num_digits
	//--------------------------------------------------------------------------
	int num_digits(int number,int base)
	{
		if (number == 0) {
			return 1;
		}

		int digits_count = 0;
		while (number > 0) {
			number /= base;
			++digits_count;
		}

		return digits_count;
	} // num_digits

	//--------------------------------------------------------------------------
	// print human readable number
	//--------------------------------------------------------------------------
	std::string human_readable (double number, unsigned precision)
	{
	 std::stack<double> S;

	 double fixed_part = floor(number);
	 double decimal_part = number - fixed_part;

	 number = fixed_part;

	 while (fabs(number) >= 1)
	 {
		  double quotient = floor(number / 1000);
		  double rem = number - quotient * 1000;
		  S.push(rem);

		  number = quotient;
	 }

	 std::ostringstream os;
	 unsigned counter = 0;

	 while (!S.empty())
	 {
		  double num = S.top();
		  S.pop();

		  if (counter > 0)
				os << setfill('0') << setw(3) << num;
		  else
				os << num;

		  if (!S.empty())
				os << ",";

		  counter++;
	 }

	 if (precision != 0)
	 {
		  os.setf(ios::fixed);
		  os.precision(0);
		  os << "." << floor(decimal_part * pow(10.0,double(precision)));
	 }

	 return os.str();
	}

	//--------------------------------------------------------------------------
	// print human readable number
	//--------------------------------------------------------------------------
	std::string human_readable (unsigned long long number)
	{
		 return human_readable(double(number), 0);
	}

	/**************************************************************************\
	*
	*                   CPU management
	*
	\**************************************************************************/
	//--------------------------------------------------------------------------
	// The function 'GetNumberOfProcessors' gives the number of 
	// all the available 
	// processors in the current PC. The function should be running correctly
	// on Win as well as on Linux systems.
	//--------------------------------------------------------------------------
	size_t GetNumberOfProcessors()
	{
#ifdef _MSC_VER
		 SYSTEM_INFO stat;
		 GetSystemInfo(&stat);
		 return stat.dwNumberOfProcessors;
#else
		 ifstream file;
		 string buffer;

		 file.open("/proc/cpuinfo", ifstream::in);

		 if (!file)
			  return 1; // default number of CPUs

		 size_t counter = 0;
		 while (!file.eof())
		 {
			  getline(file, buffer);
			  if (buffer.find("processor") != string::npos)
					counter++;
		 }
		 file.close();

		 return counter;
#endif
	}

	/**************************************************************************\
	*
	*                   Memory management
	*
	\**************************************************************************/
	//--------------------------------------------------------------------------
	// GetFreeMemory
	//--------------------------------------------------------------------------
	unsigned long long GetFreeMemory()
	{
#ifdef _MSC_VER // for windows
	#ifdef _DEBUG
		std::cout << 
		"GetFreeMemory: This code is really VERY slow if compiled as DEBUG.\n";
		std::cout << "\t As for RELEASE version, it is fine.\n";		
	#endif
		
		 MEMORYSTATUSEX stat;
		 stat.dwLength = sizeof (stat);
		 GlobalMemoryStatusEx (&stat);
		 unsigned long long free_mem = stat.ullAvailPhys;

		 // Note:
		 // 'stat.ullAvailPhys' gives free memory of the whole operating system.
		 // As we need free allocable memory, which is usually smaller, we must
		 // find this number experimentally (binary search).
		 unsigned long long left = 0, right = free_mem, middle;

		 while (left <= right)
		 {
			 middle = (left + right) / 2;
			 void *block = malloc(middle);

			 if (block == NULL)
			 {
				 right = middle-1;			 
			 }
			 else
			 {
				 left = middle + 1;
				 free(block);
			 }
		 }

		 free_mem = right;
		 
#else // for linux
		 unsigned long long free_mem, buffers, cached;
		 FILE *fp;
		 char buf[256]; 

		 // NOTE:
		 // We cannot uf sysinfo() function as it does not take care
		 // of 'buffered memory' and 'cached memory'. These two variables
		 // sometimes declare that 90% of RAM is cached and buffered. 
		 // Cached and buffered memory can be simply freed i.e. is not
		 // concerned to be occupied. When asking for free (available) memory,
		 // we should and 'MemFree", "Buffers' and 'Cached'.
		 
		 if (!(fp = fopen("/proc/meminfo", "r"))) 
		 {
			  return 0;
		 } 
		 
		 while (!feof(fp)) 
		 { 
			  if (fgets(buf, 255, fp) != NULL) 
			  { 
					if (strncmp(buf, "MemFree:", 8) == 0) 
					{
						 sscanf(buf, "%*s %llu", &free_mem); 
					} 
					else if (strncmp(buf, "Buffers:", 8) == 0) 
					{
						 sscanf(buf, "%*s %llu", &buffers); 
					} 
					else if (strncmp(buf, "Cached:", 7) == 0) 
					{
						 sscanf(buf, "%*s %llu", &cached);
					
					}
			  }
		 }

		 fclose(fp);

		 free_mem += buffers + cached;

		 // free_mem is given in KB -> must be multiplied by 1024
		 free_mem *= 1024;

#endif

#ifdef I3D_DEBUG
		 cout << "GetFreeMemory: Free memory: " << 
					human_readable(free_mem) << endl;
#endif

		 return free_mem;
	}

	//--------------------------------------------------------------------------
	// GetTotalMemory
	//--------------------------------------------------------------------------
	unsigned long long GetTotalMemory()
	{
		 unsigned long long total_mem;
#ifdef _MSC_VER // for windows 
		 MEMORYSTATUSEX stat;
		 stat.dwLength = sizeof (stat);
		 GlobalMemoryStatusEx (&stat);
		 total_mem = stat.ullTotalPhys;
#else // for linux 
		 struct sysinfo meminfo;
		 sysinfo(&meminfo);
		 total_mem = meminfo.totalram;
#endif

#ifdef I3D_DEBUG
		 cout << "GetTotalMemory: Total memory: " << human_readable(total_mem) << endl;
#endif

		 return total_mem;
	}

	/**************************************************************************\
	*
	*                   File and directory related functions
	*
	\**************************************************************************/
	//--------------------------------------------------------------------------
	// GetExtension
	//--------------------------------------------------------------------------
	void GetExtension(const string &fn, string &ext)
	{
        size_t len = fn.size(), i = len;
        char c = 0;

        while (i-- > 0)
        {
            c = fn[i];
            if (c == '\\' || c == '/' || c == '.')
                break;
        }

        ext = (c == '.') ? ext = fn.substr (i + 1, len - i - 1) : "";
	} // GetExtension

	//--------------------------------------------------------------------------
	// GetPath
	//--------------------------------------------------------------------------
	void GetPath(const string &fn, string &path)
	{
        size_t len = fn.size();

        while (len-- > 0)
        {
            char c = fn[len];
            if (c == '\\' || c == '/')
            {
                path = fn.substr (0, len + 1);
                return;
            }
        }

        path.clear();
	} // GetPath

	//--------------------------------------------------------------------------
	// SplitFilename
	//--------------------------------------------------------------------------
	void SplitFilename(const string &fn, string &path, string &name, string &ext)
	{
		GetPath (fn, path);
		GetExtension (fn, ext);

        size_t name_len = fn.length() - path.length();
        if (!ext.empty())
            name_len -= ext.length() + 1;

        name = name_len ? fn.substr (path.length(), name_len) : "";
	}

	//--------------------------------------------------------------------------
	// GetDir
	//--------------------------------------------------------------------------
	string GetDir()
	{
		static const size_t maxPathLen = 1024;

		char buf[maxPathLen];
		if (getcwd(buf, maxPathLen) == NULL)
			throw IOException("GetDir: " + string(strerror(errno)));
		else
			return string(buf);
	}

	//--------------------------------------------------------------------------
	// SetDir
	//--------------------------------------------------------------------------
	void SetDir(const char* dir)
	{
		if (chdir(dir) != 0)
			throw IOException("SetDir: " + string(strerror(errno)));
	}

	//--------------------------------------------------------------------------
	// ScanDir
	//--------------------------------------------------------------------------
	/** Scanning of a directory. Scan directory \p dir for files matching regular 
	expression \p fnames. Filenames are returned in vector \p namelist and can be unsorted.
	Type of regular experssion is specified by \p regflags	(see regcomp(3) for more info).
	\returns Number of matching files on success. 
	\throws IOException if regular expression cannot be parsed or directory scanned. */
	int ScanDir(const char *dir, const char *fnames, FileList &namelist,
		int regflags = REG_EXTENDED | REG_NOSUB)
	{
		string path = dir;
		if (path == string("")) {
			path = ".";
		}

		regex_t precomp; // precompiled regular expression
		int errcode;
		/* Parse the regular expression from fnames: */
		if ((errcode = regcomp(&precomp, fnames, regflags)) != 0) {
			const int ERRLEN =1024;
			char error[ERRLEN];
			regerror(errcode, &precomp, error, ERRLEN);
			throw IOException("ScanDir: " \
				"Error in parsing a regular expression. " \
				+ string(error) + '.');
		}

		DIR *d = opendir(path.c_str());

		if (d == NULL) {
			throw IOException(string("ScanDir: Can't open directory ") + path + " "
			+ string(strerror(errno)));
		}

		struct dirent *entry;
		while ((entry = readdir(d)) != NULL) {
			if (regexec(&precomp,entry->d_name,0,0,0) == 0) {
				namelist.push_back(entry->d_name);
			}
		}

		closedir(d);

		regfree(&precomp); // free the memory allocated by regcomp
		return namelist.size();
	}

	//--------------------------------------------------------------------------
	// MaskExpand
	//--------------------------------------------------------------------------
	int MaskExpand(const char *mask, FileList &namelist, bool expand)
	{
		/* if the regular expression support is not enabled (expand == false)
		 * then using regular expression expansion is not possible */

		namelist.clear();

		if (expand)	{
			// check, how many files are listed
			std::string path, ext, name;
			SplitFilename(mask, path, name, ext);

			// set up the list of all the requested files
			ScanDir(path.c_str(), ext != "" ? (string(name)+"."+ext).c_str() : name.c_str(), namelist);

			sort (namelist.begin (), namelist.end ());

			for (size_t i=0; i<namelist.size(); i++) {
				namelist[i] = path + namelist[i];
			}
		}	

		/* if nothing was found or no expansion was allowed, copy the
		original name (mask) to the output */
		if (namelist.empty()) {
			namelist.push_back(mask);
		}

		return namelist.size();
	}


	/**************************************************************************\
	*
	*                           Stream functions
	*
	\**************************************************************************/
	//--------------------------------------------------------------------------
	// SetBadIOException
	//--------------------------------------------------------------------------
	void SetBadIOException(ios& io)
	{
#		ifdef HAVE_SSTREAM 
			io.exceptions(ios::badbit|ios::failbit|ios::eofbit);
#		else
#			error "SetBadIOException function currently defined only using sstream"
#		endif
	} // SetBadIOException

	//--------------------------------------------------------------------------
	// LSBFirstReadLong
	//--------------------------------------------------------------------------
	unsigned long LSBFirstReadLong(istream& file)
	{
		static byte buf[4];
		static unsigned long value;
		file.read((char *) &buf, 4);
		value = (unsigned long) (buf[3] << 24);
		value |= (unsigned long) (buf[2] << 16);
		value |= (unsigned long) (buf[1] << 8);
		value |= (unsigned long) (buf[0]);
		return value;
	}

	//--------------------------------------------------------------------------
	// LSBFirstWriteLong
	//--------------------------------------------------------------------------
	void LSBFirstWriteLong(ostream& file, unsigned long value)
	{
		static byte buf[4];
		buf[0] = (byte) value;
		buf[1] = (byte) (value >> 8);
		buf[2] = (byte) (value >> 16);
		buf[3] = (byte) (value >> 24);
		file.write((char *) &buf, 4);
	}

	//--------------------------------------------------------------------------
	// LSBFirstReadShort
	//--------------------------------------------------------------------------
	unsigned short LSBFirstReadShort(istream& file)
	{
		static byte buf[2];
		static unsigned short value;
		file.read((char *) &buf, 2);
		value = (unsigned short) (buf[1] << 8);
		value |= (unsigned short) (buf[0]);
		return value;
	}

	//--------------------------------------------------------------------------
	// LSBFirstWriteShort
	//--------------------------------------------------------------------------
	void LSBFirstWriteShort(ostream& file, unsigned short value)
	{
		static byte buf[2];
		buf[0] = (byte) value; // lower byte
		buf[1] = (byte) (value >> 8); // upper byte
		file.write((char *) &buf, 2);
	}


	/**************************************************************************\
	*
	*                            Conversion functions
	*
	\**************************************************************************/
	//--------------------------------------------------------------------------
	// LSBFirstToLong
	//--------------------------------------------------------------------------
	unsigned long LSBFirstToLong(byte buf[4])
	{
		static unsigned long value;
		value = (unsigned long) (buf[3] << 24);
		value |= (unsigned long) (buf[2] << 16);
		value |= (unsigned long) (buf[1] << 8);
		value |= (unsigned long) (buf[0]);
		return value;
	}

	//--------------------------------------------------------------------------
	// LongToLSBFirst
	//--------------------------------------------------------------------------
	void LongToLSBFirst(byte buf[4], unsigned long value)
	{
		buf[0] = (byte) value;
		buf[1] = (byte) (value >> 8);
		buf[2] = (byte) (value >> 16);
		buf[3] = (byte) (value >> 24);
	}

	//--------------------------------------------------------------------------
	// LSBFirstToShort
	//--------------------------------------------------------------------------
	unsigned short LSBFirstToShort(byte buf[2])
	{
		static unsigned short value;
		value = (unsigned short) (buf[1] << 8);
		value |= (unsigned short) (buf[0]);
		return value;
	}

	//--------------------------------------------------------------------------
	// ShortToLSBFirst
	//--------------------------------------------------------------------------
	void ShortToLSBFirst(byte buf[2], unsigned short value)
	{
		buf[0] = (byte) value; // lower byte
		buf[1] = (byte) (value >> 8); // upper byte
	}

	//--------------------------------------------------------------------------
	// SwapVert
	//--------------------------------------------------------------------------
	template < class T > void
	SwapVert (size_t width, size_t height, T * data)
	{
	  for (size_t i = 0; i < height / 2; i++)    // lines
	    for (size_t j = 0; j < width; j++)       // columns
	      swap (data[i * width + j], data[(height - i - 1) * width + j]);
	}

	template CORE_I3D_DLLEXPORT void
	SwapVert (size_t, size_t, BINARY *);
	template CORE_I3D_DLLEXPORT void
	SwapVert (size_t, size_t, GRAY8 *);
	template CORE_I3D_DLLEXPORT void
	SwapVert (size_t, size_t, RGB *);
	template CORE_I3D_DLLEXPORT void
	SwapVert (size_t, size_t, GRAY16 *);
	template CORE_I3D_DLLEXPORT void
	SwapVert (size_t, size_t, RGB16 *);
	template CORE_I3D_DLLEXPORT void
	SwapVert (size_t, size_t, float *);
	template CORE_I3D_DLLEXPORT void
	SwapVert (size_t, size_t, double *);
	template CORE_I3D_DLLEXPORT void
	SwapVert (size_t, size_t, size_t *);
	template CORE_I3D_DLLEXPORT void
	SwapVert (size_t, size_t, int *);
	template CORE_I3D_DLLEXPORT void
	SwapVert (size_t, size_t, Vector3d<double> *);
	template CORE_I3D_DLLEXPORT void
	SwapVert (size_t, size_t, Vector3d<float> *);
	template CORE_I3D_DLLEXPORT void
	SwapVert (size_t, size_t, std::complex<float> *);
	template CORE_I3D_DLLEXPORT void
	SwapVert (size_t, size_t, std::complex<double> *);

	//--------------------------------------------------------------------------
	// SwapHoriz
	//--------------------------------------------------------------------------
	template < class T > void
	SwapHoriz (size_t width, size_t height, T * data)
	{
	  /*
	   * The following was originally here. Vlado considered it slow and thus changed it.
	   * If you come across an error, revert the change back. Thanks and sorry for inconvenience.
	  for (size_t j = 0; j < width / 2; j++)     // columns
	    for (size_t i = 0; i < height; i++)      // lines
	   */
	  for (size_t i = 0; i < height; i++)		// lines
	    for (size_t j = 0; j < width / 2; j++)	// columns
	      swap (data[i * width + j], data[i * width + (width - j - 1)]);
	}

	template CORE_I3D_DLLEXPORT void
	SwapHoriz (size_t, size_t, BINARY *);
	template CORE_I3D_DLLEXPORT void
	SwapHoriz (size_t, size_t, GRAY8 *);
	template CORE_I3D_DLLEXPORT void
	SwapHoriz (size_t, size_t, RGB *);
	template CORE_I3D_DLLEXPORT void
	SwapHoriz (size_t, size_t, GRAY16 *);
	template CORE_I3D_DLLEXPORT void
	SwapHoriz (size_t, size_t, RGB16 *);
	template CORE_I3D_DLLEXPORT void
	SwapHoriz (size_t, size_t, float *);
	template CORE_I3D_DLLEXPORT void
	SwapHoriz (size_t, size_t, double *);
	template CORE_I3D_DLLEXPORT void
	SwapHoriz (size_t, size_t, size_t *);
	template CORE_I3D_DLLEXPORT void
	SwapHoriz (size_t, size_t, int *);
	template CORE_I3D_DLLEXPORT void
	SwapHoriz (size_t, size_t, Vector3d<double> *);
	template CORE_I3D_DLLEXPORT void
	SwapHoriz (size_t, size_t, Vector3d<float> *);
	template CORE_I3D_DLLEXPORT void
	SwapHoriz (size_t, size_t, std::complex<double> *);
	template CORE_I3D_DLLEXPORT void
	SwapHoriz (size_t, size_t, std::complex<float> *);

	//--------------------------------------------------------------------------
	// ReverseColors 
	//--------------------------------------------------------------------------
	template < class T > void
	ReverseColors (size_t width, size_t height, T * data)
	{
	  size_t len = width * height;
	  for (size_t i = 0; i < len; i++)
	    data[i] = ColorInversion (data[i]);
	}

	template void CORE_I3D_DLLEXPORT
	ReverseColors (size_t, size_t, BINARY *);
	template void CORE_I3D_DLLEXPORT
	ReverseColors (size_t, size_t, GRAY8 *);
	template void CORE_I3D_DLLEXPORT
	ReverseColors (size_t, size_t, RGB *);
	template void CORE_I3D_DLLEXPORT
	ReverseColors (size_t, size_t, GRAY16 *);
	template void CORE_I3D_DLLEXPORT
	ReverseColors (size_t, size_t, RGB16 *);
	template void CORE_I3D_DLLEXPORT
	ReverseColors (size_t, size_t, float *);

} // i3d namespace

