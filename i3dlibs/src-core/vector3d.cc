/*
 * i3dlib - image manipulation library
 *
 * Copyright (C) 2000-2006   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 * FILE: vector3d.C
 *
 * 3D vector representation
 *
 * Petr Matula (pem@fi.muni.cz), Petr Mejzl�k (mejzlik@fi.muni.cz) 2001
 */

#ifdef __GNUG__
#pragma implementation
#endif

#include "vector3d.h"
#include "basic.h"
#include "voi.h"
#include <algorithm>
#include <string>
#include <sstream>

namespace i3d {
  using std::ostream;
  using std::istream;


  
template <class T> Vector3d<T> &Vector3d<T>::operator+=(const Vector3d<T>& v)
{
  x+=v.x;
  y+=v.y;
  z+=v.z;
  return *this;
}

template <class T> Vector3d<T> &Vector3d<T>::operator-=(const Vector3d<T>& v)
{
  x-=v.x;
  y-=v.y;
  z-=v.z;
  return *this;
}

template <class T> Vector3d<T> &Vector3d<T>::operator*=(const Vector3d<T>& v)
{
  x*=v.x;
  y*=v.y;
  z*=v.z;
  return *this;
}

template <class T> Vector3d<T> &Vector3d<T>::operator/=(const Vector3d<T>& v)
{
  x/=v.x;
  y/=v.y;
  z/=v.z;
  return *this;
}

template<class T> ostream& operator<<(ostream& os, const Vector3d<T> &v)
{
  os << "(" << v.x << "," << v.y << "," << v.z << ")";
  return os;
};

template<class T> istream& operator>>(istream& is, Vector3d<T> &v)
{
    char c, del[4] = {'(', ',', ',', ')'};
    std::string buf;

    for (int i = 0; i < 4; i++)
    {
        // Read characters from stream until next delimeter is reached and save them to buffer
        while (is.good())
        {
            is.get (c);
            if (c == del[i])
                break;

            // The first buffer must contain only spaces the following must not
            if ((!i && c != ' ') || (i > 0 && c == ' '))
            {
                is.clear (std::ios::ios_base::failbit);
                return is;
            }
            buf += c;
        }

        // Convert buffer to value
        if (i > 0)
        {
            std::istringstream val (buf);
            switch (i)
            {
            case 1: val >> v.x; break;
            case 2: val >> v.y; break;
            case 3: val >> v.z; break;
            }

            if (val.bad() || val.fail() || !val.eof())
            {
                is.clear (std::ios::ios_base::failbit);
                return is;
            }
        }

        buf.clear();
    }

    return is;
}

//specializations for Vector3d< std::valarray<float>* > where some operations are not defined
//on pointers (pointers are the elements of given Vector3d):
template <> Vector3d< std::valarray<float>* > &Vector3d< std::valarray<float>* >::operator+=(const Vector3d< std::valarray<float>* >& v)
{
	throw InternalException("operator + on Vector3d< valarray<float>* > is not defined and thus does NOTHING!\n");
	return(*this);
}

template <> Vector3d< std::valarray<float>* > &Vector3d< std::valarray<float>* >::operator-=(const Vector3d< std::valarray<float>* >& v)
{
	throw InternalException("operator - on Vector3d< valarray<float>* > is not defined and thus does NOTHING!\n");
	return(*this);
}

template <> Vector3d< std::valarray<float>* > &Vector3d< std::valarray<float>* >::operator*=(const Vector3d< std::valarray<float>* >& v)
{
	throw InternalException("operator * on Vector3d< valarray<float>* > is not defined and thus does NOTHING!\n");
	return(*this);
}

template <> Vector3d< std::valarray<float>* > &Vector3d< std::valarray<float>* >::operator/=(const Vector3d< std::valarray<float>* >& v)
{
	throw InternalException("operator / on Vector3d< valarray<float>* > is not defined and thus does NOTHING!\n");
	return(*this);
}

typedef std::valarray<float>* PointerVector3d;
template <> Vector3d< PointerVector3d > &Vector3d< PointerVector3d >::operator*=(const PointerVector3d & s)
{
	throw InternalException("operator * on Vector3d< valarray<float>* > is not defined and thus does NOTHING!\n");
	return(*this);
}

template <> Vector3d< PointerVector3d > &Vector3d< PointerVector3d >::operator/=(const PointerVector3d & s)
{
	throw InternalException("operator / on Vector3d< valarray<float>* > is not defined and thus does NOTHING!\n");
	return(*this);
}
template <> Vector3d< PointerVector3d > Vector3d< PointerVector3d >::operator-() const
{
	throw InternalException("operator / on Vector3d< valarray<float>* > is not defined and thus does NOTHING!\n");
	return(*this);
}

/** Explicit instantiations: */
template struct CORE_I3D_DLLEXPORT Vector3d<int>;
template struct CORE_I3D_DLLEXPORT Vector3d<size_t>;
template struct CORE_I3D_DLLEXPORT Vector3d<long>;
template struct CORE_I3D_DLLEXPORT Vector3d<float>;
template struct CORE_I3D_DLLEXPORT Vector3d< std::valarray<float>* >;
template struct CORE_I3D_DLLEXPORT Vector3d<double>;

template CORE_I3D_DLLEXPORT ostream& operator<<(ostream&, const Vector3d<int> &);
template CORE_I3D_DLLEXPORT ostream& operator<<(ostream&, const Vector3d<size_t> &);
template CORE_I3D_DLLEXPORT ostream& operator<<(ostream&, const Vector3d<long> &);
template CORE_I3D_DLLEXPORT ostream& operator<<(ostream&, const Vector3d<float> &);
//template CORE_I3D_DLLEXPORT ostream& operator<<(ostream&, const Vector3d< std::valarray<float>* > &);
template CORE_I3D_DLLEXPORT ostream& operator<<(ostream&, const Vector3d<double> &);

template CORE_I3D_DLLEXPORT istream& operator>>(istream&, Vector3d<int> &);
template CORE_I3D_DLLEXPORT istream& operator>>(istream&, Vector3d<size_t> &);
template CORE_I3D_DLLEXPORT istream& operator>>(istream&, Vector3d<long> &);
template CORE_I3D_DLLEXPORT istream& operator>>(istream&, Vector3d<float> &);
//template CORE_I3D_DLLEXPORT istream& operator>>(istream&, Vector3d< std::valarray<float>* > &);
template CORE_I3D_DLLEXPORT istream& operator>>(istream&, Vector3d<double> &);

// explicit instantiation of VOI, perhaps there's a better place to do this
// expl. instantiation was required for MS VS 2005 
// (despite the whole code for VOI is available via header its file...)
template struct CORE_I3D_DLLEXPORT VOI<PIXELS>;
template struct CORE_I3D_DLLEXPORT VOI<MICRONS>;
}
