#include "imgfiles.h"
#include "toolbox.h"
#include "bioformatsreader.h"
#include <java_magic.h>
#include "string.h"

using namespace std;
using namespace loci::formats;
using namespace loci::formats::ome;
using namespace loci::formats::meta;
using namespace java::lang;
using namespace java::awt::image;

using namespace i3d;


static int jvm=-1;

// Utility functions and macros
#define SWAPEL(arr,i1,i2) { arr [ i1 ]^= arr [ i2 ]; arr [ i2 ]^= arr [ i1 ]; arr [ i1 ]^=arr [ i2 ];}

// Bails out if no reader/jvm present. Keeps the error messages and handling consistent.
#define BAIL_IF_NO_JVM { \
	if (jvm!=0) \
		throw IOException("JVM not initialized. Please check that jvm.dll is in your path."); \
}
#define BAIL_IF_NO_READER { \
	BAIL_IF_NO_JVM \
	if (reader==NULL) \
		throw IOException("Reader not initialized. Is the loci_tools.jar in the same path as executable?"); \
}
#define BAIL_IF_NULL(par,name) { \
	if ( par == NULL ) \
	throw IOException("Parameter " #par " (" #name ") cannot be null"); \
}

BioFormatsReader::BioFormatsReader(const char *fname, const VOI<PIXELS> *voi) :
	ImageReader(fname, voi)
{
	// Arguments check
	BAIL_IF_NULL(fname,Filename);

	if (strlen(fname)==0)
		throw IOException("Filename can't be empty.");

	// Only one instance of the JVM must be present, so create one if no one is present
	if (jvm!=0) {
		// Build argument list for the java machine
		char * argv = "-Djava.class.path=", * argv2;
		char *class_path = getenv("CBIA_JAVA_CLASSPATH");
        
        if (!class_path)
            throw IOException("You must set the CBIA_JAVA_CLASSPATH environment variable to point to the Bio Formats JAR file if you want to use Bio Formats.");

		size_t nl = strlen(argv)+strlen(class_path)+1;

		argv2 = new char[nl];
		
		strcpy(argv2,argv);
		strcat(argv2,class_path);

		// Create the Java virtual machine
		jvm = initJavaWrapper(1,&argv2);

		delete [] argv2;
	}

	BAIL_IF_NO_JVM;

	try {
		// Create the reader
		reader = new loci::formats::ImageReader();
	}
	catch(jthrowable e) {
		throw IOException("Trying to read with BioFormats. I could not create the bioformats reader, have you set the CBIA_JAVA_CLASSPATH correctly? Is it pointing to the Bio Formats JAR file?");
	}

	BAIL_IF_NO_READER;

	// Initialize deletion vector
	toDelete = vector<void*>();

	// Unsigned is the default
	signd=false;
    channel=-1;
	channels=0;
	timepoints=0;
	activeTimepoint=0;
}

BioFormatsReader::~BioFormatsReader(void)
{
	// Deallocate residual objects
	for (size_t i=0; i<toDelete.size(); i++)
		if (toDelete[i]) {
			delete toDelete[i];
		}

	// Delete the reader
	delete reader;
}

size_t BioFormatsReader::GetNumChannels()
{
    return channels;
}

void BioFormatsReader::swapBytes(i3d::byte* buffer) {
	BAIL_IF_NULL(buffer,swapBytes);

	// Swap all bpp bytes in order to bring it back to little endian
	bool lendian = reader->isLittleEndian();

	// If it's little endian, no need to do more
	if (lendian)
		return;

	// If it's floating point, it should be already in the correct order
	if (header.itype==i3d::FloatVoxel)
		return;

	// If a region has been specified, sizes are different
	size_t dx = (crop? crop->size.x : header.size.x);
	size_t dy = (crop? crop->size.y : header.size.y);
	size_t dz = (crop? crop->size.z : header.size.z);
	
	size_t len = dx*dy*dz*header.bpp;

#ifdef I3D_DEBUG
	cout << "Swapping byte order to little endian" << endl;
#endif

	switch(header.bpp) {
		case 1:
			// Hoorray, no swap needed!
			break;
		case 2:
			// Swap byte 0 with 1, 2 with 3, ...
			for (size_t b=1; b<len; b+=2)
				SWAPEL(buffer,b,b-1);
			lendian=true;
			break;
		case 4:
			// Swap byte 0 with 3, 4 with 7, ...
			for (size_t b=3; b<len; b+=4)
				SWAPEL(buffer,b,b-3);
			// Swap byte 1 with 2, 5 with 6, ...
			for (size_t b=2; b<len; b+=4)
				SWAPEL(buffer,b,b-1);
			lendian=true;
			break;
		default:
			throw i3d::IOException("Unsupported bpp");
	}
}


size_t BioFormatsReader::GetFileNames(FileList &fl) {
	fl.push_back(header.name);

	return fl.size();
}

void BioFormatsReader::LoadImageInfo() {
	try {

		reader->setMetadataFiltered(true);	// Ignore unreadable data
		reader->setOriginalMetadataPopulated(false);	// Don't read additional proprietary metadata 

		// Create an OME-XML metadata store
		MetadataTools staticMS(wrapperIntern);
		OMEXMLMetadata* store = new OMEXML200706Metadata();
			if (!store) throw IOException("OME-Java library not found.");
		MetadataStore* storewrapper = new MetadataStore(store->getJavaObject());
			if (!storewrapper) throw IOException("Could not create MetadataStore.");

		// Push the objects to the deletable stack
		toDelete.push_back(storewrapper);
		toDelete.push_back(store);

		reader->setMetadataStore(storewrapper);

		// Open the image
		try {reader->setId(header.name.c_str());}
        catch (jthrowable t) {
            throw IOException(header.name+" not found or unrecognizable.");
        }

		// Identify information
		//int n = reader->getImageCount();
		int width = reader->getSizeX();
		int height = reader->getSizeY();
		int depth = reader->getSizeZ();
		channels = reader->getSizeC()/reader->getRGBChannelCount();
		timepoints = reader->getSizeT();

#ifdef I3D_DEBUG
		cout << " Image: " << header.name << endl;
		cout << " - type: " << reader->getFormat() << endl;
		cout << " - size(xyzct): " << width << 
			"x" << height << 
			"x" << depth << 
			"x" << channels << 
			"x" << timepoints << 
			endl;
		cout << " - color: " << reader->isRGB() << endl;
		cout << " - image count: " << reader->getImageCount() << endl;
#endif

		MetadataRetrieve* retrievewrapper = new MetadataRetrieve(store->getJavaObject());

		Float* fx = retrievewrapper->getDimensionsPhysicalSizeX(0,0);
		Float* fy = retrievewrapper->getDimensionsPhysicalSizeY(0,0);
		Float* fz = retrievewrapper->getDimensionsPhysicalSizeZ(0,0);

		float ffx, ffy, ffz;
		if (!fx || !fy) {
			ffx = 1; ffy = 1;
		} else {
			ffx = 1./fx->floatValue(); delete fx;
			ffy = 1./fy->floatValue(); delete fy;
		}
		if (!fz) {
			ffz = 1;
		} else {
			ffz = 1./fz->floatValue(); delete fz; 
		}

		// Delete previous wrapper
		if (retrievewrapper) delete retrievewrapper;

		int ptype = reader->getPixelType();

		FormatTools f(wrapperIntern);
		int bpp = f.getBytesPerPixel(ptype);

		ImgVoxelType vtype = UnknownVoxel;
		signd=false;
		if(ptype == f.UINT8) {
			if (reader->isRGB())
				vtype = RGBVoxel;
			else
				vtype = Gray8Voxel;
		} else if (ptype == f.UINT16) {
			if (reader->isRGB())
				vtype = RGB16Voxel;
			else
				vtype = i3d::Gray16Voxel;
/*		} else if (ptype == f.UINT32) {					// NO CORRESPONDENCE TO I3D
				// vtype = i3d::Gray32Voxel;
*/		} else if (ptype == f.INT8) {
			if (reader->isRGB())
				vtype = RGBVoxel;
			else
				vtype = i3d::Gray8Voxel;
			signd = true;
		} else if (ptype == f.INT16) {
			if (reader->isRGB())
				vtype = RGB16Voxel;
			else
				vtype = i3d::Gray16Voxel;
			signd = true;
/*		} else if (ptype == f.INT32) {                  // NO CORRESPONDENCE TO I3D
				vtype = i3d::??;
				signd = true;
*/		} else if (ptype == f.FLOAT) {
				vtype = i3d::FloatVoxel;
				signd = true;
/*		} else if (ptype == f.DOUBLE) {					// NO CORRESPONDENCE TO I3D
				signd = true;
*/		} else /*if (
			ptype == f.SPECTRA || 
			ptype == f.CHANNEL || 
			ptype == f.LIFETIME || 
			ptype == f.POLARIZATION)*/ {
				throw i3d::IOException("Not supported type, sorry.");
		}

		header.bpp = bpp;
		header.color_inversion;				// ASK
		header.compression;					// ASK
		header.itype = vtype; 
		header.offset;						// ASK
		header.resolution = new Vector3d<float>(ffx,ffy,ffz);
		header.size.x = width;
		header.size.y = height;
		header.size.z = depth;
		header.zero_at_the_top = true;		// ASK
		header.zero_on_the_left = true;		// ASK

	} catch (jthrowable e) {
		if (&e==NULL)
			throw IOException("Unknown exception while opening BioFormats.");
		java::lang::Throwable* thr = new java::lang::Throwable(e);
		thr->printStackTrace();
		throw IOException(" Exception occurred: " + string(thr->getMessage()));
	}
}

void BioFormatsReader::LoadImageData(GRAY8 *data){
	BAIL_IF_NULL(data,image);

	if (header.itype!=i3d::Gray8Voxel)
		throw IOException("Wrong data type for parameter.");

	LoadImageBytes((byte*)data);
}

void BioFormatsReader::LoadImageBytes(byte* data){
	BAIL_IF_NULL(data, bytes);

	try {
		size_t x0 = (crop? crop->offset.x : 0);
		size_t dx = (crop? crop->size.x : header.size.x);
		size_t y0 = (crop? crop->offset.y : 0);
		size_t dy = (crop? crop->size.y : header.size.y);
		size_t z0 = (crop? crop->offset.z : 0);
		size_t dz = (crop? crop->size.z : header.size.z);

        if (channel==-1)
        {
            channel=0;
            cout << "Reading first channel only"<<endl;
        }
        
		for (size_t zi=0; zi<dz; zi++) {
			// Entry point for writing
			byte* ep = data+(header.bpp*dx*dy)*zi;

			// Read the bytes in
#ifdef I3D_DEBUG
            cout << "Reading Z:"<<z0+zi<<",C:"<<channel<<",T:"<<activeTimepoint<<endl;
#endif
			int i = reader->getIndex(z0+zi,channel,activeTimepoint);
			JavaByteArray* ba = reader->openBytes(i,x0,y0,dx,dy);

			// Create wrapping array
			memcpy(ep, ba->getArrayData(), ba->getArrayLength());

			delete ba;

			// TODO LOG?
		}
	} catch (jthrowable e) {
		if (&e==NULL)
			throw IOException("Unknown exception while reading image using BioFormats.");
		Throwable thr(e);
		thr.printStackTrace();
		string msg = string(" Exception occurred while reading image: ") + string(thr.getMessage());
		throw
			i3d::IOException(msg);
	}
}

void BioFormatsReader::LoadImageData(BINARY *data){
}

void BioFormatsReader::LoadImageData(RGB *data) {
	BAIL_IF_NULL(data,image);

	if (header.itype!=i3d::RGBVoxel)
		throw IOException("Wrong data type for parameter.");

	LoadImageRGB((byte*) data);
}

void BioFormatsReader::LoadImageRGB(byte* data) {

	// CHANNELS R, then G, then B. Suppose they're this order.
	// If they're are more than 3 channels, read the first 3 channels.
	// If less, read the first one or two.
	
	// NOTE: Does not work for plane-interleaved images!!

	// If a region has been specified, only treat that region
	size_t dx = (crop? crop->size.x : header.size.x);
	size_t dy = (crop? crop->size.y : header.size.y);
	size_t dz = (crop? crop->size.z : header.size.z);
	
	size_t len = dx*dy*dz*header.bpp;
	size_t RGBlen = len*reader->getRGBChannelCount();

	byte* tmp = new  byte[RGBlen];

	LoadImageBytes(tmp);

	if (!(reader->isInterleaved()))
		// Interleave
		for (size_t CH=0; CH<3; CH++) {
			byte* tmpCH = tmp + len*header.bpp*CH;
			// Expand by a factor 3 and place into the data buffer in the red position
			for (size_t i=0; i<len; i++)
				for (int b=0; b<header.bpp; b++)
					*(data+i*header.bpp*3+CH*header.bpp+b)=*(tmpCH+i*header.bpp+b);
		}
	else
		memcpy(data, tmp, RGBlen);

	delete [] tmp;
}

void BioFormatsReader::LoadImageData(GRAY16 *data){
	BAIL_IF_NULL(data,image);

	if (header.itype!=i3d::Gray16Voxel)
		throw IOException("Wrong data type for parameter.");

	LoadImageBytes((byte*)data);
	
	swapBytes((byte*)data);
}

void BioFormatsReader::LoadImageData(RGB16 *data){
	BAIL_IF_NULL(data,image);

	if (header.itype!=i3d::RGB16Voxel)
		throw IOException("Wrong data type for parameter.");

	LoadImageRGB((byte*) data);

	swapBytes((byte*)data);
}

void BioFormatsReader::LoadImageData(float *data) {
	BAIL_IF_NULL(data,image);

	if (header.itype!=i3d::FloatVoxel)
		throw IOException("Wrong data type for parameter.");

	LoadImageBytes((byte*)data);
}

void BioFormatsReader::LoadImageData(double *data) {
	BAIL_IF_NULL(data,image);

	if (header.itype!=i3d::FloatVoxel)
		throw IOException("Wrong data type for parameter.");

	LoadImageBytes((byte*)data);
}

void BioFormatsReader::LoadImageData(int *data) {
	BAIL_IF_NULL(data,image);

	if (header.itype!=i3d::FloatVoxel)
		throw IOException("Wrong data type for parameter.");

	LoadImageBytes((byte*)data);

	swapBytes((byte*)data);
}

void BioFormatsReader::LoadImageData(size_t *data) {
}

void BioFormatsReader::LoadImageData(Vector3d<float> *data) {
}

void BioFormatsReader::LoadImageData(Vector3d<double> *data) {
}

