/*
 * i3dlib - image manipulation library
 *
 * Copyright (C) 2000-2006   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 * FILE: imgTGA.cc
 *
 * I/O routines for TGA images
 *
 * David Svoboda <svoboda@fi.muni.cz> 2005
 *
 */
/*Suppression of Deprecated warning for Visual studio 8*/
#ifdef _MSC_VER
#if _MSC_VER == 1400
#define _CRT_SECURE_NO_DEPRECATE 1
#endif
#endif

#include "i3d_config.h"

#ifdef WITH_TARGA

#include <string>
#include <cstdio>
#include <limits.h>

#ifdef __GNUG__
#pragma implementation
#endif

#include "basic.h"
#include "toolbox.h"
#include "imgTGA.h"

using namespace std;


namespace i3d {
  
/**********************************************************************/ 
/* SetBuf */	
/**********************************************************************/ 
/*void SetBuf(FILE *fd, long cacheSize, char* IOcache)
	{ 
   if (cacheSize > 0)
  	 {
    // Use a user-supplied buffer or allocate a temporary one:
    if (setvbuf(fd, IOcache, _IOFBF, cacheSize)) 
#ifdef I3D_DEBUG
      throw IOException("Unable to allocate input file buffer.");
    else cerr << "TGA SetBuf: Input file buffer allocated." <<endl;
#else
    ; // buffer allocation failed - we'll just use
      // the standard one
#endif
  	 };
	}
*/
/**********************************************************************/ 
/* swapRB */
/**********************************************************************/ 
inline void swapRB(RGB* voxel) {swap(voxel->blue, voxel->red);}


/*************************************************************************/
/* constructor */
/*************************************************************************/
TGAReader::TGAReader(const char *fname) : ImageReader(fname)
		{
		   if ((fd = fopen(fname, "rb")) == NULL)
		     throw IOException("Unable to open "+ string(fname) + '.');
		   //SetBuf(fd, cacheSize, IOcache);
		}

/*************************************************************************/
/* LoadImageInfo */
/*************************************************************************/
void TGAReader::LoadImageInfo()
{
	static byte tmpbuf[18];
	const string NaTGA = "Not a TGA file.";

	/* Read the TGA header from disk: */
	if (fread((void *)tmpbuf, sizeof(byte)*18, 1, fd) != 1)
		throw IOException(NaTGA);

	info_tga.id_length = tmpbuf[0];
	info_tga.colormap_type = tmpbuf[1];
	info_tga.image_type = tmpbuf[2];
	info_tga.colormap_index = LSBFirstToShort(tmpbuf+3);
	info_tga.colormap_length = LSBFirstToShort(tmpbuf+5);
	info_tga.colormap_size = tmpbuf[7];
	info_tga.x_origin = LSBFirstToShort(tmpbuf+8);
	info_tga.y_origin = LSBFirstToShort(tmpbuf+10);
	header.size.x = LSBFirstToShort(tmpbuf+12);
	header.size.y = LSBFirstToShort(tmpbuf+14);
	header.bpp = tmpbuf[16];
	info_tga.attributes = tmpbuf[17];

	/* Skip the image ID fields: */
	if(info_tga.id_length && fseek (fd, info_tga.id_length, SEEK_CUR))
		throw IOException(NaTGA);

   /* Consider various TGA types: */
	string it;
   const string unsup="Unsupported TGA image type: ";
   switch (info_tga.image_type)
		  {
   	  /* Unsupported types: */
        case TGAColormap:
        case TGALEColormap:
                it= "color mapped image."; // error message
                throw IOException(unsup+it);
        /* Supported types: */
        case TGARGB:
        case TGALERGB:
                header.itype = RGBVoxel;
                break;
        case TGAGray:
        case TGALEGray:
                header.itype = Gray8Voxel;
                break;
        default: // unknown type
                it = "type number = " + itos(info_tga.image_type) + '.';
                throw IOException(unsup + it);
        }

   /* hdr points to the TGA image header,
      fd points into the input file at the start of the pixel data. */

   /* Find the pixel structure used in the file: */
   abpp = (header.bpp == 8) ? 0 : info_tga.attributes & TGA_ATTRIB_ABITS;
        
   /* The lower four bits of info_tga.attributes contain the number
      of alpha (attribute) bits in each pixel. */
   int pbpp = header.bpp - abpp; // number of color-coding bits
   if (pbpp <=0)
	 	{
    		throw IOException("LoadImageInfo: " \
		"Wrong description of the pixel structure, bpp = " 
		+ itos(header.bpp) + ", abpp = " + itos(abpp) +'.');
       }
        
	if (header.bpp % 8     /* bit-compressed image */
            || (abpp &&  /* or alpha bits present and */
               !(header.itype == RGBVoxel && ((abpp == 8 && pbpp == 24) /*not TARGA32*/
                                          || (abpp == 1 && pbpp == 15))/* or TARGA16*/)))
	{
                throw IOException(\
                "Pixel structure doesn't correspond to the "\
                "TARGA 16/24/32 specifications.");
        }

   /* Find other characteristics of the TGA image: */
   if ((info_tga.image_type == TGALERGB) || (info_tga.image_type == TGALEGray))
		// uses RLE compression?
		header.compression = true;
	else
		header.compression = false;

   header.zero_on_the_left = (info_tga.attributes & TGA_ATTRIB_HORIZONTAL) == 0; 
   header.zero_at_the_top = (info_tga.attributes & TGA_ATTRIB_VERTICAL) != 0;   

   if (info_tga.colormap_type == 0) {
                // Read the size of the histogram portion (FISH specific):
		_tsize = LSBFirstToShort(tmpbuf+3);
   }
   else _tsize = 0;
}

/*************************************************************************/
/* LoadHistogram */
/*************************************************************************/
/*
void TGAReader::LoadHistogram(HistDesc& hd)
{
   if (_tsize <= 0) { // no histogram
     hd = HistDesc();
     return;
   }

   if (HistDesc_static_part_iosize != 16 ||
       (_tsize - HistDesc_static_part_iosize) % 4 != 0)
     throw InternalException(\
	"LoadHistogram: Histogram size mismatch.");

   size_t hlen = (_tsize - HistDesc_static_part_iosize) / 4; 
   // length of the histogram
   hd.hist.resize(hlen);

   byte buf[HistDesc_static_part_iosize];

   for (size_t i=0; i < hlen; i++) {
     if (fread((void *)buf, 4, 1, fd) != 1)
       throw IOException("Error reading the histogram.");
     hd.hist[i] = LSBFirstToLong(buf);
   }

   if (fread((void *)buf, HistDesc_static_part_iosize, 1, fd) != 1)
     throw IOException("Error reading the histogram info.");

   // Fill fixed-length fields of hd from buf:

   hd.id = LSBFirstToShort(buf);
   hd.mean1 = buf[2];
   hd.stdDev1 = buf[3];
   hd.mean2 = buf[4];
   hd.stdDev2 = buf[5];
   hd.threshold = buf[6];
   hd.histoSmooth = buf[7];
   hd.histoLMF = LSBFirstToShort(buf+8);
   hd.maxRoundness = LSBFirstToShort(buf+10);
   hd.minSize = LSBFirstToLong(buf+12);
}
*/

/**********************************************************************/ 
/* LoadData	*/	
/**********************************************************************/ 
template <class T> void TGAReader::LoadData(T *data)
  {
  /* Read pixels into the data field: */
  ReadPixels(data);

  /* Reverse the image, if necessary: */
  if (!header.zero_at_the_top) // origin in TARGA is in left bottom corner
  		SwapVert(header.size.x, header.size.y, data);

  if (!header.zero_on_the_left)
  		SwapHoriz(header.size.x, header.size.y, data);
  }

/**********************************************************************/ 
/* ~TGAReader */
/**********************************************************************/ 
TGAReader::~TGAReader()
	{
	fclose(fd);
	}

/**********************************************************************/ 
/* ReadPixels */
/**********************************************************************/ 
template <class T> void TGAReader::ReadPixels (T* data)
{
  size_t length = header.size.x*header.size.y;
  bool isRGB = (header.itype == RGBVoxel);
  //bool isRGB = typeid(T) == typeid(RGB);

  if (!abpp && header.bpp == sizeof(T) * CHAR_BIT && 
  		(header.compression ==  false)) {
    // One input operation should be sufficient to read the whole image.
    if ( fread((void *)data, length*sizeof(T), 1, fd) == 1)
      {
        if (isRGB)
          {
            /* Make RGB from GBR used in TARGA: */
            for (size_t i=0; i<length; i++)
              swapRB((RGB*)data + i);
          }
        return;
      }
    else
      if (feof(fd))
        throw IOException("ReadPixels: Premature end of file.");
      else
        throw IOException("ReadPixels: Error reading file.");
  }
  
  if (abpp) throw IOException(\
                              "ReadPixels: Alpha bits not yet implemented.");

  /* Read a RLE-compressed file: */

  if ((header.compression == true) && 
  		header.bpp == sizeof(T) * CHAR_BIT ) 
  {
    size_t i,
           pixel; // index of the current pixel in data[]
    int    clen,  // length of the current compressed packet
           rlen;  // length of the current raw packet
    /* The current packet is either compressed or raw, so if clen>0, then rlen=0
	 and vice versa. */
    int head; // value of the 1-byte head of the packet
    T sample; // repeated value from a compressed packet

    clen=rlen=0;

    for (i=0; i<header.size.y; i++) 
    // Read one line (packets never contain pixels from more than one line):
    {
	// clen=rlen=0; 
	// if the above line is commented out, packed running through
	// the end of a scan line are allowed

	for (pixel = i*header.size.x; pixel < (i+1)*header.size.x; pixel++) 
		// read data[pixel]
	{
	  if (clen == 0 && rlen == 0)  // start the next packet
	  {
	    head = getc(fd);
	    if (head == EOF) {
	      throw IOException("ReadPixels: Premature end of file.");
	    }
	    else if (head >= 128) // compressed packet
            {
 		clen = head - 127; // set the number of repeated pixels
		// read the repeated value:
		if (fread((void*)&sample, sizeof(T), 1, fd) < 1) {
		  throw IOException("ReadPixels: Premature end of file.");
		}
            }
	    else // raw packet
		rlen = head + 1;
	  }

	  if (clen > 0) // copy the repeated value to the current pixel		
	  {
	    data[pixel] = sample;
	    clen--;
	  }
	  else // read the next pixel from the raw packet (rlen > 0)
	  {
	    if (fread((void*)&data[pixel], sizeof(T), 1, fd) < 1) {
		  throw IOException("ReadPixels: Premature end of file.");
	    }
	    rlen--;
	  }

	  if (isRGB)
          {
            /* Make RGB from GBR used in TARGA: */
              swapRB((RGB*)data + pixel);
          }

	} // for
	#if 0
	if (rlen != 0 || clen !=0) 
		cerr << "ReadPixels: line-wrapping RLE packet at the end of "
		<< i << "-th line. Continuing..." <<endl;
	#endif
    } // for	
    return;
  } // RLE

  throw IOException("ReadPixels: Wrong pixel structure: bpp = "
                    + itos(header.bpp) + " abpp = " \
                    + itos(abpp) + ".");
}

/*************************************************************************/
/* constructor */
/*************************************************************************/
TGAWriter::TGAWriter(const char *fname) : ImageWriter(fname),_tsize()
		{
		  if ((fd = fopen(fname, "wb")) == NULL)
	            throw IOException("TGA: Unable to create "+ string(fname) + '.');
		  //SetBuf(fd, cacheSize, IOcache);
		};

/*************************************************************************/
/* SaveImageInfo */
/*************************************************************************/
void TGAWriter::SaveImageInfo()
	{
  switch (header.itype)
    {
    case RGBVoxel:
      header.bpp = 24; abpp = 0;
		info_tga.image_type = 
			(header.compression == true) ? TGALERGB : TGARGB;
      break;
     
   case Gray8Voxel:
      header.bpp = 8; abpp = 0;
		info_tga.image_type = 
			(header.compression == true) ? TGALEGray : TGAGray;
      break;

	case BinaryVoxel:
		header.bpp = 8; abpp = 0;
		info_tga.image_type = 
			(header.compression == true) ? TGALEGray : TGAGray;
      break;
      
    default:
      throw IOException("SaveImageInfo: Unknown voxel type.");
    }

	
   int pbpp = header.bpp - abpp; // number of color-coding bits
        if (pbpp <=0) {
                throw IOException("SaveImageInfo: " \
		"Wrong description of the pixel structure, bpp = ." 
		+ itos(header.bpp) + ", abpp = " + itos(abpp) +'.');
        }
        if (header.bpp % 8     /* bit-compressed image */
            || (abpp &&  /* or alpha bits present and */
                !(header.itype == RGBVoxel && ((abpp == 8 && pbpp == 24) /*not TARGA32*/
                                            || (abpp == 1 && pbpp == 15))))){// or TARGA16
                throw IOException(\
                "Pixel structure doesn't correspond to the "\
                "TARGA 16/24/32 specifications.");
        }


   /* Set the pixel structure used in the file: */
	info_tga.attributes = abpp & TGA_ATTRIB_ABITS;
   /* The lower four bits of info_tga.attributes contain the number
   	of alpha (attribute) bits in each pixel. */

   /* Set other characteristics of the TGA image: */
   if (!header.zero_on_the_left) 
		info_tga.attributes |= TGA_ATTRIB_HORIZONTAL; // horiz. reversed
   if (header.zero_at_the_top)
		info_tga.attributes |= TGA_ATTRIB_VERTICAL;  // vert. reversed

	/* Set the remaining fields of the TGA header; */
	info_tga.id_length = 0;
	info_tga.colormap_type = 0;
	info_tga.colormap_index =  info_tga.colormap_length = 0;
	info_tga.colormap_size = 0;
	info_tga.x_origin = info_tga.y_origin = 0;

	/* Save the header: */
	static byte tmpbuf[18];

	tmpbuf[0] = info_tga.id_length;
	tmpbuf[1] = info_tga.colormap_type;
	tmpbuf[2] = info_tga.image_type;
        if (info_tga.colormap_type == 0) {
                // Write the size of the histogram portion (FISH specific):
		ShortToLSBFirst(tmpbuf+3, _tsize);
        }
	else 
	        ShortToLSBFirst(tmpbuf+3, info_tga.colormap_index);
	ShortToLSBFirst(tmpbuf+5, info_tga.colormap_length);
	tmpbuf[7] = info_tga.colormap_size;
	ShortToLSBFirst(tmpbuf+8, info_tga.x_origin);
	ShortToLSBFirst(tmpbuf+10, info_tga.y_origin);
	ShortToLSBFirst(tmpbuf+12, header.size.x);
	ShortToLSBFirst(tmpbuf+14, header.size.y);
	tmpbuf[16] = header.bpp;
	tmpbuf[17] = info_tga.attributes;

	if (fwrite((void *)tmpbuf, sizeof(byte)*18, 1, fd) != 1)
       throw IOException("Error writing a TGA header.");
	}	

/*************************************************************************/
/* SaveHistogram */
/*************************************************************************/
/*
void TGAWriter::SaveHistogram(const HistDesc& hd)
{
   if (_tsize <= 0) return; // no histogram

   if (hd.iosize() != _tsize || HistDesc_static_part_iosize != 16)
     throw InternalException(\
	"SaveHistogram: Histogram size mismatch.");

   if (hd.mean1 > UCHAR_MAX || hd.stdDev1 > UCHAR_MAX ||
       hd.mean2 > UCHAR_MAX || hd.stdDev2 > UCHAR_MAX ||
       hd.threshold > UCHAR_MAX)
     throw InternalException(\
	"SaveHistogram: Histogram indexes don't fit into one byte.");

   byte buf[HistDesc_static_part_iosize];

   for (size_t i=0; i < hd.hist.size(); i++) {
     LongToLSBFirst(buf, hd.hist[i]);
     if (fwrite((void *)buf, 4, 1, fd) != 1)
       throw IOException("Error writing the histogram.");
   }

   // Fill buf from fixed-length fields of hd:

   ShortToLSBFirst(buf, hd.id);
   buf[2] = static_cast<byte>(hd.mean1);
   buf[3] = static_cast<byte>(hd.stdDev1);
   buf[4] = static_cast<byte>(hd.mean2);
   buf[5] = static_cast<byte>(hd.stdDev2);
   buf[6] = static_cast<byte>(hd.threshold);
   buf[7] = hd.histoSmooth;
   ShortToLSBFirst(buf+8, hd.histoLMF);
   ShortToLSBFirst(buf+10, hd.maxRoundness);
   LongToLSBFirst(buf+12, hd.minSize);

   if (fwrite((void *)buf, HistDesc_static_part_iosize, 1, fd) != 1)
     throw IOException("Error writing the histogram info.");
}
*/

  
/**********************************************************************/ 
/* writePixels */
// Writes n pixels to fd. Returns number of pixels written or EOF in the 
// case of an IO error.
/**********************************************************************/ 
template<class T> static inline long 
	writePixels(FILE *fd, T *data, size_t n, bool isRGB)
{
  static size_t i;
  static RGB voxel;
  if (isRGB)
  {
    for (i=0; i<n; i++) {
	voxel = ((RGB*)data)[i];
	swapRB(&voxel);
	if (fwrite((void *)&voxel, sizeof(RGB), 1, fd) != 1)
	  return EOF;
    }
  }
  else 
  { // grayscale
    // One output operation should be sufficient to write 
    // all pixels.
    if ( fwrite((void *)data, n*sizeof(T), 1, fd) != 1)
	return EOF;
  }
  return n;
}

/**********************************************************************/ 
/* flushRaw */
/**********************************************************************/ 
template<class T> static inline 
void flushRaw(FILE *fd, const T* from, size_t rlen, bool isRGB)
{
  if (putc(rlen - 1, fd) == EOF) // write the head of the packet
    throw IOException("SaveData: Error writing file.");
  // Write the raw packet voxels:
  if (writePixels(fd, from, rlen, isRGB) != static_cast<long>(rlen)) 
    throw IOException("SaveData: Error writing file.");
}

/**********************************************************************/ 
/* flushCompressed */
/**********************************************************************/ 
template<class T> static inline 
void flushCompressed(FILE *fd, const T* from, size_t clen, bool isRGB)
{
  if (putc(128 + clen, fd) == EOF) // write the head of the packet
    throw IOException("SaveData: Error writing file.");
  // Write the repeated value of the compressed packet:
  if (writePixels(fd, from, 1, isRGB) != 1) 
    throw IOException("SaveData: Error writing file.");
}

/**********************************************************************/ 
/* SaveData */
/**********************************************************************/ 
template <class T> void TGAWriter::SaveData(const T* data)
  {
  long length = header.size.x*header.size.y;
  bool isRGB = (header.itype == RGBVoxel);
  //bool isRGB = typeid(T) == typeid(RGB);

  if (!abpp && header.bpp == sizeof(T) * CHAR_BIT && 
  		(header.compression != true))
    {
	if (writePixels(fd, data, length, isRGB) != length)
        throw IOException("SaveData: Error writing file.");
      return;
    }

  if (abpp)
    throw IOException("SaveData: Alpha bits not yet implemented.");
  /* Write a RLE-compressed file: */

  if ((header.compression == true) && 
  		header.bpp == sizeof(T) * CHAR_BIT ) 
  {
    size_t i,
           pixel; // index of the current pixel in data[]
    int    clen,  // length of the current compressed packet
           rlen;  // length of the current raw packet
    /* The current packet is either compressed or raw, so if clen>0, then rlen=0
	 and vice versa. */
    const T* from; // pointer to the first pixel, which is not yet written:

    for (i=0; i<header.size.y; i++) 
    // Write one line (packets never contain pixels from more than one line):
    {
	clen=rlen=0;
	from = &data[i*header.size.x];

	for (pixel = i*header.size.x; pixel < (i+1)*header.size.x -1; pixel++) 
        {
	  if (data[pixel] != data[pixel+1]) // next pixel is different 
	  {
	    if (clen > 0) // flush the compressed packet up to data[pixel]
	    {
	        flushCompressed(fd, from, clen, isRGB);
		from = &data[pixel+1];
		clen=0;
		rlen=0;
	    }
	    else  // extend the raw packet
		rlen++;
	  }
	  else  // next pixel is the same
	    if (rlen > 0) // flush the raw packet up to data[pixel-1]
	    {
		flushRaw(fd, from, rlen, isRGB);
		from = &data[pixel];
		clen=1;
		rlen=0;
	    }
	    else // extend the compressed packet
		clen++;

	  if (clen == 128) // compressed packet of the max length
	  {
	        flushCompressed(fd, from, 127, isRGB);
		from = &data[pixel+1];
		clen=0;
		rlen=0;
	  }
	  else if (rlen == 128) // raw packet of the max length
	  {
		flushRaw(fd, from, 128, isRGB);
		from = &data[pixel+1];
		clen=0;
		rlen=0;
	  }
	} // for (pixels)

	// Flush the last packet of the row:
	if (clen > 0)
	        flushCompressed(fd, from, clen, isRGB);
	else // flush the raw packet, including the last pixel
		flushRaw(fd, from, rlen+1, isRGB);

    } // for (rows)

    return;
		
  } // RLE

  
  throw IOException("SaveData: Wrong pixel structure: bpp = "
                    + itos(header.bpp) + " abpp = " + itos(abpp) + ".");
}

/**********************************************************************/ 
/* ~TGAWriter */
/**********************************************************************/ 
TGAWriter::~TGAWriter()
	{
	fclose(fd);
	}

}

#endif
