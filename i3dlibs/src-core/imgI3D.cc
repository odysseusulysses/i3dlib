/*
 * i3dlib - image manipulation library
 *
 * Copyright (C) 2000-2006   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 * FILE: imgI3D.cc
 *
 * I/O routines for I3D images
 *
 * David Svoboda <svoboda@fi.muni.cz> 2005
 *
 */

#ifdef __GNUG__
#pragma implementation
#endif 

#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <map>
#include "imgI3D.h"
#include "toolbox.h"


namespace i3d {

/////////////////////////////////////////////////////////////////////////////
/** Reads text config file fname and returns map of configuration options
   (i.e. pairs \<key\>, \<val\>):

   Blank lines are skipped. From each non-blank line in file fn the pair
   \<key\>,\<val\> is constructed as follows:
   - string before the first occurence of \<sep\> is trimmed
     and supposed to be \<key\>
   - string after the first occurence of \<sep\> is trimmed and supposed to
     be \<val\>

   IOException is thrown if there is no non-blank character before \<sep\>
   (i.e. unspecified keyword)
   
   if the same keyword is used more then ones in the config file, the last
   value is set.  */
/////////////////////////////////////////////////////////////////////////////
bool ReadConfig(const char* fname, std::map<std::string,std::string> &items, 
		const char sep)
{
  std::ifstream f(fname);

  if (!f)
    throw IOException(std::string("I3DReader: Cannot open input file ") +
                      std::string(fname));
  
  size_t line_no = 1;

  while (!f.eof())
    {
      std::string s;
      std::string key,val;
      getline(f,s,'\n');

      size_t pos = s.find(sep);
      key = Trim(s.substr(0,pos));
      val = (pos == std::string::npos) ? std::string("")
			        : Trim(s.substr(pos+1,std::string::npos));

      if (key == "")
        {
          if (val == "")
            continue;
          else
            throw IOException("ReadConfig: Keyword not specified in file"
                              + std::string(fname) + " at line: " + itos(line_no));
        }

#ifdef I3D_DEBUG
  //    cerr << "Key: '" << key << "' Val: '" << val << "'" << endl;
#endif
      
      items[key] = val;
      ++line_no;
    }
  
  return true;
}

/////////////////////////////////////////////////////////////////////////////
/** Saves map into config file fname
   each pair on one line in form \<key\>\<sep\>\<val\> */
/////////////////////////////////////////////////////////////////////////////
bool SaveConfig(const char* fname, const std::map<std::string,std::string> &items,
                const char sep='=')
{
  std::ofstream f(fname);
  if (!f)
    throw IOException(std::string("Save Config: Cannot open output file ") +
                      std::string(fname));
  
  typedef std::map<std::string,std::string>::const_iterator CI;
  for (CI i = items.begin(); i != items.end(); i++)
    f << i->first << sep << i->second << std::endl;

  return true;
}

//////////////////////////////////////////////////////////////////////////
I3DReader::I3DReader(const char *fname, const VOI<PIXELS> *voi) :
		  SequenceReader(fname, voi)
{
	/// save the file name for later use
	header.name = fname;
}

//////////////////////////////////////////////////////////////////////////
size_t I3DReader::GetFileNames(FileList &fl)
{
	fl.clear();

	fl = nlist;
	fl.push_back(header.name);

	return fl.size();
}


//////////////////////////////////////////////////////////////////////////
void I3DReader::LoadImageInfo()
{
	std::map <std::string, std::string> i3d_header;

	/// read the image header information
	ReadConfig(header.name.c_str(), i3d_header, ':');

	/// store the information for later use
	header.itype = StringToVoxelType(i3d_header["type"]);

	std::string s;

   if ((s = i3d_header["resolution"]) != "")
	{
		/// create the resolution vector
		if (!header.resolution)
			header.resolution = new Vector3d<float>;
		
		/// and set its value
      if (!StrToVector3d (s.c_str (), *(header.resolution)))
            throw IOException ("I3DReader: Error reading resolution");
	}

   if ((s = i3d_header["offset"]) != "")
       if (!StrToVector3d (s.c_str (), header.offset))
           throw IOException ("I3DReader: Error reading offset");

   if ((s = i3d_header["size"]) != "")
       if (!StrToVector3d (s.c_str (), header.size))
           throw IOException ("I3DReader: Error reading size");

	if (i3d_header["mask"] != "")
	{
		/// get the path - if any
		std::string path, name, ext;
		SplitFilename(header.name.c_str(), path, name, ext);

		/** add the path to the file name given in the mask
		 * and permit the regular expansion - implicitly, it is forbidden */
		MaskExpand((path+i3d_header["mask"]).c_str(), nlist, true);
	}
	else
	{
		throw IOException("I3DReader: Error reading file mask");
	}

	SequenceReader::LoadImageInfo();
}

//////////////////////////////////////////////////////////////////////////
void I3DWriter::SaveImageInfo()
{
	size_t num = num_digits(header.size.z);

   std::string imgext = "." + FileFormatToExtension (IMPLICIT_FORMAT);

   std::ofstream f ((path + name + ".i3d").c_str ());
   if (!f)
      throw IOException (std::string ("Save Image: Cannot open output file ") +
                         path+ name + ".i3d");

   f << "size:" << header.size << std::endl;

	if (header.resolution)
	   f << "resolution:" << *(header.resolution) << std::endl;

   f << "offset:" << header.offset << std::endl;
   f << "type:" << VoxelTypeToString (header.itype) << std::endl;
	f.fill('0');
   f << "mask:" << "^" + name;
   if (header.size.z != 1) 
	{
      f << "_";
      for (size_t i = 0; i < num; i++)
           f << "[0-9]";
   }
   f << imgext << std::endl << std::endl;

	SequenceWriter::SaveImageInfo();
}
	
//////////////////////////////////////////////////////////////////////////
}

