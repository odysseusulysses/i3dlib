#pragma once

#include "imgfiles.h"

#include <vector>

#include "java_magic.h"

namespace i3d {

/// Reader subclass which uses the OME Bio-Formats Java library to read images
/// \param fname    complete path of the image to read. NOT NULL.
/// \param voi      volume of interest to open.
/// \param jvm_path path to the Java Virtual Machine 'java' executable
/// The reader will attempt to open the JVM, load the image and may throw an IOException 
/// if the operation fails
class BioFormatsReader :
	public i3d::ImageReader
{
public:

	/// Constructor which specifies the filename of the image to open, optionally the volume of interest and the path to the JAR file
    BioFormatsReader(const char *fname, const VOI<PIXELS> *voi = NULL);
    
    /// The destructor
	virtual ~BioFormatsReader(void);

	/// Get the image files name associtated to this image
    /// \param fl   reference to a file list
	virtual size_t GetFileNames(FileList &fl);

	/// Loads the image header
    /// Reads the header of the image and records its structures in the internal structures.
	virtual void LoadImageInfo();

	/// Load gray8 image data
    /// \param data pointer to a memory block of the appropriate (pre-allocated) size
	virtual void LoadImageData(GRAY8 *data);
	/// Load binary image data
    /// \param data pointer to a memory block of the appropriate (pre-allocated) size
	virtual void LoadImageData(BINARY *data);
	/// Load rgb image data
    /// \param data pointer to a memory block of the appropriate (pre-allocated) size
	virtual void LoadImageData(RGB *data);
	/// Load gray16 image data
    /// \param data pointer to a memory block of the appropriate (pre-allocated) size
	virtual void LoadImageData(GRAY16 *data);
	/// Load rgb16 image data
    /// \param data pointer to a memory block of the appropriate (pre-allocated) size
	virtual void LoadImageData(RGB16 *data);
	/// Load float data - empty function
    /// \param data pointer to a memory block of the appropriate (pre-allocated) size
	virtual void LoadImageData(float *data);
	/// Load float data - empty function
    /// \param data pointer to a memory block of the appropriate (pre-allocated) size
	virtual void LoadImageData(double *data);
	/// Load int data - empty function
    /// \param data pointer to a memory block of the appropriate (pre-allocated) size
	virtual void LoadImageData(int *data);
	/// Load size_t data - empty function
    /// \param data pointer to a memory block of the appropriate (pre-allocated) size
	virtual void LoadImageData(size_t *data);
	/// Load Vector3d<float> data - empty function
    /// \param data pointer to a memory block of the appropriate (pre-allocated) size
	virtual void LoadImageData(Vector3d<float> *data);
	/// Load Vector3d<double> data - empty function
    /// \param data pointer to a memory block of the appropriate (pre-allocated) size
	virtual void LoadImageData(Vector3d<double> *data);

    /// Get number of channels
    /// returns the number of available channels
    virtual size_t GetNumChannels();

	/// Set active channel
    /// \param timepoint    the timepoint index to set the reader to
    /// When reading a multi-dimensional image [set], the reader will read the specified image
	virtual void chooseTimePoint(size_t timepoint) { activeTimepoint=(timepoint>=timepoints?activeTimepoint:timepoint); };

private:
	loci::formats::ImageReader* reader;

	std::vector<void*> toDelete;

	void BioFormatsReader::swapBytes(i3d::byte* buffer); 
	virtual void LoadImageBytes(byte *data);
	virtual void LoadImageRGB(byte* data);

	bool signd;

	// chan (the activeChannel) is inherited from ImageReader
	size_t channels;
	size_t timepoints;
	size_t activeTimepoint;

};
}
