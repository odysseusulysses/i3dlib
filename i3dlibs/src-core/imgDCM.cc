/*
 * i3dlib - image manipulation library
 *
 * Copyright (C) 2000-2006   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 * FILE: imgDCM.cc
 *
 * I/O routines for DICOM images
 *
 * David Svoboda <svoboda@fi.muni.cz> 2008
 * Jakub Grochol <139693@mail.muni.cz> 2008,2009
 *
 */

#include "i3d_config.h"

#ifdef WITH_DCM

#include <iostream>
#include <fstream>

#include "imgDCM.h"
#include "toolbox.h"

#include "dcmtk/config/osconfig.h"    /* make sure OS specific configuration is included first */
#include "dcmtk/config/cfunix.h"
#include "dcmtk/dcmdata/dctk.h"
#include "dcmtk/dcmdata/dcvrda.h"
#include "dcmtk/dcmdata/dcvrtm.h"
#include "dcmtk/dcmdata/dcvrdt.h"
#include "dcmtk/dcmdata/dcdeftag.h"
#include "dcmtk/ofstd/ofconsol.h"
#include "dcmtk/dcmdata/dcfilefo.h"
#include "dcmtk/dcmdata/dcdict.h"
#include "dcmtk/dcmdata/dcpxitem.h"

#include "dcmtk/ofstd/ofstdinc.h"
#include "dcmtk/dcmdata/cmdlnarg.h"
#include "dcmtk/ofstd/ofconapp.h"

#include "dcmtk/dcmjpeg/djdecode.h"  /* for dcmjpeg decoders */
#include "dcmtk/dcmjpeg/djencode.h"  /* for dcmjpeg encoders */
#include "dcmtk/dcmdata/dcrleerg.h"
#include "dcmtk/dcmjpeg/djrplol.h"   /* for DJ_RPLossless */
#include "dcmtk/dcmjpeg/djrploss.h"  /* for DJ_RPLossy */
#include "dcmtk/dcmjpeg/dipijpeg.h"  /* for dcmimage JPEG plugin */
#include "dcmtk/dcmimgle/dcmimage.h"
#include "dcmtk/dcmimage/diregist.h"

namespace i3d {

  DcmFileFormat dcmHeader;
  DicomImage *dcmData;
  DcmDataset *dataset;
  DcmItem *metaInfo;
  char saveName[100];
  int overlayBits=0;
  long int bitsStored=0;

  //---------------------------------------------------------------------------
  // Constructors.
  //---------------------------------------------------------------------------
  /* Image reader constructor.
  */
  DCMReader::DCMReader(const char *fname): ImageReader(fname) 
  {
    if (!(dcmHeader.loadFile(fname)).good()){
      throw IOException("DCM: Unable to open "+ string(fname) + ".");
    }
    dataset = dcmHeader.getDataset();
  };

  /* Image writer constructor.
  */
  DCMWriter::DCMWriter(const char *fname): ImageWriter(fname) 
  {    
    dataset = dcmHeader.getDataset();
    metaInfo = dcmHeader.getMetaInfo();
    strncpy(saveName,fname,100);
  };

  /**********************************************************************/
  /* LoadImageInfo */
  /**********************************************************************/
  void DCMReader::LoadImageInfo()
  {
    unsigned int bps;

    OFString photometric;
    long int rows,columns,highBit,numFrames,pixRepre,bitsAllocated;
    long int spp = 0;
    OFString xres_str,yres_str,zres_str,xpos,ypos,zpos,pokk,bittt,retez;
    float xres,yres,zres;
 
    /// Read necessary info from image file header
    dataset->findAndGetOFString(DCM_PhotometricInterpretation, photometric);
    dataset->findAndGetLongInt(DCM_SamplesPerPixel, spp);
    dataset->findAndGetLongInt(DCM_Rows, rows);
    dataset->findAndGetLongInt(DCM_Columns, columns);
    dataset->findAndGetOFString(DCM_PixelSpacing, xres_str, 0);
    dataset->findAndGetOFString(DCM_PixelSpacing, yres_str, 1);
    dataset->findAndGetOFString(DCM_SliceThickness, zres_str);
    dataset->findAndGetOFString(DCM_ImagePosition, xpos, 0);
    dataset->findAndGetOFString(DCM_ImagePosition, ypos, 1);
    dataset->findAndGetOFString(DCM_ImagePosition, zpos, 2);
    dataset->findAndGetLongInt(DCM_BitsAllocated, bitsAllocated); 
    dataset->findAndGetLongInt(DCM_BitsStored, bitsStored);
    dataset->findAndGetLongInt(DCM_HighBit, highBit);
    dataset->findAndGetLongInt(DCM_NumberOfFrames, numFrames);
    dataset->findAndGetLongInt(DCM_PixelRepresentation, pixRepre);

    #ifdef I3D_DEBUG
      cerr << "Header Interpretace: " << photometric << endl;
      cerr << "Header SPP: " << spp << endl;
      cerr << "Header sirka : " << columns << endl;
      cerr << "Header vyska: " << rows << endl;
      cerr << "Header rozliseni X: " << xres_str << endl;
      cerr << "Header rozliseni Y: " << yres_str << endl;
      cerr << "Header Bits Allocated: " << bitsAllocated << endl;
      cerr << "Header Bits Stored: " << bitsStored << endl;
      cerr << "Header Number of Frames: " << numFrames << endl;
      cerr << "Header High bit: " << highBit << endl;
    #endif

    /// Get image size
    header.size.x=columns;
    header.size.y=rows;
    if(numFrames){
      header.size.z=numFrames;
    }else{
      header.size.z=1;
    }

    /// Get image offset
    header.offset.x = (float)atof(xpos.c_str()) * 1000.0;
    header.offset.y = (float)atof(ypos.c_str()) * 1000.0;
    header.offset.z = (float)atof(zpos.c_str()) * 1000.0;

    /// Get image resolution
    xres = (float)atof(xres_str.c_str());
    yres = (float)atof(yres_str.c_str());
    zres = (float)atof(zres_str.c_str());
    if(xres != 0.0 && yres != 0.0){
        header.resolution = new Vector3d<float>;
      
        header.resolution->x = 1.0/(xres*1000.0);
        header.resolution->y = 1.0/(yres*1000.0);
        header.resolution->z = 1.0;
      if(zres != 0.0)
        header.resolution->z = 1.0/(zres*1000.0);
    }

    if(!photometric.compare("MONOCHROME1")){ // MONOCHROME1 interpretation
        spp=1;
    }
    if(!photometric.compare("MONOCHROME2")){ // MONOCHROME2 interpretation
        spp=1;
    }
    if(!photometric.compare("RGB")){ // RGB interpretation
        spp=3;
    }
    if (spp==0){ // Unsupported photometric interpretation
      throw IOException("I3D::DCMReader: unsupported photometric interpretation.");
    }

    if(pixRepre == 1){
      //header.color_inversion = true;
    }

    /* Bits per pixel approximation */
    if(bitsStored<=8){
      bps=8;
    }else{
      if(bitsStored<=16){
        bps=16;
      }else{
        throw IOException("I3D::DCMReader: unsupported bits per sample value.");
      }
    }

    header.bpp=spp*bps;

    /// Choose proper color type from the bits per pixel value
    switch(header.bpp){
      case 8:header.itype=Gray8Voxel; break;
      case 16:header.itype=Gray16Voxel; break;
      case 24:header.itype=RGBVoxel; break;
      case 48:header.itype=RGB16Voxel; break;
      default:throw IOException("I3D::DCMReader: unsupported color type.");
    }
    
    // overlayBits defines number of least significant bits 
    // which are not pixel data, if there are some we must shift pixel data
    if(bitsAllocated > bitsStored){
      overlayBits = bitsAllocated - bitsStored;
    }else{
      overlayBits = 0;
    }

    /* If image data are compressed, we decompress them */

    DJDecoderRegistration::registerCodecs(); // register JPEG codecs

    /// Decompress data if they are compressed
    dataset->chooseRepresentation(EXS_LittleEndianExplicit, NULL);

    DJDecoderRegistration::cleanup(); // deregister JPEG codecs
  }

  /**************************************************************************/
  /* LoadData */
  /**************************************************************************/
 
  template <class T> void DCMReader::LoadData(T *data){
    T *origin = data; 
    unsigned int bitsPerSample;

    /* Choose proper bits per sample for data reading function */
    switch(header.itype){
      case Gray8Voxel:bitsPerSample=8;
        break;
      case Gray16Voxel:bitsPerSample=16;
        break;
      case RGBVoxel:bitsPerSample=8;
        break;
      case RGB16Voxel:bitsPerSample=16;
        break;
      default:throw IOException("I3D::DCMReader: unsupported color type.");
    }

    /* Image data should be decompressed and in little endian */
    if ((dcmData = new DicomImage(dataset,EXS_LittleEndianExplicit)) == NULL)
      throw IOException("DCM: Unable to access to the image data.");

    if (dcmData->getStatus() == EIS_Normal){
      //dcmData->setMinMaxWindow();
      /// Read image data layer by layer
      for(unsigned int i=0;i<header.size.z;i++){
        T *imageData = (T *)(dcmData->getOutputData(bitsPerSample,i,0));
        if (imageData != NULL){
            memcpy(data,imageData,header.size.x*header.size.y*sizeof(T));
            // overlayBits defines number of least significant bits 
            // which are not pixel data, if there are some we must shift pixel data
            if(overlayBits>0){
              for(unsigned int s=0;s<header.size.y;s++){
                for(unsigned int r=0;r<header.size.x;r++){
                  *(data+s*header.size.x+r) = *(data+s*header.size.x+r) >> overlayBits;
                }
              }
            }
            data += header.size.x*header.size.y;
        }
      }
    }else{
      throw IOException("DCM: Some image parameters are missing or are invalid.");
    }
    data = origin;

    // swap GRAY gradient
    if (header.color_inversion){
      // swap for each layer
      for(size_t i=0; i<header.size.z; i++)
      {
        ReverseColors(header.size.x, header.size.y, data);
        data += header.size.x*header.size.y;
      }
    }
  }

  
  /**************************************************************************/
  /* ~DCMReader */
  /**************************************************************************/
  DCMReader::~DCMReader()
  {
    dcmHeader.clear();
    delete dcmData;
  }

  /**************************************************************************/
  /* SaveImageInfo */
  /**************************************************************************/
  void DCMWriter::SaveImageInfo()
  { 
    char *photometric;
    Uint16 spp,bps;
    stringstream str_buf;
    Uint16 offset[3];

    /// Choose proper bit per pixel and sample per pixel values from color type
    switch(header.itype){
      case Gray8Voxel:
        photometric="MONOCHROME2\0";
        spp=1; 
        bps=8;
        break;
      case Gray16Voxel:
        photometric="MONOCHROME2\0";
        spp=1;
        bps=16;
        break;
      case RGBVoxel:
        photometric="RGB\0";
        spp=3;
        bps=8;
        break;
      case RGB16Voxel:
        photometric="RGB\0";
        spp=3;
        bps=16;
        break;
      default:throw IOException("I3D::DCMWriter: unsupported photometric interpretation.");
    }

    /// Write basic header information
    dataset->putAndInsertUint16(DCM_BitsAllocated, bps);
    dataset->putAndInsertUint16(DCM_BitsStored, bps);
    dataset->putAndInsertUint16(DCM_HighBit, bps-1);
    dataset->putAndInsertString(DCM_PhotometricInterpretation, photometric);
    dataset->putAndInsertUint16(DCM_SamplesPerPixel, spp);
    dataset->putAndInsertUint16(DCM_Columns, header.size.x);
    dataset->putAndInsertUint16(DCM_Rows, header.size.y);
str_buf << header.size.z;
    dataset->putAndInsertString(DCM_NumberOfFrames, str_buf.str().c_str());
str_buf.str("");
    dataset->putAndInsertString(DCM_PixelIntensityRelationship, "LIN");

    dataset->putAndInsertUint16(DCM_PixelRepresentation, 0);
    if(spp>1){
        dataset->putAndInsertUint16(DCM_PlanarConfiguration, 0);
    }
    
    dataset->putAndInsertString(DCM_WindowWidth, "255");
    dataset->putAndInsertString(DCM_WindowCenter, "128");

    /// Write offset information
    offset[0] = (Uint16)floor(header.offset.x / 1000.0);
    offset[1] = (Uint16)floor(header.offset.y / 1000.0);
    offset[2] = (Uint16)floor(header.offset.z / 1000.0);
    dataset->putAndInsertUint16Array(DCM_ImagePosition, offset, 3);
    dataset->putAndInsertString(DCM_SOPClassUID,UID_SecondaryCaptureImageStorage);
    char uid[100];
    dataset->putAndInsertString(DCM_SOPInstanceUID,dcmGenerateUniqueIdentifier(uid,SITE_INSTANCE_UID_ROOT));
    dataset->putAndInsertString(DCM_PatientsName,"CBIA");

    /// Write resolution information
    if(header.resolution){
      str_buf << (1.0/(header.resolution->x*1000.0)) << "\\";
      str_buf << (1.0/(header.resolution->y*1000.0));
      dataset->putAndInsertString(DCM_PixelSpacing, str_buf.str().c_str());
      str_buf.str("");
      if(header.resolution->z != 0.0)      
        str_buf << (1.0/(header.resolution->z*1000.0));
        dataset->putAndInsertString(DCM_SliceThickness, str_buf.str().c_str());
    }
  }

  /**************************************************************************/
  /* SaveData */
  /**************************************************************************/
  template <class T> void DCMWriter::SaveData(const T *data)
  {
    /// Write image data with loss-less JPEG compression or without any compression
    if(sizeof(T) == 1 || sizeof(T) == 3){
      dataset->putAndInsertUint8Array(DCM_PixelData, (Uint8*)data, 
        header.size.x*header.size.y*header.size.z*sizeof(T));
    }else if(sizeof(T) == 2 || sizeof(T) == 6){
      dataset->putAndInsertUint16Array(DCM_PixelData,(Uint16*)data, 
        header.size.x*header.size.y*header.size.z*sizeof(T)/2);   
    }else{
      throw IOException("DCM: Unsupported bits per pixel.");
    }
    
    if(header.compression == true){     
      //DJDecoderRegistration::registerCodecs(EDC_photometricInterpretation,EUC_default,EPC_default,OFFalse);
      DJEncoderRegistration::registerCodecs(ECC_lossyYCbCr,EUC_default,OFFalse,OFTrue,
		0,0,0,OFTrue,ESS_444,OFFalse,OFFalse,0,0,0.0,0.0,0,0,0,0,OFTrue,OFFalse,OFFalse,OFFalse,OFTrue);
      
      DJ_RPLossless params(6,0); // codec parameters, we use the defaults

      // this causes the lossless JPEG version of the dataset to be created
      dataset->chooseRepresentation(EXS_JPEGProcess14SV1TransferSyntax, &params);
      // check if everything went well
      if (dataset->canWriteXfer(EXS_JPEGProcess14SV1TransferSyntax)){
        // store in lossless JPEG format
        dcmHeader.loadAllDataIntoMemory();
        dcmHeader.saveFile(saveName, EXS_JPEGProcess14SV1TransferSyntax, EET_ExplicitLength, EGL_recalcGL,
              EPD_noChange, 0, 0, OFFalse);
      }
      
    }else{
      dataset->chooseRepresentation(EXS_LittleEndianExplicit, NULL);
      dcmHeader.saveFile(saveName, EXS_LittleEndianExplicit);
    }
      //DJDecoderRegistration::cleanup();
      DJEncoderRegistration::cleanup(); // deregister JPEG codecs
  }

  /**************************************************************************/
  /* ~DCMWriter */
  /**************************************************************************/
  DCMWriter::~DCMWriter()
  {
    dcmHeader.clear();
  }
}

#endif
