/*
 * i3dlib - image manipulation library
 *
 * Copyright (C) 2000-2006   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 * FILE: imgfiles.cc
 *
 * I/O routines for 3D images
 *
 * Petr MejzlÌk (mejzlik@fi.muni.cz) 2000
 *
 * Petr Matula (pem@fi.muni.cz) 2001
 *   added guessing FileFormat from extension,
 *   implementation of conversion functions: StringTo*, *ToString,
 *   ExtensionToFileFormat and FileFormatToExtension
 *	
 * David Svoboda <svoboda@fi.muni.cz> 2005
 *   changes in implementation of *To* functions
 *   move the transformation functions to "transform.cc" file
 */

#ifdef __GNUG__
#pragma implementation
#endif

#include <valarray>
#include <sstream>
#include <iomanip>

#include "toolbox.h"
#include "imgfiles.h"
#include "i3dio.h"

using namespace std;

namespace i3d {

/***************************************************************************/
ImgVoxelType StringToVoxelType(const string &s)
{
  if (s == "RGB")
    return RGBVoxel;
  else if (s == "GRAY8")
    return Gray8Voxel;
  else if (s == "BINARY")
    return BinaryVoxel;
  else if (s == "GRAY16")
  	 return Gray16Voxel;
  else if (s == "RGB16")
    return RGB16Voxel;
  else if (s == "float")
	  return FloatVoxel;
  else if (s == "complex<float>")
		return Complex32Voxel;
  else if (s == "complex<double>")
		return Complex64Voxel;
  else
    return UnknownVoxel;
}

/***************************************************************************/
string VoxelTypeToString(const ImgVoxelType vt)
{
  switch (vt)
    {
    case RGBVoxel:
      return "RGB";
    case Gray8Voxel:
      return "GRAY8";
	 case BinaryVoxel:
	 	return "BINARY";
	 case Gray16Voxel:
	 	return "GRAY16";
	 case RGB16Voxel:
	   return "RGB16";
	 case FloatVoxel:
		return "float";
	 case Complex32Voxel:
		return "complex<float>";
	 case Complex64Voxel:
		return "complex<double>";
    default:
      return "Unknown";
    }
}

/***************************************************************************/
const size_t TableSize(sizeof(ImgFormat)/sizeof(ImgFormatsTable));

/***************************************************************************/
void ImageReader::ChooseChannel(int chan)
{
	channel = chan;
}

/***************************************************************************/
Vector3d<size_t> ImageReader::GetDim()
{ 
	return header.size; 
};

/***************************************************************************/
size_t ImageReader::GetNumChannels()
{ 
	return 0; 
};

/***************************************************************************/
ImageReader::~ImageReader()
{
	if (header.resolution) 
		delete header.resolution;
}

/***************************************************************************/
size_t ImageReader::GetFileNames(FileList &fl)
{
	fl.clear();
	fl.push_back(header.name);

	/// return the count of files
	return 1;
}

/***************************************************************************/
ImageWriter::~ImageWriter()
{
	if (header.resolution) 
		delete header.resolution;
}

/***************************************************************************/
void ImageWriter::SetResolution(Vector3d<float> v)
{
	if (!header.resolution)
		header.resolution = new Vector3d<float>;
	
	*(header.resolution) = v;
}

/***************************************************************************/
SequenceReader::SequenceReader( const FileList &namelist, 
		const VOI<PIXELS> *voi) : ImageReader("", voi)
{
	nlist = namelist;
}

/***************************************************************************/
size_t SequenceReader::GetFileNames(FileList &fl)
{
	fl.clear();
	fl = nlist;

	return fl.size();
}

/***************************************************************************/
SequenceReader::SequenceReader(const char *fnames, const VOI<PIXELS> *voi) : 
	ImageReader(fnames, voi)
{
  MaskExpand(fnames, nlist);
}

/***************************************************************************/
void SequenceReader::ChooseChannel(int chan)
{
	for (size_t i=0; i<nlist.size(); i++)
	{
		ireaders[i]->ChooseChannel(chan);
	}
}

/***************************************************************************/
void SequenceReader::LoadImageInfo()
{
	Vector3d<size_t> new_size, old_size;
	ImgVoxelType new_type, old_type;
	new_type = UnknownVoxel;

	/// allocate the memory
	ireaders.resize(nlist.size());
	
	for (size_t i=0; i<nlist.size(); i++)
	{
		/// read the image
		ireaders[i] = CreateReader(nlist[i].c_str(), NULL, false);
		ireaders[i]->LoadImageInfo();
		
		/// get the size
		old_size = new_size;
		new_size = ireaders[i]->GetDim();

		if ((i>0) && (new_size != old_size))
		{
			throw IOException("SequenceReader: "\
					"the images do not have the same size");
		}

		/// get the voxeltype
		old_type = new_type;
		new_type = ireaders[i]->GetVoxelType();

		if ((i>0) && (old_type != new_type))
		{
			throw IOException("SequenceReader: "\
					"the images are of different color schemes");
		}
	}

	/// set the final image size (3D)
	header.size.x = new_size.x;
	header.size.y = new_size.y;
	header.size.z = nlist.size();

	/// set the image voxel type (binary, gray8, rgb, ...)
	header.itype = new_type;
}

/***************************************************************************/

template <class T> void SequenceReader::LoadData(T *data)
{
	size_t slice_size = header.size.x * header.size.y;

	if (crop)
	{
		T *buffer = new T [slice_size];
		size_t cropped_slice_size = crop->size.x * crop->size.y;

		for (size_t z=0; z<crop->size.z; z++)
		{
			ireaders[crop->offset.z + z]->LoadImageData(buffer);

			for (size_t y=0; y<crop->size.y; y++)
				  for (size_t x=0; x<crop->size.x; x++)
						{
							size_t idx_dest = z*cropped_slice_size + 
													y*crop->size.x + x;

							size_t idx_src = (crop->offset.x + x) +
									  (crop->offset.y + y)*header.size.x;
							
							data[idx_dest] = buffer[idx_src];
						}
		}

		delete [] buffer;
	}
	else
	{
		for (size_t i=0; i<nlist.size(); i++)
		{
			/// loading slice by slice
			ireaders[i]->LoadImageData(data + i*slice_size);
		}
	}
}

/***************************************************************************/

SequenceReader::~SequenceReader()
{
	for (size_t i=0; i<nlist.size(); i++)
		DestroyReader(ireaders[i]);
}

/***************************************************************************/

SequenceWriter::SequenceWriter(const char *name_list, FileFormat ft) : 
	ImageWriter(name_list)
{
	header.name = name_list;
	this->ft = ft;

   SplitFilename (name_list, path, name, ext);

	if (ft != IMG_UNKNOWN)
      ext = FileFormatToExtension (ft);

   ext = "." + ext;
}

/***************************************************************************/

void SequenceWriter::SaveImageInfo()
{
	iwriters.resize(header.size.z);

	ostringstream os;
	os.fill('0');

	size_t num = num_digits(header.size.z);

	Vector3d<size_t> slice_size(header.size.x, header.size.y, 1);

	for (size_t i=0; i<header.size.z; i++)
	{
		os.str("");
		os << path << name;
		
		if (header.size.z > 1)
			os << "_" << setw(num) << i;

		os << ext;

//		cout << "SequenceWriter: saving: " << os.str() << endl;

		iwriters[i] = CreateWriter(os.str().c_str(), ft, slice_size);

		iwriters[i]->SetDim(slice_size);
		iwriters[i]->SetVoxelType(header.itype);
		iwriters[i]->SetCompression(header.compression);

		iwriters[i]->SaveImageInfo();
	}
}

/***************************************************************************/

template <class T> void SequenceWriter::SaveData(const T *data)
{
	size_t block_size = header.size.x * header.size.y;

	for (size_t i=0; i<header.size.z; i++)
	{
		iwriters[i]->SaveImageData(data + i*block_size);
	}
}

/***************************************************************************/

SequenceWriter::~SequenceWriter()
{
	for (size_t i=0; i<header.size.z; i++)
		DestroyWriter(iwriters[i]);
}

/***************************************************************************/
FileFormat GuessFileFormat(const char *fname)
{
  string ext;
  GetExtension(string(fname),ext);
  return ExtensionToFileFormat(ext);
}

/***************************************************************************/
string UpperCase(string s)
	{
	size_t length = s.length();

	for (size_t i=0; i<length; i++)
		s[i] = toupper(s[i]);

	return s;
	}

/***************************************************************************/
ImgVoxelType ReadImageType(const char *fname, FileFormat ft)
{
  ImageReader *ir = CreateReader(fname);

  ir->LoadImageInfo();
  ImgVoxelType itype = ir->GetVoxelType();

  DestroyReader(ir);

  return itype;
}

/***************************************************************************/
ImageHeader ReadImageHeader(const char *fname, FileFormat ft)
{
  ImageReader *ir = CreateReader(fname);

  ir->LoadImageInfo();
  ImageHeader header(ir->GetDim(), ir->GetVoxelType(), ir->GetOffset(), ir->GetResolution());

  DestroyReader(ir);

  return header;
}
  
/***************************************************************************/
FileFormat StringToFileFormat(const string &s)
	{
	for (size_t i=0; i<TableSize; i++)
		{
		if (	(ImgFormat[i].name == s) || 
				((string("IMG_") + ImgFormat[i].name) == s))
				return ImgFormat[i].type;
		}
	return IMG_UNKNOWN;
	}

/***************************************************************************/
string FileFormatToString(const FileFormat ff)
	{
	for (size_t i=0; i<TableSize; i++)
		{
		if (ImgFormat[i].type == ff)
				return ImgFormat[i].name;
		}

	return "";
	}

/***************************************************************************/
FileFormat ExtensionToFileFormat(const string &s)
	{
	for (size_t i=0; i<TableSize; i++)
		{
		if ((ImgFormat[i].ext == s) || (UpperCase(ImgFormat[i].ext) == s))
				{
				return ImgFormat[i].type;
				}
		}

	return IMG_UNKNOWN;
	}

/***************************************************************************/
string FileFormatToExtension(const FileFormat ff)
	{
	for (size_t i=0; i<TableSize; i++)
		{
		if (ImgFormat[i].type == ff)
				return ImgFormat[i].ext;
		}

	return "";
	}

/***************************************************************************/
size_t GetMaxDimensionality(const FileFormat ff)
{
	for (size_t i=0; i<TableSize; i++)
		{
		if (ImgFormat[i].type == ff)
				return ImgFormat[i].max_dim;
		}

	return 2; // implicit value
}
 
/***************************************************************************/

template void SequenceReader::LoadData(GRAY8 *data);
template void SequenceReader::LoadData(GRAY16 *data);
template void SequenceReader::LoadData(BINARY *data);
template void SequenceReader::LoadData(RGB16 *data);

template void SequenceWriter::SaveData(const GRAY8 *data);
template void SequenceWriter::SaveData(const GRAY16 *data);
template void SequenceWriter::SaveData(const BINARY *data);
template void SequenceWriter::SaveData(const RGB16 *data);


}
