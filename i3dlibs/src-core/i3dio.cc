/*
 * i3dlib - image manipulation library
 *
 * Copyright (C) 2000-2006   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** 
 * FILE: i3dio.cc
 *
 * input/output manipulation routines for variaous image formats
 * 
 * David Svoboda <svoboda@fi.muni.cz> 2005
 *
 */

#ifdef __GNUG__
#pragma implementation
#endif 

#include "i3dio.h"
#include "toolbox.h"

#ifdef _MSC_VER
  #include <io.h>
  #define access _access
#else
  #include <unistd.h> 
#endif

#ifdef CORE_WITH_BIOFORMATS
  
  #include "bioformatsreader.h"
  
  #define TRY_WITH_BIOFORMATS ir = new BioFormatsReader(name, voi); break;
#else
    #ifdef _MSC_VER
        #pragma message ("Warning: No bioformats support")
    #else
        #warning "NO BIOFORMATS SUPPORT"
    #endif
    #define TRY_WITH_BIOFORMATS
#endif

namespace i3d {

////////////////////////////////////////////////////////////////////////////
ImageReader* CreateReader (const char *fname, 
									const VOI<PIXELS> *voi, 
									bool is_regex)
{
  std::vector<std::string> namelist;

  int count = MaskExpand(fname, namelist, is_regex);

  /* if there is more than one file, use the sequence for reading 
	* the images */
  if (count > 1)
  {
	#ifdef I3D_DEBUG
	  std::cerr << "CreateReader: Regular expression: '" << fname << "'" << std::endl;
	  std::cerr << "CreateReader: Found more files. Calling 'SequenceReader'." << 
		  std::endl;
	  std::cerr << "CreateReader: List of matching files: " << std::endl;
  	  for (size_t i=0; i<namelist.size(); i++)
	  		std::cerr << " -> " << namelist[i] << std::endl;
	#endif
	  SequenceReader *sr = new SequenceReader(namelist, voi);
	  return sr;
  	}

  const char *name = namelist[0].c_str();

  // does the file exist or is readable?
  if (access(name, 4) != 0)
  {
	  throw IOException("I3D::ImageReader: file '" + namelist[0] + 
			  "' not found or cannot be read.");
  }
  
  // check out the type of read image file
  FileFormat ft = GuessFileFormat (name);
  ImageReader *ir;

  switch (ft)
    {
	 /////////////////////
    case IMG_I3D: 
		 ir = new I3DReader(name, voi); 
		 break;
	 /////////////////////
    case IMG_TIFF: 
#ifdef WITH_TIFF
		ir = new TIFFReader(name, voi);
#else
        TRY_WITH_BIOFORMATS;
        
        throw IOException("I3D::ImageReader: recompile 'i3dlib' with"\
					" flag WITH_TIFF");
#endif
		break;
	 /////////////////////
    case IMG_DCM: 
#ifdef WITH_DCM
		if (voi != NULL)
			throw InternalException("VOI support not available for DCM images.");

		ir = new DCMReader(name);
#else
        TRY_WITH_BIOFORMATS;
        
        throw IOException("I3D::ImageReader: recompile 'i3dlib' with"\
					" flag WITH_DCM");
#endif
		break;
	 /////////////////////
    case IMG_JPEG: 
#ifdef WITH_JPEG
		if (voi != NULL)
			throw InternalException("VOI support not available for JPEG images.");

		ir = new JPEGReader(name);
#else
        TRY_WITH_BIOFORMATS;
        
        throw IOException("I3D::ImageReader: recompile 'i3dlib' with" \
					" flag WITH_JPEG");
#endif
		break;
	 /////////////////////
    case IMG_TARGA: 
#ifdef WITH_TARGA
		if (voi != NULL)
			throw InternalException("VOI support not available for TARGA images.");

		ir = new TGAReader(name);
#else
		TRY_WITH_BIOFORMATS;
        
        throw IOException("I3D::ImageReader: recompile 'i3dlib' with" \
			" flag WITH_TARGA");
#endif
		break;
	 /////////////////////
    case IMG_METAIO: 
#ifdef WITH_METAIO
		if (voi != NULL)
			throw InternalException("VOI support not available for METAIO images.");
		ir = new METAIOReader(name);
#else
		TRY_WITH_BIOFORMATS;
        
		throw IOException("I3D::ImageReader: recompile 'i3dlib' with" \
			" flag WITH_METAIO");
#endif
		break;
	 /////////////////////
    case IMG_ICS: 
		#ifdef WITH_ICS
			ir = new ICSReader(name, voi);
		#else
            TRY_WITH_BIOFORMATS;
            
			throw IOException("I3D::ImageReader: recompile 'i3dlib' with" \
					" flag WITH_ICS");
		#endif
		break;
	 /////////////////////
    default:
		TRY_WITH_BIOFORMATS;
            
        throw IOException ("I3D::ImageReader: unknown image file format.");
    }

  return ir;
}

////////////////////////////////////////////////////////////////////////////
 ImageWriter* CreateWriter (const char *fname, FileFormat ft, 
		 Vector3d<size_t> sz)
  {
  if (ft == IMG_UNKNOWN)
     ft = GuessFileFormat (fname);

  if (ft == IMG_UNKNOWN)
     throw IOException ("I3D::ImageWriter: unknown image file format.");

  // test the dimensionality of requested image file format
  size_t d = GetMaxDimensionality(ft);

  /* if the image is 3D and image file format does not support more  
	* than 2D, let us use sequence writer */
  if ((sz.z > 1) && (d == 2))
  {
	  SequenceWriter *sw = new SequenceWriter(fname, ft);
	  return sw;
  }

  ImageWriter *iw;

  switch (ft)
    {
	 /////////////////////
    case IMG_I3D: 
		 iw = new I3DWriter (fname); 
		 break;
	 /////////////////////
    case IMG_TIFF: 
		#ifdef WITH_TIFF
			iw = new TIFFWriter (fname);
		#else
		 	throw IOException("I3D::ImageWriter: recompile 'i3dlib' with" \
					" flag WITH_TIFF");
		#endif
		break;
	 /////////////////////
    case IMG_DCM: 
		#ifdef WITH_DCM
			iw = new DCMWriter (fname);
		#else
		 	throw IOException("I3D::ImageWriter: recompile 'i3dlib' with" \
					" flag WITH_DCM");
		#endif
		break;
	 /////////////////////
    case IMG_JPEG: 
		#ifdef WITH_JPEG
			iw = new JPEGWriter (fname); 
		#else
			throw IOException("I3D::ImageWriter: recompile 'i3dlib' with" \
					" flag WITH_JPEG");
		#endif
		break;
	 /////////////////////
    case IMG_TARGA: 
		#ifdef WITH_TARGA
			iw = new TGAWriter (fname);
		#else
			throw IOException("I3D::ImageWriter: recompile 'i3dlib' with" \
					" flag WITH_TARGA");
		#endif
		break;
	 /////////////////////
    case IMG_METAIO: 
		#ifdef WITH_METAIO
			iw = new METAIOWriter(fname);
		#else
			throw IOException("I3D::ImageWriter: recompile 'i3dlib' with" \
					" flag WITH_METAIO");
		#endif
		break;
	 /////////////////////
    case IMG_ICS: 
		#ifdef WITH_ICS
			iw = new ICSWriter(fname);
		#else
			throw IOException("I3D::ImageWriter: recompile 'i3dlib' with" \
					" flag WITH_ICS");
		#endif
		break;
	 /////////////////////
    default:
        throw IOException ("I3D::ImageWriter: unknown image file format.");
	 }

   return iw;
  }

////////////////////////////////////////////////////////////////////////////
void DestroyWriter(ImageWriter *iw)
{
	 if (iw)
		delete iw;
}

////////////////////////////////////////////////////////////////////////////
void DestroyReader(ImageReader *ir)
{
	 if (ir)
		delete ir;
}
	
////////////////////////////////////////////////////////////////////////////

}
