/*
 * i3dlib - image manipulation library
 *
 * Copyright (C) 2000-2006   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** \file image3d.cc
  \brief 3D image class and manipulation routines - implementation.

  This file contains bodies of members of the main Image3d template and 
  closely related functions. 
 
  \author Petr Matula (pem@fi.muni.cz) 2000
  \author Petr Mejzl�k (mejzlik@fi.muni.cz) 2000
  \author Pavel Matula (pam@fi.muni.cz) 2001
*/

#include <string>
#include <typeinfo>
#include <algorithm>
#include <cstdio>

#include <climits>
#include <fstream>

#ifdef __GNUG__
#pragma implementation
#endif

#include <algorithm>

#include "image3d.h"
#include "toolbox.h"
#include "i3dio.h"

using namespace std;

namespace i3d
{
	/*****************************************************************************\
	*
	*                            Local functions
	*
	\******************************************************************************/
	std::gslice VOItoGslice (
		const VOI <PIXELS> &v,
		const Vector3d < size_t > &s)
	{
		size_t len[3];
		size_t str[3];
		size_t start = GetIndex (v.offset, s);
		len[0] = v.size.x;
		len[1] = v.size.y;
		len[2] = v.size.z;
		str[0] = 1;
		str[1] = s.x;
		str[2] = s.x * s.y;
		size_t dim = 3;

		// Lower the dimension of the gslice, if possible:

		// 1. Put adjacent continuous blocks together:

		if (v.offset.x == 0 && 
			v.offset.y == 0 && 
			v.size.x == s.x && 
			v.size.y == s.y) 
		{ // full slices
			len[0] = s.x * s.y * v.size.z;
			dim = 1;
		} else 
		{
			if ((v.offset.x == 0) && (v.size.x == s.x)) 
			{ // full columns
				len[0] = s.x * v.size.y;
				len[1] = len[2];
				str[1] = str[2];
				dim = 2;
			}

			// 2. Delete collapsed dimensions:

			if (dim == 3 && len[2] == 1) {
				dim = 2;
			}
			if (dim >= 2 && len[1] == 1) {
				if (dim == 3) {
					len[1] = len[2];
					str[1] = str[2];
				}
				dim--;
			}

			if (dim >= 2 && len[0] == 1) {
				len[0] = len[1];
				str[0] = str[1];
				if (dim == 3) {
					len[1] = len[2];
					str[1] = str[2];
				}
				dim--;
			}
		}                  

		std::valarray < size_t > lengths (len, dim);
		std::valarray < size_t > strides (str, dim);

		return std::gslice (start, lengths, strides);
	} // VOItoGslice


	/*****************************************************************************\
	*
	*                              3d Image
	*
	\******************************************************************************/
	//--------------------------------------------------------------------------
	// Constructors
	//--------------------------------------------------------------------------
	template < class T > Image3d < T >::Image3d (
		const char *fname,
		const VOI<PIXELS> *voi,
		bool is_regex,
		int channel,
		const Offset *off_,
		const Resolution *res_)
		:	offset (), size (0), resolution (), capacity (0), data ()
	{
		ReadImage (fname, voi, is_regex, channel, off_, res_);
	}

	template < class T > void Image3d < T >::ReadImage (
		const char *fname,
		const VOI<PIXELS> *voi,
		bool is_regex,
		int channel,
		const Offset *off_,
		const Resolution *res_)
	{

#		ifdef I3D_DEBUG
		cerr << "Trying to open the image: " << fname << endl;
#		endif

		/// open image 
		ImageReader *ir = CreateReader(fname, voi, is_regex);

#		ifdef I3D_DEBUG
		cerr << "Reading image header." << endl;
#		endif

		/// read the basic image info to the predefined structures
		ir->LoadImageInfo ();

		/// verify if the required VOI is included in the image
		if (voi)
		{
			if ((voi->offset.x + voi->size.x > ir->GetDim().x) ||
				(voi->offset.y + voi->size.y > ir->GetDim().y) ||
				(voi->offset.z + voi->size.z > ir->GetDim().z))
			{
				string msg;
				msg += string ("Image3d<") + typeid (T).name () + ">::ReadImage ";
				msg += "Bad VOI size";
				throw IOException (msg);
			}

			size = voi->size;
		} else {
			/// fetch the image size 
			size = ir->GetDim ();
		}

		/** choose the channel to read
		* if the default value is set (-1), then all the channels are read */
		ir->ChooseChannel(channel);

		/** test if image file is in the same colorspace as the prepared 
		* image structure */
		if (!TestConsistency (ir->GetVoxelType())) {
			string msg;
			msg += string ("Image3d<") + typeid (T).name () + ">::ReadImage ";
			msg += "Incompatible input file type (Voxel type = ";
			switch (ir->GetVoxelType()) {
				case BinaryVoxel: 
					msg += "Binary";
					break;
				case Gray8Voxel: 
					msg += "GRAY8";
					break;
				case Gray16Voxel:
					msg += "GRAY16";
					break;
				case RGBVoxel:
					msg += "RGB8";
					break;
				case RGB16Voxel:
					msg += "RGB16";
					break;
				case FloatVoxel:
					msg += "Float";
					break;
				case DoubleVoxel:
					msg += "Double";
					break;
				case Complex32Voxel:
					msg += "Complex<float>";
					break;
				case Complex64Voxel:
					msg += "Complex<double>";
					break;
				default:
					msg += itos(ir->GetVoxelType());
					break;
			}
			msg += ")";
			throw IOException (msg);
		}

		/// test whether the resolution is defined by the user
		if (res_) {
			resolution = *res_;
		}
		else {
			/// test whether the resolution is defined within the image format
			Vector3d<float> *v;

			if ((v = ir->GetResolution()) != NULL)
				resolution = Resolution(*v);
		}

		/// set some image characteristics, only if requested:
		/// set the image offset (if set)
		if (off_) {
			offset = *off_;
		} else {
            offset = ir->GetOffset();
            
            // calculate correct offset when voi is loaded
            if (voi)
            {
                offset += PixelsToMicrons (voi->offset, resolution);
            }
		}

		/// allocate the memory
		MakeRoom (size.x, size.y, size.z);

#		ifdef I3D_DEBUG
		cerr << "Reading image data." << endl;
#		endif

		/// read the image data
		ir->LoadImageData (&data[0]);

		DestroyReader(ir);

#		ifdef I3D_DEBUG
		cerr << "Image successfully read." << endl;
#		endif

	} // Image3d

	//--------------------------------------------------------------------------
	// Copy constructors
	//--------------------------------------------------------------------------
	template < class T > Image3d < T >::Image3d (
		const Image3d < T > &src,
		const VOI<PIXELS> *voi)
		: offset (), size (),
		resolution (), capacity (0), data ()
	{
		if (voi == 0) {
			*this = src;
		}
		else {
			CopyFromVOI (src, *voi);
		}
	}

	template < class T > Image3d < T >::Image3d (
		const Image3d < T > &src,
		const VOI<MICRONS> &voi)
		: offset (),	size(), resolution(), capacity(0), data()
	{
		VOI<MICRONS> is = voi;
		is *= src.GetVOI ();

		VOI<PIXELS> v;
		v.offset =
			MicronsToPixels (is.offset - src.GetOffset(), src.GetResolution ());
		v.size = MicronsToPixels (is.size, src.GetResolution ());

		if (v.size.x == 0 || v.size.y == 0 || v.size.z == 0)
		{
#ifdef I3D_DEBUG
			cerr << "Warning: No voxel in intersection" << endl;
#endif
			return;
		}

        CopyFromVOI (src, v);
	}

	//--------------------------------------------------------------------------
	// Asignment operator
	//--------------------------------------------------------------------------
	template < class T > Image3d < T > &Image3d < T >::operator =
		(const Image3d < T > &src)
	{
		if (this != &src)
		{                         // not a self-assignment
			offset = src.offset;
			size = src.size;
			resolution = src.resolution;
			data.resize (src.data.size ());
			data = src.data;
			capacity = src.capacity;
		}
		return *this;
	}

	//--------------------------------------------------------------------------
	// CopyFromVOI
	//--------------------------------------------------------------------------
	template < class T > void Image3d < T >::CopyFromVOI (
		const Image3d < T > &src,
		const VOI <PIXELS> &voi,
		bool overrun,
		T padding_value)
	{
		 // Is it allowed to have VOI that reaches the area beyond the input 
		 // image domain?
		if (overrun)
		{
			 // allocate the memory
			 MakeRoom (voi.size);

			 // fill the image with the predefined value - implicitly '0'
			 for (size_t i=0; i<GetImageSize(); i++)
				  SetVoxel(i, padding_value);
		}
		else // the VOI must be inside the observed image
		{
			if (voi.offset.x + voi.size.x > src.size.x ||
				 voi.offset.y + voi.size.y > src.size.y ||
				 voi.offset.z + voi.size.z > src.size.z) 
			{
				throw InternalException ("CopyFromVOI: "
												 "VOI is not inside source image.");
			}

			// allocate the memory
			MakeRoom (voi.size);
		} 

		// set the resolution and offset
		resolution = src.resolution;
		offset = src.offset + PixelsToMicrons (voi.offset, resolution);

		// find out, what is the offset and size of the memory block 
		// that shall be copied from the input image
		VOI<PIXELS> 
				  src_voi(0, src.size), 
				  block = voi * src_voi;

		// find out the starting (x,y,z) position in the new image where the
		// data shall be copied to. If VOI is completely inside the input image
		// this shift is (0,0,0). Otherwise, this shift may vary.
		Vector3d<int> dest_offset = abs(min(voi.offset,Vector3d<int>(0)));
			 
		for (size_t z = 0; z < block.size.z; z++)
		{
			  for (size_t y = 0; y < block.size.y; y++)
			  {
					const T *data_from = src.GetVoxelAddr(block.offset.x, 
																	  block.offset.y + y, 
																	  block.offset.z + z);
					T *data_to = GetVoxelAddr(dest_offset.x,
													  dest_offset.y + y,
													  dest_offset.z + z);
				
					// copy the data block
					memcpy(data_to, data_from, block.size.x * sizeof(T));
			  }
		}
	} // CopyFromVOI 

	//--------------------------------------------------------------------------
	// SaveImage
	//--------------------------------------------------------------------------
	template < class T > void Image3d < T >::SaveImage (
		const char *fname,
		FileFormat ft,
		bool comp) const
	{
		ImageWriter *iw = NULL;

#		ifdef I3D_DEBUG
		cerr << "Saving image: " << fname << endl;
#		endif

		try
		{
			/// create image writer
			iw = CreateWriter(fname, ft, GetSize());

#			ifdef I3D_DEBUG
			cerr << "Saving image header." << endl;
#			endif

			/// set the image dimensions
			iw->SetDim(GetSize());

			/// set the image voxel type (binary, gray8, rgb, ...)
			iw->SetVoxelType(ConsistentVoxelType ());

			/// set the image resolution (if it is even used)
			if (GetResolution().IsDefined()) 
			{
				iw->SetResolution(GetResolution().GetRes());
			}

			/// set the referred image compression
			iw->SetCompression(comp);

			/// set the image offset
			iw->SetOffset(GetOffset());

			/// save header info
			iw->SaveImageInfo ();

#			ifdef I3D_DEBUG
			cerr << "Saving image data." << endl;
#			endif

			/// save the data
			iw->SaveImageData (GetFirstVoxelAddr());
		}
		catch(...) // destroy the image writer data in case of an exception
		{
			DestroyWriter(iw);
			throw; //rethrow
		}

		DestroyWriter(iw);

#		ifdef I3D_DEBUG
		cerr << "Image saved successfully." << endl;
#		endif
	} // SaveImage

	//--------------------------------------------------------------------------
	// Flip image
	//--------------------------------------------------------------------------
	template <class VOXEL> void Image3d<VOXEL>::Flip(bool xaxis, bool yaxis, bool zaxis)
	{
	    size_t sz = GetSliceSize();

		 // flip x-axis
		 if (xaxis)
		 {
			  for (size_t z=0; z<size.z; z++)
			  {
					SwapHoriz(size.x, size.y, &data[z*sz]);
			  }
		 }

		 // flip y-axis
		 if (yaxis)
		 {
			  for (size_t z=0; z<size.z; z++)
			  {
					SwapVert(size.x, size.y, &data[z*sz]);
			  }
		 }

		 // flip z-axis
		 if (zaxis)
		 {
			  size_t buf_size = sizeof(VOXEL) * sz;
			  VOXEL *buf = new VOXEL[GetSliceSize()];
			  for (size_t z=0; z<size.z/2; z++)
			  {
					// swap slices along z-axis
					memcpy(buf, &data[z*sz], buf_size);
					memcpy(&data[z*sz], &data[(size.z-z-1)*sz], buf_size);
					memcpy(&data[(size.z-z-1)*sz], buf, buf_size);
			  }
			  delete [] buf;

		 }

	}
	
//--------------------------------------------------------------------------
// GetRange
//--------------------------------------------------------------------------
template <class VOXEL> void Image3d<VOXEL>::GetRange(VOXEL & min, VOXEL & max) const
	{
        size_t imsz = GetImageSize();

        if (imsz > 0)
        {
            const VOXEL *ptr = GetFirstVoxelAddr();
            min = *ptr;
            max = *ptr;

            while (--imsz > 0)
            {
                ++ptr;

                if (*ptr < min)
                {
                    min = *ptr;
                }
                else if (*ptr > max)
                {
                    max = *ptr;
                }
            }
        }
	}

template <> void Image3d<std::complex<float>/**/>::
		  GetRange(std::complex<float>& min, 
					  std::complex<float>& max) const
	{
		const std::complex<float> *ptr, *last;
		min = std::complex<float>(std::numeric_limits<float>::max(),
										  std::numeric_limits<float>::max());
		max = 0;
		last = GetVoxelAddr(GetSizeX()*GetSizeY()*GetSizeZ());
		ptr = GetVoxelAddr (0);
		while (ptr != last)
		{
			if (std::abs(*ptr) < std::abs(min)) 
			{
				min = *ptr;
			}
			if (std::abs(*ptr) > std::abs(max)) 
			{
				max = *ptr;
			}
			ptr++;
		}
	}

template <> void Image3d<std::complex<double>/**/>::
		  GetRange(std::complex<double>& min, 
					  std::complex<double> & max) const
	{
		const std::complex<double> *ptr, *last;
		min = std::complex<double>(std::numeric_limits<double>::max(),
											std::numeric_limits<double>::max());
		max = 0;
		last = GetVoxelAddr(GetSizeX()*GetSizeY()*GetSizeZ());
		ptr = GetVoxelAddr (0);
		while (ptr != last)
		{
			if (std::abs(*ptr) < std::abs(min)) 
			{
				min = *ptr;
			}
			if (std::abs(*ptr) > std::abs(max)) 
			{
				max = *ptr;
			}
			ptr++;
		}
	}

template <> void Image3d<RGB>::GetRange(RGB & min, RGB & max) const 
	{
		const RGB * ptr, * last;
		min.red = min.blue = min.green = std::numeric_limits<GRAY8>::max();
		max.red = max.blue = max.green = std::numeric_limits<GRAY8>::min();
		last = GetVoxelAddr(GetSizeX()*GetSizeY()*GetSizeZ());
		ptr = GetVoxelAddr (0);
		while (ptr != last)
		{
			if ((*ptr).red < min.red)
				min.red = (*ptr).red;
			if ((*ptr).red > max.red)
				max.blue = (*ptr).blue;
			if ((*ptr).blue < min.blue)
				min.blue = (*ptr).blue;
			if ((*ptr).blue > max.blue)
				max.blue = (*ptr).blue;
			if ((*ptr).green < min.green)
				min.green = (*ptr).green;
			if ((*ptr).green > max.green)
				max.green = (*ptr).green;

			ptr++;
		}
	}

template <> void Image3d<RGB16>::GetRange(RGB16 & min, RGB16 & max) const
	{
		const RGB16 * ptr, * last;
		min.red = min.blue = min.green = std::numeric_limits<GRAY16>::max();
		max.red = max.blue = max.green = std::numeric_limits<GRAY16>::min();
		last = GetVoxelAddr(GetSizeX()*GetSizeY()*GetSizeZ());
		ptr = GetVoxelAddr (0);
		while (ptr != last)
		{
			if ((*ptr).red < min.red)
				min.red = (*ptr).red;
			if ((*ptr).red > max.red)
				max.blue = (*ptr).blue;
			if ((*ptr).blue < min.blue)
				min.blue = (*ptr).blue;
			if ((*ptr).blue > max.blue)
				max.blue = (*ptr).blue;
			if ((*ptr).green < min.green)
				min.green = (*ptr).green;
			if ((*ptr).green > max.green)
				max.green = (*ptr).green;

			ptr++;
		}
	} // GetRange


//--------------------------------------------------------------------------
// GetMaxValue
//--------------------------------------------------------------------------
template <class VOXEL> VOXEL Image3d<VOXEL>::GetMaxValue(size_t &index) const
	{
		const VOXEL * ptr, * last, *ptr_max;
		VOXEL max = std::numeric_limits<VOXEL>::min();
		last = GetVoxelAddr(GetSizeX()*GetSizeY()*GetSizeZ());
		ptr_max = ptr = GetVoxelAddr (0);
		
		while (ptr != last)
		{
			if (*ptr > max) {
				max = *ptr;
				ptr_max = ptr;
			}
			ptr++;
		}

		index = ptr_max - GetVoxelAddr (0);

		return max;
	}

template <class VOXEL> VOXEL Image3d<VOXEL>::GetMaxValue() const
{
	if (data.size() > 0) 
		return data.max();
	else {
		return std::numeric_limits<VOXEL>::max();
	}
}

void NOTIMPLFORCOMPLEX(void) {
	throw i3d::InternalException("Function not implemented for complex voxels.");
}

template <> std::complex<float> Image3d<std::complex<float> >::GetMaxValue(size_t &index) const
{
	NOTIMPLFORCOMPLEX();

	return std::complex<float>();
}

template <> std::complex<double> Image3d<std::complex<double> >::GetMaxValue(size_t &index) const
{
	NOTIMPLFORCOMPLEX();

	return std::complex<float>();
}

template <> std::complex<float> Image3d<std::complex<float> >::GetMaxValue() const
{
	NOTIMPLFORCOMPLEX();

	return std::complex<float>();
}

template <> std::complex<double> Image3d<std::complex<double> >::GetMaxValue() const
{
	NOTIMPLFORCOMPLEX();

	return std::complex<float>();
}

//--------------------------------------------------------------------------
// GetKPercentileValue
//--------------------------------------------------------------------------

template <class VOXEL> VOXEL Image3d<VOXEL>::GetKPercentileValue(float k) const
{
	std::valarray<VOXEL> copy(data);
	std::sort(&copy[0], &copy[data.size()]);

	return copy[(size_t)(data.size()*(k / 100.0f))];
}

template <> std::complex<float> Image3d<std::complex<float> >::GetKPercentileValue(float k) const
{
	NOTIMPLFORCOMPLEX();

	return std::complex<float>();
}

template <> std::complex<double> Image3d<std::complex<double> >::GetKPercentileValue(float k) const
{
	NOTIMPLFORCOMPLEX();

	return std::complex<float>();
}

//--------------------------------------------------------------------------
// SetBorderVoxels
//--------------------------------------------------------------------------
template <class VOXEL> void Image3d<VOXEL>::SetBorderVoxels(VOXEL voxel)
{
	if (GetSizeZ() > 1) {
		for (size_t y = 0; y < GetSizeY(); ++y) {
			for (size_t x = 0; x < GetSizeX(); ++x) {
				SetVoxel(x, y, 0, voxel);
				SetVoxel(x, y, GetSizeZ() - 1, voxel);
			}
		}
	}

	for (size_t z = 0; z < GetSizeZ(); ++z) {
		for (size_t y = 0; y < GetSizeY(); ++y) {
			SetVoxel(0, y, z, voxel);
			SetVoxel(GetSizeX() - 1, y, z, voxel);
		}
		for (size_t x = 0; x < GetSizeX(); ++x) {
			SetVoxel(x, 0, z, voxel);
			SetVoxel(x, GetSizeY() - 1, z, voxel);
		}
	}
}

//--------------------------------------------------------------------------
// GetMinValue
//--------------------------------------------------------------------------
template <class VOXEL> VOXEL Image3d<VOXEL>::GetMinValue(size_t &index) const
	{
		const VOXEL * ptr, * last, *ptr_min;
		VOXEL min = std::numeric_limits<VOXEL>::max();
		last = GetVoxelAddr(GetSizeX()*GetSizeY()*GetSizeZ());

		ptr_min = ptr = GetVoxelAddr (0);
		
		while (ptr != last)
		{
			if (*ptr < min) {
				min = *ptr;
				ptr_min = ptr;
			}
			ptr++;
		}

		index = ptr_min - GetVoxelAddr (0);

		return min;
	}

template <class VOXEL> VOXEL Image3d<VOXEL>::GetMinValue() const
{
	if (data.size() > 0) 
		return data.min();
	else {
		return std::numeric_limits<VOXEL>::min();
	}
}

template <> std::complex<float> Image3d<std::complex<float> >::GetMinValue(size_t &index) const
{
	NOTIMPLFORCOMPLEX();

	return std::complex<float>();
}

template <> std::complex<double> Image3d<std::complex<double> >::GetMinValue(size_t &index) const
{
	NOTIMPLFORCOMPLEX();

	return std::complex<float>();
}

template <> std::complex<float> Image3d<std::complex<float> >::GetMinValue() const
{
	NOTIMPLFORCOMPLEX();

	return std::complex<float>();
}

template <> std::complex<double> Image3d<std::complex<double> >::GetMinValue() const
{
	NOTIMPLFORCOMPLEX();

	return std::complex<float>();
}


//--------------------------------------------------------------------------
// PadImage
//--------------------------------------------------------------------------

template <class T> void Image3d <T>::PadImage(i3d::Vector3d<size_t> sz, T value)
{
	size_t dx = this->GetSizeX();
	size_t dy = this->GetSizeY();
	size_t dz = this->GetSizeZ();
	size_t new_dx = dx + 2 * sz.x;
	size_t new_dy = dy + 2 * sz.y;
	size_t new_dz = dz + 2 * sz.z;

	std::valarray<T> new_data(value, new_dx * new_dy * new_dz);

	size_t x, y, z;
	T *p, *q;

	p = &data[0];
	q = &new_data[0];

	q += new_dx * new_dy * sz.z; z = 0;
	while (z < dz) {
		q += new_dx * sz.y; y = 0;
		while (y < dy) {
			q += sz.x; x = 0;
			while (x < dx) {
				*q = *p;
				++p; ++q; ++x;			
			}
			q += sz.x; ++y;
		}
		q += new_dx * sz.y; ++z;
	}

	data = new_data;

	SetOffset(GetOffset() - PixelsToMicrons(i3d::Vector3d<float>(sz.x, sz.y, sz.z), GetResolution()));

	size.x = new_dx;
	size.y = new_dy;
	size.z = new_dz;
}

//--------------------------------------------------------------------------
// PadImage
//--------------------------------------------------------------------------

template <class T> void Image3d <T>::RemovePadding(i3d::Vector3d<size_t> sz)
{
	size_t dx = this->GetSizeX();
	size_t dy = this->GetSizeY();
	size_t dz = this->GetSizeZ();
	size_t new_dx = dx - 2 * sz.x;
	size_t new_dy = dy - 2 * sz.y;
	size_t new_dz = dz - 2 * sz.z;
	std::valarray<T> new_data(new_dx * new_dy * new_dz);

	size_t x, y, z;

	T *p, *q;

	p = &data[0];
	q = &new_data[0];

	p += dx * dy * sz.z; z = 0;
	while (z < new_dz) {
		p += dx * sz.y; y = 0;
		while (y < new_dy) {
			p += sz.x; x = 0;
			while (x < new_dx) {
				*q = *p;
				++p; ++q; ++x;
			}
			p += sz.x; ++y;
		}
		p += dx * sz.y; ++z;
	}

	data = new_data;

	SetOffset(GetOffset() + PixelsToMicrons(i3d::Vector3d<float>(sz.x, sz.y, sz.z), GetResolution()));

	size.x = new_dx;
	size.y = new_dy;
	size.z = new_dz;
}

	//--------------------------------------------------------------------------
	// TestConsistency
	//--------------------------------------------------------------------------
	template < class T > bool Image3d <	T >::
		TestConsistency (ImgVoxelType itype) const
	{
		switch (itype)
		{
		case RGBVoxel:
			return typeid (T) == typeid (RGB);
		case Gray8Voxel:
			return typeid (T) == typeid (GRAY8);
		case BinaryVoxel:
			return typeid (T) == typeid (BINARY);
		case Gray16Voxel:
			return typeid (T) == typeid (GRAY16);
		case RGB16Voxel:
			return typeid (T) == typeid (RGB16);
		case FloatVoxel:
			return typeid (T) == typeid (float);
		case DoubleVoxel:
			return typeid (T) == typeid (double);
		case VectFloatVoxel:
			return typeid (T) == typeid (Vector3d<float>);
		case VectDoubleVoxel:
			return typeid (T) == typeid (Vector3d<double>);
		case Complex32Voxel:
			return typeid (T) == typeid (std::complex<float>);
		case Complex64Voxel:
			return typeid (T) == typeid (std::complex<double>);
		default:
			return false;
		}
	} // TestConsistency

	//--------------------------------------------------------------------------
	// ConsistentVoxelType 
	//--------------------------------------------------------------------------
	template < class T > ImgVoxelType Image3d < T >::
		ConsistentVoxelType () const
	{
		if (typeid (T) == typeid (RGB))	
			return RGBVoxel;
		else if (typeid (T) == typeid (GRAY8))
			return Gray8Voxel;
		else if (typeid (T) == typeid (BINARY))
			return BinaryVoxel;
		else if (typeid (T) == typeid (GRAY16))
			return Gray16Voxel;
		else if (typeid (T) == typeid (RGB16))
			return RGB16Voxel;
		else if (typeid (T) == typeid (float))
			return FloatVoxel;
		else if (typeid (T) == typeid (double))
			return DoubleVoxel;
		else if (typeid (T) == typeid (Vector3d<float>))
			return VectFloatVoxel;
		else if (typeid (T) == typeid (Vector3d<double>))
			return VectDoubleVoxel;
		else if (typeid (T) == typeid (std::complex<float>))
			return Complex32Voxel;
		else if (typeid (T) == typeid (std::complex<double>))
			return Complex64Voxel;
		else
			throw IOException ("Image3d::ConsistentVoxelType: "
			"unknown voxel type.");
	} // ConsistentVoxelType 

	//--------------------------------------------------------------------------
	// Explicit instantiations
	//--------------------------------------------------------------------------
	template class CORE_I3D_DLLEXPORT Image3d < int > ;
	template class CORE_I3D_DLLEXPORT Image3d < size_t > ;
	template class CORE_I3D_DLLEXPORT Image3d < float >;
	template class CORE_I3D_DLLEXPORT Image3d < double >;
	template class CORE_I3D_DLLEXPORT Image3d < RGB >;
	template class CORE_I3D_DLLEXPORT Image3d < GRAY8 >;
	template class CORE_I3D_DLLEXPORT Image3d < BINARY >;
	template class CORE_I3D_DLLEXPORT Image3d < GRAY16 >;
	template class CORE_I3D_DLLEXPORT Image3d < RGB16 >;
	template class CORE_I3D_DLLEXPORT Image3d < complex<float>/**/>;
	template class CORE_I3D_DLLEXPORT Image3d < complex<double>/**/>;
	template class CORE_I3D_DLLEXPORT Image3d < Vector3d <float> >;
	template class CORE_I3D_DLLEXPORT Image3d < Vector3d <double> >;

	/***************************************************************************\
	*
	*                         Conversion functions
	*
	\***************************************************************************/
	//--------------------------------------------------------------------------
	// CopyChannel
	//--------------------------------------------------------------------------
	template < class FULL_COLOR, class GRAY_CHANNEL > bool CopyChannel (
		const Image3d < FULL_COLOR > &rgb,
		Image3d < GRAY_CHANNEL > &gray, 
		int channel)
	{
		GRAY_CHANNEL *rgbdata, *graydata;
		size_t voxels;

		if (channel < 0 || channel > 2)
			return false;
		gray.SetOffset (rgb.GetOffset ());
		gray.SetResolution (rgb.GetResolution ());
		gray.MakeRoom (rgb.GetWidth (), rgb.GetHeight (), rgb.GetNumSlices ());
		rgbdata = (GRAY_CHANNEL *) rgb.GetFirstVoxelAddr ();
		rgbdata += channel;
		graydata = gray.GetFirstVoxelAddr ();
		voxels = rgb.GetImageSize ();

		for (size_t i = 0; i < voxels; i++)
		{
			*graydata = *rgbdata;
			rgbdata += 3;
			graydata++;
		}

		return true;
	}

	//--------------------------------------------------------------------------
	// CopyChannelBack
	//--------------------------------------------------------------------------
	template < class GRAY_CHANNEL, class FULL_COLOR > bool CopyChannelBack ( 
		const Image3d < GRAY_CHANNEL > &gray,
		Image3d < FULL_COLOR > &rgb,
		int channel)
	{
		GRAY_CHANNEL *rgbdata;
        const GRAY_CHANNEL *graydata;
		size_t voxels;

		if (channel < 0 || channel > 2)
			return false;

		if ( rgb.GetImageSize() != gray.GetImageSize())
			return false;

		rgbdata = (GRAY_CHANNEL *) rgb.GetFirstVoxelAddr ();
		rgbdata += channel;
		graydata = gray.GetFirstVoxelAddr ();
		voxels = rgb.GetImageSize ();

		for (size_t i = 0; i < voxels; i++)
		{
			*rgbdata = *graydata;
			rgbdata += 3;
			++graydata;
		}

		return true;
	}

	//--------------------------------------------------------------------------
	// FloatToGray
	//--------------------------------------------------------------------------
	template <class VOXELIN, class VOXELOUT> void FloatToGray (const Image3d < VOXELIN > &fimg, Image3d < VOXELOUT > &gimg)
	{
		VOXELIN fmax = std::numeric_limits<VOXELIN>::min(),
			fmin = std::numeric_limits<VOXELIN>::max(), fvalue;

		size_t i;

		gimg.MakeRoom (fimg.GetSize ());
		gimg.SetOffset (fimg.GetOffset ());
		gimg.SetResolution (fimg.GetResolution ());

		for (i = 0; i < fimg.GetImageSize (); i++)
		{
			fvalue = fimg.GetVoxel (i);
			fmax = (fmax < fvalue) ? fvalue : fmax;
			fmin = (fmin > fvalue) ? fvalue : fmin;
		}

		VOXELIN diff = fmax - fmin;
		VOXELOUT gvalue, gmax = std::numeric_limits < VOXELOUT >::max ();


		for (i = 0; i < fimg.GetImageSize (); i++)
		{
			fvalue = fimg.GetVoxel (i);
			gvalue = (VOXELOUT)(gmax * ((fvalue - fmin) / diff));

			gimg.SetVoxel (i, gvalue);
		}
	}

	//--------------------------------------------------------------------------
	// FloatToGrayNoWeight
	//--------------------------------------------------------------------------
	template <class VOXELIN, class VOXELOUT> void FloatToGrayNoWeight (const Image3d < VOXELIN > &fimg, Image3d < VOXELOUT > &gimg)
	{
		VOXELOUT gmax = std::numeric_limits<VOXELOUT>::max(),
			gmin = std::numeric_limits<VOXELOUT>::min(), gvalue;
		VOXELIN fvalue, fmin = (VOXELIN) gmin, fmax = (VOXELIN) gmax;

		size_t i;

		gimg.MakeRoom (fimg.GetSize ());
		gimg.SetOffset (fimg.GetOffset ());
		gimg.SetResolution (fimg.GetResolution ());

		for (i = 0; i < fimg.GetImageSize (); i++)
		{
			fvalue = fimg.GetVoxel (i);
			if (fvalue < fmin)
				gvalue = gmin;
			else
				if (fvalue > fmax)
					gvalue = gmax;
				else
					gvalue = (VOXELOUT) fvalue;
			gimg.SetVoxel (i, gvalue);
		}
	}



	//--------------------------------------------------------------------------
	// GrayToFloat
	//--------------------------------------------------------------------------
	template <class VOXELIN, class VOXELOUT> void GrayToFloat (const Image3d <VOXELIN> &gimg, Image3d < VOXELOUT > &fimg)
	{
		fimg.MakeRoom (gimg.GetSize ());
		fimg.SetOffset (gimg.GetOffset ());
		fimg.SetResolution (gimg.GetResolution ());

		for (size_t i=0; i<gimg.GetImageSize(); i++) {
			fimg.SetVoxel(i, (VOXELOUT)(gimg.GetVoxel(i)));
		}
	}

	//--------------------------------------------------------------------------
	// Explicit instantiations
	//--------------------------------------------------------------------------
	template  CORE_I3D_DLLEXPORT void FloatToGray (const Image3d < float > &fimg, Image3d < GRAY8 > &gimg);
	template  CORE_I3D_DLLEXPORT void FloatToGray (const Image3d < float > &fimg, Image3d < GRAY16 > &gimg);
	template  CORE_I3D_DLLEXPORT void FloatToGray (const Image3d < double > &fimg, Image3d < GRAY8 > &gimg);
	template  CORE_I3D_DLLEXPORT void FloatToGray (const Image3d < double > &fimg, Image3d < GRAY16 > &gimg);
	
	template  CORE_I3D_DLLEXPORT void FloatToGrayNoWeight (const Image3d < float > &fimg, Image3d < GRAY8 > &gimg);
	template  CORE_I3D_DLLEXPORT void FloatToGrayNoWeight (const Image3d < float > &fimg, Image3d < GRAY16 > &gimg);
	template  CORE_I3D_DLLEXPORT void FloatToGrayNoWeight (const Image3d < double > &fimg, Image3d < GRAY8 > &gimg);
	template  CORE_I3D_DLLEXPORT void FloatToGrayNoWeight (const Image3d < double > &fimg, Image3d < GRAY16 > &gimg);
	
	template  CORE_I3D_DLLEXPORT void GrayToFloat (const Image3d <GRAY8> &gimg, Image3d < float > &fimg);
	template  CORE_I3D_DLLEXPORT void GrayToFloat (const Image3d <GRAY8> &gimg, Image3d < double > &fimg);
	template  CORE_I3D_DLLEXPORT void GrayToFloat (const Image3d <GRAY16> &gimg, Image3d < float > &fimg);
	template  CORE_I3D_DLLEXPORT void GrayToFloat (const Image3d <GRAY16> &gimg, Image3d < double > &fimg);


	//--------------------------------------------------------------------------
	// BinaryToGray and back
	//--------------------------------------------------------------------------

	template <class VOXEL> 
	void BinaryToGray(const Image3d<BINARY> &bimg, Image3d<VOXEL> &gimg)
	{
		gimg.CopyMetaData(bimg);

		for (size_t i = 0; i < bimg.GetImageSize (); ++i)
			gimg.SetVoxel(i, bimg.GetVoxel(i));
	}

	template <class VOXEL> 
	void GrayToBinary(const Image3d<VOXEL> &gimg, Image3d<BINARY> &bimg)
	{
		bimg.CopyMetaData(gimg);

		for (size_t i = 0; i < gimg.GetImageSize (); ++i)
			bimg.SetVoxel(i, gimg.GetVoxel(i) > 0 ? BINARY(1) : BINARY(0));
	}

	//--------------------------------------------------------------------------
	// Explicit instantiations
	//--------------------------------------------------------------------------
	template CORE_I3D_DLLEXPORT void BinaryToGray (const Image3d < BINARY > &, Image3d < GRAY8 > &);
	template CORE_I3D_DLLEXPORT void BinaryToGray (const Image3d < BINARY > &, Image3d < GRAY16 > &);
	template CORE_I3D_DLLEXPORT void BinaryToGray (const Image3d < BINARY > &, Image3d < float > &);
	template CORE_I3D_DLLEXPORT void BinaryToGray (const Image3d < BINARY > &, Image3d < double > &);

	template CORE_I3D_DLLEXPORT void GrayToBinary (const Image3d < GRAY8 > &, Image3d < BINARY > &);
	template CORE_I3D_DLLEXPORT void GrayToBinary (const Image3d < GRAY16 > &, Image3d < BINARY > &);
	template CORE_I3D_DLLEXPORT void GrayToBinary (const Image3d < float > &, Image3d < BINARY > &);
	template CORE_I3D_DLLEXPORT void GrayToBinary (const Image3d < double > &, Image3d < BINARY > &);

	//void BinaryToGray8 (const Image3d < BINARY > &bimg, Image3d < GRAY8 > &gimg)
	//{
	//	gimg.SetOffset (bimg.GetOffset ());
	//	gimg.SetResolution (bimg.GetResolution ());
	//	gimg.MakeRoom (bimg.GetSizeX (), bimg.GetSizeY (), bimg.GetSizeZ ());

	//	for (size_t i = 0; i < bimg.GetImageSize (); ++i)
	//		gimg.SetVoxel (i, bimg.GetVoxel (i));
	//}

	////--------------------------------------------------------------------------
	//// Gray8ToBinary 
	////--------------------------------------------------------------------------
	//void Gray8ToBinary (const Image3d < GRAY8 > &gimg, Image3d < BINARY > &bimg)
	//{
	//	bimg.SetOffset (gimg.GetOffset ());
	//	bimg.SetResolution (gimg.GetResolution ());
	//	bimg.MakeRoom (gimg.GetSizeX (), gimg.GetSizeY (), gimg.GetSizeZ ());

	//	for (size_t i = 0; i < gimg.GetImageSize (); ++i)
	//		bimg.SetVoxel (i, BINARY (gimg.GetVoxel (i)));
	//}

	////--------------------------------------------------------------------------
	//// BinaryToGray16
	////--------------------------------------------------------------------------
	//void BinaryToGray16 (const Image3d < BINARY > &bimg, Image3d < GRAY16 > &gimg)
	//{
	//	gimg.SetOffset (bimg.GetOffset ());
	//	gimg.SetResolution (bimg.GetResolution ());
	//	gimg.MakeRoom (bimg.GetSizeX (), bimg.GetSizeY (), bimg.GetSizeZ ());

	//	for (size_t i = 0; i < bimg.GetImageSize (); ++i)
	//		gimg.SetVoxel (i, bimg.GetVoxel (i));
	//}

	//--------------------------------------------------------------------------
	// Gray16ToBinary 
	//--------------------------------------------------------------------------
	//void Gray16ToBinary (const Image3d < GRAY16 > &gimg, Image3d < BINARY > &bimg)
	//{
	//	bimg.SetOffset (gimg.GetOffset ());
	//	bimg.SetResolution (gimg.GetResolution ());
	//	bimg.MakeRoom (gimg.GetSizeX (), gimg.GetSizeY (), gimg.GetSizeZ ());

	//	for (size_t i = 0; i < gimg.GetImageSize (); ++i)
	//		bimg.SetVoxel (i, BINARY (gimg.GetVoxel (i)));
	//}


	//--------------------------------------------------------------------------
	// RGBtoGray 
	//--------------------------------------------------------------------------
	template < class FULL_COLOR, class GRAY_CHANNEL > void RGBtoGray (
		const Image3d < FULL_COLOR > &rgb,
		Image3d < GRAY_CHANNEL > &red,
		Image3d < GRAY_CHANNEL > &green,
		Image3d < GRAY_CHANNEL > &blue)
	{
		CopyChannel (rgb, red, 0);
		CopyChannel (rgb, green, 1);
		CopyChannel (rgb, blue, 2);
	}

	//--------------------------------------------------------------------------
	// GrayToRGB
	//--------------------------------------------------------------------------
	template < class GRAY_CHANNEL, class FULL_COLOR > void GrayToRGB (
		const Image3d < GRAY_CHANNEL > &red,
		const Image3d < GRAY_CHANNEL > &green,
		const Image3d < GRAY_CHANNEL > &blue,
		Image3d < FULL_COLOR > &rgb)
	{
		if (red.GetOffset () != green.GetOffset () ||
			red.GetOffset () != blue.GetOffset () ||
			blue.GetOffset () != green.GetOffset () ||
			red.GetSize () != green.GetSize () ||
			red.GetSize () != blue.GetSize () ||
			blue.GetSize () != green.GetSize () ||
			red.GetResolution ().GetRes () != green.GetResolution ().GetRes () ||
			red.GetResolution ().GetRes () != blue.GetResolution ().GetRes () ||
			blue.GetResolution ().GetRes () != green.GetResolution ().GetRes ())
		{
			throw InternalException ("Couldn't compose images of different parameters");
		}

		rgb.SetOffset (red.GetOffset ());
		rgb.SetResolution (red.GetResolution ());
		rgb.MakeRoom (red.GetWidth (), red.GetHeight (), red.GetNumSlices ());


		GRAY_CHANNEL const *rdata = red.GetFirstVoxelAddr ();
		GRAY_CHANNEL const *gdata = green.GetFirstVoxelAddr ();
		GRAY_CHANNEL const *bdata = blue.GetFirstVoxelAddr ();
		FULL_COLOR *rgbdata = rgb.GetFirstVoxelAddr ();

		size_t size = red.GetImageSize ();

		for (size_t i = 0; i < size; i++)
		{
			rgbdata->red = *rdata;
			rgbdata->green = *gdata;
			rgbdata->blue = *bdata;
			rgbdata++;
			rdata++;
			gdata++;
			bdata++;
		}

		return;
	}

	//--------------------------------------------------------------------------
	// Explicit instantiations
	//--------------------------------------------------------------------------
	template CORE_I3D_DLLEXPORT void RGBtoGray < RGB, GRAY8 > (
		const Image3d < RGB > &,
		Image3d < GRAY8 > &,
		Image3d < GRAY8 > &,
		Image3d < GRAY8 > &);

	template CORE_I3D_DLLEXPORT void RGBtoGray < RGB16, GRAY16 > (
		const Image3d < RGB16 > &,
		Image3d < GRAY16 > &,
		Image3d < GRAY16 > &,
		Image3d < GRAY16 > &);

	template CORE_I3D_DLLEXPORT bool CopyChannelBack < GRAY8 , RGB > (
		const Image3d < GRAY8 > &,
		Image3d < RGB > &, 
		int channel);

	template CORE_I3D_DLLEXPORT bool CopyChannelBack < GRAY16 , RGB16 > (
		const Image3d < GRAY16 > &,
		Image3d < RGB16 > &, 
		int channel);

	template CORE_I3D_DLLEXPORT bool CopyChannel < RGB, GRAY8 > (
		const Image3d < RGB > &,
		Image3d < GRAY8 > &, 
		int channel);

	template CORE_I3D_DLLEXPORT bool CopyChannel < RGB16, GRAY16 > (
		const Image3d < RGB16 > &,
		Image3d < GRAY16 > &, 
		int channel);

	template CORE_I3D_DLLEXPORT void GrayToRGB < GRAY8, RGB > (
		const Image3d < GRAY8 > &,
		const Image3d < GRAY8 > &,
		const Image3d < GRAY8 > &,
		Image3d < RGB > &);

	template CORE_I3D_DLLEXPORT void GrayToRGB < GRAY16, RGB16 > (
		const Image3d < GRAY16 > &,
		const Image3d < GRAY16 > &,
		const Image3d < GRAY16 > &,
		Image3d < RGB16 > &);

	/***************************************************************************\
	*
	*                               Other functions
	*
	\***************************************************************************/
	//--------------------------------------------------------------------------
	// GenerateDummyVoxels 
	//--------------------------------------------------------------------------
	template <class VOXEL> void GenerateDummyVoxels (
		const Image3d<VOXEL> & ImgIn, Image3d<VOXEL> & ImgOut, 
		VOXEL Value, const i3d::Vector3d<size_t> &width)
	{
		size_t y, z;

		Vector3d<size_t> size = ImgIn.GetSize();
		cout << size << endl;
		if (size.z == 1)
			size.z -= 2*width.z;
		size += width + width;
		cout << size << endl;
		const size_t yMax = size.y,
			zMax = size.z;

		//    ImgOut.SetOffset(ImgIn.GetOffset());
		ImgOut.SetResolution(ImgIn.GetResolution());
		ImgOut.MakeRoom(size);

		VOXEL * ptrOut;
		const VOXEL * ptrIn =ImgIn.GetVoxelAddr(0);
		valarray<VOXEL> & OutData =  ImgOut.GetVoxelData();
		OutData = Value;


		if (zMax > 1) //three dimensions
		{
			ptrOut = ImgOut.GetVoxelAddr(width.z*size.x*size.y + width.y*size.x + width.x);
			for (z = width.z; z < zMax - width.z; ++z)
			{
				for (y = width.y; y < (yMax - width.y); ++y)
				{
					memcpy((void *)ptrOut,(void* )ptrIn, (size.x-2*width.x)*sizeof(VOXEL));
					ptrOut += size.x;
					ptrIn += size.x - 2*width.x;
				}
				ptrOut += 2*width.y*size.x;
			}
		}
		else
		{ // two dimensions
			ptrOut = ImgOut.GetVoxelAddr(width.y * size.x + width.x);

			for (y = width.y; y < (yMax - width.y); ++y)
			{
				memcpy((void *)ptrOut,(void* )ptrIn, (size.x - 2*width.x)*sizeof(VOXEL));
				ptrOut += size.x;
				ptrIn += size.x - 2*width.x;
			}
		}
	} // GenerateDummyVoxels 

	//--------------------------------------------------------------------------
	// Explicit instantiations
	//--------------------------------------------------------------------------
	template CORE_I3D_DLLEXPORT void GenerateDummyVoxels (const Image3d<GRAY8> & ImgIn, Image3d<GRAY8> & ImgOut, GRAY8 Value, const Vector3d<size_t> &width);
	template CORE_I3D_DLLEXPORT void GenerateDummyVoxels (const Image3d<GRAY16> & ImgIn, Image3d<GRAY16> & ImgOut, GRAY16 Value, const Vector3d<size_t> &width);


	/***************************************************************************\
	*
	*                               Vector field 3D
	*
	\***************************************************************************/
	//--------------------------------------------------------------------------
	// Explicit instantiations
	//--------------------------------------------------------------------------
	template class CORE_I3D_DLLEXPORT VectField3d < float>;
	template class CORE_I3D_DLLEXPORT VectField3d < double>;

}
