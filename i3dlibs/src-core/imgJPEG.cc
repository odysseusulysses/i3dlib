/*
 * i3dlib - image manipulation library
 *
 * Copyright (C) 2000-2006   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 * FILE: imgJPEG.cc
 *
 * I/O routines for JPEG images
 *
 * David Svoboda (svoboda@fi.muni.cz) 2002
 *
 */
/*Suppression of Deprecated warning for Visual studio 8*/
#ifdef _MSC_VER
#if _MSC_VER == 1400
#define _CRT_SECURE_NO_DEPRECATE 1
#endif
#endif

#include <string.h>
#include "i3d_config.h"

#ifdef WITH_JPEG

#include "imgJPEG.h"
extern "C" {
#include <jpeglib.h>
}

namespace i3d {

/***************************************************************************/
/* LoadImageInfo */
/***************************************************************************/
void JPEGReader::LoadImageInfo()
{
    if (jpeg_read_header(decomp_info, TRUE) != JPEG_HEADER_OK)
        throw IOException("JPEG: Unable to read JPEG header.");

    /* Now we have all informations about image in struct 'cinfo' (including
    width and height) and decompression can start out */
    if (decomp_info->out_color_space == JCS_RGB)
        header.itype = RGBVoxel;
    else if (decomp_info->out_color_space == JCS_GRAYSCALE)
        header.itype = Gray8Voxel;
    else 
        header.itype = UnknownVoxel;

    // Use image_width and image_height instead of output_width and output_height so
    // we do not have to start the decompression. This values should match, because
    // we do not use scaling provided by the jpeg library.
    header.size.x = decomp_info->image_width;
    header.size.y = decomp_info->image_height;
}
  
JPEGReader::JPEGReader(const char *fname):ImageReader(fname) 
{
	decomp_info = new jpeg_decompress_struct();
	jerr = new jpeg_error_mgr;

	if ((fd = fopen(fname, "rb")) == NULL)
		throw IOException("JPEG: Unable to open "+ std::string(fname) + ".");

    decomp_info->err = jpeg_std_error(jerr);
    jpeg_create_decompress(decomp_info); /* create decompress structure */
    jpeg_stdio_src(decomp_info, fd); /* input stream is opened file */
};

/***************************************************************************/
/* ~JPEGReader */
/***************************************************************************/
JPEGReader::~JPEGReader()
{
    jpeg_destroy_decompress(decomp_info); /* destroy decompress struct */
    delete decomp_info;
    delete jerr;
    fclose(fd);
}

/***************************************************************************
   LoadData 
   - Reads slice from JPEG file using ICJ libjpeg routines. 
 ***************************************************************************/ 
template <class T> void JPEGReader::LoadData(T *data)
{
    jpeg_start_decompress(decomp_info);

    if (decomp_info->output_width != decomp_info->image_width ||
        decomp_info->output_height != decomp_info->image_height)
    {
        throw IOException("JPEG: Output and image dimensions do not match!");
    }

    int row_stride = decomp_info->output_width * decomp_info->output_components;
    JSAMPARRAY buffer = (*decomp_info->mem->alloc_sarray)
        ((j_common_ptr) decomp_info, JPOOL_IMAGE, row_stride, 1);

    while (decomp_info->output_scanline < decomp_info->output_height)
    {
        jpeg_read_scanlines(decomp_info, buffer, 1);
        memcpy(data, buffer[0], row_stride);
        data = data + decomp_info->output_width; 
        /* move to next scanline */
    }

    jpeg_finish_decompress(decomp_info);
} 

/***************************************************************************/
/* SaveImageInfo */
/***************************************************************************/
void JPEGWriter::SaveImageInfo()
{
  comp_info->err = jpeg_std_error(jerr);
  jpeg_create_compress(comp_info);

  jpeg_stdio_dest(comp_info, fd);
	 
  comp_info->image_width = header.size.x;
  comp_info->image_height = header.size.y;

  switch (header.itype)
  {
    case RGBVoxel:
      comp_info->input_components = 3;
      comp_info->in_color_space = JCS_RGB;
      break;
    case Gray8Voxel:
      comp_info->input_components = 1;
      comp_info->in_color_space = JCS_GRAYSCALE;
      break;
	 case BinaryVoxel:
      comp_info->input_components = 1;
      comp_info->in_color_space = JCS_GRAYSCALE;
      break;
    default:
      std::string msg;
      msg = std::string("JPEGWriter::SaveImageInfo: Unknown voxel type.");
      throw IOException(msg);
  }

  /* FIXME: it should be possible to change quality here (instead of setting
     default quality) */
  
  jpeg_set_defaults(comp_info);
}


/*************************************************************************
	SaveData 
   - save one slice into file using JPEG compression. 
************************************************************************/
template <class T> void JPEGWriter::SaveData(const T *data)
{
  jpeg_start_compress(comp_info, TRUE);

  JSAMPROW row_pointer[1]; /* scanline buffer */
  
  unsigned int i = 0;
  while (i < header.size.y)
  {
    row_pointer[0] = (JSAMPROW) data;
    jpeg_write_scanlines(comp_info, row_pointer, 1);
    data = data + header.size.x;
    i++;
  }

  jpeg_finish_compress(comp_info);
  jpeg_destroy_compress(comp_info);
}

JPEGWriter::JPEGWriter(const char *fname) : ImageWriter(fname) 
{
	comp_info = new jpeg_compress_struct();
	jerr = new jpeg_error_mgr;

	if ((fd = fopen(fname, "wb")) == NULL)
		throw IOException("JPEG: Unable to create "+ std::string(fname) + '.');
};

/***************************************************************************/
/* ~JPEGWriter */
/***************************************************************************/
JPEGWriter::~JPEGWriter()
	{
		delete comp_info;
		delete jerr;
   fclose(fd);
	}


}

#endif
