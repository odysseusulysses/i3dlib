############################################################################
#
#  i3dlib - image manipulation library
#
#  Copyright (C) 2000-2006   Centre for Biomedical Image Analysis (CBIA)
# 
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Library General Public
#  License as published by the Free Software Foundation; either
#  version 2 of the License, or (at your option) any later version.
# 
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Library General Public License for more details.
# 
#  You should have received a copy of the GNU Library General Public
#  License along with this library; if not, write to the Free
#  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#
#############################################################################
#
# FILE: cross platform CMake configure script
#
# Petr Matula <pem@fi.muni.cz> 
# David Svoboda <svoboda@fi.muni.cz>
#
#############################################################################
cmake_minimum_required(VERSION 2.6)

PROJECT (I3D)

# set the compilation way
SET(CMAKE_VERBOSE_MAKEFILE ON CACHE BOOL
    "Tracing the compilation process." FORCE)

# test for presence of STD
INCLUDE(${CMAKE_ROOT}/Modules/TestForSTDNamespace.cmake)

# include the necessary macros
INCLUDE(${CMAKE_ROOT}/Modules/CheckIncludeFileCXX.cmake)

# find some necessary files - e.g. sstream
CHECK_INCLUDE_FILE_CXX("sstream" HAVE_SSTREAM)


############################################################################
# basic settings (set the parameters if they were not inherited)
############################################################################

# library name, define it if not inherited
IF (NOT CORE_LIBRARY_NAME)
	# if we got here then this file is not inherited...
	# we'll repeat most of the "general questions" from the top-level CMakeList
	SET (CORE_LIBRARY_NAME "i3dcore" CACHE STRING 
			  "The name of the core library.")

	# build shared/static library
	# ask once again if we didn't ask in the top-level CMakeList
	OPTION (CORE_BUILD_SHARED_LIBS "Static/Shared core library" ON)

	# set these paths for general environment
	SET (GLOBAL_ALT_INC_DIR "not_used" CACHE PATH 
			  "Path to headers of supplementary libraries.")
	SET (GLOBAL_ALT_LIB_DIR "not_used" CACHE PATH 
			  "Directory with supplementary libraries.")
	FIND_PROGRAM(GLOBAL_DOXYGEN_PROGRAM doxygen PATHS ${BIN_DIRS} DOC 
			  "Path to doxygen program.")

	# unix-related features
	IF (UNIX)
		# we can be a bit more specific in unix
		SET(GLOBAL_ALT_INC_DIR /usr/local/include /usr/include)
		SET(GLOBAL_ALT_LIB_DIR /usr/local/lib /usr/lib)

		# Electric fence library
		OPTION(GLOBAL_USE_EFENCE 
				  "Do you want to use electric fence tool (efence library)?" OFF)
		IF (GLOBAL_USE_EFENCE)
			FIND_LIBRARY(GLOBAL_LIB_EFENCE "efence" ${GLOBAL_ALT_LIB_DIR}
				DOC "Localization of efence library.")

			SET(LIBS ${LIBS} ${GLOBAL_LIB_EFENCE})
		ENDIF (GLOBAL_USE_EFENCE)
	ENDIF (UNIX)

	# define the path to i3d_config.h.in
	# it changes, when used CMakeList file alone
	IF (NOT CORE_SOURCE_DIR)  # do not set the value again
		SET (CORE_SOURCE_DIR ${CMAKE_SOURCE_DIR}
			CACHE INTERNAL "The path to source i3d_config.h.in." FORCE)
		SET (CORE_BINARY_DIR ${CMAKE_BINARY_DIR}
			CACHE INTERNAL "The path to created i3d_config.h." FORCE)
	ENDIF (NOT CORE_SOURCE_DIR)
ELSE (NOT CORE_LIBRARY_NAME)
	# we're called from the top-level CMakeFile
	IF (NOT CORE_SOURCE_DIR)  # do not set the value again
		SET (CORE_SOURCE_DIR ${CMAKE_SOURCE_DIR}/src-core
			CACHE INTERNAL "The path to source i3d_config.h.in." FORCE)
		SET (CORE_BINARY_DIR ${CMAKE_BINARY_DIR}/src-core
			CACHE INTERNAL "The path to created i3d_config.h." FORCE)
	ENDIF (NOT CORE_SOURCE_DIR)
ENDIF (NOT CORE_LIBRARY_NAME)

# compile the documentation as well?
OPTION (CORE_DOCUMENTATION "Documentation generation required?" ON)
IF (CORE_DOCUMENTATION)
	ADD_CUSTOM_TARGET(docs-core
		${CMAKE_COMMAND} -E chdir ${CORE_SOURCE_DIR}
		${GLOBAL_DOXYGEN_PROGRAM} Doxyfile)
ENDIF (CORE_DOCUMENTATION)

# set the debug/release version
OPTION(CORE_DEBUG_VERSION "Shall I add debug information?" ON)
IF (CORE_DEBUG_VERSION) 
	SET(CMAKE_BUILD_TYPE "DEBUG" CACHE STATIC 
			  "Compilation for debug or release version?" FORCE)
ELSE (CORE_DEBUG_VERSION)
	SET(CMAKE_BUILD_TYPE "RELEASE" CACHE STATIC 
			  "Compilation for debug or release version?" FORCE)
ENDIF (CORE_DEBUG_VERSION)


############################################################################
# I/O configuration
############################################################################

# use libtiff - to support TIFF image I/O
OPTION(CORE_WITH_TIFF "Do you want to use TIFF image format?" ON)
IF (CORE_WITH_TIFF)
	FIND_PATH(CORE_TIFF_HEADERS tiff.h ${GLOBAL_ALT_INC_DIR} 
		DOC "Path to tiff header files (tiff library).")
	IF (WIN32)
	  FIND_LIBRARY(CORE_TIFF_LIB libtiff ${GLOBAL_ALT_LIB_DIR} 
		DOC "Localization of tiff library.")
	ELSE (WIN32)
	  FIND_LIBRARY(CORE_TIFF_LIB tiff ${GLOBAL_ALT_LIB_DIR} 
		DOC "Localization of tiff library.")
	ENDIF (WIN32)

	INCLUDE_DIRECTORIES(${CORE_TIFF_HEADERS})
	SET(LIBS ${LIBS} ${CORE_TIFF_LIB})

ENDIF (CORE_WITH_TIFF)

# use libdcmtk - to support DICOM image I/O
OPTION(CORE_WITH_DCM "Do you want to use DICOM image format?" OFF)
IF (CORE_WITH_DCM)
	FIND_PATH(CORE_DCM_HEADERS cfunix.h ${GLOBAL_ALT_INC_DIR} 
		DOC "Path to dcmtk config header files (dcmtk library).")
	FIND_LIBRARY(CORE_DCM_LIB dcmdata ${GLOBAL_ALT_LIB_DIR} 
		DOC "Localization of dcmdata library.")

	INCLUDE_DIRECTORIES(${CORE_DCM_HEADERS})
	SET(LIBS ${LIBS} ${CORE_DCM_LIB} dcmimage dcmimgle ofstd pthread 
		dcmjpeg ijg8 ijg12 ijg16)

ENDIF (CORE_WITH_DCM)

# use libjpeg - to support JPEG image I/O
OPTION(CORE_WITH_JPEG "Do you want to support JPEG image format?" ON)
IF (CORE_WITH_JPEG)
	FIND_PATH(CORE_JPEG_HEADERS jpeglib.h ${GLOBAL_ALT_INC_DIR} 
		DOC "Path to jpeg header files (jpeg library).")
	FIND_LIBRARY(CORE_JPEG_LIB jpeg ${GLOBAL_ALT_LIB_DIR} 
		DOC "Localization of jpeg library.")

	INCLUDE_DIRECTORIES(${CORE_JPEG_HEADERS})
	SET(LIBS ${LIBS} ${CORE_JPEG_LIB})

ENDIF (CORE_WITH_JPEG)

# use Targa image format
# implemented internaly in i3dlib (no additional library linkage is required)
OPTION(CORE_WITH_TARGA "Do you want to support Targa image format?" ON)

# use MetaIO image format
# implemented internaly in i3dlib (no additional library linkage is required)
OPTION(CORE_WITH_METAIO "Do you want to support MetaIO image format?" ON)

# use ICS image format
OPTION(CORE_WITH_ICS "Do you want to support ICS image format?" ON)
IF (CORE_WITH_ICS)
	FIND_PATH(CORE_ICS_HEADERS libics.h ${GLOBAL_ALT_INC_DIR}
		DOC "Path to ICS header files (ICS library).")
	IF (WIN32)
	  FIND_LIBRARY(CORE_ICS_LIB libics ${GLOBAL_ALT_LIB_DIR}
		DOC "Localization of ICS library.")
	ELSE (WIN32)
	  FIND_LIBRARY(CORE_ICS_LIB ics ${GLOBAL_ALT_LIB_DIR}
		DOC "Localization of ICS library.")
	ENDIF (WIN32)

	INCLUDE_DIRECTORIES(${CORE_ICS_HEADERS})
	SET(LIBS ${LIBS} ${CORE_ICS_LIB})
ENDIF (CORE_WITH_ICS)

OPTION (CORE_WITH_ZLIB "Is the ICS compiled with LZW support?" ON)
IF (CORE_WITH_ZLIB)
	IF (WIN32)
       	  FIND_LIBRARY(CORE_Z_LIB zlib1 ${GLOBAL_ALT_LIB_DIR}
                DOC "Localization of zlib library.")
	ELSE (WIN32)
       	  FIND_LIBRARY(CORE_Z_LIB z ${GLOBAL_ALT_LIB_DIR}
                DOC "Localization of zlib library.")
	ENDIF (WIN32)

  	SET (LIBS ${LIBS} ${CORE_Z_LIB})
ENDIF (CORE_WITH_ZLIB)

OPTION (CORE_WITH_BIOFORMATS "Include support for the Java BioFormats library?" OFF)
IF (CORE_WITH_BIOFORMATS)
	INCLUDE(FindJNI)
	
	IF (NOT JNI_NOTFOUND)
		INCLUDE_DIRECTORIES(${JAVA_INCLUDE_PATH})
		INCLUDE_DIRECTORIES(${JAVA_INCLUDE_PATH2})
		
		# add the java jvm library to the list of libraries
		SET (LIBS ${LIBS} ${JAVA_JVM_LIBRARY})
		
		# check the appropriate path for the jar in the CBIA_JAVA_CLASSPATH
		SET (CP "$ENV{CBIA_JAVA_CLASSPATH}")

		IF(NOT CP)
			MESSAGE("Do not forget to set the CBIA_JAVA_CLASSPATH to include the BioFormats JAR file. Now it is empty or not set.")
		ELSE(NOT CP)
			IF(NOT "${CP}" MATCHES "loci_tools.jar" AND NOT "${CP}" MATCHES "bio-formats.jar")
				MESSAGE("Do not forget to set the CBIA_JAVA_CLASSPATH to include the BioFormats JAR file (loci_tools.jar or bio-formats.jar). Now it is CBIA_JAVA_CLASSPATH=$ENV{CBIA_JAVA_CLASSPATH}")
			ENDIF(NOT "${CP}" MATCHES "loci_tools.jar" AND NOT "${CP}" MATCHES "bio-formats.jar")
		ENDIF(NOT CP)
	ENDIF (NOT JNI_NOTFOUND)   	
ENDIF (CORE_WITH_BIOFORMATS)


#########################################################################
# manage the options into 'i3d_config.h' file
#########################################################################

CONFIGURE_FILE(${CORE_SOURCE_DIR}/i3d_config.h.in 
		  ${CORE_BINARY_DIR}/i3d_config.h)

#########################################################################
# library files 
#########################################################################

# set the list of source files
SET(CORE_SOURCES
	basic.cc
	basic.h 
	vector3d.cc
	vector3d.h
	resolution.cc
	resolution.h
	toolbox.cc
	toolbox.h
	image3d.cc
	image3d.h 
	i3dio.cc 
	i3dio.h
	i3dassert.h
	voi.h
	imgDCM.cc imgI3D.cc imgICS.cc imgJPEG.cc imgMETAIO.cc 
	imgTGA.cc imgTIFF.cc imgfiles.cc
	imgDCM.h imgI3D.h imgICS.h imgJPEG.h imgMETAIO.h
	imgTGA.h imgTIFF.h imgfiles.h 
)

# windows needs additional files
IF (WIN32)
   SET(CORE_SOURCES ${CORE_SOURCES} msdir.c msdir.h)	
ENDIF (WIN32)

# BioFormats integration needs additional files
IF (CORE_WITH_BIOFORMATS)
   # Add the java wrappers include directories
   INCLUDE_DIRECTORIES(${PROJECT_SOURCE_DIR}/java-wrappers)
   
   # Add the bioformats reader classes
   SET(CORE_SOURCES ${CORE_SOURCES} java_wrappers.cpp bioformatsreader.cpp bioformatsreader.h)
ENDIF (CORE_WITH_BIOFORMATS)


#########################################################################
# compilation details
#########################################################################

# to include the freshly (re-)created i3d_config.h
INCLUDE_DIRECTORIES(${CORE_BINARY_DIR})

# hack to make CMake create shared version of the library
IF (CORE_BUILD_SHARED_LIBS)
	SET(BUILD_SHARED_LIBS ON)
ENDIF (CORE_BUILD_SHARED_LIBS)

IF (WIN32)
	# regex library - for Win32 specific only
	FIND_PATH(CORE_REGEX_HEADERS regex.h ${GLOBAL_ALT_INC_DIR}
		DOC "Path to regex header files (regex library).")
	FIND_LIBRARY(CORE_REGEX_LIB libregex ${GLOBAL_ALT_LIB_DIR}
		DOC "Localization of regex library.")

	INCLUDE_DIRECTORIES(${CORE_REGEX_HEADERS})
	SET(LIBS ${LIBS} ${CORE_REGEX_LIB})

	SET(CMAKE_CXX_FLAGS "/GX /nologo /DNOMINMAX /Zm200" 
		 CACHE STRING "C++ compiler flags." FORCE)
	SET(CMAKE_C_FLAGS "/nologo" CACHE STRING "C compiler flags." FORCE)
	SET(CMAKE_CXX_WARNING_LEVEL 1 CACHE STRING "Warning level." FORCE)
	ADD_DEFINITIONS(-DNOMINMAX /DWIN32)

	IF (CORE_BUILD_SHARED_LIBS)
		SET(CMAKE_CXX_FLAGS_RELEASE "/MD /O2 /Ob2 /D NDEBUG" 
			CACHE STRING "C++ compiler flags used during release shared builds." FORCE)
		SET(CMAKE_C_FLAGS_RELEASE "/MD /O2 /Ob2 /D NDEBUG"
			CACHE STRING "C compiler flags used during release shared builds." FORCE)
		SET(CMAKE_SHARED_LINKER_FLAGS_RELEASE "/NODEFAULTLIB:LIBC /NODEFAULTLIB:LIBCD"
			CACHE STRING "" FORCE)

		SET(CMAKE_CXX_FLAGS_DEBUG "/D_DEBUG /MDd /Zi /Ob0 /Od /RTC1" 
			CACHE STRING "C++ compiler flags used during debug shared builds." FORCE)
		SET(CMAKE_C_FLAGS_DEBUG "/D_DEBUG /MDd /Zi /Ob0 /Od /RTC1"
			CACHE STRING "C compiler flags used during debug shared builds." FORCE)
		SET(CMAKE_SHARED_LINKER_FLAGS_DEBUG "/NODEFAULTLIB:LIBC /NODEFAULTLIB:LIBCD /NODEFAULTLIB:MSVCRT" 
			CACHE STRING "" FORCE)

		ADD_DEFINITIONS(/DUSE_RXSPENCER_DLL /DCORE_I3D_EXPORT)

	ELSE (CORE_BUILD_SHARED_LIBS)
		SET(CMAKE_CXX_FLAGS_RELEASE "/MT /O2 /Ob2 /D NDEBUG" 
			CACHE STRING "C++ compiler flags used during release static builds." FORCE)
		SET(CMAKE_C_FLAGS_RELEASE "/MT /O2 /Ob2 /D NDEBUG"
			CACHE STRING "C compiler flags used during release static builds." FORCE)

		SET(CMAKE_CXX_FLAGS_DEBUG "/D_DEBUG /MTd /Zi /Ob0 /Od /RTC1" 
			CACHE STRING "C++ compiler flags used during debug static builds." FORCE)
		SET(CMAKE_C_FLAGS_DEBUG "/D_DEBUG /MTd /Zi /Ob0 /Od /RTC1"
			CACHE STRING "C compiler flags used during debug static builds." FORCE)

		ADD_DEFINITIONS(/DUSE_RXSPENCER_STATIC)
 	ENDIF (CORE_BUILD_SHARED_LIBS)
ENDIF (WIN32)

IF (UNIX)
	# gnu compiler
	IF (CMAKE_COMPILER_IS_GNUCXX)
		SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall")
		SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fpermissive")
	ENDIF (CMAKE_COMPILER_IS_GNUCXX)
ENDIF (UNIX)

# enable debugging routines
IF (CORE_DEBUG_VERSION)
	ADD_DEFINITIONS(-DI3D_DEBUG)
       	#SET_TARGET_PROPERTIES(${CORE_LIBRARY_NAME} PROPERTIES DEFINE_SYMBOL I3D_DEBUG)
ENDIF (CORE_DEBUG_VERSION)

# build-up the library
cmake_policy(SET CMP0003 OLD)
ADD_LIBRARY(${CORE_LIBRARY_NAME} ${CORE_SOURCES})


#########################################################################
# additional technical options - SET_TARGET_PROPERTIES command
#########################################################################

# give the shared library version under unix
IF (UNIX)
	SET_TARGET_PROPERTIES(${CORE_LIBRARY_NAME} PROPERTIES VERSION 1.1.0)
ENDIF (UNIX)

# give dependency details
TARGET_LINK_LIBRARIES(${CORE_LIBRARY_NAME} ${LIBS})


#########################################################################
# installation details
#########################################################################

# don't forget to install i3d_config.h
INSTALL(FILES ${CORE_BINARY_DIR}/i3d_config.h DESTINATION include/i3d
	  PERMISSIONS ${RIGHTS_FILE})

# install header files
FILE(GLOB CORE_I3D_HEADERS "*.h")
INSTALL(FILES ${CORE_I3D_HEADERS} DESTINATION include/i3d
	  PERMISSIONS ${RIGHTS_FILE})

# install the library
INSTALL(TARGETS ${CORE_LIBRARY_NAME} 
	  LIBRARY DESTINATION lib
	  RUNTIME DESTINATION bin
	  ARCHIVE DESTINATION lib
	  PERMISSIONS ${RIGHTS_LIB})
