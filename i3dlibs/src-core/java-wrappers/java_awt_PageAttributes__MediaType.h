#ifndef java_awt_PageAttributes__MediaType_H
#define java_awt_PageAttributes__MediaType_H
#include <jni.h>
#include <java_marker.h>
#include <java_awt_AttributeValue.h>

namespace java {
namespace lang {
class String;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
class PageAttributes__MediaType : public java::awt::AttributeValue {
  public:
    PageAttributes__MediaType(JavaMarker* dummy);
    PageAttributes__MediaType(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual int hashCode(); // public int java.awt.PageAttributes$MediaType.hashCode()
    virtual const char* toString(); // public java.lang.String java.awt.PageAttributes$MediaType.toString()

    java::awt::PageAttributes__MediaType* ISO_4A0; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.ISO_4A0
    java::awt::PageAttributes__MediaType* ISO_2A0; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.ISO_2A0
    java::awt::PageAttributes__MediaType* ISO_A0; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.ISO_A0
    java::awt::PageAttributes__MediaType* ISO_A1; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.ISO_A1
    java::awt::PageAttributes__MediaType* ISO_A2; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.ISO_A2
    java::awt::PageAttributes__MediaType* ISO_A3; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.ISO_A3
    java::awt::PageAttributes__MediaType* ISO_A4; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.ISO_A4
    java::awt::PageAttributes__MediaType* ISO_A5; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.ISO_A5
    java::awt::PageAttributes__MediaType* ISO_A6; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.ISO_A6
    java::awt::PageAttributes__MediaType* ISO_A7; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.ISO_A7
    java::awt::PageAttributes__MediaType* ISO_A8; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.ISO_A8
    java::awt::PageAttributes__MediaType* ISO_A9; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.ISO_A9
    java::awt::PageAttributes__MediaType* ISO_A10; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.ISO_A10
    java::awt::PageAttributes__MediaType* ISO_B0; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.ISO_B0
    java::awt::PageAttributes__MediaType* ISO_B1; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.ISO_B1
    java::awt::PageAttributes__MediaType* ISO_B2; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.ISO_B2
    java::awt::PageAttributes__MediaType* ISO_B3; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.ISO_B3
    java::awt::PageAttributes__MediaType* ISO_B4; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.ISO_B4
    java::awt::PageAttributes__MediaType* ISO_B5; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.ISO_B5
    java::awt::PageAttributes__MediaType* ISO_B6; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.ISO_B6
    java::awt::PageAttributes__MediaType* ISO_B7; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.ISO_B7
    java::awt::PageAttributes__MediaType* ISO_B8; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.ISO_B8
    java::awt::PageAttributes__MediaType* ISO_B9; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.ISO_B9
    java::awt::PageAttributes__MediaType* ISO_B10; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.ISO_B10
    java::awt::PageAttributes__MediaType* JIS_B0; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.JIS_B0
    java::awt::PageAttributes__MediaType* JIS_B1; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.JIS_B1
    java::awt::PageAttributes__MediaType* JIS_B2; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.JIS_B2
    java::awt::PageAttributes__MediaType* JIS_B3; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.JIS_B3
    java::awt::PageAttributes__MediaType* JIS_B4; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.JIS_B4
    java::awt::PageAttributes__MediaType* JIS_B5; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.JIS_B5
    java::awt::PageAttributes__MediaType* JIS_B6; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.JIS_B6
    java::awt::PageAttributes__MediaType* JIS_B7; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.JIS_B7
    java::awt::PageAttributes__MediaType* JIS_B8; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.JIS_B8
    java::awt::PageAttributes__MediaType* JIS_B9; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.JIS_B9
    java::awt::PageAttributes__MediaType* JIS_B10; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.JIS_B10
    java::awt::PageAttributes__MediaType* ISO_C0; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.ISO_C0
    java::awt::PageAttributes__MediaType* ISO_C1; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.ISO_C1
    java::awt::PageAttributes__MediaType* ISO_C2; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.ISO_C2
    java::awt::PageAttributes__MediaType* ISO_C3; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.ISO_C3
    java::awt::PageAttributes__MediaType* ISO_C4; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.ISO_C4
    java::awt::PageAttributes__MediaType* ISO_C5; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.ISO_C5
    java::awt::PageAttributes__MediaType* ISO_C6; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.ISO_C6
    java::awt::PageAttributes__MediaType* ISO_C7; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.ISO_C7
    java::awt::PageAttributes__MediaType* ISO_C8; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.ISO_C8
    java::awt::PageAttributes__MediaType* ISO_C9; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.ISO_C9
    java::awt::PageAttributes__MediaType* ISO_C10; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.ISO_C10
    java::awt::PageAttributes__MediaType* ISO_DESIGNATED_LONG; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.ISO_DESIGNATED_LONG
    java::awt::PageAttributes__MediaType* EXECUTIVE; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.EXECUTIVE
    java::awt::PageAttributes__MediaType* FOLIO; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.FOLIO
    java::awt::PageAttributes__MediaType* INVOICE; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.INVOICE
    java::awt::PageAttributes__MediaType* LEDGER; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.LEDGER
    java::awt::PageAttributes__MediaType* NA_LETTER; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.NA_LETTER
    java::awt::PageAttributes__MediaType* NA_LEGAL; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.NA_LEGAL
    java::awt::PageAttributes__MediaType* QUARTO; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.QUARTO
    java::awt::PageAttributes__MediaType* A; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.A
    java::awt::PageAttributes__MediaType* B; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.B
    java::awt::PageAttributes__MediaType* C; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.C
    java::awt::PageAttributes__MediaType* D; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.D
    java::awt::PageAttributes__MediaType* E; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.E
    java::awt::PageAttributes__MediaType* NA_10X15_ENVELOPE; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.NA_10X15_ENVELOPE
    java::awt::PageAttributes__MediaType* NA_10X14_ENVELOPE; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.NA_10X14_ENVELOPE
    java::awt::PageAttributes__MediaType* NA_10X13_ENVELOPE; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.NA_10X13_ENVELOPE
    java::awt::PageAttributes__MediaType* NA_9X12_ENVELOPE; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.NA_9X12_ENVELOPE
    java::awt::PageAttributes__MediaType* NA_9X11_ENVELOPE; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.NA_9X11_ENVELOPE
    java::awt::PageAttributes__MediaType* NA_7X9_ENVELOPE; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.NA_7X9_ENVELOPE
    java::awt::PageAttributes__MediaType* NA_6X9_ENVELOPE; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.NA_6X9_ENVELOPE
    java::awt::PageAttributes__MediaType* NA_NUMBER_9_ENVELOPE; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.NA_NUMBER_9_ENVELOPE
    java::awt::PageAttributes__MediaType* NA_NUMBER_10_ENVELOPE; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.NA_NUMBER_10_ENVELOPE
    java::awt::PageAttributes__MediaType* NA_NUMBER_11_ENVELOPE; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.NA_NUMBER_11_ENVELOPE
    java::awt::PageAttributes__MediaType* NA_NUMBER_12_ENVELOPE; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.NA_NUMBER_12_ENVELOPE
    java::awt::PageAttributes__MediaType* NA_NUMBER_14_ENVELOPE; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.NA_NUMBER_14_ENVELOPE
    java::awt::PageAttributes__MediaType* INVITE_ENVELOPE; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.INVITE_ENVELOPE
    java::awt::PageAttributes__MediaType* ITALY_ENVELOPE; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.ITALY_ENVELOPE
    java::awt::PageAttributes__MediaType* MONARCH_ENVELOPE; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.MONARCH_ENVELOPE
    java::awt::PageAttributes__MediaType* PERSONAL_ENVELOPE; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.PERSONAL_ENVELOPE
    java::awt::PageAttributes__MediaType* A0; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.A0
    java::awt::PageAttributes__MediaType* A1; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.A1
    java::awt::PageAttributes__MediaType* A2; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.A2
    java::awt::PageAttributes__MediaType* A3; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.A3
    java::awt::PageAttributes__MediaType* A4; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.A4
    java::awt::PageAttributes__MediaType* A5; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.A5
    java::awt::PageAttributes__MediaType* A6; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.A6
    java::awt::PageAttributes__MediaType* A7; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.A7
    java::awt::PageAttributes__MediaType* A8; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.A8
    java::awt::PageAttributes__MediaType* A9; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.A9
    java::awt::PageAttributes__MediaType* A10; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.A10
    java::awt::PageAttributes__MediaType* B0; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.B0
    java::awt::PageAttributes__MediaType* B1; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.B1
    java::awt::PageAttributes__MediaType* B2; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.B2
    java::awt::PageAttributes__MediaType* B3; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.B3
    java::awt::PageAttributes__MediaType* B4; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.B4
    java::awt::PageAttributes__MediaType* ISO_B4_ENVELOPE; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.ISO_B4_ENVELOPE
    java::awt::PageAttributes__MediaType* B5; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.B5
    java::awt::PageAttributes__MediaType* ISO_B5_ENVELOPE; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.ISO_B5_ENVELOPE
    java::awt::PageAttributes__MediaType* B6; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.B6
    java::awt::PageAttributes__MediaType* B7; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.B7
    java::awt::PageAttributes__MediaType* B8; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.B8
    java::awt::PageAttributes__MediaType* B9; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.B9
    java::awt::PageAttributes__MediaType* B10; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.B10
    java::awt::PageAttributes__MediaType* C0; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.C0
    java::awt::PageAttributes__MediaType* ISO_C0_ENVELOPE; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.ISO_C0_ENVELOPE
    java::awt::PageAttributes__MediaType* C1; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.C1
    java::awt::PageAttributes__MediaType* ISO_C1_ENVELOPE; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.ISO_C1_ENVELOPE
    java::awt::PageAttributes__MediaType* C2; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.C2
    java::awt::PageAttributes__MediaType* ISO_C2_ENVELOPE; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.ISO_C2_ENVELOPE
    java::awt::PageAttributes__MediaType* C3; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.C3
    java::awt::PageAttributes__MediaType* ISO_C3_ENVELOPE; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.ISO_C3_ENVELOPE
    java::awt::PageAttributes__MediaType* C4; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.C4
    java::awt::PageAttributes__MediaType* ISO_C4_ENVELOPE; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.ISO_C4_ENVELOPE
    java::awt::PageAttributes__MediaType* C5; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.C5
    java::awt::PageAttributes__MediaType* ISO_C5_ENVELOPE; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.ISO_C5_ENVELOPE
    java::awt::PageAttributes__MediaType* C6; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.C6
    java::awt::PageAttributes__MediaType* ISO_C6_ENVELOPE; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.ISO_C6_ENVELOPE
    java::awt::PageAttributes__MediaType* C7; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.C7
    java::awt::PageAttributes__MediaType* ISO_C7_ENVELOPE; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.ISO_C7_ENVELOPE
    java::awt::PageAttributes__MediaType* C8; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.C8
    java::awt::PageAttributes__MediaType* ISO_C8_ENVELOPE; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.ISO_C8_ENVELOPE
    java::awt::PageAttributes__MediaType* C9; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.C9
    java::awt::PageAttributes__MediaType* ISO_C9_ENVELOPE; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.ISO_C9_ENVELOPE
    java::awt::PageAttributes__MediaType* C10; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.C10
    java::awt::PageAttributes__MediaType* ISO_C10_ENVELOPE; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.ISO_C10_ENVELOPE
    java::awt::PageAttributes__MediaType* ISO_DESIGNATED_LONG_ENVELOPE; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.ISO_DESIGNATED_LONG_ENVELOPE
    java::awt::PageAttributes__MediaType* STATEMENT; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.STATEMENT
    java::awt::PageAttributes__MediaType* TABLOID; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.TABLOID
    java::awt::PageAttributes__MediaType* LETTER; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.LETTER
    java::awt::PageAttributes__MediaType* NOTE; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.NOTE
    java::awt::PageAttributes__MediaType* LEGAL; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.LEGAL
    java::awt::PageAttributes__MediaType* ENV_10X15; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.ENV_10X15
    java::awt::PageAttributes__MediaType* ENV_10X14; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.ENV_10X14
    java::awt::PageAttributes__MediaType* ENV_10X13; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.ENV_10X13
    java::awt::PageAttributes__MediaType* ENV_9X12; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.ENV_9X12
    java::awt::PageAttributes__MediaType* ENV_9X11; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.ENV_9X11
    java::awt::PageAttributes__MediaType* ENV_7X9; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.ENV_7X9
    java::awt::PageAttributes__MediaType* ENV_6X9; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.ENV_6X9
    java::awt::PageAttributes__MediaType* ENV_9; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.ENV_9
    java::awt::PageAttributes__MediaType* ENV_10; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.ENV_10
    java::awt::PageAttributes__MediaType* ENV_11; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.ENV_11
    java::awt::PageAttributes__MediaType* ENV_12; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.ENV_12
    java::awt::PageAttributes__MediaType* ENV_14; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.ENV_14
    java::awt::PageAttributes__MediaType* ENV_INVITE; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.ENV_INVITE
    java::awt::PageAttributes__MediaType* ENV_ITALY; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.ENV_ITALY
    java::awt::PageAttributes__MediaType* ENV_MONARCH; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.ENV_MONARCH
    java::awt::PageAttributes__MediaType* ENV_PERSONAL; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.ENV_PERSONAL
    java::awt::PageAttributes__MediaType* INVITE; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.INVITE
    java::awt::PageAttributes__MediaType* ITALY; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.ITALY
    java::awt::PageAttributes__MediaType* MONARCH; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.MONARCH
    java::awt::PageAttributes__MediaType* PERSONAL; // public static final java.awt.PageAttributes$MediaType java.awt.PageAttributes$MediaType.PERSONAL
};
}
}
#endif
