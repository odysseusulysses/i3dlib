#ifndef java_awt_geom_Line2D_H
#define java_awt_geom_Line2D_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace awt {
class Shape;
}
}

namespace java {
namespace lang {
class Cloneable;
}
}

namespace java {
namespace awt {
namespace geom {
class Rectangle2D;
}
}
}

namespace java {
namespace awt {
namespace geom {
class Point2D;
}
}
}

namespace java {
namespace awt {
class Rectangle;
}
}

namespace java {
namespace awt {
namespace geom {
class PathIterator;
}
}
}

namespace java {
namespace awt {
namespace geom {
class AffineTransform;
}
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace geom {
class Line2D : public java::lang::Object {
  public:
    Line2D(JavaMarker* dummy);
    Line2D(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual java::lang::Object* clone(); // public java.lang.Object java.awt.geom.Line2D.clone()
    virtual bool contains(double arg1, double arg2, double arg3, double arg4); // public boolean java.awt.geom.Line2D.contains(double,double,double,double)
    virtual bool contains(java::awt::geom::Rectangle2D* arg1); // public boolean java.awt.geom.Line2D.contains(java.awt.geom.Rectangle2D)
    virtual bool contains(java::awt::geom::Point2D* arg1); // public boolean java.awt.geom.Line2D.contains(java.awt.geom.Point2D)
    virtual bool contains(double arg1, double arg2); // public boolean java.awt.geom.Line2D.contains(double,double)
    virtual bool intersects(double arg1, double arg2, double arg3, double arg4); // public boolean java.awt.geom.Line2D.intersects(double,double,double,double)
    virtual bool intersects(java::awt::geom::Rectangle2D* arg1); // public boolean java.awt.geom.Line2D.intersects(java.awt.geom.Rectangle2D)
    virtual java::awt::Rectangle* getBounds(); // public java.awt.Rectangle java.awt.geom.Line2D.getBounds()
    virtual java::awt::geom::PathIterator* getPathIterator(java::awt::geom::AffineTransform* arg1, double arg2); // public java.awt.geom.PathIterator java.awt.geom.Line2D.getPathIterator(java.awt.geom.AffineTransform,double)
    virtual java::awt::geom::PathIterator* getPathIterator(java::awt::geom::AffineTransform* arg1); // public java.awt.geom.PathIterator java.awt.geom.Line2D.getPathIterator(java.awt.geom.AffineTransform)
    virtual double getX1(); // public abstract double java.awt.geom.Line2D.getX1()
    virtual double getX2(); // public abstract double java.awt.geom.Line2D.getX2()
    virtual double getY1(); // public abstract double java.awt.geom.Line2D.getY1()
    virtual double getY2(); // public abstract double java.awt.geom.Line2D.getY2()
    virtual bool intersectsLine(java::awt::geom::Line2D* arg1); // public boolean java.awt.geom.Line2D.intersectsLine(java.awt.geom.Line2D)
    virtual bool intersectsLine(double arg1, double arg2, double arg3, double arg4); // public boolean java.awt.geom.Line2D.intersectsLine(double,double,double,double)
    virtual java::awt::geom::Point2D* getP1(); // public abstract java.awt.geom.Point2D java.awt.geom.Line2D.getP1()
    virtual java::awt::geom::Point2D* getP2(); // public abstract java.awt.geom.Point2D java.awt.geom.Line2D.getP2()
    virtual bool linesIntersect(double arg1, double arg2, double arg3, double arg4, double arg5, double arg6, double arg7, double arg8); // public static boolean java.awt.geom.Line2D.linesIntersect(double,double,double,double,double,double,double,double)
    virtual double ptLineDist(double arg1, double arg2); // public double java.awt.geom.Line2D.ptLineDist(double,double)
    virtual double ptLineDist(double arg1, double arg2, double arg3, double arg4, double arg5, double arg6); // public static double java.awt.geom.Line2D.ptLineDist(double,double,double,double,double,double)
    virtual double ptLineDist(java::awt::geom::Point2D* arg1); // public double java.awt.geom.Line2D.ptLineDist(java.awt.geom.Point2D)
    virtual double ptLineDistSq(double arg1, double arg2); // public double java.awt.geom.Line2D.ptLineDistSq(double,double)
    virtual double ptLineDistSq(java::awt::geom::Point2D* arg1); // public double java.awt.geom.Line2D.ptLineDistSq(java.awt.geom.Point2D)
    virtual double ptLineDistSq(double arg1, double arg2, double arg3, double arg4, double arg5, double arg6); // public static double java.awt.geom.Line2D.ptLineDistSq(double,double,double,double,double,double)
    virtual double ptSegDist(double arg1, double arg2); // public double java.awt.geom.Line2D.ptSegDist(double,double)
    virtual double ptSegDist(double arg1, double arg2, double arg3, double arg4, double arg5, double arg6); // public static double java.awt.geom.Line2D.ptSegDist(double,double,double,double,double,double)
    virtual double ptSegDist(java::awt::geom::Point2D* arg1); // public double java.awt.geom.Line2D.ptSegDist(java.awt.geom.Point2D)
    virtual double ptSegDistSq(double arg1, double arg2); // public double java.awt.geom.Line2D.ptSegDistSq(double,double)
    virtual double ptSegDistSq(double arg1, double arg2, double arg3, double arg4, double arg5, double arg6); // public static double java.awt.geom.Line2D.ptSegDistSq(double,double,double,double,double,double)
    virtual double ptSegDistSq(java::awt::geom::Point2D* arg1); // public double java.awt.geom.Line2D.ptSegDistSq(java.awt.geom.Point2D)
    virtual int relativeCCW(java::awt::geom::Point2D* arg1); // public int java.awt.geom.Line2D.relativeCCW(java.awt.geom.Point2D)
    virtual int relativeCCW(double arg1, double arg2, double arg3, double arg4, double arg5, double arg6); // public static int java.awt.geom.Line2D.relativeCCW(double,double,double,double,double,double)
    virtual int relativeCCW(double arg1, double arg2); // public int java.awt.geom.Line2D.relativeCCW(double,double)
    virtual void setLine(double arg1, double arg2, double arg3, double arg4); // public abstract void java.awt.geom.Line2D.setLine(double,double,double,double)
    virtual void setLine(java::awt::geom::Point2D* arg1, java::awt::geom::Point2D* arg2); // public void java.awt.geom.Line2D.setLine(java.awt.geom.Point2D,java.awt.geom.Point2D)
    virtual void setLine(java::awt::geom::Line2D* arg1); // public void java.awt.geom.Line2D.setLine(java.awt.geom.Line2D)

};
}
}
}
#endif
