#ifndef java_awt_Container_H
#define java_awt_Container_H
#include <jni.h>
#include <java_marker.h>
#include <java_awt_Component.h>

namespace java {
namespace lang {
class Object;
}
}

namespace java {
namespace lang {
class String;
}
}

namespace java {
namespace io {
class PrintStream;
}
}

namespace java {
namespace io {
class PrintWriter;
}
}

namespace java {
namespace awt {
class Graphics;
}
}

namespace java {
namespace awt {
class Font;
}
}

namespace java {
namespace beans {
class PropertyChangeListener;
}
}

namespace java {
namespace awt {
class ComponentOrientation;
}
}

namespace java {
namespace util {
class Set;
}
}

namespace java {
namespace awt {
class FocusTraversalPolicy;
}
}

namespace java {
namespace util {
class EventListener;
}
}

namespace java {
namespace lang {
class Class;
}
}

namespace java {
namespace awt {
class Dimension;
}
}

namespace java {
namespace awt {
class LayoutManager;
}
}

namespace java {
namespace awt {
namespace event {
class ContainerListener;
}
}
}

namespace java {
namespace awt {
class Event;
}
}

namespace java {
namespace awt {
class Point;
}
}

namespace java {
namespace awt {
class Insets;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
class Container : public java::awt::Component {
  public:
    Container(JavaMarker* dummy);
    Container(jobject obj);
    Container(); // public java.awt.Container()

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual void add(java::awt::Component* arg1, java::lang::Object* arg2, int arg3); // public void java.awt.Container.add(java.awt.Component,java.lang.Object,int)
    virtual java::awt::Component* add(java::awt::Component* arg1, int arg2); // public java.awt.Component java.awt.Container.add(java.awt.Component,int)
    virtual java::awt::Component* add(const char* arg1, java::awt::Component* arg2); // public java.awt.Component java.awt.Container.add(java.lang.String,java.awt.Component)
    virtual java::awt::Component* add(java::awt::Component* arg1); // public java.awt.Component java.awt.Container.add(java.awt.Component)
    virtual void add(java::awt::Component* arg1, java::lang::Object* arg2); // public void java.awt.Container.add(java.awt.Component,java.lang.Object)
    virtual void remove(int arg1); // public void java.awt.Container.remove(int)
    virtual void remove(java::awt::Component* arg1); // public void java.awt.Container.remove(java.awt.Component)
    virtual void list(java::io::PrintStream* arg1, int arg2); // public void java.awt.Container.list(java.io.PrintStream,int)
    virtual void list(java::io::PrintWriter* arg1, int arg2); // public void java.awt.Container.list(java.io.PrintWriter,int)
    virtual void print(java::awt::Graphics* arg1); // public void java.awt.Container.print(java.awt.Graphics)
    virtual void removeAll(); // public void java.awt.Container.removeAll()
    virtual void update(java::awt::Graphics* arg1); // public void java.awt.Container.update(java.awt.Graphics)
    virtual JavaObjectArray* getComponents(); // public java.awt.Component[] java.awt.Container.getComponents()
    virtual void setFont(java::awt::Font* arg1); // public void java.awt.Container.setFont(java.awt.Font)
    virtual void invalidate(); // public void java.awt.Container.invalidate()
    virtual void layout(); // public void java.awt.Container.layout()
    virtual void validate(); // public void java.awt.Container.validate()
    virtual void addNotify(); // public void java.awt.Container.addNotify()
    virtual void addPropertyChangeListener(const char* arg1, java::beans::PropertyChangeListener* arg2); // public void java.awt.Container.addPropertyChangeListener(java.lang.String,java.beans.PropertyChangeListener)
    virtual void addPropertyChangeListener(java::beans::PropertyChangeListener* arg1); // public void java.awt.Container.addPropertyChangeListener(java.beans.PropertyChangeListener)
    virtual void applyComponentOrientation(java::awt::ComponentOrientation* arg1); // public void java.awt.Container.applyComponentOrientation(java.awt.ComponentOrientation)
    virtual java::util::Set* getFocusTraversalKeys(int arg1); // public java.util.Set java.awt.Container.getFocusTraversalKeys(int)
    virtual java::awt::FocusTraversalPolicy* getFocusTraversalPolicy(); // public java.awt.FocusTraversalPolicy java.awt.Container.getFocusTraversalPolicy()
    virtual JavaObjectArray* getListeners(java::lang::Class* arg1); // public java.util.EventListener[] java.awt.Container.getListeners(java.lang.Class)
    virtual java::awt::Dimension* getMinimumSize(); // public java.awt.Dimension java.awt.Container.getMinimumSize()
    virtual java::awt::Dimension* getPreferredSize(); // public java.awt.Dimension java.awt.Container.getPreferredSize()
    virtual bool isFocusCycleRoot(); // public boolean java.awt.Container.isFocusCycleRoot()
    virtual bool isFocusCycleRoot(java::awt::Container* arg1); // public boolean java.awt.Container.isFocusCycleRoot(java.awt.Container)
    virtual void removeNotify(); // public void java.awt.Container.removeNotify()
    virtual void setFocusCycleRoot(bool arg1); // public void java.awt.Container.setFocusCycleRoot(boolean)
    virtual void setLayout(java::awt::LayoutManager* arg1); // public void java.awt.Container.setLayout(java.awt.LayoutManager)
    virtual void addContainerListener(java::awt::event::ContainerListener* arg1); // public synchronized void java.awt.Container.addContainerListener(java.awt.event.ContainerListener)
    virtual bool areFocusTraversalKeysSet(int arg1); // public boolean java.awt.Container.areFocusTraversalKeysSet(int)
    virtual int countComponents(); // public int java.awt.Container.countComponents()
    virtual void deliverEvent(java::awt::Event* arg1); // public void java.awt.Container.deliverEvent(java.awt.Event)
    virtual void doLayout(); // public void java.awt.Container.doLayout()
    virtual java::awt::Component* findComponentAt(int arg1, int arg2); // public java.awt.Component java.awt.Container.findComponentAt(int,int)
    virtual java::awt::Component* findComponentAt(java::awt::Point* arg1); // public java.awt.Component java.awt.Container.findComponentAt(java.awt.Point)
    virtual float getAlignmentX(); // public float java.awt.Container.getAlignmentX()
    virtual float getAlignmentY(); // public float java.awt.Container.getAlignmentY()
    virtual java::awt::Component* getComponent(int arg1); // public java.awt.Component java.awt.Container.getComponent(int)
    virtual java::awt::Component* getComponentAt(java::awt::Point* arg1); // public java.awt.Component java.awt.Container.getComponentAt(java.awt.Point)
    virtual java::awt::Component* getComponentAt(int arg1, int arg2); // public java.awt.Component java.awt.Container.getComponentAt(int,int)
    virtual int getComponentCount(); // public int java.awt.Container.getComponentCount()
    virtual int getComponentZOrder(java::awt::Component* arg1); // public int java.awt.Container.getComponentZOrder(java.awt.Component)
    virtual JavaObjectArray* getContainerListeners(); // public synchronized java.awt.event.ContainerListener[] java.awt.Container.getContainerListeners()
    virtual java::awt::Insets* getInsets(); // public java.awt.Insets java.awt.Container.getInsets()
    virtual java::awt::LayoutManager* getLayout(); // public java.awt.LayoutManager java.awt.Container.getLayout()
    virtual java::awt::Dimension* getMaximumSize(); // public java.awt.Dimension java.awt.Container.getMaximumSize()
    virtual java::awt::Point* getMousePosition(bool arg1); // public java.awt.Point java.awt.Container.getMousePosition(boolean) throws java.awt.HeadlessException
    virtual java::awt::Insets* insets(); // public java.awt.Insets java.awt.Container.insets()
    virtual bool isAncestorOf(java::awt::Component* arg1); // public boolean java.awt.Container.isAncestorOf(java.awt.Component)
    virtual bool isFocusTraversalPolicyProvider(); // public final boolean java.awt.Container.isFocusTraversalPolicyProvider()
    virtual bool isFocusTraversalPolicySet(); // public boolean java.awt.Container.isFocusTraversalPolicySet()
    virtual java::awt::Component* locate(int arg1, int arg2); // public java.awt.Component java.awt.Container.locate(int,int)
    virtual java::awt::Dimension* minimumSize(); // public java.awt.Dimension java.awt.Container.minimumSize()
    virtual void paint(java::awt::Graphics* arg1); // public void java.awt.Container.paint(java.awt.Graphics)
    virtual void paintComponents(java::awt::Graphics* arg1); // public void java.awt.Container.paintComponents(java.awt.Graphics)
    virtual java::awt::Dimension* preferredSize(); // public java.awt.Dimension java.awt.Container.preferredSize()
    virtual void printComponents(java::awt::Graphics* arg1); // public void java.awt.Container.printComponents(java.awt.Graphics)
    virtual void removeContainerListener(java::awt::event::ContainerListener* arg1); // public synchronized void java.awt.Container.removeContainerListener(java.awt.event.ContainerListener)
    virtual void setComponentZOrder(java::awt::Component* arg1, int arg2); // public void java.awt.Container.setComponentZOrder(java.awt.Component,int)
    virtual void setFocusTraversalKeys(int arg1, java::util::Set* arg2); // public void java.awt.Container.setFocusTraversalKeys(int,java.util.Set)
    virtual void setFocusTraversalPolicy(java::awt::FocusTraversalPolicy* arg1); // public void java.awt.Container.setFocusTraversalPolicy(java.awt.FocusTraversalPolicy)
    virtual void setFocusTraversalPolicyProvider(bool arg1); // public final void java.awt.Container.setFocusTraversalPolicyProvider(boolean)
    virtual void transferFocusBackward(); // public void java.awt.Container.transferFocusBackward()
    virtual void transferFocusDownCycle(); // public void java.awt.Container.transferFocusDownCycle()

};
}
}
#endif
