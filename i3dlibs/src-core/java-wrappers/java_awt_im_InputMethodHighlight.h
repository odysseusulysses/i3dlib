#ifndef java_awt_im_InputMethodHighlight_H
#define java_awt_im_InputMethodHighlight_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace util {
class Map;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace im {
class InputMethodHighlight : public java::lang::Object {
  public:
    InputMethodHighlight(JavaMarker* dummy);
    InputMethodHighlight(jobject obj);
    InputMethodHighlight(bool arg1, int arg2, int arg3); // public java.awt.im.InputMethodHighlight(boolean,int,int)
    InputMethodHighlight(bool arg1, int arg2, int arg3, java::util::Map* arg4); // public java.awt.im.InputMethodHighlight(boolean,int,int,java.util.Map)
    InputMethodHighlight(bool arg1, int arg2); // public java.awt.im.InputMethodHighlight(boolean,int)

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual int getState(); // public int java.awt.im.InputMethodHighlight.getState()
    virtual java::util::Map* getStyle(); // public java.util.Map java.awt.im.InputMethodHighlight.getStyle()
    virtual int getVariation(); // public int java.awt.im.InputMethodHighlight.getVariation()
    virtual bool isSelected(); // public boolean java.awt.im.InputMethodHighlight.isSelected()

    int RAW_TEXT; // public static final int java.awt.im.InputMethodHighlight.RAW_TEXT
    int CONVERTED_TEXT; // public static final int java.awt.im.InputMethodHighlight.CONVERTED_TEXT
    java::awt::im::InputMethodHighlight* UNSELECTED_RAW_TEXT_HIGHLIGHT; // public static final java.awt.im.InputMethodHighlight java.awt.im.InputMethodHighlight.UNSELECTED_RAW_TEXT_HIGHLIGHT
    java::awt::im::InputMethodHighlight* SELECTED_RAW_TEXT_HIGHLIGHT; // public static final java.awt.im.InputMethodHighlight java.awt.im.InputMethodHighlight.SELECTED_RAW_TEXT_HIGHLIGHT
    java::awt::im::InputMethodHighlight* UNSELECTED_CONVERTED_TEXT_HIGHLIGHT; // public static final java.awt.im.InputMethodHighlight java.awt.im.InputMethodHighlight.UNSELECTED_CONVERTED_TEXT_HIGHLIGHT
    java::awt::im::InputMethodHighlight* SELECTED_CONVERTED_TEXT_HIGHLIGHT; // public static final java.awt.im.InputMethodHighlight java.awt.im.InputMethodHighlight.SELECTED_CONVERTED_TEXT_HIGHLIGHT
};
}
}
}
#endif
