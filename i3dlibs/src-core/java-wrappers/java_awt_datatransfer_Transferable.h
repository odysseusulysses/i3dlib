#ifndef java_awt_datatransfer_Transferable_H
#define java_awt_datatransfer_Transferable_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace awt {
namespace datatransfer {
class DataFlavor;
}
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace datatransfer {
class Transferable : public java::lang::Object {
  public:
    Transferable(JavaMarker* dummy);
    Transferable(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual java::lang::Object* getTransferData(java::awt::datatransfer::DataFlavor* arg1); // public abstract java.lang.Object java.awt.datatransfer.Transferable.getTransferData(java.awt.datatransfer.DataFlavor) throws java.awt.datatransfer.UnsupportedFlavorException,java.io.IOException
    virtual JavaObjectArray* getTransferDataFlavors(); // public abstract java.awt.datatransfer.DataFlavor[] java.awt.datatransfer.Transferable.getTransferDataFlavors()
    virtual bool isDataFlavorSupported(java::awt::datatransfer::DataFlavor* arg1); // public abstract boolean java.awt.datatransfer.Transferable.isDataFlavorSupported(java.awt.datatransfer.DataFlavor)

};
}
}
}
#endif
