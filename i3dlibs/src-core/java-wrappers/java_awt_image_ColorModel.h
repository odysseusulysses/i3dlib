#ifndef java_awt_image_ColorModel_H
#define java_awt_image_ColorModel_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace awt {
class Transparency;
}
}

namespace java {
namespace lang {
class String;
}
}

namespace java {
namespace awt {
namespace image {
class WritableRaster;
}
}
}

namespace java {
namespace awt {
namespace image {
class SampleModel;
}
}
}

namespace java {
namespace awt {
namespace color {
class ColorSpace;
}
}
}

namespace java {
namespace awt {
namespace image {
class Raster;
}
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace image {
class ColorModel : public java::lang::Object {
  public:
    ColorModel(JavaMarker* dummy);
    ColorModel(jobject obj);
    ColorModel(int arg1); // public java.awt.image.ColorModel(int)

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual int hashCode(); // public int java.awt.image.ColorModel.hashCode()
    virtual void finalize(); // public void java.awt.image.ColorModel.finalize()
    virtual bool equals(java::lang::Object* arg1); // public boolean java.awt.image.ColorModel.equals(java.lang.Object)
    virtual const char* toString(); // public java.lang.String java.awt.image.ColorModel.toString()
    virtual java::awt::image::ColorModel* coerceData(java::awt::image::WritableRaster* arg1, bool arg2); // public java.awt.image.ColorModel java.awt.image.ColorModel.coerceData(java.awt.image.WritableRaster,boolean)
    virtual java::awt::image::SampleModel* createCompatibleSampleModel(int arg1, int arg2); // public java.awt.image.SampleModel java.awt.image.ColorModel.createCompatibleSampleModel(int,int)
    virtual java::awt::image::WritableRaster* createCompatibleWritableRaster(int arg1, int arg2); // public java.awt.image.WritableRaster java.awt.image.ColorModel.createCompatibleWritableRaster(int,int)
    virtual java::awt::image::WritableRaster* getAlphaRaster(java::awt::image::WritableRaster* arg1); // public java.awt.image.WritableRaster java.awt.image.ColorModel.getAlphaRaster(java.awt.image.WritableRaster)
    virtual java::awt::color::ColorSpace* getColorSpace(); // public final java.awt.color.ColorSpace java.awt.image.ColorModel.getColorSpace()
    virtual JavaIntArray* getComponentSize(); // public int[] java.awt.image.ColorModel.getComponentSize()
    virtual int getComponentSize(int arg1); // public int java.awt.image.ColorModel.getComponentSize(int)
    virtual java::lang::Object* getDataElements(int arg1, java::lang::Object* arg2); // public java.lang.Object java.awt.image.ColorModel.getDataElements(int,java.lang.Object)
    virtual java::lang::Object* getDataElements(JavaFloatArray* arg1, int arg2, java::lang::Object* arg3); // public java.lang.Object java.awt.image.ColorModel.getDataElements(float[],int,java.lang.Object)
    virtual java::lang::Object* getDataElements(JavaIntArray* arg1, int arg2, java::lang::Object* arg3); // public java.lang.Object java.awt.image.ColorModel.getDataElements(int[],int,java.lang.Object)
    virtual int getNumComponents(); // public int java.awt.image.ColorModel.getNumComponents()
    virtual int getPixelSize(); // public int java.awt.image.ColorModel.getPixelSize()
    virtual int getRGB(java::lang::Object* arg1); // public int java.awt.image.ColorModel.getRGB(java.lang.Object)
    virtual int getRGB(int arg1); // public int java.awt.image.ColorModel.getRGB(int)
    virtual java::awt::image::ColorModel* getRGBdefault(); // public static java.awt.image.ColorModel java.awt.image.ColorModel.getRGBdefault()
    virtual int getTransparency(); // public int java.awt.image.ColorModel.getTransparency()
    virtual bool hasAlpha(); // public final boolean java.awt.image.ColorModel.hasAlpha()
    virtual bool isAlphaPremultiplied(); // public final boolean java.awt.image.ColorModel.isAlphaPremultiplied()
    virtual bool isCompatibleRaster(java::awt::image::Raster* arg1); // public boolean java.awt.image.ColorModel.isCompatibleRaster(java.awt.image.Raster)
    virtual int getAlpha(int arg1); // public abstract int java.awt.image.ColorModel.getAlpha(int)
    virtual int getAlpha(java::lang::Object* arg1); // public int java.awt.image.ColorModel.getAlpha(java.lang.Object)
    virtual int getBlue(java::lang::Object* arg1); // public int java.awt.image.ColorModel.getBlue(java.lang.Object)
    virtual int getBlue(int arg1); // public abstract int java.awt.image.ColorModel.getBlue(int)
    virtual JavaIntArray* getComponents(java::lang::Object* arg1, JavaIntArray* arg2, int arg3); // public int[] java.awt.image.ColorModel.getComponents(java.lang.Object,int[],int)
    virtual JavaIntArray* getComponents(int arg1, JavaIntArray* arg2, int arg3); // public int[] java.awt.image.ColorModel.getComponents(int,int[],int)
    virtual int getDataElement(JavaFloatArray* arg1, int arg2); // public int java.awt.image.ColorModel.getDataElement(float[],int)
    virtual int getDataElement(JavaIntArray* arg1, int arg2); // public int java.awt.image.ColorModel.getDataElement(int[],int)
    virtual int getGreen(java::lang::Object* arg1); // public int java.awt.image.ColorModel.getGreen(java.lang.Object)
    virtual int getGreen(int arg1); // public abstract int java.awt.image.ColorModel.getGreen(int)
    virtual int getRed(int arg1); // public abstract int java.awt.image.ColorModel.getRed(int)
    virtual int getRed(java::lang::Object* arg1); // public int java.awt.image.ColorModel.getRed(java.lang.Object)
    virtual int getTransferType(); // public final int java.awt.image.ColorModel.getTransferType()
    virtual bool isCompatibleSampleModel(java::awt::image::SampleModel* arg1); // public boolean java.awt.image.ColorModel.isCompatibleSampleModel(java.awt.image.SampleModel)
    virtual JavaFloatArray* getNormalizedComponents(JavaIntArray* arg1, int arg2, JavaFloatArray* arg3, int arg4); // public float[] java.awt.image.ColorModel.getNormalizedComponents(int[],int,float[],int)
    virtual JavaFloatArray* getNormalizedComponents(java::lang::Object* arg1, JavaFloatArray* arg2, int arg3); // public float[] java.awt.image.ColorModel.getNormalizedComponents(java.lang.Object,float[],int)
    virtual int getNumColorComponents(); // public int java.awt.image.ColorModel.getNumColorComponents()
    virtual JavaIntArray* getUnnormalizedComponents(JavaFloatArray* arg1, int arg2, JavaIntArray* arg3, int arg4); // public int[] java.awt.image.ColorModel.getUnnormalizedComponents(float[],int,int[],int)

};
}
}
}
#endif
