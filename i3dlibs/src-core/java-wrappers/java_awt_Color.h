#ifndef java_awt_Color_H
#define java_awt_Color_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace awt {
class Paint;
}
}

namespace java {
namespace io {
class Serializable;
}
}

namespace java {
namespace awt {
namespace color {
class ColorSpace;
}
}
}

namespace java {
namespace lang {
class String;
}
}

namespace java {
namespace awt {
class PaintContext;
}
}

namespace java {
namespace awt {
namespace image {
class ColorModel;
}
}
}

namespace java {
namespace awt {
class Rectangle;
}
}

namespace java {
namespace awt {
namespace geom {
class Rectangle2D;
}
}
}

namespace java {
namespace awt {
namespace geom {
class AffineTransform;
}
}
}

namespace java {
namespace awt {
class RenderingHints;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
class Color : public java::lang::Object {
  public:
    Color(JavaMarker* dummy);
    Color(jobject obj);
    Color(float arg1, float arg2, float arg3, float arg4); // public java.awt.Color(float,float,float,float)
    Color(int arg1); // public java.awt.Color(int)
    Color(int arg1, int arg2, int arg3); // public java.awt.Color(int,int,int)
    Color(int arg1, int arg2, int arg3, int arg4); // public java.awt.Color(int,int,int,int)
    Color(java::awt::color::ColorSpace* arg1, JavaFloatArray* arg2, float arg3); // public java.awt.Color(java.awt.color.ColorSpace,float[],float)
    Color(int arg1, bool arg2); // public java.awt.Color(int,boolean)
    Color(float arg1, float arg2, float arg3); // public java.awt.Color(float,float,float)

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual int hashCode(); // public int java.awt.Color.hashCode()
    virtual bool equals(java::lang::Object* arg1); // public boolean java.awt.Color.equals(java.lang.Object)
    virtual const char* toString(); // public java.lang.String java.awt.Color.toString()
    virtual java::awt::Color* decode(const char* arg1); // public static java.awt.Color java.awt.Color.decode(java.lang.String) throws java.lang.NumberFormatException
    virtual java::awt::color::ColorSpace* getColorSpace(); // public java.awt.color.ColorSpace java.awt.Color.getColorSpace()
    virtual int getRGB(); // public int java.awt.Color.getRGB()
    virtual int getTransparency(); // public int java.awt.Color.getTransparency()
    virtual int getAlpha(); // public int java.awt.Color.getAlpha()
    virtual int getBlue(); // public int java.awt.Color.getBlue()
    virtual JavaFloatArray* getComponents(JavaFloatArray* arg1); // public float[] java.awt.Color.getComponents(float[])
    virtual JavaFloatArray* getComponents(java::awt::color::ColorSpace* arg1, JavaFloatArray* arg2); // public float[] java.awt.Color.getComponents(java.awt.color.ColorSpace,float[])
    virtual int getGreen(); // public int java.awt.Color.getGreen()
    virtual int getRed(); // public int java.awt.Color.getRed()
    virtual java::awt::Color* brighter(); // public java.awt.Color java.awt.Color.brighter()
    virtual java::awt::Color* darker(); // public java.awt.Color java.awt.Color.darker()
    virtual java::awt::Color* getColor(const char* arg1); // public static java.awt.Color java.awt.Color.getColor(java.lang.String)
    virtual java::awt::Color* getColor(const char* arg1, java::awt::Color* arg2); // public static java.awt.Color java.awt.Color.getColor(java.lang.String,java.awt.Color)
    virtual java::awt::Color* getColor(const char* arg1, int arg2); // public static java.awt.Color java.awt.Color.getColor(java.lang.String,int)
    virtual int HSBtoRGB(float arg1, float arg2, float arg3); // public static int java.awt.Color.HSBtoRGB(float,float,float)
    virtual JavaFloatArray* RGBtoHSB(int arg1, int arg2, int arg3, JavaFloatArray* arg4); // public static float[] java.awt.Color.RGBtoHSB(int,int,int,float[])
    virtual java::awt::PaintContext* createContext(java::awt::image::ColorModel* arg1, java::awt::Rectangle* arg2, java::awt::geom::Rectangle2D* arg3, java::awt::geom::AffineTransform* arg4, java::awt::RenderingHints* arg5); // public synchronized java.awt.PaintContext java.awt.Color.createContext(java.awt.image.ColorModel,java.awt.Rectangle,java.awt.geom.Rectangle2D,java.awt.geom.AffineTransform,java.awt.RenderingHints)
    virtual JavaFloatArray* getColorComponents(JavaFloatArray* arg1); // public float[] java.awt.Color.getColorComponents(float[])
    virtual JavaFloatArray* getColorComponents(java::awt::color::ColorSpace* arg1, JavaFloatArray* arg2); // public float[] java.awt.Color.getColorComponents(java.awt.color.ColorSpace,float[])
    virtual java::awt::Color* getHSBColor(float arg1, float arg2, float arg3); // public static java.awt.Color java.awt.Color.getHSBColor(float,float,float)
    virtual JavaFloatArray* getRGBColorComponents(JavaFloatArray* arg1); // public float[] java.awt.Color.getRGBColorComponents(float[])
    virtual JavaFloatArray* getRGBComponents(JavaFloatArray* arg1); // public float[] java.awt.Color.getRGBComponents(float[])

    java::awt::Color* white; // public static final java.awt.Color java.awt.Color.white
    java::awt::Color* WHITE; // public static final java.awt.Color java.awt.Color.WHITE
    java::awt::Color* lightGray; // public static final java.awt.Color java.awt.Color.lightGray
    java::awt::Color* LIGHT_GRAY; // public static final java.awt.Color java.awt.Color.LIGHT_GRAY
    java::awt::Color* gray; // public static final java.awt.Color java.awt.Color.gray
    java::awt::Color* GRAY; // public static final java.awt.Color java.awt.Color.GRAY
    java::awt::Color* darkGray; // public static final java.awt.Color java.awt.Color.darkGray
    java::awt::Color* DARK_GRAY; // public static final java.awt.Color java.awt.Color.DARK_GRAY
    java::awt::Color* black; // public static final java.awt.Color java.awt.Color.black
    java::awt::Color* BLACK; // public static final java.awt.Color java.awt.Color.BLACK
    java::awt::Color* red; // public static final java.awt.Color java.awt.Color.red
    java::awt::Color* RED; // public static final java.awt.Color java.awt.Color.RED
    java::awt::Color* pink; // public static final java.awt.Color java.awt.Color.pink
    java::awt::Color* PINK; // public static final java.awt.Color java.awt.Color.PINK
    java::awt::Color* orange; // public static final java.awt.Color java.awt.Color.orange
    java::awt::Color* ORANGE; // public static final java.awt.Color java.awt.Color.ORANGE
    java::awt::Color* yellow; // public static final java.awt.Color java.awt.Color.yellow
    java::awt::Color* YELLOW; // public static final java.awt.Color java.awt.Color.YELLOW
    java::awt::Color* green; // public static final java.awt.Color java.awt.Color.green
    java::awt::Color* GREEN; // public static final java.awt.Color java.awt.Color.GREEN
    java::awt::Color* magenta; // public static final java.awt.Color java.awt.Color.magenta
    java::awt::Color* MAGENTA; // public static final java.awt.Color java.awt.Color.MAGENTA
    java::awt::Color* cyan; // public static final java.awt.Color java.awt.Color.cyan
    java::awt::Color* CYAN; // public static final java.awt.Color java.awt.Color.CYAN
    java::awt::Color* blue; // public static final java.awt.Color java.awt.Color.blue
    java::awt::Color* BLUE; // public static final java.awt.Color java.awt.Color.BLUE
};
}
}
#endif
