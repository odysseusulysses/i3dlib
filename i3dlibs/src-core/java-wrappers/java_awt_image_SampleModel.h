#ifndef java_awt_image_SampleModel_H
#define java_awt_image_SampleModel_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace awt {
namespace image {
class DataBuffer;
}
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace image {
class SampleModel : public java::lang::Object {
  public:
    SampleModel(JavaMarker* dummy);
    SampleModel(jobject obj);
    SampleModel(int arg1, int arg2, int arg3, int arg4); // public java.awt.image.SampleModel(int,int,int,int)

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual JavaFloatArray* getSamples(int arg1, int arg2, int arg3, int arg4, int arg5, JavaFloatArray* arg6, java::awt::image::DataBuffer* arg7); // public float[] java.awt.image.SampleModel.getSamples(int,int,int,int,int,float[],java.awt.image.DataBuffer)
    virtual JavaDoubleArray* getSamples(int arg1, int arg2, int arg3, int arg4, int arg5, JavaDoubleArray* arg6, java::awt::image::DataBuffer* arg7); // public double[] java.awt.image.SampleModel.getSamples(int,int,int,int,int,double[],java.awt.image.DataBuffer)
    virtual JavaIntArray* getSamples(int arg1, int arg2, int arg3, int arg4, int arg5, JavaIntArray* arg6, java::awt::image::DataBuffer* arg7); // public int[] java.awt.image.SampleModel.getSamples(int,int,int,int,int,int[],java.awt.image.DataBuffer)
    virtual java::awt::image::SampleModel* createCompatibleSampleModel(int arg1, int arg2); // public abstract java.awt.image.SampleModel java.awt.image.SampleModel.createCompatibleSampleModel(int,int)
    virtual java::lang::Object* getDataElements(int arg1, int arg2, int arg3, int arg4, java::lang::Object* arg5, java::awt::image::DataBuffer* arg6); // public java.lang.Object java.awt.image.SampleModel.getDataElements(int,int,int,int,java.lang.Object,java.awt.image.DataBuffer)
    virtual java::lang::Object* getDataElements(int arg1, int arg2, java::lang::Object* arg3, java::awt::image::DataBuffer* arg4); // public abstract java.lang.Object java.awt.image.SampleModel.getDataElements(int,int,java.lang.Object,java.awt.image.DataBuffer)
    virtual int getDataType(); // public final int java.awt.image.SampleModel.getDataType()
    virtual int getHeight(); // public final int java.awt.image.SampleModel.getHeight()
    virtual int getNumBands(); // public final int java.awt.image.SampleModel.getNumBands()
    virtual JavaIntArray* getPixels(int arg1, int arg2, int arg3, int arg4, JavaIntArray* arg5, java::awt::image::DataBuffer* arg6); // public int[] java.awt.image.SampleModel.getPixels(int,int,int,int,int[],java.awt.image.DataBuffer)
    virtual JavaDoubleArray* getPixels(int arg1, int arg2, int arg3, int arg4, JavaDoubleArray* arg5, java::awt::image::DataBuffer* arg6); // public double[] java.awt.image.SampleModel.getPixels(int,int,int,int,double[],java.awt.image.DataBuffer)
    virtual JavaFloatArray* getPixels(int arg1, int arg2, int arg3, int arg4, JavaFloatArray* arg5, java::awt::image::DataBuffer* arg6); // public float[] java.awt.image.SampleModel.getPixels(int,int,int,int,float[],java.awt.image.DataBuffer)
    virtual int getWidth(); // public final int java.awt.image.SampleModel.getWidth()
    virtual void setDataElements(int arg1, int arg2, int arg3, int arg4, java::lang::Object* arg5, java::awt::image::DataBuffer* arg6); // public void java.awt.image.SampleModel.setDataElements(int,int,int,int,java.lang.Object,java.awt.image.DataBuffer)
    virtual void setDataElements(int arg1, int arg2, java::lang::Object* arg3, java::awt::image::DataBuffer* arg4); // public abstract void java.awt.image.SampleModel.setDataElements(int,int,java.lang.Object,java.awt.image.DataBuffer)
    virtual void setPixels(int arg1, int arg2, int arg3, int arg4, JavaIntArray* arg5, java::awt::image::DataBuffer* arg6); // public void java.awt.image.SampleModel.setPixels(int,int,int,int,int[],java.awt.image.DataBuffer)
    virtual void setPixels(int arg1, int arg2, int arg3, int arg4, JavaDoubleArray* arg5, java::awt::image::DataBuffer* arg6); // public void java.awt.image.SampleModel.setPixels(int,int,int,int,double[],java.awt.image.DataBuffer)
    virtual void setPixels(int arg1, int arg2, int arg3, int arg4, JavaFloatArray* arg5, java::awt::image::DataBuffer* arg6); // public void java.awt.image.SampleModel.setPixels(int,int,int,int,float[],java.awt.image.DataBuffer)
    virtual int getSampleSize(int arg1); // public abstract int java.awt.image.SampleModel.getSampleSize(int)
    virtual JavaIntArray* getSampleSize(); // public abstract int[] java.awt.image.SampleModel.getSampleSize()
    virtual int getTransferType(); // public int java.awt.image.SampleModel.getTransferType()
    virtual java::awt::image::DataBuffer* createDataBuffer(); // public abstract java.awt.image.DataBuffer java.awt.image.SampleModel.createDataBuffer()
    virtual java::awt::image::SampleModel* createSubsetSampleModel(JavaIntArray* arg1); // public abstract java.awt.image.SampleModel java.awt.image.SampleModel.createSubsetSampleModel(int[])
    virtual void setPixel(int arg1, int arg2, JavaDoubleArray* arg3, java::awt::image::DataBuffer* arg4); // public void java.awt.image.SampleModel.setPixel(int,int,double[],java.awt.image.DataBuffer)
    virtual void setPixel(int arg1, int arg2, JavaFloatArray* arg3, java::awt::image::DataBuffer* arg4); // public void java.awt.image.SampleModel.setPixel(int,int,float[],java.awt.image.DataBuffer)
    virtual void setPixel(int arg1, int arg2, JavaIntArray* arg3, java::awt::image::DataBuffer* arg4); // public void java.awt.image.SampleModel.setPixel(int,int,int[],java.awt.image.DataBuffer)
    virtual void setSample(int arg1, int arg2, int arg3, int arg4, java::awt::image::DataBuffer* arg5); // public abstract void java.awt.image.SampleModel.setSample(int,int,int,int,java.awt.image.DataBuffer)
    virtual void setSample(int arg1, int arg2, int arg3, float arg4, java::awt::image::DataBuffer* arg5); // public void java.awt.image.SampleModel.setSample(int,int,int,float,java.awt.image.DataBuffer)
    virtual void setSample(int arg1, int arg2, int arg3, double arg4, java::awt::image::DataBuffer* arg5); // public void java.awt.image.SampleModel.setSample(int,int,int,double,java.awt.image.DataBuffer)
    virtual void setSamples(int arg1, int arg2, int arg3, int arg4, int arg5, JavaDoubleArray* arg6, java::awt::image::DataBuffer* arg7); // public void java.awt.image.SampleModel.setSamples(int,int,int,int,int,double[],java.awt.image.DataBuffer)
    virtual void setSamples(int arg1, int arg2, int arg3, int arg4, int arg5, JavaIntArray* arg6, java::awt::image::DataBuffer* arg7); // public void java.awt.image.SampleModel.setSamples(int,int,int,int,int,int[],java.awt.image.DataBuffer)
    virtual void setSamples(int arg1, int arg2, int arg3, int arg4, int arg5, JavaFloatArray* arg6, java::awt::image::DataBuffer* arg7); // public void java.awt.image.SampleModel.setSamples(int,int,int,int,int,float[],java.awt.image.DataBuffer)
    virtual int getNumDataElements(); // public abstract int java.awt.image.SampleModel.getNumDataElements()
    virtual JavaIntArray* getPixel(int arg1, int arg2, JavaIntArray* arg3, java::awt::image::DataBuffer* arg4); // public int[] java.awt.image.SampleModel.getPixel(int,int,int[],java.awt.image.DataBuffer)
    virtual JavaDoubleArray* getPixel(int arg1, int arg2, JavaDoubleArray* arg3, java::awt::image::DataBuffer* arg4); // public double[] java.awt.image.SampleModel.getPixel(int,int,double[],java.awt.image.DataBuffer)
    virtual JavaFloatArray* getPixel(int arg1, int arg2, JavaFloatArray* arg3, java::awt::image::DataBuffer* arg4); // public float[] java.awt.image.SampleModel.getPixel(int,int,float[],java.awt.image.DataBuffer)
    virtual int getSample(int arg1, int arg2, int arg3, java::awt::image::DataBuffer* arg4); // public abstract int java.awt.image.SampleModel.getSample(int,int,int,java.awt.image.DataBuffer)
    virtual double getSampleDouble(int arg1, int arg2, int arg3, java::awt::image::DataBuffer* arg4); // public double java.awt.image.SampleModel.getSampleDouble(int,int,int,java.awt.image.DataBuffer)
    virtual float getSampleFloat(int arg1, int arg2, int arg3, java::awt::image::DataBuffer* arg4); // public float java.awt.image.SampleModel.getSampleFloat(int,int,int,java.awt.image.DataBuffer)

};
}
}
}
#endif
