#ifndef java_awt_MenuComponent_H
#define java_awt_MenuComponent_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace io {
class Serializable;
}
}

namespace java {
namespace lang {
class String;
}
}

namespace java {
namespace awt {
class MenuContainer;
}
}

namespace java {
namespace awt {
class Font;
}
}

namespace java {
namespace awt {
namespace peer {
class MenuComponentPeer;
}
}
}

namespace javax {
namespace accessibility {
class AccessibleContext;
}
}

namespace java {
namespace awt {
class Event;
}
}

namespace java {
namespace awt {
class AWTEvent;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
class MenuComponent : public java::lang::Object {
  public:
    MenuComponent(JavaMarker* dummy);
    MenuComponent(jobject obj);
    MenuComponent(); // public java.awt.MenuComponent() throws java.awt.HeadlessException

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual const char* toString(); // public java.lang.String java.awt.MenuComponent.toString()
    virtual const char* getName(); // public java.lang.String java.awt.MenuComponent.getName()
    virtual java::awt::MenuContainer* getParent(); // public java.awt.MenuContainer java.awt.MenuComponent.getParent()
    virtual void setName(const char* arg1); // public void java.awt.MenuComponent.setName(java.lang.String)
    virtual java::awt::Font* getFont(); // public java.awt.Font java.awt.MenuComponent.getFont()
    virtual void setFont(java::awt::Font* arg1); // public void java.awt.MenuComponent.setFont(java.awt.Font)
    virtual java::awt::peer::MenuComponentPeer* getPeer(); // public java.awt.peer.MenuComponentPeer java.awt.MenuComponent.getPeer()
    virtual javax::accessibility::AccessibleContext* getAccessibleContext(); // public javax.accessibility.AccessibleContext java.awt.MenuComponent.getAccessibleContext()
    virtual bool postEvent(java::awt::Event* arg1); // public boolean java.awt.MenuComponent.postEvent(java.awt.Event)
    virtual void removeNotify(); // public void java.awt.MenuComponent.removeNotify()
    virtual void dispatchEvent(java::awt::AWTEvent* arg1); // public final void java.awt.MenuComponent.dispatchEvent(java.awt.AWTEvent)

};
}
}
#endif
