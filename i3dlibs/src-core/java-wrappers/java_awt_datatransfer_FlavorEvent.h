#ifndef java_awt_datatransfer_FlavorEvent_H
#define java_awt_datatransfer_FlavorEvent_H
#include <jni.h>
#include <java_marker.h>
#include <java_util_EventObject.h>

namespace java {
namespace awt {
namespace datatransfer {
class Clipboard;
}
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace datatransfer {
class FlavorEvent : public java::util::EventObject {
  public:
    FlavorEvent(JavaMarker* dummy);
    FlavorEvent(jobject obj);
    FlavorEvent(java::awt::datatransfer::Clipboard* arg1); // public java.awt.datatransfer.FlavorEvent(java.awt.datatransfer.Clipboard)

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);

};
}
}
}
#endif
