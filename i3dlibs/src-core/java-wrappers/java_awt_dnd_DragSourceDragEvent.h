#ifndef java_awt_dnd_DragSourceDragEvent_H
#define java_awt_dnd_DragSourceDragEvent_H
#include <jni.h>
#include <java_marker.h>
#include <java_awt_dnd_DragSourceEvent.h>

namespace java {
namespace awt {
namespace dnd {
class DragSourceContext;
}
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace dnd {
class DragSourceDragEvent : public java::awt::dnd::DragSourceEvent {
  public:
    DragSourceDragEvent(JavaMarker* dummy);
    DragSourceDragEvent(jobject obj);
    DragSourceDragEvent(java::awt::dnd::DragSourceContext* arg1, int arg2, int arg3, int arg4, int arg5, int arg6); // public java.awt.dnd.DragSourceDragEvent(java.awt.dnd.DragSourceContext,int,int,int,int,int)
    DragSourceDragEvent(java::awt::dnd::DragSourceContext* arg1, int arg2, int arg3, int arg4); // public java.awt.dnd.DragSourceDragEvent(java.awt.dnd.DragSourceContext,int,int,int)

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual int getTargetActions(); // public int java.awt.dnd.DragSourceDragEvent.getTargetActions()
    virtual int getDropAction(); // public int java.awt.dnd.DragSourceDragEvent.getDropAction()
    virtual int getGestureModifiers(); // public int java.awt.dnd.DragSourceDragEvent.getGestureModifiers()
    virtual int getGestureModifiersEx(); // public int java.awt.dnd.DragSourceDragEvent.getGestureModifiersEx()
    virtual int getUserAction(); // public int java.awt.dnd.DragSourceDragEvent.getUserAction()

};
}
}
}
#endif
