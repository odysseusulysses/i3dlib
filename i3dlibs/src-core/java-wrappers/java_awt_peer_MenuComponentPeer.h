#ifndef java_awt_peer_MenuComponentPeer_H
#define java_awt_peer_MenuComponentPeer_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace awt {
class Font;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace peer {
class MenuComponentPeer : public java::lang::Object {
  public:
    MenuComponentPeer(JavaMarker* dummy);
    MenuComponentPeer(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual void dispose(); // public abstract void java.awt.peer.MenuComponentPeer.dispose()
    virtual void setFont(java::awt::Font* arg1); // public abstract void java.awt.peer.MenuComponentPeer.setFont(java.awt.Font)

};
}
}
}
#endif
