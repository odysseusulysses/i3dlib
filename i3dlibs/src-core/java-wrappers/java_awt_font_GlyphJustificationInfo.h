#ifndef java_awt_font_GlyphJustificationInfo_H
#define java_awt_font_GlyphJustificationInfo_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace font {
class GlyphJustificationInfo : public java::lang::Object {
  public:
    GlyphJustificationInfo(JavaMarker* dummy);
    GlyphJustificationInfo(jobject obj);
    GlyphJustificationInfo(float arg1, bool arg2, int arg3, float arg4, float arg5, bool arg6, int arg7, float arg8, float arg9); // public java.awt.font.GlyphJustificationInfo(float,boolean,int,float,float,boolean,int,float,float)

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);

    int PRIORITY_KASHIDA; // public static final int java.awt.font.GlyphJustificationInfo.PRIORITY_KASHIDA
    int PRIORITY_WHITESPACE; // public static final int java.awt.font.GlyphJustificationInfo.PRIORITY_WHITESPACE
    int PRIORITY_INTERCHAR; // public static final int java.awt.font.GlyphJustificationInfo.PRIORITY_INTERCHAR
    int PRIORITY_NONE; // public static final int java.awt.font.GlyphJustificationInfo.PRIORITY_NONE
    float weight; // public final float java.awt.font.GlyphJustificationInfo.weight
    int growPriority; // public final int java.awt.font.GlyphJustificationInfo.growPriority
    bool growAbsorb; // public final boolean java.awt.font.GlyphJustificationInfo.growAbsorb
    float growLeftLimit; // public final float java.awt.font.GlyphJustificationInfo.growLeftLimit
    float growRightLimit; // public final float java.awt.font.GlyphJustificationInfo.growRightLimit
    int shrinkPriority; // public final int java.awt.font.GlyphJustificationInfo.shrinkPriority
    bool shrinkAbsorb; // public final boolean java.awt.font.GlyphJustificationInfo.shrinkAbsorb
    float shrinkLeftLimit; // public final float java.awt.font.GlyphJustificationInfo.shrinkLeftLimit
    float shrinkRightLimit; // public final float java.awt.font.GlyphJustificationInfo.shrinkRightLimit
};
}
}
}
#endif
