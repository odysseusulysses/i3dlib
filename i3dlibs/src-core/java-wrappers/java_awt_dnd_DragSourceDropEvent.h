#ifndef java_awt_dnd_DragSourceDropEvent_H
#define java_awt_dnd_DragSourceDropEvent_H
#include <jni.h>
#include <java_marker.h>
#include <java_awt_dnd_DragSourceEvent.h>

namespace java {
namespace awt {
namespace dnd {
class DragSourceContext;
}
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace dnd {
class DragSourceDropEvent : public java::awt::dnd::DragSourceEvent {
  public:
    DragSourceDropEvent(JavaMarker* dummy);
    DragSourceDropEvent(jobject obj);
    DragSourceDropEvent(java::awt::dnd::DragSourceContext* arg1, int arg2, bool arg3); // public java.awt.dnd.DragSourceDropEvent(java.awt.dnd.DragSourceContext,int,boolean)
    DragSourceDropEvent(java::awt::dnd::DragSourceContext* arg1, int arg2, bool arg3, int arg4, int arg5); // public java.awt.dnd.DragSourceDropEvent(java.awt.dnd.DragSourceContext,int,boolean,int,int)
    DragSourceDropEvent(java::awt::dnd::DragSourceContext* arg1); // public java.awt.dnd.DragSourceDropEvent(java.awt.dnd.DragSourceContext)

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual int getDropAction(); // public int java.awt.dnd.DragSourceDropEvent.getDropAction()
    virtual bool getDropSuccess(); // public boolean java.awt.dnd.DragSourceDropEvent.getDropSuccess()

};
}
}
}
#endif
