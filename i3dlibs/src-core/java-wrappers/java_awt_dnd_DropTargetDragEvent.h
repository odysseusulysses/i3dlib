#ifndef java_awt_dnd_DropTargetDragEvent_H
#define java_awt_dnd_DropTargetDragEvent_H
#include <jni.h>
#include <java_marker.h>
#include <java_awt_dnd_DropTargetEvent.h>

namespace java {
namespace awt {
namespace dnd {
class DropTargetContext;
}
}
}

namespace java {
namespace awt {
class Point;
}
}

namespace java {
namespace awt {
namespace datatransfer {
class DataFlavor;
}
}
}

namespace java {
namespace util {
class List;
}
}

namespace java {
namespace awt {
namespace datatransfer {
class Transferable;
}
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace dnd {
class DropTargetDragEvent : public java::awt::dnd::DropTargetEvent {
  public:
    DropTargetDragEvent(JavaMarker* dummy);
    DropTargetDragEvent(jobject obj);
    DropTargetDragEvent(java::awt::dnd::DropTargetContext* arg1, java::awt::Point* arg2, int arg3, int arg4); // public java.awt.dnd.DropTargetDragEvent(java.awt.dnd.DropTargetContext,java.awt.Point,int,int)

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual java::awt::Point* getLocation(); // public java.awt.Point java.awt.dnd.DropTargetDragEvent.getLocation()
    virtual int getSourceActions(); // public int java.awt.dnd.DropTargetDragEvent.getSourceActions()
    virtual bool isDataFlavorSupported(java::awt::datatransfer::DataFlavor* arg1); // public boolean java.awt.dnd.DropTargetDragEvent.isDataFlavorSupported(java.awt.datatransfer.DataFlavor)
    virtual void acceptDrag(int arg1); // public void java.awt.dnd.DropTargetDragEvent.acceptDrag(int)
    virtual JavaObjectArray* getCurrentDataFlavors(); // public java.awt.datatransfer.DataFlavor[] java.awt.dnd.DropTargetDragEvent.getCurrentDataFlavors()
    virtual java::util::List* getCurrentDataFlavorsAsList(); // public java.util.List java.awt.dnd.DropTargetDragEvent.getCurrentDataFlavorsAsList()
    virtual java::awt::datatransfer::Transferable* getTransferable(); // public java.awt.datatransfer.Transferable java.awt.dnd.DropTargetDragEvent.getTransferable()
    virtual void rejectDrag(); // public void java.awt.dnd.DropTargetDragEvent.rejectDrag()
    virtual int getDropAction(); // public int java.awt.dnd.DropTargetDragEvent.getDropAction()

};
}
}
}
#endif
