#ifndef java_awt_event_MouseListener_H
#define java_awt_event_MouseListener_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace util {
class EventListener;
}
}

namespace java {
namespace awt {
namespace event {
class MouseEvent;
}
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace event {
class MouseListener : public java::lang::Object {
  public:
    MouseListener(JavaMarker* dummy);
    MouseListener(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual void mouseClicked(java::awt::event::MouseEvent* arg1); // public abstract void java.awt.event.MouseListener.mouseClicked(java.awt.event.MouseEvent)
    virtual void mouseEntered(java::awt::event::MouseEvent* arg1); // public abstract void java.awt.event.MouseListener.mouseEntered(java.awt.event.MouseEvent)
    virtual void mouseExited(java::awt::event::MouseEvent* arg1); // public abstract void java.awt.event.MouseListener.mouseExited(java.awt.event.MouseEvent)
    virtual void mousePressed(java::awt::event::MouseEvent* arg1); // public abstract void java.awt.event.MouseListener.mousePressed(java.awt.event.MouseEvent)
    virtual void mouseReleased(java::awt::event::MouseEvent* arg1); // public abstract void java.awt.event.MouseListener.mouseReleased(java.awt.event.MouseEvent)

};
}
}
}
#endif
