#ifndef java_awt_Window_H
#define java_awt_Window_H
#include <jni.h>
#include <java_marker.h>
#include <java_awt_Container.h>

namespace javax {
namespace accessibility {
class Accessible;
}
}

namespace java {
namespace awt {
class GraphicsConfiguration;
}
}

namespace java {
namespace awt {
class Frame;
}
}

namespace java {
namespace awt {
class Dimension;
}
}

namespace java {
namespace awt {
class Rectangle;
}
}

namespace java {
namespace util {
class Locale;
}
}

namespace java {
namespace lang {
class String;
}
}

namespace java {
namespace beans {
class PropertyChangeListener;
}
}

namespace java {
namespace awt {
namespace event {
class WindowFocusListener;
}
}
}

namespace java {
namespace awt {
namespace event {
class WindowListener;
}
}
}

namespace java {
namespace awt {
namespace event {
class WindowStateListener;
}
}
}

namespace java {
namespace util {
class ResourceBundle;
}
}

namespace java {
namespace awt {
class BufferCapabilities;
}
}

namespace javax {
namespace accessibility {
class AccessibleContext;
}
}

namespace java {
namespace awt {
namespace image {
class BufferStrategy;
}
}
}

namespace java {
namespace awt {
class Component;
}
}

namespace java {
namespace util {
class Set;
}
}

namespace java {
namespace util {
class List;
}
}

namespace java {
namespace awt {
namespace im {
class InputContext;
}
}
}

namespace java {
namespace util {
class EventListener;
}
}

namespace java {
namespace lang {
class Class;
}
}

namespace java {
namespace awt {
class Dialog__ModalExclusionType;
}
}

namespace java {
namespace awt {
class Toolkit;
}
}

namespace java {
namespace awt {
class Event;
}
}

namespace java {
namespace awt {
class Cursor;
}
}

namespace java {
namespace awt {
class Image;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
class Window : public java::awt::Container {
  public:
    Window(JavaMarker* dummy);
    Window(jobject obj);
    Window(java::awt::Window* arg1, java::awt::GraphicsConfiguration* arg2); // public java.awt.Window(java.awt.Window,java.awt.GraphicsConfiguration)
    Window(java::awt::Window* arg1); // public java.awt.Window(java.awt.Window)
    Window(java::awt::Frame* arg1); // public java.awt.Window(java.awt.Frame)

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual void setSize(int arg1, int arg2); // public void java.awt.Window.setSize(int,int)
    virtual void setSize(java::awt::Dimension* arg1); // public void java.awt.Window.setSize(java.awt.Dimension)
    virtual java::awt::Window* getOwner(); // public java.awt.Window java.awt.Window.getOwner()
    virtual void reshape(int arg1, int arg2, int arg3, int arg4); // public void java.awt.Window.reshape(int,int,int,int)
    virtual void setBounds(int arg1, int arg2, int arg3, int arg4); // public void java.awt.Window.setBounds(int,int,int,int)
    virtual void setBounds(java::awt::Rectangle* arg1); // public void java.awt.Window.setBounds(java.awt.Rectangle)
    virtual void dispose(); // public void java.awt.Window.dispose()
    virtual java::util::Locale* getLocale(); // public java.util.Locale java.awt.Window.getLocale()
    virtual void setVisible(bool arg1); // public void java.awt.Window.setVisible(boolean)
    virtual void toFront(); // public void java.awt.Window.toFront()
    virtual void addNotify(); // public void java.awt.Window.addNotify()
    virtual void addPropertyChangeListener(const char* arg1, java::beans::PropertyChangeListener* arg2); // public void java.awt.Window.addPropertyChangeListener(java.lang.String,java.beans.PropertyChangeListener)
    virtual void addPropertyChangeListener(java::beans::PropertyChangeListener* arg1); // public void java.awt.Window.addPropertyChangeListener(java.beans.PropertyChangeListener)
    virtual void addWindowFocusListener(java::awt::event::WindowFocusListener* arg1); // public synchronized void java.awt.Window.addWindowFocusListener(java.awt.event.WindowFocusListener)
    virtual void addWindowListener(java::awt::event::WindowListener* arg1); // public synchronized void java.awt.Window.addWindowListener(java.awt.event.WindowListener)
    virtual void addWindowStateListener(java::awt::event::WindowStateListener* arg1); // public synchronized void java.awt.Window.addWindowStateListener(java.awt.event.WindowStateListener)
    virtual void applyResourceBundle(java::util::ResourceBundle* arg1); // public void java.awt.Window.applyResourceBundle(java.util.ResourceBundle)
    virtual void applyResourceBundle(const char* arg1); // public void java.awt.Window.applyResourceBundle(java.lang.String)
    virtual void createBufferStrategy(int arg1); // public void java.awt.Window.createBufferStrategy(int)
    virtual void createBufferStrategy(int arg1, java::awt::BufferCapabilities* arg2); // public void java.awt.Window.createBufferStrategy(int,java.awt.BufferCapabilities) throws java.awt.AWTException
    virtual javax::accessibility::AccessibleContext* getAccessibleContext(); // public javax.accessibility.AccessibleContext java.awt.Window.getAccessibleContext()
    virtual java::awt::image::BufferStrategy* getBufferStrategy(); // public java.awt.image.BufferStrategy java.awt.Window.getBufferStrategy()
    virtual java::awt::Container* getFocusCycleRootAncestor(); // public final java.awt.Container java.awt.Window.getFocusCycleRootAncestor()
    virtual java::awt::Component* getFocusOwner(); // public java.awt.Component java.awt.Window.getFocusOwner()
    virtual java::util::Set* getFocusTraversalKeys(int arg1); // public java.util.Set java.awt.Window.getFocusTraversalKeys(int)
    virtual bool getFocusableWindowState(); // public boolean java.awt.Window.getFocusableWindowState()
    virtual java::awt::GraphicsConfiguration* getGraphicsConfiguration(); // public java.awt.GraphicsConfiguration java.awt.Window.getGraphicsConfiguration()
    virtual java::util::List* getIconImages(); // public java.util.List java.awt.Window.getIconImages()
    virtual java::awt::im::InputContext* getInputContext(); // public java.awt.im.InputContext java.awt.Window.getInputContext()
    virtual JavaObjectArray* getListeners(java::lang::Class* arg1); // public java.util.EventListener[] java.awt.Window.getListeners(java.lang.Class)
    virtual java::awt::Dialog__ModalExclusionType* getModalExclusionType(); // public java.awt.Dialog$ModalExclusionType java.awt.Window.getModalExclusionType()
    virtual java::awt::Component* getMostRecentFocusOwner(); // public java.awt.Component java.awt.Window.getMostRecentFocusOwner()
    virtual JavaObjectArray* getOwnedWindows(); // public java.awt.Window[] java.awt.Window.getOwnedWindows()
    virtual JavaObjectArray* getOwnerlessWindows(); // public static java.awt.Window[] java.awt.Window.getOwnerlessWindows()
    virtual java::awt::Toolkit* getToolkit(); // public java.awt.Toolkit java.awt.Window.getToolkit()
    virtual const char* getWarningString(); // public final java.lang.String java.awt.Window.getWarningString()
    virtual JavaObjectArray* getWindowFocusListeners(); // public synchronized java.awt.event.WindowFocusListener[] java.awt.Window.getWindowFocusListeners()
    virtual JavaObjectArray* getWindowListeners(); // public synchronized java.awt.event.WindowListener[] java.awt.Window.getWindowListeners()
    virtual JavaObjectArray* getWindowStateListeners(); // public synchronized java.awt.event.WindowStateListener[] java.awt.Window.getWindowStateListeners()
    virtual JavaObjectArray* getWindows(); // public static java.awt.Window[] java.awt.Window.getWindows()
    virtual void hide(); // public void java.awt.Window.hide()
    virtual bool isActive(); // public boolean java.awt.Window.isActive()
    virtual bool isAlwaysOnTop(); // public final boolean java.awt.Window.isAlwaysOnTop()
    virtual bool isAlwaysOnTopSupported(); // public boolean java.awt.Window.isAlwaysOnTopSupported()
    virtual bool isFocusCycleRoot(); // public final boolean java.awt.Window.isFocusCycleRoot()
    virtual bool isFocusableWindow(); // public final boolean java.awt.Window.isFocusableWindow()
    virtual bool isFocused(); // public boolean java.awt.Window.isFocused()
    virtual bool isLocationByPlatform(); // public boolean java.awt.Window.isLocationByPlatform()
    virtual bool isShowing(); // public boolean java.awt.Window.isShowing()
    virtual void pack(); // public void java.awt.Window.pack()
    virtual bool postEvent(java::awt::Event* arg1); // public boolean java.awt.Window.postEvent(java.awt.Event)
    virtual void removeNotify(); // public void java.awt.Window.removeNotify()
    virtual void removeWindowFocusListener(java::awt::event::WindowFocusListener* arg1); // public synchronized void java.awt.Window.removeWindowFocusListener(java.awt.event.WindowFocusListener)
    virtual void removeWindowListener(java::awt::event::WindowListener* arg1); // public synchronized void java.awt.Window.removeWindowListener(java.awt.event.WindowListener)
    virtual void removeWindowStateListener(java::awt::event::WindowStateListener* arg1); // public synchronized void java.awt.Window.removeWindowStateListener(java.awt.event.WindowStateListener)
    virtual void setAlwaysOnTop(bool arg1); // public final void java.awt.Window.setAlwaysOnTop(boolean) throws java.lang.SecurityException
    virtual void setCursor(java::awt::Cursor* arg1); // public void java.awt.Window.setCursor(java.awt.Cursor)
    virtual void setFocusCycleRoot(bool arg1); // public final void java.awt.Window.setFocusCycleRoot(boolean)
    virtual void setFocusableWindowState(bool arg1); // public void java.awt.Window.setFocusableWindowState(boolean)
    virtual void setIconImage(java::awt::Image* arg1); // public void java.awt.Window.setIconImage(java.awt.Image)
    virtual void setIconImages(java::util::List* arg1); // public synchronized void java.awt.Window.setIconImages(java.util.List)
    virtual void setLocationByPlatform(bool arg1); // public void java.awt.Window.setLocationByPlatform(boolean)
    virtual void setLocationRelativeTo(java::awt::Component* arg1); // public void java.awt.Window.setLocationRelativeTo(java.awt.Component)
    virtual void setMinimumSize(java::awt::Dimension* arg1); // public void java.awt.Window.setMinimumSize(java.awt.Dimension)
    virtual void setModalExclusionType(java::awt::Dialog__ModalExclusionType* arg1); // public void java.awt.Window.setModalExclusionType(java.awt.Dialog$ModalExclusionType)
    virtual void show(); // public void java.awt.Window.show()
    virtual void toBack(); // public void java.awt.Window.toBack()

};
}
}
#endif
