#ifndef java_awt_event_ActionEvent_H
#define java_awt_event_ActionEvent_H
#include <jni.h>
#include <java_marker.h>
#include <java_awt_AWTEvent.h>

namespace java {
namespace lang {
class Object;
}
}

namespace java {
namespace lang {
class String;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace event {
class ActionEvent : public java::awt::AWTEvent {
  public:
    ActionEvent(JavaMarker* dummy);
    ActionEvent(jobject obj);
    ActionEvent(java::lang::Object* arg1, int arg2, const char* arg3); // public java.awt.event.ActionEvent(java.lang.Object,int,java.lang.String)
    ActionEvent(java::lang::Object* arg1, int arg2, const char* arg3, int arg4); // public java.awt.event.ActionEvent(java.lang.Object,int,java.lang.String,int)
    ActionEvent(java::lang::Object* arg1, int arg2, const char* arg3, long arg4, int arg5); // public java.awt.event.ActionEvent(java.lang.Object,int,java.lang.String,long,int)

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual int getModifiers(); // public int java.awt.event.ActionEvent.getModifiers()
    virtual const char* paramString(); // public java.lang.String java.awt.event.ActionEvent.paramString()
    virtual long getWhen(); // public long java.awt.event.ActionEvent.getWhen()
    virtual const char* getActionCommand(); // public java.lang.String java.awt.event.ActionEvent.getActionCommand()

    int SHIFT_MASK; // public static final int java.awt.event.ActionEvent.SHIFT_MASK
    int CTRL_MASK; // public static final int java.awt.event.ActionEvent.CTRL_MASK
    int META_MASK; // public static final int java.awt.event.ActionEvent.META_MASK
    int ALT_MASK; // public static final int java.awt.event.ActionEvent.ALT_MASK
    int ACTION_FIRST; // public static final int java.awt.event.ActionEvent.ACTION_FIRST
    int ACTION_LAST; // public static final int java.awt.event.ActionEvent.ACTION_LAST
    int ACTION_PERFORMED; // public static final int java.awt.event.ActionEvent.ACTION_PERFORMED
};
}
}
}
#endif
