#ifndef java_awt_datatransfer_FlavorMap_H
#define java_awt_datatransfer_FlavorMap_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace util {
class Map;
}
}

namespace java {
namespace lang {
class String;
}
}

namespace java {
namespace awt {
namespace datatransfer {
class DataFlavor;
}
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace datatransfer {
class FlavorMap : public java::lang::Object {
  public:
    FlavorMap(JavaMarker* dummy);
    FlavorMap(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual java::util::Map* getFlavorsForNatives(JavaObjectArray* arg1); // public abstract java.util.Map java.awt.datatransfer.FlavorMap.getFlavorsForNatives(java.lang.String[])
    virtual java::util::Map* getNativesForFlavors(JavaObjectArray* arg1); // public abstract java.util.Map java.awt.datatransfer.FlavorMap.getNativesForFlavors(java.awt.datatransfer.DataFlavor[])

};
}
}
}
#endif
