#ifndef java_awt_dnd_DropTargetContext_H
#define java_awt_dnd_DropTargetContext_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace io {
class Serializable;
}
}

namespace java {
namespace awt {
namespace dnd {
namespace peer {
class DropTargetContextPeer;
}
}
}
}

namespace java {
namespace awt {
class Component;
}
}

namespace java {
namespace awt {
namespace dnd {
class DropTarget;
}
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace dnd {
class DropTargetContext : public java::lang::Object {
  public:
    DropTargetContext(JavaMarker* dummy);
    DropTargetContext(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual void addNotify(java::awt::dnd::peer::DropTargetContextPeer* arg1); // public void java.awt.dnd.DropTargetContext.addNotify(java.awt.dnd.peer.DropTargetContextPeer)
    virtual void removeNotify(); // public void java.awt.dnd.DropTargetContext.removeNotify()
    virtual java::awt::Component* getComponent(); // public java.awt.Component java.awt.dnd.DropTargetContext.getComponent()
    virtual java::awt::dnd::DropTarget* getDropTarget(); // public java.awt.dnd.DropTarget java.awt.dnd.DropTargetContext.getDropTarget()
    virtual void dropComplete(bool arg1); // public void java.awt.dnd.DropTargetContext.dropComplete(boolean) throws java.awt.dnd.InvalidDnDOperationException

};
}
}
}
#endif
