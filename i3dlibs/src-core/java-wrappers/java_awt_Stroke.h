#ifndef java_awt_Stroke_H
#define java_awt_Stroke_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace awt {
class Shape;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
class Stroke : public java::lang::Object {
  public:
    Stroke(JavaMarker* dummy);
    Stroke(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual java::awt::Shape* createStrokedShape(java::awt::Shape* arg1); // public abstract java.awt.Shape java.awt.Stroke.createStrokedShape(java.awt.Shape)

};
}
}
#endif
