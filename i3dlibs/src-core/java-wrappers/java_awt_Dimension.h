#ifndef java_awt_Dimension_H
#define java_awt_Dimension_H
#include <jni.h>
#include <java_marker.h>
#include <java_awt_geom_Dimension2D.h>

namespace java {
namespace io {
class Serializable;
}
}

namespace java {
namespace lang {
class Object;
}
}

namespace java {
namespace lang {
class String;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
class Dimension : public java::awt::geom::Dimension2D {
  public:
    Dimension(JavaMarker* dummy);
    Dimension(jobject obj);
    Dimension(java::awt::Dimension* arg1); // public java.awt.Dimension(java.awt.Dimension)
    Dimension(int arg1, int arg2); // public java.awt.Dimension(int,int)
    Dimension(); // public java.awt.Dimension()

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual int hashCode(); // public int java.awt.Dimension.hashCode()
    virtual bool equals(java::lang::Object* arg1); // public boolean java.awt.Dimension.equals(java.lang.Object)
    virtual const char* toString(); // public java.lang.String java.awt.Dimension.toString()
    virtual java::awt::Dimension* getSize(); // public java.awt.Dimension java.awt.Dimension.getSize()
    virtual void setSize(int arg1, int arg2); // public void java.awt.Dimension.setSize(int,int)
    virtual void setSize(double arg1, double arg2); // public void java.awt.Dimension.setSize(double,double)
    virtual void setSize(java::awt::Dimension* arg1); // public void java.awt.Dimension.setSize(java.awt.Dimension)
    virtual double getHeight(); // public double java.awt.Dimension.getHeight()
    virtual double getWidth(); // public double java.awt.Dimension.getWidth()

    int width; // public int java.awt.Dimension.width
    int height; // public int java.awt.Dimension.height
};
}
}
#endif
