#ifndef java_awt_event_ActionListener_H
#define java_awt_event_ActionListener_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace util {
class EventListener;
}
}

namespace java {
namespace awt {
namespace event {
class ActionEvent;
}
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace event {
class ActionListener : public java::lang::Object {
  public:
    ActionListener(JavaMarker* dummy);
    ActionListener(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual void actionPerformed(java::awt::event::ActionEvent* arg1); // public abstract void java.awt.event.ActionListener.actionPerformed(java.awt.event.ActionEvent)

};
}
}
}
#endif
