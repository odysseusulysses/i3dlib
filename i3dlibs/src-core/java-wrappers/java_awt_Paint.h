#ifndef java_awt_Paint_H
#define java_awt_Paint_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace awt {
class Transparency;
}
}

namespace java {
namespace awt {
class PaintContext;
}
}

namespace java {
namespace awt {
namespace image {
class ColorModel;
}
}
}

namespace java {
namespace awt {
class Rectangle;
}
}

namespace java {
namespace awt {
namespace geom {
class Rectangle2D;
}
}
}

namespace java {
namespace awt {
namespace geom {
class AffineTransform;
}
}
}

namespace java {
namespace awt {
class RenderingHints;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
class Paint : public java::lang::Object {
  public:
    Paint(JavaMarker* dummy);
    Paint(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual java::awt::PaintContext* createContext(java::awt::image::ColorModel* arg1, java::awt::Rectangle* arg2, java::awt::geom::Rectangle2D* arg3, java::awt::geom::AffineTransform* arg4, java::awt::RenderingHints* arg5); // public abstract java.awt.PaintContext java.awt.Paint.createContext(java.awt.image.ColorModel,java.awt.Rectangle,java.awt.geom.Rectangle2D,java.awt.geom.AffineTransform,java.awt.RenderingHints)

};
}
}
#endif
