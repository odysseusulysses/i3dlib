#ifndef loci_formats_StatusEvent_H
#define loci_formats_StatusEvent_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace lang {
class String;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace loci {
namespace formats {
class StatusEvent : public java::lang::Object {
  public:
    StatusEvent(JavaMarker* dummy);
    StatusEvent(jobject obj);
    StatusEvent(const char* arg1); // public loci.formats.StatusEvent(java.lang.String)
    StatusEvent(int arg1, int arg2, const char* arg3); // public loci.formats.StatusEvent(int,int,java.lang.String)

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual int getProgressValue(); // public int loci.formats.StatusEvent.getProgressValue()
    virtual int getProgressMaximum(); // public int loci.formats.StatusEvent.getProgressMaximum()
    virtual const char* getStatusMessage(); // public java.lang.String loci.formats.StatusEvent.getStatusMessage()

};
}
}
#endif
