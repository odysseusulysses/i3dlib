#ifndef java_awt_RenderingHints__Key_H
#define java_awt_RenderingHints__Key_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
class RenderingHints__Key : public java::lang::Object {
  public:
    RenderingHints__Key(JavaMarker* dummy);
    RenderingHints__Key(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual int hashCode(); // public final int java.awt.RenderingHints$Key.hashCode()
    virtual bool equals(java::lang::Object* arg1); // public final boolean java.awt.RenderingHints$Key.equals(java.lang.Object)
    virtual bool isCompatibleValue(java::lang::Object* arg1); // public abstract boolean java.awt.RenderingHints$Key.isCompatibleValue(java.lang.Object)

};
}
}
#endif
