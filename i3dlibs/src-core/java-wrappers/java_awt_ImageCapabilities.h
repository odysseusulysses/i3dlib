#ifndef java_awt_ImageCapabilities_H
#define java_awt_ImageCapabilities_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace lang {
class Cloneable;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
class ImageCapabilities : public java::lang::Object {
  public:
    ImageCapabilities(JavaMarker* dummy);
    ImageCapabilities(jobject obj);
    ImageCapabilities(bool arg1); // public java.awt.ImageCapabilities(boolean)

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual java::lang::Object* clone(); // public java.lang.Object java.awt.ImageCapabilities.clone()
    virtual bool isAccelerated(); // public boolean java.awt.ImageCapabilities.isAccelerated()
    virtual bool isTrueVolatile(); // public boolean java.awt.ImageCapabilities.isTrueVolatile()

};
}
}
#endif
