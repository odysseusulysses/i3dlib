#ifndef java_awt_font_LineMetrics_H
#define java_awt_font_LineMetrics_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace font {
class LineMetrics : public java::lang::Object {
  public:
    LineMetrics(JavaMarker* dummy);
    LineMetrics(jobject obj);
    LineMetrics(); // public java.awt.font.LineMetrics()

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual float getHeight(); // public abstract float java.awt.font.LineMetrics.getHeight()
    virtual float getAscent(); // public abstract float java.awt.font.LineMetrics.getAscent()
    virtual float getDescent(); // public abstract float java.awt.font.LineMetrics.getDescent()
    virtual float getLeading(); // public abstract float java.awt.font.LineMetrics.getLeading()
    virtual int getBaselineIndex(); // public abstract int java.awt.font.LineMetrics.getBaselineIndex()
    virtual JavaFloatArray* getBaselineOffsets(); // public abstract float[] java.awt.font.LineMetrics.getBaselineOffsets()
    virtual int getNumChars(); // public abstract int java.awt.font.LineMetrics.getNumChars()
    virtual float getStrikethroughOffset(); // public abstract float java.awt.font.LineMetrics.getStrikethroughOffset()
    virtual float getStrikethroughThickness(); // public abstract float java.awt.font.LineMetrics.getStrikethroughThickness()
    virtual float getUnderlineOffset(); // public abstract float java.awt.font.LineMetrics.getUnderlineOffset()
    virtual float getUnderlineThickness(); // public abstract float java.awt.font.LineMetrics.getUnderlineThickness()

};
}
}
}
#endif
