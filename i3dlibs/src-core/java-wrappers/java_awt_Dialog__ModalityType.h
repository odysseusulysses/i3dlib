#ifndef java_awt_Dialog__ModalityType_H
#define java_awt_Dialog__ModalityType_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Enum.h>

namespace java {
namespace lang {
class String;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
class Dialog__ModalityType : public java::lang::Enum {
  public:
    Dialog__ModalityType(JavaMarker* dummy);
    Dialog__ModalityType(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual java::awt::Dialog__ModalityType* valueOf(const char* arg1); // public static java.awt.Dialog$ModalityType java.awt.Dialog$ModalityType.valueOf(java.lang.String)
    virtual JavaObjectArray* values(); // public static java.awt.Dialog$ModalityType[] java.awt.Dialog$ModalityType.values()

    java::awt::Dialog__ModalityType* MODELESS; // public static final java.awt.Dialog$ModalityType java.awt.Dialog$ModalityType.MODELESS
    java::awt::Dialog__ModalityType* DOCUMENT_MODAL; // public static final java.awt.Dialog$ModalityType java.awt.Dialog$ModalityType.DOCUMENT_MODAL
    java::awt::Dialog__ModalityType* APPLICATION_MODAL; // public static final java.awt.Dialog$ModalityType java.awt.Dialog$ModalityType.APPLICATION_MODAL
    java::awt::Dialog__ModalityType* TOOLKIT_MODAL; // public static final java.awt.Dialog$ModalityType java.awt.Dialog$ModalityType.TOOLKIT_MODAL
};
}
}
#endif
