#ifndef java_awt_image_Raster_H
#define java_awt_image_Raster_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace awt {
namespace image {
class WritableRaster;
}
}
}

namespace java {
namespace awt {
class Rectangle;
}
}

namespace java {
namespace awt {
class Point;
}
}

namespace java {
namespace awt {
namespace image {
class DataBuffer;
}
}
}

namespace java {
namespace awt {
namespace image {
class SampleModel;
}
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace image {
class Raster : public java::lang::Object {
  public:
    Raster(JavaMarker* dummy);
    Raster(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual java::awt::image::Raster* getParent(); // public java.awt.image.Raster java.awt.image.Raster.getParent()
    virtual JavaIntArray* getSamples(int arg1, int arg2, int arg3, int arg4, int arg5, JavaIntArray* arg6); // public int[] java.awt.image.Raster.getSamples(int,int,int,int,int,int[])
    virtual JavaDoubleArray* getSamples(int arg1, int arg2, int arg3, int arg4, int arg5, JavaDoubleArray* arg6); // public double[] java.awt.image.Raster.getSamples(int,int,int,int,int,double[])
    virtual JavaFloatArray* getSamples(int arg1, int arg2, int arg3, int arg4, int arg5, JavaFloatArray* arg6); // public float[] java.awt.image.Raster.getSamples(int,int,int,int,int,float[])
    virtual java::awt::image::WritableRaster* createCompatibleWritableRaster(java::awt::Rectangle* arg1); // public java.awt.image.WritableRaster java.awt.image.Raster.createCompatibleWritableRaster(java.awt.Rectangle)
    virtual java::awt::image::WritableRaster* createCompatibleWritableRaster(int arg1, int arg2); // public java.awt.image.WritableRaster java.awt.image.Raster.createCompatibleWritableRaster(int,int)
    virtual java::awt::image::WritableRaster* createCompatibleWritableRaster(int arg1, int arg2, int arg3, int arg4); // public java.awt.image.WritableRaster java.awt.image.Raster.createCompatibleWritableRaster(int,int,int,int)
    virtual java::awt::image::WritableRaster* createCompatibleWritableRaster(); // public java.awt.image.WritableRaster java.awt.image.Raster.createCompatibleWritableRaster()
    virtual java::awt::image::WritableRaster* createInterleavedRaster(int arg1, int arg2, int arg3, int arg4, int arg5, JavaIntArray* arg6, java::awt::Point* arg7); // public static java.awt.image.WritableRaster java.awt.image.Raster.createInterleavedRaster(int,int,int,int,int,int[],java.awt.Point)
    virtual java::awt::image::WritableRaster* createInterleavedRaster(java::awt::image::DataBuffer* arg1, int arg2, int arg3, int arg4, int arg5, JavaIntArray* arg6, java::awt::Point* arg7); // public static java.awt.image.WritableRaster java.awt.image.Raster.createInterleavedRaster(java.awt.image.DataBuffer,int,int,int,int,int[],java.awt.Point)
    virtual java::awt::image::WritableRaster* createInterleavedRaster(int arg1, int arg2, int arg3, int arg4, java::awt::Point* arg5); // public static java.awt.image.WritableRaster java.awt.image.Raster.createInterleavedRaster(int,int,int,int,java.awt.Point)
    virtual java::awt::image::WritableRaster* createPackedRaster(int arg1, int arg2, int arg3, int arg4, int arg5, java::awt::Point* arg6); // public static java.awt.image.WritableRaster java.awt.image.Raster.createPackedRaster(int,int,int,int,int,java.awt.Point)
    virtual java::awt::image::WritableRaster* createPackedRaster(java::awt::image::DataBuffer* arg1, int arg2, int arg3, int arg4, JavaIntArray* arg5, java::awt::Point* arg6); // public static java.awt.image.WritableRaster java.awt.image.Raster.createPackedRaster(java.awt.image.DataBuffer,int,int,int,int[],java.awt.Point)
    virtual java::awt::image::WritableRaster* createPackedRaster(java::awt::image::DataBuffer* arg1, int arg2, int arg3, int arg4, java::awt::Point* arg5); // public static java.awt.image.WritableRaster java.awt.image.Raster.createPackedRaster(java.awt.image.DataBuffer,int,int,int,java.awt.Point)
    virtual java::awt::image::WritableRaster* createPackedRaster(int arg1, int arg2, int arg3, JavaIntArray* arg4, java::awt::Point* arg5); // public static java.awt.image.WritableRaster java.awt.image.Raster.createPackedRaster(int,int,int,int[],java.awt.Point)
    virtual java::awt::image::WritableRaster* createWritableRaster(java::awt::image::SampleModel* arg1, java::awt::Point* arg2); // public static java.awt.image.WritableRaster java.awt.image.Raster.createWritableRaster(java.awt.image.SampleModel,java.awt.Point)
    virtual java::awt::image::WritableRaster* createWritableRaster(java::awt::image::SampleModel* arg1, java::awt::image::DataBuffer* arg2, java::awt::Point* arg3); // public static java.awt.image.WritableRaster java.awt.image.Raster.createWritableRaster(java.awt.image.SampleModel,java.awt.image.DataBuffer,java.awt.Point)
    virtual java::awt::image::DataBuffer* getDataBuffer(); // public java.awt.image.DataBuffer java.awt.image.Raster.getDataBuffer()
    virtual java::lang::Object* getDataElements(int arg1, int arg2, int arg3, int arg4, java::lang::Object* arg5); // public java.lang.Object java.awt.image.Raster.getDataElements(int,int,int,int,java.lang.Object)
    virtual java::lang::Object* getDataElements(int arg1, int arg2, java::lang::Object* arg3); // public java.lang.Object java.awt.image.Raster.getDataElements(int,int,java.lang.Object)
    virtual int getHeight(); // public final int java.awt.image.Raster.getHeight()
    virtual int getMinX(); // public final int java.awt.image.Raster.getMinX()
    virtual int getMinY(); // public final int java.awt.image.Raster.getMinY()
    virtual int getNumBands(); // public final int java.awt.image.Raster.getNumBands()
    virtual JavaIntArray* getPixels(int arg1, int arg2, int arg3, int arg4, JavaIntArray* arg5); // public int[] java.awt.image.Raster.getPixels(int,int,int,int,int[])
    virtual JavaFloatArray* getPixels(int arg1, int arg2, int arg3, int arg4, JavaFloatArray* arg5); // public float[] java.awt.image.Raster.getPixels(int,int,int,int,float[])
    virtual JavaDoubleArray* getPixels(int arg1, int arg2, int arg3, int arg4, JavaDoubleArray* arg5); // public double[] java.awt.image.Raster.getPixels(int,int,int,int,double[])
    virtual java::awt::image::SampleModel* getSampleModel(); // public java.awt.image.SampleModel java.awt.image.Raster.getSampleModel()
    virtual int getSampleModelTranslateX(); // public final int java.awt.image.Raster.getSampleModelTranslateX()
    virtual int getSampleModelTranslateY(); // public final int java.awt.image.Raster.getSampleModelTranslateY()
    virtual int getWidth(); // public final int java.awt.image.Raster.getWidth()
    virtual int getTransferType(); // public final int java.awt.image.Raster.getTransferType()
    virtual java::awt::image::WritableRaster* createBandedRaster(int arg1, int arg2, int arg3, int arg4, java::awt::Point* arg5); // public static java.awt.image.WritableRaster java.awt.image.Raster.createBandedRaster(int,int,int,int,java.awt.Point)
    virtual java::awt::image::WritableRaster* createBandedRaster(int arg1, int arg2, int arg3, int arg4, JavaIntArray* arg5, JavaIntArray* arg6, java::awt::Point* arg7); // public static java.awt.image.WritableRaster java.awt.image.Raster.createBandedRaster(int,int,int,int,int[],int[],java.awt.Point)
    virtual java::awt::image::WritableRaster* createBandedRaster(java::awt::image::DataBuffer* arg1, int arg2, int arg3, int arg4, JavaIntArray* arg5, JavaIntArray* arg6, java::awt::Point* arg7); // public static java.awt.image.WritableRaster java.awt.image.Raster.createBandedRaster(java.awt.image.DataBuffer,int,int,int,int[],int[],java.awt.Point)
    virtual java::awt::image::Raster* createChild(int arg1, int arg2, int arg3, int arg4, int arg5, int arg6, JavaIntArray* arg7); // public java.awt.image.Raster java.awt.image.Raster.createChild(int,int,int,int,int,int,int[])
    virtual java::awt::image::Raster* createRaster(java::awt::image::SampleModel* arg1, java::awt::image::DataBuffer* arg2, java::awt::Point* arg3); // public static java.awt.image.Raster java.awt.image.Raster.createRaster(java.awt.image.SampleModel,java.awt.image.DataBuffer,java.awt.Point)
    virtual java::awt::image::Raster* createTranslatedChild(int arg1, int arg2); // public java.awt.image.Raster java.awt.image.Raster.createTranslatedChild(int,int)
    virtual java::awt::Rectangle* getBounds(); // public java.awt.Rectangle java.awt.image.Raster.getBounds()
    virtual int getNumDataElements(); // public final int java.awt.image.Raster.getNumDataElements()
    virtual JavaFloatArray* getPixel(int arg1, int arg2, JavaFloatArray* arg3); // public float[] java.awt.image.Raster.getPixel(int,int,float[])
    virtual JavaDoubleArray* getPixel(int arg1, int arg2, JavaDoubleArray* arg3); // public double[] java.awt.image.Raster.getPixel(int,int,double[])
    virtual JavaIntArray* getPixel(int arg1, int arg2, JavaIntArray* arg3); // public int[] java.awt.image.Raster.getPixel(int,int,int[])
    virtual int getSample(int arg1, int arg2, int arg3); // public int java.awt.image.Raster.getSample(int,int,int)
    virtual double getSampleDouble(int arg1, int arg2, int arg3); // public double java.awt.image.Raster.getSampleDouble(int,int,int)
    virtual float getSampleFloat(int arg1, int arg2, int arg3); // public float java.awt.image.Raster.getSampleFloat(int,int,int)

};
}
}
}
#endif
