#ifndef loci_formats_CoreMetadata_H
#define loci_formats_CoreMetadata_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace lang {
class String;
}
}

namespace java {
namespace util {
class Hashtable;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace loci {
namespace formats {
class CoreMetadata : public java::lang::Object {
  public:
    CoreMetadata(JavaMarker* dummy);
    CoreMetadata(jobject obj);
    CoreMetadata(int arg1); // public loci.formats.CoreMetadata(int)

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);

    JavaIntArray* sizeX; // public int[] loci.formats.CoreMetadata.sizeX
    JavaIntArray* sizeY; // public int[] loci.formats.CoreMetadata.sizeY
    JavaIntArray* sizeZ; // public int[] loci.formats.CoreMetadata.sizeZ
    JavaIntArray* sizeC; // public int[] loci.formats.CoreMetadata.sizeC
    JavaIntArray* sizeT; // public int[] loci.formats.CoreMetadata.sizeT
    JavaIntArray* thumbSizeX; // public int[] loci.formats.CoreMetadata.thumbSizeX
    JavaIntArray* thumbSizeY; // public int[] loci.formats.CoreMetadata.thumbSizeY
    JavaIntArray* pixelType; // public int[] loci.formats.CoreMetadata.pixelType
    JavaIntArray* imageCount; // public int[] loci.formats.CoreMetadata.imageCount
    JavaObjectArray* cLengths; // public int[][] loci.formats.CoreMetadata.cLengths
    JavaObjectArray* cTypes; // public java.lang.String[][] loci.formats.CoreMetadata.cTypes
    JavaObjectArray* currentOrder; // public java.lang.String[] loci.formats.CoreMetadata.currentOrder
    JavaBooleanArray* orderCertain; // public boolean[] loci.formats.CoreMetadata.orderCertain
    JavaBooleanArray* rgb; // public boolean[] loci.formats.CoreMetadata.rgb
    JavaBooleanArray* littleEndian; // public boolean[] loci.formats.CoreMetadata.littleEndian
    JavaBooleanArray* interleaved; // public boolean[] loci.formats.CoreMetadata.interleaved
    JavaBooleanArray* indexed; // public boolean[] loci.formats.CoreMetadata.indexed
    JavaBooleanArray* falseColor; // public boolean[] loci.formats.CoreMetadata.falseColor
    JavaBooleanArray* metadataComplete; // public boolean[] loci.formats.CoreMetadata.metadataComplete
    JavaObjectArray* seriesMetadata; // public java.util.Hashtable[] loci.formats.CoreMetadata.seriesMetadata
};
}
}
#endif
