#ifndef java_awt_image_VolatileImage_H
#define java_awt_image_VolatileImage_H
#include <jni.h>
#include <java_marker.h>
#include <java_awt_Image.h>

namespace java {
namespace awt {
class Transparency;
}
}

namespace java {
namespace awt {
class Graphics2D;
}
}

namespace java {
namespace awt {
class Graphics;
}
}

namespace java {
namespace awt {
namespace image {
class ImageProducer;
}
}
}

namespace java {
namespace awt {
class ImageCapabilities;
}
}

namespace java {
namespace awt {
namespace image {
class BufferedImage;
}
}
}

namespace java {
namespace awt {
class GraphicsConfiguration;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace image {
class VolatileImage : public java::awt::Image {
  public:
    VolatileImage(JavaMarker* dummy);
    VolatileImage(jobject obj);
    VolatileImage(); // public java.awt.image.VolatileImage()

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual java::awt::Graphics2D* createGraphics(); // public abstract java.awt.Graphics2D java.awt.image.VolatileImage.createGraphics()
    virtual java::awt::Graphics* getGraphics(); // public java.awt.Graphics java.awt.image.VolatileImage.getGraphics()
    virtual int getHeight(); // public abstract int java.awt.image.VolatileImage.getHeight()
    virtual java::awt::image::ImageProducer* getSource(); // public java.awt.image.ImageProducer java.awt.image.VolatileImage.getSource()
    virtual int getTransparency(); // public int java.awt.image.VolatileImage.getTransparency()
    virtual int getWidth(); // public abstract int java.awt.image.VolatileImage.getWidth()
    virtual java::awt::ImageCapabilities* getCapabilities(); // public abstract java.awt.ImageCapabilities java.awt.image.VolatileImage.getCapabilities()
    virtual bool contentsLost(); // public abstract boolean java.awt.image.VolatileImage.contentsLost()
    virtual java::awt::image::BufferedImage* getSnapshot(); // public abstract java.awt.image.BufferedImage java.awt.image.VolatileImage.getSnapshot()
    virtual int validate(java::awt::GraphicsConfiguration* arg1); // public abstract int java.awt.image.VolatileImage.validate(java.awt.GraphicsConfiguration)

    int IMAGE_OK; // public static final int java.awt.image.VolatileImage.IMAGE_OK
    int IMAGE_RESTORED; // public static final int java.awt.image.VolatileImage.IMAGE_RESTORED
    int IMAGE_INCOMPATIBLE; // public static final int java.awt.image.VolatileImage.IMAGE_INCOMPATIBLE
};
}
}
}
#endif
