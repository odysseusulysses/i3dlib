#ifndef java_awt_dnd_DragGestureEvent_H
#define java_awt_dnd_DragGestureEvent_H
#include <jni.h>
#include <java_marker.h>
#include <java_util_EventObject.h>

namespace java {
namespace awt {
namespace dnd {
class DragGestureRecognizer;
}
}
}

namespace java {
namespace awt {
class Point;
}
}

namespace java {
namespace util {
class List;
}
}

namespace java {
namespace util {
class Iterator;
}
}

namespace java {
namespace lang {
class Object;
}
}

namespace java {
namespace awt {
class Component;
}
}

namespace java {
namespace awt {
class Cursor;
}
}

namespace java {
namespace awt {
class Image;
}
}

namespace java {
namespace awt {
namespace datatransfer {
class Transferable;
}
}
}

namespace java {
namespace awt {
namespace dnd {
class DragSourceListener;
}
}
}

namespace java {
namespace awt {
namespace dnd {
class DragSource;
}
}
}

namespace java {
namespace awt {
namespace event {
class InputEvent;
}
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace dnd {
class DragGestureEvent : public java::util::EventObject {
  public:
    DragGestureEvent(JavaMarker* dummy);
    DragGestureEvent(jobject obj);
    DragGestureEvent(java::awt::dnd::DragGestureRecognizer* arg1, int arg2, java::awt::Point* arg3, java::util::List* arg4); // public java.awt.dnd.DragGestureEvent(java.awt.dnd.DragGestureRecognizer,int,java.awt.Point,java.util.List)

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual java::util::Iterator* iterator(); // public java.util.Iterator java.awt.dnd.DragGestureEvent.iterator()
    virtual JavaObjectArray* toArray(); // public java.lang.Object[] java.awt.dnd.DragGestureEvent.toArray()
    virtual JavaObjectArray* toArray(JavaObjectArray* arg1); // public java.lang.Object[] java.awt.dnd.DragGestureEvent.toArray(java.lang.Object[])
    virtual java::awt::Component* getComponent(); // public java.awt.Component java.awt.dnd.DragGestureEvent.getComponent()
    virtual void startDrag(java::awt::Cursor* arg1, java::awt::Image* arg2, java::awt::Point* arg3, java::awt::datatransfer::Transferable* arg4, java::awt::dnd::DragSourceListener* arg5); // public void java.awt.dnd.DragGestureEvent.startDrag(java.awt.Cursor,java.awt.Image,java.awt.Point,java.awt.datatransfer.Transferable,java.awt.dnd.DragSourceListener) throws java.awt.dnd.InvalidDnDOperationException
    virtual void startDrag(java::awt::Cursor* arg1, java::awt::datatransfer::Transferable* arg2, java::awt::dnd::DragSourceListener* arg3); // public void java.awt.dnd.DragGestureEvent.startDrag(java.awt.Cursor,java.awt.datatransfer.Transferable,java.awt.dnd.DragSourceListener) throws java.awt.dnd.InvalidDnDOperationException
    virtual void startDrag(java::awt::Cursor* arg1, java::awt::datatransfer::Transferable* arg2); // public void java.awt.dnd.DragGestureEvent.startDrag(java.awt.Cursor,java.awt.datatransfer.Transferable) throws java.awt.dnd.InvalidDnDOperationException
    virtual java::awt::dnd::DragSource* getDragSource(); // public java.awt.dnd.DragSource java.awt.dnd.DragGestureEvent.getDragSource()
    virtual java::awt::event::InputEvent* getTriggerEvent(); // public java.awt.event.InputEvent java.awt.dnd.DragGestureEvent.getTriggerEvent()
    virtual int getDragAction(); // public int java.awt.dnd.DragGestureEvent.getDragAction()
    virtual java::awt::Point* getDragOrigin(); // public java.awt.Point java.awt.dnd.DragGestureEvent.getDragOrigin()
    virtual java::awt::dnd::DragGestureRecognizer* getSourceAsDragGestureRecognizer(); // public java.awt.dnd.DragGestureRecognizer java.awt.dnd.DragGestureEvent.getSourceAsDragGestureRecognizer()

};
}
}
}
#endif
