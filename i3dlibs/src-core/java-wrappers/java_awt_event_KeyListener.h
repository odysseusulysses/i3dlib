#ifndef java_awt_event_KeyListener_H
#define java_awt_event_KeyListener_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace util {
class EventListener;
}
}

namespace java {
namespace awt {
namespace event {
class KeyEvent;
}
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace event {
class KeyListener : public java::lang::Object {
  public:
    KeyListener(JavaMarker* dummy);
    KeyListener(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual void keyPressed(java::awt::event::KeyEvent* arg1); // public abstract void java.awt.event.KeyListener.keyPressed(java.awt.event.KeyEvent)
    virtual void keyReleased(java::awt::event::KeyEvent* arg1); // public abstract void java.awt.event.KeyListener.keyReleased(java.awt.event.KeyEvent)
    virtual void keyTyped(java::awt::event::KeyEvent* arg1); // public abstract void java.awt.event.KeyListener.keyTyped(java.awt.event.KeyEvent)

};
}
}
}
#endif
