#ifndef java_awt_JobAttributes__DialogType_H
#define java_awt_JobAttributes__DialogType_H
#include <jni.h>
#include <java_marker.h>
#include <java_awt_AttributeValue.h>

namespace java {
namespace lang {
class String;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
class JobAttributes__DialogType : public java::awt::AttributeValue {
  public:
    JobAttributes__DialogType(JavaMarker* dummy);
    JobAttributes__DialogType(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual int hashCode(); // public int java.awt.JobAttributes$DialogType.hashCode()
    virtual const char* toString(); // public java.lang.String java.awt.JobAttributes$DialogType.toString()

    java::awt::JobAttributes__DialogType* COMMON; // public static final java.awt.JobAttributes$DialogType java.awt.JobAttributes$DialogType.COMMON
    java::awt::JobAttributes__DialogType* NATIVE; // public static final java.awt.JobAttributes$DialogType java.awt.JobAttributes$DialogType.NATIVE
    java::awt::JobAttributes__DialogType* NONE; // public static final java.awt.JobAttributes$DialogType java.awt.JobAttributes$DialogType.NONE
};
}
}
#endif
