#ifndef java_awt_datatransfer_Clipboard_H
#define java_awt_datatransfer_Clipboard_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace lang {
class String;
}
}

namespace java {
namespace awt {
namespace datatransfer {
class DataFlavor;
}
}
}

namespace java {
namespace awt {
namespace datatransfer {
class Transferable;
}
}
}

namespace java {
namespace awt {
namespace datatransfer {
class FlavorListener;
}
}
}

namespace java {
namespace awt {
namespace datatransfer {
class ClipboardOwner;
}
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace datatransfer {
class Clipboard : public java::lang::Object {
  public:
    Clipboard(JavaMarker* dummy);
    Clipboard(jobject obj);
    Clipboard(const char* arg1); // public java.awt.datatransfer.Clipboard(java.lang.String)

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual const char* getName(); // public java.lang.String java.awt.datatransfer.Clipboard.getName()
    virtual java::lang::Object* getData(java::awt::datatransfer::DataFlavor* arg1); // public java.lang.Object java.awt.datatransfer.Clipboard.getData(java.awt.datatransfer.DataFlavor) throws java.awt.datatransfer.UnsupportedFlavorException,java.io.IOException
    virtual java::awt::datatransfer::Transferable* getContents(java::lang::Object* arg1); // public synchronized java.awt.datatransfer.Transferable java.awt.datatransfer.Clipboard.getContents(java.lang.Object)
    virtual void addFlavorListener(java::awt::datatransfer::FlavorListener* arg1); // public synchronized void java.awt.datatransfer.Clipboard.addFlavorListener(java.awt.datatransfer.FlavorListener)
    virtual JavaObjectArray* getAvailableDataFlavors(); // public java.awt.datatransfer.DataFlavor[] java.awt.datatransfer.Clipboard.getAvailableDataFlavors()
    virtual JavaObjectArray* getFlavorListeners(); // public synchronized java.awt.datatransfer.FlavorListener[] java.awt.datatransfer.Clipboard.getFlavorListeners()
    virtual bool isDataFlavorAvailable(java::awt::datatransfer::DataFlavor* arg1); // public boolean java.awt.datatransfer.Clipboard.isDataFlavorAvailable(java.awt.datatransfer.DataFlavor)
    virtual void removeFlavorListener(java::awt::datatransfer::FlavorListener* arg1); // public synchronized void java.awt.datatransfer.Clipboard.removeFlavorListener(java.awt.datatransfer.FlavorListener)
    virtual void setContents(java::awt::datatransfer::Transferable* arg1, java::awt::datatransfer::ClipboardOwner* arg2); // public synchronized void java.awt.datatransfer.Clipboard.setContents(java.awt.datatransfer.Transferable,java.awt.datatransfer.ClipboardOwner)

};
}
}
}
#endif
