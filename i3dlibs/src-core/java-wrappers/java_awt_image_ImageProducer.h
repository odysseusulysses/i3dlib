#ifndef java_awt_image_ImageProducer_H
#define java_awt_image_ImageProducer_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace awt {
namespace image {
class ImageConsumer;
}
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace image {
class ImageProducer : public java::lang::Object {
  public:
    ImageProducer(JavaMarker* dummy);
    ImageProducer(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual void addConsumer(java::awt::image::ImageConsumer* arg1); // public abstract void java.awt.image.ImageProducer.addConsumer(java.awt.image.ImageConsumer)
    virtual bool isConsumer(java::awt::image::ImageConsumer* arg1); // public abstract boolean java.awt.image.ImageProducer.isConsumer(java.awt.image.ImageConsumer)
    virtual void removeConsumer(java::awt::image::ImageConsumer* arg1); // public abstract void java.awt.image.ImageProducer.removeConsumer(java.awt.image.ImageConsumer)
    virtual void requestTopDownLeftRightResend(java::awt::image::ImageConsumer* arg1); // public abstract void java.awt.image.ImageProducer.requestTopDownLeftRightResend(java.awt.image.ImageConsumer)
    virtual void startProduction(java::awt::image::ImageConsumer* arg1); // public abstract void java.awt.image.ImageProducer.startProduction(java.awt.image.ImageConsumer)

};
}
}
}
#endif
