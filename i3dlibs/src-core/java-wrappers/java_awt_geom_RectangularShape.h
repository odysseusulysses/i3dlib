#ifndef java_awt_geom_RectangularShape_H
#define java_awt_geom_RectangularShape_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace awt {
class Shape;
}
}

namespace java {
namespace lang {
class Cloneable;
}
}

namespace java {
namespace awt {
namespace geom {
class Rectangle2D;
}
}
}

namespace java {
namespace awt {
namespace geom {
class Point2D;
}
}
}

namespace java {
namespace awt {
class Rectangle;
}
}

namespace java {
namespace awt {
namespace geom {
class PathIterator;
}
}
}

namespace java {
namespace awt {
namespace geom {
class AffineTransform;
}
}
}

namespace java {
namespace awt {
namespace geom {
class Dimension2D;
}
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace geom {
class RectangularShape : public java::lang::Object {
  public:
    RectangularShape(JavaMarker* dummy);
    RectangularShape(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual java::lang::Object* clone(); // public java.lang.Object java.awt.geom.RectangularShape.clone()
    virtual bool contains(java::awt::geom::Rectangle2D* arg1); // public boolean java.awt.geom.RectangularShape.contains(java.awt.geom.Rectangle2D)
    virtual bool contains(java::awt::geom::Point2D* arg1); // public boolean java.awt.geom.RectangularShape.contains(java.awt.geom.Point2D)
    virtual bool isEmpty(); // public abstract boolean java.awt.geom.RectangularShape.isEmpty()
    virtual bool intersects(java::awt::geom::Rectangle2D* arg1); // public boolean java.awt.geom.RectangularShape.intersects(java.awt.geom.Rectangle2D)
    virtual double getHeight(); // public abstract double java.awt.geom.RectangularShape.getHeight()
    virtual double getMinX(); // public double java.awt.geom.RectangularShape.getMinX()
    virtual double getMinY(); // public double java.awt.geom.RectangularShape.getMinY()
    virtual double getWidth(); // public abstract double java.awt.geom.RectangularShape.getWidth()
    virtual java::awt::Rectangle* getBounds(); // public java.awt.Rectangle java.awt.geom.RectangularShape.getBounds()
    virtual double getX(); // public abstract double java.awt.geom.RectangularShape.getX()
    virtual double getY(); // public abstract double java.awt.geom.RectangularShape.getY()
    virtual java::awt::geom::PathIterator* getPathIterator(java::awt::geom::AffineTransform* arg1, double arg2); // public java.awt.geom.PathIterator java.awt.geom.RectangularShape.getPathIterator(java.awt.geom.AffineTransform,double)
    virtual double getMaxX(); // public double java.awt.geom.RectangularShape.getMaxX()
    virtual double getMaxY(); // public double java.awt.geom.RectangularShape.getMaxY()
    virtual void setFrame(java::awt::geom::Rectangle2D* arg1); // public void java.awt.geom.RectangularShape.setFrame(java.awt.geom.Rectangle2D)
    virtual void setFrame(java::awt::geom::Point2D* arg1, java::awt::geom::Dimension2D* arg2); // public void java.awt.geom.RectangularShape.setFrame(java.awt.geom.Point2D,java.awt.geom.Dimension2D)
    virtual void setFrame(double arg1, double arg2, double arg3, double arg4); // public abstract void java.awt.geom.RectangularShape.setFrame(double,double,double,double)
    virtual void setFrameFromDiagonal(java::awt::geom::Point2D* arg1, java::awt::geom::Point2D* arg2); // public void java.awt.geom.RectangularShape.setFrameFromDiagonal(java.awt.geom.Point2D,java.awt.geom.Point2D)
    virtual void setFrameFromDiagonal(double arg1, double arg2, double arg3, double arg4); // public void java.awt.geom.RectangularShape.setFrameFromDiagonal(double,double,double,double)
    virtual double getCenterX(); // public double java.awt.geom.RectangularShape.getCenterX()
    virtual double getCenterY(); // public double java.awt.geom.RectangularShape.getCenterY()
    virtual java::awt::geom::Rectangle2D* getFrame(); // public java.awt.geom.Rectangle2D java.awt.geom.RectangularShape.getFrame()
    virtual void setFrameFromCenter(java::awt::geom::Point2D* arg1, java::awt::geom::Point2D* arg2); // public void java.awt.geom.RectangularShape.setFrameFromCenter(java.awt.geom.Point2D,java.awt.geom.Point2D)
    virtual void setFrameFromCenter(double arg1, double arg2, double arg3, double arg4); // public void java.awt.geom.RectangularShape.setFrameFromCenter(double,double,double,double)

};
}
}
}
#endif
