#ifndef java_awt_font_TextHitInfo_H
#define java_awt_font_TextHitInfo_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace lang {
class String;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace font {
class TextHitInfo : public java::lang::Object {
  public:
    TextHitInfo(JavaMarker* dummy);
    TextHitInfo(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual int hashCode(); // public int java.awt.font.TextHitInfo.hashCode()
    virtual bool equals(java::lang::Object* arg1); // public boolean java.awt.font.TextHitInfo.equals(java.lang.Object)
    virtual bool equals(java::awt::font::TextHitInfo* arg1); // public boolean java.awt.font.TextHitInfo.equals(java.awt.font.TextHitInfo)
    virtual const char* toString(); // public java.lang.String java.awt.font.TextHitInfo.toString()
    virtual java::awt::font::TextHitInfo* leading(int arg1); // public static java.awt.font.TextHitInfo java.awt.font.TextHitInfo.leading(int)
    virtual java::awt::font::TextHitInfo* afterOffset(int arg1); // public static java.awt.font.TextHitInfo java.awt.font.TextHitInfo.afterOffset(int)
    virtual java::awt::font::TextHitInfo* beforeOffset(int arg1); // public static java.awt.font.TextHitInfo java.awt.font.TextHitInfo.beforeOffset(int)
    virtual int getCharIndex(); // public int java.awt.font.TextHitInfo.getCharIndex()
    virtual int getInsertionIndex(); // public int java.awt.font.TextHitInfo.getInsertionIndex()
    virtual java::awt::font::TextHitInfo* getOffsetHit(int arg1); // public java.awt.font.TextHitInfo java.awt.font.TextHitInfo.getOffsetHit(int)
    virtual java::awt::font::TextHitInfo* getOtherHit(); // public java.awt.font.TextHitInfo java.awt.font.TextHitInfo.getOtherHit()
    virtual bool isLeadingEdge(); // public boolean java.awt.font.TextHitInfo.isLeadingEdge()
    virtual java::awt::font::TextHitInfo* trailing(int arg1); // public static java.awt.font.TextHitInfo java.awt.font.TextHitInfo.trailing(int)

};
}
}
}
#endif
