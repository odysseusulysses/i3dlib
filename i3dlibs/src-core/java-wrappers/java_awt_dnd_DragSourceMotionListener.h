#ifndef java_awt_dnd_DragSourceMotionListener_H
#define java_awt_dnd_DragSourceMotionListener_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace util {
class EventListener;
}
}

namespace java {
namespace awt {
namespace dnd {
class DragSourceDragEvent;
}
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace dnd {
class DragSourceMotionListener : public java::lang::Object {
  public:
    DragSourceMotionListener(JavaMarker* dummy);
    DragSourceMotionListener(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual void dragMouseMoved(java::awt::dnd::DragSourceDragEvent* arg1); // public abstract void java.awt.dnd.DragSourceMotionListener.dragMouseMoved(java.awt.dnd.DragSourceDragEvent)

};
}
}
}
#endif
