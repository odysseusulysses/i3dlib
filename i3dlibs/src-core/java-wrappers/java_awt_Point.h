#ifndef java_awt_Point_H
#define java_awt_Point_H
#include <jni.h>
#include <java_marker.h>
#include <java_awt_geom_Point2D.h>

namespace java {
namespace io {
class Serializable;
}
}

namespace java {
namespace lang {
class Object;
}
}

namespace java {
namespace lang {
class String;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
class Point : public java::awt::geom::Point2D {
  public:
    Point(JavaMarker* dummy);
    Point(jobject obj);
    Point(java::awt::Point* arg1); // public java.awt.Point(java.awt.Point)
    Point(int arg1, int arg2); // public java.awt.Point(int,int)
    Point(); // public java.awt.Point()

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual bool equals(java::lang::Object* arg1); // public boolean java.awt.Point.equals(java.lang.Object)
    virtual const char* toString(); // public java.lang.String java.awt.Point.toString()
    virtual java::awt::Point* getLocation(); // public java.awt.Point java.awt.Point.getLocation()
    virtual double getX(); // public double java.awt.Point.getX()
    virtual double getY(); // public double java.awt.Point.getY()
    virtual void move(int arg1, int arg2); // public void java.awt.Point.move(int,int)
    virtual void setLocation(java::awt::Point* arg1); // public void java.awt.Point.setLocation(java.awt.Point)
    virtual void setLocation(double arg1, double arg2); // public void java.awt.Point.setLocation(double,double)
    virtual void setLocation(int arg1, int arg2); // public void java.awt.Point.setLocation(int,int)
    virtual void translate(int arg1, int arg2); // public void java.awt.Point.translate(int,int)

    int x; // public int java.awt.Point.x
    int y; // public int java.awt.Point.y
};
}
}
#endif
