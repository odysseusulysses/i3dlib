#ifndef loci_formats_DataTools_H
#define loci_formats_DataTools_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace io {
class DataOutput;
}
}

namespace java {
namespace io {
class DataInput;
}
}

namespace java {
namespace lang {
class String;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace loci {
namespace formats {
class DataTools : public java::lang::Object {
  public:
    DataTools(JavaMarker* dummy);
    DataTools(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual void writeInt(java::io::DataOutput* arg1, int arg2, bool arg3); // public static void loci.formats.DataTools.writeInt(java.io.DataOutput,int,boolean) throws java.io.IOException
    virtual char swap(char arg1); // public static char loci.formats.DataTools.swap(char)
    virtual int swap(int arg1); // public static int loci.formats.DataTools.swap(int)
    virtual long swap(long arg1); // public static long loci.formats.DataTools.swap(long)
    virtual short swap(short arg1); // public static short loci.formats.DataTools.swap(short)
    virtual void writeShort(java::io::DataOutput* arg1, int arg2, bool arg3); // public static void loci.formats.DataTools.writeShort(java.io.DataOutput,int,boolean) throws java.io.IOException
    virtual float readFloat(java::io::DataInput* arg1, bool arg2); // public static float loci.formats.DataTools.readFloat(java.io.DataInput,boolean) throws java.io.IOException
    virtual double readDouble(java::io::DataInput* arg1, bool arg2); // public static double loci.formats.DataTools.readDouble(java.io.DataInput,boolean) throws java.io.IOException
    virtual short readUnsignedByte(java::io::DataInput* arg1); // public static short loci.formats.DataTools.readUnsignedByte(java.io.DataInput) throws java.io.IOException
    virtual const char* stripString(const char* arg1); // public static java.lang.String loci.formats.DataTools.stripString(java.lang.String)
    virtual const char* convertDate(long arg1, int arg2); // public static java.lang.String loci.formats.DataTools.convertDate(long,int)
    virtual const char* sanitize(const char* arg1); // public static java.lang.String loci.formats.DataTools.sanitize(java.lang.String)
    virtual char readSignedByte(java::io::DataInput* arg1); // public static byte loci.formats.DataTools.readSignedByte(java.io.DataInput) throws java.io.IOException
    virtual short read2SignedBytes(java::io::DataInput* arg1, bool arg2); // public static short loci.formats.DataTools.read2SignedBytes(java.io.DataInput,boolean) throws java.io.IOException
    virtual int read2UnsignedBytes(java::io::DataInput* arg1, bool arg2); // public static int loci.formats.DataTools.read2UnsignedBytes(java.io.DataInput,boolean) throws java.io.IOException
    virtual int read4SignedBytes(java::io::DataInput* arg1, bool arg2); // public static int loci.formats.DataTools.read4SignedBytes(java.io.DataInput,boolean) throws java.io.IOException
    virtual long read4UnsignedBytes(java::io::DataInput* arg1, bool arg2); // public static long loci.formats.DataTools.read4UnsignedBytes(java.io.DataInput,boolean) throws java.io.IOException
    virtual long read8SignedBytes(java::io::DataInput* arg1, bool arg2); // public static long loci.formats.DataTools.read8SignedBytes(java.io.DataInput,boolean) throws java.io.IOException
    virtual void writeString(java::io::DataOutput* arg1, const char* arg2); // public static void loci.formats.DataTools.writeString(java.io.DataOutput,java.lang.String) throws java.io.IOException
    virtual short bytesToShort(JavaByteArray* arg1, int arg2, bool arg3); // public static short loci.formats.DataTools.bytesToShort(byte[],int,boolean)
    virtual short bytesToShort(JavaShortArray* arg1, bool arg2); // public static short loci.formats.DataTools.bytesToShort(short[],boolean)
    virtual short bytesToShort(JavaByteArray* arg1, int arg2, int arg3, bool arg4); // public static short loci.formats.DataTools.bytesToShort(byte[],int,int,boolean)
    virtual short bytesToShort(JavaShortArray* arg1, int arg2, bool arg3); // public static short loci.formats.DataTools.bytesToShort(short[],int,boolean)
    virtual short bytesToShort(JavaByteArray* arg1, bool arg2); // public static short loci.formats.DataTools.bytesToShort(byte[],boolean)
    virtual short bytesToShort(JavaShortArray* arg1, int arg2, int arg3, bool arg4); // public static short loci.formats.DataTools.bytesToShort(short[],int,int,boolean)
    virtual int bytesToInt(JavaShortArray* arg1, int arg2, bool arg3); // public static int loci.formats.DataTools.bytesToInt(short[],int,boolean)
    virtual int bytesToInt(JavaByteArray* arg1, int arg2, bool arg3); // public static int loci.formats.DataTools.bytesToInt(byte[],int,boolean)
    virtual int bytesToInt(JavaByteArray* arg1, int arg2, int arg3, bool arg4); // public static int loci.formats.DataTools.bytesToInt(byte[],int,int,boolean)
    virtual int bytesToInt(JavaByteArray* arg1, bool arg2); // public static int loci.formats.DataTools.bytesToInt(byte[],boolean)
    virtual int bytesToInt(JavaShortArray* arg1, bool arg2); // public static int loci.formats.DataTools.bytesToInt(short[],boolean)
    virtual int bytesToInt(JavaShortArray* arg1, int arg2, int arg3, bool arg4); // public static int loci.formats.DataTools.bytesToInt(short[],int,int,boolean)
    virtual long bytesToLong(JavaByteArray* arg1, int arg2, int arg3, bool arg4); // public static long loci.formats.DataTools.bytesToLong(byte[],int,int,boolean)
    virtual long bytesToLong(JavaByteArray* arg1, int arg2, bool arg3); // public static long loci.formats.DataTools.bytesToLong(byte[],int,boolean)
    virtual long bytesToLong(JavaByteArray* arg1, bool arg2); // public static long loci.formats.DataTools.bytesToLong(byte[],boolean)
    virtual long bytesToLong(JavaShortArray* arg1, bool arg2); // public static long loci.formats.DataTools.bytesToLong(short[],boolean)
    virtual long bytesToLong(JavaShortArray* arg1, int arg2, bool arg3); // public static long loci.formats.DataTools.bytesToLong(short[],int,boolean)
    virtual long bytesToLong(JavaShortArray* arg1, int arg2, int arg3, bool arg4); // public static long loci.formats.DataTools.bytesToLong(short[],int,int,boolean)
    virtual JavaIntArray* makeSigned(JavaIntArray* arg1); // public static int[] loci.formats.DataTools.makeSigned(int[])
    virtual JavaShortArray* makeSigned(JavaShortArray* arg1); // public static short[] loci.formats.DataTools.makeSigned(short[])
    virtual JavaByteArray* makeSigned(JavaByteArray* arg1); // public static byte[] loci.formats.DataTools.makeSigned(byte[])
    virtual java::lang::Object* makeDataArray(JavaByteArray* arg1, int arg2, bool arg3, bool arg4); // public static java.lang.Object loci.formats.DataTools.makeDataArray(byte[],int,boolean,boolean)
    virtual bool samePrefix(const char* arg1, const char* arg2); // public static boolean loci.formats.DataTools.samePrefix(java.lang.String,java.lang.String)
    virtual JavaFloatArray* normalizeFloats(JavaFloatArray* arg1); // public static float[] loci.formats.DataTools.normalizeFloats(float[])

    int UNIX; // public static final int loci.formats.DataTools.UNIX
    int COBOL; // public static final int loci.formats.DataTools.COBOL
    long UNIX_EPOCH; // public static final long loci.formats.DataTools.UNIX_EPOCH
    long COBOL_EPOCH; // public static final long loci.formats.DataTools.COBOL_EPOCH
};
}
}
#endif
