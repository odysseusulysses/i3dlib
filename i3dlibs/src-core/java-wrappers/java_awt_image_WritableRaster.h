#ifndef java_awt_image_WritableRaster_H
#define java_awt_image_WritableRaster_H
#include <jni.h>
#include <java_marker.h>
#include <java_awt_image_Raster.h>

namespace java {
namespace lang {
class Object;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace image {
class WritableRaster : public java::awt::image::Raster {
  public:
    WritableRaster(JavaMarker* dummy);
    WritableRaster(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual java::awt::image::WritableRaster* createWritableChild(int arg1, int arg2, int arg3, int arg4, int arg5, int arg6, JavaIntArray* arg7); // public java.awt.image.WritableRaster java.awt.image.WritableRaster.createWritableChild(int,int,int,int,int,int,int[])
    virtual void setDataElements(int arg1, int arg2, java::awt::image::Raster* arg3); // public void java.awt.image.WritableRaster.setDataElements(int,int,java.awt.image.Raster)
    virtual void setDataElements(int arg1, int arg2, int arg3, int arg4, java::lang::Object* arg5); // public void java.awt.image.WritableRaster.setDataElements(int,int,int,int,java.lang.Object)
    virtual void setDataElements(int arg1, int arg2, java::lang::Object* arg3); // public void java.awt.image.WritableRaster.setDataElements(int,int,java.lang.Object)
    virtual void setPixels(int arg1, int arg2, int arg3, int arg4, JavaIntArray* arg5); // public void java.awt.image.WritableRaster.setPixels(int,int,int,int,int[])
    virtual void setPixels(int arg1, int arg2, int arg3, int arg4, JavaDoubleArray* arg5); // public void java.awt.image.WritableRaster.setPixels(int,int,int,int,double[])
    virtual void setPixels(int arg1, int arg2, int arg3, int arg4, JavaFloatArray* arg5); // public void java.awt.image.WritableRaster.setPixels(int,int,int,int,float[])
    virtual java::awt::image::WritableRaster* createWritableTranslatedChild(int arg1, int arg2); // public java.awt.image.WritableRaster java.awt.image.WritableRaster.createWritableTranslatedChild(int,int)
    virtual java::awt::image::WritableRaster* getWritableParent(); // public java.awt.image.WritableRaster java.awt.image.WritableRaster.getWritableParent()
    virtual void setPixel(int arg1, int arg2, JavaDoubleArray* arg3); // public void java.awt.image.WritableRaster.setPixel(int,int,double[])
    virtual void setPixel(int arg1, int arg2, JavaFloatArray* arg3); // public void java.awt.image.WritableRaster.setPixel(int,int,float[])
    virtual void setPixel(int arg1, int arg2, JavaIntArray* arg3); // public void java.awt.image.WritableRaster.setPixel(int,int,int[])
    virtual void setRect(java::awt::image::Raster* arg1); // public void java.awt.image.WritableRaster.setRect(java.awt.image.Raster)
    virtual void setRect(int arg1, int arg2, java::awt::image::Raster* arg3); // public void java.awt.image.WritableRaster.setRect(int,int,java.awt.image.Raster)
    virtual void setSample(int arg1, int arg2, int arg3, float arg4); // public void java.awt.image.WritableRaster.setSample(int,int,int,float)
    virtual void setSample(int arg1, int arg2, int arg3, double arg4); // public void java.awt.image.WritableRaster.setSample(int,int,int,double)
    virtual void setSample(int arg1, int arg2, int arg3, int arg4); // public void java.awt.image.WritableRaster.setSample(int,int,int,int)
    virtual void setSamples(int arg1, int arg2, int arg3, int arg4, int arg5, JavaFloatArray* arg6); // public void java.awt.image.WritableRaster.setSamples(int,int,int,int,int,float[])
    virtual void setSamples(int arg1, int arg2, int arg3, int arg4, int arg5, JavaDoubleArray* arg6); // public void java.awt.image.WritableRaster.setSamples(int,int,int,int,int,double[])
    virtual void setSamples(int arg1, int arg2, int arg3, int arg4, int arg5, JavaIntArray* arg6); // public void java.awt.image.WritableRaster.setSamples(int,int,int,int,int,int[])

};
}
}
}
#endif
