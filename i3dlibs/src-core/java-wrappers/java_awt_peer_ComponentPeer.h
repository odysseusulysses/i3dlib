#ifndef java_awt_peer_ComponentPeer_H
#define java_awt_peer_ComponentPeer_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace awt {
class Graphics;
}
}

namespace java {
namespace awt {
class BufferCapabilities__FlipContents;
}
}

namespace java {
namespace awt {
namespace image {
class ColorModel;
}
}
}

namespace java {
namespace awt {
class Image;
}
}

namespace java {
namespace awt {
namespace image {
class ImageProducer;
}
}
}

namespace java {
namespace awt {
class Rectangle;
}
}

namespace java {
namespace awt {
class Color;
}
}

namespace java {
namespace awt {
class FontMetrics;
}
}

namespace java {
namespace awt {
class Font;
}
}

namespace java {
namespace awt {
class GraphicsConfiguration;
}
}

namespace java {
namespace awt {
class Point;
}
}

namespace java {
namespace awt {
class Dimension;
}
}

namespace java {
namespace awt {
class Toolkit;
}
}

namespace java {
namespace awt {
class AWTEvent;
}
}

namespace java {
namespace awt {
class Component;
}
}

namespace sun {
namespace awt {
class CausedFocusEvent__Cause;
}
}

namespace java {
namespace awt {
namespace peer {
class ContainerPeer;
}
}
}

namespace java {
namespace awt {
namespace image {
class ImageObserver;
}
}
}

namespace java {
namespace awt {
namespace image {
class VolatileImage;
}
}
}

namespace java {
namespace awt {
namespace event {
class PaintEvent;
}
}
}

namespace java {
namespace awt {
class BufferCapabilities;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace peer {
class ComponentPeer : public java::lang::Object {
  public:
    ComponentPeer(JavaMarker* dummy);
    ComponentPeer(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual void print(java::awt::Graphics* arg1); // public abstract void java.awt.peer.ComponentPeer.print(java.awt.Graphics)
    virtual void flip(java::awt::BufferCapabilities__FlipContents* arg1); // public abstract void java.awt.peer.ComponentPeer.flip(java.awt.BufferCapabilities$FlipContents)
    virtual void disable(); // public abstract void java.awt.peer.ComponentPeer.disable()
    virtual void enable(); // public abstract void java.awt.peer.ComponentPeer.enable()
    virtual java::awt::image::ColorModel* getColorModel(); // public abstract java.awt.image.ColorModel java.awt.peer.ComponentPeer.getColorModel()
    virtual java::awt::Graphics* getGraphics(); // public abstract java.awt.Graphics java.awt.peer.ComponentPeer.getGraphics()
    virtual java::awt::Image* createImage(java::awt::image::ImageProducer* arg1); // public abstract java.awt.Image java.awt.peer.ComponentPeer.createImage(java.awt.image.ImageProducer)
    virtual java::awt::Image* createImage(int arg1, int arg2); // public abstract java.awt.Image java.awt.peer.ComponentPeer.createImage(int,int)
    virtual java::awt::Rectangle* getBounds(); // public abstract java.awt.Rectangle java.awt.peer.ComponentPeer.getBounds()
    virtual void reshape(int arg1, int arg2, int arg3, int arg4); // public abstract void java.awt.peer.ComponentPeer.reshape(int,int,int,int)
    virtual void setBounds(int arg1, int arg2, int arg3, int arg4, int arg5); // public abstract void java.awt.peer.ComponentPeer.setBounds(int,int,int,int,int)
    virtual void setBackground(java::awt::Color* arg1); // public abstract void java.awt.peer.ComponentPeer.setBackground(java.awt.Color)
    virtual void dispose(); // public abstract void java.awt.peer.ComponentPeer.dispose()
    virtual java::awt::FontMetrics* getFontMetrics(java::awt::Font* arg1); // public abstract java.awt.FontMetrics java.awt.peer.ComponentPeer.getFontMetrics(java.awt.Font)
    virtual void setFont(java::awt::Font* arg1); // public abstract void java.awt.peer.ComponentPeer.setFont(java.awt.Font)
    virtual void layout(); // public abstract void java.awt.peer.ComponentPeer.layout()
    virtual void setVisible(bool arg1); // public abstract void java.awt.peer.ComponentPeer.setVisible(boolean)
    virtual void setForeground(java::awt::Color* arg1); // public abstract void java.awt.peer.ComponentPeer.setForeground(java.awt.Color)
    virtual java::awt::GraphicsConfiguration* getGraphicsConfiguration(); // public abstract java.awt.GraphicsConfiguration java.awt.peer.ComponentPeer.getGraphicsConfiguration()
    virtual java::awt::Point* getLocationOnScreen(); // public abstract java.awt.Point java.awt.peer.ComponentPeer.getLocationOnScreen()
    virtual java::awt::Dimension* getMinimumSize(); // public abstract java.awt.Dimension java.awt.peer.ComponentPeer.getMinimumSize()
    virtual java::awt::Dimension* getPreferredSize(); // public abstract java.awt.Dimension java.awt.peer.ComponentPeer.getPreferredSize()
    virtual java::awt::Toolkit* getToolkit(); // public abstract java.awt.Toolkit java.awt.peer.ComponentPeer.getToolkit()
    virtual void handleEvent(java::awt::AWTEvent* arg1); // public abstract void java.awt.peer.ComponentPeer.handleEvent(java.awt.AWTEvent)
    virtual void hide(); // public abstract void java.awt.peer.ComponentPeer.hide()
    virtual bool isFocusable(); // public abstract boolean java.awt.peer.ComponentPeer.isFocusable()
    virtual bool requestFocus(java::awt::Component* arg1, bool arg2, bool arg3, long arg4, sun::awt::CausedFocusEvent__Cause* arg5); // public abstract boolean java.awt.peer.ComponentPeer.requestFocus(java.awt.Component,boolean,boolean,long,sun.awt.CausedFocusEvent$Cause)
    virtual void show(); // public abstract void java.awt.peer.ComponentPeer.show()
    virtual bool isReparentSupported(); // public abstract boolean java.awt.peer.ComponentPeer.isReparentSupported()
    virtual java::awt::Dimension* minimumSize(); // public abstract java.awt.Dimension java.awt.peer.ComponentPeer.minimumSize()
    virtual void paint(java::awt::Graphics* arg1); // public abstract void java.awt.peer.ComponentPeer.paint(java.awt.Graphics)
    virtual java::awt::Dimension* preferredSize(); // public abstract java.awt.Dimension java.awt.peer.ComponentPeer.preferredSize()
    virtual void reparent(java::awt::peer::ContainerPeer* arg1); // public abstract void java.awt.peer.ComponentPeer.reparent(java.awt.peer.ContainerPeer)
    virtual void updateCursorImmediately(); // public abstract void java.awt.peer.ComponentPeer.updateCursorImmediately()
    virtual int checkImage(java::awt::Image* arg1, int arg2, int arg3, java::awt::image::ImageObserver* arg4); // public abstract int java.awt.peer.ComponentPeer.checkImage(java.awt.Image,int,int,java.awt.image.ImageObserver)
    virtual java::awt::image::VolatileImage* createVolatileImage(int arg1, int arg2); // public abstract java.awt.image.VolatileImage java.awt.peer.ComponentPeer.createVolatileImage(int,int)
    virtual void destroyBuffers(); // public abstract void java.awt.peer.ComponentPeer.destroyBuffers()
    virtual java::awt::Image* getBackBuffer(); // public abstract java.awt.Image java.awt.peer.ComponentPeer.getBackBuffer()
    virtual bool handlesWheelScrolling(); // public abstract boolean java.awt.peer.ComponentPeer.handlesWheelScrolling()
    virtual bool prepareImage(java::awt::Image* arg1, int arg2, int arg3, java::awt::image::ImageObserver* arg4); // public abstract boolean java.awt.peer.ComponentPeer.prepareImage(java.awt.Image,int,int,java.awt.image.ImageObserver)
    virtual void repaint(long arg1, int arg2, int arg3, int arg4, int arg5); // public abstract void java.awt.peer.ComponentPeer.repaint(long,int,int,int,int)
    virtual void setEnabled(bool arg1); // public abstract void java.awt.peer.ComponentPeer.setEnabled(boolean)
    virtual bool canDetermineObscurity(); // public abstract boolean java.awt.peer.ComponentPeer.canDetermineObscurity()
    virtual void coalescePaintEvent(java::awt::event::PaintEvent* arg1); // public abstract void java.awt.peer.ComponentPeer.coalescePaintEvent(java.awt.event.PaintEvent)
    virtual void createBuffers(int arg1, java::awt::BufferCapabilities* arg2); // public abstract void java.awt.peer.ComponentPeer.createBuffers(int,java.awt.BufferCapabilities) throws java.awt.AWTException
    virtual bool isObscured(); // public abstract boolean java.awt.peer.ComponentPeer.isObscured()

    int SET_LOCATION; // public static final int java.awt.peer.ComponentPeer.SET_LOCATION
    int SET_SIZE; // public static final int java.awt.peer.ComponentPeer.SET_SIZE
    int SET_BOUNDS; // public static final int java.awt.peer.ComponentPeer.SET_BOUNDS
    int SET_CLIENT_SIZE; // public static final int java.awt.peer.ComponentPeer.SET_CLIENT_SIZE
    int RESET_OPERATION; // public static final int java.awt.peer.ComponentPeer.RESET_OPERATION
    int NO_EMBEDDED_CHECK; // public static final int java.awt.peer.ComponentPeer.NO_EMBEDDED_CHECK
    int DEFAULT_OPERATION; // public static final int java.awt.peer.ComponentPeer.DEFAULT_OPERATION
};
}
}
}
#endif
