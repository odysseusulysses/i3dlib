#ifndef java_awt_font_GlyphVector_H
#define java_awt_font_GlyphVector_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace lang {
class Cloneable;
}
}

namespace java {
namespace awt {
namespace font {
class FontRenderContext;
}
}
}

namespace java {
namespace awt {
class Font;
}
}

namespace java {
namespace awt {
namespace font {
class GlyphJustificationInfo;
}
}
}

namespace java {
namespace awt {
class Shape;
}
}

namespace java {
namespace awt {
namespace font {
class GlyphMetrics;
}
}
}

namespace java {
namespace awt {
class Rectangle;
}
}

namespace java {
namespace awt {
namespace geom {
class Point2D;
}
}
}

namespace java {
namespace awt {
namespace geom {
class AffineTransform;
}
}
}

namespace java {
namespace awt {
namespace geom {
class Rectangle2D;
}
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace font {
class GlyphVector : public java::lang::Object {
  public:
    GlyphVector(JavaMarker* dummy);
    GlyphVector(jobject obj);
    GlyphVector(); // public java.awt.font.GlyphVector()

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual bool equals(java::awt::font::GlyphVector* arg1); // public abstract boolean java.awt.font.GlyphVector.equals(java.awt.font.GlyphVector)
    virtual java::awt::font::FontRenderContext* getFontRenderContext(); // public abstract java.awt.font.FontRenderContext java.awt.font.GlyphVector.getFontRenderContext()
    virtual java::awt::Font* getFont(); // public abstract java.awt.Font java.awt.font.GlyphVector.getFont()
    virtual int getGlyphCharIndex(int arg1); // public int java.awt.font.GlyphVector.getGlyphCharIndex(int)
    virtual JavaIntArray* getGlyphCharIndices(int arg1, int arg2, JavaIntArray* arg3); // public int[] java.awt.font.GlyphVector.getGlyphCharIndices(int,int,int[])
    virtual int getGlyphCode(int arg1); // public abstract int java.awt.font.GlyphVector.getGlyphCode(int)
    virtual JavaIntArray* getGlyphCodes(int arg1, int arg2, JavaIntArray* arg3); // public abstract int[] java.awt.font.GlyphVector.getGlyphCodes(int,int,int[])
    virtual java::awt::font::GlyphJustificationInfo* getGlyphJustificationInfo(int arg1); // public abstract java.awt.font.GlyphJustificationInfo java.awt.font.GlyphVector.getGlyphJustificationInfo(int)
    virtual java::awt::Shape* getGlyphLogicalBounds(int arg1); // public abstract java.awt.Shape java.awt.font.GlyphVector.getGlyphLogicalBounds(int)
    virtual java::awt::font::GlyphMetrics* getGlyphMetrics(int arg1); // public abstract java.awt.font.GlyphMetrics java.awt.font.GlyphVector.getGlyphMetrics(int)
    virtual java::awt::Shape* getGlyphOutline(int arg1, float arg2, float arg3); // public java.awt.Shape java.awt.font.GlyphVector.getGlyphOutline(int,float,float)
    virtual java::awt::Shape* getGlyphOutline(int arg1); // public abstract java.awt.Shape java.awt.font.GlyphVector.getGlyphOutline(int)
    virtual java::awt::Rectangle* getGlyphPixelBounds(int arg1, java::awt::font::FontRenderContext* arg2, float arg3, float arg4); // public java.awt.Rectangle java.awt.font.GlyphVector.getGlyphPixelBounds(int,java.awt.font.FontRenderContext,float,float)
    virtual java::awt::geom::Point2D* getGlyphPosition(int arg1); // public abstract java.awt.geom.Point2D java.awt.font.GlyphVector.getGlyphPosition(int)
    virtual JavaFloatArray* getGlyphPositions(int arg1, int arg2, JavaFloatArray* arg3); // public abstract float[] java.awt.font.GlyphVector.getGlyphPositions(int,int,float[])
    virtual java::awt::geom::AffineTransform* getGlyphTransform(int arg1); // public abstract java.awt.geom.AffineTransform java.awt.font.GlyphVector.getGlyphTransform(int)
    virtual java::awt::Shape* getGlyphVisualBounds(int arg1); // public abstract java.awt.Shape java.awt.font.GlyphVector.getGlyphVisualBounds(int)
    virtual int getLayoutFlags(); // public int java.awt.font.GlyphVector.getLayoutFlags()
    virtual java::awt::geom::Rectangle2D* getLogicalBounds(); // public abstract java.awt.geom.Rectangle2D java.awt.font.GlyphVector.getLogicalBounds()
    virtual int getNumGlyphs(); // public abstract int java.awt.font.GlyphVector.getNumGlyphs()
    virtual java::awt::Shape* getOutline(float arg1, float arg2); // public abstract java.awt.Shape java.awt.font.GlyphVector.getOutline(float,float)
    virtual java::awt::Shape* getOutline(); // public abstract java.awt.Shape java.awt.font.GlyphVector.getOutline()
    virtual java::awt::Rectangle* getPixelBounds(java::awt::font::FontRenderContext* arg1, float arg2, float arg3); // public java.awt.Rectangle java.awt.font.GlyphVector.getPixelBounds(java.awt.font.FontRenderContext,float,float)
    virtual java::awt::geom::Rectangle2D* getVisualBounds(); // public abstract java.awt.geom.Rectangle2D java.awt.font.GlyphVector.getVisualBounds()
    virtual void performDefaultLayout(); // public abstract void java.awt.font.GlyphVector.performDefaultLayout()
    virtual void setGlyphPosition(int arg1, java::awt::geom::Point2D* arg2); // public abstract void java.awt.font.GlyphVector.setGlyphPosition(int,java.awt.geom.Point2D)
    virtual void setGlyphTransform(int arg1, java::awt::geom::AffineTransform* arg2); // public abstract void java.awt.font.GlyphVector.setGlyphTransform(int,java.awt.geom.AffineTransform)

    int FLAG_HAS_TRANSFORMS; // public static final int java.awt.font.GlyphVector.FLAG_HAS_TRANSFORMS
    int FLAG_HAS_POSITION_ADJUSTMENTS; // public static final int java.awt.font.GlyphVector.FLAG_HAS_POSITION_ADJUSTMENTS
    int FLAG_RUN_RTL; // public static final int java.awt.font.GlyphVector.FLAG_RUN_RTL
    int FLAG_COMPLEX_GLYPHS; // public static final int java.awt.font.GlyphVector.FLAG_COMPLEX_GLYPHS
    int FLAG_MASK; // public static final int java.awt.font.GlyphVector.FLAG_MASK
};
}
}
}
#endif
