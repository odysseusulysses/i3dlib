#ifndef java_awt_PopupMenu_H
#define java_awt_PopupMenu_H
#include <jni.h>
#include <java_marker.h>
#include <java_awt_Menu.h>

namespace java {
namespace lang {
class String;
}
}

namespace java {
namespace awt {
class MenuContainer;
}
}

namespace javax {
namespace accessibility {
class AccessibleContext;
}
}

namespace java {
namespace awt {
class Component;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
class PopupMenu : public java::awt::Menu {
  public:
    PopupMenu(JavaMarker* dummy);
    PopupMenu(jobject obj);
    PopupMenu(const char* arg1); // public java.awt.PopupMenu(java.lang.String) throws java.awt.HeadlessException
    PopupMenu(); // public java.awt.PopupMenu() throws java.awt.HeadlessException

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual java::awt::MenuContainer* getParent(); // public java.awt.MenuContainer java.awt.PopupMenu.getParent()
    virtual void addNotify(); // public void java.awt.PopupMenu.addNotify()
    virtual javax::accessibility::AccessibleContext* getAccessibleContext(); // public javax.accessibility.AccessibleContext java.awt.PopupMenu.getAccessibleContext()
    virtual void show(java::awt::Component* arg1, int arg2, int arg3); // public void java.awt.PopupMenu.show(java.awt.Component,int,int)

};
}
}
#endif
