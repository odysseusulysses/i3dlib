#ifndef java_lang_Float_H
#define java_lang_Float_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Number.h>

namespace java {
namespace lang {
class Comparable;
}
}

namespace java {
namespace lang {
class String;
}
}

namespace java {
namespace lang {
class Object;
}
}

namespace java {
namespace lang {
class Class;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace lang {
class Float : public java::lang::Number {
  public:
    Float(JavaMarker* dummy);
    Float(jobject obj);
    Float(float arg1); // public java.lang.Float(float)
    Float(double arg1); // public java.lang.Float(double)
    Float(const char* arg1); // public java.lang.Float(java.lang.String) throws java.lang.NumberFormatException

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual int floatToRawIntBits(float arg1); // public static native int java.lang.Float.floatToRawIntBits(float)
    virtual int floatToIntBits(float arg1); // public static int java.lang.Float.floatToIntBits(float)
    virtual float intBitsToFloat(int arg1); // public static native float java.lang.Float.intBitsToFloat(int)
    virtual int hashCode(); // public int java.lang.Float.hashCode()
    virtual int compareTo(java::lang::Object* arg1); // public int java.lang.Float.compareTo(java.lang.Object)
    virtual int compareTo(java::lang::Float* arg1); // public int java.lang.Float.compareTo(java.lang.Float)
    virtual bool equals(java::lang::Object* arg1); // public boolean java.lang.Float.equals(java.lang.Object)
    virtual const char* toString(float arg1); // public static java.lang.String java.lang.Float.toString(float)
    virtual const char* toString(); // public java.lang.String java.lang.Float.toString()
    virtual const char* toHexString(float arg1); // public static java.lang.String java.lang.Float.toHexString(float)
    virtual int compare(float arg1, float arg2); // public static int java.lang.Float.compare(float,float)
    virtual java::lang::Float* valueOf(float arg1); // public static java.lang.Float java.lang.Float.valueOf(float)
    virtual java::lang::Float* valueOf(const char* arg1); // public static java.lang.Float java.lang.Float.valueOf(java.lang.String) throws java.lang.NumberFormatException
    virtual bool isNaN(float arg1); // public static boolean java.lang.Float.isNaN(float)
    virtual bool isNaN(); // public boolean java.lang.Float.isNaN()
    virtual char byteValue(); // public byte java.lang.Float.byteValue()
    virtual double doubleValue(); // public double java.lang.Float.doubleValue()
    virtual float floatValue(); // public float java.lang.Float.floatValue()
    virtual int intValue(); // public int java.lang.Float.intValue()
    virtual bool isInfinite(); // public boolean java.lang.Float.isInfinite()
    virtual bool isInfinite(float arg1); // public static boolean java.lang.Float.isInfinite(float)
    virtual long longValue(); // public long java.lang.Float.longValue()
    virtual float parseFloat(const char* arg1); // public static float java.lang.Float.parseFloat(java.lang.String) throws java.lang.NumberFormatException
    virtual short shortValue(); // public short java.lang.Float.shortValue()

//    float POSITIVE_INFINITY; // public static final float java.lang.Float.POSITIVE_INFINITY
//    float NEGATIVE_INFINITY; // public static final float java.lang.Float.NEGATIVE_INFINITY
//    float NaN; // public static final float java.lang.Float.NaN
//    float MAX_VALUE; // public static final float java.lang.Float.MAX_VALUE
//    float MIN_NORMAL; // public static final float java.lang.Float.MIN_NORMAL
//    float MIN_VALUE; // public static final float java.lang.Float.MIN_VALUE
//    int MAX_EXPONENT; // public static final int java.lang.Float.MAX_EXPONENT
//    int MIN_EXPONENT; // public static final int java.lang.Float.MIN_EXPONENT
    int SIZE; // public static final int java.lang.Float.SIZE
    java::lang::Class* TYPE; // public static final java.lang.Class java.lang.Float.TYPE
};
}
}
#endif
