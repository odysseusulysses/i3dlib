#ifndef java_awt_image_BufferedImageOp_H
#define java_awt_image_BufferedImageOp_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace awt {
namespace image {
class BufferedImage;
}
}
}

namespace java {
namespace awt {
namespace geom {
class Rectangle2D;
}
}
}

namespace java {
namespace awt {
class RenderingHints;
}
}

namespace java {
namespace awt {
namespace image {
class ColorModel;
}
}
}

namespace java {
namespace awt {
namespace geom {
class Point2D;
}
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace image {
class BufferedImageOp : public java::lang::Object {
  public:
    BufferedImageOp(JavaMarker* dummy);
    BufferedImageOp(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual java::awt::image::BufferedImage* filter(java::awt::image::BufferedImage* arg1, java::awt::image::BufferedImage* arg2); // public abstract java.awt.image.BufferedImage java.awt.image.BufferedImageOp.filter(java.awt.image.BufferedImage,java.awt.image.BufferedImage)
    virtual java::awt::geom::Rectangle2D* getBounds2D(java::awt::image::BufferedImage* arg1); // public abstract java.awt.geom.Rectangle2D java.awt.image.BufferedImageOp.getBounds2D(java.awt.image.BufferedImage)
    virtual java::awt::RenderingHints* getRenderingHints(); // public abstract java.awt.RenderingHints java.awt.image.BufferedImageOp.getRenderingHints()
    virtual java::awt::image::BufferedImage* createCompatibleDestImage(java::awt::image::BufferedImage* arg1, java::awt::image::ColorModel* arg2); // public abstract java.awt.image.BufferedImage java.awt.image.BufferedImageOp.createCompatibleDestImage(java.awt.image.BufferedImage,java.awt.image.ColorModel)
    virtual java::awt::geom::Point2D* getPoint2D(java::awt::geom::Point2D* arg1, java::awt::geom::Point2D* arg2); // public abstract java.awt.geom.Point2D java.awt.image.BufferedImageOp.getPoint2D(java.awt.geom.Point2D,java.awt.geom.Point2D)

};
}
}
}
#endif
