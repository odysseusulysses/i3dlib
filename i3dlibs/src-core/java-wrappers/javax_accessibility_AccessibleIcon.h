#ifndef javax_accessibility_AccessibleIcon_H
#define javax_accessibility_AccessibleIcon_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace lang {
class String;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace javax {
namespace accessibility {
class AccessibleIcon : public java::lang::Object {
  public:
    AccessibleIcon(JavaMarker* dummy);
    AccessibleIcon(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual const char* getAccessibleIconDescription(); // public abstract java.lang.String javax.accessibility.AccessibleIcon.getAccessibleIconDescription()
    virtual int getAccessibleIconHeight(); // public abstract int javax.accessibility.AccessibleIcon.getAccessibleIconHeight()
    virtual int getAccessibleIconWidth(); // public abstract int javax.accessibility.AccessibleIcon.getAccessibleIconWidth()
    virtual void setAccessibleIconDescription(const char* arg1); // public abstract void javax.accessibility.AccessibleIcon.setAccessibleIconDescription(java.lang.String)

};
}
}
#endif
