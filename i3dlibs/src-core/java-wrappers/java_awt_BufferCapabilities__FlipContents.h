#ifndef java_awt_BufferCapabilities__FlipContents_H
#define java_awt_BufferCapabilities__FlipContents_H
#include <jni.h>
#include <java_marker.h>
#include <java_awt_AttributeValue.h>

namespace java {
namespace lang {
class String;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
class BufferCapabilities__FlipContents : public java::awt::AttributeValue {
  public:
    BufferCapabilities__FlipContents(JavaMarker* dummy);
    BufferCapabilities__FlipContents(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual int hashCode(); // public int java.awt.BufferCapabilities$FlipContents.hashCode()
    virtual const char* toString(); // public java.lang.String java.awt.BufferCapabilities$FlipContents.toString()

    java::awt::BufferCapabilities__FlipContents* UNDEFINED; // public static final java.awt.BufferCapabilities$FlipContents java.awt.BufferCapabilities$FlipContents.UNDEFINED
    java::awt::BufferCapabilities__FlipContents* BACKGROUND; // public static final java.awt.BufferCapabilities$FlipContents java.awt.BufferCapabilities$FlipContents.BACKGROUND
    java::awt::BufferCapabilities__FlipContents* PRIOR; // public static final java.awt.BufferCapabilities$FlipContents java.awt.BufferCapabilities$FlipContents.PRIOR
    java::awt::BufferCapabilities__FlipContents* COPIED; // public static final java.awt.BufferCapabilities$FlipContents java.awt.BufferCapabilities$FlipContents.COPIED
};
}
}
#endif
