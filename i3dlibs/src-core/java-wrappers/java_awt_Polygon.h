#ifndef java_awt_Polygon_H
#define java_awt_Polygon_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace awt {
class Shape;
}
}

namespace java {
namespace io {
class Serializable;
}
}

namespace java {
namespace awt {
namespace geom {
class Rectangle2D;
}
}
}

namespace java {
namespace awt {
namespace geom {
class Point2D;
}
}
}

namespace java {
namespace awt {
class Point;
}
}

namespace java {
namespace awt {
class Rectangle;
}
}

namespace java {
namespace awt {
namespace geom {
class PathIterator;
}
}
}

namespace java {
namespace awt {
namespace geom {
class AffineTransform;
}
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
class Polygon : public java::lang::Object {
  public:
    Polygon(JavaMarker* dummy);
    Polygon(jobject obj);
    Polygon(JavaIntArray* arg1, JavaIntArray* arg2, int arg3); // public java.awt.Polygon(int[],int[],int)
    Polygon(); // public java.awt.Polygon()

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual bool contains(java::awt::geom::Rectangle2D* arg1); // public boolean java.awt.Polygon.contains(java.awt.geom.Rectangle2D)
    virtual bool contains(double arg1, double arg2, double arg3, double arg4); // public boolean java.awt.Polygon.contains(double,double,double,double)
    virtual bool contains(java::awt::geom::Point2D* arg1); // public boolean java.awt.Polygon.contains(java.awt.geom.Point2D)
    virtual bool contains(double arg1, double arg2); // public boolean java.awt.Polygon.contains(double,double)
    virtual bool contains(int arg1, int arg2); // public boolean java.awt.Polygon.contains(int,int)
    virtual bool contains(java::awt::Point* arg1); // public boolean java.awt.Polygon.contains(java.awt.Point)
    virtual void reset(); // public void java.awt.Polygon.reset()
    virtual bool intersects(double arg1, double arg2, double arg3, double arg4); // public boolean java.awt.Polygon.intersects(double,double,double,double)
    virtual bool intersects(java::awt::geom::Rectangle2D* arg1); // public boolean java.awt.Polygon.intersects(java.awt.geom.Rectangle2D)
    virtual java::awt::Rectangle* getBounds(); // public java.awt.Rectangle java.awt.Polygon.getBounds()
    virtual java::awt::geom::Rectangle2D* getBounds2D(); // public java.awt.geom.Rectangle2D java.awt.Polygon.getBounds2D()
    virtual bool inside(int arg1, int arg2); // public boolean java.awt.Polygon.inside(int,int)
    virtual void translate(int arg1, int arg2); // public void java.awt.Polygon.translate(int,int)
    virtual java::awt::geom::PathIterator* getPathIterator(java::awt::geom::AffineTransform* arg1, double arg2); // public java.awt.geom.PathIterator java.awt.Polygon.getPathIterator(java.awt.geom.AffineTransform,double)
    virtual java::awt::geom::PathIterator* getPathIterator(java::awt::geom::AffineTransform* arg1); // public java.awt.geom.PathIterator java.awt.Polygon.getPathIterator(java.awt.geom.AffineTransform)
    virtual void addPoint(int arg1, int arg2); // public void java.awt.Polygon.addPoint(int,int)
    virtual java::awt::Rectangle* getBoundingBox(); // public java.awt.Rectangle java.awt.Polygon.getBoundingBox()
    virtual void invalidate(); // public void java.awt.Polygon.invalidate()

    int npoints; // public int java.awt.Polygon.npoints
    JavaIntArray* xpoints; // public int[] java.awt.Polygon.xpoints
    JavaIntArray* ypoints; // public int[] java.awt.Polygon.ypoints
};
}
}
#endif
