#ifndef javax_swing_text_AttributeSet_H
#define javax_swing_text_AttributeSet_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace util {
class Enumeration;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace javax {
namespace swing {
namespace text {
class AttributeSet : public java::lang::Object {
  public:
    AttributeSet(JavaMarker* dummy);
    AttributeSet(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual bool isDefined(java::lang::Object* arg1); // public abstract boolean javax.swing.text.AttributeSet.isDefined(java.lang.Object)
    virtual bool isEqual(javax::swing::text::AttributeSet* arg1); // public abstract boolean javax.swing.text.AttributeSet.isEqual(javax.swing.text.AttributeSet)
    virtual java::lang::Object* getAttribute(java::lang::Object* arg1); // public abstract java.lang.Object javax.swing.text.AttributeSet.getAttribute(java.lang.Object)
    virtual bool containsAttribute(java::lang::Object* arg1, java::lang::Object* arg2); // public abstract boolean javax.swing.text.AttributeSet.containsAttribute(java.lang.Object,java.lang.Object)
    virtual bool containsAttributes(javax::swing::text::AttributeSet* arg1); // public abstract boolean javax.swing.text.AttributeSet.containsAttributes(javax.swing.text.AttributeSet)
    virtual javax::swing::text::AttributeSet* copyAttributes(); // public abstract javax.swing.text.AttributeSet javax.swing.text.AttributeSet.copyAttributes()
    virtual int getAttributeCount(); // public abstract int javax.swing.text.AttributeSet.getAttributeCount()
    virtual java::util::Enumeration* getAttributeNames(); // public abstract java.util.Enumeration javax.swing.text.AttributeSet.getAttributeNames()
    virtual javax::swing::text::AttributeSet* getResolveParent(); // public abstract javax.swing.text.AttributeSet javax.swing.text.AttributeSet.getResolveParent()

    java::lang::Object* NameAttribute; // public static final java.lang.Object javax.swing.text.AttributeSet.NameAttribute
    java::lang::Object* ResolveAttribute; // public static final java.lang.Object javax.swing.text.AttributeSet.ResolveAttribute
};
}
}
}
#endif
