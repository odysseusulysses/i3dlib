#ifndef javax_accessibility_AccessibleText_H
#define javax_accessibility_AccessibleText_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace lang {
class String;
}
}

namespace javax {
namespace swing {
namespace text {
class AttributeSet;
}
}
}

namespace java {
namespace awt {
class Rectangle;
}
}

namespace java {
namespace awt {
class Point;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace javax {
namespace accessibility {
class AccessibleText : public java::lang::Object {
  public:
    AccessibleText(JavaMarker* dummy);
    AccessibleText(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual const char* getAfterIndex(int arg1, int arg2); // public abstract java.lang.String javax.accessibility.AccessibleText.getAfterIndex(int,int)
    virtual const char* getAtIndex(int arg1, int arg2); // public abstract java.lang.String javax.accessibility.AccessibleText.getAtIndex(int,int)
    virtual const char* getBeforeIndex(int arg1, int arg2); // public abstract java.lang.String javax.accessibility.AccessibleText.getBeforeIndex(int,int)
    virtual int getCaretPosition(); // public abstract int javax.accessibility.AccessibleText.getCaretPosition()
    virtual int getCharCount(); // public abstract int javax.accessibility.AccessibleText.getCharCount()
    virtual javax::swing::text::AttributeSet* getCharacterAttribute(int arg1); // public abstract javax.swing.text.AttributeSet javax.accessibility.AccessibleText.getCharacterAttribute(int)
    virtual java::awt::Rectangle* getCharacterBounds(int arg1); // public abstract java.awt.Rectangle javax.accessibility.AccessibleText.getCharacterBounds(int)
    virtual int getIndexAtPoint(java::awt::Point* arg1); // public abstract int javax.accessibility.AccessibleText.getIndexAtPoint(java.awt.Point)
    virtual const char* getSelectedText(); // public abstract java.lang.String javax.accessibility.AccessibleText.getSelectedText()
    virtual int getSelectionEnd(); // public abstract int javax.accessibility.AccessibleText.getSelectionEnd()
    virtual int getSelectionStart(); // public abstract int javax.accessibility.AccessibleText.getSelectionStart()

    int CHARACTER; // public static final int javax.accessibility.AccessibleText.CHARACTER
    int WORD; // public static final int javax.accessibility.AccessibleText.WORD
    int SENTENCE; // public static final int javax.accessibility.AccessibleText.SENTENCE
};
}
}
#endif
