#ifndef java_awt_event_MouseMotionListener_H
#define java_awt_event_MouseMotionListener_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace util {
class EventListener;
}
}

namespace java {
namespace awt {
namespace event {
class MouseEvent;
}
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace event {
class MouseMotionListener : public java::lang::Object {
  public:
    MouseMotionListener(JavaMarker* dummy);
    MouseMotionListener(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual void mouseDragged(java::awt::event::MouseEvent* arg1); // public abstract void java.awt.event.MouseMotionListener.mouseDragged(java.awt.event.MouseEvent)
    virtual void mouseMoved(java::awt::event::MouseEvent* arg1); // public abstract void java.awt.event.MouseMotionListener.mouseMoved(java.awt.event.MouseEvent)

};
}
}
}
#endif
