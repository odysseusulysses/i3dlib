#ifndef java_awt_Event_H
#define java_awt_Event_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace io {
class Serializable;
}
}

namespace java {
namespace lang {
class String;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
class Event : public java::lang::Object {
  public:
    Event(JavaMarker* dummy);
    Event(jobject obj);
    Event(java::lang::Object* arg1, long arg2, int arg3, int arg4, int arg5, int arg6, int arg7, java::lang::Object* arg8); // public java.awt.Event(java.lang.Object,long,int,int,int,int,int,java.lang.Object)
    Event(java::lang::Object* arg1, long arg2, int arg3, int arg4, int arg5, int arg6, int arg7); // public java.awt.Event(java.lang.Object,long,int,int,int,int,int)
    Event(java::lang::Object* arg1, int arg2, java::lang::Object* arg3); // public java.awt.Event(java.lang.Object,int,java.lang.Object)

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual const char* toString(); // public java.lang.String java.awt.Event.toString()
    virtual void translate(int arg1, int arg2); // public void java.awt.Event.translate(int,int)
    virtual bool controlDown(); // public boolean java.awt.Event.controlDown()
    virtual bool metaDown(); // public boolean java.awt.Event.metaDown()
    virtual bool shiftDown(); // public boolean java.awt.Event.shiftDown()

    int SHIFT_MASK; // public static final int java.awt.Event.SHIFT_MASK
    int CTRL_MASK; // public static final int java.awt.Event.CTRL_MASK
    int META_MASK; // public static final int java.awt.Event.META_MASK
    int ALT_MASK; // public static final int java.awt.Event.ALT_MASK
    int HOME; // public static final int java.awt.Event.HOME
    int END; // public static final int java.awt.Event.END
    int PGUP; // public static final int java.awt.Event.PGUP
    int PGDN; // public static final int java.awt.Event.PGDN
    int UP; // public static final int java.awt.Event.UP
    int DOWN; // public static final int java.awt.Event.DOWN
    int LEFT; // public static final int java.awt.Event.LEFT
    int RIGHT; // public static final int java.awt.Event.RIGHT
    int F1; // public static final int java.awt.Event.F1
    int F2; // public static final int java.awt.Event.F2
    int F3; // public static final int java.awt.Event.F3
    int F4; // public static final int java.awt.Event.F4
    int F5; // public static final int java.awt.Event.F5
    int F6; // public static final int java.awt.Event.F6
    int F7; // public static final int java.awt.Event.F7
    int F8; // public static final int java.awt.Event.F8
    int F9; // public static final int java.awt.Event.F9
    int F10; // public static final int java.awt.Event.F10
    int F11; // public static final int java.awt.Event.F11
    int F12; // public static final int java.awt.Event.F12
    int PRINT_SCREEN; // public static final int java.awt.Event.PRINT_SCREEN
    int SCROLL_LOCK; // public static final int java.awt.Event.SCROLL_LOCK
    int CAPS_LOCK; // public static final int java.awt.Event.CAPS_LOCK
    int NUM_LOCK; // public static final int java.awt.Event.NUM_LOCK
    int PAUSE; // public static final int java.awt.Event.PAUSE
    int INSERT; // public static final int java.awt.Event.INSERT
    int ENTER; // public static final int java.awt.Event.ENTER
    int BACK_SPACE; // public static final int java.awt.Event.BACK_SPACE
    int TAB; // public static final int java.awt.Event.TAB
    int ESCAPE; // public static final int java.awt.Event.ESCAPE
    int DELETE; // public static final int java.awt.Event.DELETE
    int WINDOW_DESTROY; // public static final int java.awt.Event.WINDOW_DESTROY
    int WINDOW_EXPOSE; // public static final int java.awt.Event.WINDOW_EXPOSE
    int WINDOW_ICONIFY; // public static final int java.awt.Event.WINDOW_ICONIFY
    int WINDOW_DEICONIFY; // public static final int java.awt.Event.WINDOW_DEICONIFY
    int WINDOW_MOVED; // public static final int java.awt.Event.WINDOW_MOVED
    int KEY_PRESS; // public static final int java.awt.Event.KEY_PRESS
    int KEY_RELEASE; // public static final int java.awt.Event.KEY_RELEASE
    int KEY_ACTION; // public static final int java.awt.Event.KEY_ACTION
    int KEY_ACTION_RELEASE; // public static final int java.awt.Event.KEY_ACTION_RELEASE
    int MOUSE_DOWN; // public static final int java.awt.Event.MOUSE_DOWN
    int MOUSE_UP; // public static final int java.awt.Event.MOUSE_UP
    int MOUSE_MOVE; // public static final int java.awt.Event.MOUSE_MOVE
    int MOUSE_ENTER; // public static final int java.awt.Event.MOUSE_ENTER
    int MOUSE_EXIT; // public static final int java.awt.Event.MOUSE_EXIT
    int MOUSE_DRAG; // public static final int java.awt.Event.MOUSE_DRAG
    int SCROLL_LINE_UP; // public static final int java.awt.Event.SCROLL_LINE_UP
    int SCROLL_LINE_DOWN; // public static final int java.awt.Event.SCROLL_LINE_DOWN
    int SCROLL_PAGE_UP; // public static final int java.awt.Event.SCROLL_PAGE_UP
    int SCROLL_PAGE_DOWN; // public static final int java.awt.Event.SCROLL_PAGE_DOWN
    int SCROLL_ABSOLUTE; // public static final int java.awt.Event.SCROLL_ABSOLUTE
    int SCROLL_BEGIN; // public static final int java.awt.Event.SCROLL_BEGIN
    int SCROLL_END; // public static final int java.awt.Event.SCROLL_END
    int LIST_SELECT; // public static final int java.awt.Event.LIST_SELECT
    int LIST_DESELECT; // public static final int java.awt.Event.LIST_DESELECT
    int ACTION_EVENT; // public static final int java.awt.Event.ACTION_EVENT
    int LOAD_FILE; // public static final int java.awt.Event.LOAD_FILE
    int SAVE_FILE; // public static final int java.awt.Event.SAVE_FILE
    int GOT_FOCUS; // public static final int java.awt.Event.GOT_FOCUS
    int LOST_FOCUS; // public static final int java.awt.Event.LOST_FOCUS
    java::lang::Object* target; // public java.lang.Object java.awt.Event.target
    long when; // public long java.awt.Event.when
    int id; // public int java.awt.Event.id
    int x; // public int java.awt.Event.x
    int y; // public int java.awt.Event.y
    int key; // public int java.awt.Event.key
    int modifiers; // public int java.awt.Event.modifiers
    int clickCount; // public int java.awt.Event.clickCount
    java::lang::Object* arg; // public java.lang.Object java.awt.Event.arg
    java::awt::Event* evt; // public java.awt.Event java.awt.Event.evt
};
}
}
#endif
