#ifndef java_awt_peer_ContainerPeer_H
#define java_awt_peer_ContainerPeer_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace awt {
namespace peer {
class ComponentPeer;
}
}
}

namespace java {
namespace awt {
class Insets;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace peer {
class ContainerPeer : public java::lang::Object {
  public:
    ContainerPeer(JavaMarker* dummy);
    ContainerPeer(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual void beginLayout(); // public abstract void java.awt.peer.ContainerPeer.beginLayout()
    virtual void beginValidate(); // public abstract void java.awt.peer.ContainerPeer.beginValidate()
    virtual void endLayout(); // public abstract void java.awt.peer.ContainerPeer.endLayout()
    virtual void endValidate(); // public abstract void java.awt.peer.ContainerPeer.endValidate()
    virtual java::awt::Insets* getInsets(); // public abstract java.awt.Insets java.awt.peer.ContainerPeer.getInsets()
    virtual java::awt::Insets* insets(); // public abstract java.awt.Insets java.awt.peer.ContainerPeer.insets()
    virtual bool isRestackSupported(); // public abstract boolean java.awt.peer.ContainerPeer.isRestackSupported()
    virtual void restack(); // public abstract void java.awt.peer.ContainerPeer.restack()
    virtual bool isPaintPending(); // public abstract boolean java.awt.peer.ContainerPeer.isPaintPending()

};
}
}
}
#endif
