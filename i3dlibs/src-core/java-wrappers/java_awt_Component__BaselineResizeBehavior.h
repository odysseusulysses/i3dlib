#ifndef java_awt_Component__BaselineResizeBehavior_H
#define java_awt_Component__BaselineResizeBehavior_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Enum.h>

namespace java {
namespace lang {
class String;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
class Component__BaselineResizeBehavior : public java::lang::Enum {
  public:
    Component__BaselineResizeBehavior(JavaMarker* dummy);
    Component__BaselineResizeBehavior(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual java::awt::Component__BaselineResizeBehavior* valueOf(const char* arg1); // public static java.awt.Component$BaselineResizeBehavior java.awt.Component$BaselineResizeBehavior.valueOf(java.lang.String)
    virtual JavaObjectArray* values(); // public static java.awt.Component$BaselineResizeBehavior[] java.awt.Component$BaselineResizeBehavior.values()

    java::awt::Component__BaselineResizeBehavior* CONSTANT_ASCENT; // public static final java.awt.Component$BaselineResizeBehavior java.awt.Component$BaselineResizeBehavior.CONSTANT_ASCENT
    java::awt::Component__BaselineResizeBehavior* CONSTANT_DESCENT; // public static final java.awt.Component$BaselineResizeBehavior java.awt.Component$BaselineResizeBehavior.CONSTANT_DESCENT
    java::awt::Component__BaselineResizeBehavior* CENTER_OFFSET; // public static final java.awt.Component$BaselineResizeBehavior java.awt.Component$BaselineResizeBehavior.CENTER_OFFSET
    java::awt::Component__BaselineResizeBehavior* OTHER; // public static final java.awt.Component$BaselineResizeBehavior java.awt.Component$BaselineResizeBehavior.OTHER
};
}
}
#endif
