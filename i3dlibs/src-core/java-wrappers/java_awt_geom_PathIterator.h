#ifndef java_awt_geom_PathIterator_H
#define java_awt_geom_PathIterator_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace geom {
class PathIterator : public java::lang::Object {
  public:
    PathIterator(JavaMarker* dummy);
    PathIterator(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual void next(); // public abstract void java.awt.geom.PathIterator.next()
    virtual int currentSegment(JavaFloatArray* arg1); // public abstract int java.awt.geom.PathIterator.currentSegment(float[])
    virtual int currentSegment(JavaDoubleArray* arg1); // public abstract int java.awt.geom.PathIterator.currentSegment(double[])
    virtual int getWindingRule(); // public abstract int java.awt.geom.PathIterator.getWindingRule()
    virtual bool isDone(); // public abstract boolean java.awt.geom.PathIterator.isDone()

    int WIND_EVEN_ODD; // public static final int java.awt.geom.PathIterator.WIND_EVEN_ODD
    int WIND_NON_ZERO; // public static final int java.awt.geom.PathIterator.WIND_NON_ZERO
    int SEG_MOVETO; // public static final int java.awt.geom.PathIterator.SEG_MOVETO
    int SEG_LINETO; // public static final int java.awt.geom.PathIterator.SEG_LINETO
    int SEG_QUADTO; // public static final int java.awt.geom.PathIterator.SEG_QUADTO
    int SEG_CUBICTO; // public static final int java.awt.geom.PathIterator.SEG_CUBICTO
    int SEG_CLOSE; // public static final int java.awt.geom.PathIterator.SEG_CLOSE
};
}
}
}
#endif
