#ifndef java_net_NetworkInterface_H
#define java_net_NetworkInterface_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace lang {
class String;
}
}

namespace java {
namespace net {
class InetAddress;
}
}

namespace java {
namespace util {
class Enumeration;
}
}

namespace java {
namespace util {
class List;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace net {
class NetworkInterface : public java::lang::Object {
  public:
    NetworkInterface(JavaMarker* dummy);
    NetworkInterface(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual int hashCode(); // public int java.net.NetworkInterface.hashCode()
    virtual bool equals(java::lang::Object* arg1); // public boolean java.net.NetworkInterface.equals(java.lang.Object)
    virtual const char* toString(); // public java.lang.String java.net.NetworkInterface.toString()
    virtual const char* getName(); // public java.lang.String java.net.NetworkInterface.getName()
    virtual java::net::NetworkInterface* getParent(); // public java.net.NetworkInterface java.net.NetworkInterface.getParent()
    virtual const char* getDisplayName(); // public java.lang.String java.net.NetworkInterface.getDisplayName()
    virtual java::net::NetworkInterface* getByName(const char* arg1); // public static java.net.NetworkInterface java.net.NetworkInterface.getByName(java.lang.String) throws java.net.SocketException
    virtual java::net::NetworkInterface* getByInetAddress(java::net::InetAddress* arg1); // public static java.net.NetworkInterface java.net.NetworkInterface.getByInetAddress(java.net.InetAddress) throws java.net.SocketException
    virtual JavaByteArray* getHardwareAddress(); // public byte[] java.net.NetworkInterface.getHardwareAddress() throws java.net.SocketException
    virtual java::util::Enumeration* getInetAddresses(); // public java.util.Enumeration java.net.NetworkInterface.getInetAddresses()
    virtual java::util::List* getInterfaceAddresses(); // public java.util.List java.net.NetworkInterface.getInterfaceAddresses()
    virtual int getMTU(); // public int java.net.NetworkInterface.getMTU() throws java.net.SocketException
    virtual java::util::Enumeration* getNetworkInterfaces(); // public static java.util.Enumeration java.net.NetworkInterface.getNetworkInterfaces() throws java.net.SocketException
    virtual java::util::Enumeration* getSubInterfaces(); // public java.util.Enumeration java.net.NetworkInterface.getSubInterfaces()
    virtual bool isLoopback(); // public boolean java.net.NetworkInterface.isLoopback() throws java.net.SocketException
    virtual bool isPointToPoint(); // public boolean java.net.NetworkInterface.isPointToPoint() throws java.net.SocketException
    virtual bool isUp(); // public boolean java.net.NetworkInterface.isUp() throws java.net.SocketException
    virtual bool isVirtual(); // public boolean java.net.NetworkInterface.isVirtual()
    virtual bool supportsMulticast(); // public boolean java.net.NetworkInterface.supportsMulticast() throws java.net.SocketException

};
}
}
#endif
