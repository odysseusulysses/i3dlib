#ifndef java_awt_RenderingHints_H
#define java_awt_RenderingHints_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace util {
class Map;
}
}

namespace java {
namespace lang {
class Cloneable;
}
}

namespace java {
namespace awt {
class RenderingHints__Key;
}
}

namespace java {
namespace lang {
class String;
}
}

namespace java {
namespace util {
class Set;
}
}

namespace java {
namespace util {
class Collection;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
class RenderingHints : public java::lang::Object {
  public:
    RenderingHints(JavaMarker* dummy);
    RenderingHints(jobject obj);
    RenderingHints(java::awt::RenderingHints__Key* arg1, java::lang::Object* arg2); // public java.awt.RenderingHints(java.awt.RenderingHints$Key,java.lang.Object)
    RenderingHints(java::util::Map* arg1); // public java.awt.RenderingHints(java.util.Map)

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual int hashCode(); // public int java.awt.RenderingHints.hashCode()
    virtual void add(java::awt::RenderingHints* arg1); // public void java.awt.RenderingHints.add(java.awt.RenderingHints)
    virtual java::lang::Object* put(java::lang::Object* arg1, java::lang::Object* arg2); // public java.lang.Object java.awt.RenderingHints.put(java.lang.Object,java.lang.Object)
    virtual java::lang::Object* clone(); // public java.lang.Object java.awt.RenderingHints.clone()
    virtual void clear(); // public void java.awt.RenderingHints.clear()
    virtual bool equals(java::lang::Object* arg1); // public boolean java.awt.RenderingHints.equals(java.lang.Object)
    virtual const char* toString(); // public java.lang.String java.awt.RenderingHints.toString()
    virtual bool isEmpty(); // public boolean java.awt.RenderingHints.isEmpty()
    virtual java::util::Set* entrySet(); // public java.util.Set java.awt.RenderingHints.entrySet()
    virtual java::lang::Object* get(java::lang::Object* arg1); // public java.lang.Object java.awt.RenderingHints.get(java.lang.Object)
    virtual void putAll(java::util::Map* arg1); // public void java.awt.RenderingHints.putAll(java.util.Map)
    virtual int size(); // public int java.awt.RenderingHints.size()
    virtual java::util::Collection* values(); // public java.util.Collection java.awt.RenderingHints.values()
    virtual java::lang::Object* remove(java::lang::Object* arg1); // public java.lang.Object java.awt.RenderingHints.remove(java.lang.Object)
    virtual java::util::Set* keySet(); // public java.util.Set java.awt.RenderingHints.keySet()
    virtual bool containsKey(java::lang::Object* arg1); // public boolean java.awt.RenderingHints.containsKey(java.lang.Object)
    virtual bool containsValue(java::lang::Object* arg1); // public boolean java.awt.RenderingHints.containsValue(java.lang.Object)

    java::awt::RenderingHints__Key* KEY_ANTIALIASING; // public static final java.awt.RenderingHints$Key java.awt.RenderingHints.KEY_ANTIALIASING
    java::lang::Object* VALUE_ANTIALIAS_ON; // public static final java.lang.Object java.awt.RenderingHints.VALUE_ANTIALIAS_ON
    java::lang::Object* VALUE_ANTIALIAS_OFF; // public static final java.lang.Object java.awt.RenderingHints.VALUE_ANTIALIAS_OFF
    java::lang::Object* VALUE_ANTIALIAS_DEFAULT; // public static final java.lang.Object java.awt.RenderingHints.VALUE_ANTIALIAS_DEFAULT
    java::awt::RenderingHints__Key* KEY_RENDERING; // public static final java.awt.RenderingHints$Key java.awt.RenderingHints.KEY_RENDERING
    java::lang::Object* VALUE_RENDER_SPEED; // public static final java.lang.Object java.awt.RenderingHints.VALUE_RENDER_SPEED
    java::lang::Object* VALUE_RENDER_QUALITY; // public static final java.lang.Object java.awt.RenderingHints.VALUE_RENDER_QUALITY
    java::lang::Object* VALUE_RENDER_DEFAULT; // public static final java.lang.Object java.awt.RenderingHints.VALUE_RENDER_DEFAULT
    java::awt::RenderingHints__Key* KEY_DITHERING; // public static final java.awt.RenderingHints$Key java.awt.RenderingHints.KEY_DITHERING
    java::lang::Object* VALUE_DITHER_DISABLE; // public static final java.lang.Object java.awt.RenderingHints.VALUE_DITHER_DISABLE
    java::lang::Object* VALUE_DITHER_ENABLE; // public static final java.lang.Object java.awt.RenderingHints.VALUE_DITHER_ENABLE
    java::lang::Object* VALUE_DITHER_DEFAULT; // public static final java.lang.Object java.awt.RenderingHints.VALUE_DITHER_DEFAULT
    java::awt::RenderingHints__Key* KEY_TEXT_ANTIALIASING; // public static final java.awt.RenderingHints$Key java.awt.RenderingHints.KEY_TEXT_ANTIALIASING
    java::lang::Object* VALUE_TEXT_ANTIALIAS_ON; // public static final java.lang.Object java.awt.RenderingHints.VALUE_TEXT_ANTIALIAS_ON
    java::lang::Object* VALUE_TEXT_ANTIALIAS_OFF; // public static final java.lang.Object java.awt.RenderingHints.VALUE_TEXT_ANTIALIAS_OFF
    java::lang::Object* VALUE_TEXT_ANTIALIAS_DEFAULT; // public static final java.lang.Object java.awt.RenderingHints.VALUE_TEXT_ANTIALIAS_DEFAULT
    java::lang::Object* VALUE_TEXT_ANTIALIAS_GASP; // public static final java.lang.Object java.awt.RenderingHints.VALUE_TEXT_ANTIALIAS_GASP
    java::lang::Object* VALUE_TEXT_ANTIALIAS_LCD_HRGB; // public static final java.lang.Object java.awt.RenderingHints.VALUE_TEXT_ANTIALIAS_LCD_HRGB
    java::lang::Object* VALUE_TEXT_ANTIALIAS_LCD_HBGR; // public static final java.lang.Object java.awt.RenderingHints.VALUE_TEXT_ANTIALIAS_LCD_HBGR
    java::lang::Object* VALUE_TEXT_ANTIALIAS_LCD_VRGB; // public static final java.lang.Object java.awt.RenderingHints.VALUE_TEXT_ANTIALIAS_LCD_VRGB
    java::lang::Object* VALUE_TEXT_ANTIALIAS_LCD_VBGR; // public static final java.lang.Object java.awt.RenderingHints.VALUE_TEXT_ANTIALIAS_LCD_VBGR
    java::awt::RenderingHints__Key* KEY_TEXT_LCD_CONTRAST; // public static final java.awt.RenderingHints$Key java.awt.RenderingHints.KEY_TEXT_LCD_CONTRAST
    java::awt::RenderingHints__Key* KEY_FRACTIONALMETRICS; // public static final java.awt.RenderingHints$Key java.awt.RenderingHints.KEY_FRACTIONALMETRICS
    java::lang::Object* VALUE_FRACTIONALMETRICS_OFF; // public static final java.lang.Object java.awt.RenderingHints.VALUE_FRACTIONALMETRICS_OFF
    java::lang::Object* VALUE_FRACTIONALMETRICS_ON; // public static final java.lang.Object java.awt.RenderingHints.VALUE_FRACTIONALMETRICS_ON
    java::lang::Object* VALUE_FRACTIONALMETRICS_DEFAULT; // public static final java.lang.Object java.awt.RenderingHints.VALUE_FRACTIONALMETRICS_DEFAULT
    java::awt::RenderingHints__Key* KEY_INTERPOLATION; // public static final java.awt.RenderingHints$Key java.awt.RenderingHints.KEY_INTERPOLATION
    java::lang::Object* VALUE_INTERPOLATION_NEAREST_NEIGHBOR; // public static final java.lang.Object java.awt.RenderingHints.VALUE_INTERPOLATION_NEAREST_NEIGHBOR
    java::lang::Object* VALUE_INTERPOLATION_BILINEAR; // public static final java.lang.Object java.awt.RenderingHints.VALUE_INTERPOLATION_BILINEAR
    java::lang::Object* VALUE_INTERPOLATION_BICUBIC; // public static final java.lang.Object java.awt.RenderingHints.VALUE_INTERPOLATION_BICUBIC
    java::awt::RenderingHints__Key* KEY_ALPHA_INTERPOLATION; // public static final java.awt.RenderingHints$Key java.awt.RenderingHints.KEY_ALPHA_INTERPOLATION
    java::lang::Object* VALUE_ALPHA_INTERPOLATION_SPEED; // public static final java.lang.Object java.awt.RenderingHints.VALUE_ALPHA_INTERPOLATION_SPEED
    java::lang::Object* VALUE_ALPHA_INTERPOLATION_QUALITY; // public static final java.lang.Object java.awt.RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY
    java::lang::Object* VALUE_ALPHA_INTERPOLATION_DEFAULT; // public static final java.lang.Object java.awt.RenderingHints.VALUE_ALPHA_INTERPOLATION_DEFAULT
    java::awt::RenderingHints__Key* KEY_COLOR_RENDERING; // public static final java.awt.RenderingHints$Key java.awt.RenderingHints.KEY_COLOR_RENDERING
    java::lang::Object* VALUE_COLOR_RENDER_SPEED; // public static final java.lang.Object java.awt.RenderingHints.VALUE_COLOR_RENDER_SPEED
    java::lang::Object* VALUE_COLOR_RENDER_QUALITY; // public static final java.lang.Object java.awt.RenderingHints.VALUE_COLOR_RENDER_QUALITY
    java::lang::Object* VALUE_COLOR_RENDER_DEFAULT; // public static final java.lang.Object java.awt.RenderingHints.VALUE_COLOR_RENDER_DEFAULT
    java::awt::RenderingHints__Key* KEY_STROKE_CONTROL; // public static final java.awt.RenderingHints$Key java.awt.RenderingHints.KEY_STROKE_CONTROL
    java::lang::Object* VALUE_STROKE_DEFAULT; // public static final java.lang.Object java.awt.RenderingHints.VALUE_STROKE_DEFAULT
    java::lang::Object* VALUE_STROKE_NORMALIZE; // public static final java.lang.Object java.awt.RenderingHints.VALUE_STROKE_NORMALIZE
    java::lang::Object* VALUE_STROKE_PURE; // public static final java.lang.Object java.awt.RenderingHints.VALUE_STROKE_PURE
};
}
}
#endif
