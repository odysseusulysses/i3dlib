#ifndef java_awt_AWTEvent_H
#define java_awt_AWTEvent_H
#include <jni.h>
#include <java_marker.h>
#include <java_util_EventObject.h>

namespace java {
namespace lang {
class Object;
}
}

namespace java {
namespace awt {
class Event;
}
}

namespace java {
namespace lang {
class String;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
class AWTEvent : public java::util::EventObject {
  public:
    AWTEvent(JavaMarker* dummy);
    AWTEvent(jobject obj);
    AWTEvent(java::lang::Object* arg1, int arg2); // public java.awt.AWTEvent(java.lang.Object,int)
    AWTEvent(java::awt::Event* arg1); // public java.awt.AWTEvent(java.awt.Event)

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual const char* toString(); // public java.lang.String java.awt.AWTEvent.toString()
    virtual int getID(); // public int java.awt.AWTEvent.getID()
    virtual const char* paramString(); // public java.lang.String java.awt.AWTEvent.paramString()
    virtual void setSource(java::lang::Object* arg1); // public void java.awt.AWTEvent.setSource(java.lang.Object)

    long COMPONENT_EVENT_MASK; // public static final long java.awt.AWTEvent.COMPONENT_EVENT_MASK
    long CONTAINER_EVENT_MASK; // public static final long java.awt.AWTEvent.CONTAINER_EVENT_MASK
    long FOCUS_EVENT_MASK; // public static final long java.awt.AWTEvent.FOCUS_EVENT_MASK
    long KEY_EVENT_MASK; // public static final long java.awt.AWTEvent.KEY_EVENT_MASK
    long MOUSE_EVENT_MASK; // public static final long java.awt.AWTEvent.MOUSE_EVENT_MASK
    long MOUSE_MOTION_EVENT_MASK; // public static final long java.awt.AWTEvent.MOUSE_MOTION_EVENT_MASK
    long WINDOW_EVENT_MASK; // public static final long java.awt.AWTEvent.WINDOW_EVENT_MASK
    long ACTION_EVENT_MASK; // public static final long java.awt.AWTEvent.ACTION_EVENT_MASK
    long ADJUSTMENT_EVENT_MASK; // public static final long java.awt.AWTEvent.ADJUSTMENT_EVENT_MASK
    long ITEM_EVENT_MASK; // public static final long java.awt.AWTEvent.ITEM_EVENT_MASK
    long TEXT_EVENT_MASK; // public static final long java.awt.AWTEvent.TEXT_EVENT_MASK
    long INPUT_METHOD_EVENT_MASK; // public static final long java.awt.AWTEvent.INPUT_METHOD_EVENT_MASK
    long PAINT_EVENT_MASK; // public static final long java.awt.AWTEvent.PAINT_EVENT_MASK
    long INVOCATION_EVENT_MASK; // public static final long java.awt.AWTEvent.INVOCATION_EVENT_MASK
    long HIERARCHY_EVENT_MASK; // public static final long java.awt.AWTEvent.HIERARCHY_EVENT_MASK
    long HIERARCHY_BOUNDS_EVENT_MASK; // public static final long java.awt.AWTEvent.HIERARCHY_BOUNDS_EVENT_MASK
    long MOUSE_WHEEL_EVENT_MASK; // public static final long java.awt.AWTEvent.MOUSE_WHEEL_EVENT_MASK
    long WINDOW_STATE_EVENT_MASK; // public static final long java.awt.AWTEvent.WINDOW_STATE_EVENT_MASK
    long WINDOW_FOCUS_EVENT_MASK; // public static final long java.awt.AWTEvent.WINDOW_FOCUS_EVENT_MASK
    int RESERVED_ID_MAX; // public static final int java.awt.AWTEvent.RESERVED_ID_MAX
};
}
}
#endif
