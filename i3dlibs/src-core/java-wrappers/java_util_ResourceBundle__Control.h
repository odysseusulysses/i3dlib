#ifndef java_util_ResourceBundle__Control_H
#define java_util_ResourceBundle__Control_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace util {
class List;
}
}

namespace java {
namespace lang {
class String;
}
}

namespace java {
namespace util {
class Locale;
}
}

namespace java {
namespace lang {
class ClassLoader;
}
}

namespace java {
namespace util {
class ResourceBundle;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace util {
class ResourceBundle__Control : public java::lang::Object {
  public:
    ResourceBundle__Control(JavaMarker* dummy);
    ResourceBundle__Control(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual java::util::List* getFormats(const char* arg1); // public java.util.List java.util.ResourceBundle$Control.getFormats(java.lang.String)
    virtual java::util::List* getCandidateLocales(const char* arg1, java::util::Locale* arg2); // public java.util.List java.util.ResourceBundle$Control.getCandidateLocales(java.lang.String,java.util.Locale)
    virtual java::util::Locale* getFallbackLocale(const char* arg1, java::util::Locale* arg2); // public java.util.Locale java.util.ResourceBundle$Control.getFallbackLocale(java.lang.String,java.util.Locale)
    virtual long getTimeToLive(const char* arg1, java::util::Locale* arg2); // public long java.util.ResourceBundle$Control.getTimeToLive(java.lang.String,java.util.Locale)
    virtual bool needsReload(const char* arg1, java::util::Locale* arg2, const char* arg3, java::lang::ClassLoader* arg4, java::util::ResourceBundle* arg5, long arg6); // public boolean java.util.ResourceBundle$Control.needsReload(java.lang.String,java.util.Locale,java.lang.String,java.lang.ClassLoader,java.util.ResourceBundle,long)
    virtual java::util::ResourceBundle* newBundle(const char* arg1, java::util::Locale* arg2, const char* arg3, java::lang::ClassLoader* arg4, bool arg5); // public java.util.ResourceBundle java.util.ResourceBundle$Control.newBundle(java.lang.String,java.util.Locale,java.lang.String,java.lang.ClassLoader,boolean) throws java.lang.IllegalAccessException,java.lang.InstantiationException,java.io.IOException
    virtual java::util::ResourceBundle__Control* getControl(java::util::List* arg1); // public static final java.util.ResourceBundle$Control java.util.ResourceBundle$Control.getControl(java.util.List)
    virtual java::util::ResourceBundle__Control* getNoFallbackControl(java::util::List* arg1); // public static final java.util.ResourceBundle$Control java.util.ResourceBundle$Control.getNoFallbackControl(java.util.List)
    virtual const char* toBundleName(const char* arg1, java::util::Locale* arg2); // public java.lang.String java.util.ResourceBundle$Control.toBundleName(java.lang.String,java.util.Locale)
    virtual const char* toResourceName(const char* arg1, const char* arg2); // public final java.lang.String java.util.ResourceBundle$Control.toResourceName(java.lang.String,java.lang.String)

    java::util::List* FORMAT_DEFAULT; // public static final java.util.List java.util.ResourceBundle$Control.FORMAT_DEFAULT
    java::util::List* FORMAT_CLASS; // public static final java.util.List java.util.ResourceBundle$Control.FORMAT_CLASS
    java::util::List* FORMAT_PROPERTIES; // public static final java.util.List java.util.ResourceBundle$Control.FORMAT_PROPERTIES
    long TTL_DONT_CACHE; // public static final long java.util.ResourceBundle$Control.TTL_DONT_CACHE
    long TTL_NO_EXPIRATION_CONTROL; // public static final long java.util.ResourceBundle$Control.TTL_NO_EXPIRATION_CONTROL
};
}
}
#endif
