#ifndef java_io_Externalizable_H
#define java_io_Externalizable_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace io {
class Serializable;
}
}

namespace java {
namespace io {
class ObjectOutput;
}
}

namespace java {
namespace io {
class ObjectInput;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace io {
class Externalizable : public java::lang::Object {
  public:
    Externalizable(JavaMarker* dummy);
    Externalizable(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual void writeExternal(java::io::ObjectOutput* arg1); // public abstract void java.io.Externalizable.writeExternal(java.io.ObjectOutput) throws java.io.IOException
    virtual void readExternal(java::io::ObjectInput* arg1); // public abstract void java.io.Externalizable.readExternal(java.io.ObjectInput) throws java.io.IOException,java.lang.ClassNotFoundException

};
}
}
#endif
