#ifndef java_io_Console_H
#define java_io_Console_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace io {
class Flushable;
}
}

namespace java {
namespace lang {
class String;
}
}

namespace java {
namespace io {
class Reader;
}
}

namespace java {
namespace io {
class PrintWriter;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace io {
class Console : public java::lang::Object {
  public:
    Console(JavaMarker* dummy);
    Console(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual java::io::Console* format(const char* arg1, JavaObjectArray* arg2); // public java.io.Console java.io.Console.format(java.lang.String,java.lang.Object[])
    virtual void flush(); // public void java.io.Console.flush()
    virtual const char* readLine(const char* arg1, JavaObjectArray* arg2); // public java.lang.String java.io.Console.readLine(java.lang.String,java.lang.Object[])
    virtual const char* readLine(); // public java.lang.String java.io.Console.readLine()
    virtual java::io::Console* printf(const char* arg1, JavaObjectArray* arg2); // public java.io.Console java.io.Console.printf(java.lang.String,java.lang.Object[])
    virtual JavaCharArray* readPassword(); // public char[] java.io.Console.readPassword()
    virtual JavaCharArray* readPassword(const char* arg1, JavaObjectArray* arg2); // public char[] java.io.Console.readPassword(java.lang.String,java.lang.Object[])
    virtual java::io::Reader* reader(); // public java.io.Reader java.io.Console.reader()
    virtual java::io::PrintWriter* writer(); // public java.io.PrintWriter java.io.Console.writer()

};
}
}
#endif
