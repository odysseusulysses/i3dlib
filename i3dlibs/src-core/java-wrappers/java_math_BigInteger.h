#ifndef java_math_BigInteger_H
#define java_math_BigInteger_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Number.h>

namespace java {
namespace lang {
class Comparable;
}
}

namespace java {
namespace lang {
class String;
}
}

namespace java {
namespace util {
class Random;
}
}

namespace java {
namespace lang {
class Object;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace math {
class BigInteger : public java::lang::Number {
  public:
    BigInteger(JavaMarker* dummy);
    BigInteger(jobject obj);
    BigInteger(const char* arg1, int arg2); // public java.math.BigInteger(java.lang.String,int)
    BigInteger(int arg1, JavaByteArray* arg2); // public java.math.BigInteger(int,byte[])
    BigInteger(int arg1, java::util::Random* arg2); // public java.math.BigInteger(int,java.util.Random)
    BigInteger(JavaByteArray* arg1); // public java.math.BigInteger(byte[])
    BigInteger(int arg1, int arg2, java::util::Random* arg3); // public java.math.BigInteger(int,int,java.util.Random)
    BigInteger(const char* arg1); // public java.math.BigInteger(java.lang.String)

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual java::math::BigInteger* abs(); // public java.math.BigInteger java.math.BigInteger.abs()
    virtual java::math::BigInteger* pow(int arg1); // public java.math.BigInteger java.math.BigInteger.pow(int)
    virtual int hashCode(); // public int java.math.BigInteger.hashCode()
    virtual int compareTo(java::lang::Object* arg1); // public int java.math.BigInteger.compareTo(java.lang.Object)
    virtual int compareTo(java::math::BigInteger* arg1); // public int java.math.BigInteger.compareTo(java.math.BigInteger)
    virtual java::math::BigInteger* add(java::math::BigInteger* arg1); // public java.math.BigInteger java.math.BigInteger.add(java.math.BigInteger)
    virtual bool equals(java::lang::Object* arg1); // public boolean java.math.BigInteger.equals(java.lang.Object)
    virtual const char* toString(int arg1); // public java.lang.String java.math.BigInteger.toString(int)
    virtual const char* toString(); // public java.lang.String java.math.BigInteger.toString()
    virtual java::math::BigInteger* min(java::math::BigInteger* arg1); // public java.math.BigInteger java.math.BigInteger.min(java.math.BigInteger)
    virtual java::math::BigInteger* valueOf(long arg1); // public static java.math.BigInteger java.math.BigInteger.valueOf(long)
    virtual java::math::BigInteger* max(java::math::BigInteger* arg1); // public java.math.BigInteger java.math.BigInteger.max(java.math.BigInteger)
    virtual double doubleValue(); // public double java.math.BigInteger.doubleValue()
    virtual float floatValue(); // public float java.math.BigInteger.floatValue()
    virtual int intValue(); // public int java.math.BigInteger.intValue()
    virtual long longValue(); // public long java.math.BigInteger.longValue()
    virtual int bitCount(); // public int java.math.BigInteger.bitCount()
    virtual int signum(); // public int java.math.BigInteger.signum()
    virtual JavaByteArray* toByteArray(); // public byte[] java.math.BigInteger.toByteArray()
    //virtual java::math::BigInteger* and(java::math::BigInteger* arg1); // public java.math.BigInteger java.math.BigInteger.and(java.math.BigInteger)
    virtual java::math::BigInteger* andNot(java::math::BigInteger* arg1); // public java.math.BigInteger java.math.BigInteger.andNot(java.math.BigInteger)
    //virtual java::math::BigInteger* or(java::math::BigInteger* arg1); // public java.math.BigInteger java.math.BigInteger.or(java.math.BigInteger)
    //virtual java::math::BigInteger* xor(java::math::BigInteger* arg1); // public java.math.BigInteger java.math.BigInteger.xor(java.math.BigInteger)
    virtual bool testBit(int arg1); // public boolean java.math.BigInteger.testBit(int)
    virtual int bitLength(); // public int java.math.BigInteger.bitLength()
    virtual java::math::BigInteger* clearBit(int arg1); // public java.math.BigInteger java.math.BigInteger.clearBit(int)
    virtual java::math::BigInteger* divide(java::math::BigInteger* arg1); // public java.math.BigInteger java.math.BigInteger.divide(java.math.BigInteger)
    virtual JavaObjectArray* divideAndRemainder(java::math::BigInteger* arg1); // public java.math.BigInteger[] java.math.BigInteger.divideAndRemainder(java.math.BigInteger)
    virtual java::math::BigInteger* flipBit(int arg1); // public java.math.BigInteger java.math.BigInteger.flipBit(int)
    virtual java::math::BigInteger* gcd(java::math::BigInteger* arg1); // public java.math.BigInteger java.math.BigInteger.gcd(java.math.BigInteger)
    virtual int getLowestSetBit(); // public int java.math.BigInteger.getLowestSetBit()
    virtual bool isProbablePrime(int arg1); // public boolean java.math.BigInteger.isProbablePrime(int)
    virtual java::math::BigInteger* mod(java::math::BigInteger* arg1); // public java.math.BigInteger java.math.BigInteger.mod(java.math.BigInteger)
    virtual java::math::BigInteger* modInverse(java::math::BigInteger* arg1); // public java.math.BigInteger java.math.BigInteger.modInverse(java.math.BigInteger)
    virtual java::math::BigInteger* modPow(java::math::BigInteger* arg1, java::math::BigInteger* arg2); // public java.math.BigInteger java.math.BigInteger.modPow(java.math.BigInteger,java.math.BigInteger)
    virtual java::math::BigInteger* multiply(java::math::BigInteger* arg1); // public java.math.BigInteger java.math.BigInteger.multiply(java.math.BigInteger)
    virtual java::math::BigInteger* negate(); // public java.math.BigInteger java.math.BigInteger.negate()
    virtual java::math::BigInteger* nextProbablePrime(); // public java.math.BigInteger java.math.BigInteger.nextProbablePrime()
    //virtual java::math::BigInteger* not(); // public java.math.BigInteger java.math.BigInteger.not()
    virtual java::math::BigInteger* probablePrime(int arg1, java::util::Random* arg2); // public static java.math.BigInteger java.math.BigInteger.probablePrime(int,java.util.Random)
    virtual java::math::BigInteger* remainder(java::math::BigInteger* arg1); // public java.math.BigInteger java.math.BigInteger.remainder(java.math.BigInteger)
    virtual java::math::BigInteger* setBit(int arg1); // public java.math.BigInteger java.math.BigInteger.setBit(int)
    virtual java::math::BigInteger* shiftLeft(int arg1); // public java.math.BigInteger java.math.BigInteger.shiftLeft(int)
    virtual java::math::BigInteger* shiftRight(int arg1); // public java.math.BigInteger java.math.BigInteger.shiftRight(int)
    virtual java::math::BigInteger* subtract(java::math::BigInteger* arg1); // public java.math.BigInteger java.math.BigInteger.subtract(java.math.BigInteger)

    java::math::BigInteger* ZERO; // public static final java.math.BigInteger java.math.BigInteger.ZERO
    java::math::BigInteger* ONE; // public static final java.math.BigInteger java.math.BigInteger.ONE
    java::math::BigInteger* TEN; // public static final java.math.BigInteger java.math.BigInteger.TEN
};
}
}
#endif
