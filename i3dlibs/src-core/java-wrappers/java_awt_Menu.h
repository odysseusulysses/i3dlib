#ifndef java_awt_Menu_H
#define java_awt_Menu_H
#include <jni.h>
#include <java_marker.h>
#include <java_awt_MenuItem.h>

namespace java {
namespace awt {
class MenuContainer;
}
}

namespace javax {
namespace accessibility {
class Accessible;
}
}

namespace java {
namespace lang {
class String;
}
}

namespace java {
namespace awt {
class MenuComponent;
}
}

namespace javax {
namespace accessibility {
class AccessibleContext;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
class Menu : public java::awt::MenuItem {
  public:
    Menu(JavaMarker* dummy);
    Menu(jobject obj);
    Menu(const char* arg1); // public java.awt.Menu(java.lang.String) throws java.awt.HeadlessException
    Menu(const char* arg1, bool arg2); // public java.awt.Menu(java.lang.String,boolean) throws java.awt.HeadlessException
    Menu(); // public java.awt.Menu() throws java.awt.HeadlessException

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual void add(const char* arg1); // public void java.awt.Menu.add(java.lang.String)
    virtual java::awt::MenuItem* add(java::awt::MenuItem* arg1); // public java.awt.MenuItem java.awt.Menu.add(java.awt.MenuItem)
    virtual void remove(int arg1); // public void java.awt.Menu.remove(int)
    virtual void remove(java::awt::MenuComponent* arg1); // public void java.awt.Menu.remove(java.awt.MenuComponent)
    virtual void removeAll(); // public void java.awt.Menu.removeAll()
    virtual void insert(const char* arg1, int arg2); // public void java.awt.Menu.insert(java.lang.String,int)
    virtual void insert(java::awt::MenuItem* arg1, int arg2); // public void java.awt.Menu.insert(java.awt.MenuItem,int)
    virtual void addNotify(); // public void java.awt.Menu.addNotify()
    virtual javax::accessibility::AccessibleContext* getAccessibleContext(); // public javax.accessibility.AccessibleContext java.awt.Menu.getAccessibleContext()
    virtual void removeNotify(); // public void java.awt.Menu.removeNotify()
    virtual const char* paramString(); // public java.lang.String java.awt.Menu.paramString()
    virtual java::awt::MenuItem* getItem(int arg1); // public java.awt.MenuItem java.awt.Menu.getItem(int)
    virtual int getItemCount(); // public int java.awt.Menu.getItemCount()
    virtual void addSeparator(); // public void java.awt.Menu.addSeparator()
    virtual int countItems(); // public int java.awt.Menu.countItems()
    virtual void insertSeparator(int arg1); // public void java.awt.Menu.insertSeparator(int)
    virtual bool isTearOff(); // public boolean java.awt.Menu.isTearOff()

};
}
}
#endif
