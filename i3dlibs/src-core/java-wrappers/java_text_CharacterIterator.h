#ifndef java_text_CharacterIterator_H
#define java_text_CharacterIterator_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace lang {
class Cloneable;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace text {
class CharacterIterator : public java::lang::Object {
  public:
    CharacterIterator(JavaMarker* dummy);
    CharacterIterator(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual java::lang::Object* clone(); // public abstract java.lang.Object java.text.CharacterIterator.clone()
    virtual char next(); // public abstract char java.text.CharacterIterator.next()
    virtual char previous(); // public abstract char java.text.CharacterIterator.previous()
    virtual int getIndex(); // public abstract int java.text.CharacterIterator.getIndex()
    virtual char first(); // public abstract char java.text.CharacterIterator.first()
    virtual char last(); // public abstract char java.text.CharacterIterator.last()
    virtual char current(); // public abstract char java.text.CharacterIterator.current()
    virtual int getBeginIndex(); // public abstract int java.text.CharacterIterator.getBeginIndex()
    virtual int getEndIndex(); // public abstract int java.text.CharacterIterator.getEndIndex()
    virtual char setIndex(int arg1); // public abstract char java.text.CharacterIterator.setIndex(int)

    char DONE; // public static final char java.text.CharacterIterator.DONE
};
}
}
#endif
