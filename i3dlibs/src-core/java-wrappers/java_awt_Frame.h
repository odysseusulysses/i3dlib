#ifndef java_awt_Frame_H
#define java_awt_Frame_H
#include <jni.h>
#include <java_marker.h>
#include <java_awt_Window.h>

namespace java {
namespace awt {
class MenuContainer;
}
}

namespace java {
namespace awt {
class GraphicsConfiguration;
}
}

namespace java {
namespace lang {
class String;
}
}

namespace java {
namespace awt {
class MenuComponent;
}
}

namespace javax {
namespace accessibility {
class AccessibleContext;
}
}

namespace java {
namespace awt {
class Image;
}
}

namespace java {
namespace awt {
class Rectangle;
}
}

namespace java {
namespace awt {
class MenuBar;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
class Frame : public java::awt::Window {
  public:
    Frame(JavaMarker* dummy);
    Frame(jobject obj);
    Frame(java::awt::GraphicsConfiguration* arg1); // public java.awt.Frame(java.awt.GraphicsConfiguration)
    Frame(const char* arg1); // public java.awt.Frame(java.lang.String) throws java.awt.HeadlessException
    Frame(const char* arg1, java::awt::GraphicsConfiguration* arg2); // public java.awt.Frame(java.lang.String,java.awt.GraphicsConfiguration)
    Frame(); // public java.awt.Frame() throws java.awt.HeadlessException

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual void remove(java::awt::MenuComponent* arg1); // public void java.awt.Frame.remove(java.awt.MenuComponent)
    virtual int getState(); // public synchronized int java.awt.Frame.getState()
    virtual void setState(int arg1); // public synchronized void java.awt.Frame.setState(int)
    virtual void addNotify(); // public void java.awt.Frame.addNotify()
    virtual javax::accessibility::AccessibleContext* getAccessibleContext(); // public javax.accessibility.AccessibleContext java.awt.Frame.getAccessibleContext()
    virtual void removeNotify(); // public void java.awt.Frame.removeNotify()
    virtual void setCursor(int arg1); // public void java.awt.Frame.setCursor(int)
    virtual void setIconImage(java::awt::Image* arg1); // public void java.awt.Frame.setIconImage(java.awt.Image)
    virtual int getCursorType(); // public int java.awt.Frame.getCursorType()
    virtual int getExtendedState(); // public synchronized int java.awt.Frame.getExtendedState()
    virtual JavaObjectArray* getFrames(); // public static java.awt.Frame[] java.awt.Frame.getFrames()
    virtual java::awt::Image* getIconImage(); // public java.awt.Image java.awt.Frame.getIconImage()
    virtual java::awt::Rectangle* getMaximizedBounds(); // public java.awt.Rectangle java.awt.Frame.getMaximizedBounds()
    virtual java::awt::MenuBar* getMenuBar(); // public java.awt.MenuBar java.awt.Frame.getMenuBar()
    virtual const char* getTitle(); // public java.lang.String java.awt.Frame.getTitle()
    virtual bool isResizable(); // public boolean java.awt.Frame.isResizable()
    virtual bool isUndecorated(); // public boolean java.awt.Frame.isUndecorated()
    virtual void setExtendedState(int arg1); // public synchronized void java.awt.Frame.setExtendedState(int)
    virtual void setMaximizedBounds(java::awt::Rectangle* arg1); // public synchronized void java.awt.Frame.setMaximizedBounds(java.awt.Rectangle)
    virtual void setMenuBar(java::awt::MenuBar* arg1); // public void java.awt.Frame.setMenuBar(java.awt.MenuBar)
    virtual void setResizable(bool arg1); // public void java.awt.Frame.setResizable(boolean)
    virtual void setTitle(const char* arg1); // public void java.awt.Frame.setTitle(java.lang.String)
    virtual void setUndecorated(bool arg1); // public void java.awt.Frame.setUndecorated(boolean)

    int DEFAULT_CURSOR; // public static final int java.awt.Frame.DEFAULT_CURSOR
    int CROSSHAIR_CURSOR; // public static final int java.awt.Frame.CROSSHAIR_CURSOR
    int TEXT_CURSOR; // public static final int java.awt.Frame.TEXT_CURSOR
    int WAIT_CURSOR; // public static final int java.awt.Frame.WAIT_CURSOR
    int SW_RESIZE_CURSOR; // public static final int java.awt.Frame.SW_RESIZE_CURSOR
    int SE_RESIZE_CURSOR; // public static final int java.awt.Frame.SE_RESIZE_CURSOR
    int NW_RESIZE_CURSOR; // public static final int java.awt.Frame.NW_RESIZE_CURSOR
    int NE_RESIZE_CURSOR; // public static final int java.awt.Frame.NE_RESIZE_CURSOR
    int N_RESIZE_CURSOR; // public static final int java.awt.Frame.N_RESIZE_CURSOR
    int S_RESIZE_CURSOR; // public static final int java.awt.Frame.S_RESIZE_CURSOR
    int W_RESIZE_CURSOR; // public static final int java.awt.Frame.W_RESIZE_CURSOR
    int E_RESIZE_CURSOR; // public static final int java.awt.Frame.E_RESIZE_CURSOR
    int HAND_CURSOR; // public static final int java.awt.Frame.HAND_CURSOR
    int MOVE_CURSOR; // public static final int java.awt.Frame.MOVE_CURSOR
    int NORMAL; // public static final int java.awt.Frame.NORMAL
    int ICONIFIED; // public static final int java.awt.Frame.ICONIFIED
    int MAXIMIZED_HORIZ; // public static final int java.awt.Frame.MAXIMIZED_HORIZ
    int MAXIMIZED_VERT; // public static final int java.awt.Frame.MAXIMIZED_VERT
    int MAXIMIZED_BOTH; // public static final int java.awt.Frame.MAXIMIZED_BOTH
};
}
}
#endif
