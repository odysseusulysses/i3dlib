#ifndef java_awt_event_KeyEvent_H
#define java_awt_event_KeyEvent_H
#include <jni.h>
#include <java_marker.h>
#include <java_awt_event_InputEvent.h>

namespace java {
namespace awt {
class Component;
}
}

namespace java {
namespace lang {
class String;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace event {
class KeyEvent : public java::awt::event::InputEvent {
  public:
    KeyEvent(JavaMarker* dummy);
    KeyEvent(jobject obj);
    KeyEvent(java::awt::Component* arg1, int arg2, long arg3, int arg4, int arg5, char arg6, int arg7); // public java.awt.event.KeyEvent(java.awt.Component,int,long,int,int,char,int)
    KeyEvent(java::awt::Component* arg1, int arg2, long arg3, int arg4, int arg5, char arg6); // public java.awt.event.KeyEvent(java.awt.Component,int,long,int,int,char)
    KeyEvent(java::awt::Component* arg1, int arg2, long arg3, int arg4, int arg5); // public java.awt.event.KeyEvent(java.awt.Component,int,long,int,int)

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual int getKeyCode(); // public int java.awt.event.KeyEvent.getKeyCode()
    virtual bool isActionKey(); // public boolean java.awt.event.KeyEvent.isActionKey()
    virtual const char* paramString(); // public java.lang.String java.awt.event.KeyEvent.paramString()
    virtual char getKeyChar(); // public char java.awt.event.KeyEvent.getKeyChar()
    virtual void setKeyChar(char arg1); // public void java.awt.event.KeyEvent.setKeyChar(char)
    virtual void setModifiers(int arg1); // public void java.awt.event.KeyEvent.setModifiers(int)
    virtual int getKeyLocation(); // public int java.awt.event.KeyEvent.getKeyLocation()
    virtual const char* getKeyModifiersText(int arg1); // public static java.lang.String java.awt.event.KeyEvent.getKeyModifiersText(int)
    virtual const char* getKeyText(int arg1); // public static java.lang.String java.awt.event.KeyEvent.getKeyText(int)
    virtual void setKeyCode(int arg1); // public void java.awt.event.KeyEvent.setKeyCode(int)

    int KEY_FIRST; // public static final int java.awt.event.KeyEvent.KEY_FIRST
    int KEY_LAST; // public static final int java.awt.event.KeyEvent.KEY_LAST
    int KEY_TYPED; // public static final int java.awt.event.KeyEvent.KEY_TYPED
    int KEY_PRESSED; // public static final int java.awt.event.KeyEvent.KEY_PRESSED
    int KEY_RELEASED; // public static final int java.awt.event.KeyEvent.KEY_RELEASED
    int VK_ENTER; // public static final int java.awt.event.KeyEvent.VK_ENTER
    int VK_BACK_SPACE; // public static final int java.awt.event.KeyEvent.VK_BACK_SPACE
    int VK_TAB; // public static final int java.awt.event.KeyEvent.VK_TAB
    int VK_CANCEL; // public static final int java.awt.event.KeyEvent.VK_CANCEL
    int VK_CLEAR; // public static final int java.awt.event.KeyEvent.VK_CLEAR
    int VK_SHIFT; // public static final int java.awt.event.KeyEvent.VK_SHIFT
    int VK_CONTROL; // public static final int java.awt.event.KeyEvent.VK_CONTROL
    int VK_ALT; // public static final int java.awt.event.KeyEvent.VK_ALT
    int VK_PAUSE; // public static final int java.awt.event.KeyEvent.VK_PAUSE
    int VK_CAPS_LOCK; // public static final int java.awt.event.KeyEvent.VK_CAPS_LOCK
    int VK_ESCAPE; // public static final int java.awt.event.KeyEvent.VK_ESCAPE
    int VK_SPACE; // public static final int java.awt.event.KeyEvent.VK_SPACE
    int VK_PAGE_UP; // public static final int java.awt.event.KeyEvent.VK_PAGE_UP
    int VK_PAGE_DOWN; // public static final int java.awt.event.KeyEvent.VK_PAGE_DOWN
    int VK_END; // public static final int java.awt.event.KeyEvent.VK_END
    int VK_HOME; // public static final int java.awt.event.KeyEvent.VK_HOME
    int VK_LEFT; // public static final int java.awt.event.KeyEvent.VK_LEFT
    int VK_UP; // public static final int java.awt.event.KeyEvent.VK_UP
    int VK_RIGHT; // public static final int java.awt.event.KeyEvent.VK_RIGHT
    int VK_DOWN; // public static final int java.awt.event.KeyEvent.VK_DOWN
    int VK_COMMA; // public static final int java.awt.event.KeyEvent.VK_COMMA
    int VK_MINUS; // public static final int java.awt.event.KeyEvent.VK_MINUS
    int VK_PERIOD; // public static final int java.awt.event.KeyEvent.VK_PERIOD
    int VK_SLASH; // public static final int java.awt.event.KeyEvent.VK_SLASH
    int VK_0; // public static final int java.awt.event.KeyEvent.VK_0
    int VK_1; // public static final int java.awt.event.KeyEvent.VK_1
    int VK_2; // public static final int java.awt.event.KeyEvent.VK_2
    int VK_3; // public static final int java.awt.event.KeyEvent.VK_3
    int VK_4; // public static final int java.awt.event.KeyEvent.VK_4
    int VK_5; // public static final int java.awt.event.KeyEvent.VK_5
    int VK_6; // public static final int java.awt.event.KeyEvent.VK_6
    int VK_7; // public static final int java.awt.event.KeyEvent.VK_7
    int VK_8; // public static final int java.awt.event.KeyEvent.VK_8
    int VK_9; // public static final int java.awt.event.KeyEvent.VK_9
    int VK_SEMICOLON; // public static final int java.awt.event.KeyEvent.VK_SEMICOLON
    int VK_EQUALS; // public static final int java.awt.event.KeyEvent.VK_EQUALS
    int VK_A; // public static final int java.awt.event.KeyEvent.VK_A
    int VK_B; // public static final int java.awt.event.KeyEvent.VK_B
    int VK_C; // public static final int java.awt.event.KeyEvent.VK_C
    int VK_D; // public static final int java.awt.event.KeyEvent.VK_D
    int VK_E; // public static final int java.awt.event.KeyEvent.VK_E
    int VK_F; // public static final int java.awt.event.KeyEvent.VK_F
    int VK_G; // public static final int java.awt.event.KeyEvent.VK_G
    int VK_H; // public static final int java.awt.event.KeyEvent.VK_H
    int VK_I; // public static final int java.awt.event.KeyEvent.VK_I
    int VK_J; // public static final int java.awt.event.KeyEvent.VK_J
    int VK_K; // public static final int java.awt.event.KeyEvent.VK_K
    int VK_L; // public static final int java.awt.event.KeyEvent.VK_L
    int VK_M; // public static final int java.awt.event.KeyEvent.VK_M
    int VK_N; // public static final int java.awt.event.KeyEvent.VK_N
    int VK_O; // public static final int java.awt.event.KeyEvent.VK_O
    int VK_P; // public static final int java.awt.event.KeyEvent.VK_P
    int VK_Q; // public static final int java.awt.event.KeyEvent.VK_Q
    int VK_R; // public static final int java.awt.event.KeyEvent.VK_R
    int VK_S; // public static final int java.awt.event.KeyEvent.VK_S
    int VK_T; // public static final int java.awt.event.KeyEvent.VK_T
    int VK_U; // public static final int java.awt.event.KeyEvent.VK_U
    int VK_V; // public static final int java.awt.event.KeyEvent.VK_V
    int VK_W; // public static final int java.awt.event.KeyEvent.VK_W
    int VK_X; // public static final int java.awt.event.KeyEvent.VK_X
    int VK_Y; // public static final int java.awt.event.KeyEvent.VK_Y
    int VK_Z; // public static final int java.awt.event.KeyEvent.VK_Z
    int VK_OPEN_BRACKET; // public static final int java.awt.event.KeyEvent.VK_OPEN_BRACKET
    int VK_BACK_SLASH; // public static final int java.awt.event.KeyEvent.VK_BACK_SLASH
    int VK_CLOSE_BRACKET; // public static final int java.awt.event.KeyEvent.VK_CLOSE_BRACKET
    int VK_NUMPAD0; // public static final int java.awt.event.KeyEvent.VK_NUMPAD0
    int VK_NUMPAD1; // public static final int java.awt.event.KeyEvent.VK_NUMPAD1
    int VK_NUMPAD2; // public static final int java.awt.event.KeyEvent.VK_NUMPAD2
    int VK_NUMPAD3; // public static final int java.awt.event.KeyEvent.VK_NUMPAD3
    int VK_NUMPAD4; // public static final int java.awt.event.KeyEvent.VK_NUMPAD4
    int VK_NUMPAD5; // public static final int java.awt.event.KeyEvent.VK_NUMPAD5
    int VK_NUMPAD6; // public static final int java.awt.event.KeyEvent.VK_NUMPAD6
    int VK_NUMPAD7; // public static final int java.awt.event.KeyEvent.VK_NUMPAD7
    int VK_NUMPAD8; // public static final int java.awt.event.KeyEvent.VK_NUMPAD8
    int VK_NUMPAD9; // public static final int java.awt.event.KeyEvent.VK_NUMPAD9
    int VK_MULTIPLY; // public static final int java.awt.event.KeyEvent.VK_MULTIPLY
    int VK_ADD; // public static final int java.awt.event.KeyEvent.VK_ADD
    int VK_SEPARATER; // public static final int java.awt.event.KeyEvent.VK_SEPARATER
    int VK_SEPARATOR; // public static final int java.awt.event.KeyEvent.VK_SEPARATOR
    int VK_SUBTRACT; // public static final int java.awt.event.KeyEvent.VK_SUBTRACT
    int VK_DECIMAL; // public static final int java.awt.event.KeyEvent.VK_DECIMAL
    int VK_DIVIDE; // public static final int java.awt.event.KeyEvent.VK_DIVIDE
    int VK_DELETE; // public static final int java.awt.event.KeyEvent.VK_DELETE
    int VK_NUM_LOCK; // public static final int java.awt.event.KeyEvent.VK_NUM_LOCK
    int VK_SCROLL_LOCK; // public static final int java.awt.event.KeyEvent.VK_SCROLL_LOCK
    int VK_F1; // public static final int java.awt.event.KeyEvent.VK_F1
    int VK_F2; // public static final int java.awt.event.KeyEvent.VK_F2
    int VK_F3; // public static final int java.awt.event.KeyEvent.VK_F3
    int VK_F4; // public static final int java.awt.event.KeyEvent.VK_F4
    int VK_F5; // public static final int java.awt.event.KeyEvent.VK_F5
    int VK_F6; // public static final int java.awt.event.KeyEvent.VK_F6
    int VK_F7; // public static final int java.awt.event.KeyEvent.VK_F7
    int VK_F8; // public static final int java.awt.event.KeyEvent.VK_F8
    int VK_F9; // public static final int java.awt.event.KeyEvent.VK_F9
    int VK_F10; // public static final int java.awt.event.KeyEvent.VK_F10
    int VK_F11; // public static final int java.awt.event.KeyEvent.VK_F11
    int VK_F12; // public static final int java.awt.event.KeyEvent.VK_F12
    int VK_F13; // public static final int java.awt.event.KeyEvent.VK_F13
    int VK_F14; // public static final int java.awt.event.KeyEvent.VK_F14
    int VK_F15; // public static final int java.awt.event.KeyEvent.VK_F15
    int VK_F16; // public static final int java.awt.event.KeyEvent.VK_F16
    int VK_F17; // public static final int java.awt.event.KeyEvent.VK_F17
    int VK_F18; // public static final int java.awt.event.KeyEvent.VK_F18
    int VK_F19; // public static final int java.awt.event.KeyEvent.VK_F19
    int VK_F20; // public static final int java.awt.event.KeyEvent.VK_F20
    int VK_F21; // public static final int java.awt.event.KeyEvent.VK_F21
    int VK_F22; // public static final int java.awt.event.KeyEvent.VK_F22
    int VK_F23; // public static final int java.awt.event.KeyEvent.VK_F23
    int VK_F24; // public static final int java.awt.event.KeyEvent.VK_F24
    int VK_PRINTSCREEN; // public static final int java.awt.event.KeyEvent.VK_PRINTSCREEN
    int VK_INSERT; // public static final int java.awt.event.KeyEvent.VK_INSERT
    int VK_HELP; // public static final int java.awt.event.KeyEvent.VK_HELP
    int VK_META; // public static final int java.awt.event.KeyEvent.VK_META
    int VK_BACK_QUOTE; // public static final int java.awt.event.KeyEvent.VK_BACK_QUOTE
    int VK_QUOTE; // public static final int java.awt.event.KeyEvent.VK_QUOTE
    int VK_KP_UP; // public static final int java.awt.event.KeyEvent.VK_KP_UP
    int VK_KP_DOWN; // public static final int java.awt.event.KeyEvent.VK_KP_DOWN
    int VK_KP_LEFT; // public static final int java.awt.event.KeyEvent.VK_KP_LEFT
    int VK_KP_RIGHT; // public static final int java.awt.event.KeyEvent.VK_KP_RIGHT
    int VK_DEAD_GRAVE; // public static final int java.awt.event.KeyEvent.VK_DEAD_GRAVE
    int VK_DEAD_ACUTE; // public static final int java.awt.event.KeyEvent.VK_DEAD_ACUTE
    int VK_DEAD_CIRCUMFLEX; // public static final int java.awt.event.KeyEvent.VK_DEAD_CIRCUMFLEX
    int VK_DEAD_TILDE; // public static final int java.awt.event.KeyEvent.VK_DEAD_TILDE
    int VK_DEAD_MACRON; // public static final int java.awt.event.KeyEvent.VK_DEAD_MACRON
    int VK_DEAD_BREVE; // public static final int java.awt.event.KeyEvent.VK_DEAD_BREVE
    int VK_DEAD_ABOVEDOT; // public static final int java.awt.event.KeyEvent.VK_DEAD_ABOVEDOT
    int VK_DEAD_DIAERESIS; // public static final int java.awt.event.KeyEvent.VK_DEAD_DIAERESIS
    int VK_DEAD_ABOVERING; // public static final int java.awt.event.KeyEvent.VK_DEAD_ABOVERING
    int VK_DEAD_DOUBLEACUTE; // public static final int java.awt.event.KeyEvent.VK_DEAD_DOUBLEACUTE
    int VK_DEAD_CARON; // public static final int java.awt.event.KeyEvent.VK_DEAD_CARON
    int VK_DEAD_CEDILLA; // public static final int java.awt.event.KeyEvent.VK_DEAD_CEDILLA
    int VK_DEAD_OGONEK; // public static final int java.awt.event.KeyEvent.VK_DEAD_OGONEK
    int VK_DEAD_IOTA; // public static final int java.awt.event.KeyEvent.VK_DEAD_IOTA
    int VK_DEAD_VOICED_SOUND; // public static final int java.awt.event.KeyEvent.VK_DEAD_VOICED_SOUND
    int VK_DEAD_SEMIVOICED_SOUND; // public static final int java.awt.event.KeyEvent.VK_DEAD_SEMIVOICED_SOUND
    int VK_AMPERSAND; // public static final int java.awt.event.KeyEvent.VK_AMPERSAND
    int VK_ASTERISK; // public static final int java.awt.event.KeyEvent.VK_ASTERISK
    int VK_QUOTEDBL; // public static final int java.awt.event.KeyEvent.VK_QUOTEDBL
    int VK_LESS; // public static final int java.awt.event.KeyEvent.VK_LESS
    int VK_GREATER; // public static final int java.awt.event.KeyEvent.VK_GREATER
    int VK_BRACELEFT; // public static final int java.awt.event.KeyEvent.VK_BRACELEFT
    int VK_BRACERIGHT; // public static final int java.awt.event.KeyEvent.VK_BRACERIGHT
    int VK_AT; // public static final int java.awt.event.KeyEvent.VK_AT
    int VK_COLON; // public static final int java.awt.event.KeyEvent.VK_COLON
    int VK_CIRCUMFLEX; // public static final int java.awt.event.KeyEvent.VK_CIRCUMFLEX
    int VK_DOLLAR; // public static final int java.awt.event.KeyEvent.VK_DOLLAR
    int VK_EURO_SIGN; // public static final int java.awt.event.KeyEvent.VK_EURO_SIGN
    int VK_EXCLAMATION_MARK; // public static final int java.awt.event.KeyEvent.VK_EXCLAMATION_MARK
    int VK_INVERTED_EXCLAMATION_MARK; // public static final int java.awt.event.KeyEvent.VK_INVERTED_EXCLAMATION_MARK
    int VK_LEFT_PARENTHESIS; // public static final int java.awt.event.KeyEvent.VK_LEFT_PARENTHESIS
    int VK_NUMBER_SIGN; // public static final int java.awt.event.KeyEvent.VK_NUMBER_SIGN
    int VK_PLUS; // public static final int java.awt.event.KeyEvent.VK_PLUS
    int VK_RIGHT_PARENTHESIS; // public static final int java.awt.event.KeyEvent.VK_RIGHT_PARENTHESIS
    int VK_UNDERSCORE; // public static final int java.awt.event.KeyEvent.VK_UNDERSCORE
    int VK_WINDOWS; // public static final int java.awt.event.KeyEvent.VK_WINDOWS
    int VK_CONTEXT_MENU; // public static final int java.awt.event.KeyEvent.VK_CONTEXT_MENU
    int VK_FINAL; // public static final int java.awt.event.KeyEvent.VK_FINAL
    int VK_CONVERT; // public static final int java.awt.event.KeyEvent.VK_CONVERT
    int VK_NONCONVERT; // public static final int java.awt.event.KeyEvent.VK_NONCONVERT
    int VK_ACCEPT; // public static final int java.awt.event.KeyEvent.VK_ACCEPT
    int VK_MODECHANGE; // public static final int java.awt.event.KeyEvent.VK_MODECHANGE
    int VK_KANA; // public static final int java.awt.event.KeyEvent.VK_KANA
    int VK_KANJI; // public static final int java.awt.event.KeyEvent.VK_KANJI
    int VK_ALPHANUMERIC; // public static final int java.awt.event.KeyEvent.VK_ALPHANUMERIC
    int VK_KATAKANA; // public static final int java.awt.event.KeyEvent.VK_KATAKANA
    int VK_HIRAGANA; // public static final int java.awt.event.KeyEvent.VK_HIRAGANA
    int VK_FULL_WIDTH; // public static final int java.awt.event.KeyEvent.VK_FULL_WIDTH
    int VK_HALF_WIDTH; // public static final int java.awt.event.KeyEvent.VK_HALF_WIDTH
    int VK_ROMAN_CHARACTERS; // public static final int java.awt.event.KeyEvent.VK_ROMAN_CHARACTERS
    int VK_ALL_CANDIDATES; // public static final int java.awt.event.KeyEvent.VK_ALL_CANDIDATES
    int VK_PREVIOUS_CANDIDATE; // public static final int java.awt.event.KeyEvent.VK_PREVIOUS_CANDIDATE
    int VK_CODE_INPUT; // public static final int java.awt.event.KeyEvent.VK_CODE_INPUT
    int VK_JAPANESE_KATAKANA; // public static final int java.awt.event.KeyEvent.VK_JAPANESE_KATAKANA
    int VK_JAPANESE_HIRAGANA; // public static final int java.awt.event.KeyEvent.VK_JAPANESE_HIRAGANA
    int VK_JAPANESE_ROMAN; // public static final int java.awt.event.KeyEvent.VK_JAPANESE_ROMAN
    int VK_KANA_LOCK; // public static final int java.awt.event.KeyEvent.VK_KANA_LOCK
    int VK_INPUT_METHOD_ON_OFF; // public static final int java.awt.event.KeyEvent.VK_INPUT_METHOD_ON_OFF
    int VK_CUT; // public static final int java.awt.event.KeyEvent.VK_CUT
    int VK_COPY; // public static final int java.awt.event.KeyEvent.VK_COPY
    int VK_PASTE; // public static final int java.awt.event.KeyEvent.VK_PASTE
    int VK_UNDO; // public static final int java.awt.event.KeyEvent.VK_UNDO
    int VK_AGAIN; // public static final int java.awt.event.KeyEvent.VK_AGAIN
    int VK_FIND; // public static final int java.awt.event.KeyEvent.VK_FIND
    int VK_PROPS; // public static final int java.awt.event.KeyEvent.VK_PROPS
    int VK_STOP; // public static final int java.awt.event.KeyEvent.VK_STOP
    int VK_COMPOSE; // public static final int java.awt.event.KeyEvent.VK_COMPOSE
    int VK_ALT_GRAPH; // public static final int java.awt.event.KeyEvent.VK_ALT_GRAPH
    int VK_BEGIN; // public static final int java.awt.event.KeyEvent.VK_BEGIN
    int VK_UNDEFINED; // public static final int java.awt.event.KeyEvent.VK_UNDEFINED
    char CHAR_UNDEFINED; // public static final char java.awt.event.KeyEvent.CHAR_UNDEFINED
    int KEY_LOCATION_UNKNOWN; // public static final int java.awt.event.KeyEvent.KEY_LOCATION_UNKNOWN
    int KEY_LOCATION_STANDARD; // public static final int java.awt.event.KeyEvent.KEY_LOCATION_STANDARD
    int KEY_LOCATION_LEFT; // public static final int java.awt.event.KeyEvent.KEY_LOCATION_LEFT
    int KEY_LOCATION_RIGHT; // public static final int java.awt.event.KeyEvent.KEY_LOCATION_RIGHT
    int KEY_LOCATION_NUMPAD; // public static final int java.awt.event.KeyEvent.KEY_LOCATION_NUMPAD
};
}
}
}
#endif
