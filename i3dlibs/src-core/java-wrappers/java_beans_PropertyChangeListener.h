#ifndef java_beans_PropertyChangeListener_H
#define java_beans_PropertyChangeListener_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace util {
class EventListener;
}
}

namespace java {
namespace beans {
class PropertyChangeEvent;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace beans {
class PropertyChangeListener : public java::lang::Object {
  public:
    PropertyChangeListener(JavaMarker* dummy);
    PropertyChangeListener(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual void propertyChange(java::beans::PropertyChangeEvent* arg1); // public abstract void java.beans.PropertyChangeListener.propertyChange(java.beans.PropertyChangeEvent)

};
}
}
#endif
