#ifndef java_util_Vector_H
#define java_util_Vector_H
#include <jni.h>
#include <java_marker.h>
#include <java_util_AbstractList.h>

namespace java {
namespace util {
class List;
}
}

namespace java {
namespace util {
class RandomAccess;
}
}

namespace java {
namespace lang {
class Cloneable;
}
}

namespace java {
namespace io {
class Serializable;
}
}

namespace java {
namespace util {
class Collection;
}
}

namespace java {
namespace lang {
class Object;
}
}

namespace java {
namespace lang {
class String;
}
}

namespace java {
namespace util {
class Enumeration;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace util {
class Vector : public java::util::AbstractList {
  public:
    Vector(JavaMarker* dummy);
    Vector(jobject obj);
    Vector(java::util::Collection* arg1); // public java.util.Vector(java.util.Collection)
    Vector(); // public java.util.Vector()
    Vector(int arg1); // public java.util.Vector(int)
    Vector(int arg1, int arg2); // public java.util.Vector(int,int)

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual int hashCode(); // public synchronized int java.util.Vector.hashCode()
    virtual java::lang::Object* elementAt(int arg1); // public synchronized java.lang.Object java.util.Vector.elementAt(int)
    virtual bool add(java::lang::Object* arg1); // public synchronized boolean java.util.Vector.add(java.lang.Object)
    virtual void add(int arg1, java::lang::Object* arg2); // public void java.util.Vector.add(int,java.lang.Object)
    virtual int indexOf(java::lang::Object* arg1, int arg2); // public synchronized int java.util.Vector.indexOf(java.lang.Object,int)
    virtual int indexOf(java::lang::Object* arg1); // public int java.util.Vector.indexOf(java.lang.Object)
    virtual java::lang::Object* clone(); // public synchronized java.lang.Object java.util.Vector.clone()
    virtual void clear(); // public void java.util.Vector.clear()
    virtual bool equals(java::lang::Object* arg1); // public synchronized boolean java.util.Vector.equals(java.lang.Object)
    virtual const char* toString(); // public synchronized java.lang.String java.util.Vector.toString()
    virtual bool contains(java::lang::Object* arg1); // public boolean java.util.Vector.contains(java.lang.Object)
    virtual bool isEmpty(); // public synchronized boolean java.util.Vector.isEmpty()
    virtual int lastIndexOf(java::lang::Object* arg1); // public synchronized int java.util.Vector.lastIndexOf(java.lang.Object)
    virtual int lastIndexOf(java::lang::Object* arg1, int arg2); // public synchronized int java.util.Vector.lastIndexOf(java.lang.Object,int)
    virtual bool addAll(java::util::Collection* arg1); // public synchronized boolean java.util.Vector.addAll(java.util.Collection)
    virtual bool addAll(int arg1, java::util::Collection* arg2); // public synchronized boolean java.util.Vector.addAll(int,java.util.Collection)
    virtual java::lang::Object* get(int arg1); // public synchronized java.lang.Object java.util.Vector.get(int)
    virtual int size(); // public synchronized int java.util.Vector.size()
    virtual JavaObjectArray* toArray(); // public synchronized java.lang.Object[] java.util.Vector.toArray()
    virtual JavaObjectArray* toArray(JavaObjectArray* arg1); // public synchronized java.lang.Object[] java.util.Vector.toArray(java.lang.Object[])
    virtual void addElement(java::lang::Object* arg1); // public synchronized void java.util.Vector.addElement(java.lang.Object)
    virtual bool remove(java::lang::Object* arg1); // public boolean java.util.Vector.remove(java.lang.Object)
    virtual java::lang::Object* remove(int arg1); // public synchronized java.lang.Object java.util.Vector.remove(int)
    virtual java::util::Enumeration* elements(); // public java.util.Enumeration java.util.Vector.elements()
    virtual java::lang::Object* set(int arg1, java::lang::Object* arg2); // public synchronized java.lang.Object java.util.Vector.set(int,java.lang.Object)
    virtual int capacity(); // public synchronized int java.util.Vector.capacity()
    virtual bool containsAll(java::util::Collection* arg1); // public synchronized boolean java.util.Vector.containsAll(java.util.Collection)
    virtual void copyInto(JavaObjectArray* arg1); // public synchronized void java.util.Vector.copyInto(java.lang.Object[])
    virtual void ensureCapacity(int arg1); // public synchronized void java.util.Vector.ensureCapacity(int)
    virtual java::lang::Object* firstElement(); // public synchronized java.lang.Object java.util.Vector.firstElement()
    virtual void insertElementAt(java::lang::Object* arg1, int arg2); // public synchronized void java.util.Vector.insertElementAt(java.lang.Object,int)
    virtual java::lang::Object* lastElement(); // public synchronized java.lang.Object java.util.Vector.lastElement()
    virtual bool removeAll(java::util::Collection* arg1); // public synchronized boolean java.util.Vector.removeAll(java.util.Collection)
    virtual void removeAllElements(); // public synchronized void java.util.Vector.removeAllElements()
    virtual bool removeElement(java::lang::Object* arg1); // public synchronized boolean java.util.Vector.removeElement(java.lang.Object)
    virtual void removeElementAt(int arg1); // public synchronized void java.util.Vector.removeElementAt(int)
    virtual bool retainAll(java::util::Collection* arg1); // public synchronized boolean java.util.Vector.retainAll(java.util.Collection)
    virtual void setElementAt(java::lang::Object* arg1, int arg2); // public synchronized void java.util.Vector.setElementAt(java.lang.Object,int)
    virtual void setSize(int arg1); // public synchronized void java.util.Vector.setSize(int)
    virtual java::util::List* subList(int arg1, int arg2); // public synchronized java.util.List java.util.Vector.subList(int,int)
    virtual void trimToSize(); // public synchronized void java.util.Vector.trimToSize()

};
}
}
#endif
