#ifndef java_awt_MenuBar_H
#define java_awt_MenuBar_H
#include <jni.h>
#include <java_marker.h>
#include <java_awt_MenuComponent.h>

namespace java {
namespace awt {
class MenuContainer;
}
}

namespace javax {
namespace accessibility {
class Accessible;
}
}

namespace java {
namespace awt {
class Menu;
}
}

namespace javax {
namespace accessibility {
class AccessibleContext;
}
}

namespace java {
namespace awt {
class MenuShortcut;
}
}

namespace java {
namespace awt {
class MenuItem;
}
}

namespace java {
namespace util {
class Enumeration;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
class MenuBar : public java::awt::MenuComponent {
  public:
    MenuBar(JavaMarker* dummy);
    MenuBar(jobject obj);
    MenuBar(); // public java.awt.MenuBar() throws java.awt.HeadlessException

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual java::awt::Menu* add(java::awt::Menu* arg1); // public java.awt.Menu java.awt.MenuBar.add(java.awt.Menu)
    virtual void remove(int arg1); // public void java.awt.MenuBar.remove(int)
    virtual void remove(java::awt::MenuComponent* arg1); // public void java.awt.MenuBar.remove(java.awt.MenuComponent)
    virtual void addNotify(); // public void java.awt.MenuBar.addNotify()
    virtual javax::accessibility::AccessibleContext* getAccessibleContext(); // public javax.accessibility.AccessibleContext java.awt.MenuBar.getAccessibleContext()
    virtual void removeNotify(); // public void java.awt.MenuBar.removeNotify()
    virtual int countMenus(); // public int java.awt.MenuBar.countMenus()
    virtual void deleteShortcut(java::awt::MenuShortcut* arg1); // public void java.awt.MenuBar.deleteShortcut(java.awt.MenuShortcut)
    virtual java::awt::Menu* getHelpMenu(); // public java.awt.Menu java.awt.MenuBar.getHelpMenu()
    virtual java::awt::Menu* getMenu(int arg1); // public java.awt.Menu java.awt.MenuBar.getMenu(int)
    virtual int getMenuCount(); // public int java.awt.MenuBar.getMenuCount()
    virtual java::awt::MenuItem* getShortcutMenuItem(java::awt::MenuShortcut* arg1); // public java.awt.MenuItem java.awt.MenuBar.getShortcutMenuItem(java.awt.MenuShortcut)
    virtual void setHelpMenu(java::awt::Menu* arg1); // public void java.awt.MenuBar.setHelpMenu(java.awt.Menu)
    virtual java::util::Enumeration* shortcuts(); // public synchronized java.util.Enumeration java.awt.MenuBar.shortcuts()

};
}
}
#endif
