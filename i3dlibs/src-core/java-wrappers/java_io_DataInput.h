#ifndef java_io_DataInput_H
#define java_io_DataInput_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace lang {
class String;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace io {
class DataInput : public java::lang::Object {
  public:
    DataInput(JavaMarker* dummy);
    DataInput(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual const char* readLine(); // public abstract java.lang.String java.io.DataInput.readLine() throws java.io.IOException
    virtual int readInt(); // public abstract int java.io.DataInput.readInt() throws java.io.IOException
    virtual char readChar(); // public abstract char java.io.DataInput.readChar() throws java.io.IOException
    virtual char readByte(); // public abstract byte java.io.DataInput.readByte() throws java.io.IOException
    virtual long readLong(); // public abstract long java.io.DataInput.readLong() throws java.io.IOException
    virtual short readShort(); // public abstract short java.io.DataInput.readShort() throws java.io.IOException
    virtual const char* readUTF(); // public abstract java.lang.String java.io.DataInput.readUTF() throws java.io.IOException
    virtual float readFloat(); // public abstract float java.io.DataInput.readFloat() throws java.io.IOException
    virtual void readFully(JavaByteArray* arg1, int arg2, int arg3); // public abstract void java.io.DataInput.readFully(byte[],int,int) throws java.io.IOException
    virtual void readFully(JavaByteArray* arg1); // public abstract void java.io.DataInput.readFully(byte[]) throws java.io.IOException
    virtual bool readBoolean(); // public abstract boolean java.io.DataInput.readBoolean() throws java.io.IOException
    virtual double readDouble(); // public abstract double java.io.DataInput.readDouble() throws java.io.IOException
    virtual int readUnsignedByte(); // public abstract int java.io.DataInput.readUnsignedByte() throws java.io.IOException
    virtual int readUnsignedShort(); // public abstract int java.io.DataInput.readUnsignedShort() throws java.io.IOException
    virtual int skipBytes(int arg1); // public abstract int java.io.DataInput.skipBytes(int) throws java.io.IOException

};
}
}
#endif
