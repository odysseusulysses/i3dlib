#ifndef java_awt_Graphics_H
#define java_awt_Graphics_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace lang {
class String;
}
}

namespace java {
namespace awt {
class Image;
}
}

namespace java {
namespace awt {
namespace image {
class ImageObserver;
}
}
}

namespace java {
namespace awt {
class Color;
}
}

namespace java {
namespace text {
class AttributedCharacterIterator;
}
}

namespace java {
namespace awt {
class Polygon;
}
}

namespace java {
namespace awt {
class Shape;
}
}

namespace java {
namespace awt {
class Rectangle;
}
}

namespace java {
namespace awt {
class Font;
}
}

namespace java {
namespace awt {
class FontMetrics;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
class Graphics : public java::lang::Object {
  public:
    Graphics(JavaMarker* dummy);
    Graphics(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual void finalize(); // public void java.awt.Graphics.finalize()
    virtual const char* toString(); // public java.lang.String java.awt.Graphics.toString()
    virtual java::awt::Graphics* create(int arg1, int arg2, int arg3, int arg4); // public java.awt.Graphics java.awt.Graphics.create(int,int,int,int)
    virtual java::awt::Graphics* create(); // public abstract java.awt.Graphics java.awt.Graphics.create()
    virtual void translate(int arg1, int arg2); // public abstract void java.awt.Graphics.translate(int,int)
    virtual void draw3DRect(int arg1, int arg2, int arg3, int arg4, bool arg5); // public void java.awt.Graphics.draw3DRect(int,int,int,int,boolean)
    virtual bool drawImage(java::awt::Image* arg1, int arg2, int arg3, int arg4, int arg5, int arg6, int arg7, int arg8, int arg9, java::awt::image::ImageObserver* arg10); // public abstract boolean java.awt.Graphics.drawImage(java.awt.Image,int,int,int,int,int,int,int,int,java.awt.image.ImageObserver)
    virtual bool drawImage(java::awt::Image* arg1, int arg2, int arg3, int arg4, int arg5, int arg6, int arg7, int arg8, int arg9, java::awt::Color* arg10, java::awt::image::ImageObserver* arg11); // public abstract boolean java.awt.Graphics.drawImage(java.awt.Image,int,int,int,int,int,int,int,int,java.awt.Color,java.awt.image.ImageObserver)
    virtual bool drawImage(java::awt::Image* arg1, int arg2, int arg3, java::awt::Color* arg4, java::awt::image::ImageObserver* arg5); // public abstract boolean java.awt.Graphics.drawImage(java.awt.Image,int,int,java.awt.Color,java.awt.image.ImageObserver)
    virtual bool drawImage(java::awt::Image* arg1, int arg2, int arg3, int arg4, int arg5, java::awt::image::ImageObserver* arg6); // public abstract boolean java.awt.Graphics.drawImage(java.awt.Image,int,int,int,int,java.awt.image.ImageObserver)
    virtual bool drawImage(java::awt::Image* arg1, int arg2, int arg3, java::awt::image::ImageObserver* arg4); // public abstract boolean java.awt.Graphics.drawImage(java.awt.Image,int,int,java.awt.image.ImageObserver)
    virtual bool drawImage(java::awt::Image* arg1, int arg2, int arg3, int arg4, int arg5, java::awt::Color* arg6, java::awt::image::ImageObserver* arg7); // public abstract boolean java.awt.Graphics.drawImage(java.awt.Image,int,int,int,int,java.awt.Color,java.awt.image.ImageObserver)
    virtual void drawString(const char* arg1, int arg2, int arg3); // public abstract void java.awt.Graphics.drawString(java.lang.String,int,int)
    virtual void drawString(java::text::AttributedCharacterIterator* arg1, int arg2, int arg3); // public abstract void java.awt.Graphics.drawString(java.text.AttributedCharacterIterator,int,int)
    virtual void fill3DRect(int arg1, int arg2, int arg3, int arg4, bool arg5); // public void java.awt.Graphics.fill3DRect(int,int,int,int,boolean)
    virtual void fillRect(int arg1, int arg2, int arg3, int arg4); // public abstract void java.awt.Graphics.fillRect(int,int,int,int)
    virtual java::awt::Color* getColor(); // public abstract java.awt.Color java.awt.Graphics.getColor()
    virtual void setColor(java::awt::Color* arg1); // public abstract void java.awt.Graphics.setColor(java.awt.Color)
    virtual void clearRect(int arg1, int arg2, int arg3, int arg4); // public abstract void java.awt.Graphics.clearRect(int,int,int,int)
    virtual void clipRect(int arg1, int arg2, int arg3, int arg4); // public abstract void java.awt.Graphics.clipRect(int,int,int,int)
    virtual void copyArea(int arg1, int arg2, int arg3, int arg4, int arg5, int arg6); // public abstract void java.awt.Graphics.copyArea(int,int,int,int,int,int)
    virtual void dispose(); // public abstract void java.awt.Graphics.dispose()
    virtual void drawArc(int arg1, int arg2, int arg3, int arg4, int arg5, int arg6); // public abstract void java.awt.Graphics.drawArc(int,int,int,int,int,int)
    virtual void drawBytes(JavaByteArray* arg1, int arg2, int arg3, int arg4, int arg5); // public void java.awt.Graphics.drawBytes(byte[],int,int,int,int)
    virtual void drawChars(JavaCharArray* arg1, int arg2, int arg3, int arg4, int arg5); // public void java.awt.Graphics.drawChars(char[],int,int,int,int)
    virtual void drawLine(int arg1, int arg2, int arg3, int arg4); // public abstract void java.awt.Graphics.drawLine(int,int,int,int)
    virtual void drawOval(int arg1, int arg2, int arg3, int arg4); // public abstract void java.awt.Graphics.drawOval(int,int,int,int)
    virtual void drawPolygon(JavaIntArray* arg1, JavaIntArray* arg2, int arg3); // public abstract void java.awt.Graphics.drawPolygon(int[],int[],int)
    virtual void drawPolygon(java::awt::Polygon* arg1); // public void java.awt.Graphics.drawPolygon(java.awt.Polygon)
    virtual void drawPolyline(JavaIntArray* arg1, JavaIntArray* arg2, int arg3); // public abstract void java.awt.Graphics.drawPolyline(int[],int[],int)
    virtual void drawRect(int arg1, int arg2, int arg3, int arg4); // public void java.awt.Graphics.drawRect(int,int,int,int)
    virtual void drawRoundRect(int arg1, int arg2, int arg3, int arg4, int arg5, int arg6); // public abstract void java.awt.Graphics.drawRoundRect(int,int,int,int,int,int)
    virtual void fillArc(int arg1, int arg2, int arg3, int arg4, int arg5, int arg6); // public abstract void java.awt.Graphics.fillArc(int,int,int,int,int,int)
    virtual void fillOval(int arg1, int arg2, int arg3, int arg4); // public abstract void java.awt.Graphics.fillOval(int,int,int,int)
    virtual void fillPolygon(java::awt::Polygon* arg1); // public void java.awt.Graphics.fillPolygon(java.awt.Polygon)
    virtual void fillPolygon(JavaIntArray* arg1, JavaIntArray* arg2, int arg3); // public abstract void java.awt.Graphics.fillPolygon(int[],int[],int)
    virtual void fillRoundRect(int arg1, int arg2, int arg3, int arg4, int arg5, int arg6); // public abstract void java.awt.Graphics.fillRoundRect(int,int,int,int,int,int)
    virtual java::awt::Shape* getClip(); // public abstract java.awt.Shape java.awt.Graphics.getClip()
    virtual java::awt::Rectangle* getClipBounds(java::awt::Rectangle* arg1); // public java.awt.Rectangle java.awt.Graphics.getClipBounds(java.awt.Rectangle)
    virtual java::awt::Rectangle* getClipBounds(); // public abstract java.awt.Rectangle java.awt.Graphics.getClipBounds()
    virtual java::awt::Rectangle* getClipRect(); // public java.awt.Rectangle java.awt.Graphics.getClipRect()
    virtual java::awt::Font* getFont(); // public abstract java.awt.Font java.awt.Graphics.getFont()
    virtual java::awt::FontMetrics* getFontMetrics(); // public java.awt.FontMetrics java.awt.Graphics.getFontMetrics()
    virtual java::awt::FontMetrics* getFontMetrics(java::awt::Font* arg1); // public abstract java.awt.FontMetrics java.awt.Graphics.getFontMetrics(java.awt.Font)
    virtual bool hitClip(int arg1, int arg2, int arg3, int arg4); // public boolean java.awt.Graphics.hitClip(int,int,int,int)
    virtual void setClip(java::awt::Shape* arg1); // public abstract void java.awt.Graphics.setClip(java.awt.Shape)
    virtual void setClip(int arg1, int arg2, int arg3, int arg4); // public abstract void java.awt.Graphics.setClip(int,int,int,int)
    virtual void setFont(java::awt::Font* arg1); // public abstract void java.awt.Graphics.setFont(java.awt.Font)
    virtual void setPaintMode(); // public abstract void java.awt.Graphics.setPaintMode()
    virtual void setXORMode(java::awt::Color* arg1); // public abstract void java.awt.Graphics.setXORMode(java.awt.Color)

};
}
}
#endif
