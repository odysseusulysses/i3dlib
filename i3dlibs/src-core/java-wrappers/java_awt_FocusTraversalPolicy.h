#ifndef java_awt_FocusTraversalPolicy_H
#define java_awt_FocusTraversalPolicy_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace awt {
class Component;
}
}

namespace java {
namespace awt {
class Container;
}
}

namespace java {
namespace awt {
class Window;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
class FocusTraversalPolicy : public java::lang::Object {
  public:
    FocusTraversalPolicy(JavaMarker* dummy);
    FocusTraversalPolicy(jobject obj);
    FocusTraversalPolicy(); // public java.awt.FocusTraversalPolicy()

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual java::awt::Component* getDefaultComponent(java::awt::Container* arg1); // public abstract java.awt.Component java.awt.FocusTraversalPolicy.getDefaultComponent(java.awt.Container)
    virtual java::awt::Component* getInitialComponent(java::awt::Window* arg1); // public java.awt.Component java.awt.FocusTraversalPolicy.getInitialComponent(java.awt.Window)
    virtual java::awt::Component* getComponentAfter(java::awt::Container* arg1, java::awt::Component* arg2); // public abstract java.awt.Component java.awt.FocusTraversalPolicy.getComponentAfter(java.awt.Container,java.awt.Component)
    virtual java::awt::Component* getComponentBefore(java::awt::Container* arg1, java::awt::Component* arg2); // public abstract java.awt.Component java.awt.FocusTraversalPolicy.getComponentBefore(java.awt.Container,java.awt.Component)
    virtual java::awt::Component* getFirstComponent(java::awt::Container* arg1); // public abstract java.awt.Component java.awt.FocusTraversalPolicy.getFirstComponent(java.awt.Container)
    virtual java::awt::Component* getLastComponent(java::awt::Container* arg1); // public abstract java.awt.Component java.awt.FocusTraversalPolicy.getLastComponent(java.awt.Container)

};
}
}
#endif
