#ifndef java_awt_PageAttributes__OrientationRequestedType_H
#define java_awt_PageAttributes__OrientationRequestedType_H
#include <jni.h>
#include <java_marker.h>
#include <java_awt_AttributeValue.h>

namespace java {
namespace lang {
class String;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
class PageAttributes__OrientationRequestedType : public java::awt::AttributeValue {
  public:
    PageAttributes__OrientationRequestedType(JavaMarker* dummy);
    PageAttributes__OrientationRequestedType(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual int hashCode(); // public int java.awt.PageAttributes$OrientationRequestedType.hashCode()
    virtual const char* toString(); // public java.lang.String java.awt.PageAttributes$OrientationRequestedType.toString()

    java::awt::PageAttributes__OrientationRequestedType* PORTRAIT; // public static final java.awt.PageAttributes$OrientationRequestedType java.awt.PageAttributes$OrientationRequestedType.PORTRAIT
    java::awt::PageAttributes__OrientationRequestedType* LANDSCAPE; // public static final java.awt.PageAttributes$OrientationRequestedType java.awt.PageAttributes$OrientationRequestedType.LANDSCAPE
};
}
}
#endif
