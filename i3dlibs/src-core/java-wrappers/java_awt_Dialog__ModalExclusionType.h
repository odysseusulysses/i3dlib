#ifndef java_awt_Dialog__ModalExclusionType_H
#define java_awt_Dialog__ModalExclusionType_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Enum.h>

namespace java {
namespace lang {
class String;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
class Dialog__ModalExclusionType : public java::lang::Enum {
  public:
    Dialog__ModalExclusionType(JavaMarker* dummy);
    Dialog__ModalExclusionType(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual java::awt::Dialog__ModalExclusionType* valueOf(const char* arg1); // public static java.awt.Dialog$ModalExclusionType java.awt.Dialog$ModalExclusionType.valueOf(java.lang.String)
    virtual JavaObjectArray* values(); // public static java.awt.Dialog$ModalExclusionType[] java.awt.Dialog$ModalExclusionType.values()

    java::awt::Dialog__ModalExclusionType* NO_EXCLUDE; // public static final java.awt.Dialog$ModalExclusionType java.awt.Dialog$ModalExclusionType.NO_EXCLUDE
    java::awt::Dialog__ModalExclusionType* APPLICATION_EXCLUDE; // public static final java.awt.Dialog$ModalExclusionType java.awt.Dialog$ModalExclusionType.APPLICATION_EXCLUDE
    java::awt::Dialog__ModalExclusionType* TOOLKIT_EXCLUDE; // public static final java.awt.Dialog$ModalExclusionType java.awt.Dialog$ModalExclusionType.TOOLKIT_EXCLUDE
};
}
}
#endif
