#ifndef java_lang_ThreadGroup_H
#define java_lang_ThreadGroup_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace lang {
class Thread__UncaughtExceptionHandler;
}
}

namespace java {
namespace lang {
class String;
}
}

namespace java {
namespace lang {
class Thread;
}
}

namespace java {
namespace lang {
class Throwable;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace lang {
class ThreadGroup : public java::lang::Object {
  public:
    ThreadGroup(JavaMarker* dummy);
    ThreadGroup(jobject obj);
    ThreadGroup(const char* arg1); // public java.lang.ThreadGroup(java.lang.String)
    ThreadGroup(java::lang::ThreadGroup* arg1, const char* arg2); // public java.lang.ThreadGroup(java.lang.ThreadGroup,java.lang.String)

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual void uncaughtException(java::lang::Thread* arg1, java::lang::Throwable* arg2); // public void java.lang.ThreadGroup.uncaughtException(java.lang.Thread,java.lang.Throwable)
    virtual const char* toString(); // public java.lang.String java.lang.ThreadGroup.toString()
    virtual const char* getName(); // public final java.lang.String java.lang.ThreadGroup.getName()
    virtual java::lang::ThreadGroup* getParent(); // public final java.lang.ThreadGroup java.lang.ThreadGroup.getParent()
    virtual void setDaemon(bool arg1); // public final void java.lang.ThreadGroup.setDaemon(boolean)
    virtual int activeCount(); // public int java.lang.ThreadGroup.activeCount()
    virtual void checkAccess(); // public final void java.lang.ThreadGroup.checkAccess()
    virtual void destroy(); // public final void java.lang.ThreadGroup.destroy()
    virtual int enumerate(JavaObjectArray* arg1); // public int java.lang.ThreadGroup.enumerate(java.lang.ThreadGroup[])
    virtual int enumerate(JavaObjectArray* arg1, bool arg2); // public int java.lang.ThreadGroup.enumerate(java.lang.Thread[],boolean)
    virtual int getMaxPriority(); // public final int java.lang.ThreadGroup.getMaxPriority()
    virtual void interrupt(); // public final void java.lang.ThreadGroup.interrupt()
    virtual bool isDaemon(); // public final boolean java.lang.ThreadGroup.isDaemon()
    virtual void resume(); // public final void java.lang.ThreadGroup.resume()
    virtual void stop(); // public final void java.lang.ThreadGroup.stop()
    virtual void suspend(); // public final void java.lang.ThreadGroup.suspend()
    virtual int activeGroupCount(); // public int java.lang.ThreadGroup.activeGroupCount()
    virtual bool allowThreadSuspension(bool arg1); // public boolean java.lang.ThreadGroup.allowThreadSuspension(boolean)
    virtual bool isDestroyed(); // public synchronized boolean java.lang.ThreadGroup.isDestroyed()
    virtual void list(); // public void java.lang.ThreadGroup.list()
    virtual bool parentOf(java::lang::ThreadGroup* arg1); // public final boolean java.lang.ThreadGroup.parentOf(java.lang.ThreadGroup)
    virtual void setMaxPriority(int arg1); // public final void java.lang.ThreadGroup.setMaxPriority(int)

};
}
}
#endif
