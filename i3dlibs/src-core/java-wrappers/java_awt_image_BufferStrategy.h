#ifndef java_awt_image_BufferStrategy_H
#define java_awt_image_BufferStrategy_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace awt {
class BufferCapabilities;
}
}

namespace java {
namespace awt {
class Graphics;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace image {
class BufferStrategy : public java::lang::Object {
  public:
    BufferStrategy(JavaMarker* dummy);
    BufferStrategy(jobject obj);
    BufferStrategy(); // public java.awt.image.BufferStrategy()

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual java::awt::BufferCapabilities* getCapabilities(); // public abstract java.awt.BufferCapabilities java.awt.image.BufferStrategy.getCapabilities()
    virtual void dispose(); // public void java.awt.image.BufferStrategy.dispose()
    virtual bool contentsLost(); // public abstract boolean java.awt.image.BufferStrategy.contentsLost()
    virtual void show(); // public abstract void java.awt.image.BufferStrategy.show()
    virtual bool contentsRestored(); // public abstract boolean java.awt.image.BufferStrategy.contentsRestored()
    virtual java::awt::Graphics* getDrawGraphics(); // public abstract java.awt.Graphics java.awt.image.BufferStrategy.getDrawGraphics()

};
}
}
}
#endif
