#ifndef java_util_Hashtable_H
#define java_util_Hashtable_H
#include <jni.h>
#include <java_marker.h>
#include <java_util_Dictionary.h>

namespace java {
namespace util {
class Map;
}
}

namespace java {
namespace lang {
class Cloneable;
}
}

namespace java {
namespace io {
class Serializable;
}
}

namespace java {
namespace lang {
class Object;
}
}

namespace java {
namespace lang {
class String;
}
}

namespace java {
namespace util {
class Set;
}
}

namespace java {
namespace util {
class Collection;
}
}

namespace java {
namespace util {
class Enumeration;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace util {
class Hashtable : public java::util::Dictionary {
  public:
    Hashtable(JavaMarker* dummy);
    Hashtable(jobject obj);
    Hashtable(); // public java.util.Hashtable()
    Hashtable(java::util::Map* arg1); // public java.util.Hashtable(java.util.Map)
    Hashtable(int arg1); // public java.util.Hashtable(int)
    Hashtable(int arg1, float arg2); // public java.util.Hashtable(int,float)

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual int hashCode(); // public synchronized int java.util.Hashtable.hashCode()
    virtual java::lang::Object* put(java::lang::Object* arg1, java::lang::Object* arg2); // public synchronized java.lang.Object java.util.Hashtable.put(java.lang.Object,java.lang.Object)
    virtual java::lang::Object* clone(); // public synchronized java.lang.Object java.util.Hashtable.clone()
    virtual void clear(); // public synchronized void java.util.Hashtable.clear()
    virtual bool equals(java::lang::Object* arg1); // public synchronized boolean java.util.Hashtable.equals(java.lang.Object)
    virtual const char* toString(); // public synchronized java.lang.String java.util.Hashtable.toString()
    virtual bool contains(java::lang::Object* arg1); // public synchronized boolean java.util.Hashtable.contains(java.lang.Object)
    virtual bool isEmpty(); // public synchronized boolean java.util.Hashtable.isEmpty()
    virtual java::util::Set* entrySet(); // public java.util.Set java.util.Hashtable.entrySet()
    virtual java::lang::Object* get(java::lang::Object* arg1); // public synchronized java.lang.Object java.util.Hashtable.get(java.lang.Object)
    virtual void putAll(java::util::Map* arg1); // public synchronized void java.util.Hashtable.putAll(java.util.Map)
    virtual int size(); // public synchronized int java.util.Hashtable.size()
    virtual java::util::Collection* values(); // public java.util.Collection java.util.Hashtable.values()
    virtual java::lang::Object* remove(java::lang::Object* arg1); // public synchronized java.lang.Object java.util.Hashtable.remove(java.lang.Object)
    virtual java::util::Enumeration* elements(); // public synchronized java.util.Enumeration java.util.Hashtable.elements()
    virtual java::util::Set* keySet(); // public java.util.Set java.util.Hashtable.keySet()
    virtual java::util::Enumeration* keys(); // public synchronized java.util.Enumeration java.util.Hashtable.keys()
    virtual bool containsKey(java::lang::Object* arg1); // public synchronized boolean java.util.Hashtable.containsKey(java.lang.Object)
    virtual bool containsValue(java::lang::Object* arg1); // public boolean java.util.Hashtable.containsValue(java.lang.Object)

};
}
}
#endif
