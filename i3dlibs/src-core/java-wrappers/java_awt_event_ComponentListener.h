#ifndef java_awt_event_ComponentListener_H
#define java_awt_event_ComponentListener_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace util {
class EventListener;
}
}

namespace java {
namespace awt {
namespace event {
class ComponentEvent;
}
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace event {
class ComponentListener : public java::lang::Object {
  public:
    ComponentListener(JavaMarker* dummy);
    ComponentListener(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual void componentHidden(java::awt::event::ComponentEvent* arg1); // public abstract void java.awt.event.ComponentListener.componentHidden(java.awt.event.ComponentEvent)
    virtual void componentMoved(java::awt::event::ComponentEvent* arg1); // public abstract void java.awt.event.ComponentListener.componentMoved(java.awt.event.ComponentEvent)
    virtual void componentResized(java::awt::event::ComponentEvent* arg1); // public abstract void java.awt.event.ComponentListener.componentResized(java.awt.event.ComponentEvent)
    virtual void componentShown(java::awt::event::ComponentEvent* arg1); // public abstract void java.awt.event.ComponentListener.componentShown(java.awt.event.ComponentEvent)

};
}
}
}
#endif
