#ifndef loci_formats_meta_MetadataStore_H
#define loci_formats_meta_MetadataStore_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace lang {
class String;
}
}

namespace java {
namespace lang {
class Float;
}
}

namespace java {
namespace lang {
class Integer;
}
}

namespace java {
namespace lang {
class Boolean;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace loci {
namespace formats {
namespace meta {
class MetadataStore : public java::lang::Object {
  public:
    MetadataStore(JavaMarker* dummy);
    MetadataStore(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual void setImageName(const char* arg1, int arg2); // public abstract void loci.formats.meta.MetadataStore.setImageName(java.lang.String,int)
    virtual void setImageCreationDate(const char* arg1, int arg2); // public abstract void loci.formats.meta.MetadataStore.setImageCreationDate(java.lang.String,int)
    virtual void setDimensionsPhysicalSizeX(java::lang::Float* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setDimensionsPhysicalSizeX(java.lang.Float,int,int)
    virtual void setDimensionsPhysicalSizeY(java::lang::Float* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setDimensionsPhysicalSizeY(java.lang.Float,int,int)
    virtual void createRoot(); // public abstract void loci.formats.meta.MetadataStore.createRoot()
    virtual java::lang::Object* getRoot(); // public abstract java.lang.Object loci.formats.meta.MetadataStore.getRoot()
    virtual void setRoot(java::lang::Object* arg1); // public abstract void loci.formats.meta.MetadataStore.setRoot(java.lang.Object)
    virtual void setImageDescription(const char* arg1, int arg2); // public abstract void loci.formats.meta.MetadataStore.setImageDescription(java.lang.String,int)
    virtual void setPixelsSizeX(java::lang::Integer* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setPixelsSizeX(java.lang.Integer,int,int)
    virtual void setPixelsSizeY(java::lang::Integer* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setPixelsSizeY(java.lang.Integer,int,int)
    virtual void setPixelsSizeZ(java::lang::Integer* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setPixelsSizeZ(java.lang.Integer,int,int)
    virtual void setPixelsSizeC(java::lang::Integer* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setPixelsSizeC(java.lang.Integer,int,int)
    virtual void setPixelsSizeT(java::lang::Integer* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setPixelsSizeT(java.lang.Integer,int,int)
    virtual void setPixelsPixelType(const char* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setPixelsPixelType(java.lang.String,int,int)
    virtual void setPixelsBigEndian(java::lang::Boolean* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setPixelsBigEndian(java.lang.Boolean,int,int)
    virtual void setPixelsDimensionOrder(const char* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setPixelsDimensionOrder(java.lang.String,int,int)
    virtual void setDimensionsPhysicalSizeZ(java::lang::Float* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setDimensionsPhysicalSizeZ(java.lang.Float,int,int)
    virtual void setDimensionsTimeIncrement(java::lang::Float* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setDimensionsTimeIncrement(java.lang.Float,int,int)
    virtual void setDimensionsWaveStart(java::lang::Integer* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setDimensionsWaveStart(java.lang.Integer,int,int)
    virtual void setDimensionsWaveIncrement(java::lang::Integer* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setDimensionsWaveIncrement(java.lang.Integer,int,int)
    virtual void setImagingEnvironmentTemperature(java::lang::Float* arg1, int arg2); // public abstract void loci.formats.meta.MetadataStore.setImagingEnvironmentTemperature(java.lang.Float,int)
    virtual void setImagingEnvironmentAirPressure(java::lang::Float* arg1, int arg2); // public abstract void loci.formats.meta.MetadataStore.setImagingEnvironmentAirPressure(java.lang.Float,int)
    virtual void setImagingEnvironmentHumidity(java::lang::Float* arg1, int arg2); // public abstract void loci.formats.meta.MetadataStore.setImagingEnvironmentHumidity(java.lang.Float,int)
    virtual void setImagingEnvironmentCO2Percent(java::lang::Float* arg1, int arg2); // public abstract void loci.formats.meta.MetadataStore.setImagingEnvironmentCO2Percent(java.lang.Float,int)
    virtual void setPlaneTheZ(java::lang::Integer* arg1, int arg2, int arg3, int arg4); // public abstract void loci.formats.meta.MetadataStore.setPlaneTheZ(java.lang.Integer,int,int,int)
    virtual void setPlaneTheC(java::lang::Integer* arg1, int arg2, int arg3, int arg4); // public abstract void loci.formats.meta.MetadataStore.setPlaneTheC(java.lang.Integer,int,int,int)
    virtual void setPlaneTheT(java::lang::Integer* arg1, int arg2, int arg3, int arg4); // public abstract void loci.formats.meta.MetadataStore.setPlaneTheT(java.lang.Integer,int,int,int)
    virtual void setPlaneTimingDeltaT(java::lang::Float* arg1, int arg2, int arg3, int arg4); // public abstract void loci.formats.meta.MetadataStore.setPlaneTimingDeltaT(java.lang.Float,int,int,int)
    virtual void setPlaneTimingExposureTime(java::lang::Float* arg1, int arg2, int arg3, int arg4); // public abstract void loci.formats.meta.MetadataStore.setPlaneTimingExposureTime(java.lang.Float,int,int,int)
    virtual void setStagePositionPositionX(java::lang::Float* arg1, int arg2, int arg3, int arg4); // public abstract void loci.formats.meta.MetadataStore.setStagePositionPositionX(java.lang.Float,int,int,int)
    virtual void setStagePositionPositionY(java::lang::Float* arg1, int arg2, int arg3, int arg4); // public abstract void loci.formats.meta.MetadataStore.setStagePositionPositionY(java.lang.Float,int,int,int)
    virtual void setStagePositionPositionZ(java::lang::Float* arg1, int arg2, int arg3, int arg4); // public abstract void loci.formats.meta.MetadataStore.setStagePositionPositionZ(java.lang.Float,int,int,int)
    virtual void setLogicalChannelName(const char* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setLogicalChannelName(java.lang.String,int,int)
    virtual void setLogicalChannelSamplesPerPixel(java::lang::Integer* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setLogicalChannelSamplesPerPixel(java.lang.Integer,int,int)
    virtual void setLogicalChannelIlluminationType(const char* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setLogicalChannelIlluminationType(java.lang.String,int,int)
    virtual void setLogicalChannelPinholeSize(java::lang::Integer* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setLogicalChannelPinholeSize(java.lang.Integer,int,int)
    virtual void setLogicalChannelPhotometricInterpretation(const char* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setLogicalChannelPhotometricInterpretation(java.lang.String,int,int)
    virtual void setLogicalChannelMode(const char* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setLogicalChannelMode(java.lang.String,int,int)
    virtual void setLogicalChannelContrastMethod(const char* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setLogicalChannelContrastMethod(java.lang.String,int,int)
    virtual void setLogicalChannelExWave(java::lang::Integer* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setLogicalChannelExWave(java.lang.Integer,int,int)
    virtual void setLogicalChannelEmWave(java::lang::Integer* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setLogicalChannelEmWave(java.lang.Integer,int,int)
    virtual void setLogicalChannelFluor(const char* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setLogicalChannelFluor(java.lang.String,int,int)
    virtual void setLogicalChannelNdFilter(java::lang::Float* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setLogicalChannelNdFilter(java.lang.Float,int,int)
    virtual void setLogicalChannelPockelCellSetting(java::lang::Integer* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setLogicalChannelPockelCellSetting(java.lang.Integer,int,int)
    virtual void setDetectorSettingsDetector(java::lang::Object* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setDetectorSettingsDetector(java.lang.Object,int,int)
    virtual void setDetectorSettingsOffset(java::lang::Float* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setDetectorSettingsOffset(java.lang.Float,int,int)
    virtual void setDetectorSettingsGain(java::lang::Float* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setDetectorSettingsGain(java.lang.Float,int,int)
    virtual void setLightSourceSettingsLightSource(java::lang::Object* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setLightSourceSettingsLightSource(java.lang.Object,int,int)
    virtual void setLightSourceSettingsAttenuation(java::lang::Float* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setLightSourceSettingsAttenuation(java.lang.Float,int,int)
    virtual void setLightSourceSettingsWavelength(java::lang::Integer* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setLightSourceSettingsWavelength(java.lang.Integer,int,int)
    virtual void setROIX0(java::lang::Integer* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setROIX0(java.lang.Integer,int,int)
    virtual void setROIY0(java::lang::Integer* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setROIY0(java.lang.Integer,int,int)
    virtual void setROIZ0(java::lang::Integer* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setROIZ0(java.lang.Integer,int,int)
    virtual void setROIT0(java::lang::Integer* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setROIT0(java.lang.Integer,int,int)
    virtual void setROIX1(java::lang::Integer* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setROIX1(java.lang.Integer,int,int)
    virtual void setROIY1(java::lang::Integer* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setROIY1(java.lang.Integer,int,int)
    virtual void setROIZ1(java::lang::Integer* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setROIZ1(java.lang.Integer,int,int)
    virtual void setROIT1(java::lang::Integer* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setROIT1(java.lang.Integer,int,int)
    virtual void setStageLabelName(const char* arg1, int arg2); // public abstract void loci.formats.meta.MetadataStore.setStageLabelName(java.lang.String,int)
    virtual void setStageLabelX(java::lang::Float* arg1, int arg2); // public abstract void loci.formats.meta.MetadataStore.setStageLabelX(java.lang.Float,int)
    virtual void setStageLabelY(java::lang::Float* arg1, int arg2); // public abstract void loci.formats.meta.MetadataStore.setStageLabelY(java.lang.Float,int)
    virtual void setStageLabelZ(java::lang::Float* arg1, int arg2); // public abstract void loci.formats.meta.MetadataStore.setStageLabelZ(java.lang.Float,int)
    virtual void setLightSourceManufacturer(const char* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setLightSourceManufacturer(java.lang.String,int,int)
    virtual void setLightSourceModel(const char* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setLightSourceModel(java.lang.String,int,int)
    virtual void setLightSourceSerialNumber(const char* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setLightSourceSerialNumber(java.lang.String,int,int)
    virtual void setLaserType(const char* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setLaserType(java.lang.String,int,int)
    virtual void setLaserLaserMedium(const char* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setLaserLaserMedium(java.lang.String,int,int)
    virtual void setLaserWavelength(java::lang::Integer* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setLaserWavelength(java.lang.Integer,int,int)
    virtual void setLaserFrequencyMultiplication(java::lang::Integer* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setLaserFrequencyMultiplication(java.lang.Integer,int,int)
    virtual void setLaserTuneable(java::lang::Boolean* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setLaserTuneable(java.lang.Boolean,int,int)
    virtual void setLaserPulse(const char* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setLaserPulse(java.lang.String,int,int)
    virtual void setLaserPower(java::lang::Float* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setLaserPower(java.lang.Float,int,int)
    virtual void setFilamentType(const char* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setFilamentType(java.lang.String,int,int)
    virtual void setFilamentPower(java::lang::Float* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setFilamentPower(java.lang.Float,int,int)
    virtual void setArcType(const char* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setArcType(java.lang.String,int,int)
    virtual void setArcPower(java::lang::Float* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setArcPower(java.lang.Float,int,int)
    virtual void setDetectorManufacturer(const char* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setDetectorManufacturer(java.lang.String,int,int)
    virtual void setDetectorModel(const char* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setDetectorModel(java.lang.String,int,int)
    virtual void setDetectorSerialNumber(const char* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setDetectorSerialNumber(java.lang.String,int,int)
    virtual void setDetectorType(const char* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setDetectorType(java.lang.String,int,int)
    virtual void setDetectorGain(java::lang::Float* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setDetectorGain(java.lang.Float,int,int)
    virtual void setDetectorVoltage(java::lang::Float* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setDetectorVoltage(java.lang.Float,int,int)
    virtual void setDetectorOffset(java::lang::Float* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setDetectorOffset(java.lang.Float,int,int)
    virtual void setObjectiveManufacturer(const char* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setObjectiveManufacturer(java.lang.String,int,int)
    virtual void setObjectiveModel(const char* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setObjectiveModel(java.lang.String,int,int)
    virtual void setObjectiveSerialNumber(const char* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setObjectiveSerialNumber(java.lang.String,int,int)
    virtual void setObjectiveCorrection(const char* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setObjectiveCorrection(java.lang.String,int,int)
    virtual void setObjectiveImmersion(const char* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setObjectiveImmersion(java.lang.String,int,int)
    virtual void setObjectiveLensNA(java::lang::Float* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setObjectiveLensNA(java.lang.Float,int,int)
    virtual void setObjectiveNominalMagnification(java::lang::Integer* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setObjectiveNominalMagnification(java.lang.Integer,int,int)
    virtual void setObjectiveCalibratedMagnification(java::lang::Float* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setObjectiveCalibratedMagnification(java.lang.Float,int,int)
    virtual void setObjectiveWorkingDistance(java::lang::Float* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setObjectiveWorkingDistance(java.lang.Float,int,int)
    virtual void setOTFSizeX(java::lang::Integer* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setOTFSizeX(java.lang.Integer,int,int)
    virtual void setOTFSizeY(java::lang::Integer* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setOTFSizeY(java.lang.Integer,int,int)
    virtual void setOTFPixelType(const char* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setOTFPixelType(java.lang.String,int,int)
    virtual void setOTFPath(const char* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setOTFPath(java.lang.String,int,int)
    virtual void setOTFOpticalAxisAveraged(java::lang::Boolean* arg1, int arg2, int arg3); // public abstract void loci.formats.meta.MetadataStore.setOTFOpticalAxisAveraged(java.lang.Boolean,int,int)
    virtual void setExperimenterFirstName(const char* arg1, int arg2); // public abstract void loci.formats.meta.MetadataStore.setExperimenterFirstName(java.lang.String,int)
    virtual void setExperimenterLastName(const char* arg1, int arg2); // public abstract void loci.formats.meta.MetadataStore.setExperimenterLastName(java.lang.String,int)
    virtual void setExperimenterEmail(const char* arg1, int arg2); // public abstract void loci.formats.meta.MetadataStore.setExperimenterEmail(java.lang.String,int)
    virtual void setExperimenterInstitution(const char* arg1, int arg2); // public abstract void loci.formats.meta.MetadataStore.setExperimenterInstitution(java.lang.String,int)
    virtual void setExperimenterDataDirectory(const char* arg1, int arg2); // public abstract void loci.formats.meta.MetadataStore.setExperimenterDataDirectory(java.lang.String,int)

};
}
}
}
#endif
