#ifndef java_awt_event_InputMethodListener_H
#define java_awt_event_InputMethodListener_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace util {
class EventListener;
}
}

namespace java {
namespace awt {
namespace event {
class InputMethodEvent;
}
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace event {
class InputMethodListener : public java::lang::Object {
  public:
    InputMethodListener(JavaMarker* dummy);
    InputMethodListener(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual void caretPositionChanged(java::awt::event::InputMethodEvent* arg1); // public abstract void java.awt.event.InputMethodListener.caretPositionChanged(java.awt.event.InputMethodEvent)
    virtual void inputMethodTextChanged(java::awt::event::InputMethodEvent* arg1); // public abstract void java.awt.event.InputMethodListener.inputMethodTextChanged(java.awt.event.InputMethodEvent)

};
}
}
}
#endif
