#ifndef javax_accessibility_AccessibleSelection_H
#define javax_accessibility_AccessibleSelection_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace javax {
namespace accessibility {
class Accessible;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace javax {
namespace accessibility {
class AccessibleSelection : public java::lang::Object {
  public:
    AccessibleSelection(JavaMarker* dummy);
    AccessibleSelection(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual javax::accessibility::Accessible* getAccessibleSelection(int arg1); // public abstract javax.accessibility.Accessible javax.accessibility.AccessibleSelection.getAccessibleSelection(int)
    virtual bool isAccessibleChildSelected(int arg1); // public abstract boolean javax.accessibility.AccessibleSelection.isAccessibleChildSelected(int)
    virtual void addAccessibleSelection(int arg1); // public abstract void javax.accessibility.AccessibleSelection.addAccessibleSelection(int)
    virtual void clearAccessibleSelection(); // public abstract void javax.accessibility.AccessibleSelection.clearAccessibleSelection()
    virtual int getAccessibleSelectionCount(); // public abstract int javax.accessibility.AccessibleSelection.getAccessibleSelectionCount()
    virtual void removeAccessibleSelection(int arg1); // public abstract void javax.accessibility.AccessibleSelection.removeAccessibleSelection(int)
    virtual void selectAllAccessibleSelection(); // public abstract void javax.accessibility.AccessibleSelection.selectAllAccessibleSelection()

};
}
}
#endif
