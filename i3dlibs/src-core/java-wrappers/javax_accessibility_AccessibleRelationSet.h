#ifndef javax_accessibility_AccessibleRelationSet_H
#define javax_accessibility_AccessibleRelationSet_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace javax {
namespace accessibility {
class AccessibleRelation;
}
}

namespace java {
namespace lang {
class String;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace javax {
namespace accessibility {
class AccessibleRelationSet : public java::lang::Object {
  public:
    AccessibleRelationSet(JavaMarker* dummy);
    AccessibleRelationSet(jobject obj);
    AccessibleRelationSet(JavaObjectArray* arg1); // public javax.accessibility.AccessibleRelationSet(javax.accessibility.AccessibleRelation[])
    AccessibleRelationSet(); // public javax.accessibility.AccessibleRelationSet()

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual bool add(javax::accessibility::AccessibleRelation* arg1); // public boolean javax.accessibility.AccessibleRelationSet.add(javax.accessibility.AccessibleRelation)
    virtual void clear(); // public void javax.accessibility.AccessibleRelationSet.clear()
    virtual const char* toString(); // public java.lang.String javax.accessibility.AccessibleRelationSet.toString()
    virtual bool contains(const char* arg1); // public boolean javax.accessibility.AccessibleRelationSet.contains(java.lang.String)
    virtual void addAll(JavaObjectArray* arg1); // public void javax.accessibility.AccessibleRelationSet.addAll(javax.accessibility.AccessibleRelation[])
    virtual javax::accessibility::AccessibleRelation* get(const char* arg1); // public javax.accessibility.AccessibleRelation javax.accessibility.AccessibleRelationSet.get(java.lang.String)
    virtual int size(); // public int javax.accessibility.AccessibleRelationSet.size()
    virtual JavaObjectArray* toArray(); // public javax.accessibility.AccessibleRelation[] javax.accessibility.AccessibleRelationSet.toArray()
    virtual bool remove(javax::accessibility::AccessibleRelation* arg1); // public boolean javax.accessibility.AccessibleRelationSet.remove(javax.accessibility.AccessibleRelation)

};
}
}
#endif
