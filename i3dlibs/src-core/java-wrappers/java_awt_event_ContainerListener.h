#ifndef java_awt_event_ContainerListener_H
#define java_awt_event_ContainerListener_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace util {
class EventListener;
}
}

namespace java {
namespace awt {
namespace event {
class ContainerEvent;
}
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace event {
class ContainerListener : public java::lang::Object {
  public:
    ContainerListener(JavaMarker* dummy);
    ContainerListener(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual void componentAdded(java::awt::event::ContainerEvent* arg1); // public abstract void java.awt.event.ContainerListener.componentAdded(java.awt.event.ContainerEvent)
    virtual void componentRemoved(java::awt::event::ContainerEvent* arg1); // public abstract void java.awt.event.ContainerListener.componentRemoved(java.awt.event.ContainerEvent)

};
}
}
}
#endif
