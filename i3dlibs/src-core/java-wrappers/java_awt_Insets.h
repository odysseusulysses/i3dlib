#ifndef java_awt_Insets_H
#define java_awt_Insets_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace lang {
class Cloneable;
}
}

namespace java {
namespace io {
class Serializable;
}
}

namespace java {
namespace lang {
class String;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
class Insets : public java::lang::Object {
  public:
    Insets(JavaMarker* dummy);
    Insets(jobject obj);
    Insets(int arg1, int arg2, int arg3, int arg4); // public java.awt.Insets(int,int,int,int)

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual int hashCode(); // public int java.awt.Insets.hashCode()
    virtual java::lang::Object* clone(); // public java.lang.Object java.awt.Insets.clone()
    virtual bool equals(java::lang::Object* arg1); // public boolean java.awt.Insets.equals(java.lang.Object)
    virtual const char* toString(); // public java.lang.String java.awt.Insets.toString()
    virtual void set(int arg1, int arg2, int arg3, int arg4); // public void java.awt.Insets.set(int,int,int,int)

    int top; // public int java.awt.Insets.top
    int left; // public int java.awt.Insets.left
    int bottom; // public int java.awt.Insets.bottom
    int right; // public int java.awt.Insets.right
};
}
}
#endif
