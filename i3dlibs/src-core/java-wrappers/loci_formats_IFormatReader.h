#ifndef loci_formats_IFormatReader_H
#define loci_formats_IFormatReader_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace loci {
namespace formats {
class IFormatHandler;
}
}

namespace java {
namespace lang {
class String;
}
}

namespace java {
namespace awt {
namespace image {
class BufferedImage;
}
}
}

namespace loci {
namespace formats {
namespace meta {
class MetadataStore;
}
}
}

namespace java {
namespace util {
class Hashtable;
}
}

namespace loci {
namespace formats {
class CoreMetadata;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace loci {
namespace formats {
class IFormatReader : public java::lang::Object {
  public:
    IFormatReader(JavaMarker* dummy);
    IFormatReader(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual void close(bool arg1); // public abstract void loci.formats.IFormatReader.close(boolean) throws java.io.IOException
    virtual int getIndex(const char* arg1, int arg2, int arg3, int arg4); // public abstract int loci.formats.IFormatReader.getIndex(java.lang.String,int,int,int) throws loci.formats.FormatException,java.io.IOException
    virtual int getIndex(int arg1, int arg2, int arg3); // public abstract int loci.formats.IFormatReader.getIndex(int,int,int)
    virtual bool isThisType(JavaByteArray* arg1); // public abstract boolean loci.formats.IFormatReader.isThisType(byte[])
    virtual int fileGroupOption(const char* arg1); // public abstract int loci.formats.IFormatReader.fileGroupOption(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual JavaByteArray* openBytes(const char* arg1, int arg2, JavaByteArray* arg3); // public abstract byte[] loci.formats.IFormatReader.openBytes(java.lang.String,int,byte[]) throws loci.formats.FormatException,java.io.IOException
    virtual JavaByteArray* openBytes(const char* arg1, int arg2); // public abstract byte[] loci.formats.IFormatReader.openBytes(java.lang.String,int) throws loci.formats.FormatException,java.io.IOException
    virtual JavaByteArray* openBytes(int arg1, JavaByteArray* arg2, int arg3, int arg4, int arg5, int arg6); // public abstract byte[] loci.formats.IFormatReader.openBytes(int,byte[],int,int,int,int) throws loci.formats.FormatException,java.io.IOException
    virtual JavaByteArray* openBytes(int arg1, JavaByteArray* arg2); // public abstract byte[] loci.formats.IFormatReader.openBytes(int,byte[]) throws loci.formats.FormatException,java.io.IOException
    virtual JavaByteArray* openBytes(int arg1, int arg2, int arg3, int arg4, int arg5); // public abstract byte[] loci.formats.IFormatReader.openBytes(int,int,int,int,int) throws loci.formats.FormatException,java.io.IOException
    virtual JavaByteArray* openBytes(int arg1); // public abstract byte[] loci.formats.IFormatReader.openBytes(int) throws loci.formats.FormatException,java.io.IOException
    //virtual java::awt::image::BufferedImage* openThumbImage(int arg1); // public abstract java.awt.image.BufferedImage loci.formats.IFormatReader.openThumbImage(int) throws loci.formats.FormatException,java.io.IOException
    //virtual java::awt::image::BufferedImage* openThumbImage(const char* arg1, int arg2); // public abstract java.awt.image.BufferedImage loci.formats.IFormatReader.openThumbImage(java.lang.String,int) throws loci.formats.FormatException,java.io.IOException
    virtual JavaObjectArray* getUsedFiles(); // public abstract java.lang.String[] loci.formats.IFormatReader.getUsedFiles()
    virtual JavaObjectArray* getUsedFiles(const char* arg1); // public abstract java.lang.String[] loci.formats.IFormatReader.getUsedFiles(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    //virtual java::awt::image::BufferedImage* openImage(int arg1); // public abstract java.awt.image.BufferedImage loci.formats.IFormatReader.openImage(int) throws loci.formats.FormatException,java.io.IOException
    //virtual java::awt::image::BufferedImage* openImage(int arg1, int arg2, int arg3, int arg4, int arg5); // public abstract java.awt.image.BufferedImage loci.formats.IFormatReader.openImage(int,int,int,int,int) throws loci.formats.FormatException,java.io.IOException
    //virtual java::awt::image::BufferedImage* openImage(const char* arg1, int arg2); // public abstract java.awt.image.BufferedImage loci.formats.IFormatReader.openImage(java.lang.String,int) throws loci.formats.FormatException,java.io.IOException
    virtual int getSizeX(const char* arg1); // public abstract int loci.formats.IFormatReader.getSizeX(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual int getSizeX(); // public abstract int loci.formats.IFormatReader.getSizeX()
    virtual int getSizeY(const char* arg1); // public abstract int loci.formats.IFormatReader.getSizeY(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual int getSizeY(); // public abstract int loci.formats.IFormatReader.getSizeY()
    virtual loci::formats::meta::MetadataStore* getMetadataStore(const char* arg1); // public abstract loci.formats.meta.MetadataStore loci.formats.IFormatReader.getMetadataStore(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual loci::formats::meta::MetadataStore* getMetadataStore(); // public abstract loci.formats.meta.MetadataStore loci.formats.IFormatReader.getMetadataStore()
    virtual bool isMetadataFiltered(); // public abstract boolean loci.formats.IFormatReader.isMetadataFiltered()
    virtual int getImageCount(const char* arg1); // public abstract int loci.formats.IFormatReader.getImageCount(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual int getImageCount(); // public abstract int loci.formats.IFormatReader.getImageCount()
    virtual bool isRGB(const char* arg1); // public abstract boolean loci.formats.IFormatReader.isRGB(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual bool isRGB(); // public abstract boolean loci.formats.IFormatReader.isRGB()
    virtual int getSizeZ(const char* arg1); // public abstract int loci.formats.IFormatReader.getSizeZ(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual int getSizeZ(); // public abstract int loci.formats.IFormatReader.getSizeZ()
    virtual int getSizeC(const char* arg1); // public abstract int loci.formats.IFormatReader.getSizeC(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual int getSizeC(); // public abstract int loci.formats.IFormatReader.getSizeC()
    virtual int getSizeT(const char* arg1); // public abstract int loci.formats.IFormatReader.getSizeT(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual int getSizeT(); // public abstract int loci.formats.IFormatReader.getSizeT()
    virtual int getPixelType(); // public abstract int loci.formats.IFormatReader.getPixelType()
    virtual int getPixelType(const char* arg1); // public abstract int loci.formats.IFormatReader.getPixelType(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual int getEffectiveSizeC(); // public abstract int loci.formats.IFormatReader.getEffectiveSizeC()
    virtual int getEffectiveSizeC(const char* arg1); // public abstract int loci.formats.IFormatReader.getEffectiveSizeC(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual int getRGBChannelCount(); // public abstract int loci.formats.IFormatReader.getRGBChannelCount()
    virtual int getRGBChannelCount(const char* arg1); // public abstract int loci.formats.IFormatReader.getRGBChannelCount(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual bool isIndexed(); // public abstract boolean loci.formats.IFormatReader.isIndexed()
    virtual bool isFalseColor(); // public abstract boolean loci.formats.IFormatReader.isFalseColor()
    virtual JavaObjectArray* get8BitLookupTable(); // public abstract byte[][] loci.formats.IFormatReader.get8BitLookupTable() throws loci.formats.FormatException,java.io.IOException
    virtual JavaObjectArray* get16BitLookupTable(); // public abstract short[][] loci.formats.IFormatReader.get16BitLookupTable() throws loci.formats.FormatException,java.io.IOException
    virtual JavaIntArray* getChannelDimLengths(const char* arg1); // public abstract int[] loci.formats.IFormatReader.getChannelDimLengths(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual JavaIntArray* getChannelDimLengths(); // public abstract int[] loci.formats.IFormatReader.getChannelDimLengths()
    virtual JavaObjectArray* getChannelDimTypes(); // public abstract java.lang.String[] loci.formats.IFormatReader.getChannelDimTypes()
    virtual JavaObjectArray* getChannelDimTypes(const char* arg1); // public abstract java.lang.String[] loci.formats.IFormatReader.getChannelDimTypes(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual int getThumbSizeX(); // public abstract int loci.formats.IFormatReader.getThumbSizeX()
    virtual int getThumbSizeX(const char* arg1); // public abstract int loci.formats.IFormatReader.getThumbSizeX(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual int getThumbSizeY(); // public abstract int loci.formats.IFormatReader.getThumbSizeY()
    virtual int getThumbSizeY(const char* arg1); // public abstract int loci.formats.IFormatReader.getThumbSizeY(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual bool isLittleEndian(); // public abstract boolean loci.formats.IFormatReader.isLittleEndian()
    virtual bool isLittleEndian(const char* arg1); // public abstract boolean loci.formats.IFormatReader.isLittleEndian(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual const char* getDimensionOrder(); // public abstract java.lang.String loci.formats.IFormatReader.getDimensionOrder()
    virtual const char* getDimensionOrder(const char* arg1); // public abstract java.lang.String loci.formats.IFormatReader.getDimensionOrder(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual bool isOrderCertain(); // public abstract boolean loci.formats.IFormatReader.isOrderCertain()
    virtual bool isOrderCertain(const char* arg1); // public abstract boolean loci.formats.IFormatReader.isOrderCertain(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual bool isInterleaved(); // public abstract boolean loci.formats.IFormatReader.isInterleaved()
    virtual bool isInterleaved(const char* arg1, int arg2); // public abstract boolean loci.formats.IFormatReader.isInterleaved(java.lang.String,int) throws loci.formats.FormatException,java.io.IOException
    virtual bool isInterleaved(const char* arg1); // public abstract boolean loci.formats.IFormatReader.isInterleaved(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual bool isInterleaved(int arg1); // public abstract boolean loci.formats.IFormatReader.isInterleaved(int)
    virtual JavaByteArray* openThumbBytes(int arg1); // public abstract byte[] loci.formats.IFormatReader.openThumbBytes(int) throws loci.formats.FormatException,java.io.IOException
    virtual JavaByteArray* openThumbBytes(const char* arg1, int arg2); // public abstract byte[] loci.formats.IFormatReader.openThumbBytes(java.lang.String,int) throws loci.formats.FormatException,java.io.IOException
    virtual int getSeriesCount(); // public abstract int loci.formats.IFormatReader.getSeriesCount()
    virtual int getSeriesCount(const char* arg1); // public abstract int loci.formats.IFormatReader.getSeriesCount(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual void setSeries(int arg1); // public abstract void loci.formats.IFormatReader.setSeries(int)
    virtual void setSeries(const char* arg1, int arg2); // public abstract void loci.formats.IFormatReader.setSeries(java.lang.String,int) throws loci.formats.FormatException,java.io.IOException
    virtual int getSeries(); // public abstract int loci.formats.IFormatReader.getSeries()
    virtual int getSeries(const char* arg1); // public abstract int loci.formats.IFormatReader.getSeries(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual void setGroupFiles(bool arg1); // public abstract void loci.formats.IFormatReader.setGroupFiles(boolean)
    virtual bool isGroupFiles(); // public abstract boolean loci.formats.IFormatReader.isGroupFiles()
    virtual bool isMetadataComplete(); // public abstract boolean loci.formats.IFormatReader.isMetadataComplete()
    virtual void setNormalized(bool arg1); // public abstract void loci.formats.IFormatReader.setNormalized(boolean)
    virtual bool isNormalized(); // public abstract boolean loci.formats.IFormatReader.isNormalized()
    virtual void setMetadataCollected(bool arg1); // public abstract void loci.formats.IFormatReader.setMetadataCollected(boolean)
    virtual bool isMetadataCollected(); // public abstract boolean loci.formats.IFormatReader.isMetadataCollected()
    virtual void setOriginalMetadataPopulated(bool arg1); // public abstract void loci.formats.IFormatReader.setOriginalMetadataPopulated(boolean)
    virtual bool isOriginalMetadataPopulated(); // public abstract boolean loci.formats.IFormatReader.isOriginalMetadataPopulated()
    virtual const char* getCurrentFile(); // public abstract java.lang.String loci.formats.IFormatReader.getCurrentFile()
    virtual JavaIntArray* getZCTCoords(const char* arg1, int arg2); // public abstract int[] loci.formats.IFormatReader.getZCTCoords(java.lang.String,int) throws loci.formats.FormatException,java.io.IOException
    virtual JavaIntArray* getZCTCoords(int arg1); // public abstract int[] loci.formats.IFormatReader.getZCTCoords(int)
    virtual java::lang::Object* getMetadataValue(const char* arg1, const char* arg2); // public abstract java.lang.Object loci.formats.IFormatReader.getMetadataValue(java.lang.String,java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual java::lang::Object* getMetadataValue(const char* arg1); // public abstract java.lang.Object loci.formats.IFormatReader.getMetadataValue(java.lang.String)
    //virtual java::util::Hashtable* getMetadata(); // public abstract java.util.Hashtable loci.formats.IFormatReader.getMetadata()
    //virtual java::util::Hashtable* getMetadata(const char* arg1); // public abstract java.util.Hashtable loci.formats.IFormatReader.getMetadata(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual loci::formats::CoreMetadata* getCoreMetadata(const char* arg1); // public abstract loci.formats.CoreMetadata loci.formats.IFormatReader.getCoreMetadata(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual loci::formats::CoreMetadata* getCoreMetadata(); // public abstract loci.formats.CoreMetadata loci.formats.IFormatReader.getCoreMetadata()
    virtual void setMetadataFiltered(bool arg1); // public abstract void loci.formats.IFormatReader.setMetadataFiltered(boolean)
    virtual void setMetadataStore(loci::formats::meta::MetadataStore* arg1); // public abstract void loci.formats.IFormatReader.setMetadataStore(loci.formats.meta.MetadataStore)
    virtual java::lang::Object* getMetadataStoreRoot(const char* arg1); // public abstract java.lang.Object loci.formats.IFormatReader.getMetadataStoreRoot(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual java::lang::Object* getMetadataStoreRoot(); // public abstract java.lang.Object loci.formats.IFormatReader.getMetadataStoreRoot()
    virtual JavaObjectArray* getUnderlyingReaders(); // public abstract loci.formats.IFormatReader[] loci.formats.IFormatReader.getUnderlyingReaders()

    int MUST_GROUP; // public static final int loci.formats.IFormatReader.MUST_GROUP
    int CAN_GROUP; // public static final int loci.formats.IFormatReader.CAN_GROUP
    int CANNOT_GROUP; // public static final int loci.formats.IFormatReader.CANNOT_GROUP
};
}
}
#endif
