#ifndef java_awt_dnd_DragSourceListener_H
#define java_awt_dnd_DragSourceListener_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace util {
class EventListener;
}
}

namespace java {
namespace awt {
namespace dnd {
class DragSourceDragEvent;
}
}
}

namespace java {
namespace awt {
namespace dnd {
class DragSourceEvent;
}
}
}

namespace java {
namespace awt {
namespace dnd {
class DragSourceDropEvent;
}
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace dnd {
class DragSourceListener : public java::lang::Object {
  public:
    DragSourceListener(JavaMarker* dummy);
    DragSourceListener(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual void dragEnter(java::awt::dnd::DragSourceDragEvent* arg1); // public abstract void java.awt.dnd.DragSourceListener.dragEnter(java.awt.dnd.DragSourceDragEvent)
    virtual void dragExit(java::awt::dnd::DragSourceEvent* arg1); // public abstract void java.awt.dnd.DragSourceListener.dragExit(java.awt.dnd.DragSourceEvent)
    virtual void dragOver(java::awt::dnd::DragSourceDragEvent* arg1); // public abstract void java.awt.dnd.DragSourceListener.dragOver(java.awt.dnd.DragSourceDragEvent)
    virtual void dropActionChanged(java::awt::dnd::DragSourceDragEvent* arg1); // public abstract void java.awt.dnd.DragSourceListener.dropActionChanged(java.awt.dnd.DragSourceDragEvent)
    virtual void dragDropEnd(java::awt::dnd::DragSourceDropEvent* arg1); // public abstract void java.awt.dnd.DragSourceListener.dragDropEnd(java.awt.dnd.DragSourceDropEvent)

};
}
}
}
#endif
