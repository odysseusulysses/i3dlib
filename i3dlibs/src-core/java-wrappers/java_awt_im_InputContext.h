#ifndef java_awt_im_InputContext_H
#define java_awt_im_InputContext_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace util {
class Locale;
}
}

namespace java {
namespace awt {
class Component;
}
}

namespace java {
namespace awt {
class AWTEvent;
}
}

namespace java {
namespace lang {
class Character__Subset;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace im {
class InputContext : public java::lang::Object {
  public:
    InputContext(JavaMarker* dummy);
    InputContext(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual java::awt::im::InputContext* getInstance(); // public static java.awt.im.InputContext java.awt.im.InputContext.getInstance()
    virtual void dispose(); // public void java.awt.im.InputContext.dispose()
    virtual java::util::Locale* getLocale(); // public java.util.Locale java.awt.im.InputContext.getLocale()
    virtual void removeNotify(java::awt::Component* arg1); // public void java.awt.im.InputContext.removeNotify(java.awt.Component)
    virtual void dispatchEvent(java::awt::AWTEvent* arg1); // public void java.awt.im.InputContext.dispatchEvent(java.awt.AWTEvent)
    virtual void endComposition(); // public void java.awt.im.InputContext.endComposition()
    virtual java::lang::Object* getInputMethodControlObject(); // public java.lang.Object java.awt.im.InputContext.getInputMethodControlObject()
    virtual bool isCompositionEnabled(); // public boolean java.awt.im.InputContext.isCompositionEnabled()
    virtual void reconvert(); // public void java.awt.im.InputContext.reconvert()
    virtual bool selectInputMethod(java::util::Locale* arg1); // public boolean java.awt.im.InputContext.selectInputMethod(java.util.Locale)
    virtual void setCharacterSubsets(JavaObjectArray* arg1); // public void java.awt.im.InputContext.setCharacterSubsets(java.lang.Character$Subset[])
    virtual void setCompositionEnabled(bool arg1); // public void java.awt.im.InputContext.setCompositionEnabled(boolean)

};
}
}
}
#endif
