#ifndef java_util_AbstractList_H
#define java_util_AbstractList_H
#include <jni.h>
#include <java_marker.h>
#include <java_util_AbstractCollection.h>

namespace java {
namespace util {
class List;
}
}

namespace java {
namespace lang {
class Object;
}
}

namespace java {
namespace util {
class Collection;
}
}

namespace java {
namespace util {
class Iterator;
}
}

namespace java {
namespace util {
class ListIterator;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace util {
class AbstractList : public java::util::AbstractCollection {
  public:
    AbstractList(JavaMarker* dummy);
    AbstractList(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual int hashCode(); // public int java.util.AbstractList.hashCode()
    virtual void add(int arg1, java::lang::Object* arg2); // public void java.util.AbstractList.add(int,java.lang.Object)
    virtual bool add(java::lang::Object* arg1); // public boolean java.util.AbstractList.add(java.lang.Object)
    virtual int indexOf(java::lang::Object* arg1); // public int java.util.AbstractList.indexOf(java.lang.Object)
    virtual void clear(); // public void java.util.AbstractList.clear()
    virtual bool equals(java::lang::Object* arg1); // public boolean java.util.AbstractList.equals(java.lang.Object)
    virtual int lastIndexOf(java::lang::Object* arg1); // public int java.util.AbstractList.lastIndexOf(java.lang.Object)
    virtual bool addAll(int arg1, java::util::Collection* arg2); // public boolean java.util.AbstractList.addAll(int,java.util.Collection)
    virtual java::lang::Object* get(int arg1); // public abstract java.lang.Object java.util.AbstractList.get(int)
    virtual java::util::Iterator* iterator(); // public java.util.Iterator java.util.AbstractList.iterator()
    virtual java::lang::Object* remove(int arg1); // public java.lang.Object java.util.AbstractList.remove(int)
    virtual java::lang::Object* set(int arg1, java::lang::Object* arg2); // public java.lang.Object java.util.AbstractList.set(int,java.lang.Object)
    virtual java::util::List* subList(int arg1, int arg2); // public java.util.List java.util.AbstractList.subList(int,int)
    virtual java::util::ListIterator* listIterator(); // public java.util.ListIterator java.util.AbstractList.listIterator()
    virtual java::util::ListIterator* listIterator(int arg1); // public java.util.ListIterator java.util.AbstractList.listIterator(int)

};
}
}
#endif
