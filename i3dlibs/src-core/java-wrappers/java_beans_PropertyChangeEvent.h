#ifndef java_beans_PropertyChangeEvent_H
#define java_beans_PropertyChangeEvent_H
#include <jni.h>
#include <java_marker.h>
#include <java_util_EventObject.h>

namespace java {
namespace lang {
class Object;
}
}

namespace java {
namespace lang {
class String;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace beans {
class PropertyChangeEvent : public java::util::EventObject {
  public:
    PropertyChangeEvent(JavaMarker* dummy);
    PropertyChangeEvent(jobject obj);
    PropertyChangeEvent(java::lang::Object* arg1, const char* arg2, java::lang::Object* arg3, java::lang::Object* arg4); // public java.beans.PropertyChangeEvent(java.lang.Object,java.lang.String,java.lang.Object,java.lang.Object)

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual java::lang::Object* getNewValue(); // public java.lang.Object java.beans.PropertyChangeEvent.getNewValue()
    virtual java::lang::Object* getOldValue(); // public java.lang.Object java.beans.PropertyChangeEvent.getOldValue()
    virtual java::lang::Object* getPropagationId(); // public java.lang.Object java.beans.PropertyChangeEvent.getPropagationId()
    virtual const char* getPropertyName(); // public java.lang.String java.beans.PropertyChangeEvent.getPropertyName()
    virtual void setPropagationId(java::lang::Object* arg1); // public void java.beans.PropertyChangeEvent.setPropagationId(java.lang.Object)

};
}
}
#endif
