#ifndef loci_formats_StatusReporter_H
#define loci_formats_StatusReporter_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace loci {
namespace formats {
class StatusListener;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace loci {
namespace formats {
class StatusReporter : public java::lang::Object {
  public:
    StatusReporter(JavaMarker* dummy);
    StatusReporter(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual void addStatusListener(loci::formats::StatusListener* arg1); // public abstract void loci.formats.StatusReporter.addStatusListener(loci.formats.StatusListener)
    virtual void removeStatusListener(loci::formats::StatusListener* arg1); // public abstract void loci.formats.StatusReporter.removeStatusListener(loci.formats.StatusListener)
    virtual JavaObjectArray* getStatusListeners(); // public abstract loci.formats.StatusListener[] loci.formats.StatusReporter.getStatusListeners()

};
}
}
#endif
