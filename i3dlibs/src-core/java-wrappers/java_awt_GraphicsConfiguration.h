#ifndef java_awt_GraphicsConfiguration_H
#define java_awt_GraphicsConfiguration_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace awt {
namespace image {
class ColorModel;
}
}
}

namespace java {
namespace awt {
class Rectangle;
}
}

namespace java {
namespace awt {
namespace image {
class BufferedImage;
}
}
}

namespace java {
namespace awt {
namespace image {
class VolatileImage;
}
}
}

namespace java {
namespace awt {
class ImageCapabilities;
}
}

namespace java {
namespace awt {
class BufferCapabilities;
}
}

namespace java {
namespace awt {
namespace geom {
class AffineTransform;
}
}
}

namespace java {
namespace awt {
class GraphicsDevice;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
class GraphicsConfiguration : public java::lang::Object {
  public:
    GraphicsConfiguration(JavaMarker* dummy);
    GraphicsConfiguration(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual java::awt::image::ColorModel* getColorModel(int arg1); // public abstract java.awt.image.ColorModel java.awt.GraphicsConfiguration.getColorModel(int)
    virtual java::awt::image::ColorModel* getColorModel(); // public abstract java.awt.image.ColorModel java.awt.GraphicsConfiguration.getColorModel()
    virtual java::awt::Rectangle* getBounds(); // public abstract java.awt.Rectangle java.awt.GraphicsConfiguration.getBounds()
    virtual java::awt::image::BufferedImage* createCompatibleImage(int arg1, int arg2, int arg3); // public java.awt.image.BufferedImage java.awt.GraphicsConfiguration.createCompatibleImage(int,int,int)
    virtual java::awt::image::BufferedImage* createCompatibleImage(int arg1, int arg2); // public abstract java.awt.image.BufferedImage java.awt.GraphicsConfiguration.createCompatibleImage(int,int)
    virtual java::awt::image::VolatileImage* createCompatibleVolatileImage(int arg1, int arg2, java::awt::ImageCapabilities* arg3, int arg4); // public java.awt.image.VolatileImage java.awt.GraphicsConfiguration.createCompatibleVolatileImage(int,int,java.awt.ImageCapabilities,int) throws java.awt.AWTException
    virtual java::awt::image::VolatileImage* createCompatibleVolatileImage(int arg1, int arg2, java::awt::ImageCapabilities* arg3); // public java.awt.image.VolatileImage java.awt.GraphicsConfiguration.createCompatibleVolatileImage(int,int,java.awt.ImageCapabilities) throws java.awt.AWTException
    virtual java::awt::image::VolatileImage* createCompatibleVolatileImage(int arg1, int arg2, int arg3); // public java.awt.image.VolatileImage java.awt.GraphicsConfiguration.createCompatibleVolatileImage(int,int,int)
    virtual java::awt::image::VolatileImage* createCompatibleVolatileImage(int arg1, int arg2); // public java.awt.image.VolatileImage java.awt.GraphicsConfiguration.createCompatibleVolatileImage(int,int)
    virtual java::awt::BufferCapabilities* getBufferCapabilities(); // public java.awt.BufferCapabilities java.awt.GraphicsConfiguration.getBufferCapabilities()
    virtual java::awt::geom::AffineTransform* getDefaultTransform(); // public abstract java.awt.geom.AffineTransform java.awt.GraphicsConfiguration.getDefaultTransform()
    virtual java::awt::GraphicsDevice* getDevice(); // public abstract java.awt.GraphicsDevice java.awt.GraphicsConfiguration.getDevice()
    virtual java::awt::ImageCapabilities* getImageCapabilities(); // public java.awt.ImageCapabilities java.awt.GraphicsConfiguration.getImageCapabilities()
    virtual java::awt::geom::AffineTransform* getNormalizingTransform(); // public abstract java.awt.geom.AffineTransform java.awt.GraphicsConfiguration.getNormalizingTransform()

};
}
}
#endif
