#ifndef java_awt_image_RenderedImage_H
#define java_awt_image_RenderedImage_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace lang {
class String;
}
}

namespace java {
namespace awt {
namespace image {
class Raster;
}
}
}

namespace java {
namespace awt {
class Rectangle;
}
}

namespace java {
namespace awt {
namespace image {
class WritableRaster;
}
}
}

namespace java {
namespace awt {
namespace image {
class ColorModel;
}
}
}

namespace java {
namespace awt {
namespace image {
class SampleModel;
}
}
}

namespace java {
namespace util {
class Vector;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace image {
class RenderedImage : public java::lang::Object {
  public:
    RenderedImage(JavaMarker* dummy);
    RenderedImage(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual java::lang::Object* getProperty(const char* arg1); // public abstract java.lang.Object java.awt.image.RenderedImage.getProperty(java.lang.String)
    virtual java::awt::image::Raster* getData(java::awt::Rectangle* arg1); // public abstract java.awt.image.Raster java.awt.image.RenderedImage.getData(java.awt.Rectangle)
    virtual java::awt::image::Raster* getData(); // public abstract java.awt.image.Raster java.awt.image.RenderedImage.getData()
    virtual java::awt::image::WritableRaster* copyData(java::awt::image::WritableRaster* arg1); // public abstract java.awt.image.WritableRaster java.awt.image.RenderedImage.copyData(java.awt.image.WritableRaster)
    virtual java::awt::image::ColorModel* getColorModel(); // public abstract java.awt.image.ColorModel java.awt.image.RenderedImage.getColorModel()
    virtual int getHeight(); // public abstract int java.awt.image.RenderedImage.getHeight()
    virtual int getMinTileX(); // public abstract int java.awt.image.RenderedImage.getMinTileX()
    virtual int getMinTileY(); // public abstract int java.awt.image.RenderedImage.getMinTileY()
    virtual int getMinX(); // public abstract int java.awt.image.RenderedImage.getMinX()
    virtual int getMinY(); // public abstract int java.awt.image.RenderedImage.getMinY()
    virtual int getNumXTiles(); // public abstract int java.awt.image.RenderedImage.getNumXTiles()
    virtual int getNumYTiles(); // public abstract int java.awt.image.RenderedImage.getNumYTiles()
    virtual JavaObjectArray* getPropertyNames(); // public abstract java.lang.String[] java.awt.image.RenderedImage.getPropertyNames()
    virtual java::awt::image::SampleModel* getSampleModel(); // public abstract java.awt.image.SampleModel java.awt.image.RenderedImage.getSampleModel()
    virtual java::util::Vector* getSources(); // public abstract java.util.Vector java.awt.image.RenderedImage.getSources()
    virtual java::awt::image::Raster* getTile(int arg1, int arg2); // public abstract java.awt.image.Raster java.awt.image.RenderedImage.getTile(int,int)
    virtual int getTileGridXOffset(); // public abstract int java.awt.image.RenderedImage.getTileGridXOffset()
    virtual int getTileGridYOffset(); // public abstract int java.awt.image.RenderedImage.getTileGridYOffset()
    virtual int getTileHeight(); // public abstract int java.awt.image.RenderedImage.getTileHeight()
    virtual int getTileWidth(); // public abstract int java.awt.image.RenderedImage.getTileWidth()
    virtual int getWidth(); // public abstract int java.awt.image.RenderedImage.getWidth()

};
}
}
}
#endif
