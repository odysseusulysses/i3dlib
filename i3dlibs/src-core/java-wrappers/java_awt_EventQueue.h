#ifndef java_awt_EventQueue_H
#define java_awt_EventQueue_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace lang {
class Runnable;
}
}

namespace java {
namespace awt {
class AWTEvent;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
class EventQueue : public java::lang::Object {
  public:
    EventQueue(JavaMarker* dummy);
    EventQueue(jobject obj);
    EventQueue(); // public java.awt.EventQueue()

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual void push(java::awt::EventQueue* arg1); // public synchronized void java.awt.EventQueue.push(java.awt.EventQueue)
    virtual void invokeAndWait(java::lang::Runnable* arg1); // public static void java.awt.EventQueue.invokeAndWait(java.lang.Runnable) throws java.lang.InterruptedException,java.lang.reflect.InvocationTargetException
    virtual bool isDispatchThread(); // public static boolean java.awt.EventQueue.isDispatchThread()
    virtual void postEvent(java::awt::AWTEvent* arg1); // public void java.awt.EventQueue.postEvent(java.awt.AWTEvent)
    virtual long getMostRecentEventTime(); // public static long java.awt.EventQueue.getMostRecentEventTime()
    virtual void invokeLater(java::lang::Runnable* arg1); // public static void java.awt.EventQueue.invokeLater(java.lang.Runnable)
    virtual java::awt::AWTEvent* getCurrentEvent(); // public static java.awt.AWTEvent java.awt.EventQueue.getCurrentEvent()
    virtual java::awt::AWTEvent* getNextEvent(); // public java.awt.AWTEvent java.awt.EventQueue.getNextEvent() throws java.lang.InterruptedException
    virtual java::awt::AWTEvent* peekEvent(int arg1); // public synchronized java.awt.AWTEvent java.awt.EventQueue.peekEvent(int)
    virtual java::awt::AWTEvent* peekEvent(); // public synchronized java.awt.AWTEvent java.awt.EventQueue.peekEvent()

};
}
}
#endif
