#ifndef java_awt_PageAttributes__PrintQualityType_H
#define java_awt_PageAttributes__PrintQualityType_H
#include <jni.h>
#include <java_marker.h>
#include <java_awt_AttributeValue.h>

namespace java {
namespace lang {
class String;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
class PageAttributes__PrintQualityType : public java::awt::AttributeValue {
  public:
    PageAttributes__PrintQualityType(JavaMarker* dummy);
    PageAttributes__PrintQualityType(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual int hashCode(); // public int java.awt.PageAttributes$PrintQualityType.hashCode()
    virtual const char* toString(); // public java.lang.String java.awt.PageAttributes$PrintQualityType.toString()

    java::awt::PageAttributes__PrintQualityType* HIGH; // public static final java.awt.PageAttributes$PrintQualityType java.awt.PageAttributes$PrintQualityType.HIGH
    java::awt::PageAttributes__PrintQualityType* NORMAL; // public static final java.awt.PageAttributes$PrintQualityType java.awt.PageAttributes$PrintQualityType.NORMAL
    java::awt::PageAttributes__PrintQualityType* DRAFT; // public static final java.awt.PageAttributes$PrintQualityType java.awt.PageAttributes$PrintQualityType.DRAFT
};
}
}
#endif
