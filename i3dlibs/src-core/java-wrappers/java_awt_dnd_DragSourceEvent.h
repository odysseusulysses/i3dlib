#ifndef java_awt_dnd_DragSourceEvent_H
#define java_awt_dnd_DragSourceEvent_H
#include <jni.h>
#include <java_marker.h>
#include <java_util_EventObject.h>

namespace java {
namespace awt {
namespace dnd {
class DragSourceContext;
}
}
}

namespace java {
namespace awt {
class Point;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace dnd {
class DragSourceEvent : public java::util::EventObject {
  public:
    DragSourceEvent(JavaMarker* dummy);
    DragSourceEvent(jobject obj);
    DragSourceEvent(java::awt::dnd::DragSourceContext* arg1); // public java.awt.dnd.DragSourceEvent(java.awt.dnd.DragSourceContext)
    DragSourceEvent(java::awt::dnd::DragSourceContext* arg1, int arg2, int arg3); // public java.awt.dnd.DragSourceEvent(java.awt.dnd.DragSourceContext,int,int)

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual java::awt::Point* getLocation(); // public java.awt.Point java.awt.dnd.DragSourceEvent.getLocation()
    virtual int getX(); // public int java.awt.dnd.DragSourceEvent.getX()
    virtual int getY(); // public int java.awt.dnd.DragSourceEvent.getY()
    virtual java::awt::dnd::DragSourceContext* getDragSourceContext(); // public java.awt.dnd.DragSourceContext java.awt.dnd.DragSourceEvent.getDragSourceContext()

};
}
}
}
#endif
