#ifndef java_awt_geom_Point2D_H
#define java_awt_geom_Point2D_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace lang {
class Cloneable;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace geom {
class Point2D : public java::lang::Object {
  public:
    Point2D(JavaMarker* dummy);
    Point2D(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual int hashCode(); // public int java.awt.geom.Point2D.hashCode()
    virtual java::lang::Object* clone(); // public java.lang.Object java.awt.geom.Point2D.clone()
    virtual bool equals(java::lang::Object* arg1); // public boolean java.awt.geom.Point2D.equals(java.lang.Object)
    virtual double getX(); // public abstract double java.awt.geom.Point2D.getX()
    virtual double getY(); // public abstract double java.awt.geom.Point2D.getY()
    virtual void setLocation(double arg1, double arg2); // public abstract void java.awt.geom.Point2D.setLocation(double,double)
    virtual void setLocation(java::awt::geom::Point2D* arg1); // public void java.awt.geom.Point2D.setLocation(java.awt.geom.Point2D)
    virtual double distance(double arg1, double arg2, double arg3, double arg4); // public static double java.awt.geom.Point2D.distance(double,double,double,double)
    virtual double distance(double arg1, double arg2); // public double java.awt.geom.Point2D.distance(double,double)
    virtual double distance(java::awt::geom::Point2D* arg1); // public double java.awt.geom.Point2D.distance(java.awt.geom.Point2D)
    virtual double distanceSq(double arg1, double arg2, double arg3, double arg4); // public static double java.awt.geom.Point2D.distanceSq(double,double,double,double)
    virtual double distanceSq(double arg1, double arg2); // public double java.awt.geom.Point2D.distanceSq(double,double)
    virtual double distanceSq(java::awt::geom::Point2D* arg1); // public double java.awt.geom.Point2D.distanceSq(java.awt.geom.Point2D)

};
}
}
}
#endif
