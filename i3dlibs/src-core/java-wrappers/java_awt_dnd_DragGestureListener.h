#ifndef java_awt_dnd_DragGestureListener_H
#define java_awt_dnd_DragGestureListener_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace util {
class EventListener;
}
}

namespace java {
namespace awt {
namespace dnd {
class DragGestureEvent;
}
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace dnd {
class DragGestureListener : public java::lang::Object {
  public:
    DragGestureListener(JavaMarker* dummy);
    DragGestureListener(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual void dragGestureRecognized(java::awt::dnd::DragGestureEvent* arg1); // public abstract void java.awt.dnd.DragGestureListener.dragGestureRecognized(java.awt.dnd.DragGestureEvent)

};
}
}
}
#endif
