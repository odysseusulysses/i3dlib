#ifndef java_awt_JobAttributes__MultipleDocumentHandlingType_H
#define java_awt_JobAttributes__MultipleDocumentHandlingType_H
#include <jni.h>
#include <java_marker.h>
#include <java_awt_AttributeValue.h>

namespace java {
namespace lang {
class String;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
class JobAttributes__MultipleDocumentHandlingType : public java::awt::AttributeValue {
  public:
    JobAttributes__MultipleDocumentHandlingType(JavaMarker* dummy);
    JobAttributes__MultipleDocumentHandlingType(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual int hashCode(); // public int java.awt.JobAttributes$MultipleDocumentHandlingType.hashCode()
    virtual const char* toString(); // public java.lang.String java.awt.JobAttributes$MultipleDocumentHandlingType.toString()

    java::awt::JobAttributes__MultipleDocumentHandlingType* SEPARATE_DOCUMENTS_COLLATED_COPIES; // public static final java.awt.JobAttributes$MultipleDocumentHandlingType java.awt.JobAttributes$MultipleDocumentHandlingType.SEPARATE_DOCUMENTS_COLLATED_COPIES
    java::awt::JobAttributes__MultipleDocumentHandlingType* SEPARATE_DOCUMENTS_UNCOLLATED_COPIES; // public static final java.awt.JobAttributes$MultipleDocumentHandlingType java.awt.JobAttributes$MultipleDocumentHandlingType.SEPARATE_DOCUMENTS_UNCOLLATED_COPIES
};
}
}
#endif
