#ifndef java_awt_image_ImageObserver_H
#define java_awt_image_ImageObserver_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace awt {
class Image;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace image {
class ImageObserver : public java::lang::Object {
  public:
    ImageObserver(JavaMarker* dummy);
    ImageObserver(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual bool imageUpdate(java::awt::Image* arg1, int arg2, int arg3, int arg4, int arg5, int arg6); // public abstract boolean java.awt.image.ImageObserver.imageUpdate(java.awt.Image,int,int,int,int,int)

    int WIDTH; // public static final int java.awt.image.ImageObserver.WIDTH
    int HEIGHT; // public static final int java.awt.image.ImageObserver.HEIGHT
    int PROPERTIES; // public static final int java.awt.image.ImageObserver.PROPERTIES
    int SOMEBITS; // public static final int java.awt.image.ImageObserver.SOMEBITS
    int FRAMEBITS; // public static final int java.awt.image.ImageObserver.FRAMEBITS
    int ALLBITS; // public static final int java.awt.image.ImageObserver.ALLBITS
    int ERROR; // public static final int java.awt.image.ImageObserver.ERROR
    int ABORT; // public static final int java.awt.image.ImageObserver.ABORT
};
}
}
}
#endif
