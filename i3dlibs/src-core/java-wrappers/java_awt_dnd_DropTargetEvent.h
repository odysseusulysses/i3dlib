#ifndef java_awt_dnd_DropTargetEvent_H
#define java_awt_dnd_DropTargetEvent_H
#include <jni.h>
#include <java_marker.h>
#include <java_util_EventObject.h>

namespace java {
namespace awt {
namespace dnd {
class DropTargetContext;
}
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace dnd {
class DropTargetEvent : public java::util::EventObject {
  public:
    DropTargetEvent(JavaMarker* dummy);
    DropTargetEvent(jobject obj);
    DropTargetEvent(java::awt::dnd::DropTargetContext* arg1); // public java.awt.dnd.DropTargetEvent(java.awt.dnd.DropTargetContext)

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual java::awt::dnd::DropTargetContext* getDropTargetContext(); // public java.awt.dnd.DropTargetContext java.awt.dnd.DropTargetEvent.getDropTargetContext()

};
}
}
}
#endif
