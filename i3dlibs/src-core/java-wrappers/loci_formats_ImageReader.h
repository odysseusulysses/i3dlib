#ifndef loci_formats_ImageReader_H
#define loci_formats_ImageReader_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace loci {
namespace formats {
class IFormatReader;
}
}

namespace loci {
namespace formats {
class ClassList;
}
}

namespace java {
namespace lang {
class String;
}
}

namespace java {
namespace awt {
namespace image {
class BufferedImage;
}
}
}

namespace loci {
namespace formats {
namespace meta {
class MetadataStore;
}
}
}

namespace java {
namespace util {
class Hashtable;
}
}

namespace loci {
namespace formats {
class CoreMetadata;
}
}

namespace loci {
namespace formats {
class StatusListener;
}
}

namespace java {
namespace lang {
class Class;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace loci {
namespace formats {
class ImageReader : public java::lang::Object {
  public:
    ImageReader(JavaMarker* dummy);
    ImageReader(jobject obj);
    ImageReader(); // public loci.formats.ImageReader()
    ImageReader(loci::formats::ClassList* arg1); // public loci.formats.ImageReader(loci.formats.ClassList)

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual void close(bool arg1); // public void loci.formats.ImageReader.close(boolean) throws java.io.IOException
    virtual void close(); // public void loci.formats.ImageReader.close() throws java.io.IOException
    virtual int getIndex(int arg1, int arg2, int arg3); // public int loci.formats.ImageReader.getIndex(int,int,int)
    virtual int getIndex(const char* arg1, int arg2, int arg3, int arg4); // public int loci.formats.ImageReader.getIndex(java.lang.String,int,int,int) throws loci.formats.FormatException,java.io.IOException
    virtual bool isThisType(JavaByteArray* arg1); // public boolean loci.formats.ImageReader.isThisType(byte[])
    virtual bool isThisType(const char* arg1); // public boolean loci.formats.ImageReader.isThisType(java.lang.String)
    virtual bool isThisType(const char* arg1, bool arg2); // public boolean loci.formats.ImageReader.isThisType(java.lang.String,boolean)
    virtual int fileGroupOption(const char* arg1); // public int loci.formats.ImageReader.fileGroupOption(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual JavaByteArray* openBytes(int arg1, JavaByteArray* arg2); // public byte[] loci.formats.ImageReader.openBytes(int,byte[]) throws loci.formats.FormatException,java.io.IOException
    virtual JavaByteArray* openBytes(int arg1, int arg2, int arg3, int arg4, int arg5); // public byte[] loci.formats.ImageReader.openBytes(int,int,int,int,int) throws loci.formats.FormatException,java.io.IOException
    virtual JavaByteArray* openBytes(int arg1); // public byte[] loci.formats.ImageReader.openBytes(int) throws loci.formats.FormatException,java.io.IOException
    virtual JavaByteArray* openBytes(const char* arg1, int arg2); // public byte[] loci.formats.ImageReader.openBytes(java.lang.String,int) throws loci.formats.FormatException,java.io.IOException
    virtual JavaByteArray* openBytes(int arg1, JavaByteArray* arg2, int arg3, int arg4, int arg5, int arg6); // public byte[] loci.formats.ImageReader.openBytes(int,byte[],int,int,int,int) throws loci.formats.FormatException,java.io.IOException
    virtual JavaByteArray* openBytes(const char* arg1, int arg2, JavaByteArray* arg3); // public byte[] loci.formats.ImageReader.openBytes(java.lang.String,int,byte[]) throws loci.formats.FormatException,java.io.IOException
    //virtual java::awt::image::BufferedImage* openThumbImage(const char* arg1, int arg2); // public java.awt.image.BufferedImage loci.formats.ImageReader.openThumbImage(java.lang.String,int) throws loci.formats.FormatException,java.io.IOException
    //virtual java::awt::image::BufferedImage* openThumbImage(int arg1); // public java.awt.image.BufferedImage loci.formats.ImageReader.openThumbImage(int) throws loci.formats.FormatException,java.io.IOException
    virtual JavaObjectArray* getUsedFiles(); // public java.lang.String[] loci.formats.ImageReader.getUsedFiles()
    virtual JavaObjectArray* getUsedFiles(const char* arg1); // public java.lang.String[] loci.formats.ImageReader.getUsedFiles(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual void setId(const char* arg1, bool arg2); // public void loci.formats.ImageReader.setId(java.lang.String,boolean) throws loci.formats.FormatException,java.io.IOException
    virtual void setId(const char* arg1); // public void loci.formats.ImageReader.setId(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    //virtual java::awt::image::BufferedImage* openImage(int arg1); // public java.awt.image.BufferedImage loci.formats.ImageReader.openImage(int) throws loci.formats.FormatException,java.io.IOException
    //virtual java::awt::image::BufferedImage* openImage(const char* arg1, int arg2); // public java.awt.image.BufferedImage loci.formats.ImageReader.openImage(java.lang.String,int) throws loci.formats.FormatException,java.io.IOException
    //virtual java::awt::image::BufferedImage* openImage(int arg1, int arg2, int arg3, int arg4, int arg5); // public java.awt.image.BufferedImage loci.formats.ImageReader.openImage(int,int,int,int,int) throws loci.formats.FormatException,java.io.IOException
    virtual int getSizeX(const char* arg1); // public int loci.formats.ImageReader.getSizeX(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual int getSizeX(); // public int loci.formats.ImageReader.getSizeX()
    virtual int getSizeY(); // public int loci.formats.ImageReader.getSizeY()
    virtual int getSizeY(const char* arg1); // public int loci.formats.ImageReader.getSizeY(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual loci::formats::meta::MetadataStore* getMetadataStore(); // public loci.formats.meta.MetadataStore loci.formats.ImageReader.getMetadataStore()
    virtual loci::formats::meta::MetadataStore* getMetadataStore(const char* arg1); // public loci.formats.meta.MetadataStore loci.formats.ImageReader.getMetadataStore(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual bool isMetadataFiltered(); // public boolean loci.formats.ImageReader.isMetadataFiltered()
    virtual int getImageCount(); // public int loci.formats.ImageReader.getImageCount()
    virtual int getImageCount(const char* arg1); // public int loci.formats.ImageReader.getImageCount(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual bool isRGB(const char* arg1); // public boolean loci.formats.ImageReader.isRGB(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual bool isRGB(); // public boolean loci.formats.ImageReader.isRGB()
    virtual int getSizeZ(); // public int loci.formats.ImageReader.getSizeZ()
    virtual int getSizeZ(const char* arg1); // public int loci.formats.ImageReader.getSizeZ(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual int getSizeC(const char* arg1); // public int loci.formats.ImageReader.getSizeC(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual int getSizeC(); // public int loci.formats.ImageReader.getSizeC()
    virtual int getSizeT(const char* arg1); // public int loci.formats.ImageReader.getSizeT(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual int getSizeT(); // public int loci.formats.ImageReader.getSizeT()
    virtual int getPixelType(const char* arg1); // public int loci.formats.ImageReader.getPixelType(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual int getPixelType(); // public int loci.formats.ImageReader.getPixelType()
    virtual int getEffectiveSizeC(); // public int loci.formats.ImageReader.getEffectiveSizeC()
    virtual int getEffectiveSizeC(const char* arg1); // public int loci.formats.ImageReader.getEffectiveSizeC(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual int getRGBChannelCount(); // public int loci.formats.ImageReader.getRGBChannelCount()
    virtual int getRGBChannelCount(const char* arg1); // public int loci.formats.ImageReader.getRGBChannelCount(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual bool isIndexed(); // public boolean loci.formats.ImageReader.isIndexed()
    virtual bool isFalseColor(); // public boolean loci.formats.ImageReader.isFalseColor()
    virtual JavaObjectArray* get8BitLookupTable(); // public byte[][] loci.formats.ImageReader.get8BitLookupTable() throws loci.formats.FormatException,java.io.IOException
    virtual JavaObjectArray* get16BitLookupTable(); // public short[][] loci.formats.ImageReader.get16BitLookupTable() throws loci.formats.FormatException,java.io.IOException
    virtual JavaIntArray* getChannelDimLengths(const char* arg1); // public int[] loci.formats.ImageReader.getChannelDimLengths(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual JavaIntArray* getChannelDimLengths(); // public int[] loci.formats.ImageReader.getChannelDimLengths()
    virtual JavaObjectArray* getChannelDimTypes(const char* arg1); // public java.lang.String[] loci.formats.ImageReader.getChannelDimTypes(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual JavaObjectArray* getChannelDimTypes(); // public java.lang.String[] loci.formats.ImageReader.getChannelDimTypes()
    virtual int getThumbSizeX(const char* arg1); // public int loci.formats.ImageReader.getThumbSizeX(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual int getThumbSizeX(); // public int loci.formats.ImageReader.getThumbSizeX()
    virtual int getThumbSizeY(); // public int loci.formats.ImageReader.getThumbSizeY()
    virtual int getThumbSizeY(const char* arg1); // public int loci.formats.ImageReader.getThumbSizeY(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual bool isLittleEndian(); // public boolean loci.formats.ImageReader.isLittleEndian()
    virtual bool isLittleEndian(const char* arg1); // public boolean loci.formats.ImageReader.isLittleEndian(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual const char* getDimensionOrder(); // public java.lang.String loci.formats.ImageReader.getDimensionOrder()
    virtual const char* getDimensionOrder(const char* arg1); // public java.lang.String loci.formats.ImageReader.getDimensionOrder(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual bool isOrderCertain(); // public boolean loci.formats.ImageReader.isOrderCertain()
    virtual bool isOrderCertain(const char* arg1); // public boolean loci.formats.ImageReader.isOrderCertain(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual bool isInterleaved(int arg1); // public boolean loci.formats.ImageReader.isInterleaved(int)
    virtual bool isInterleaved(const char* arg1, int arg2); // public boolean loci.formats.ImageReader.isInterleaved(java.lang.String,int) throws loci.formats.FormatException,java.io.IOException
    virtual bool isInterleaved(const char* arg1); // public boolean loci.formats.ImageReader.isInterleaved(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual bool isInterleaved(); // public boolean loci.formats.ImageReader.isInterleaved()
    virtual JavaByteArray* openThumbBytes(const char* arg1, int arg2); // public byte[] loci.formats.ImageReader.openThumbBytes(java.lang.String,int) throws loci.formats.FormatException,java.io.IOException
    virtual JavaByteArray* openThumbBytes(int arg1); // public byte[] loci.formats.ImageReader.openThumbBytes(int) throws loci.formats.FormatException,java.io.IOException
    virtual int getSeriesCount(const char* arg1); // public int loci.formats.ImageReader.getSeriesCount(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual int getSeriesCount(); // public int loci.formats.ImageReader.getSeriesCount()
    virtual void setSeries(int arg1); // public void loci.formats.ImageReader.setSeries(int)
    virtual void setSeries(const char* arg1, int arg2); // public void loci.formats.ImageReader.setSeries(java.lang.String,int) throws loci.formats.FormatException,java.io.IOException
    virtual int getSeries(); // public int loci.formats.ImageReader.getSeries()
    virtual int getSeries(const char* arg1); // public int loci.formats.ImageReader.getSeries(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual void setGroupFiles(bool arg1); // public void loci.formats.ImageReader.setGroupFiles(boolean)
    virtual bool isGroupFiles(); // public boolean loci.formats.ImageReader.isGroupFiles()
    virtual bool isMetadataComplete(); // public boolean loci.formats.ImageReader.isMetadataComplete()
    virtual void setNormalized(bool arg1); // public void loci.formats.ImageReader.setNormalized(boolean)
    virtual bool isNormalized(); // public boolean loci.formats.ImageReader.isNormalized()
    virtual void setMetadataCollected(bool arg1); // public void loci.formats.ImageReader.setMetadataCollected(boolean)
    virtual bool isMetadataCollected(); // public boolean loci.formats.ImageReader.isMetadataCollected()
    virtual void setOriginalMetadataPopulated(bool arg1); // public void loci.formats.ImageReader.setOriginalMetadataPopulated(boolean)
    virtual bool isOriginalMetadataPopulated(); // public boolean loci.formats.ImageReader.isOriginalMetadataPopulated()
    virtual const char* getCurrentFile(); // public java.lang.String loci.formats.ImageReader.getCurrentFile()
    virtual JavaIntArray* getZCTCoords(int arg1); // public int[] loci.formats.ImageReader.getZCTCoords(int)
    virtual JavaIntArray* getZCTCoords(const char* arg1, int arg2); // public int[] loci.formats.ImageReader.getZCTCoords(java.lang.String,int) throws loci.formats.FormatException,java.io.IOException
    virtual java::lang::Object* getMetadataValue(const char* arg1, const char* arg2); // public java.lang.Object loci.formats.ImageReader.getMetadataValue(java.lang.String,java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual java::lang::Object* getMetadataValue(const char* arg1); // public java.lang.Object loci.formats.ImageReader.getMetadataValue(java.lang.String)
    //virtual java::util::Hashtable* getMetadata(const char* arg1); // public java.util.Hashtable loci.formats.ImageReader.getMetadata(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    //virtual java::util::Hashtable* getMetadata(); // public java.util.Hashtable loci.formats.ImageReader.getMetadata()
    virtual loci::formats::CoreMetadata* getCoreMetadata(); // public loci.formats.CoreMetadata loci.formats.ImageReader.getCoreMetadata()
    virtual loci::formats::CoreMetadata* getCoreMetadata(const char* arg1); // public loci.formats.CoreMetadata loci.formats.ImageReader.getCoreMetadata(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual void setMetadataFiltered(bool arg1); // public void loci.formats.ImageReader.setMetadataFiltered(boolean)
    virtual void setMetadataStore(loci::formats::meta::MetadataStore* arg1); // public void loci.formats.ImageReader.setMetadataStore(loci.formats.meta.MetadataStore)
    virtual java::lang::Object* getMetadataStoreRoot(); // public java.lang.Object loci.formats.ImageReader.getMetadataStoreRoot()
    virtual java::lang::Object* getMetadataStoreRoot(const char* arg1); // public java.lang.Object loci.formats.ImageReader.getMetadataStoreRoot(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual JavaObjectArray* getUnderlyingReaders(); // public loci.formats.IFormatReader[] loci.formats.ImageReader.getUnderlyingReaders()
    virtual const char* getFormat(const char* arg1); // public java.lang.String loci.formats.ImageReader.getFormat(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual const char* getFormat(); // public java.lang.String loci.formats.ImageReader.getFormat()
    virtual JavaObjectArray* getSuffixes(); // public java.lang.String[] loci.formats.ImageReader.getSuffixes()
    virtual void addStatusListener(loci::formats::StatusListener* arg1); // public void loci.formats.ImageReader.addStatusListener(loci.formats.StatusListener)
    virtual void removeStatusListener(loci::formats::StatusListener* arg1); // public void loci.formats.ImageReader.removeStatusListener(loci.formats.StatusListener)
    virtual JavaObjectArray* getStatusListeners(); // public loci.formats.StatusListener[] loci.formats.ImageReader.getStatusListeners()
    virtual loci::formats::IFormatReader* getReader(java::lang::Class* arg1); // public loci.formats.IFormatReader loci.formats.ImageReader.getReader(java.lang.Class)
    virtual loci::formats::IFormatReader* getReader(); // public loci.formats.IFormatReader loci.formats.ImageReader.getReader()
    virtual loci::formats::IFormatReader* getReader(const char* arg1); // public loci.formats.IFormatReader loci.formats.ImageReader.getReader(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual JavaObjectArray* getReaders(); // public loci.formats.IFormatReader[] loci.formats.ImageReader.getReaders()

};
}
}
#endif
