#ifndef loci_formats_FormatTools_H
#define loci_formats_FormatTools_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace loci {
namespace formats {
class IFormatReader;
}
}

namespace java {
namespace lang {
class String;
}
}

namespace java {
namespace lang {
class Class;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace loci {
namespace formats {
class FormatTools : public java::lang::Object {
  public:
    FormatTools(JavaMarker* dummy);
    FormatTools(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual int getIndex(loci::formats::IFormatReader* arg1, int arg2, int arg3, int arg4); // public static int loci.formats.FormatTools.getIndex(loci.formats.IFormatReader,int,int,int)
    virtual int getIndex(const char* arg1, int arg2, int arg3, int arg4, int arg5, int arg6, int arg7, int arg8); // public static int loci.formats.FormatTools.getIndex(java.lang.String,int,int,int,int,int,int,int)
    virtual void assertId(const char* arg1, bool arg2, int arg3); // public static void loci.formats.FormatTools.assertId(java.lang.String,boolean,int)
    virtual void checkPlaneNumber(loci::formats::IFormatReader* arg1, int arg2); // public static void loci.formats.FormatTools.checkPlaneNumber(loci.formats.IFormatReader,int) throws loci.formats.FormatException
    virtual JavaIntArray* getZCTCoords(const char* arg1, int arg2, int arg3, int arg4, int arg5, int arg6); // public static int[] loci.formats.FormatTools.getZCTCoords(java.lang.String,int,int,int,int,int)
    virtual JavaIntArray* getZCTCoords(loci::formats::IFormatReader* arg1, int arg2); // public static int[] loci.formats.FormatTools.getZCTCoords(loci.formats.IFormatReader,int)
    virtual int getBytesPerPixel(int arg1); // public static int loci.formats.FormatTools.getBytesPerPixel(int)
    virtual loci::formats::IFormatReader* getReader(loci::formats::IFormatReader* arg1, java::lang::Class* arg2); // public static loci.formats.IFormatReader loci.formats.FormatTools.getReader(loci.formats.IFormatReader,java.lang.Class)
    virtual const char* getPixelTypeString(int arg1); // public static java.lang.String loci.formats.FormatTools.getPixelTypeString(int)
    virtual int getReorderedIndex(const char* arg1, const char* arg2, int arg3, int arg4, int arg5, int arg6, int arg7); // public static int loci.formats.FormatTools.getReorderedIndex(java.lang.String,java.lang.String,int,int,int,int,int)
    virtual int getReorderedIndex(loci::formats::IFormatReader* arg1, const char* arg2, int arg3); // public static int loci.formats.FormatTools.getReorderedIndex(loci.formats.IFormatReader,java.lang.String,int) throws loci.formats.FormatException
    virtual int positionToRaster(JavaIntArray* arg1, JavaIntArray* arg2); // public static int loci.formats.FormatTools.positionToRaster(int[],int[])
    virtual JavaIntArray* rasterToPosition(JavaIntArray* arg1, int arg2, JavaIntArray* arg3); // public static int[] loci.formats.FormatTools.rasterToPosition(int[],int,int[])
    virtual JavaIntArray* rasterToPosition(JavaIntArray* arg1, int arg2); // public static int[] loci.formats.FormatTools.rasterToPosition(int[],int)
    virtual int getRasterLength(JavaIntArray* arg1); // public static int loci.formats.FormatTools.getRasterLength(int[])
    virtual int pixelTypeFromString(const char* arg1); // public static int loci.formats.FormatTools.pixelTypeFromString(java.lang.String)
    virtual void checkBufferSize(loci::formats::IFormatReader* arg1, int arg2); // public static void loci.formats.FormatTools.checkBufferSize(loci.formats.IFormatReader,int) throws loci.formats.FormatException
    virtual void checkBufferSize(loci::formats::IFormatReader* arg1, int arg2, int arg3, int arg4); // public static void loci.formats.FormatTools.checkBufferSize(loci.formats.IFormatReader,int,int,int) throws loci.formats.FormatException

    int INT8; // public static final int loci.formats.FormatTools.INT8
    int UINT8; // public static final int loci.formats.FormatTools.UINT8
    int INT16; // public static final int loci.formats.FormatTools.INT16
    int UINT16; // public static final int loci.formats.FormatTools.UINT16
    int INT32; // public static final int loci.formats.FormatTools.INT32
    int UINT32; // public static final int loci.formats.FormatTools.UINT32
    int FLOAT; // public static final int loci.formats.FormatTools.FLOAT
    int DOUBLE; // public static final int loci.formats.FormatTools.DOUBLE
    const char* CHANNEL; // public static final java.lang.String loci.formats.FormatTools.CHANNEL
    const char* SPECTRA; // public static final java.lang.String loci.formats.FormatTools.SPECTRA
    const char* LIFETIME; // public static final java.lang.String loci.formats.FormatTools.LIFETIME
    const char* POLARIZATION; // public static final java.lang.String loci.formats.FormatTools.POLARIZATION
    int MUST_GROUP; // public static final int loci.formats.FormatTools.MUST_GROUP
    int CAN_GROUP; // public static final int loci.formats.FormatTools.CAN_GROUP
    int CANNOT_GROUP; // public static final int loci.formats.FormatTools.CANNOT_GROUP
};
}
}
#endif
