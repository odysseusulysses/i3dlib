#ifndef loci_formats_ome_OMEXMLMetadata_H
#define loci_formats_ome_OMEXMLMetadata_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace loci {
namespace formats {
namespace meta {
class MetadataStore;
}
}
}

namespace loci {
namespace formats {
namespace meta {
class MetadataRetrieve;
}
}
}

namespace java {
namespace lang {
class String;
}
}

namespace java {
namespace lang {
class Double;
}
}

namespace java {
namespace lang {
class Integer;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace loci {
namespace formats {
namespace ome {
class OMEXMLMetadata : public java::lang::Object {
  public:
    OMEXMLMetadata(JavaMarker* dummy);
    OMEXMLMetadata(jobject obj);
    OMEXMLMetadata(); // public loci.formats.ome.OMEXMLMetadata()

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual void populateOriginalMetadata(const char* arg1, const char* arg2); // public void loci.formats.ome.OMEXMLMetadata.populateOriginalMetadata(java.lang.String,java.lang.String)
    virtual java::lang::Object* getRoot(); // public java.lang.Object loci.formats.ome.OMEXMLMetadata.getRoot()
    virtual const char* dumpXML(); // public abstract java.lang.String loci.formats.ome.OMEXMLMetadata.dumpXML()
    virtual java::lang::Double* getGlobalMin(java::lang::Integer* arg1, java::lang::Integer* arg2); // public java.lang.Double loci.formats.ome.OMEXMLMetadata.getGlobalMin(java.lang.Integer,java.lang.Integer)
    virtual java::lang::Double* getGlobalMax(java::lang::Integer* arg1, java::lang::Integer* arg2); // public java.lang.Double loci.formats.ome.OMEXMLMetadata.getGlobalMax(java.lang.Integer,java.lang.Integer)
    virtual void setChannelGlobalMinMax(int arg1, java::lang::Double* arg2, java::lang::Double* arg3, java::lang::Integer* arg4); // public void loci.formats.ome.OMEXMLMetadata.setChannelGlobalMinMax(int,java.lang.Double,java.lang.Double,java.lang.Integer)

};
}
}
}
#endif
