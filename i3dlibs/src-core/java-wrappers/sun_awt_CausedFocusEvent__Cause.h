#ifndef sun_awt_CausedFocusEvent__Cause_H
#define sun_awt_CausedFocusEvent__Cause_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Enum.h>

namespace java {
namespace lang {
class String;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace sun {
namespace awt {
class CausedFocusEvent__Cause : public java::lang::Enum {
  public:
    CausedFocusEvent__Cause(JavaMarker* dummy);
    CausedFocusEvent__Cause(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual sun::awt::CausedFocusEvent__Cause* valueOf(const char* arg1); // public static sun.awt.CausedFocusEvent$Cause sun.awt.CausedFocusEvent$Cause.valueOf(java.lang.String)
    virtual JavaObjectArray* values(); // public static sun.awt.CausedFocusEvent$Cause[] sun.awt.CausedFocusEvent$Cause.values()

    sun::awt::CausedFocusEvent__Cause* UNKNOWN; // public static final sun.awt.CausedFocusEvent$Cause sun.awt.CausedFocusEvent$Cause.UNKNOWN
    sun::awt::CausedFocusEvent__Cause* MOUSE_EVENT; // public static final sun.awt.CausedFocusEvent$Cause sun.awt.CausedFocusEvent$Cause.MOUSE_EVENT
    sun::awt::CausedFocusEvent__Cause* TRAVERSAL; // public static final sun.awt.CausedFocusEvent$Cause sun.awt.CausedFocusEvent$Cause.TRAVERSAL
    sun::awt::CausedFocusEvent__Cause* TRAVERSAL_UP; // public static final sun.awt.CausedFocusEvent$Cause sun.awt.CausedFocusEvent$Cause.TRAVERSAL_UP
    sun::awt::CausedFocusEvent__Cause* TRAVERSAL_DOWN; // public static final sun.awt.CausedFocusEvent$Cause sun.awt.CausedFocusEvent$Cause.TRAVERSAL_DOWN
    sun::awt::CausedFocusEvent__Cause* TRAVERSAL_FORWARD; // public static final sun.awt.CausedFocusEvent$Cause sun.awt.CausedFocusEvent$Cause.TRAVERSAL_FORWARD
    sun::awt::CausedFocusEvent__Cause* TRAVERSAL_BACKWARD; // public static final sun.awt.CausedFocusEvent$Cause sun.awt.CausedFocusEvent$Cause.TRAVERSAL_BACKWARD
    sun::awt::CausedFocusEvent__Cause* MANUAL_REQUEST; // public static final sun.awt.CausedFocusEvent$Cause sun.awt.CausedFocusEvent$Cause.MANUAL_REQUEST
    sun::awt::CausedFocusEvent__Cause* AUTOMATIC_TRAVERSE; // public static final sun.awt.CausedFocusEvent$Cause sun.awt.CausedFocusEvent$Cause.AUTOMATIC_TRAVERSE
    sun::awt::CausedFocusEvent__Cause* ROLLBACK; // public static final sun.awt.CausedFocusEvent$Cause sun.awt.CausedFocusEvent$Cause.ROLLBACK
    sun::awt::CausedFocusEvent__Cause* NATIVE_SYSTEM; // public static final sun.awt.CausedFocusEvent$Cause sun.awt.CausedFocusEvent$Cause.NATIVE_SYSTEM
    sun::awt::CausedFocusEvent__Cause* ACTIVATION; // public static final sun.awt.CausedFocusEvent$Cause sun.awt.CausedFocusEvent$Cause.ACTIVATION
    sun::awt::CausedFocusEvent__Cause* CLEAR_GLOBAL_FOCUS_OWNER; // public static final sun.awt.CausedFocusEvent$Cause sun.awt.CausedFocusEvent$Cause.CLEAR_GLOBAL_FOCUS_OWNER
    sun::awt::CausedFocusEvent__Cause* RETARGETED; // public static final sun.awt.CausedFocusEvent$Cause sun.awt.CausedFocusEvent$Cause.RETARGETED
};
}
}
#endif
