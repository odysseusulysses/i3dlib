#ifndef java_awt_Transparency_H
#define java_awt_Transparency_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
class Transparency : public java::lang::Object {
  public:
    Transparency(JavaMarker* dummy);
    Transparency(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual int getTransparency(); // public abstract int java.awt.Transparency.getTransparency()

    int OPAQUE; // public static final int java.awt.Transparency.OPAQUE
    int BITMASK; // public static final int java.awt.Transparency.BITMASK
    int TRANSLUCENT; // public static final int java.awt.Transparency.TRANSLUCENT
};
}
}
#endif
