#ifndef java_net_InetAddress_H
#define java_net_InetAddress_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace io {
class Serializable;
}
}

namespace java {
namespace lang {
class String;
}
}

namespace java {
namespace net {
class NetworkInterface;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace net {
class InetAddress : public java::lang::Object {
  public:
    InetAddress(JavaMarker* dummy);
    InetAddress(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual int hashCode(); // public int java.net.InetAddress.hashCode()
    virtual JavaByteArray* getAddress(); // public byte[] java.net.InetAddress.getAddress()
    virtual bool equals(java::lang::Object* arg1); // public boolean java.net.InetAddress.equals(java.lang.Object)
    virtual const char* toString(); // public java.lang.String java.net.InetAddress.toString()
    virtual const char* getHostAddress(); // public java.lang.String java.net.InetAddress.getHostAddress()
    virtual const char* getHostName(); // public java.lang.String java.net.InetAddress.getHostName()
    virtual java::net::InetAddress* getByName(const char* arg1); // public static java.net.InetAddress java.net.InetAddress.getByName(java.lang.String) throws java.net.UnknownHostException
    virtual JavaObjectArray* getAllByName(const char* arg1); // public static java.net.InetAddress[] java.net.InetAddress.getAllByName(java.lang.String) throws java.net.UnknownHostException
    virtual java::net::InetAddress* getByAddress(JavaByteArray* arg1); // public static java.net.InetAddress java.net.InetAddress.getByAddress(byte[]) throws java.net.UnknownHostException
    virtual java::net::InetAddress* getByAddress(const char* arg1, JavaByteArray* arg2); // public static java.net.InetAddress java.net.InetAddress.getByAddress(java.lang.String,byte[]) throws java.net.UnknownHostException
    virtual const char* getCanonicalHostName(); // public java.lang.String java.net.InetAddress.getCanonicalHostName()
    virtual java::net::InetAddress* getLocalHost(); // public static java.net.InetAddress java.net.InetAddress.getLocalHost() throws java.net.UnknownHostException
    virtual bool isAnyLocalAddress(); // public boolean java.net.InetAddress.isAnyLocalAddress()
    virtual bool isLinkLocalAddress(); // public boolean java.net.InetAddress.isLinkLocalAddress()
    virtual bool isLoopbackAddress(); // public boolean java.net.InetAddress.isLoopbackAddress()
    virtual bool isMCGlobal(); // public boolean java.net.InetAddress.isMCGlobal()
    virtual bool isMCLinkLocal(); // public boolean java.net.InetAddress.isMCLinkLocal()
    virtual bool isMCNodeLocal(); // public boolean java.net.InetAddress.isMCNodeLocal()
    virtual bool isMCOrgLocal(); // public boolean java.net.InetAddress.isMCOrgLocal()
    virtual bool isMCSiteLocal(); // public boolean java.net.InetAddress.isMCSiteLocal()
    virtual bool isMulticastAddress(); // public boolean java.net.InetAddress.isMulticastAddress()
    virtual bool isReachable(java::net::NetworkInterface* arg1, int arg2, int arg3); // public boolean java.net.InetAddress.isReachable(java.net.NetworkInterface,int,int) throws java.io.IOException
    virtual bool isReachable(int arg1); // public boolean java.net.InetAddress.isReachable(int) throws java.io.IOException
    virtual bool isSiteLocalAddress(); // public boolean java.net.InetAddress.isSiteLocalAddress()

};
}
}
#endif
