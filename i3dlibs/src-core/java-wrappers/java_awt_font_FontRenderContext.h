#ifndef java_awt_font_FontRenderContext_H
#define java_awt_font_FontRenderContext_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace awt {
namespace geom {
class AffineTransform;
}
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace font {
class FontRenderContext : public java::lang::Object {
  public:
    FontRenderContext(JavaMarker* dummy);
    FontRenderContext(jobject obj);
    FontRenderContext(java::awt::geom::AffineTransform* arg1, bool arg2, bool arg3); // public java.awt.font.FontRenderContext(java.awt.geom.AffineTransform,boolean,boolean)
    FontRenderContext(java::awt::geom::AffineTransform* arg1, java::lang::Object* arg2, java::lang::Object* arg3); // public java.awt.font.FontRenderContext(java.awt.geom.AffineTransform,java.lang.Object,java.lang.Object)

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual int hashCode(); // public int java.awt.font.FontRenderContext.hashCode()
    virtual bool equals(java::awt::font::FontRenderContext* arg1); // public boolean java.awt.font.FontRenderContext.equals(java.awt.font.FontRenderContext)
    virtual bool equals(java::lang::Object* arg1); // public boolean java.awt.font.FontRenderContext.equals(java.lang.Object)
    virtual java::awt::geom::AffineTransform* getTransform(); // public java.awt.geom.AffineTransform java.awt.font.FontRenderContext.getTransform()
    virtual java::lang::Object* getAntiAliasingHint(); // public java.lang.Object java.awt.font.FontRenderContext.getAntiAliasingHint()
    virtual java::lang::Object* getFractionalMetricsHint(); // public java.lang.Object java.awt.font.FontRenderContext.getFractionalMetricsHint()
    virtual int getTransformType(); // public int java.awt.font.FontRenderContext.getTransformType()
    virtual bool isAntiAliased(); // public boolean java.awt.font.FontRenderContext.isAntiAliased()
    virtual bool isTransformed(); // public boolean java.awt.font.FontRenderContext.isTransformed()
    virtual bool usesFractionalMetrics(); // public boolean java.awt.font.FontRenderContext.usesFractionalMetrics()

};
}
}
}
#endif
