#ifndef loci_formats_FormatReader_H
#define loci_formats_FormatReader_H
#include <jni.h>
#include <java_marker.h>
#include <loci_formats_FormatHandler.h>

namespace loci {
namespace formats {
class IFormatReader;
}
}

namespace java {
namespace lang {
class String;
}
}

namespace java {
namespace awt {
namespace image {
class BufferedImage;
}
}
}

namespace loci {
namespace formats {
namespace meta {
class MetadataStore;
}
}
}

namespace java {
namespace lang {
class Object;
}
}

namespace java {
namespace util {
class Hashtable;
}
}

namespace loci {
namespace formats {
class CoreMetadata;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace loci {
namespace formats {
class FormatReader : public loci::formats::FormatHandler {
  public:
    FormatReader(JavaMarker* dummy);
    FormatReader(jobject obj);
    FormatReader(const char* arg1, JavaObjectArray* arg2); // public loci.formats.FormatReader(java.lang.String,java.lang.String[])
    FormatReader(const char* arg1, const char* arg2); // public loci.formats.FormatReader(java.lang.String,java.lang.String)

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual void close(bool arg1); // public void loci.formats.FormatReader.close(boolean) throws java.io.IOException
    virtual void close(); // public void loci.formats.FormatReader.close() throws java.io.IOException
    virtual int getIndex(int arg1, int arg2, int arg3); // public int loci.formats.FormatReader.getIndex(int,int,int)
    virtual int getIndex(const char* arg1, int arg2, int arg3, int arg4); // public int loci.formats.FormatReader.getIndex(java.lang.String,int,int,int) throws loci.formats.FormatException,java.io.IOException
    virtual int fileGroupOption(const char* arg1); // public int loci.formats.FormatReader.fileGroupOption(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual JavaByteArray* openBytes(const char* arg1, int arg2); // public byte[] loci.formats.FormatReader.openBytes(java.lang.String,int) throws loci.formats.FormatException,java.io.IOException
    virtual JavaByteArray* openBytes(int arg1, JavaByteArray* arg2); // public byte[] loci.formats.FormatReader.openBytes(int,byte[]) throws loci.formats.FormatException,java.io.IOException
    virtual JavaByteArray* openBytes(int arg1); // public byte[] loci.formats.FormatReader.openBytes(int) throws loci.formats.FormatException,java.io.IOException
    virtual JavaByteArray* openBytes(int arg1, JavaByteArray* arg2, int arg3, int arg4, int arg5, int arg6); // public abstract byte[] loci.formats.FormatReader.openBytes(int,byte[],int,int,int,int) throws loci.formats.FormatException,java.io.IOException
    virtual JavaByteArray* openBytes(int arg1, int arg2, int arg3, int arg4, int arg5); // public byte[] loci.formats.FormatReader.openBytes(int,int,int,int,int) throws loci.formats.FormatException,java.io.IOException
    virtual JavaByteArray* openBytes(const char* arg1, int arg2, JavaByteArray* arg3); // public byte[] loci.formats.FormatReader.openBytes(java.lang.String,int,byte[]) throws loci.formats.FormatException,java.io.IOException
    virtual java::awt::image::BufferedImage* openThumbImage(const char* arg1, int arg2); // public java.awt.image.BufferedImage loci.formats.FormatReader.openThumbImage(java.lang.String,int) throws loci.formats.FormatException,java.io.IOException
    virtual java::awt::image::BufferedImage* openThumbImage(int arg1); // public java.awt.image.BufferedImage loci.formats.FormatReader.openThumbImage(int) throws loci.formats.FormatException,java.io.IOException
    virtual JavaObjectArray* getUsedFiles(const char* arg1); // public java.lang.String[] loci.formats.FormatReader.getUsedFiles(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual JavaObjectArray* getUsedFiles(); // public java.lang.String[] loci.formats.FormatReader.getUsedFiles()
    virtual void setId(const char* arg1, bool arg2); // public void loci.formats.FormatReader.setId(java.lang.String,boolean) throws loci.formats.FormatException,java.io.IOException
    virtual java::awt::image::BufferedImage* openImage(int arg1, int arg2, int arg3, int arg4, int arg5); // public java.awt.image.BufferedImage loci.formats.FormatReader.openImage(int,int,int,int,int) throws loci.formats.FormatException,java.io.IOException
    virtual java::awt::image::BufferedImage* openImage(int arg1); // public java.awt.image.BufferedImage loci.formats.FormatReader.openImage(int) throws loci.formats.FormatException,java.io.IOException
    virtual java::awt::image::BufferedImage* openImage(const char* arg1, int arg2); // public java.awt.image.BufferedImage loci.formats.FormatReader.openImage(java.lang.String,int) throws loci.formats.FormatException,java.io.IOException
    virtual int getSizeX(const char* arg1); // public int loci.formats.FormatReader.getSizeX(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual int getSizeX(); // public int loci.formats.FormatReader.getSizeX()
    virtual int getSizeY(const char* arg1); // public int loci.formats.FormatReader.getSizeY(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual int getSizeY(); // public int loci.formats.FormatReader.getSizeY()
    virtual loci::formats::meta::MetadataStore* getMetadataStore(const char* arg1); // public loci.formats.meta.MetadataStore loci.formats.FormatReader.getMetadataStore(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual loci::formats::meta::MetadataStore* getMetadataStore(); // public loci.formats.meta.MetadataStore loci.formats.FormatReader.getMetadataStore()
    virtual bool isMetadataFiltered(); // public boolean loci.formats.FormatReader.isMetadataFiltered()
    virtual int getImageCount(const char* arg1); // public int loci.formats.FormatReader.getImageCount(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual int getImageCount(); // public int loci.formats.FormatReader.getImageCount()
    virtual bool isRGB(); // public boolean loci.formats.FormatReader.isRGB()
    virtual bool isRGB(const char* arg1); // public boolean loci.formats.FormatReader.isRGB(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual int getSizeZ(); // public int loci.formats.FormatReader.getSizeZ()
    virtual int getSizeZ(const char* arg1); // public int loci.formats.FormatReader.getSizeZ(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual int getSizeC(); // public int loci.formats.FormatReader.getSizeC()
    virtual int getSizeC(const char* arg1); // public int loci.formats.FormatReader.getSizeC(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual int getSizeT(const char* arg1); // public int loci.formats.FormatReader.getSizeT(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual int getSizeT(); // public int loci.formats.FormatReader.getSizeT()
    virtual int getPixelType(const char* arg1); // public int loci.formats.FormatReader.getPixelType(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual int getPixelType(); // public int loci.formats.FormatReader.getPixelType()
    virtual int getEffectiveSizeC(); // public int loci.formats.FormatReader.getEffectiveSizeC()
    virtual int getEffectiveSizeC(const char* arg1); // public int loci.formats.FormatReader.getEffectiveSizeC(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual int getRGBChannelCount(); // public int loci.formats.FormatReader.getRGBChannelCount()
    virtual int getRGBChannelCount(const char* arg1); // public int loci.formats.FormatReader.getRGBChannelCount(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual bool isIndexed(); // public boolean loci.formats.FormatReader.isIndexed()
    virtual bool isFalseColor(); // public boolean loci.formats.FormatReader.isFalseColor()
    virtual JavaObjectArray* get8BitLookupTable(); // public byte[][] loci.formats.FormatReader.get8BitLookupTable() throws loci.formats.FormatException,java.io.IOException
    virtual JavaObjectArray* get16BitLookupTable(); // public short[][] loci.formats.FormatReader.get16BitLookupTable() throws loci.formats.FormatException,java.io.IOException
    virtual JavaIntArray* getChannelDimLengths(); // public int[] loci.formats.FormatReader.getChannelDimLengths()
    virtual JavaIntArray* getChannelDimLengths(const char* arg1); // public int[] loci.formats.FormatReader.getChannelDimLengths(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual JavaObjectArray* getChannelDimTypes(); // public java.lang.String[] loci.formats.FormatReader.getChannelDimTypes()
    virtual JavaObjectArray* getChannelDimTypes(const char* arg1); // public java.lang.String[] loci.formats.FormatReader.getChannelDimTypes(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual int getThumbSizeX(const char* arg1); // public int loci.formats.FormatReader.getThumbSizeX(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual int getThumbSizeX(); // public int loci.formats.FormatReader.getThumbSizeX()
    virtual int getThumbSizeY(const char* arg1); // public int loci.formats.FormatReader.getThumbSizeY(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual int getThumbSizeY(); // public int loci.formats.FormatReader.getThumbSizeY()
    virtual bool isLittleEndian(const char* arg1); // public boolean loci.formats.FormatReader.isLittleEndian(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual bool isLittleEndian(); // public boolean loci.formats.FormatReader.isLittleEndian()
    virtual const char* getDimensionOrder(const char* arg1); // public java.lang.String loci.formats.FormatReader.getDimensionOrder(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual const char* getDimensionOrder(); // public java.lang.String loci.formats.FormatReader.getDimensionOrder()
    virtual bool isOrderCertain(const char* arg1); // public boolean loci.formats.FormatReader.isOrderCertain(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual bool isOrderCertain(); // public boolean loci.formats.FormatReader.isOrderCertain()
    virtual bool isInterleaved(const char* arg1, int arg2); // public boolean loci.formats.FormatReader.isInterleaved(java.lang.String,int) throws loci.formats.FormatException,java.io.IOException
    virtual bool isInterleaved(const char* arg1); // public boolean loci.formats.FormatReader.isInterleaved(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual bool isInterleaved(); // public boolean loci.formats.FormatReader.isInterleaved()
    virtual bool isInterleaved(int arg1); // public boolean loci.formats.FormatReader.isInterleaved(int)
    virtual JavaByteArray* openThumbBytes(int arg1); // public byte[] loci.formats.FormatReader.openThumbBytes(int) throws loci.formats.FormatException,java.io.IOException
    virtual JavaByteArray* openThumbBytes(const char* arg1, int arg2); // public byte[] loci.formats.FormatReader.openThumbBytes(java.lang.String,int) throws loci.formats.FormatException,java.io.IOException
    virtual int getSeriesCount(); // public int loci.formats.FormatReader.getSeriesCount()
    virtual int getSeriesCount(const char* arg1); // public int loci.formats.FormatReader.getSeriesCount(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual void setSeries(int arg1); // public void loci.formats.FormatReader.setSeries(int)
    virtual void setSeries(const char* arg1, int arg2); // public void loci.formats.FormatReader.setSeries(java.lang.String,int) throws loci.formats.FormatException,java.io.IOException
    virtual int getSeries(const char* arg1); // public int loci.formats.FormatReader.getSeries(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual int getSeries(); // public int loci.formats.FormatReader.getSeries()
    virtual void setGroupFiles(bool arg1); // public void loci.formats.FormatReader.setGroupFiles(boolean)
    virtual bool isGroupFiles(); // public boolean loci.formats.FormatReader.isGroupFiles()
    virtual bool isMetadataComplete(); // public boolean loci.formats.FormatReader.isMetadataComplete()
    virtual void setNormalized(bool arg1); // public void loci.formats.FormatReader.setNormalized(boolean)
    virtual bool isNormalized(); // public boolean loci.formats.FormatReader.isNormalized()
    virtual void setMetadataCollected(bool arg1); // public void loci.formats.FormatReader.setMetadataCollected(boolean)
    virtual bool isMetadataCollected(); // public boolean loci.formats.FormatReader.isMetadataCollected()
    virtual void setOriginalMetadataPopulated(bool arg1); // public void loci.formats.FormatReader.setOriginalMetadataPopulated(boolean)
    virtual bool isOriginalMetadataPopulated(); // public boolean loci.formats.FormatReader.isOriginalMetadataPopulated()
    virtual const char* getCurrentFile(); // public java.lang.String loci.formats.FormatReader.getCurrentFile()
    virtual JavaIntArray* getZCTCoords(const char* arg1, int arg2); // public int[] loci.formats.FormatReader.getZCTCoords(java.lang.String,int) throws loci.formats.FormatException,java.io.IOException
    virtual JavaIntArray* getZCTCoords(int arg1); // public int[] loci.formats.FormatReader.getZCTCoords(int)
    virtual java::lang::Object* getMetadataValue(const char* arg1, const char* arg2); // public java.lang.Object loci.formats.FormatReader.getMetadataValue(java.lang.String,java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual java::lang::Object* getMetadataValue(const char* arg1); // public java.lang.Object loci.formats.FormatReader.getMetadataValue(java.lang.String)
    virtual java::util::Hashtable* getMetadata(const char* arg1); // public java.util.Hashtable loci.formats.FormatReader.getMetadata(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual java::util::Hashtable* getMetadata(); // public java.util.Hashtable loci.formats.FormatReader.getMetadata()
    virtual loci::formats::CoreMetadata* getCoreMetadata(const char* arg1); // public loci.formats.CoreMetadata loci.formats.FormatReader.getCoreMetadata(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual loci::formats::CoreMetadata* getCoreMetadata(); // public loci.formats.CoreMetadata loci.formats.FormatReader.getCoreMetadata()
    virtual void setMetadataFiltered(bool arg1); // public void loci.formats.FormatReader.setMetadataFiltered(boolean)
    virtual void setMetadataStore(loci::formats::meta::MetadataStore* arg1); // public void loci.formats.FormatReader.setMetadataStore(loci.formats.meta.MetadataStore)
    virtual java::lang::Object* getMetadataStoreRoot(); // public java.lang.Object loci.formats.FormatReader.getMetadataStoreRoot()
    virtual java::lang::Object* getMetadataStoreRoot(const char* arg1); // public java.lang.Object loci.formats.FormatReader.getMetadataStoreRoot(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual JavaObjectArray* getUnderlyingReaders(); // public loci.formats.IFormatReader[] loci.formats.FormatReader.getUnderlyingReaders()

};
}
}
#endif
