#ifndef java_awt_FontMetrics_H
#define java_awt_FontMetrics_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace io {
class Serializable;
}
}

namespace java {
namespace lang {
class String;
}
}

namespace java {
namespace awt {
namespace font {
class FontRenderContext;
}
}
}

namespace java {
namespace awt {
class Font;
}
}

namespace java {
namespace awt {
namespace font {
class LineMetrics;
}
}
}

namespace java {
namespace awt {
class Graphics;
}
}

namespace java {
namespace text {
class CharacterIterator;
}
}

namespace java {
namespace awt {
namespace geom {
class Rectangle2D;
}
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
class FontMetrics : public java::lang::Object {
  public:
    FontMetrics(JavaMarker* dummy);
    FontMetrics(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual const char* toString(); // public java.lang.String java.awt.FontMetrics.toString()
    virtual int getHeight(); // public int java.awt.FontMetrics.getHeight()
    virtual java::awt::font::FontRenderContext* getFontRenderContext(); // public java.awt.font.FontRenderContext java.awt.FontMetrics.getFontRenderContext()
    virtual java::awt::Font* getFont(); // public java.awt.Font java.awt.FontMetrics.getFont()
    virtual int getAscent(); // public int java.awt.FontMetrics.getAscent()
    virtual int getDescent(); // public int java.awt.FontMetrics.getDescent()
    virtual int getLeading(); // public int java.awt.FontMetrics.getLeading()
    virtual java::awt::font::LineMetrics* getLineMetrics(JavaCharArray* arg1, int arg2, int arg3, java::awt::Graphics* arg4); // public java.awt.font.LineMetrics java.awt.FontMetrics.getLineMetrics(char[],int,int,java.awt.Graphics)
    virtual java::awt::font::LineMetrics* getLineMetrics(const char* arg1, java::awt::Graphics* arg2); // public java.awt.font.LineMetrics java.awt.FontMetrics.getLineMetrics(java.lang.String,java.awt.Graphics)
    virtual java::awt::font::LineMetrics* getLineMetrics(const char* arg1, int arg2, int arg3, java::awt::Graphics* arg4); // public java.awt.font.LineMetrics java.awt.FontMetrics.getLineMetrics(java.lang.String,int,int,java.awt.Graphics)
    virtual java::awt::font::LineMetrics* getLineMetrics(java::text::CharacterIterator* arg1, int arg2, int arg3, java::awt::Graphics* arg4); // public java.awt.font.LineMetrics java.awt.FontMetrics.getLineMetrics(java.text.CharacterIterator,int,int,java.awt.Graphics)
    virtual java::awt::geom::Rectangle2D* getMaxCharBounds(java::awt::Graphics* arg1); // public java.awt.geom.Rectangle2D java.awt.FontMetrics.getMaxCharBounds(java.awt.Graphics)
    virtual java::awt::geom::Rectangle2D* getStringBounds(JavaCharArray* arg1, int arg2, int arg3, java::awt::Graphics* arg4); // public java.awt.geom.Rectangle2D java.awt.FontMetrics.getStringBounds(char[],int,int,java.awt.Graphics)
    virtual java::awt::geom::Rectangle2D* getStringBounds(const char* arg1, int arg2, int arg3, java::awt::Graphics* arg4); // public java.awt.geom.Rectangle2D java.awt.FontMetrics.getStringBounds(java.lang.String,int,int,java.awt.Graphics)
    virtual java::awt::geom::Rectangle2D* getStringBounds(java::text::CharacterIterator* arg1, int arg2, int arg3, java::awt::Graphics* arg4); // public java.awt.geom.Rectangle2D java.awt.FontMetrics.getStringBounds(java.text.CharacterIterator,int,int,java.awt.Graphics)
    virtual java::awt::geom::Rectangle2D* getStringBounds(const char* arg1, java::awt::Graphics* arg2); // public java.awt.geom.Rectangle2D java.awt.FontMetrics.getStringBounds(java.lang.String,java.awt.Graphics)
    virtual bool hasUniformLineMetrics(); // public boolean java.awt.FontMetrics.hasUniformLineMetrics()
    virtual int bytesWidth(JavaByteArray* arg1, int arg2, int arg3); // public int java.awt.FontMetrics.bytesWidth(byte[],int,int)
    virtual int charWidth(int arg1); // public int java.awt.FontMetrics.charWidth(int)
    virtual int charWidth(char arg1); // public int java.awt.FontMetrics.charWidth(char)
    virtual int charsWidth(JavaCharArray* arg1, int arg2, int arg3); // public int java.awt.FontMetrics.charsWidth(char[],int,int)
    virtual int getMaxAdvance(); // public int java.awt.FontMetrics.getMaxAdvance()
    virtual int getMaxAscent(); // public int java.awt.FontMetrics.getMaxAscent()
    virtual int getMaxDecent(); // public int java.awt.FontMetrics.getMaxDecent()
    virtual int getMaxDescent(); // public int java.awt.FontMetrics.getMaxDescent()
    virtual JavaIntArray* getWidths(); // public int[] java.awt.FontMetrics.getWidths()
    virtual int stringWidth(const char* arg1); // public int java.awt.FontMetrics.stringWidth(java.lang.String)

};
}
}
#endif
