#ifndef java_awt_event_FocusEvent_H
#define java_awt_event_FocusEvent_H
#include <jni.h>
#include <java_marker.h>
#include <java_awt_event_ComponentEvent.h>

namespace java {
namespace awt {
class Component;
}
}

namespace java {
namespace lang {
class String;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace event {
class FocusEvent : public java::awt::event::ComponentEvent {
  public:
    FocusEvent(JavaMarker* dummy);
    FocusEvent(jobject obj);
    FocusEvent(java::awt::Component* arg1, int arg2, bool arg3, java::awt::Component* arg4); // public java.awt.event.FocusEvent(java.awt.Component,int,boolean,java.awt.Component)
    FocusEvent(java::awt::Component* arg1, int arg2, bool arg3); // public java.awt.event.FocusEvent(java.awt.Component,int,boolean)
    FocusEvent(java::awt::Component* arg1, int arg2); // public java.awt.event.FocusEvent(java.awt.Component,int)

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual const char* paramString(); // public java.lang.String java.awt.event.FocusEvent.paramString()
    virtual java::awt::Component* getOppositeComponent(); // public java.awt.Component java.awt.event.FocusEvent.getOppositeComponent()
    virtual bool isTemporary(); // public boolean java.awt.event.FocusEvent.isTemporary()

    int FOCUS_FIRST; // public static final int java.awt.event.FocusEvent.FOCUS_FIRST
    int FOCUS_LAST; // public static final int java.awt.event.FocusEvent.FOCUS_LAST
    int FOCUS_GAINED; // public static final int java.awt.event.FocusEvent.FOCUS_GAINED
    int FOCUS_LOST; // public static final int java.awt.event.FocusEvent.FOCUS_LOST
};
}
}
}
#endif
