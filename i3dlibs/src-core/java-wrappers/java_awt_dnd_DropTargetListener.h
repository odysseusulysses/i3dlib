#ifndef java_awt_dnd_DropTargetListener_H
#define java_awt_dnd_DropTargetListener_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace util {
class EventListener;
}
}

namespace java {
namespace awt {
namespace dnd {
class DropTargetDragEvent;
}
}
}

namespace java {
namespace awt {
namespace dnd {
class DropTargetEvent;
}
}
}

namespace java {
namespace awt {
namespace dnd {
class DropTargetDropEvent;
}
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace dnd {
class DropTargetListener : public java::lang::Object {
  public:
    DropTargetListener(JavaMarker* dummy);
    DropTargetListener(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual void dragEnter(java::awt::dnd::DropTargetDragEvent* arg1); // public abstract void java.awt.dnd.DropTargetListener.dragEnter(java.awt.dnd.DropTargetDragEvent)
    virtual void dragExit(java::awt::dnd::DropTargetEvent* arg1); // public abstract void java.awt.dnd.DropTargetListener.dragExit(java.awt.dnd.DropTargetEvent)
    virtual void dragOver(java::awt::dnd::DropTargetDragEvent* arg1); // public abstract void java.awt.dnd.DropTargetListener.dragOver(java.awt.dnd.DropTargetDragEvent)
    virtual void drop(java::awt::dnd::DropTargetDropEvent* arg1); // public abstract void java.awt.dnd.DropTargetListener.drop(java.awt.dnd.DropTargetDropEvent)
    virtual void dropActionChanged(java::awt::dnd::DropTargetDragEvent* arg1); // public abstract void java.awt.dnd.DropTargetListener.dropActionChanged(java.awt.dnd.DropTargetDragEvent)

};
}
}
}
#endif
