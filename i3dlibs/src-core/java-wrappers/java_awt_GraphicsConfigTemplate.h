#ifndef java_awt_GraphicsConfigTemplate_H
#define java_awt_GraphicsConfigTemplate_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace io {
class Serializable;
}
}

namespace java {
namespace awt {
class GraphicsConfiguration;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
class GraphicsConfigTemplate : public java::lang::Object {
  public:
    GraphicsConfigTemplate(JavaMarker* dummy);
    GraphicsConfigTemplate(jobject obj);
    GraphicsConfigTemplate(); // public java.awt.GraphicsConfigTemplate()

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual java::awt::GraphicsConfiguration* getBestConfiguration(JavaObjectArray* arg1); // public abstract java.awt.GraphicsConfiguration java.awt.GraphicsConfigTemplate.getBestConfiguration(java.awt.GraphicsConfiguration[])
    virtual bool isGraphicsConfigSupported(java::awt::GraphicsConfiguration* arg1); // public abstract boolean java.awt.GraphicsConfigTemplate.isGraphicsConfigSupported(java.awt.GraphicsConfiguration)

    int REQUIRED; // public static final int java.awt.GraphicsConfigTemplate.REQUIRED
    int PREFERRED; // public static final int java.awt.GraphicsConfigTemplate.PREFERRED
    int UNNECESSARY; // public static final int java.awt.GraphicsConfigTemplate.UNNECESSARY
};
}
}
#endif
