#ifndef java_awt_image_DataBuffer_H
#define java_awt_image_DataBuffer_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace image {
class DataBuffer : public java::lang::Object {
  public:
    DataBuffer(JavaMarker* dummy);
    DataBuffer(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual int getSize(); // public int java.awt.image.DataBuffer.getSize()
    virtual int getOffset(); // public int java.awt.image.DataBuffer.getOffset()
    virtual int getDataType(); // public int java.awt.image.DataBuffer.getDataType()
    virtual int getDataTypeSize(int arg1); // public static int java.awt.image.DataBuffer.getDataTypeSize(int)
    virtual int getNumBanks(); // public int java.awt.image.DataBuffer.getNumBanks()
    virtual int getElem(int arg1, int arg2); // public abstract int java.awt.image.DataBuffer.getElem(int,int)
    virtual int getElem(int arg1); // public int java.awt.image.DataBuffer.getElem(int)
    virtual double getElemDouble(int arg1); // public double java.awt.image.DataBuffer.getElemDouble(int)
    virtual double getElemDouble(int arg1, int arg2); // public double java.awt.image.DataBuffer.getElemDouble(int,int)
    virtual float getElemFloat(int arg1, int arg2); // public float java.awt.image.DataBuffer.getElemFloat(int,int)
    virtual float getElemFloat(int arg1); // public float java.awt.image.DataBuffer.getElemFloat(int)
    virtual JavaIntArray* getOffsets(); // public int[] java.awt.image.DataBuffer.getOffsets()
    virtual void setElem(int arg1, int arg2, int arg3); // public abstract void java.awt.image.DataBuffer.setElem(int,int,int)
    virtual void setElem(int arg1, int arg2); // public void java.awt.image.DataBuffer.setElem(int,int)
    virtual void setElemDouble(int arg1, int arg2, double arg3); // public void java.awt.image.DataBuffer.setElemDouble(int,int,double)
    virtual void setElemDouble(int arg1, double arg2); // public void java.awt.image.DataBuffer.setElemDouble(int,double)
    virtual void setElemFloat(int arg1, int arg2, float arg3); // public void java.awt.image.DataBuffer.setElemFloat(int,int,float)
    virtual void setElemFloat(int arg1, float arg2); // public void java.awt.image.DataBuffer.setElemFloat(int,float)

    int TYPE_BYTE; // public static final int java.awt.image.DataBuffer.TYPE_BYTE
    int TYPE_USHORT; // public static final int java.awt.image.DataBuffer.TYPE_USHORT
    int TYPE_SHORT; // public static final int java.awt.image.DataBuffer.TYPE_SHORT
    int TYPE_INT; // public static final int java.awt.image.DataBuffer.TYPE_INT
    int TYPE_FLOAT; // public static final int java.awt.image.DataBuffer.TYPE_FLOAT
    int TYPE_DOUBLE; // public static final int java.awt.image.DataBuffer.TYPE_DOUBLE
    int TYPE_UNDEFINED; // public static final int java.awt.image.DataBuffer.TYPE_UNDEFINED
};
}
}
}
#endif
