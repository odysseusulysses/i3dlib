#ifndef java_awt_DisplayMode_H
#define java_awt_DisplayMode_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
class DisplayMode : public java::lang::Object {
  public:
    DisplayMode(JavaMarker* dummy);
    DisplayMode(jobject obj);
    DisplayMode(int arg1, int arg2, int arg3, int arg4); // public java.awt.DisplayMode(int,int,int,int)

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual int hashCode(); // public int java.awt.DisplayMode.hashCode()
    virtual bool equals(java::lang::Object* arg1); // public boolean java.awt.DisplayMode.equals(java.lang.Object)
    virtual bool equals(java::awt::DisplayMode* arg1); // public boolean java.awt.DisplayMode.equals(java.awt.DisplayMode)
    virtual int getHeight(); // public int java.awt.DisplayMode.getHeight()
    virtual int getWidth(); // public int java.awt.DisplayMode.getWidth()
    virtual int getBitDepth(); // public int java.awt.DisplayMode.getBitDepth()
    virtual int getRefreshRate(); // public int java.awt.DisplayMode.getRefreshRate()

    int BIT_DEPTH_MULTI; // public static final int java.awt.DisplayMode.BIT_DEPTH_MULTI
    int REFRESH_RATE_UNKNOWN; // public static final int java.awt.DisplayMode.REFRESH_RATE_UNKNOWN
};
}
}
#endif
