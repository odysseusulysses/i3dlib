#ifndef java_awt_Graphics2D_H
#define java_awt_Graphics2D_H
#include <jni.h>
#include <java_marker.h>
#include <java_awt_Graphics.h>

namespace java {
namespace awt {
namespace geom {
class AffineTransform;
}
}
}

namespace java {
namespace awt {
class Shape;
}
}

namespace java {
namespace util {
class Map;
}
}

namespace java {
namespace awt {
namespace font {
class GlyphVector;
}
}
}

namespace java {
namespace awt {
class Image;
}
}

namespace java {
namespace awt {
namespace image {
class ImageObserver;
}
}
}

namespace java {
namespace awt {
namespace image {
class BufferedImage;
}
}
}

namespace java {
namespace awt {
namespace image {
class BufferedImageOp;
}
}
}

namespace java {
namespace awt {
namespace image {
namespace renderable {
class RenderableImage;
}
}
}
}

namespace java {
namespace awt {
namespace image {
class RenderedImage;
}
}
}

namespace java {
namespace text {
class AttributedCharacterIterator;
}
}

namespace java {
namespace lang {
class String;
}
}

namespace java {
namespace awt {
class Color;
}
}

namespace java {
namespace awt {
class Composite;
}
}

namespace java {
namespace awt {
class GraphicsConfiguration;
}
}

namespace java {
namespace awt {
namespace font {
class FontRenderContext;
}
}
}

namespace java {
namespace awt {
class Paint;
}
}

namespace java {
namespace lang {
class Object;
}
}

namespace java {
namespace awt {
class RenderingHints__Key;
}
}

namespace java {
namespace awt {
class RenderingHints;
}
}

namespace java {
namespace awt {
class Stroke;
}
}

namespace java {
namespace awt {
class Rectangle;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
class Graphics2D : public java::awt::Graphics {
  public:
    Graphics2D(JavaMarker* dummy);
    Graphics2D(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual void transform(java::awt::geom::AffineTransform* arg1); // public abstract void java.awt.Graphics2D.transform(java.awt.geom.AffineTransform)
    virtual void fill(java::awt::Shape* arg1); // public abstract void java.awt.Graphics2D.fill(java.awt.Shape)
    virtual void rotate(double arg1); // public abstract void java.awt.Graphics2D.rotate(double)
    virtual void rotate(double arg1, double arg2, double arg3); // public abstract void java.awt.Graphics2D.rotate(double,double,double)
    virtual void scale(double arg1, double arg2); // public abstract void java.awt.Graphics2D.scale(double,double)
    virtual void clip(java::awt::Shape* arg1); // public abstract void java.awt.Graphics2D.clip(java.awt.Shape)
    virtual void translate(int arg1, int arg2); // public abstract void java.awt.Graphics2D.translate(int,int)
    virtual void translate(double arg1, double arg2); // public abstract void java.awt.Graphics2D.translate(double,double)
    virtual void addRenderingHints(java::util::Map* arg1); // public abstract void java.awt.Graphics2D.addRenderingHints(java.util.Map)
    virtual void draw(java::awt::Shape* arg1); // public abstract void java.awt.Graphics2D.draw(java.awt.Shape)
    virtual void draw3DRect(int arg1, int arg2, int arg3, int arg4, bool arg5); // public void java.awt.Graphics2D.draw3DRect(int,int,int,int,boolean)
    virtual void drawGlyphVector(java::awt::font::GlyphVector* arg1, float arg2, float arg3); // public abstract void java.awt.Graphics2D.drawGlyphVector(java.awt.font.GlyphVector,float,float)
    virtual bool drawImage(java::awt::Image* arg1, java::awt::geom::AffineTransform* arg2, java::awt::image::ImageObserver* arg3); // public abstract boolean java.awt.Graphics2D.drawImage(java.awt.Image,java.awt.geom.AffineTransform,java.awt.image.ImageObserver)
    virtual void drawImage(java::awt::image::BufferedImage* arg1, java::awt::image::BufferedImageOp* arg2, int arg3, int arg4); // public abstract void java.awt.Graphics2D.drawImage(java.awt.image.BufferedImage,java.awt.image.BufferedImageOp,int,int)
    virtual void drawRenderableImage(java::awt::image::renderable::RenderableImage* arg1, java::awt::geom::AffineTransform* arg2); // public abstract void java.awt.Graphics2D.drawRenderableImage(java.awt.image.renderable.RenderableImage,java.awt.geom.AffineTransform)
    virtual void drawRenderedImage(java::awt::image::RenderedImage* arg1, java::awt::geom::AffineTransform* arg2); // public abstract void java.awt.Graphics2D.drawRenderedImage(java.awt.image.RenderedImage,java.awt.geom.AffineTransform)
    virtual void drawString(java::text::AttributedCharacterIterator* arg1, int arg2, int arg3); // public abstract void java.awt.Graphics2D.drawString(java.text.AttributedCharacterIterator,int,int)
    virtual void drawString(const char* arg1, float arg2, float arg3); // public abstract void java.awt.Graphics2D.drawString(java.lang.String,float,float)
    virtual void drawString(java::text::AttributedCharacterIterator* arg1, float arg2, float arg3); // public abstract void java.awt.Graphics2D.drawString(java.text.AttributedCharacterIterator,float,float)
    virtual void drawString(const char* arg1, int arg2, int arg3); // public abstract void java.awt.Graphics2D.drawString(java.lang.String,int,int)
    virtual void fill3DRect(int arg1, int arg2, int arg3, int arg4, bool arg5); // public void java.awt.Graphics2D.fill3DRect(int,int,int,int,boolean)
    virtual java::awt::Color* getBackground(); // public abstract java.awt.Color java.awt.Graphics2D.getBackground()
    virtual java::awt::Composite* getComposite(); // public abstract java.awt.Composite java.awt.Graphics2D.getComposite()
    virtual java::awt::GraphicsConfiguration* getDeviceConfiguration(); // public abstract java.awt.GraphicsConfiguration java.awt.Graphics2D.getDeviceConfiguration()
    virtual java::awt::font::FontRenderContext* getFontRenderContext(); // public abstract java.awt.font.FontRenderContext java.awt.Graphics2D.getFontRenderContext()
    virtual java::awt::Paint* getPaint(); // public abstract java.awt.Paint java.awt.Graphics2D.getPaint()
    virtual java::lang::Object* getRenderingHint(java::awt::RenderingHints__Key* arg1); // public abstract java.lang.Object java.awt.Graphics2D.getRenderingHint(java.awt.RenderingHints$Key)
    virtual java::awt::RenderingHints* getRenderingHints(); // public abstract java.awt.RenderingHints java.awt.Graphics2D.getRenderingHints()
    virtual java::awt::Stroke* getStroke(); // public abstract java.awt.Stroke java.awt.Graphics2D.getStroke()
    virtual java::awt::geom::AffineTransform* getTransform(); // public abstract java.awt.geom.AffineTransform java.awt.Graphics2D.getTransform()
    virtual bool hit(java::awt::Rectangle* arg1, java::awt::Shape* arg2, bool arg3); // public abstract boolean java.awt.Graphics2D.hit(java.awt.Rectangle,java.awt.Shape,boolean)
    virtual void setBackground(java::awt::Color* arg1); // public abstract void java.awt.Graphics2D.setBackground(java.awt.Color)
    virtual void setComposite(java::awt::Composite* arg1); // public abstract void java.awt.Graphics2D.setComposite(java.awt.Composite)
    virtual void setPaint(java::awt::Paint* arg1); // public abstract void java.awt.Graphics2D.setPaint(java.awt.Paint)
    virtual void setRenderingHint(java::awt::RenderingHints__Key* arg1, java::lang::Object* arg2); // public abstract void java.awt.Graphics2D.setRenderingHint(java.awt.RenderingHints$Key,java.lang.Object)
    virtual void setRenderingHints(java::util::Map* arg1); // public abstract void java.awt.Graphics2D.setRenderingHints(java.util.Map)
    virtual void setStroke(java::awt::Stroke* arg1); // public abstract void java.awt.Graphics2D.setStroke(java.awt.Stroke)
    virtual void setTransform(java::awt::geom::AffineTransform* arg1); // public abstract void java.awt.Graphics2D.setTransform(java.awt.geom.AffineTransform)
    virtual void shear(double arg1, double arg2); // public abstract void java.awt.Graphics2D.shear(double,double)

};
}
}
#endif
