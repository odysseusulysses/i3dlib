#ifndef java_awt_MenuContainer_H
#define java_awt_MenuContainer_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace awt {
class MenuComponent;
}
}

namespace java {
namespace awt {
class Font;
}
}

namespace java {
namespace awt {
class Event;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
class MenuContainer : public java::lang::Object {
  public:
    MenuContainer(JavaMarker* dummy);
    MenuContainer(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual void remove(java::awt::MenuComponent* arg1); // public abstract void java.awt.MenuContainer.remove(java.awt.MenuComponent)
    virtual java::awt::Font* getFont(); // public abstract java.awt.Font java.awt.MenuContainer.getFont()
    virtual bool postEvent(java::awt::Event* arg1); // public abstract boolean java.awt.MenuContainer.postEvent(java.awt.Event)

};
}
}
#endif
