#ifndef java_awt_font_GlyphMetrics_H
#define java_awt_font_GlyphMetrics_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace awt {
namespace geom {
class Rectangle2D;
}
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace font {
class GlyphMetrics : public java::lang::Object {
  public:
    GlyphMetrics(JavaMarker* dummy);
    GlyphMetrics(jobject obj);
    GlyphMetrics(bool arg1, float arg2, float arg3, java::awt::geom::Rectangle2D* arg4, char arg5); // public java.awt.font.GlyphMetrics(boolean,float,float,java.awt.geom.Rectangle2D,byte)
    GlyphMetrics(float arg1, java::awt::geom::Rectangle2D* arg2, char arg3); // public java.awt.font.GlyphMetrics(float,java.awt.geom.Rectangle2D,byte)

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual int getType(); // public int java.awt.font.GlyphMetrics.getType()
    virtual bool isWhitespace(); // public boolean java.awt.font.GlyphMetrics.isWhitespace()
    virtual java::awt::geom::Rectangle2D* getBounds2D(); // public java.awt.geom.Rectangle2D java.awt.font.GlyphMetrics.getBounds2D()
    virtual float getAdvance(); // public float java.awt.font.GlyphMetrics.getAdvance()
    virtual float getAdvanceX(); // public float java.awt.font.GlyphMetrics.getAdvanceX()
    virtual float getAdvanceY(); // public float java.awt.font.GlyphMetrics.getAdvanceY()
    virtual float getLSB(); // public float java.awt.font.GlyphMetrics.getLSB()
    virtual float getRSB(); // public float java.awt.font.GlyphMetrics.getRSB()
    virtual bool isCombining(); // public boolean java.awt.font.GlyphMetrics.isCombining()
    virtual bool isComponent(); // public boolean java.awt.font.GlyphMetrics.isComponent()
    virtual bool isLigature(); // public boolean java.awt.font.GlyphMetrics.isLigature()
    virtual bool isStandard(); // public boolean java.awt.font.GlyphMetrics.isStandard()

    char STANDARD; // public static final byte java.awt.font.GlyphMetrics.STANDARD
    char LIGATURE; // public static final byte java.awt.font.GlyphMetrics.LIGATURE
    char COMBINING; // public static final byte java.awt.font.GlyphMetrics.COMBINING
    char COMPONENT; // public static final byte java.awt.font.GlyphMetrics.COMPONENT
    char WHITESPACE; // public static final byte java.awt.font.GlyphMetrics.WHITESPACE
};
}
}
}
#endif
