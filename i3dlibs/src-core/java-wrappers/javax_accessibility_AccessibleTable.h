#ifndef javax_accessibility_AccessibleTable_H
#define javax_accessibility_AccessibleTable_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace javax {
namespace accessibility {
class Accessible;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace javax {
namespace accessibility {
class AccessibleTable : public java::lang::Object {
  public:
    AccessibleTable(JavaMarker* dummy);
    AccessibleTable(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual javax::accessibility::Accessible* getAccessibleAt(int arg1, int arg2); // public abstract javax.accessibility.Accessible javax.accessibility.AccessibleTable.getAccessibleAt(int,int)
    virtual javax::accessibility::Accessible* getAccessibleCaption(); // public abstract javax.accessibility.Accessible javax.accessibility.AccessibleTable.getAccessibleCaption()
    virtual int getAccessibleColumnCount(); // public abstract int javax.accessibility.AccessibleTable.getAccessibleColumnCount()
    virtual javax::accessibility::Accessible* getAccessibleColumnDescription(int arg1); // public abstract javax.accessibility.Accessible javax.accessibility.AccessibleTable.getAccessibleColumnDescription(int)
    virtual int getAccessibleColumnExtentAt(int arg1, int arg2); // public abstract int javax.accessibility.AccessibleTable.getAccessibleColumnExtentAt(int,int)
    virtual javax::accessibility::AccessibleTable* getAccessibleColumnHeader(); // public abstract javax.accessibility.AccessibleTable javax.accessibility.AccessibleTable.getAccessibleColumnHeader()
    virtual int getAccessibleRowCount(); // public abstract int javax.accessibility.AccessibleTable.getAccessibleRowCount()
    virtual javax::accessibility::Accessible* getAccessibleRowDescription(int arg1); // public abstract javax.accessibility.Accessible javax.accessibility.AccessibleTable.getAccessibleRowDescription(int)
    virtual int getAccessibleRowExtentAt(int arg1, int arg2); // public abstract int javax.accessibility.AccessibleTable.getAccessibleRowExtentAt(int,int)
    virtual javax::accessibility::AccessibleTable* getAccessibleRowHeader(); // public abstract javax.accessibility.AccessibleTable javax.accessibility.AccessibleTable.getAccessibleRowHeader()
    virtual javax::accessibility::Accessible* getAccessibleSummary(); // public abstract javax.accessibility.Accessible javax.accessibility.AccessibleTable.getAccessibleSummary()
    virtual JavaIntArray* getSelectedAccessibleColumns(); // public abstract int[] javax.accessibility.AccessibleTable.getSelectedAccessibleColumns()
    virtual JavaIntArray* getSelectedAccessibleRows(); // public abstract int[] javax.accessibility.AccessibleTable.getSelectedAccessibleRows()
    virtual bool isAccessibleColumnSelected(int arg1); // public abstract boolean javax.accessibility.AccessibleTable.isAccessibleColumnSelected(int)
    virtual bool isAccessibleRowSelected(int arg1); // public abstract boolean javax.accessibility.AccessibleTable.isAccessibleRowSelected(int)
    virtual bool isAccessibleSelected(int arg1, int arg2); // public abstract boolean javax.accessibility.AccessibleTable.isAccessibleSelected(int,int)
    virtual void setAccessibleCaption(javax::accessibility::Accessible* arg1); // public abstract void javax.accessibility.AccessibleTable.setAccessibleCaption(javax.accessibility.Accessible)
    virtual void setAccessibleColumnDescription(int arg1, javax::accessibility::Accessible* arg2); // public abstract void javax.accessibility.AccessibleTable.setAccessibleColumnDescription(int,javax.accessibility.Accessible)
    virtual void setAccessibleColumnHeader(javax::accessibility::AccessibleTable* arg1); // public abstract void javax.accessibility.AccessibleTable.setAccessibleColumnHeader(javax.accessibility.AccessibleTable)
    virtual void setAccessibleRowDescription(int arg1, javax::accessibility::Accessible* arg2); // public abstract void javax.accessibility.AccessibleTable.setAccessibleRowDescription(int,javax.accessibility.Accessible)
    virtual void setAccessibleRowHeader(javax::accessibility::AccessibleTable* arg1); // public abstract void javax.accessibility.AccessibleTable.setAccessibleRowHeader(javax.accessibility.AccessibleTable)
    virtual void setAccessibleSummary(javax::accessibility::Accessible* arg1); // public abstract void javax.accessibility.AccessibleTable.setAccessibleSummary(javax.accessibility.Accessible)

};
}
}
#endif
