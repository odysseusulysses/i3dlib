#ifndef loci_formats_ClassList_H
#define loci_formats_ClassList_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace lang {
class Class;
}
}

namespace java {
namespace lang {
class String;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace loci {
namespace formats {
class ClassList : public java::lang::Object {
  public:
    ClassList(JavaMarker* dummy);
    ClassList(jobject obj);
    ClassList(java::lang::Class* arg1); // public loci.formats.ClassList(java.lang.Class)
    ClassList(const char* arg1, java::lang::Class* arg2); // public loci.formats.ClassList(java.lang.String,java.lang.Class) throws java.io.IOException

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual void addClass(java::lang::Class* arg1); // public void loci.formats.ClassList.addClass(java.lang.Class) throws loci.formats.FormatException
    virtual JavaObjectArray* getClasses(); // public java.lang.Class[] loci.formats.ClassList.getClasses()
    virtual void removeClass(java::lang::Class* arg1); // public void loci.formats.ClassList.removeClass(java.lang.Class)

};
}
}
#endif
