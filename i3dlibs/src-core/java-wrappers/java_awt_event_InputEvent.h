#ifndef java_awt_event_InputEvent_H
#define java_awt_event_InputEvent_H
#include <jni.h>
#include <java_marker.h>
#include <java_awt_event_ComponentEvent.h>

namespace java {
namespace lang {
class String;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace event {
class InputEvent : public java::awt::event::ComponentEvent {
  public:
    InputEvent(JavaMarker* dummy);
    InputEvent(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual int getModifiers(); // public int java.awt.event.InputEvent.getModifiers()
    virtual void consume(); // public void java.awt.event.InputEvent.consume()
    virtual bool isControlDown(); // public boolean java.awt.event.InputEvent.isControlDown()
    virtual bool isShiftDown(); // public boolean java.awt.event.InputEvent.isShiftDown()
    virtual long getWhen(); // public long java.awt.event.InputEvent.getWhen()
    virtual bool isConsumed(); // public boolean java.awt.event.InputEvent.isConsumed()
    virtual int getModifiersEx(); // public int java.awt.event.InputEvent.getModifiersEx()
    virtual const char* getModifiersExText(int arg1); // public static java.lang.String java.awt.event.InputEvent.getModifiersExText(int)
    virtual bool isAltDown(); // public boolean java.awt.event.InputEvent.isAltDown()
    virtual bool isAltGraphDown(); // public boolean java.awt.event.InputEvent.isAltGraphDown()
    virtual bool isMetaDown(); // public boolean java.awt.event.InputEvent.isMetaDown()

    int SHIFT_MASK; // public static final int java.awt.event.InputEvent.SHIFT_MASK
    int CTRL_MASK; // public static final int java.awt.event.InputEvent.CTRL_MASK
    int META_MASK; // public static final int java.awt.event.InputEvent.META_MASK
    int ALT_MASK; // public static final int java.awt.event.InputEvent.ALT_MASK
    int ALT_GRAPH_MASK; // public static final int java.awt.event.InputEvent.ALT_GRAPH_MASK
    int BUTTON1_MASK; // public static final int java.awt.event.InputEvent.BUTTON1_MASK
    int BUTTON2_MASK; // public static final int java.awt.event.InputEvent.BUTTON2_MASK
    int BUTTON3_MASK; // public static final int java.awt.event.InputEvent.BUTTON3_MASK
    int SHIFT_DOWN_MASK; // public static final int java.awt.event.InputEvent.SHIFT_DOWN_MASK
    int CTRL_DOWN_MASK; // public static final int java.awt.event.InputEvent.CTRL_DOWN_MASK
    int META_DOWN_MASK; // public static final int java.awt.event.InputEvent.META_DOWN_MASK
    int ALT_DOWN_MASK; // public static final int java.awt.event.InputEvent.ALT_DOWN_MASK
    int BUTTON1_DOWN_MASK; // public static final int java.awt.event.InputEvent.BUTTON1_DOWN_MASK
    int BUTTON2_DOWN_MASK; // public static final int java.awt.event.InputEvent.BUTTON2_DOWN_MASK
    int BUTTON3_DOWN_MASK; // public static final int java.awt.event.InputEvent.BUTTON3_DOWN_MASK
    int ALT_GRAPH_DOWN_MASK; // public static final int java.awt.event.InputEvent.ALT_GRAPH_DOWN_MASK
};
}
}
}
#endif
