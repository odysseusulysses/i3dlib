#ifndef javax_accessibility_AccessibleStateSet_H
#define javax_accessibility_AccessibleStateSet_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace javax {
namespace accessibility {
class AccessibleState;
}
}

namespace java {
namespace lang {
class String;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace javax {
namespace accessibility {
class AccessibleStateSet : public java::lang::Object {
  public:
    AccessibleStateSet(JavaMarker* dummy);
    AccessibleStateSet(jobject obj);
    AccessibleStateSet(JavaObjectArray* arg1); // public javax.accessibility.AccessibleStateSet(javax.accessibility.AccessibleState[])
    AccessibleStateSet(); // public javax.accessibility.AccessibleStateSet()

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual bool add(javax::accessibility::AccessibleState* arg1); // public boolean javax.accessibility.AccessibleStateSet.add(javax.accessibility.AccessibleState)
    virtual void clear(); // public void javax.accessibility.AccessibleStateSet.clear()
    virtual const char* toString(); // public java.lang.String javax.accessibility.AccessibleStateSet.toString()
    virtual bool contains(javax::accessibility::AccessibleState* arg1); // public boolean javax.accessibility.AccessibleStateSet.contains(javax.accessibility.AccessibleState)
    virtual void addAll(JavaObjectArray* arg1); // public void javax.accessibility.AccessibleStateSet.addAll(javax.accessibility.AccessibleState[])
    virtual JavaObjectArray* toArray(); // public javax.accessibility.AccessibleState[] javax.accessibility.AccessibleStateSet.toArray()
    virtual bool remove(javax::accessibility::AccessibleState* arg1); // public boolean javax.accessibility.AccessibleStateSet.remove(javax.accessibility.AccessibleState)

};
}
}
#endif
