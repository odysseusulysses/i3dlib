#ifndef java_awt_dnd_DropTargetDropEvent_H
#define java_awt_dnd_DropTargetDropEvent_H
#include <jni.h>
#include <java_marker.h>
#include <java_awt_dnd_DropTargetEvent.h>

namespace java {
namespace awt {
namespace dnd {
class DropTargetContext;
}
}
}

namespace java {
namespace awt {
class Point;
}
}

namespace java {
namespace awt {
namespace datatransfer {
class DataFlavor;
}
}
}

namespace java {
namespace util {
class List;
}
}

namespace java {
namespace awt {
namespace datatransfer {
class Transferable;
}
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace dnd {
class DropTargetDropEvent : public java::awt::dnd::DropTargetEvent {
  public:
    DropTargetDropEvent(JavaMarker* dummy);
    DropTargetDropEvent(jobject obj);
    DropTargetDropEvent(java::awt::dnd::DropTargetContext* arg1, java::awt::Point* arg2, int arg3, int arg4, bool arg5); // public java.awt.dnd.DropTargetDropEvent(java.awt.dnd.DropTargetContext,java.awt.Point,int,int,boolean)
    DropTargetDropEvent(java::awt::dnd::DropTargetContext* arg1, java::awt::Point* arg2, int arg3, int arg4); // public java.awt.dnd.DropTargetDropEvent(java.awt.dnd.DropTargetContext,java.awt.Point,int,int)

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual java::awt::Point* getLocation(); // public java.awt.Point java.awt.dnd.DropTargetDropEvent.getLocation()
    virtual void rejectDrop(); // public void java.awt.dnd.DropTargetDropEvent.rejectDrop()
    virtual int getSourceActions(); // public int java.awt.dnd.DropTargetDropEvent.getSourceActions()
    virtual bool isDataFlavorSupported(java::awt::datatransfer::DataFlavor* arg1); // public boolean java.awt.dnd.DropTargetDropEvent.isDataFlavorSupported(java.awt.datatransfer.DataFlavor)
    virtual void acceptDrop(int arg1); // public void java.awt.dnd.DropTargetDropEvent.acceptDrop(int)
    virtual void dropComplete(bool arg1); // public void java.awt.dnd.DropTargetDropEvent.dropComplete(boolean)
    virtual JavaObjectArray* getCurrentDataFlavors(); // public java.awt.datatransfer.DataFlavor[] java.awt.dnd.DropTargetDropEvent.getCurrentDataFlavors()
    virtual java::util::List* getCurrentDataFlavorsAsList(); // public java.util.List java.awt.dnd.DropTargetDropEvent.getCurrentDataFlavorsAsList()
    virtual java::awt::datatransfer::Transferable* getTransferable(); // public java.awt.datatransfer.Transferable java.awt.dnd.DropTargetDropEvent.getTransferable()
    virtual int getDropAction(); // public int java.awt.dnd.DropTargetDropEvent.getDropAction()
    virtual bool isLocalTransfer(); // public boolean java.awt.dnd.DropTargetDropEvent.isLocalTransfer()

};
}
}
}
#endif
