#ifndef java_lang_SecurityManager_H
#define java_lang_SecurityManager_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace lang {
class String;
}
}

namespace java {
namespace lang {
class Class;
}
}

namespace java {
namespace security {
class Permission;
}
}

namespace java {
namespace lang {
class ThreadGroup;
}
}

namespace java {
namespace lang {
class Thread;
}
}

namespace java {
namespace io {
class FileDescriptor;
}
}

namespace java {
namespace net {
class InetAddress;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace lang {
class SecurityManager : public java::lang::Object {
  public:
    SecurityManager(JavaMarker* dummy);
    SecurityManager(jobject obj);
    SecurityManager(); // public java.lang.SecurityManager()

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual void checkPackageAccess(const char* arg1); // public void java.lang.SecurityManager.checkPackageAccess(java.lang.String)
    virtual void checkMemberAccess(java::lang::Class* arg1, int arg2); // public void java.lang.SecurityManager.checkMemberAccess(java.lang.Class,int)
    virtual void checkPermission(java::security::Permission* arg1); // public void java.lang.SecurityManager.checkPermission(java.security.Permission)
    virtual void checkPermission(java::security::Permission* arg1, java::lang::Object* arg2); // public void java.lang.SecurityManager.checkPermission(java.security.Permission,java.lang.Object)
    virtual void checkCreateClassLoader(); // public void java.lang.SecurityManager.checkCreateClassLoader()
    virtual void checkPropertiesAccess(); // public void java.lang.SecurityManager.checkPropertiesAccess()
    virtual void checkPropertyAccess(const char* arg1); // public void java.lang.SecurityManager.checkPropertyAccess(java.lang.String)
    virtual java::lang::ThreadGroup* getThreadGroup(); // public java.lang.ThreadGroup java.lang.SecurityManager.getThreadGroup()
    virtual void checkAccess(java::lang::ThreadGroup* arg1); // public void java.lang.SecurityManager.checkAccess(java.lang.ThreadGroup)
    virtual void checkAccess(java::lang::Thread* arg1); // public void java.lang.SecurityManager.checkAccess(java.lang.Thread)
    virtual void checkRead(const char* arg1); // public void java.lang.SecurityManager.checkRead(java.lang.String)
    virtual void checkRead(const char* arg1, java::lang::Object* arg2); // public void java.lang.SecurityManager.checkRead(java.lang.String,java.lang.Object)
    virtual void checkRead(java::io::FileDescriptor* arg1); // public void java.lang.SecurityManager.checkRead(java.io.FileDescriptor)
    virtual void checkWrite(const char* arg1); // public void java.lang.SecurityManager.checkWrite(java.lang.String)
    virtual void checkWrite(java::io::FileDescriptor* arg1); // public void java.lang.SecurityManager.checkWrite(java.io.FileDescriptor)
    virtual void checkExit(int arg1); // public void java.lang.SecurityManager.checkExit(int)
    virtual void checkLink(const char* arg1); // public void java.lang.SecurityManager.checkLink(java.lang.String)
    virtual void checkDelete(const char* arg1); // public void java.lang.SecurityManager.checkDelete(java.lang.String)
    virtual void checkExec(const char* arg1); // public void java.lang.SecurityManager.checkExec(java.lang.String)
    virtual void checkConnect(const char* arg1, int arg2); // public void java.lang.SecurityManager.checkConnect(java.lang.String,int)
    virtual void checkConnect(const char* arg1, int arg2, java::lang::Object* arg3); // public void java.lang.SecurityManager.checkConnect(java.lang.String,int,java.lang.Object)
    virtual void checkSetFactory(); // public void java.lang.SecurityManager.checkSetFactory()
    virtual void checkSecurityAccess(const char* arg1); // public void java.lang.SecurityManager.checkSecurityAccess(java.lang.String)
    virtual void checkAccept(const char* arg1, int arg2); // public void java.lang.SecurityManager.checkAccept(java.lang.String,int)
    virtual void checkAwtEventQueueAccess(); // public void java.lang.SecurityManager.checkAwtEventQueueAccess()
    virtual void checkListen(int arg1); // public void java.lang.SecurityManager.checkListen(int)
    virtual void checkMulticast(java::net::InetAddress* arg1, char arg2); // public void java.lang.SecurityManager.checkMulticast(java.net.InetAddress,byte)
    virtual void checkMulticast(java::net::InetAddress* arg1); // public void java.lang.SecurityManager.checkMulticast(java.net.InetAddress)
    virtual void checkPackageDefinition(const char* arg1); // public void java.lang.SecurityManager.checkPackageDefinition(java.lang.String)
    virtual void checkPrintJobAccess(); // public void java.lang.SecurityManager.checkPrintJobAccess()
    virtual void checkSystemClipboardAccess(); // public void java.lang.SecurityManager.checkSystemClipboardAccess()
    virtual bool checkTopLevelWindow(java::lang::Object* arg1); // public boolean java.lang.SecurityManager.checkTopLevelWindow(java.lang.Object)
    virtual bool getInCheck(); // public boolean java.lang.SecurityManager.getInCheck()
    virtual java::lang::Object* getSecurityContext(); // public java.lang.Object java.lang.SecurityManager.getSecurityContext()

};
}
}
#endif
