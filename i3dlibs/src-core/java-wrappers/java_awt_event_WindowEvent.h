#ifndef java_awt_event_WindowEvent_H
#define java_awt_event_WindowEvent_H
#include <jni.h>
#include <java_marker.h>
#include <java_awt_event_ComponentEvent.h>

namespace java {
namespace awt {
class Window;
}
}

namespace java {
namespace lang {
class String;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace event {
class WindowEvent : public java::awt::event::ComponentEvent {
  public:
    WindowEvent(JavaMarker* dummy);
    WindowEvent(jobject obj);
    WindowEvent(java::awt::Window* arg1, int arg2, java::awt::Window* arg3); // public java.awt.event.WindowEvent(java.awt.Window,int,java.awt.Window)
    WindowEvent(java::awt::Window* arg1, int arg2, int arg3, int arg4); // public java.awt.event.WindowEvent(java.awt.Window,int,int,int)
    WindowEvent(java::awt::Window* arg1, int arg2); // public java.awt.event.WindowEvent(java.awt.Window,int)
    WindowEvent(java::awt::Window* arg1, int arg2, java::awt::Window* arg3, int arg4, int arg5); // public java.awt.event.WindowEvent(java.awt.Window,int,java.awt.Window,int,int)

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual const char* paramString(); // public java.lang.String java.awt.event.WindowEvent.paramString()
    virtual int getNewState(); // public int java.awt.event.WindowEvent.getNewState()
    virtual int getOldState(); // public int java.awt.event.WindowEvent.getOldState()
    virtual java::awt::Window* getOppositeWindow(); // public java.awt.Window java.awt.event.WindowEvent.getOppositeWindow()
    virtual java::awt::Window* getWindow(); // public java.awt.Window java.awt.event.WindowEvent.getWindow()

    int WINDOW_FIRST; // public static final int java.awt.event.WindowEvent.WINDOW_FIRST
    int WINDOW_OPENED; // public static final int java.awt.event.WindowEvent.WINDOW_OPENED
    int WINDOW_CLOSING; // public static final int java.awt.event.WindowEvent.WINDOW_CLOSING
    int WINDOW_CLOSED; // public static final int java.awt.event.WindowEvent.WINDOW_CLOSED
    int WINDOW_ICONIFIED; // public static final int java.awt.event.WindowEvent.WINDOW_ICONIFIED
    int WINDOW_DEICONIFIED; // public static final int java.awt.event.WindowEvent.WINDOW_DEICONIFIED
    int WINDOW_ACTIVATED; // public static final int java.awt.event.WindowEvent.WINDOW_ACTIVATED
    int WINDOW_DEACTIVATED; // public static final int java.awt.event.WindowEvent.WINDOW_DEACTIVATED
    int WINDOW_GAINED_FOCUS; // public static final int java.awt.event.WindowEvent.WINDOW_GAINED_FOCUS
    int WINDOW_LOST_FOCUS; // public static final int java.awt.event.WindowEvent.WINDOW_LOST_FOCUS
    int WINDOW_STATE_CHANGED; // public static final int java.awt.event.WindowEvent.WINDOW_STATE_CHANGED
    int WINDOW_LAST; // public static final int java.awt.event.WindowEvent.WINDOW_LAST
};
}
}
}
#endif
