#ifndef java_awt_PageAttributes__ColorType_H
#define java_awt_PageAttributes__ColorType_H
#include <jni.h>
#include <java_marker.h>
#include <java_awt_AttributeValue.h>

namespace java {
namespace lang {
class String;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
class PageAttributes__ColorType : public java::awt::AttributeValue {
  public:
    PageAttributes__ColorType(JavaMarker* dummy);
    PageAttributes__ColorType(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual int hashCode(); // public int java.awt.PageAttributes$ColorType.hashCode()
    virtual const char* toString(); // public java.lang.String java.awt.PageAttributes$ColorType.toString()

    java::awt::PageAttributes__ColorType* COLOR; // public static final java.awt.PageAttributes$ColorType java.awt.PageAttributes$ColorType.COLOR
    java::awt::PageAttributes__ColorType* MONOCHROME; // public static final java.awt.PageAttributes$ColorType java.awt.PageAttributes$ColorType.MONOCHROME
};
}
}
#endif
