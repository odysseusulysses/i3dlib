#ifndef java_awt_event_AWTEventListener_H
#define java_awt_event_AWTEventListener_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace util {
class EventListener;
}
}

namespace java {
namespace awt {
class AWTEvent;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace event {
class AWTEventListener : public java::lang::Object {
  public:
    AWTEventListener(JavaMarker* dummy);
    AWTEventListener(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual void eventDispatched(java::awt::AWTEvent* arg1); // public abstract void java.awt.event.AWTEventListener.eventDispatched(java.awt.AWTEvent)

};
}
}
}
#endif
