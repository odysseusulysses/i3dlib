#ifndef javax_accessibility_AccessibleValue_H
#define javax_accessibility_AccessibleValue_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace lang {
class Number;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace javax {
namespace accessibility {
class AccessibleValue : public java::lang::Object {
  public:
    AccessibleValue(JavaMarker* dummy);
    AccessibleValue(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual java::lang::Number* getCurrentAccessibleValue(); // public abstract java.lang.Number javax.accessibility.AccessibleValue.getCurrentAccessibleValue()
    virtual java::lang::Number* getMaximumAccessibleValue(); // public abstract java.lang.Number javax.accessibility.AccessibleValue.getMaximumAccessibleValue()
    virtual java::lang::Number* getMinimumAccessibleValue(); // public abstract java.lang.Number javax.accessibility.AccessibleValue.getMinimumAccessibleValue()
    virtual bool setCurrentAccessibleValue(java::lang::Number* arg1); // public abstract boolean javax.accessibility.AccessibleValue.setCurrentAccessibleValue(java.lang.Number)

};
}
}
#endif
