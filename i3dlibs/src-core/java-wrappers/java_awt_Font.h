#ifndef java_awt_Font_H
#define java_awt_Font_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace io {
class Serializable;
}
}

namespace java {
namespace util {
class Map;
}
}

namespace java {
namespace lang {
class String;
}
}

namespace java {
namespace awt {
namespace geom {
class AffineTransform;
}
}
}

namespace java {
namespace text {
class CharacterIterator;
}
}

namespace java {
namespace io {
class InputStream;
}
}

namespace java {
namespace io {
class File;
}
}

namespace java {
namespace awt {
namespace font {
class GlyphVector;
}
}
}

namespace java {
namespace awt {
namespace font {
class FontRenderContext;
}
}
}

namespace java {
namespace text {
class AttributedCharacterIterator__Attribute;
}
}

namespace java {
namespace util {
class Locale;
}
}

namespace java {
namespace awt {
namespace font {
class LineMetrics;
}
}
}

namespace java {
namespace awt {
namespace geom {
class Rectangle2D;
}
}
}

namespace java {
namespace awt {
namespace peer {
class FontPeer;
}
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
class Font : public java::lang::Object {
  public:
    Font(JavaMarker* dummy);
    Font(jobject obj);
    Font(java::util::Map* arg1); // public java.awt.Font(java.util.Map)
    Font(const char* arg1, int arg2, int arg3); // public java.awt.Font(java.lang.String,int,int)

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual int hashCode(); // public int java.awt.Font.hashCode()
    virtual bool equals(java::lang::Object* arg1); // public boolean java.awt.Font.equals(java.lang.Object)
    virtual const char* toString(); // public java.lang.String java.awt.Font.toString()
    virtual const char* getName(); // public java.lang.String java.awt.Font.getName()
    virtual java::awt::Font* decode(const char* arg1); // public static java.awt.Font java.awt.Font.decode(java.lang.String)
    virtual int getSize(); // public int java.awt.Font.getSize()
    virtual java::util::Map* getAttributes(); // public java.util.Map java.awt.Font.getAttributes()
    virtual java::awt::geom::AffineTransform* getTransform(); // public java.awt.geom.AffineTransform java.awt.Font.getTransform()
    virtual java::awt::Font* getFont(const char* arg1, java::awt::Font* arg2); // public static java.awt.Font java.awt.Font.getFont(java.lang.String,java.awt.Font)
    virtual java::awt::Font* getFont(const char* arg1); // public static java.awt.Font java.awt.Font.getFont(java.lang.String)
    virtual java::awt::Font* getFont(java::util::Map* arg1); // public static java.awt.Font java.awt.Font.getFont(java.util.Map)
    virtual int getNumGlyphs(); // public int java.awt.Font.getNumGlyphs()
    virtual bool isTransformed(); // public boolean java.awt.Font.isTransformed()
    virtual bool canDisplay(char arg1); // public boolean java.awt.Font.canDisplay(char)
    virtual bool canDisplay(int arg1); // public boolean java.awt.Font.canDisplay(int)
    virtual int canDisplayUpTo(const char* arg1); // public int java.awt.Font.canDisplayUpTo(java.lang.String)
    virtual int canDisplayUpTo(JavaCharArray* arg1, int arg2, int arg3); // public int java.awt.Font.canDisplayUpTo(char[],int,int)
    virtual int canDisplayUpTo(java::text::CharacterIterator* arg1, int arg2, int arg3); // public int java.awt.Font.canDisplayUpTo(java.text.CharacterIterator,int,int)
    virtual java::awt::Font* createFont(int arg1, java::io::InputStream* arg2); // public static java.awt.Font java.awt.Font.createFont(int,java.io.InputStream) throws java.awt.FontFormatException,java.io.IOException
    virtual java::awt::Font* createFont(int arg1, java::io::File* arg2); // public static java.awt.Font java.awt.Font.createFont(int,java.io.File) throws java.awt.FontFormatException,java.io.IOException
    virtual java::awt::font::GlyphVector* createGlyphVector(java::awt::font::FontRenderContext* arg1, JavaIntArray* arg2); // public java.awt.font.GlyphVector java.awt.Font.createGlyphVector(java.awt.font.FontRenderContext,int[])
    virtual java::awt::font::GlyphVector* createGlyphVector(java::awt::font::FontRenderContext* arg1, java::text::CharacterIterator* arg2); // public java.awt.font.GlyphVector java.awt.Font.createGlyphVector(java.awt.font.FontRenderContext,java.text.CharacterIterator)
    virtual java::awt::font::GlyphVector* createGlyphVector(java::awt::font::FontRenderContext* arg1, JavaCharArray* arg2); // public java.awt.font.GlyphVector java.awt.Font.createGlyphVector(java.awt.font.FontRenderContext,char[])
    virtual java::awt::font::GlyphVector* createGlyphVector(java::awt::font::FontRenderContext* arg1, const char* arg2); // public java.awt.font.GlyphVector java.awt.Font.createGlyphVector(java.awt.font.FontRenderContext,java.lang.String)
    virtual java::awt::Font* deriveFont(java::awt::geom::AffineTransform* arg1); // public java.awt.Font java.awt.Font.deriveFont(java.awt.geom.AffineTransform)
    virtual java::awt::Font* deriveFont(java::util::Map* arg1); // public java.awt.Font java.awt.Font.deriveFont(java.util.Map)
    virtual java::awt::Font* deriveFont(int arg1); // public java.awt.Font java.awt.Font.deriveFont(int)
    virtual java::awt::Font* deriveFont(float arg1); // public java.awt.Font java.awt.Font.deriveFont(float)
    virtual java::awt::Font* deriveFont(int arg1, java::awt::geom::AffineTransform* arg2); // public java.awt.Font java.awt.Font.deriveFont(int,java.awt.geom.AffineTransform)
    virtual java::awt::Font* deriveFont(int arg1, float arg2); // public java.awt.Font java.awt.Font.deriveFont(int,float)
    virtual JavaObjectArray* getAvailableAttributes(); // public java.text.AttributedCharacterIterator$Attribute[] java.awt.Font.getAvailableAttributes()
    virtual char getBaselineFor(char arg1); // public byte java.awt.Font.getBaselineFor(char)
    virtual const char* getFamily(java::util::Locale* arg1); // public java.lang.String java.awt.Font.getFamily(java.util.Locale)
    virtual const char* getFamily(); // public java.lang.String java.awt.Font.getFamily()
    virtual const char* getFontName(); // public java.lang.String java.awt.Font.getFontName()
    virtual const char* getFontName(java::util::Locale* arg1); // public java.lang.String java.awt.Font.getFontName(java.util.Locale)
    virtual float getItalicAngle(); // public float java.awt.Font.getItalicAngle()
    virtual java::awt::font::LineMetrics* getLineMetrics(const char* arg1, int arg2, int arg3, java::awt::font::FontRenderContext* arg4); // public java.awt.font.LineMetrics java.awt.Font.getLineMetrics(java.lang.String,int,int,java.awt.font.FontRenderContext)
    virtual java::awt::font::LineMetrics* getLineMetrics(JavaCharArray* arg1, int arg2, int arg3, java::awt::font::FontRenderContext* arg4); // public java.awt.font.LineMetrics java.awt.Font.getLineMetrics(char[],int,int,java.awt.font.FontRenderContext)
    virtual java::awt::font::LineMetrics* getLineMetrics(const char* arg1, java::awt::font::FontRenderContext* arg2); // public java.awt.font.LineMetrics java.awt.Font.getLineMetrics(java.lang.String,java.awt.font.FontRenderContext)
    virtual java::awt::font::LineMetrics* getLineMetrics(java::text::CharacterIterator* arg1, int arg2, int arg3, java::awt::font::FontRenderContext* arg4); // public java.awt.font.LineMetrics java.awt.Font.getLineMetrics(java.text.CharacterIterator,int,int,java.awt.font.FontRenderContext)
    virtual java::awt::geom::Rectangle2D* getMaxCharBounds(java::awt::font::FontRenderContext* arg1); // public java.awt.geom.Rectangle2D java.awt.Font.getMaxCharBounds(java.awt.font.FontRenderContext)
    virtual int getMissingGlyphCode(); // public int java.awt.Font.getMissingGlyphCode()
    virtual const char* getPSName(); // public java.lang.String java.awt.Font.getPSName()
    virtual java::awt::peer::FontPeer* getPeer(); // public java.awt.peer.FontPeer java.awt.Font.getPeer()
    virtual float getSize2D(); // public float java.awt.Font.getSize2D()
    virtual java::awt::geom::Rectangle2D* getStringBounds(const char* arg1, java::awt::font::FontRenderContext* arg2); // public java.awt.geom.Rectangle2D java.awt.Font.getStringBounds(java.lang.String,java.awt.font.FontRenderContext)
    virtual java::awt::geom::Rectangle2D* getStringBounds(java::text::CharacterIterator* arg1, int arg2, int arg3, java::awt::font::FontRenderContext* arg4); // public java.awt.geom.Rectangle2D java.awt.Font.getStringBounds(java.text.CharacterIterator,int,int,java.awt.font.FontRenderContext)
    virtual java::awt::geom::Rectangle2D* getStringBounds(JavaCharArray* arg1, int arg2, int arg3, java::awt::font::FontRenderContext* arg4); // public java.awt.geom.Rectangle2D java.awt.Font.getStringBounds(char[],int,int,java.awt.font.FontRenderContext)
    virtual java::awt::geom::Rectangle2D* getStringBounds(const char* arg1, int arg2, int arg3, java::awt::font::FontRenderContext* arg4); // public java.awt.geom.Rectangle2D java.awt.Font.getStringBounds(java.lang.String,int,int,java.awt.font.FontRenderContext)
    virtual int getStyle(); // public int java.awt.Font.getStyle()
    virtual bool hasLayoutAttributes(); // public boolean java.awt.Font.hasLayoutAttributes()
    virtual bool hasUniformLineMetrics(); // public boolean java.awt.Font.hasUniformLineMetrics()
    virtual bool isBold(); // public boolean java.awt.Font.isBold()
    virtual bool isItalic(); // public boolean java.awt.Font.isItalic()
    virtual bool isPlain(); // public boolean java.awt.Font.isPlain()
    virtual java::awt::font::GlyphVector* layoutGlyphVector(java::awt::font::FontRenderContext* arg1, JavaCharArray* arg2, int arg3, int arg4, int arg5); // public java.awt.font.GlyphVector java.awt.Font.layoutGlyphVector(java.awt.font.FontRenderContext,char[],int,int,int)

    const char* DIALOG; // public static final java.lang.String java.awt.Font.DIALOG
    const char* DIALOG_INPUT; // public static final java.lang.String java.awt.Font.DIALOG_INPUT
    const char* SANS_SERIF; // public static final java.lang.String java.awt.Font.SANS_SERIF
    const char* SERIF; // public static final java.lang.String java.awt.Font.SERIF
    const char* MONOSPACED; // public static final java.lang.String java.awt.Font.MONOSPACED
    int PLAIN; // public static final int java.awt.Font.PLAIN
    int BOLD; // public static final int java.awt.Font.BOLD
    int ITALIC; // public static final int java.awt.Font.ITALIC
    int ROMAN_BASELINE; // public static final int java.awt.Font.ROMAN_BASELINE
    int CENTER_BASELINE; // public static final int java.awt.Font.CENTER_BASELINE
    int HANGING_BASELINE; // public static final int java.awt.Font.HANGING_BASELINE
    int TRUETYPE_FONT; // public static final int java.awt.Font.TRUETYPE_FONT
    int TYPE1_FONT; // public static final int java.awt.Font.TYPE1_FONT
    int LAYOUT_LEFT_TO_RIGHT; // public static final int java.awt.Font.LAYOUT_LEFT_TO_RIGHT
    int LAYOUT_RIGHT_TO_LEFT; // public static final int java.awt.Font.LAYOUT_RIGHT_TO_LEFT
    int LAYOUT_NO_START_CONTEXT; // public static final int java.awt.Font.LAYOUT_NO_START_CONTEXT
    int LAYOUT_NO_LIMIT_CONTEXT; // public static final int java.awt.Font.LAYOUT_NO_LIMIT_CONTEXT
};
}
}
#endif
