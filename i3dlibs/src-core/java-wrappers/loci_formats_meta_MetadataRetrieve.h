#ifndef loci_formats_meta_MetadataRetrieve_H
#define loci_formats_meta_MetadataRetrieve_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace lang {
class String;
}
}

namespace java {
namespace lang {
class Integer;
}
}

namespace java {
namespace lang {
class Boolean;
}
}

namespace java {
namespace lang {
class Float;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace loci {
namespace formats {
namespace meta {
class MetadataRetrieve : public java::lang::Object {
  public:
    MetadataRetrieve(JavaMarker* dummy);
    MetadataRetrieve(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual java::lang::Object* getPlane(int arg1, int arg2, int arg3); // public abstract java.lang.Object loci.formats.meta.MetadataRetrieve.getPlane(int,int,int)
    virtual int getImageCount(); // public abstract int loci.formats.meta.MetadataRetrieve.getImageCount()
    virtual int getPixelsCount(int arg1); // public abstract int loci.formats.meta.MetadataRetrieve.getPixelsCount(int)
    virtual int getPlaneCount(int arg1, int arg2); // public abstract int loci.formats.meta.MetadataRetrieve.getPlaneCount(int,int)
    virtual int getLogicalChannelCount(int arg1); // public abstract int loci.formats.meta.MetadataRetrieve.getLogicalChannelCount(int)
    virtual int getROICount(int arg1); // public abstract int loci.formats.meta.MetadataRetrieve.getROICount(int)
    virtual int getInstrumentCount(); // public abstract int loci.formats.meta.MetadataRetrieve.getInstrumentCount()
    virtual int getLightSourceCount(int arg1); // public abstract int loci.formats.meta.MetadataRetrieve.getLightSourceCount(int)
    virtual int getDetectorCount(int arg1); // public abstract int loci.formats.meta.MetadataRetrieve.getDetectorCount(int)
    virtual int getObjectiveCount(int arg1); // public abstract int loci.formats.meta.MetadataRetrieve.getObjectiveCount(int)
    virtual int getOTFCount(int arg1); // public abstract int loci.formats.meta.MetadataRetrieve.getOTFCount(int)
    virtual int getExperimenterCount(); // public abstract int loci.formats.meta.MetadataRetrieve.getExperimenterCount()
    virtual java::lang::Object* getImage(int arg1); // public abstract java.lang.Object loci.formats.meta.MetadataRetrieve.getImage(int)
    virtual java::lang::Object* getPixels(int arg1, int arg2); // public abstract java.lang.Object loci.formats.meta.MetadataRetrieve.getPixels(int,int)
    virtual java::lang::Object* getDimensions(int arg1, int arg2); // public abstract java.lang.Object loci.formats.meta.MetadataRetrieve.getDimensions(int,int)
    virtual java::lang::Object* getImagingEnvironment(int arg1); // public abstract java.lang.Object loci.formats.meta.MetadataRetrieve.getImagingEnvironment(int)
    virtual java::lang::Object* getPlaneTiming(int arg1, int arg2, int arg3); // public abstract java.lang.Object loci.formats.meta.MetadataRetrieve.getPlaneTiming(int,int,int)
    virtual java::lang::Object* getStagePosition(int arg1, int arg2, int arg3); // public abstract java.lang.Object loci.formats.meta.MetadataRetrieve.getStagePosition(int,int,int)
    virtual java::lang::Object* getLogicalChannel(int arg1, int arg2); // public abstract java.lang.Object loci.formats.meta.MetadataRetrieve.getLogicalChannel(int,int)
    virtual java::lang::Object* getDetectorSettings(int arg1, int arg2); // public abstract java.lang.Object loci.formats.meta.MetadataRetrieve.getDetectorSettings(int,int)
    virtual java::lang::Object* getLightSourceSettings(int arg1, int arg2); // public abstract java.lang.Object loci.formats.meta.MetadataRetrieve.getLightSourceSettings(int,int)
    virtual java::lang::Object* getROI(int arg1, int arg2); // public abstract java.lang.Object loci.formats.meta.MetadataRetrieve.getROI(int,int)
    virtual java::lang::Object* getStageLabel(int arg1); // public abstract java.lang.Object loci.formats.meta.MetadataRetrieve.getStageLabel(int)
    virtual java::lang::Object* getInstrument(int arg1); // public abstract java.lang.Object loci.formats.meta.MetadataRetrieve.getInstrument(int)
    virtual java::lang::Object* getLightSource(int arg1, int arg2); // public abstract java.lang.Object loci.formats.meta.MetadataRetrieve.getLightSource(int,int)
    virtual java::lang::Object* getLaser(int arg1, int arg2); // public abstract java.lang.Object loci.formats.meta.MetadataRetrieve.getLaser(int,int)
    virtual java::lang::Object* getFilament(int arg1, int arg2); // public abstract java.lang.Object loci.formats.meta.MetadataRetrieve.getFilament(int,int)
    virtual java::lang::Object* getArc(int arg1, int arg2); // public abstract java.lang.Object loci.formats.meta.MetadataRetrieve.getArc(int,int)
    virtual java::lang::Object* getDetector(int arg1, int arg2); // public abstract java.lang.Object loci.formats.meta.MetadataRetrieve.getDetector(int,int)
    virtual java::lang::Object* getObjective(int arg1, int arg2); // public abstract java.lang.Object loci.formats.meta.MetadataRetrieve.getObjective(int,int)
    virtual java::lang::Object* getOTF(int arg1, int arg2); // public abstract java.lang.Object loci.formats.meta.MetadataRetrieve.getOTF(int,int)
    virtual java::lang::Object* getExperimenter(int arg1); // public abstract java.lang.Object loci.formats.meta.MetadataRetrieve.getExperimenter(int)
    virtual const char* getImageName(int arg1); // public abstract java.lang.String loci.formats.meta.MetadataRetrieve.getImageName(int)
    virtual const char* getImageCreationDate(int arg1); // public abstract java.lang.String loci.formats.meta.MetadataRetrieve.getImageCreationDate(int)
    virtual const char* getImageDescription(int arg1); // public abstract java.lang.String loci.formats.meta.MetadataRetrieve.getImageDescription(int)
    virtual java::lang::Integer* getPixelsSizeX(int arg1, int arg2); // public abstract java.lang.Integer loci.formats.meta.MetadataRetrieve.getPixelsSizeX(int,int)
    virtual java::lang::Integer* getPixelsSizeY(int arg1, int arg2); // public abstract java.lang.Integer loci.formats.meta.MetadataRetrieve.getPixelsSizeY(int,int)
    virtual java::lang::Integer* getPixelsSizeZ(int arg1, int arg2); // public abstract java.lang.Integer loci.formats.meta.MetadataRetrieve.getPixelsSizeZ(int,int)
    virtual java::lang::Integer* getPixelsSizeC(int arg1, int arg2); // public abstract java.lang.Integer loci.formats.meta.MetadataRetrieve.getPixelsSizeC(int,int)
    virtual java::lang::Integer* getPixelsSizeT(int arg1, int arg2); // public abstract java.lang.Integer loci.formats.meta.MetadataRetrieve.getPixelsSizeT(int,int)
    virtual const char* getPixelsPixelType(int arg1, int arg2); // public abstract java.lang.String loci.formats.meta.MetadataRetrieve.getPixelsPixelType(int,int)
    virtual java::lang::Boolean* getPixelsBigEndian(int arg1, int arg2); // public abstract java.lang.Boolean loci.formats.meta.MetadataRetrieve.getPixelsBigEndian(int,int)
    virtual const char* getPixelsDimensionOrder(int arg1, int arg2); // public abstract java.lang.String loci.formats.meta.MetadataRetrieve.getPixelsDimensionOrder(int,int)
    virtual java::lang::Float* getDimensionsPhysicalSizeX(int arg1, int arg2); // public abstract java.lang.Float loci.formats.meta.MetadataRetrieve.getDimensionsPhysicalSizeX(int,int)
    virtual java::lang::Float* getDimensionsPhysicalSizeY(int arg1, int arg2); // public abstract java.lang.Float loci.formats.meta.MetadataRetrieve.getDimensionsPhysicalSizeY(int,int)
    virtual java::lang::Float* getDimensionsPhysicalSizeZ(int arg1, int arg2); // public abstract java.lang.Float loci.formats.meta.MetadataRetrieve.getDimensionsPhysicalSizeZ(int,int)
    virtual java::lang::Float* getDimensionsTimeIncrement(int arg1, int arg2); // public abstract java.lang.Float loci.formats.meta.MetadataRetrieve.getDimensionsTimeIncrement(int,int)
    virtual java::lang::Integer* getDimensionsWaveStart(int arg1, int arg2); // public abstract java.lang.Integer loci.formats.meta.MetadataRetrieve.getDimensionsWaveStart(int,int)
    virtual java::lang::Integer* getDimensionsWaveIncrement(int arg1, int arg2); // public abstract java.lang.Integer loci.formats.meta.MetadataRetrieve.getDimensionsWaveIncrement(int,int)
    virtual java::lang::Float* getImagingEnvironmentTemperature(int arg1); // public abstract java.lang.Float loci.formats.meta.MetadataRetrieve.getImagingEnvironmentTemperature(int)
    virtual java::lang::Float* getImagingEnvironmentAirPressure(int arg1); // public abstract java.lang.Float loci.formats.meta.MetadataRetrieve.getImagingEnvironmentAirPressure(int)
    virtual java::lang::Float* getImagingEnvironmentHumidity(int arg1); // public abstract java.lang.Float loci.formats.meta.MetadataRetrieve.getImagingEnvironmentHumidity(int)
    virtual java::lang::Float* getImagingEnvironmentCO2Percent(int arg1); // public abstract java.lang.Float loci.formats.meta.MetadataRetrieve.getImagingEnvironmentCO2Percent(int)
    virtual java::lang::Integer* getPlaneTheZ(int arg1, int arg2, int arg3); // public abstract java.lang.Integer loci.formats.meta.MetadataRetrieve.getPlaneTheZ(int,int,int)
    virtual java::lang::Integer* getPlaneTheC(int arg1, int arg2, int arg3); // public abstract java.lang.Integer loci.formats.meta.MetadataRetrieve.getPlaneTheC(int,int,int)
    virtual java::lang::Integer* getPlaneTheT(int arg1, int arg2, int arg3); // public abstract java.lang.Integer loci.formats.meta.MetadataRetrieve.getPlaneTheT(int,int,int)
    virtual java::lang::Float* getPlaneTimingDeltaT(int arg1, int arg2, int arg3); // public abstract java.lang.Float loci.formats.meta.MetadataRetrieve.getPlaneTimingDeltaT(int,int,int)
    virtual java::lang::Float* getPlaneTimingExposureTime(int arg1, int arg2, int arg3); // public abstract java.lang.Float loci.formats.meta.MetadataRetrieve.getPlaneTimingExposureTime(int,int,int)
    virtual java::lang::Float* getStagePositionPositionX(int arg1, int arg2, int arg3); // public abstract java.lang.Float loci.formats.meta.MetadataRetrieve.getStagePositionPositionX(int,int,int)
    virtual java::lang::Float* getStagePositionPositionY(int arg1, int arg2, int arg3); // public abstract java.lang.Float loci.formats.meta.MetadataRetrieve.getStagePositionPositionY(int,int,int)
    virtual java::lang::Float* getStagePositionPositionZ(int arg1, int arg2, int arg3); // public abstract java.lang.Float loci.formats.meta.MetadataRetrieve.getStagePositionPositionZ(int,int,int)
    virtual const char* getLogicalChannelName(int arg1, int arg2); // public abstract java.lang.String loci.formats.meta.MetadataRetrieve.getLogicalChannelName(int,int)
    virtual java::lang::Integer* getLogicalChannelSamplesPerPixel(int arg1, int arg2); // public abstract java.lang.Integer loci.formats.meta.MetadataRetrieve.getLogicalChannelSamplesPerPixel(int,int)
    virtual const char* getLogicalChannelIlluminationType(int arg1, int arg2); // public abstract java.lang.String loci.formats.meta.MetadataRetrieve.getLogicalChannelIlluminationType(int,int)
    virtual java::lang::Integer* getLogicalChannelPinholeSize(int arg1, int arg2); // public abstract java.lang.Integer loci.formats.meta.MetadataRetrieve.getLogicalChannelPinholeSize(int,int)
    virtual const char* getLogicalChannelPhotometricInterpretation(int arg1, int arg2); // public abstract java.lang.String loci.formats.meta.MetadataRetrieve.getLogicalChannelPhotometricInterpretation(int,int)
    virtual const char* getLogicalChannelMode(int arg1, int arg2); // public abstract java.lang.String loci.formats.meta.MetadataRetrieve.getLogicalChannelMode(int,int)
    virtual const char* getLogicalChannelContrastMethod(int arg1, int arg2); // public abstract java.lang.String loci.formats.meta.MetadataRetrieve.getLogicalChannelContrastMethod(int,int)
    virtual java::lang::Integer* getLogicalChannelExWave(int arg1, int arg2); // public abstract java.lang.Integer loci.formats.meta.MetadataRetrieve.getLogicalChannelExWave(int,int)
    virtual java::lang::Integer* getLogicalChannelEmWave(int arg1, int arg2); // public abstract java.lang.Integer loci.formats.meta.MetadataRetrieve.getLogicalChannelEmWave(int,int)
    virtual const char* getLogicalChannelFluor(int arg1, int arg2); // public abstract java.lang.String loci.formats.meta.MetadataRetrieve.getLogicalChannelFluor(int,int)
    virtual java::lang::Float* getLogicalChannelNdFilter(int arg1, int arg2); // public abstract java.lang.Float loci.formats.meta.MetadataRetrieve.getLogicalChannelNdFilter(int,int)
    virtual java::lang::Integer* getLogicalChannelPockelCellSetting(int arg1, int arg2); // public abstract java.lang.Integer loci.formats.meta.MetadataRetrieve.getLogicalChannelPockelCellSetting(int,int)
    virtual java::lang::Object* getDetectorSettingsDetector(int arg1, int arg2); // public abstract java.lang.Object loci.formats.meta.MetadataRetrieve.getDetectorSettingsDetector(int,int)
    virtual java::lang::Float* getDetectorSettingsOffset(int arg1, int arg2); // public abstract java.lang.Float loci.formats.meta.MetadataRetrieve.getDetectorSettingsOffset(int,int)
    virtual java::lang::Float* getDetectorSettingsGain(int arg1, int arg2); // public abstract java.lang.Float loci.formats.meta.MetadataRetrieve.getDetectorSettingsGain(int,int)
    virtual java::lang::Object* getLightSourceSettingsLightSource(int arg1, int arg2); // public abstract java.lang.Object loci.formats.meta.MetadataRetrieve.getLightSourceSettingsLightSource(int,int)
    virtual java::lang::Float* getLightSourceSettingsAttenuation(int arg1, int arg2); // public abstract java.lang.Float loci.formats.meta.MetadataRetrieve.getLightSourceSettingsAttenuation(int,int)
    virtual java::lang::Integer* getLightSourceSettingsWavelength(int arg1, int arg2); // public abstract java.lang.Integer loci.formats.meta.MetadataRetrieve.getLightSourceSettingsWavelength(int,int)
    virtual java::lang::Integer* getROIX0(int arg1, int arg2); // public abstract java.lang.Integer loci.formats.meta.MetadataRetrieve.getROIX0(int,int)
    virtual java::lang::Integer* getROIY0(int arg1, int arg2); // public abstract java.lang.Integer loci.formats.meta.MetadataRetrieve.getROIY0(int,int)
    virtual java::lang::Integer* getROIZ0(int arg1, int arg2); // public abstract java.lang.Integer loci.formats.meta.MetadataRetrieve.getROIZ0(int,int)
    virtual java::lang::Integer* getROIT0(int arg1, int arg2); // public abstract java.lang.Integer loci.formats.meta.MetadataRetrieve.getROIT0(int,int)
    virtual java::lang::Integer* getROIX1(int arg1, int arg2); // public abstract java.lang.Integer loci.formats.meta.MetadataRetrieve.getROIX1(int,int)
    virtual java::lang::Integer* getROIY1(int arg1, int arg2); // public abstract java.lang.Integer loci.formats.meta.MetadataRetrieve.getROIY1(int,int)
    virtual java::lang::Integer* getROIZ1(int arg1, int arg2); // public abstract java.lang.Integer loci.formats.meta.MetadataRetrieve.getROIZ1(int,int)
    virtual java::lang::Integer* getROIT1(int arg1, int arg2); // public abstract java.lang.Integer loci.formats.meta.MetadataRetrieve.getROIT1(int,int)
    virtual const char* getStageLabelName(int arg1); // public abstract java.lang.String loci.formats.meta.MetadataRetrieve.getStageLabelName(int)
    virtual java::lang::Float* getStageLabelX(int arg1); // public abstract java.lang.Float loci.formats.meta.MetadataRetrieve.getStageLabelX(int)
    virtual java::lang::Float* getStageLabelY(int arg1); // public abstract java.lang.Float loci.formats.meta.MetadataRetrieve.getStageLabelY(int)
    virtual java::lang::Float* getStageLabelZ(int arg1); // public abstract java.lang.Float loci.formats.meta.MetadataRetrieve.getStageLabelZ(int)
    virtual const char* getLightSourceManufacturer(int arg1, int arg2); // public abstract java.lang.String loci.formats.meta.MetadataRetrieve.getLightSourceManufacturer(int,int)
    virtual const char* getLightSourceModel(int arg1, int arg2); // public abstract java.lang.String loci.formats.meta.MetadataRetrieve.getLightSourceModel(int,int)
    virtual const char* getLightSourceSerialNumber(int arg1, int arg2); // public abstract java.lang.String loci.formats.meta.MetadataRetrieve.getLightSourceSerialNumber(int,int)
    virtual const char* getLaserType(int arg1, int arg2); // public abstract java.lang.String loci.formats.meta.MetadataRetrieve.getLaserType(int,int)
    virtual const char* getLaserLaserMedium(int arg1, int arg2); // public abstract java.lang.String loci.formats.meta.MetadataRetrieve.getLaserLaserMedium(int,int)
    virtual java::lang::Integer* getLaserWavelength(int arg1, int arg2); // public abstract java.lang.Integer loci.formats.meta.MetadataRetrieve.getLaserWavelength(int,int)
    virtual java::lang::Integer* getLaserFrequencyMultiplication(int arg1, int arg2); // public abstract java.lang.Integer loci.formats.meta.MetadataRetrieve.getLaserFrequencyMultiplication(int,int)
    virtual java::lang::Boolean* getLaserTuneable(int arg1, int arg2); // public abstract java.lang.Boolean loci.formats.meta.MetadataRetrieve.getLaserTuneable(int,int)
    virtual const char* getLaserPulse(int arg1, int arg2); // public abstract java.lang.String loci.formats.meta.MetadataRetrieve.getLaserPulse(int,int)
    virtual java::lang::Float* getLaserPower(int arg1, int arg2); // public abstract java.lang.Float loci.formats.meta.MetadataRetrieve.getLaserPower(int,int)
    virtual const char* getFilamentType(int arg1, int arg2); // public abstract java.lang.String loci.formats.meta.MetadataRetrieve.getFilamentType(int,int)
    virtual java::lang::Float* getFilamentPower(int arg1, int arg2); // public abstract java.lang.Float loci.formats.meta.MetadataRetrieve.getFilamentPower(int,int)
    virtual const char* getArcType(int arg1, int arg2); // public abstract java.lang.String loci.formats.meta.MetadataRetrieve.getArcType(int,int)
    virtual java::lang::Float* getArcPower(int arg1, int arg2); // public abstract java.lang.Float loci.formats.meta.MetadataRetrieve.getArcPower(int,int)
    virtual const char* getDetectorManufacturer(int arg1, int arg2); // public abstract java.lang.String loci.formats.meta.MetadataRetrieve.getDetectorManufacturer(int,int)
    virtual const char* getDetectorModel(int arg1, int arg2); // public abstract java.lang.String loci.formats.meta.MetadataRetrieve.getDetectorModel(int,int)
    virtual const char* getDetectorSerialNumber(int arg1, int arg2); // public abstract java.lang.String loci.formats.meta.MetadataRetrieve.getDetectorSerialNumber(int,int)
    virtual const char* getDetectorType(int arg1, int arg2); // public abstract java.lang.String loci.formats.meta.MetadataRetrieve.getDetectorType(int,int)
    virtual java::lang::Float* getDetectorGain(int arg1, int arg2); // public abstract java.lang.Float loci.formats.meta.MetadataRetrieve.getDetectorGain(int,int)
    virtual java::lang::Float* getDetectorVoltage(int arg1, int arg2); // public abstract java.lang.Float loci.formats.meta.MetadataRetrieve.getDetectorVoltage(int,int)
    virtual java::lang::Float* getDetectorOffset(int arg1, int arg2); // public abstract java.lang.Float loci.formats.meta.MetadataRetrieve.getDetectorOffset(int,int)
    virtual const char* getObjectiveManufacturer(int arg1, int arg2); // public abstract java.lang.String loci.formats.meta.MetadataRetrieve.getObjectiveManufacturer(int,int)
    virtual const char* getObjectiveModel(int arg1, int arg2); // public abstract java.lang.String loci.formats.meta.MetadataRetrieve.getObjectiveModel(int,int)
    virtual const char* getObjectiveSerialNumber(int arg1, int arg2); // public abstract java.lang.String loci.formats.meta.MetadataRetrieve.getObjectiveSerialNumber(int,int)
    virtual const char* getObjectiveCorrection(int arg1, int arg2); // public abstract java.lang.String loci.formats.meta.MetadataRetrieve.getObjectiveCorrection(int,int)
    virtual const char* getObjectiveImmersion(int arg1, int arg2); // public abstract java.lang.String loci.formats.meta.MetadataRetrieve.getObjectiveImmersion(int,int)
    virtual java::lang::Float* getObjectiveLensNA(int arg1, int arg2); // public abstract java.lang.Float loci.formats.meta.MetadataRetrieve.getObjectiveLensNA(int,int)
    virtual java::lang::Integer* getObjectiveNominalMagnification(int arg1, int arg2); // public abstract java.lang.Integer loci.formats.meta.MetadataRetrieve.getObjectiveNominalMagnification(int,int)
    virtual java::lang::Float* getObjectiveCalibratedMagnification(int arg1, int arg2); // public abstract java.lang.Float loci.formats.meta.MetadataRetrieve.getObjectiveCalibratedMagnification(int,int)
    virtual java::lang::Float* getObjectiveWorkingDistance(int arg1, int arg2); // public abstract java.lang.Float loci.formats.meta.MetadataRetrieve.getObjectiveWorkingDistance(int,int)
    virtual java::lang::Integer* getOTFSizeX(int arg1, int arg2); // public abstract java.lang.Integer loci.formats.meta.MetadataRetrieve.getOTFSizeX(int,int)
    virtual java::lang::Integer* getOTFSizeY(int arg1, int arg2); // public abstract java.lang.Integer loci.formats.meta.MetadataRetrieve.getOTFSizeY(int,int)
    virtual const char* getOTFPixelType(int arg1, int arg2); // public abstract java.lang.String loci.formats.meta.MetadataRetrieve.getOTFPixelType(int,int)
    virtual const char* getOTFPath(int arg1, int arg2); // public abstract java.lang.String loci.formats.meta.MetadataRetrieve.getOTFPath(int,int)
    virtual java::lang::Boolean* getOTFOpticalAxisAveraged(int arg1, int arg2); // public abstract java.lang.Boolean loci.formats.meta.MetadataRetrieve.getOTFOpticalAxisAveraged(int,int)
    virtual const char* getExperimenterFirstName(int arg1); // public abstract java.lang.String loci.formats.meta.MetadataRetrieve.getExperimenterFirstName(int)
    virtual const char* getExperimenterLastName(int arg1); // public abstract java.lang.String loci.formats.meta.MetadataRetrieve.getExperimenterLastName(int)
    virtual const char* getExperimenterEmail(int arg1); // public abstract java.lang.String loci.formats.meta.MetadataRetrieve.getExperimenterEmail(int)
    virtual const char* getExperimenterInstitution(int arg1); // public abstract java.lang.String loci.formats.meta.MetadataRetrieve.getExperimenterInstitution(int)
    virtual const char* getExperimenterDataDirectory(int arg1); // public abstract java.lang.String loci.formats.meta.MetadataRetrieve.getExperimenterDataDirectory(int)

};
}
}
}
#endif
