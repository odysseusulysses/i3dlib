#ifndef java_lang_Runnable_H
#define java_lang_Runnable_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace lang {
class Runnable : public java::lang::Object {
  public:
    Runnable(JavaMarker* dummy);
    Runnable(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual void run(); // public abstract void java.lang.Runnable.run()

};
}
}
#endif
