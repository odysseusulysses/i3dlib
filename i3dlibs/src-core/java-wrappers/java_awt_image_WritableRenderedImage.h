#ifndef java_awt_image_WritableRenderedImage_H
#define java_awt_image_WritableRenderedImage_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace awt {
namespace image {
class RenderedImage;
}
}
}

namespace java {
namespace awt {
namespace image {
class TileObserver;
}
}
}

namespace java {
namespace awt {
namespace image {
class WritableRaster;
}
}
}

namespace java {
namespace awt {
class Point;
}
}

namespace java {
namespace awt {
namespace image {
class Raster;
}
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace image {
class WritableRenderedImage : public java::lang::Object {
  public:
    WritableRenderedImage(JavaMarker* dummy);
    WritableRenderedImage(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual void addTileObserver(java::awt::image::TileObserver* arg1); // public abstract void java.awt.image.WritableRenderedImage.addTileObserver(java.awt.image.TileObserver)
    virtual java::awt::image::WritableRaster* getWritableTile(int arg1, int arg2); // public abstract java.awt.image.WritableRaster java.awt.image.WritableRenderedImage.getWritableTile(int,int)
    virtual JavaObjectArray* getWritableTileIndices(); // public abstract java.awt.Point[] java.awt.image.WritableRenderedImage.getWritableTileIndices()
    virtual bool hasTileWriters(); // public abstract boolean java.awt.image.WritableRenderedImage.hasTileWriters()
    virtual bool isTileWritable(int arg1, int arg2); // public abstract boolean java.awt.image.WritableRenderedImage.isTileWritable(int,int)
    virtual void releaseWritableTile(int arg1, int arg2); // public abstract void java.awt.image.WritableRenderedImage.releaseWritableTile(int,int)
    virtual void removeTileObserver(java::awt::image::TileObserver* arg1); // public abstract void java.awt.image.WritableRenderedImage.removeTileObserver(java.awt.image.TileObserver)
    virtual void setData(java::awt::image::Raster* arg1); // public abstract void java.awt.image.WritableRenderedImage.setData(java.awt.image.Raster)

};
}
}
}
#endif
