#ifndef java_util_EventObject_H
#define java_util_EventObject_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace io {
class Serializable;
}
}

namespace java {
namespace lang {
class String;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace util {
class EventObject : public java::lang::Object {
  public:
    EventObject(JavaMarker* dummy);
    EventObject(jobject obj);
    EventObject(java::lang::Object* arg1); // public java.util.EventObject(java.lang.Object)

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual const char* toString(); // public java.lang.String java.util.EventObject.toString()
    virtual java::lang::Object* getSource(); // public java.lang.Object java.util.EventObject.getSource()

};
}
}
#endif
