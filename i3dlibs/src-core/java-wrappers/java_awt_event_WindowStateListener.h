#ifndef java_awt_event_WindowStateListener_H
#define java_awt_event_WindowStateListener_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace util {
class EventListener;
}
}

namespace java {
namespace awt {
namespace event {
class WindowEvent;
}
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace event {
class WindowStateListener : public java::lang::Object {
  public:
    WindowStateListener(JavaMarker* dummy);
    WindowStateListener(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual void windowStateChanged(java::awt::event::WindowEvent* arg1); // public abstract void java.awt.event.WindowStateListener.windowStateChanged(java.awt.event.WindowEvent)

};
}
}
}
#endif
