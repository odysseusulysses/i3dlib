#ifndef java_awt_image_renderable_RenderableImage_H
#define java_awt_image_renderable_RenderableImage_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace lang {
class String;
}
}

namespace java {
namespace util {
class Vector;
}
}

namespace java {
namespace awt {
namespace image {
class RenderedImage;
}
}
}

namespace java {
namespace awt {
namespace image {
namespace renderable {
class RenderContext;
}
}
}
}

namespace java {
namespace awt {
class RenderingHints;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace image {
namespace renderable {
class RenderableImage : public java::lang::Object {
  public:
    RenderableImage(JavaMarker* dummy);
    RenderableImage(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual java::lang::Object* getProperty(const char* arg1); // public abstract java.lang.Object java.awt.image.renderable.RenderableImage.getProperty(java.lang.String)
    virtual float getHeight(); // public abstract float java.awt.image.renderable.RenderableImage.getHeight()
    virtual float getMinX(); // public abstract float java.awt.image.renderable.RenderableImage.getMinX()
    virtual float getMinY(); // public abstract float java.awt.image.renderable.RenderableImage.getMinY()
    virtual JavaObjectArray* getPropertyNames(); // public abstract java.lang.String[] java.awt.image.renderable.RenderableImage.getPropertyNames()
    virtual java::util::Vector* getSources(); // public abstract java.util.Vector java.awt.image.renderable.RenderableImage.getSources()
    virtual float getWidth(); // public abstract float java.awt.image.renderable.RenderableImage.getWidth()
    virtual java::awt::image::RenderedImage* createDefaultRendering(); // public abstract java.awt.image.RenderedImage java.awt.image.renderable.RenderableImage.createDefaultRendering()
    virtual java::awt::image::RenderedImage* createRendering(java::awt::image::renderable::RenderContext* arg1); // public abstract java.awt.image.RenderedImage java.awt.image.renderable.RenderableImage.createRendering(java.awt.image.renderable.RenderContext)
    virtual java::awt::image::RenderedImage* createScaledRendering(int arg1, int arg2, java::awt::RenderingHints* arg3); // public abstract java.awt.image.RenderedImage java.awt.image.renderable.RenderableImage.createScaledRendering(int,int,java.awt.RenderingHints)
    virtual bool isDynamic(); // public abstract boolean java.awt.image.renderable.RenderableImage.isDynamic()

    const char* HINTS_OBSERVED; // public static final java.lang.String java.awt.image.renderable.RenderableImage.HINTS_OBSERVED
};
}
}
}
}
#endif
