#ifndef java_awt_dnd_peer_DragSourceContextPeer_H
#define java_awt_dnd_peer_DragSourceContextPeer_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace awt {
class Cursor;
}
}

namespace java {
namespace awt {
namespace dnd {
class DragSourceContext;
}
}
}

namespace java {
namespace awt {
class Image;
}
}

namespace java {
namespace awt {
class Point;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace dnd {
namespace peer {
class DragSourceContextPeer : public java::lang::Object {
  public:
    DragSourceContextPeer(JavaMarker* dummy);
    DragSourceContextPeer(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual void setCursor(java::awt::Cursor* arg1); // public abstract void java.awt.dnd.peer.DragSourceContextPeer.setCursor(java.awt.Cursor) throws java.awt.dnd.InvalidDnDOperationException
    virtual java::awt::Cursor* getCursor(); // public abstract java.awt.Cursor java.awt.dnd.peer.DragSourceContextPeer.getCursor()
    virtual void startDrag(java::awt::dnd::DragSourceContext* arg1, java::awt::Cursor* arg2, java::awt::Image* arg3, java::awt::Point* arg4); // public abstract void java.awt.dnd.peer.DragSourceContextPeer.startDrag(java.awt.dnd.DragSourceContext,java.awt.Cursor,java.awt.Image,java.awt.Point) throws java.awt.dnd.InvalidDnDOperationException
    virtual void transferablesFlavorsChanged(); // public abstract void java.awt.dnd.peer.DragSourceContextPeer.transferablesFlavorsChanged()

};
}
}
}
}
#endif
