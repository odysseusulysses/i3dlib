#ifndef java_awt_BufferCapabilities_H
#define java_awt_BufferCapabilities_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace lang {
class Cloneable;
}
}

namespace java {
namespace awt {
class ImageCapabilities;
}
}

namespace java {
namespace awt {
class BufferCapabilities__FlipContents;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
class BufferCapabilities : public java::lang::Object {
  public:
    BufferCapabilities(JavaMarker* dummy);
    BufferCapabilities(jobject obj);
    BufferCapabilities(java::awt::ImageCapabilities* arg1, java::awt::ImageCapabilities* arg2, java::awt::BufferCapabilities__FlipContents* arg3); // public java.awt.BufferCapabilities(java.awt.ImageCapabilities,java.awt.ImageCapabilities,java.awt.BufferCapabilities$FlipContents)

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual java::lang::Object* clone(); // public java.lang.Object java.awt.BufferCapabilities.clone()
    virtual java::awt::ImageCapabilities* getBackBufferCapabilities(); // public java.awt.ImageCapabilities java.awt.BufferCapabilities.getBackBufferCapabilities()
    virtual java::awt::BufferCapabilities__FlipContents* getFlipContents(); // public java.awt.BufferCapabilities$FlipContents java.awt.BufferCapabilities.getFlipContents()
    virtual java::awt::ImageCapabilities* getFrontBufferCapabilities(); // public java.awt.ImageCapabilities java.awt.BufferCapabilities.getFrontBufferCapabilities()
    virtual bool isFullScreenRequired(); // public boolean java.awt.BufferCapabilities.isFullScreenRequired()
    virtual bool isMultiBufferAvailable(); // public boolean java.awt.BufferCapabilities.isMultiBufferAvailable()
    virtual bool isPageFlipping(); // public boolean java.awt.BufferCapabilities.isPageFlipping()

};
}
}
#endif
