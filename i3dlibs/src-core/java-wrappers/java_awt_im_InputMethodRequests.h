#ifndef java_awt_im_InputMethodRequests_H
#define java_awt_im_InputMethodRequests_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace text {
class AttributedCharacterIterator;
}
}

namespace java {
namespace text {
class AttributedCharacterIterator__Attribute;
}
}

namespace java {
namespace awt {
namespace font {
class TextHitInfo;
}
}
}

namespace java {
namespace awt {
class Rectangle;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace im {
class InputMethodRequests : public java::lang::Object {
  public:
    InputMethodRequests(JavaMarker* dummy);
    InputMethodRequests(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual java::text::AttributedCharacterIterator* getSelectedText(JavaObjectArray* arg1); // public abstract java.text.AttributedCharacterIterator java.awt.im.InputMethodRequests.getSelectedText(java.text.AttributedCharacterIterator$Attribute[])
    virtual java::text::AttributedCharacterIterator* cancelLatestCommittedText(JavaObjectArray* arg1); // public abstract java.text.AttributedCharacterIterator java.awt.im.InputMethodRequests.cancelLatestCommittedText(java.text.AttributedCharacterIterator$Attribute[])
    virtual java::text::AttributedCharacterIterator* getCommittedText(int arg1, int arg2, JavaObjectArray* arg3); // public abstract java.text.AttributedCharacterIterator java.awt.im.InputMethodRequests.getCommittedText(int,int,java.text.AttributedCharacterIterator$Attribute[])
    virtual int getCommittedTextLength(); // public abstract int java.awt.im.InputMethodRequests.getCommittedTextLength()
    virtual int getInsertPositionOffset(); // public abstract int java.awt.im.InputMethodRequests.getInsertPositionOffset()
    virtual java::awt::font::TextHitInfo* getLocationOffset(int arg1, int arg2); // public abstract java.awt.font.TextHitInfo java.awt.im.InputMethodRequests.getLocationOffset(int,int)
    virtual java::awt::Rectangle* getTextLocation(java::awt::font::TextHitInfo* arg1); // public abstract java.awt.Rectangle java.awt.im.InputMethodRequests.getTextLocation(java.awt.font.TextHitInfo)

};
}
}
}
#endif
