#ifndef java_io_DataOutput_H
#define java_io_DataOutput_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace lang {
class String;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace io {
class DataOutput : public java::lang::Object {
  public:
    DataOutput(JavaMarker* dummy);
    DataOutput(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual void write(JavaByteArray* arg1); // public abstract void java.io.DataOutput.write(byte[]) throws java.io.IOException
    virtual void write(JavaByteArray* arg1, int arg2, int arg3); // public abstract void java.io.DataOutput.write(byte[],int,int) throws java.io.IOException
    virtual void write(int arg1); // public abstract void java.io.DataOutput.write(int) throws java.io.IOException
    virtual void writeInt(int arg1); // public abstract void java.io.DataOutput.writeInt(int) throws java.io.IOException
    virtual void writeBytes(const char* arg1); // public abstract void java.io.DataOutput.writeBytes(java.lang.String) throws java.io.IOException
    virtual void writeChar(int arg1); // public abstract void java.io.DataOutput.writeChar(int) throws java.io.IOException
    virtual void writeByte(int arg1); // public abstract void java.io.DataOutput.writeByte(int) throws java.io.IOException
    virtual void writeLong(long arg1); // public abstract void java.io.DataOutput.writeLong(long) throws java.io.IOException
    virtual void writeShort(int arg1); // public abstract void java.io.DataOutput.writeShort(int) throws java.io.IOException
    virtual void writeUTF(const char* arg1); // public abstract void java.io.DataOutput.writeUTF(java.lang.String) throws java.io.IOException
    virtual void writeFloat(float arg1); // public abstract void java.io.DataOutput.writeFloat(float) throws java.io.IOException
    virtual void writeBoolean(bool arg1); // public abstract void java.io.DataOutput.writeBoolean(boolean) throws java.io.IOException
    virtual void writeChars(const char* arg1); // public abstract void java.io.DataOutput.writeChars(java.lang.String) throws java.io.IOException
    virtual void writeDouble(double arg1); // public abstract void java.io.DataOutput.writeDouble(double) throws java.io.IOException

};
}
}
#endif
