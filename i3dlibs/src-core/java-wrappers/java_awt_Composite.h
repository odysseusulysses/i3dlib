#ifndef java_awt_Composite_H
#define java_awt_Composite_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace awt {
class CompositeContext;
}
}

namespace java {
namespace awt {
namespace image {
class ColorModel;
}
}
}

namespace java {
namespace awt {
class RenderingHints;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
class Composite : public java::lang::Object {
  public:
    Composite(JavaMarker* dummy);
    Composite(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual java::awt::CompositeContext* createContext(java::awt::image::ColorModel* arg1, java::awt::image::ColorModel* arg2, java::awt::RenderingHints* arg3); // public abstract java.awt.CompositeContext java.awt.Composite.createContext(java.awt.image.ColorModel,java.awt.image.ColorModel,java.awt.RenderingHints)

};
}
}
#endif
