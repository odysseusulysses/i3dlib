#ifndef java_awt_image_BufferedImage_H
#define java_awt_image_BufferedImage_H
#include <jni.h>
#include <java_marker.h>
#include <java_awt_Image.h>

namespace java {
namespace awt {
namespace image {
class WritableRenderedImage;
}
}
}

namespace java {
namespace awt {
class Transparency;
}
}

namespace java {
namespace awt {
namespace image {
class IndexColorModel;
}
}
}

namespace java {
namespace awt {
namespace image {
class ColorModel;
}
}
}

namespace java {
namespace awt {
namespace image {
class WritableRaster;
}
}
}

namespace java {
namespace util {
class Hashtable;
}
}

namespace java {
namespace lang {
class Object;
}
}

namespace java {
namespace lang {
class String;
}
}

namespace java {
namespace awt {
namespace image {
class ImageObserver;
}
}
}

namespace java {
namespace awt {
namespace image {
class Raster;
}
}
}

namespace java {
namespace awt {
class Rectangle;
}
}

namespace java {
namespace awt {
namespace image {
class TileObserver;
}
}
}

namespace java {
namespace awt {
class Graphics2D;
}
}

namespace java {
namespace awt {
class Graphics;
}
}

namespace java {
namespace awt {
namespace image {
class SampleModel;
}
}
}

namespace java {
namespace awt {
namespace image {
class ImageProducer;
}
}
}

namespace java {
namespace util {
class Vector;
}
}

namespace java {
namespace awt {
class Point;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace image {
class BufferedImage : public java::awt::Image {
  public:
    BufferedImage(JavaMarker* dummy);
    BufferedImage(jobject obj);
    BufferedImage(int arg1, int arg2, int arg3); // public java.awt.image.BufferedImage(int,int,int)
    BufferedImage(int arg1, int arg2, int arg3, java::awt::image::IndexColorModel* arg4); // public java.awt.image.BufferedImage(int,int,int,java.awt.image.IndexColorModel)
    BufferedImage(java::awt::image::ColorModel* arg1, java::awt::image::WritableRaster* arg2, bool arg3, java::util::Hashtable* arg4); // public java.awt.image.BufferedImage(java.awt.image.ColorModel,java.awt.image.WritableRaster,boolean,java.util.Hashtable)

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual java::lang::Object* getProperty(const char* arg1, java::awt::image::ImageObserver* arg2); // public java.lang.Object java.awt.image.BufferedImage.getProperty(java.lang.String,java.awt.image.ImageObserver)
    virtual java::lang::Object* getProperty(const char* arg1); // public java.lang.Object java.awt.image.BufferedImage.getProperty(java.lang.String)
    virtual const char* toString(); // public java.lang.String java.awt.image.BufferedImage.toString()
    virtual int getType(); // public int java.awt.image.BufferedImage.getType()
    virtual java::awt::image::Raster* getData(java::awt::Rectangle* arg1); // public java.awt.image.Raster java.awt.image.BufferedImage.getData(java.awt.Rectangle)
    virtual java::awt::image::Raster* getData(); // public java.awt.image.Raster java.awt.image.BufferedImage.getData()
    virtual void addTileObserver(java::awt::image::TileObserver* arg1); // public void java.awt.image.BufferedImage.addTileObserver(java.awt.image.TileObserver)
    virtual void coerceData(bool arg1); // public void java.awt.image.BufferedImage.coerceData(boolean)
    virtual java::awt::image::WritableRaster* copyData(java::awt::image::WritableRaster* arg1); // public java.awt.image.WritableRaster java.awt.image.BufferedImage.copyData(java.awt.image.WritableRaster)
    virtual java::awt::Graphics2D* createGraphics(); // public java.awt.Graphics2D java.awt.image.BufferedImage.createGraphics()
    virtual java::awt::image::WritableRaster* getAlphaRaster(); // public java.awt.image.WritableRaster java.awt.image.BufferedImage.getAlphaRaster()
    virtual java::awt::image::ColorModel* getColorModel(); // public java.awt.image.ColorModel java.awt.image.BufferedImage.getColorModel()
    virtual java::awt::Graphics* getGraphics(); // public java.awt.Graphics java.awt.image.BufferedImage.getGraphics()
    virtual int getHeight(); // public int java.awt.image.BufferedImage.getHeight()
    virtual int getHeight(java::awt::image::ImageObserver* arg1); // public int java.awt.image.BufferedImage.getHeight(java.awt.image.ImageObserver)
    virtual int getMinTileX(); // public int java.awt.image.BufferedImage.getMinTileX()
    virtual int getMinTileY(); // public int java.awt.image.BufferedImage.getMinTileY()
    virtual int getMinX(); // public int java.awt.image.BufferedImage.getMinX()
    virtual int getMinY(); // public int java.awt.image.BufferedImage.getMinY()
    virtual int getNumXTiles(); // public int java.awt.image.BufferedImage.getNumXTiles()
    virtual int getNumYTiles(); // public int java.awt.image.BufferedImage.getNumYTiles()
    virtual JavaObjectArray* getPropertyNames(); // public java.lang.String[] java.awt.image.BufferedImage.getPropertyNames()
    virtual int getRGB(int arg1, int arg2); // public int java.awt.image.BufferedImage.getRGB(int,int)
    virtual JavaIntArray* getRGB(int arg1, int arg2, int arg3, int arg4, JavaIntArray* arg5, int arg6, int arg7); // public int[] java.awt.image.BufferedImage.getRGB(int,int,int,int,int[],int,int)
    virtual java::awt::image::WritableRaster* getRaster(); // public java.awt.image.WritableRaster java.awt.image.BufferedImage.getRaster()
    virtual java::awt::image::SampleModel* getSampleModel(); // public java.awt.image.SampleModel java.awt.image.BufferedImage.getSampleModel()
    virtual java::awt::image::ImageProducer* getSource(); // public java.awt.image.ImageProducer java.awt.image.BufferedImage.getSource()
    virtual java::util::Vector* getSources(); // public java.util.Vector java.awt.image.BufferedImage.getSources()
    virtual java::awt::image::BufferedImage* getSubimage(int arg1, int arg2, int arg3, int arg4); // public java.awt.image.BufferedImage java.awt.image.BufferedImage.getSubimage(int,int,int,int)
    virtual java::awt::image::Raster* getTile(int arg1, int arg2); // public java.awt.image.Raster java.awt.image.BufferedImage.getTile(int,int)
    virtual int getTileGridXOffset(); // public int java.awt.image.BufferedImage.getTileGridXOffset()
    virtual int getTileGridYOffset(); // public int java.awt.image.BufferedImage.getTileGridYOffset()
    virtual int getTileHeight(); // public int java.awt.image.BufferedImage.getTileHeight()
    virtual int getTileWidth(); // public int java.awt.image.BufferedImage.getTileWidth()
    virtual int getTransparency(); // public int java.awt.image.BufferedImage.getTransparency()
    virtual int getWidth(); // public int java.awt.image.BufferedImage.getWidth()
    virtual int getWidth(java::awt::image::ImageObserver* arg1); // public int java.awt.image.BufferedImage.getWidth(java.awt.image.ImageObserver)
    virtual java::awt::image::WritableRaster* getWritableTile(int arg1, int arg2); // public java.awt.image.WritableRaster java.awt.image.BufferedImage.getWritableTile(int,int)
    virtual JavaObjectArray* getWritableTileIndices(); // public java.awt.Point[] java.awt.image.BufferedImage.getWritableTileIndices()
    virtual bool hasTileWriters(); // public boolean java.awt.image.BufferedImage.hasTileWriters()
    virtual bool isAlphaPremultiplied(); // public boolean java.awt.image.BufferedImage.isAlphaPremultiplied()
    virtual bool isTileWritable(int arg1, int arg2); // public boolean java.awt.image.BufferedImage.isTileWritable(int,int)
    virtual void releaseWritableTile(int arg1, int arg2); // public void java.awt.image.BufferedImage.releaseWritableTile(int,int)
    virtual void removeTileObserver(java::awt::image::TileObserver* arg1); // public void java.awt.image.BufferedImage.removeTileObserver(java.awt.image.TileObserver)
    virtual void setData(java::awt::image::Raster* arg1); // public void java.awt.image.BufferedImage.setData(java.awt.image.Raster)
    virtual void setRGB(int arg1, int arg2, int arg3, int arg4, JavaIntArray* arg5, int arg6, int arg7); // public void java.awt.image.BufferedImage.setRGB(int,int,int,int,int[],int,int)
    virtual void setRGB(int arg1, int arg2, int arg3); // public synchronized void java.awt.image.BufferedImage.setRGB(int,int,int)

    int TYPE_CUSTOM; // public static final int java.awt.image.BufferedImage.TYPE_CUSTOM
    int TYPE_INT_RGB; // public static final int java.awt.image.BufferedImage.TYPE_INT_RGB
    int TYPE_INT_ARGB; // public static final int java.awt.image.BufferedImage.TYPE_INT_ARGB
    int TYPE_INT_ARGB_PRE; // public static final int java.awt.image.BufferedImage.TYPE_INT_ARGB_PRE
    int TYPE_INT_BGR; // public static final int java.awt.image.BufferedImage.TYPE_INT_BGR
    int TYPE_3BYTE_BGR; // public static final int java.awt.image.BufferedImage.TYPE_3BYTE_BGR
    int TYPE_4BYTE_ABGR; // public static final int java.awt.image.BufferedImage.TYPE_4BYTE_ABGR
    int TYPE_4BYTE_ABGR_PRE; // public static final int java.awt.image.BufferedImage.TYPE_4BYTE_ABGR_PRE
    int TYPE_USHORT_565_RGB; // public static final int java.awt.image.BufferedImage.TYPE_USHORT_565_RGB
    int TYPE_USHORT_555_RGB; // public static final int java.awt.image.BufferedImage.TYPE_USHORT_555_RGB
    int TYPE_BYTE_GRAY; // public static final int java.awt.image.BufferedImage.TYPE_BYTE_GRAY
    int TYPE_USHORT_GRAY; // public static final int java.awt.image.BufferedImage.TYPE_USHORT_GRAY
    int TYPE_BYTE_BINARY; // public static final int java.awt.image.BufferedImage.TYPE_BYTE_BINARY
    int TYPE_BYTE_INDEXED; // public static final int java.awt.image.BufferedImage.TYPE_BYTE_INDEXED
};
}
}
}
#endif
