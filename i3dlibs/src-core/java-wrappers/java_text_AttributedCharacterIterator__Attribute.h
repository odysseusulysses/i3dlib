#ifndef java_text_AttributedCharacterIterator__Attribute_H
#define java_text_AttributedCharacterIterator__Attribute_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace io {
class Serializable;
}
}

namespace java {
namespace lang {
class String;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace text {
class AttributedCharacterIterator__Attribute : public java::lang::Object {
  public:
    AttributedCharacterIterator__Attribute(JavaMarker* dummy);
    AttributedCharacterIterator__Attribute(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual int hashCode(); // public final int java.text.AttributedCharacterIterator$Attribute.hashCode()
    virtual bool equals(java::lang::Object* arg1); // public final boolean java.text.AttributedCharacterIterator$Attribute.equals(java.lang.Object)
    virtual const char* toString(); // public java.lang.String java.text.AttributedCharacterIterator$Attribute.toString()

    java::text::AttributedCharacterIterator__Attribute* LANGUAGE; // public static final java.text.AttributedCharacterIterator$Attribute java.text.AttributedCharacterIterator$Attribute.LANGUAGE
    java::text::AttributedCharacterIterator__Attribute* READING; // public static final java.text.AttributedCharacterIterator$Attribute java.text.AttributedCharacterIterator$Attribute.READING
    java::text::AttributedCharacterIterator__Attribute* INPUT_METHOD_SEGMENT; // public static final java.text.AttributedCharacterIterator$Attribute java.text.AttributedCharacterIterator$Attribute.INPUT_METHOD_SEGMENT
};
}
}
#endif
