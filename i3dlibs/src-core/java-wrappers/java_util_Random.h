#ifndef java_util_Random_H
#define java_util_Random_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace io {
class Serializable;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace util {
class Random : public java::lang::Object {
  public:
    Random(JavaMarker* dummy);
    Random(jobject obj);
    Random(long arg1); // public java.util.Random(long)
    Random(); // public java.util.Random()

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual int nextInt(int arg1); // public int java.util.Random.nextInt(int)
    virtual int nextInt(); // public int java.util.Random.nextInt()
    virtual double nextDouble(); // public double java.util.Random.nextDouble()
    virtual void nextBytes(JavaByteArray* arg1); // public void java.util.Random.nextBytes(byte[])
    virtual bool nextBoolean(); // public boolean java.util.Random.nextBoolean()
    virtual float nextFloat(); // public float java.util.Random.nextFloat()
    virtual double nextGaussian(); // public synchronized double java.util.Random.nextGaussian()
    virtual long nextLong(); // public long java.util.Random.nextLong()
    virtual void setSeed(long arg1); // public synchronized void java.util.Random.setSeed(long)

};
}
}
#endif
