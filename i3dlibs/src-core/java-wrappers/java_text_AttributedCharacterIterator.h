#ifndef java_text_AttributedCharacterIterator_H
#define java_text_AttributedCharacterIterator_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace text {
class CharacterIterator;
}
}

namespace java {
namespace util {
class Map;
}
}

namespace java {
namespace text {
class AttributedCharacterIterator__Attribute;
}
}

namespace java {
namespace util {
class Set;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace text {
class AttributedCharacterIterator : public java::lang::Object {
  public:
    AttributedCharacterIterator(JavaMarker* dummy);
    AttributedCharacterIterator(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual java::util::Map* getAttributes(); // public abstract java.util.Map java.text.AttributedCharacterIterator.getAttributes()
    virtual java::lang::Object* getAttribute(java::text::AttributedCharacterIterator__Attribute* arg1); // public abstract java.lang.Object java.text.AttributedCharacterIterator.getAttribute(java.text.AttributedCharacterIterator$Attribute)
    virtual java::util::Set* getAllAttributeKeys(); // public abstract java.util.Set java.text.AttributedCharacterIterator.getAllAttributeKeys()
    virtual int getRunLimit(); // public abstract int java.text.AttributedCharacterIterator.getRunLimit()
    virtual int getRunLimit(java::text::AttributedCharacterIterator__Attribute* arg1); // public abstract int java.text.AttributedCharacterIterator.getRunLimit(java.text.AttributedCharacterIterator$Attribute)
    virtual int getRunLimit(java::util::Set* arg1); // public abstract int java.text.AttributedCharacterIterator.getRunLimit(java.util.Set)
    virtual int getRunStart(java::text::AttributedCharacterIterator__Attribute* arg1); // public abstract int java.text.AttributedCharacterIterator.getRunStart(java.text.AttributedCharacterIterator$Attribute)
    virtual int getRunStart(); // public abstract int java.text.AttributedCharacterIterator.getRunStart()
    virtual int getRunStart(java::util::Set* arg1); // public abstract int java.text.AttributedCharacterIterator.getRunStart(java.util.Set)

};
}
}
#endif
