#ifndef java_awt_Shape_H
#define java_awt_Shape_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace awt {
namespace geom {
class Rectangle2D;
}
}
}

namespace java {
namespace awt {
namespace geom {
class Point2D;
}
}
}

namespace java {
namespace awt {
class Rectangle;
}
}

namespace java {
namespace awt {
namespace geom {
class PathIterator;
}
}
}

namespace java {
namespace awt {
namespace geom {
class AffineTransform;
}
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
class Shape : public java::lang::Object {
  public:
    Shape(JavaMarker* dummy);
    Shape(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual bool contains(java::awt::geom::Rectangle2D* arg1); // public abstract boolean java.awt.Shape.contains(java.awt.geom.Rectangle2D)
    virtual bool contains(double arg1, double arg2); // public abstract boolean java.awt.Shape.contains(double,double)
    virtual bool contains(java::awt::geom::Point2D* arg1); // public abstract boolean java.awt.Shape.contains(java.awt.geom.Point2D)
    virtual bool contains(double arg1, double arg2, double arg3, double arg4); // public abstract boolean java.awt.Shape.contains(double,double,double,double)
    virtual bool intersects(double arg1, double arg2, double arg3, double arg4); // public abstract boolean java.awt.Shape.intersects(double,double,double,double)
    virtual bool intersects(java::awt::geom::Rectangle2D* arg1); // public abstract boolean java.awt.Shape.intersects(java.awt.geom.Rectangle2D)
    virtual java::awt::Rectangle* getBounds(); // public abstract java.awt.Rectangle java.awt.Shape.getBounds()
    virtual java::awt::geom::Rectangle2D* getBounds2D(); // public abstract java.awt.geom.Rectangle2D java.awt.Shape.getBounds2D()
    virtual java::awt::geom::PathIterator* getPathIterator(java::awt::geom::AffineTransform* arg1, double arg2); // public abstract java.awt.geom.PathIterator java.awt.Shape.getPathIterator(java.awt.geom.AffineTransform,double)
    virtual java::awt::geom::PathIterator* getPathIterator(java::awt::geom::AffineTransform* arg1); // public abstract java.awt.geom.PathIterator java.awt.Shape.getPathIterator(java.awt.geom.AffineTransform)

};
}
}
#endif
