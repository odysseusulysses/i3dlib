#ifndef java_awt_JobAttributes__DefaultSelectionType_H
#define java_awt_JobAttributes__DefaultSelectionType_H
#include <jni.h>
#include <java_marker.h>
#include <java_awt_AttributeValue.h>

namespace java {
namespace lang {
class String;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
class JobAttributes__DefaultSelectionType : public java::awt::AttributeValue {
  public:
    JobAttributes__DefaultSelectionType(JavaMarker* dummy);
    JobAttributes__DefaultSelectionType(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual int hashCode(); // public int java.awt.JobAttributes$DefaultSelectionType.hashCode()
    virtual const char* toString(); // public java.lang.String java.awt.JobAttributes$DefaultSelectionType.toString()

    java::awt::JobAttributes__DefaultSelectionType* ALL; // public static final java.awt.JobAttributes$DefaultSelectionType java.awt.JobAttributes$DefaultSelectionType.ALL
    java::awt::JobAttributes__DefaultSelectionType* RANGE; // public static final java.awt.JobAttributes$DefaultSelectionType java.awt.JobAttributes$DefaultSelectionType.RANGE
    java::awt::JobAttributes__DefaultSelectionType* SELECTION; // public static final java.awt.JobAttributes$DefaultSelectionType java.awt.JobAttributes$DefaultSelectionType.SELECTION
};
}
}
#endif
