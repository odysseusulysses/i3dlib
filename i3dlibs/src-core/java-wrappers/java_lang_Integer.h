#ifndef java_lang_Integer_H
#define java_lang_Integer_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Number.h>

namespace java {
namespace lang {
class Comparable;
}
}

namespace java {
namespace lang {
class String;
}
}

namespace java {
namespace lang {
class Object;
}
}

namespace java {
namespace lang {
class Class;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace lang {
class Integer : public java::lang::Number {
  public:
    Integer(JavaMarker* dummy);
    Integer(jobject obj);
    Integer(const char* arg1); // public java.lang.Integer(java.lang.String) throws java.lang.NumberFormatException
    Integer(int arg1); // public java.lang.Integer(int)

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual int hashCode(); // public int java.lang.Integer.hashCode()
    virtual int compareTo(java::lang::Integer* arg1); // public int java.lang.Integer.compareTo(java.lang.Integer)
    virtual int compareTo(java::lang::Object* arg1); // public int java.lang.Integer.compareTo(java.lang.Object)
    virtual bool equals(java::lang::Object* arg1); // public boolean java.lang.Integer.equals(java.lang.Object)
    virtual const char* toString(int arg1, int arg2); // public static java.lang.String java.lang.Integer.toString(int,int)
    virtual const char* toString(int arg1); // public static java.lang.String java.lang.Integer.toString(int)
    virtual const char* toString(); // public java.lang.String java.lang.Integer.toString()
    virtual const char* toHexString(int arg1); // public static java.lang.String java.lang.Integer.toHexString(int)
    virtual java::lang::Integer* decode(const char* arg1); // public static java.lang.Integer java.lang.Integer.decode(java.lang.String) throws java.lang.NumberFormatException
    virtual java::lang::Integer* valueOf(int arg1); // public static java.lang.Integer java.lang.Integer.valueOf(int)
    virtual java::lang::Integer* valueOf(const char* arg1); // public static java.lang.Integer java.lang.Integer.valueOf(java.lang.String) throws java.lang.NumberFormatException
    virtual java::lang::Integer* valueOf(const char* arg1, int arg2); // public static java.lang.Integer java.lang.Integer.valueOf(java.lang.String,int) throws java.lang.NumberFormatException
    virtual int reverse(int arg1); // public static int java.lang.Integer.reverse(int)
    virtual int reverseBytes(int arg1); // public static int java.lang.Integer.reverseBytes(int)
    virtual char byteValue(); // public byte java.lang.Integer.byteValue()
    virtual double doubleValue(); // public double java.lang.Integer.doubleValue()
    virtual float floatValue(); // public float java.lang.Integer.floatValue()
    virtual int intValue(); // public int java.lang.Integer.intValue()
    virtual long longValue(); // public long java.lang.Integer.longValue()
    virtual short shortValue(); // public short java.lang.Integer.shortValue()
    virtual int parseInt(const char* arg1, int arg2); // public static int java.lang.Integer.parseInt(java.lang.String,int) throws java.lang.NumberFormatException
    virtual int parseInt(const char* arg1); // public static int java.lang.Integer.parseInt(java.lang.String) throws java.lang.NumberFormatException
    virtual int bitCount(int arg1); // public static int java.lang.Integer.bitCount(int)
    virtual java::lang::Integer* getInteger(const char* arg1); // public static java.lang.Integer java.lang.Integer.getInteger(java.lang.String)
    virtual java::lang::Integer* getInteger(const char* arg1, java::lang::Integer* arg2); // public static java.lang.Integer java.lang.Integer.getInteger(java.lang.String,java.lang.Integer)
    virtual java::lang::Integer* getInteger(const char* arg1, int arg2); // public static java.lang.Integer java.lang.Integer.getInteger(java.lang.String,int)
    virtual int highestOneBit(int arg1); // public static int java.lang.Integer.highestOneBit(int)
    virtual int lowestOneBit(int arg1); // public static int java.lang.Integer.lowestOneBit(int)
    virtual int numberOfLeadingZeros(int arg1); // public static int java.lang.Integer.numberOfLeadingZeros(int)
    virtual int numberOfTrailingZeros(int arg1); // public static int java.lang.Integer.numberOfTrailingZeros(int)
    virtual int rotateLeft(int arg1, int arg2); // public static int java.lang.Integer.rotateLeft(int,int)
    virtual int rotateRight(int arg1, int arg2); // public static int java.lang.Integer.rotateRight(int,int)
    virtual int signum(int arg1); // public static int java.lang.Integer.signum(int)
    virtual const char* toBinaryString(int arg1); // public static java.lang.String java.lang.Integer.toBinaryString(int)
    virtual const char* toOctalString(int arg1); // public static java.lang.String java.lang.Integer.toOctalString(int)

    int MIN_VALUE; // public static final int java.lang.Integer.MIN_VALUE
    int MAX_VALUE; // public static final int java.lang.Integer.MAX_VALUE
    java::lang::Class* TYPE; // public static final java.lang.Class java.lang.Integer.TYPE
    int SIZE; // public static final int java.lang.Integer.SIZE
};
}
}
#endif
