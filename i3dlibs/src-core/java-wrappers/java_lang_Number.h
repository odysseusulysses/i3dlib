#ifndef java_lang_Number_H
#define java_lang_Number_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace io {
class Serializable;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace lang {
class Number : public java::lang::Object {
  public:
    Number(JavaMarker* dummy);
    Number(jobject obj);
    Number(); // public java.lang.Number()

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual char byteValue(); // public byte java.lang.Number.byteValue()
    virtual double doubleValue(); // public abstract double java.lang.Number.doubleValue()
    virtual float floatValue(); // public abstract float java.lang.Number.floatValue()
    virtual int intValue(); // public abstract int java.lang.Number.intValue()
    virtual long longValue(); // public abstract long java.lang.Number.longValue()
    virtual short shortValue(); // public short java.lang.Number.shortValue()

};
}
}
#endif
