#ifndef javax_accessibility_AccessibleState_H
#define javax_accessibility_AccessibleState_H
#include <jni.h>
#include <java_marker.h>
#include <javax_accessibility_AccessibleBundle.h>
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace javax {
namespace accessibility {
class AccessibleState : public javax::accessibility::AccessibleBundle {
  public:
    AccessibleState(JavaMarker* dummy);
    AccessibleState(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);

    javax::accessibility::AccessibleState* ACTIVE; // public static final javax.accessibility.AccessibleState javax.accessibility.AccessibleState.ACTIVE
    javax::accessibility::AccessibleState* PRESSED; // public static final javax.accessibility.AccessibleState javax.accessibility.AccessibleState.PRESSED
    javax::accessibility::AccessibleState* ARMED; // public static final javax.accessibility.AccessibleState javax.accessibility.AccessibleState.ARMED
    javax::accessibility::AccessibleState* BUSY; // public static final javax.accessibility.AccessibleState javax.accessibility.AccessibleState.BUSY
    javax::accessibility::AccessibleState* CHECKED; // public static final javax.accessibility.AccessibleState javax.accessibility.AccessibleState.CHECKED
    javax::accessibility::AccessibleState* EDITABLE; // public static final javax.accessibility.AccessibleState javax.accessibility.AccessibleState.EDITABLE
    javax::accessibility::AccessibleState* EXPANDABLE; // public static final javax.accessibility.AccessibleState javax.accessibility.AccessibleState.EXPANDABLE
    javax::accessibility::AccessibleState* COLLAPSED; // public static final javax.accessibility.AccessibleState javax.accessibility.AccessibleState.COLLAPSED
    javax::accessibility::AccessibleState* EXPANDED; // public static final javax.accessibility.AccessibleState javax.accessibility.AccessibleState.EXPANDED
    javax::accessibility::AccessibleState* ENABLED; // public static final javax.accessibility.AccessibleState javax.accessibility.AccessibleState.ENABLED
    javax::accessibility::AccessibleState* FOCUSABLE; // public static final javax.accessibility.AccessibleState javax.accessibility.AccessibleState.FOCUSABLE
    javax::accessibility::AccessibleState* FOCUSED; // public static final javax.accessibility.AccessibleState javax.accessibility.AccessibleState.FOCUSED
    javax::accessibility::AccessibleState* ICONIFIED; // public static final javax.accessibility.AccessibleState javax.accessibility.AccessibleState.ICONIFIED
    javax::accessibility::AccessibleState* MODAL; // public static final javax.accessibility.AccessibleState javax.accessibility.AccessibleState.MODAL
    javax::accessibility::AccessibleState* OPAQUE; // public static final javax.accessibility.AccessibleState javax.accessibility.AccessibleState.OPAQUE
    javax::accessibility::AccessibleState* RESIZABLE; // public static final javax.accessibility.AccessibleState javax.accessibility.AccessibleState.RESIZABLE
    javax::accessibility::AccessibleState* MULTISELECTABLE; // public static final javax.accessibility.AccessibleState javax.accessibility.AccessibleState.MULTISELECTABLE
    javax::accessibility::AccessibleState* SELECTABLE; // public static final javax.accessibility.AccessibleState javax.accessibility.AccessibleState.SELECTABLE
    javax::accessibility::AccessibleState* SELECTED; // public static final javax.accessibility.AccessibleState javax.accessibility.AccessibleState.SELECTED
    javax::accessibility::AccessibleState* SHOWING; // public static final javax.accessibility.AccessibleState javax.accessibility.AccessibleState.SHOWING
    javax::accessibility::AccessibleState* VISIBLE; // public static final javax.accessibility.AccessibleState javax.accessibility.AccessibleState.VISIBLE
    javax::accessibility::AccessibleState* VERTICAL; // public static final javax.accessibility.AccessibleState javax.accessibility.AccessibleState.VERTICAL
    javax::accessibility::AccessibleState* HORIZONTAL; // public static final javax.accessibility.AccessibleState javax.accessibility.AccessibleState.HORIZONTAL
    javax::accessibility::AccessibleState* SINGLE_LINE; // public static final javax.accessibility.AccessibleState javax.accessibility.AccessibleState.SINGLE_LINE
    javax::accessibility::AccessibleState* MULTI_LINE; // public static final javax.accessibility.AccessibleState javax.accessibility.AccessibleState.MULTI_LINE
    javax::accessibility::AccessibleState* TRANSIENT; // public static final javax.accessibility.AccessibleState javax.accessibility.AccessibleState.TRANSIENT
    javax::accessibility::AccessibleState* MANAGES_DESCENDANTS; // public static final javax.accessibility.AccessibleState javax.accessibility.AccessibleState.MANAGES_DESCENDANTS
    javax::accessibility::AccessibleState* INDETERMINATE; // public static final javax.accessibility.AccessibleState javax.accessibility.AccessibleState.INDETERMINATE
    javax::accessibility::AccessibleState* TRUNCATED; // public static final javax.accessibility.AccessibleState javax.accessibility.AccessibleState.TRUNCATED
};
}
}
#endif
