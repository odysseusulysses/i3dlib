#ifndef java_awt_AttributeValue_H
#define java_awt_AttributeValue_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace lang {
class String;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
class AttributeValue : public java::lang::Object {
  public:
    AttributeValue(JavaMarker* dummy);
    AttributeValue(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual int hashCode(); // public int java.awt.AttributeValue.hashCode()
    virtual const char* toString(); // public java.lang.String java.awt.AttributeValue.toString()

};
}
}
#endif
