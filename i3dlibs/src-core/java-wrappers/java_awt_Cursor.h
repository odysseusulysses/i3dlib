#ifndef java_awt_Cursor_H
#define java_awt_Cursor_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace io {
class Serializable;
}
}

namespace java {
namespace lang {
class String;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
class Cursor : public java::lang::Object {
  public:
    Cursor(JavaMarker* dummy);
    Cursor(jobject obj);
    Cursor(int arg1); // public java.awt.Cursor(int)

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual const char* toString(); // public java.lang.String java.awt.Cursor.toString()
    virtual const char* getName(); // public java.lang.String java.awt.Cursor.getName()
    virtual int getType(); // public int java.awt.Cursor.getType()
    virtual java::awt::Cursor* getPredefinedCursor(int arg1); // public static java.awt.Cursor java.awt.Cursor.getPredefinedCursor(int)
    virtual java::awt::Cursor* getDefaultCursor(); // public static java.awt.Cursor java.awt.Cursor.getDefaultCursor()
    virtual java::awt::Cursor* getSystemCustomCursor(const char* arg1); // public static java.awt.Cursor java.awt.Cursor.getSystemCustomCursor(java.lang.String) throws java.awt.AWTException,java.awt.HeadlessException

    int DEFAULT_CURSOR; // public static final int java.awt.Cursor.DEFAULT_CURSOR
    int CROSSHAIR_CURSOR; // public static final int java.awt.Cursor.CROSSHAIR_CURSOR
    int TEXT_CURSOR; // public static final int java.awt.Cursor.TEXT_CURSOR
    int WAIT_CURSOR; // public static final int java.awt.Cursor.WAIT_CURSOR
    int SW_RESIZE_CURSOR; // public static final int java.awt.Cursor.SW_RESIZE_CURSOR
    int SE_RESIZE_CURSOR; // public static final int java.awt.Cursor.SE_RESIZE_CURSOR
    int NW_RESIZE_CURSOR; // public static final int java.awt.Cursor.NW_RESIZE_CURSOR
    int NE_RESIZE_CURSOR; // public static final int java.awt.Cursor.NE_RESIZE_CURSOR
    int N_RESIZE_CURSOR; // public static final int java.awt.Cursor.N_RESIZE_CURSOR
    int S_RESIZE_CURSOR; // public static final int java.awt.Cursor.S_RESIZE_CURSOR
    int W_RESIZE_CURSOR; // public static final int java.awt.Cursor.W_RESIZE_CURSOR
    int E_RESIZE_CURSOR; // public static final int java.awt.Cursor.E_RESIZE_CURSOR
    int HAND_CURSOR; // public static final int java.awt.Cursor.HAND_CURSOR
    int MOVE_CURSOR; // public static final int java.awt.Cursor.MOVE_CURSOR
    int CUSTOM_CURSOR; // public static final int java.awt.Cursor.CUSTOM_CURSOR
};
}
}
#endif
