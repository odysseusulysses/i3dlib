#ifndef java_lang_Boolean_H
#define java_lang_Boolean_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace io {
class Serializable;
}
}

namespace java {
namespace lang {
class Comparable;
}
}

namespace java {
namespace lang {
class String;
}
}

namespace java {
namespace lang {
class Class;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace lang {
class Boolean : public java::lang::Object {
  public:
    Boolean(JavaMarker* dummy);
    Boolean(jobject obj);
    Boolean(const char* arg1); // public java.lang.Boolean(java.lang.String)
    Boolean(bool arg1); // public java.lang.Boolean(boolean)

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual int hashCode(); // public int java.lang.Boolean.hashCode()
    virtual int compareTo(java::lang::Object* arg1); // public int java.lang.Boolean.compareTo(java.lang.Object)
    virtual int compareTo(java::lang::Boolean* arg1); // public int java.lang.Boolean.compareTo(java.lang.Boolean)
    virtual bool getBoolean(const char* arg1); // public static boolean java.lang.Boolean.getBoolean(java.lang.String)
    virtual bool equals(java::lang::Object* arg1); // public boolean java.lang.Boolean.equals(java.lang.Object)
    virtual const char* toString(); // public java.lang.String java.lang.Boolean.toString()
    virtual const char* toString(bool arg1); // public static java.lang.String java.lang.Boolean.toString(boolean)
    virtual java::lang::Boolean* valueOf(bool arg1); // public static java.lang.Boolean java.lang.Boolean.valueOf(boolean)
    virtual java::lang::Boolean* valueOf(const char* arg1); // public static java.lang.Boolean java.lang.Boolean.valueOf(java.lang.String)
    virtual bool booleanValue(); // public boolean java.lang.Boolean.booleanValue()
    virtual bool parseBoolean(const char* arg1); // public static boolean java.lang.Boolean.parseBoolean(java.lang.String)

    java::lang::Boolean* TRUE; // public static final java.lang.Boolean java.lang.Boolean.TRUE
    java::lang::Boolean* FALSE; // public static final java.lang.Boolean java.lang.Boolean.FALSE
    java::lang::Class* TYPE; // public static final java.lang.Class java.lang.Boolean.TYPE
};
}
}
#endif
