#ifndef java_awt_event_ComponentEvent_H
#define java_awt_event_ComponentEvent_H
#include <jni.h>
#include <java_marker.h>
#include <java_awt_AWTEvent.h>

namespace java {
namespace awt {
class Component;
}
}

namespace java {
namespace lang {
class String;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace event {
class ComponentEvent : public java::awt::AWTEvent {
  public:
    ComponentEvent(JavaMarker* dummy);
    ComponentEvent(jobject obj);
    ComponentEvent(java::awt::Component* arg1, int arg2); // public java.awt.event.ComponentEvent(java.awt.Component,int)

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual java::awt::Component* getComponent(); // public java.awt.Component java.awt.event.ComponentEvent.getComponent()
    virtual const char* paramString(); // public java.lang.String java.awt.event.ComponentEvent.paramString()

    int COMPONENT_FIRST; // public static final int java.awt.event.ComponentEvent.COMPONENT_FIRST
    int COMPONENT_LAST; // public static final int java.awt.event.ComponentEvent.COMPONENT_LAST
    int COMPONENT_MOVED; // public static final int java.awt.event.ComponentEvent.COMPONENT_MOVED
    int COMPONENT_RESIZED; // public static final int java.awt.event.ComponentEvent.COMPONENT_RESIZED
    int COMPONENT_SHOWN; // public static final int java.awt.event.ComponentEvent.COMPONENT_SHOWN
    int COMPONENT_HIDDEN; // public static final int java.awt.event.ComponentEvent.COMPONENT_HIDDEN
};
}
}
}
#endif
