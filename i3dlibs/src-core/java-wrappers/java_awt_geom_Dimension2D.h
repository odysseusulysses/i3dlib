#ifndef java_awt_geom_Dimension2D_H
#define java_awt_geom_Dimension2D_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace lang {
class Cloneable;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace geom {
class Dimension2D : public java::lang::Object {
  public:
    Dimension2D(JavaMarker* dummy);
    Dimension2D(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual java::lang::Object* clone(); // public java.lang.Object java.awt.geom.Dimension2D.clone()
    virtual void setSize(double arg1, double arg2); // public abstract void java.awt.geom.Dimension2D.setSize(double,double)
    virtual void setSize(java::awt::geom::Dimension2D* arg1); // public void java.awt.geom.Dimension2D.setSize(java.awt.geom.Dimension2D)
    virtual double getHeight(); // public abstract double java.awt.geom.Dimension2D.getHeight()
    virtual double getWidth(); // public abstract double java.awt.geom.Dimension2D.getWidth()

};
}
}
}
#endif
