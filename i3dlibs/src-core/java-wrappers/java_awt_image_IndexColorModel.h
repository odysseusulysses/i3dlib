#ifndef java_awt_image_IndexColorModel_H
#define java_awt_image_IndexColorModel_H
#include <jni.h>
#include <java_marker.h>
#include <java_awt_image_ColorModel.h>

namespace java {
namespace math {
class BigInteger;
}
}

namespace java {
namespace lang {
class String;
}
}

namespace java {
namespace awt {
namespace image {
class SampleModel;
}
}
}

namespace java {
namespace awt {
namespace image {
class WritableRaster;
}
}
}

namespace java {
namespace lang {
class Object;
}
}

namespace java {
namespace awt {
namespace image {
class Raster;
}
}
}

namespace java {
namespace awt {
namespace image {
class BufferedImage;
}
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace image {
class IndexColorModel : public java::awt::image::ColorModel {
  public:
    IndexColorModel(JavaMarker* dummy);
    IndexColorModel(jobject obj);
    IndexColorModel(int arg1, int arg2, JavaByteArray* arg3, JavaByteArray* arg4, JavaByteArray* arg5); // public java.awt.image.IndexColorModel(int,int,byte[],byte[],byte[])
    IndexColorModel(int arg1, int arg2, JavaByteArray* arg3, JavaByteArray* arg4, JavaByteArray* arg5, int arg6); // public java.awt.image.IndexColorModel(int,int,byte[],byte[],byte[],int)
    IndexColorModel(int arg1, int arg2, JavaByteArray* arg3, JavaByteArray* arg4, JavaByteArray* arg5, JavaByteArray* arg6); // public java.awt.image.IndexColorModel(int,int,byte[],byte[],byte[],byte[])
    IndexColorModel(int arg1, int arg2, JavaByteArray* arg3, int arg4, bool arg5); // public java.awt.image.IndexColorModel(int,int,byte[],int,boolean)
    IndexColorModel(int arg1, int arg2, JavaIntArray* arg3, int arg4, int arg5, java::math::BigInteger* arg6); // public java.awt.image.IndexColorModel(int,int,int[],int,int,java.math.BigInteger)
    IndexColorModel(int arg1, int arg2, JavaIntArray* arg3, int arg4, bool arg5, int arg6, int arg7); // public java.awt.image.IndexColorModel(int,int,int[],int,boolean,int,int)
    IndexColorModel(int arg1, int arg2, JavaByteArray* arg3, int arg4, bool arg5, int arg6); // public java.awt.image.IndexColorModel(int,int,byte[],int,boolean,int)

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual void finalize(); // public void java.awt.image.IndexColorModel.finalize()
    virtual const char* toString(); // public java.lang.String java.awt.image.IndexColorModel.toString()
    virtual bool isValid(int arg1); // public boolean java.awt.image.IndexColorModel.isValid(int)
    virtual bool isValid(); // public boolean java.awt.image.IndexColorModel.isValid()
    virtual java::awt::image::SampleModel* createCompatibleSampleModel(int arg1, int arg2); // public java.awt.image.SampleModel java.awt.image.IndexColorModel.createCompatibleSampleModel(int,int)
    virtual java::awt::image::WritableRaster* createCompatibleWritableRaster(int arg1, int arg2); // public java.awt.image.WritableRaster java.awt.image.IndexColorModel.createCompatibleWritableRaster(int,int)
    virtual JavaIntArray* getComponentSize(); // public int[] java.awt.image.IndexColorModel.getComponentSize()
    virtual java::lang::Object* getDataElements(int arg1, java::lang::Object* arg2); // public synchronized java.lang.Object java.awt.image.IndexColorModel.getDataElements(int,java.lang.Object)
    virtual java::lang::Object* getDataElements(JavaIntArray* arg1, int arg2, java::lang::Object* arg3); // public java.lang.Object java.awt.image.IndexColorModel.getDataElements(int[],int,java.lang.Object)
    virtual int getMapSize(); // public final int java.awt.image.IndexColorModel.getMapSize()
    virtual int getRGB(int arg1); // public final int java.awt.image.IndexColorModel.getRGB(int)
    virtual int getTransparency(); // public int java.awt.image.IndexColorModel.getTransparency()
    virtual bool isCompatibleRaster(java::awt::image::Raster* arg1); // public boolean java.awt.image.IndexColorModel.isCompatibleRaster(java.awt.image.Raster)
    virtual java::awt::image::BufferedImage* convertToIntDiscrete(java::awt::image::Raster* arg1, bool arg2); // public java.awt.image.BufferedImage java.awt.image.IndexColorModel.convertToIntDiscrete(java.awt.image.Raster,boolean)
    virtual int getAlpha(int arg1); // public final int java.awt.image.IndexColorModel.getAlpha(int)
    virtual void getAlphas(JavaByteArray* arg1); // public final void java.awt.image.IndexColorModel.getAlphas(byte[])
    virtual int getBlue(int arg1); // public final int java.awt.image.IndexColorModel.getBlue(int)
    virtual void getBlues(JavaByteArray* arg1); // public final void java.awt.image.IndexColorModel.getBlues(byte[])
    virtual JavaIntArray* getComponents(java::lang::Object* arg1, JavaIntArray* arg2, int arg3); // public int[] java.awt.image.IndexColorModel.getComponents(java.lang.Object,int[],int)
    virtual JavaIntArray* getComponents(int arg1, JavaIntArray* arg2, int arg3); // public int[] java.awt.image.IndexColorModel.getComponents(int,int[],int)
    virtual int getDataElement(JavaIntArray* arg1, int arg2); // public int java.awt.image.IndexColorModel.getDataElement(int[],int)
    virtual int getGreen(int arg1); // public final int java.awt.image.IndexColorModel.getGreen(int)
    virtual void getGreens(JavaByteArray* arg1); // public final void java.awt.image.IndexColorModel.getGreens(byte[])
    virtual void getRGBs(JavaIntArray* arg1); // public final void java.awt.image.IndexColorModel.getRGBs(int[])
    virtual int getRed(int arg1); // public final int java.awt.image.IndexColorModel.getRed(int)
    virtual void getReds(JavaByteArray* arg1); // public final void java.awt.image.IndexColorModel.getReds(byte[])
    virtual int getTransparentPixel(); // public final int java.awt.image.IndexColorModel.getTransparentPixel()
    virtual java::math::BigInteger* getValidPixels(); // public java.math.BigInteger java.awt.image.IndexColorModel.getValidPixels()
    virtual bool isCompatibleSampleModel(java::awt::image::SampleModel* arg1); // public boolean java.awt.image.IndexColorModel.isCompatibleSampleModel(java.awt.image.SampleModel)

};
}
}
}
#endif
