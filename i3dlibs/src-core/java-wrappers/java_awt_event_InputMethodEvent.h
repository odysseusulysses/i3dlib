#ifndef java_awt_event_InputMethodEvent_H
#define java_awt_event_InputMethodEvent_H
#include <jni.h>
#include <java_marker.h>
#include <java_awt_AWTEvent.h>

namespace java {
namespace awt {
class Component;
}
}

namespace java {
namespace text {
class AttributedCharacterIterator;
}
}

namespace java {
namespace awt {
namespace font {
class TextHitInfo;
}
}
}

namespace java {
namespace lang {
class String;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace event {
class InputMethodEvent : public java::awt::AWTEvent {
  public:
    InputMethodEvent(JavaMarker* dummy);
    InputMethodEvent(jobject obj);
    InputMethodEvent(java::awt::Component* arg1, int arg2, java::text::AttributedCharacterIterator* arg3, int arg4, java::awt::font::TextHitInfo* arg5, java::awt::font::TextHitInfo* arg6); // public java.awt.event.InputMethodEvent(java.awt.Component,int,java.text.AttributedCharacterIterator,int,java.awt.font.TextHitInfo,java.awt.font.TextHitInfo)
    InputMethodEvent(java::awt::Component* arg1, int arg2, java::awt::font::TextHitInfo* arg3, java::awt::font::TextHitInfo* arg4); // public java.awt.event.InputMethodEvent(java.awt.Component,int,java.awt.font.TextHitInfo,java.awt.font.TextHitInfo)
    InputMethodEvent(java::awt::Component* arg1, int arg2, long arg3, java::text::AttributedCharacterIterator* arg4, int arg5, java::awt::font::TextHitInfo* arg6, java::awt::font::TextHitInfo* arg7); // public java.awt.event.InputMethodEvent(java.awt.Component,int,long,java.text.AttributedCharacterIterator,int,java.awt.font.TextHitInfo,java.awt.font.TextHitInfo)

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual void consume(); // public void java.awt.event.InputMethodEvent.consume()
    virtual const char* paramString(); // public java.lang.String java.awt.event.InputMethodEvent.paramString()
    virtual long getWhen(); // public long java.awt.event.InputMethodEvent.getWhen()
    virtual bool isConsumed(); // public boolean java.awt.event.InputMethodEvent.isConsumed()
    virtual java::awt::font::TextHitInfo* getCaret(); // public java.awt.font.TextHitInfo java.awt.event.InputMethodEvent.getCaret()
    virtual int getCommittedCharacterCount(); // public int java.awt.event.InputMethodEvent.getCommittedCharacterCount()
    virtual java::text::AttributedCharacterIterator* getText(); // public java.text.AttributedCharacterIterator java.awt.event.InputMethodEvent.getText()
    virtual java::awt::font::TextHitInfo* getVisiblePosition(); // public java.awt.font.TextHitInfo java.awt.event.InputMethodEvent.getVisiblePosition()

    int INPUT_METHOD_FIRST; // public static final int java.awt.event.InputMethodEvent.INPUT_METHOD_FIRST
    int INPUT_METHOD_TEXT_CHANGED; // public static final int java.awt.event.InputMethodEvent.INPUT_METHOD_TEXT_CHANGED
    int CARET_POSITION_CHANGED; // public static final int java.awt.event.InputMethodEvent.CARET_POSITION_CHANGED
    int INPUT_METHOD_LAST; // public static final int java.awt.event.InputMethodEvent.INPUT_METHOD_LAST
};
}
}
}
#endif
