#ifndef java_lang_System_H
#define java_lang_System_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace lang {
class String;
}
}

namespace java {
namespace lang {
class SecurityManager;
}
}

namespace java {
namespace io {
class Console;
}
}

namespace java {
namespace util {
class Properties;
}
}

namespace java {
namespace util {
class Map;
}
}

namespace java {
namespace nio {
namespace channels {
class Channel;
}
}
}

namespace java {
namespace io {
class PrintStream;
}
}

namespace java {
namespace io {
class InputStream;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace lang {
class System : public java::lang::Object {
  public:
    System(JavaMarker* dummy);
    System(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
/*    virtual void arraycopy(java::lang::Object* arg1, int arg2, java::lang::Object* arg3, int arg4, int arg5); // public static native void java.lang.System.arraycopy(java.lang.Object,int,java.lang.Object,int,int)
    virtual long currentTimeMillis(); // public static native long java.lang.System.currentTimeMillis()
    virtual long nanoTime(); // public static native long java.lang.System.nanoTime()
    virtual int identityHashCode(java::lang::Object* arg1); // public static native int java.lang.System.identityHashCode(java.lang.Object)
    virtual void exit(int arg1); // public static void java.lang.System.exit(int)
    virtual void runFinalizersOnExit(bool arg1); // public static void java.lang.System.runFinalizersOnExit(boolean)
    virtual const char* setProperty(const char* arg1, const char* arg2); // public static java.lang.String java.lang.System.setProperty(java.lang.String,java.lang.String)
*/    virtual const char* getProperty(const char* arg1, const char* arg2); // public static java.lang.String java.lang.System.getProperty(java.lang.String,java.lang.String)
    virtual const char* getProperty(const char* arg1); // public static java.lang.String java.lang.System.getProperty(java.lang.String)
/*    virtual java::lang::SecurityManager* getSecurityManager(); // public static java.lang.SecurityManager java.lang.System.getSecurityManager()
    virtual void load(const char* arg1); // public static void java.lang.System.load(java.lang.String)
    virtual void loadLibrary(const char* arg1); // public static void java.lang.System.loadLibrary(java.lang.String)
    virtual const char* mapLibraryName(const char* arg1); // public static native java.lang.String java.lang.System.mapLibraryName(java.lang.String)
    virtual const char* clearProperty(const char* arg1); // public static java.lang.String java.lang.System.clearProperty(java.lang.String)
    virtual java::io::Console* console(); // public static java.io.Console java.lang.System.console()
    virtual void gc(); // public static void java.lang.System.gc()
    virtual java::util::Properties* getProperties(); // public static java.util.Properties java.lang.System.getProperties()
    virtual const char* getenv(const char* arg1); // public static java.lang.String java.lang.System.getenv(java.lang.String)
    virtual java::util::Map* getenv(); // public static java.util.Map java.lang.System.getenv()
    virtual java::nio::channels::Channel* inheritedChannel(); // public static java.nio.channels.Channel java.lang.System.inheritedChannel() throws java.io.IOException
    virtual void runFinalization(); // public static void java.lang.System.runFinalization()
    virtual void setErr(java::io::PrintStream* arg1); // public static void java.lang.System.setErr(java.io.PrintStream)
    virtual void setIn(java::io::InputStream* arg1); // public static void java.lang.System.setIn(java.io.InputStream)
    virtual void setOut(java::io::PrintStream* arg1); // public static void java.lang.System.setOut(java.io.PrintStream)
    virtual void setProperties(java::util::Properties* arg1); // public static void java.lang.System.setProperties(java.util.Properties)
    virtual void setSecurityManager(java::lang::SecurityManager* arg1); // public static void java.lang.System.setSecurityManager(java.lang.SecurityManager)
*/
    java::io::InputStream* in; // public static final java.io.InputStream java.lang.System.in
    java::io::PrintStream* out; // public static final java.io.PrintStream java.lang.System.out
    java::io::PrintStream* err; // public static final java.io.PrintStream java.lang.System.err
};
}
}
#endif
