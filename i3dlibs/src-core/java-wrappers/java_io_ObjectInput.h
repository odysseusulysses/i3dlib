#ifndef java_io_ObjectInput_H
#define java_io_ObjectInput_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace io {
class DataInput;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace io {
class ObjectInput : public java::lang::Object {
  public:
    ObjectInput(JavaMarker* dummy);
    ObjectInput(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual void close(); // public abstract void java.io.ObjectInput.close() throws java.io.IOException
    virtual java::lang::Object* readObject(); // public abstract java.lang.Object java.io.ObjectInput.readObject() throws java.lang.ClassNotFoundException,java.io.IOException
    virtual int read(); // public abstract int java.io.ObjectInput.read() throws java.io.IOException
    virtual int read(JavaByteArray* arg1); // public abstract int java.io.ObjectInput.read(byte[]) throws java.io.IOException
    virtual int read(JavaByteArray* arg1, int arg2, int arg3); // public abstract int java.io.ObjectInput.read(byte[],int,int) throws java.io.IOException
    virtual int available(); // public abstract int java.io.ObjectInput.available() throws java.io.IOException
    virtual long skip(long arg1); // public abstract long java.io.ObjectInput.skip(long) throws java.io.IOException

};
}
}
#endif
