#ifndef java_awt_geom_Rectangle2D_H
#define java_awt_geom_Rectangle2D_H
#include <jni.h>
#include <java_marker.h>
#include <java_awt_geom_RectangularShape.h>

namespace java {
namespace awt {
namespace geom {
class Point2D;
}
}
}

namespace java {
namespace lang {
class Object;
}
}

namespace java {
namespace awt {
namespace geom {
class PathIterator;
}
}
}

namespace java {
namespace awt {
namespace geom {
class AffineTransform;
}
}
}

namespace java {
namespace awt {
namespace geom {
class Line2D;
}
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace geom {
class Rectangle2D : public java::awt::geom::RectangularShape {
  public:
    Rectangle2D(JavaMarker* dummy);
    Rectangle2D(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual int hashCode(); // public int java.awt.geom.Rectangle2D.hashCode()
    virtual void add(java::awt::geom::Point2D* arg1); // public void java.awt.geom.Rectangle2D.add(java.awt.geom.Point2D)
    virtual void add(double arg1, double arg2); // public void java.awt.geom.Rectangle2D.add(double,double)
    virtual void add(java::awt::geom::Rectangle2D* arg1); // public void java.awt.geom.Rectangle2D.add(java.awt.geom.Rectangle2D)
    virtual bool equals(java::lang::Object* arg1); // public boolean java.awt.geom.Rectangle2D.equals(java.lang.Object)
    virtual bool contains(double arg1, double arg2); // public boolean java.awt.geom.Rectangle2D.contains(double,double)
    virtual bool contains(double arg1, double arg2, double arg3, double arg4); // public boolean java.awt.geom.Rectangle2D.contains(double,double,double,double)
    virtual bool intersects(double arg1, double arg2, double arg3, double arg4); // public boolean java.awt.geom.Rectangle2D.intersects(double,double,double,double)
    virtual void union_(java::awt::geom::Rectangle2D* arg1, java::awt::geom::Rectangle2D* arg2, java::awt::geom::Rectangle2D* arg3); // public static void java.awt.geom.Rectangle2D.union(java.awt.geom.Rectangle2D,java.awt.geom.Rectangle2D,java.awt.geom.Rectangle2D)
    virtual void setRect(java::awt::geom::Rectangle2D* arg1); // public void java.awt.geom.Rectangle2D.setRect(java.awt.geom.Rectangle2D)
    virtual void setRect(double arg1, double arg2, double arg3, double arg4); // public abstract void java.awt.geom.Rectangle2D.setRect(double,double,double,double)
    virtual java::awt::geom::Rectangle2D* createIntersection(java::awt::geom::Rectangle2D* arg1); // public abstract java.awt.geom.Rectangle2D java.awt.geom.Rectangle2D.createIntersection(java.awt.geom.Rectangle2D)
    virtual java::awt::geom::Rectangle2D* createUnion(java::awt::geom::Rectangle2D* arg1); // public abstract java.awt.geom.Rectangle2D java.awt.geom.Rectangle2D.createUnion(java.awt.geom.Rectangle2D)
    virtual java::awt::geom::Rectangle2D* getBounds2D(); // public java.awt.geom.Rectangle2D java.awt.geom.Rectangle2D.getBounds2D()
    virtual void intersect(java::awt::geom::Rectangle2D* arg1, java::awt::geom::Rectangle2D* arg2, java::awt::geom::Rectangle2D* arg3); // public static void java.awt.geom.Rectangle2D.intersect(java.awt.geom.Rectangle2D,java.awt.geom.Rectangle2D,java.awt.geom.Rectangle2D)
    virtual int outcode(double arg1, double arg2); // public abstract int java.awt.geom.Rectangle2D.outcode(double,double)
    virtual int outcode(java::awt::geom::Point2D* arg1); // public int java.awt.geom.Rectangle2D.outcode(java.awt.geom.Point2D)
    virtual java::awt::geom::PathIterator* getPathIterator(java::awt::geom::AffineTransform* arg1); // public java.awt.geom.PathIterator java.awt.geom.Rectangle2D.getPathIterator(java.awt.geom.AffineTransform)
    virtual java::awt::geom::PathIterator* getPathIterator(java::awt::geom::AffineTransform* arg1, double arg2); // public java.awt.geom.PathIterator java.awt.geom.Rectangle2D.getPathIterator(java.awt.geom.AffineTransform,double)
    virtual bool intersectsLine(double arg1, double arg2, double arg3, double arg4); // public boolean java.awt.geom.Rectangle2D.intersectsLine(double,double,double,double)
    virtual bool intersectsLine(java::awt::geom::Line2D* arg1); // public boolean java.awt.geom.Rectangle2D.intersectsLine(java.awt.geom.Line2D)
    virtual void setFrame(double arg1, double arg2, double arg3, double arg4); // public void java.awt.geom.Rectangle2D.setFrame(double,double,double,double)

    int OUT_LEFT; // public static final int java.awt.geom.Rectangle2D.OUT_LEFT
    int OUT_TOP; // public static final int java.awt.geom.Rectangle2D.OUT_TOP
    int OUT_RIGHT; // public static final int java.awt.geom.Rectangle2D.OUT_RIGHT
    int OUT_BOTTOM; // public static final int java.awt.geom.Rectangle2D.OUT_BOTTOM
};
}
}
}
#endif
