#ifndef javax_accessibility_AccessibleRole_H
#define javax_accessibility_AccessibleRole_H
#include <jni.h>
#include <java_marker.h>
#include <javax_accessibility_AccessibleBundle.h>
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace javax {
namespace accessibility {
class AccessibleRole : public javax::accessibility::AccessibleBundle {
  public:
    AccessibleRole(JavaMarker* dummy);
    AccessibleRole(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);

    javax::accessibility::AccessibleRole* ALERT; // public static final javax.accessibility.AccessibleRole javax.accessibility.AccessibleRole.ALERT
    javax::accessibility::AccessibleRole* COLUMN_HEADER; // public static final javax.accessibility.AccessibleRole javax.accessibility.AccessibleRole.COLUMN_HEADER
    javax::accessibility::AccessibleRole* CANVAS; // public static final javax.accessibility.AccessibleRole javax.accessibility.AccessibleRole.CANVAS
    javax::accessibility::AccessibleRole* COMBO_BOX; // public static final javax.accessibility.AccessibleRole javax.accessibility.AccessibleRole.COMBO_BOX
    javax::accessibility::AccessibleRole* DESKTOP_ICON; // public static final javax.accessibility.AccessibleRole javax.accessibility.AccessibleRole.DESKTOP_ICON
    javax::accessibility::AccessibleRole* HTML_CONTAINER; // public static final javax.accessibility.AccessibleRole javax.accessibility.AccessibleRole.HTML_CONTAINER
    javax::accessibility::AccessibleRole* INTERNAL_FRAME; // public static final javax.accessibility.AccessibleRole javax.accessibility.AccessibleRole.INTERNAL_FRAME
    javax::accessibility::AccessibleRole* DESKTOP_PANE; // public static final javax.accessibility.AccessibleRole javax.accessibility.AccessibleRole.DESKTOP_PANE
    javax::accessibility::AccessibleRole* OPTION_PANE; // public static final javax.accessibility.AccessibleRole javax.accessibility.AccessibleRole.OPTION_PANE
    javax::accessibility::AccessibleRole* WINDOW; // public static final javax.accessibility.AccessibleRole javax.accessibility.AccessibleRole.WINDOW
    javax::accessibility::AccessibleRole* FRAME; // public static final javax.accessibility.AccessibleRole javax.accessibility.AccessibleRole.FRAME
    javax::accessibility::AccessibleRole* DIALOG; // public static final javax.accessibility.AccessibleRole javax.accessibility.AccessibleRole.DIALOG
    javax::accessibility::AccessibleRole* COLOR_CHOOSER; // public static final javax.accessibility.AccessibleRole javax.accessibility.AccessibleRole.COLOR_CHOOSER
    javax::accessibility::AccessibleRole* DIRECTORY_PANE; // public static final javax.accessibility.AccessibleRole javax.accessibility.AccessibleRole.DIRECTORY_PANE
    javax::accessibility::AccessibleRole* FILE_CHOOSER; // public static final javax.accessibility.AccessibleRole javax.accessibility.AccessibleRole.FILE_CHOOSER
    javax::accessibility::AccessibleRole* FILLER; // public static final javax.accessibility.AccessibleRole javax.accessibility.AccessibleRole.FILLER
    javax::accessibility::AccessibleRole* HYPERLINK; // public static final javax.accessibility.AccessibleRole javax.accessibility.AccessibleRole.HYPERLINK
    javax::accessibility::AccessibleRole* ICON; // public static final javax.accessibility.AccessibleRole javax.accessibility.AccessibleRole.ICON
    javax::accessibility::AccessibleRole* LABEL; // public static final javax.accessibility.AccessibleRole javax.accessibility.AccessibleRole.LABEL
    javax::accessibility::AccessibleRole* ROOT_PANE; // public static final javax.accessibility.AccessibleRole javax.accessibility.AccessibleRole.ROOT_PANE
    javax::accessibility::AccessibleRole* GLASS_PANE; // public static final javax.accessibility.AccessibleRole javax.accessibility.AccessibleRole.GLASS_PANE
    javax::accessibility::AccessibleRole* LAYERED_PANE; // public static final javax.accessibility.AccessibleRole javax.accessibility.AccessibleRole.LAYERED_PANE
    javax::accessibility::AccessibleRole* LIST; // public static final javax.accessibility.AccessibleRole javax.accessibility.AccessibleRole.LIST
    javax::accessibility::AccessibleRole* LIST_ITEM; // public static final javax.accessibility.AccessibleRole javax.accessibility.AccessibleRole.LIST_ITEM
    javax::accessibility::AccessibleRole* MENU_BAR; // public static final javax.accessibility.AccessibleRole javax.accessibility.AccessibleRole.MENU_BAR
    javax::accessibility::AccessibleRole* POPUP_MENU; // public static final javax.accessibility.AccessibleRole javax.accessibility.AccessibleRole.POPUP_MENU
    javax::accessibility::AccessibleRole* MENU; // public static final javax.accessibility.AccessibleRole javax.accessibility.AccessibleRole.MENU
    javax::accessibility::AccessibleRole* MENU_ITEM; // public static final javax.accessibility.AccessibleRole javax.accessibility.AccessibleRole.MENU_ITEM
    javax::accessibility::AccessibleRole* SEPARATOR; // public static final javax.accessibility.AccessibleRole javax.accessibility.AccessibleRole.SEPARATOR
    javax::accessibility::AccessibleRole* PAGE_TAB_LIST; // public static final javax.accessibility.AccessibleRole javax.accessibility.AccessibleRole.PAGE_TAB_LIST
    javax::accessibility::AccessibleRole* PAGE_TAB; // public static final javax.accessibility.AccessibleRole javax.accessibility.AccessibleRole.PAGE_TAB
    javax::accessibility::AccessibleRole* PANEL; // public static final javax.accessibility.AccessibleRole javax.accessibility.AccessibleRole.PANEL
    javax::accessibility::AccessibleRole* PROGRESS_BAR; // public static final javax.accessibility.AccessibleRole javax.accessibility.AccessibleRole.PROGRESS_BAR
    javax::accessibility::AccessibleRole* PASSWORD_TEXT; // public static final javax.accessibility.AccessibleRole javax.accessibility.AccessibleRole.PASSWORD_TEXT
    javax::accessibility::AccessibleRole* PUSH_BUTTON; // public static final javax.accessibility.AccessibleRole javax.accessibility.AccessibleRole.PUSH_BUTTON
    javax::accessibility::AccessibleRole* TOGGLE_BUTTON; // public static final javax.accessibility.AccessibleRole javax.accessibility.AccessibleRole.TOGGLE_BUTTON
    javax::accessibility::AccessibleRole* CHECK_BOX; // public static final javax.accessibility.AccessibleRole javax.accessibility.AccessibleRole.CHECK_BOX
    javax::accessibility::AccessibleRole* RADIO_BUTTON; // public static final javax.accessibility.AccessibleRole javax.accessibility.AccessibleRole.RADIO_BUTTON
    javax::accessibility::AccessibleRole* ROW_HEADER; // public static final javax.accessibility.AccessibleRole javax.accessibility.AccessibleRole.ROW_HEADER
    javax::accessibility::AccessibleRole* SCROLL_PANE; // public static final javax.accessibility.AccessibleRole javax.accessibility.AccessibleRole.SCROLL_PANE
    javax::accessibility::AccessibleRole* SCROLL_BAR; // public static final javax.accessibility.AccessibleRole javax.accessibility.AccessibleRole.SCROLL_BAR
    javax::accessibility::AccessibleRole* VIEWPORT; // public static final javax.accessibility.AccessibleRole javax.accessibility.AccessibleRole.VIEWPORT
    javax::accessibility::AccessibleRole* SLIDER; // public static final javax.accessibility.AccessibleRole javax.accessibility.AccessibleRole.SLIDER
    javax::accessibility::AccessibleRole* SPLIT_PANE; // public static final javax.accessibility.AccessibleRole javax.accessibility.AccessibleRole.SPLIT_PANE
    javax::accessibility::AccessibleRole* TABLE; // public static final javax.accessibility.AccessibleRole javax.accessibility.AccessibleRole.TABLE
    javax::accessibility::AccessibleRole* TEXT; // public static final javax.accessibility.AccessibleRole javax.accessibility.AccessibleRole.TEXT
    javax::accessibility::AccessibleRole* TREE; // public static final javax.accessibility.AccessibleRole javax.accessibility.AccessibleRole.TREE
    javax::accessibility::AccessibleRole* TOOL_BAR; // public static final javax.accessibility.AccessibleRole javax.accessibility.AccessibleRole.TOOL_BAR
    javax::accessibility::AccessibleRole* TOOL_TIP; // public static final javax.accessibility.AccessibleRole javax.accessibility.AccessibleRole.TOOL_TIP
    javax::accessibility::AccessibleRole* AWT_COMPONENT; // public static final javax.accessibility.AccessibleRole javax.accessibility.AccessibleRole.AWT_COMPONENT
    javax::accessibility::AccessibleRole* SWING_COMPONENT; // public static final javax.accessibility.AccessibleRole javax.accessibility.AccessibleRole.SWING_COMPONENT
    javax::accessibility::AccessibleRole* UNKNOWN; // public static final javax.accessibility.AccessibleRole javax.accessibility.AccessibleRole.UNKNOWN
    javax::accessibility::AccessibleRole* STATUS_BAR; // public static final javax.accessibility.AccessibleRole javax.accessibility.AccessibleRole.STATUS_BAR
    javax::accessibility::AccessibleRole* DATE_EDITOR; // public static final javax.accessibility.AccessibleRole javax.accessibility.AccessibleRole.DATE_EDITOR
    javax::accessibility::AccessibleRole* SPIN_BOX; // public static final javax.accessibility.AccessibleRole javax.accessibility.AccessibleRole.SPIN_BOX
    javax::accessibility::AccessibleRole* FONT_CHOOSER; // public static final javax.accessibility.AccessibleRole javax.accessibility.AccessibleRole.FONT_CHOOSER
    javax::accessibility::AccessibleRole* GROUP_BOX; // public static final javax.accessibility.AccessibleRole javax.accessibility.AccessibleRole.GROUP_BOX
    javax::accessibility::AccessibleRole* HEADER; // public static final javax.accessibility.AccessibleRole javax.accessibility.AccessibleRole.HEADER
    javax::accessibility::AccessibleRole* FOOTER; // public static final javax.accessibility.AccessibleRole javax.accessibility.AccessibleRole.FOOTER
    javax::accessibility::AccessibleRole* PARAGRAPH; // public static final javax.accessibility.AccessibleRole javax.accessibility.AccessibleRole.PARAGRAPH
    javax::accessibility::AccessibleRole* RULER; // public static final javax.accessibility.AccessibleRole javax.accessibility.AccessibleRole.RULER
    javax::accessibility::AccessibleRole* EDITBAR; // public static final javax.accessibility.AccessibleRole javax.accessibility.AccessibleRole.EDITBAR
    javax::accessibility::AccessibleRole* PROGRESS_MONITOR; // public static final javax.accessibility.AccessibleRole javax.accessibility.AccessibleRole.PROGRESS_MONITOR
};
}
}
#endif
