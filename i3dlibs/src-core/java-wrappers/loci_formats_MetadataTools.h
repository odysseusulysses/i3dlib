#ifndef loci_formats_MetadataTools_H
#define loci_formats_MetadataTools_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace loci {
namespace formats {
namespace meta {
class MetadataStore;
}
}
}

namespace loci {
namespace formats {
class IFormatReader;
}
}

namespace java {
namespace lang {
class String;
}
}

namespace loci {
namespace formats {
namespace meta {
class MetadataRetrieve;
}
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace loci {
namespace formats {
class MetadataTools : public java::lang::Object {
  public:
    MetadataTools(JavaMarker* dummy);
    MetadataTools(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual void populatePixels(loci::formats::meta::MetadataStore* arg1, loci::formats::IFormatReader* arg2); // public static void loci.formats.MetadataTools.populatePixels(loci.formats.meta.MetadataStore,loci.formats.IFormatReader)
    virtual bool isOMEXMLMetadata(java::lang::Object* arg1); // public static boolean loci.formats.MetadataTools.isOMEXMLMetadata(java.lang.Object)
    virtual void populateOriginalMetadata(java::lang::Object* arg1, const char* arg2, const char* arg3); // public static void loci.formats.MetadataTools.populateOriginalMetadata(java.lang.Object,java.lang.String,java.lang.String)
    virtual loci::formats::meta::MetadataStore* createOMEXMLMetadata(const char* arg1, const char* arg2); // public static loci.formats.meta.MetadataStore loci.formats.MetadataTools.createOMEXMLMetadata(java.lang.String,java.lang.String)
    virtual loci::formats::meta::MetadataStore* createOMEXMLMetadata(const char* arg1); // public static loci.formats.meta.MetadataStore loci.formats.MetadataTools.createOMEXMLMetadata(java.lang.String)
    virtual loci::formats::meta::MetadataStore* createOMEXMLMetadata(); // public static loci.formats.meta.MetadataStore loci.formats.MetadataTools.createOMEXMLMetadata()
    virtual java::lang::Object* createOMEXMLRoot(const char* arg1); // public static java.lang.Object loci.formats.MetadataTools.createOMEXMLRoot(java.lang.String)
    virtual bool isOMEXMLRoot(java::lang::Object* arg1); // public static boolean loci.formats.MetadataTools.isOMEXMLRoot(java.lang.Object)
    virtual const char* getOMEXMLVersion(java::lang::Object* arg1); // public static java.lang.String loci.formats.MetadataTools.getOMEXMLVersion(java.lang.Object)
    virtual const char* getOMEXML(loci::formats::meta::MetadataRetrieve* arg1); // public static java.lang.String loci.formats.MetadataTools.getOMEXML(loci.formats.meta.MetadataRetrieve)
    virtual void validateOMEXML(const char* arg1); // public static void loci.formats.MetadataTools.validateOMEXML(java.lang.String)
    virtual void convertMetadata(loci::formats::meta::MetadataRetrieve* arg1, loci::formats::meta::MetadataStore* arg2); // public static void loci.formats.MetadataTools.convertMetadata(loci.formats.meta.MetadataRetrieve,loci.formats.meta.MetadataStore)
    virtual void convertMetadata(const char* arg1, loci::formats::meta::MetadataStore* arg2); // public static void loci.formats.MetadataTools.convertMetadata(java.lang.String,loci.formats.meta.MetadataStore)

};
}
}
#endif
