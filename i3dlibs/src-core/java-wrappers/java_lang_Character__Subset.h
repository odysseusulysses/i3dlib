#ifndef java_lang_Character__Subset_H
#define java_lang_Character__Subset_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace lang {
class String;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace lang {
class Character__Subset : public java::lang::Object {
  public:
    Character__Subset(JavaMarker* dummy);
    Character__Subset(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual int hashCode(); // public final int java.lang.Character$Subset.hashCode()
    virtual bool equals(java::lang::Object* arg1); // public final boolean java.lang.Character$Subset.equals(java.lang.Object)
    virtual const char* toString(); // public final java.lang.String java.lang.Character$Subset.toString()

};
}
}
#endif
