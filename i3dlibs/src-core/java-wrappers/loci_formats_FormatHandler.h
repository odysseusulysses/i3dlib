#ifndef loci_formats_FormatHandler_H
#define loci_formats_FormatHandler_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace loci {
namespace formats {
class IFormatHandler;
}
}

namespace java {
namespace lang {
class String;
}
}

namespace loci {
namespace formats {
class StatusListener;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace loci {
namespace formats {
class FormatHandler : public java::lang::Object {
  public:
    FormatHandler(JavaMarker* dummy);
    FormatHandler(jobject obj);
    FormatHandler(const char* arg1, JavaObjectArray* arg2); // public loci.formats.FormatHandler(java.lang.String,java.lang.String[])
    FormatHandler(const char* arg1, const char* arg2); // public loci.formats.FormatHandler(java.lang.String,java.lang.String)

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual bool isThisType(const char* arg1); // public boolean loci.formats.FormatHandler.isThisType(java.lang.String)
    virtual bool isThisType(const char* arg1, bool arg2); // public boolean loci.formats.FormatHandler.isThisType(java.lang.String,boolean)
    virtual void setId(const char* arg1); // public void loci.formats.FormatHandler.setId(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual const char* getFormat(); // public java.lang.String loci.formats.FormatHandler.getFormat()
    virtual JavaObjectArray* getSuffixes(); // public java.lang.String[] loci.formats.FormatHandler.getSuffixes()
    virtual void addStatusListener(loci::formats::StatusListener* arg1); // public void loci.formats.FormatHandler.addStatusListener(loci.formats.StatusListener)
    virtual void removeStatusListener(loci::formats::StatusListener* arg1); // public void loci.formats.FormatHandler.removeStatusListener(loci.formats.StatusListener)
    virtual JavaObjectArray* getStatusListeners(); // public loci.formats.StatusListener[] loci.formats.FormatHandler.getStatusListeners()
    virtual void setDebug(bool arg1); // public static void loci.formats.FormatHandler.setDebug(boolean)
    virtual void setDebugLevel(int arg1); // public static void loci.formats.FormatHandler.setDebugLevel(int)

    bool debug; // public static boolean loci.formats.FormatHandler.debug
    int debugLevel; // public static int loci.formats.FormatHandler.debugLevel
};
}
}
#endif
