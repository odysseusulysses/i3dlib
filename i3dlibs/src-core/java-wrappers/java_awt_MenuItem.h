#ifndef java_awt_MenuItem_H
#define java_awt_MenuItem_H
#include <jni.h>
#include <java_marker.h>
#include <java_awt_MenuComponent.h>

namespace javax {
namespace accessibility {
class Accessible;
}
}

namespace java {
namespace lang {
class String;
}
}

namespace java {
namespace awt {
class MenuShortcut;
}
}

namespace javax {
namespace accessibility {
class AccessibleContext;
}
}

namespace java {
namespace util {
class EventListener;
}
}

namespace java {
namespace lang {
class Class;
}
}

namespace java {
namespace awt {
namespace event {
class ActionListener;
}
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
class MenuItem : public java::awt::MenuComponent {
  public:
    MenuItem(JavaMarker* dummy);
    MenuItem(jobject obj);
    MenuItem(const char* arg1); // public java.awt.MenuItem(java.lang.String) throws java.awt.HeadlessException
    MenuItem(const char* arg1, java::awt::MenuShortcut* arg2); // public java.awt.MenuItem(java.lang.String,java.awt.MenuShortcut) throws java.awt.HeadlessException
    MenuItem(); // public java.awt.MenuItem() throws java.awt.HeadlessException

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual void disable(); // public synchronized void java.awt.MenuItem.disable()
    virtual void enable(); // public synchronized void java.awt.MenuItem.enable()
    virtual void enable(bool arg1); // public void java.awt.MenuItem.enable(boolean)
    virtual void addNotify(); // public void java.awt.MenuItem.addNotify()
    virtual javax::accessibility::AccessibleContext* getAccessibleContext(); // public javax.accessibility.AccessibleContext java.awt.MenuItem.getAccessibleContext()
    virtual JavaObjectArray* getListeners(java::lang::Class* arg1); // public java.util.EventListener[] java.awt.MenuItem.getListeners(java.lang.Class)
    virtual bool isEnabled(); // public boolean java.awt.MenuItem.isEnabled()
    virtual const char* paramString(); // public java.lang.String java.awt.MenuItem.paramString()
    virtual void setEnabled(bool arg1); // public synchronized void java.awt.MenuItem.setEnabled(boolean)
    virtual const char* getActionCommand(); // public java.lang.String java.awt.MenuItem.getActionCommand()
    virtual const char* getLabel(); // public java.lang.String java.awt.MenuItem.getLabel()
    virtual void deleteShortcut(); // public void java.awt.MenuItem.deleteShortcut()
    virtual java::awt::MenuShortcut* getShortcut(); // public java.awt.MenuShortcut java.awt.MenuItem.getShortcut()
    virtual void addActionListener(java::awt::event::ActionListener* arg1); // public synchronized void java.awt.MenuItem.addActionListener(java.awt.event.ActionListener)
    virtual JavaObjectArray* getActionListeners(); // public synchronized java.awt.event.ActionListener[] java.awt.MenuItem.getActionListeners()
    virtual void removeActionListener(java::awt::event::ActionListener* arg1); // public synchronized void java.awt.MenuItem.removeActionListener(java.awt.event.ActionListener)
    virtual void setActionCommand(const char* arg1); // public void java.awt.MenuItem.setActionCommand(java.lang.String)
    virtual void setLabel(const char* arg1); // public synchronized void java.awt.MenuItem.setLabel(java.lang.String)
    virtual void setShortcut(java::awt::MenuShortcut* arg1); // public void java.awt.MenuItem.setShortcut(java.awt.MenuShortcut)

};
}
}
#endif
