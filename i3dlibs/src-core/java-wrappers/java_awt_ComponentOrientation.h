#ifndef java_awt_ComponentOrientation_H
#define java_awt_ComponentOrientation_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace io {
class Serializable;
}
}

namespace java {
namespace util {
class Locale;
}
}

namespace java {
namespace util {
class ResourceBundle;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
class ComponentOrientation : public java::lang::Object {
  public:
    ComponentOrientation(JavaMarker* dummy);
    ComponentOrientation(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual java::awt::ComponentOrientation* getOrientation(java::util::Locale* arg1); // public static java.awt.ComponentOrientation java.awt.ComponentOrientation.getOrientation(java.util.Locale)
    virtual java::awt::ComponentOrientation* getOrientation(java::util::ResourceBundle* arg1); // public static java.awt.ComponentOrientation java.awt.ComponentOrientation.getOrientation(java.util.ResourceBundle)
    virtual bool isHorizontal(); // public boolean java.awt.ComponentOrientation.isHorizontal()
    virtual bool isLeftToRight(); // public boolean java.awt.ComponentOrientation.isLeftToRight()

    java::awt::ComponentOrientation* LEFT_TO_RIGHT; // public static final java.awt.ComponentOrientation java.awt.ComponentOrientation.LEFT_TO_RIGHT
    java::awt::ComponentOrientation* RIGHT_TO_LEFT; // public static final java.awt.ComponentOrientation java.awt.ComponentOrientation.RIGHT_TO_LEFT
    java::awt::ComponentOrientation* UNKNOWN; // public static final java.awt.ComponentOrientation java.awt.ComponentOrientation.UNKNOWN
};
}
}
#endif
