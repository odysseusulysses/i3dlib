#ifndef java_awt_MenuShortcut_H
#define java_awt_MenuShortcut_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace io {
class Serializable;
}
}

namespace java {
namespace lang {
class String;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
class MenuShortcut : public java::lang::Object {
  public:
    MenuShortcut(JavaMarker* dummy);
    MenuShortcut(jobject obj);
    MenuShortcut(int arg1, bool arg2); // public java.awt.MenuShortcut(int,boolean)
    MenuShortcut(int arg1); // public java.awt.MenuShortcut(int)

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual int hashCode(); // public int java.awt.MenuShortcut.hashCode()
    virtual bool equals(java::lang::Object* arg1); // public boolean java.awt.MenuShortcut.equals(java.lang.Object)
    virtual bool equals(java::awt::MenuShortcut* arg1); // public boolean java.awt.MenuShortcut.equals(java.awt.MenuShortcut)
    virtual const char* toString(); // public java.lang.String java.awt.MenuShortcut.toString()
    virtual int getKey(); // public int java.awt.MenuShortcut.getKey()
    virtual bool usesShiftModifier(); // public boolean java.awt.MenuShortcut.usesShiftModifier()

};
}
}
#endif
