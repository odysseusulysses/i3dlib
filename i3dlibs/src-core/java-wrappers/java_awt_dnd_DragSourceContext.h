#ifndef java_awt_dnd_DragSourceContext_H
#define java_awt_dnd_DragSourceContext_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace awt {
namespace dnd {
class DragSourceListener;
}
}
}

namespace java {
namespace awt {
namespace dnd {
class DragSourceMotionListener;
}
}
}

namespace java {
namespace io {
class Serializable;
}
}

namespace java {
namespace awt {
namespace dnd {
namespace peer {
class DragSourceContextPeer;
}
}
}
}

namespace java {
namespace awt {
namespace dnd {
class DragGestureEvent;
}
}
}

namespace java {
namespace awt {
class Cursor;
}
}

namespace java {
namespace awt {
class Image;
}
}

namespace java {
namespace awt {
class Point;
}
}

namespace java {
namespace awt {
namespace datatransfer {
class Transferable;
}
}
}

namespace java {
namespace awt {
class Component;
}
}

namespace java {
namespace awt {
namespace dnd {
class DragSourceDragEvent;
}
}
}

namespace java {
namespace awt {
namespace dnd {
class DragSourceEvent;
}
}
}

namespace java {
namespace awt {
namespace dnd {
class DragSourceDropEvent;
}
}
}

namespace java {
namespace awt {
namespace dnd {
class DragSource;
}
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace dnd {
class DragSourceContext : public java::lang::Object {
  public:
    DragSourceContext(JavaMarker* dummy);
    DragSourceContext(jobject obj);
    DragSourceContext(java::awt::dnd::peer::DragSourceContextPeer* arg1, java::awt::dnd::DragGestureEvent* arg2, java::awt::Cursor* arg3, java::awt::Image* arg4, java::awt::Point* arg5, java::awt::datatransfer::Transferable* arg6, java::awt::dnd::DragSourceListener* arg7); // public java.awt.dnd.DragSourceContext(java.awt.dnd.peer.DragSourceContextPeer,java.awt.dnd.DragGestureEvent,java.awt.Cursor,java.awt.Image,java.awt.Point,java.awt.datatransfer.Transferable,java.awt.dnd.DragSourceListener)

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual void setCursor(java::awt::Cursor* arg1); // public synchronized void java.awt.dnd.DragSourceContext.setCursor(java.awt.Cursor)
    virtual java::awt::Component* getComponent(); // public java.awt.Component java.awt.dnd.DragSourceContext.getComponent()
    virtual java::awt::Cursor* getCursor(); // public java.awt.Cursor java.awt.dnd.DragSourceContext.getCursor()
    virtual void dragEnter(java::awt::dnd::DragSourceDragEvent* arg1); // public void java.awt.dnd.DragSourceContext.dragEnter(java.awt.dnd.DragSourceDragEvent)
    virtual void dragExit(java::awt::dnd::DragSourceEvent* arg1); // public void java.awt.dnd.DragSourceContext.dragExit(java.awt.dnd.DragSourceEvent)
    virtual void dragOver(java::awt::dnd::DragSourceDragEvent* arg1); // public void java.awt.dnd.DragSourceContext.dragOver(java.awt.dnd.DragSourceDragEvent)
    virtual void dropActionChanged(java::awt::dnd::DragSourceDragEvent* arg1); // public void java.awt.dnd.DragSourceContext.dropActionChanged(java.awt.dnd.DragSourceDragEvent)
    virtual void addDragSourceListener(java::awt::dnd::DragSourceListener* arg1); // public synchronized void java.awt.dnd.DragSourceContext.addDragSourceListener(java.awt.dnd.DragSourceListener) throws java.util.TooManyListenersException
    virtual void dragDropEnd(java::awt::dnd::DragSourceDropEvent* arg1); // public void java.awt.dnd.DragSourceContext.dragDropEnd(java.awt.dnd.DragSourceDropEvent)
    virtual void dragMouseMoved(java::awt::dnd::DragSourceDragEvent* arg1); // public void java.awt.dnd.DragSourceContext.dragMouseMoved(java.awt.dnd.DragSourceDragEvent)
    virtual void removeDragSourceListener(java::awt::dnd::DragSourceListener* arg1); // public synchronized void java.awt.dnd.DragSourceContext.removeDragSourceListener(java.awt.dnd.DragSourceListener)
    virtual java::awt::dnd::DragSource* getDragSource(); // public java.awt.dnd.DragSource java.awt.dnd.DragSourceContext.getDragSource()
    virtual int getSourceActions(); // public int java.awt.dnd.DragSourceContext.getSourceActions()
    virtual void transferablesFlavorsChanged(); // public void java.awt.dnd.DragSourceContext.transferablesFlavorsChanged()
    virtual java::awt::datatransfer::Transferable* getTransferable(); // public java.awt.datatransfer.Transferable java.awt.dnd.DragSourceContext.getTransferable()
    virtual java::awt::dnd::DragGestureEvent* getTrigger(); // public java.awt.dnd.DragGestureEvent java.awt.dnd.DragSourceContext.getTrigger()

};
}
}
}
#endif
