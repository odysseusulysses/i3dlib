#ifndef java_awt_dnd_DragSource_H
#define java_awt_dnd_DragSource_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace io {
class Serializable;
}
}

namespace java {
namespace util {
class EventListener;
}
}

namespace java {
namespace lang {
class Class;
}
}

namespace java {
namespace awt {
namespace dnd {
class DragGestureRecognizer;
}
}
}

namespace java {
namespace awt {
class Component;
}
}

namespace java {
namespace awt {
namespace dnd {
class DragGestureListener;
}
}
}

namespace java {
namespace awt {
namespace datatransfer {
class FlavorMap;
}
}
}

namespace java {
namespace awt {
namespace dnd {
class DragSourceListener;
}
}
}

namespace java {
namespace awt {
namespace dnd {
class DragSourceMotionListener;
}
}
}

namespace java {
namespace awt {
namespace dnd {
class DragGestureEvent;
}
}
}

namespace java {
namespace awt {
class Cursor;
}
}

namespace java {
namespace awt {
namespace datatransfer {
class Transferable;
}
}
}

namespace java {
namespace awt {
class Image;
}
}

namespace java {
namespace awt {
class Point;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace dnd {
class DragSource : public java::lang::Object {
  public:
    DragSource(JavaMarker* dummy);
    DragSource(jobject obj);
    DragSource(); // public java.awt.dnd.DragSource() throws java.awt.HeadlessException

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual JavaObjectArray* getListeners(java::lang::Class* arg1); // public java.util.EventListener[] java.awt.dnd.DragSource.getListeners(java.lang.Class)
    virtual java::awt::dnd::DragGestureRecognizer* createDragGestureRecognizer(java::lang::Class* arg1, java::awt::Component* arg2, int arg3, java::awt::dnd::DragGestureListener* arg4); // public java.awt.dnd.DragGestureRecognizer java.awt.dnd.DragSource.createDragGestureRecognizer(java.lang.Class,java.awt.Component,int,java.awt.dnd.DragGestureListener)
    virtual java::awt::datatransfer::FlavorMap* getFlavorMap(); // public java.awt.datatransfer.FlavorMap java.awt.dnd.DragSource.getFlavorMap()
    virtual void addDragSourceListener(java::awt::dnd::DragSourceListener* arg1); // public void java.awt.dnd.DragSource.addDragSourceListener(java.awt.dnd.DragSourceListener)
    virtual void addDragSourceMotionListener(java::awt::dnd::DragSourceMotionListener* arg1); // public void java.awt.dnd.DragSource.addDragSourceMotionListener(java.awt.dnd.DragSourceMotionListener)
    virtual java::awt::dnd::DragGestureRecognizer* createDefaultDragGestureRecognizer(java::awt::Component* arg1, int arg2, java::awt::dnd::DragGestureListener* arg3); // public java.awt.dnd.DragGestureRecognizer java.awt.dnd.DragSource.createDefaultDragGestureRecognizer(java.awt.Component,int,java.awt.dnd.DragGestureListener)
    virtual java::awt::dnd::DragSource* getDefaultDragSource(); // public static java.awt.dnd.DragSource java.awt.dnd.DragSource.getDefaultDragSource()
    virtual JavaObjectArray* getDragSourceListeners(); // public java.awt.dnd.DragSourceListener[] java.awt.dnd.DragSource.getDragSourceListeners()
    virtual JavaObjectArray* getDragSourceMotionListeners(); // public java.awt.dnd.DragSourceMotionListener[] java.awt.dnd.DragSource.getDragSourceMotionListeners()
    virtual int getDragThreshold(); // public static int java.awt.dnd.DragSource.getDragThreshold()
    virtual bool isDragImageSupported(); // public static boolean java.awt.dnd.DragSource.isDragImageSupported()
    virtual void removeDragSourceListener(java::awt::dnd::DragSourceListener* arg1); // public void java.awt.dnd.DragSource.removeDragSourceListener(java.awt.dnd.DragSourceListener)
    virtual void removeDragSourceMotionListener(java::awt::dnd::DragSourceMotionListener* arg1); // public void java.awt.dnd.DragSource.removeDragSourceMotionListener(java.awt.dnd.DragSourceMotionListener)
    virtual void startDrag(java::awt::dnd::DragGestureEvent* arg1, java::awt::Cursor* arg2, java::awt::datatransfer::Transferable* arg3, java::awt::dnd::DragSourceListener* arg4, java::awt::datatransfer::FlavorMap* arg5); // public void java.awt.dnd.DragSource.startDrag(java.awt.dnd.DragGestureEvent,java.awt.Cursor,java.awt.datatransfer.Transferable,java.awt.dnd.DragSourceListener,java.awt.datatransfer.FlavorMap) throws java.awt.dnd.InvalidDnDOperationException
    virtual void startDrag(java::awt::dnd::DragGestureEvent* arg1, java::awt::Cursor* arg2, java::awt::Image* arg3, java::awt::Point* arg4, java::awt::datatransfer::Transferable* arg5, java::awt::dnd::DragSourceListener* arg6, java::awt::datatransfer::FlavorMap* arg7); // public void java.awt.dnd.DragSource.startDrag(java.awt.dnd.DragGestureEvent,java.awt.Cursor,java.awt.Image,java.awt.Point,java.awt.datatransfer.Transferable,java.awt.dnd.DragSourceListener,java.awt.datatransfer.FlavorMap) throws java.awt.dnd.InvalidDnDOperationException
    virtual void startDrag(java::awt::dnd::DragGestureEvent* arg1, java::awt::Cursor* arg2, java::awt::Image* arg3, java::awt::Point* arg4, java::awt::datatransfer::Transferable* arg5, java::awt::dnd::DragSourceListener* arg6); // public void java.awt.dnd.DragSource.startDrag(java.awt.dnd.DragGestureEvent,java.awt.Cursor,java.awt.Image,java.awt.Point,java.awt.datatransfer.Transferable,java.awt.dnd.DragSourceListener) throws java.awt.dnd.InvalidDnDOperationException
    virtual void startDrag(java::awt::dnd::DragGestureEvent* arg1, java::awt::Cursor* arg2, java::awt::datatransfer::Transferable* arg3, java::awt::dnd::DragSourceListener* arg4); // public void java.awt.dnd.DragSource.startDrag(java.awt.dnd.DragGestureEvent,java.awt.Cursor,java.awt.datatransfer.Transferable,java.awt.dnd.DragSourceListener) throws java.awt.dnd.InvalidDnDOperationException

    java::awt::Cursor* DefaultCopyDrop; // public static final java.awt.Cursor java.awt.dnd.DragSource.DefaultCopyDrop
    java::awt::Cursor* DefaultMoveDrop; // public static final java.awt.Cursor java.awt.dnd.DragSource.DefaultMoveDrop
    java::awt::Cursor* DefaultLinkDrop; // public static final java.awt.Cursor java.awt.dnd.DragSource.DefaultLinkDrop
    java::awt::Cursor* DefaultCopyNoDrop; // public static final java.awt.Cursor java.awt.dnd.DragSource.DefaultCopyNoDrop
    java::awt::Cursor* DefaultMoveNoDrop; // public static final java.awt.Cursor java.awt.dnd.DragSource.DefaultMoveNoDrop
    java::awt::Cursor* DefaultLinkNoDrop; // public static final java.awt.Cursor java.awt.dnd.DragSource.DefaultLinkNoDrop
};
}
}
}
#endif
