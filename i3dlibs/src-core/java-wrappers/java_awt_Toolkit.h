#ifndef java_awt_Toolkit_H
#define java_awt_Toolkit_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace lang {
class String;
}
}

namespace java {
namespace awt {
namespace image {
class ColorModel;
}
}
}

namespace java {
namespace awt {
class Image;
}
}

namespace java {
namespace net {
class URL;
}
}

namespace java {
namespace awt {
namespace image {
class ImageProducer;
}
}
}

namespace java {
namespace awt {
class FontMetrics;
}
}

namespace java {
namespace awt {
class Font;
}
}

namespace java {
namespace util {
class Map;
}
}

namespace java {
namespace awt {
namespace im {
class InputMethodHighlight;
}
}
}

namespace java {
namespace beans {
class PropertyChangeListener;
}
}

namespace java {
namespace awt {
class Insets;
}
}

namespace java {
namespace awt {
class GraphicsConfiguration;
}
}

namespace java {
namespace awt {
class Dialog__ModalExclusionType;
}
}

namespace java {
namespace awt {
namespace image {
class ImageObserver;
}
}
}

namespace java {
namespace awt {
namespace event {
class AWTEventListener;
}
}
}

namespace java {
namespace awt {
class Cursor;
}
}

namespace java {
namespace awt {
class Point;
}
}

namespace java {
namespace awt {
namespace dnd {
class DragGestureRecognizer;
}
}
}

namespace java {
namespace lang {
class Class;
}
}

namespace java {
namespace awt {
namespace dnd {
class DragSource;
}
}
}

namespace java {
namespace awt {
class Component;
}
}

namespace java {
namespace awt {
namespace dnd {
class DragGestureListener;
}
}
}

namespace java {
namespace awt {
namespace dnd {
namespace peer {
class DragSourceContextPeer;
}
}
}
}

namespace java {
namespace awt {
namespace dnd {
class DragGestureEvent;
}
}
}

namespace java {
namespace awt {
class Dimension;
}
}

namespace java {
namespace awt {
class PrintJob;
}
}

namespace java {
namespace awt {
class Frame;
}
}

namespace java {
namespace util {
class Properties;
}
}

namespace java {
namespace awt {
class JobAttributes;
}
}

namespace java {
namespace awt {
class PageAttributes;
}
}

namespace java {
namespace awt {
namespace datatransfer {
class Clipboard;
}
}
}

namespace java {
namespace awt {
class EventQueue;
}
}

namespace java {
namespace awt {
class Dialog__ModalityType;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
class Toolkit : public java::lang::Object {
  public:
    Toolkit(JavaMarker* dummy);
    Toolkit(jobject obj);
    Toolkit(); // public java.awt.Toolkit()

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual const char* getProperty(const char* arg1, const char* arg2); // public static java.lang.String java.awt.Toolkit.getProperty(java.lang.String,java.lang.String)
    virtual void sync(); // public abstract void java.awt.Toolkit.sync()
    virtual java::awt::image::ColorModel* getColorModel(); // public abstract java.awt.image.ColorModel java.awt.Toolkit.getColorModel() throws java.awt.HeadlessException
    virtual java::awt::Image* createImage(java::net::URL* arg1); // public abstract java.awt.Image java.awt.Toolkit.createImage(java.net.URL)
    virtual java::awt::Image* createImage(const char* arg1); // public abstract java.awt.Image java.awt.Toolkit.createImage(java.lang.String)
    virtual java::awt::Image* createImage(java::awt::image::ImageProducer* arg1); // public abstract java.awt.Image java.awt.Toolkit.createImage(java.awt.image.ImageProducer)
    virtual java::awt::Image* createImage(JavaByteArray* arg1, int arg2, int arg3); // public abstract java.awt.Image java.awt.Toolkit.createImage(byte[],int,int)
    virtual java::awt::Image* createImage(JavaByteArray* arg1); // public java.awt.Image java.awt.Toolkit.createImage(byte[])
    virtual java::awt::Toolkit* getDefaultToolkit(); // public static synchronized java.awt.Toolkit java.awt.Toolkit.getDefaultToolkit()
    virtual java::awt::FontMetrics* getFontMetrics(java::awt::Font* arg1); // public abstract java.awt.FontMetrics java.awt.Toolkit.getFontMetrics(java.awt.Font)
    virtual java::util::Map* mapInputMethodHighlight(java::awt::im::InputMethodHighlight* arg1); // public abstract java.util.Map java.awt.Toolkit.mapInputMethodHighlight(java.awt.im.InputMethodHighlight) throws java.awt.HeadlessException
    virtual void addPropertyChangeListener(const char* arg1, java::beans::PropertyChangeListener* arg2); // public synchronized void java.awt.Toolkit.addPropertyChangeListener(java.lang.String,java.beans.PropertyChangeListener)
    virtual java::awt::Insets* getScreenInsets(java::awt::GraphicsConfiguration* arg1); // public java.awt.Insets java.awt.Toolkit.getScreenInsets(java.awt.GraphicsConfiguration) throws java.awt.HeadlessException
    virtual bool isAlwaysOnTopSupported(); // public boolean java.awt.Toolkit.isAlwaysOnTopSupported()
    virtual bool isModalExclusionTypeSupported(java::awt::Dialog__ModalExclusionType* arg1); // public abstract boolean java.awt.Toolkit.isModalExclusionTypeSupported(java.awt.Dialog$ModalExclusionType)
    virtual int checkImage(java::awt::Image* arg1, int arg2, int arg3, java::awt::image::ImageObserver* arg4); // public abstract int java.awt.Toolkit.checkImage(java.awt.Image,int,int,java.awt.image.ImageObserver)
    virtual JavaObjectArray* getPropertyChangeListeners(const char* arg1); // public synchronized java.beans.PropertyChangeListener[] java.awt.Toolkit.getPropertyChangeListeners(java.lang.String)
    virtual JavaObjectArray* getPropertyChangeListeners(); // public java.beans.PropertyChangeListener[] java.awt.Toolkit.getPropertyChangeListeners()
    virtual bool prepareImage(java::awt::Image* arg1, int arg2, int arg3, java::awt::image::ImageObserver* arg4); // public abstract boolean java.awt.Toolkit.prepareImage(java.awt.Image,int,int,java.awt.image.ImageObserver)
    virtual void removePropertyChangeListener(const char* arg1, java::beans::PropertyChangeListener* arg2); // public synchronized void java.awt.Toolkit.removePropertyChangeListener(java.lang.String,java.beans.PropertyChangeListener)
    virtual bool isFrameStateSupported(int arg1); // public boolean java.awt.Toolkit.isFrameStateSupported(int) throws java.awt.HeadlessException
    virtual void addAWTEventListener(java::awt::event::AWTEventListener* arg1, long arg2); // public void java.awt.Toolkit.addAWTEventListener(java.awt.event.AWTEventListener,long)
    virtual void beep(); // public abstract void java.awt.Toolkit.beep()
    virtual java::awt::Cursor* createCustomCursor(java::awt::Image* arg1, java::awt::Point* arg2, const char* arg3); // public java.awt.Cursor java.awt.Toolkit.createCustomCursor(java.awt.Image,java.awt.Point,java.lang.String) throws java.lang.IndexOutOfBoundsException,java.awt.HeadlessException
    virtual java::awt::dnd::DragGestureRecognizer* createDragGestureRecognizer(java::lang::Class* arg1, java::awt::dnd::DragSource* arg2, java::awt::Component* arg3, int arg4, java::awt::dnd::DragGestureListener* arg5); // public java.awt.dnd.DragGestureRecognizer java.awt.Toolkit.createDragGestureRecognizer(java.lang.Class,java.awt.dnd.DragSource,java.awt.Component,int,java.awt.dnd.DragGestureListener)
    virtual java::awt::dnd::peer::DragSourceContextPeer* createDragSourceContextPeer(java::awt::dnd::DragGestureEvent* arg1); // public abstract java.awt.dnd.peer.DragSourceContextPeer java.awt.Toolkit.createDragSourceContextPeer(java.awt.dnd.DragGestureEvent) throws java.awt.dnd.InvalidDnDOperationException
    virtual JavaObjectArray* getAWTEventListeners(long arg1); // public java.awt.event.AWTEventListener[] java.awt.Toolkit.getAWTEventListeners(long)
    virtual JavaObjectArray* getAWTEventListeners(); // public java.awt.event.AWTEventListener[] java.awt.Toolkit.getAWTEventListeners()
    virtual java::awt::Dimension* getBestCursorSize(int arg1, int arg2); // public java.awt.Dimension java.awt.Toolkit.getBestCursorSize(int,int) throws java.awt.HeadlessException
    virtual java::lang::Object* getDesktopProperty(const char* arg1); // public final synchronized java.lang.Object java.awt.Toolkit.getDesktopProperty(java.lang.String)
    virtual JavaObjectArray* getFontList(); // public abstract java.lang.String[] java.awt.Toolkit.getFontList()
    virtual java::awt::Image* getImage(const char* arg1); // public abstract java.awt.Image java.awt.Toolkit.getImage(java.lang.String)
    virtual java::awt::Image* getImage(java::net::URL* arg1); // public abstract java.awt.Image java.awt.Toolkit.getImage(java.net.URL)
    virtual bool getLockingKeyState(int arg1); // public boolean java.awt.Toolkit.getLockingKeyState(int) throws java.lang.UnsupportedOperationException
    virtual int getMaximumCursorColors(); // public int java.awt.Toolkit.getMaximumCursorColors() throws java.awt.HeadlessException
    virtual int getMenuShortcutKeyMask(); // public int java.awt.Toolkit.getMenuShortcutKeyMask() throws java.awt.HeadlessException
    virtual java::awt::PrintJob* getPrintJob(java::awt::Frame* arg1, const char* arg2, java::util::Properties* arg3); // public abstract java.awt.PrintJob java.awt.Toolkit.getPrintJob(java.awt.Frame,java.lang.String,java.util.Properties)
    virtual java::awt::PrintJob* getPrintJob(java::awt::Frame* arg1, const char* arg2, java::awt::JobAttributes* arg3, java::awt::PageAttributes* arg4); // public java.awt.PrintJob java.awt.Toolkit.getPrintJob(java.awt.Frame,java.lang.String,java.awt.JobAttributes,java.awt.PageAttributes)
    virtual int getScreenResolution(); // public abstract int java.awt.Toolkit.getScreenResolution() throws java.awt.HeadlessException
    virtual java::awt::Dimension* getScreenSize(); // public abstract java.awt.Dimension java.awt.Toolkit.getScreenSize() throws java.awt.HeadlessException
    virtual java::awt::datatransfer::Clipboard* getSystemClipboard(); // public abstract java.awt.datatransfer.Clipboard java.awt.Toolkit.getSystemClipboard() throws java.awt.HeadlessException
    virtual java::awt::EventQueue* getSystemEventQueue(); // public final java.awt.EventQueue java.awt.Toolkit.getSystemEventQueue()
    virtual java::awt::datatransfer::Clipboard* getSystemSelection(); // public java.awt.datatransfer.Clipboard java.awt.Toolkit.getSystemSelection() throws java.awt.HeadlessException
    virtual bool isDynamicLayoutActive(); // public boolean java.awt.Toolkit.isDynamicLayoutActive() throws java.awt.HeadlessException
    virtual bool isModalityTypeSupported(java::awt::Dialog__ModalityType* arg1); // public abstract boolean java.awt.Toolkit.isModalityTypeSupported(java.awt.Dialog$ModalityType)
    virtual void removeAWTEventListener(java::awt::event::AWTEventListener* arg1); // public void java.awt.Toolkit.removeAWTEventListener(java.awt.event.AWTEventListener)
    virtual void setDynamicLayout(bool arg1); // public void java.awt.Toolkit.setDynamicLayout(boolean) throws java.awt.HeadlessException
    virtual void setLockingKeyState(int arg1, bool arg2); // public void java.awt.Toolkit.setLockingKeyState(int,boolean) throws java.lang.UnsupportedOperationException

};
}
}
#endif
