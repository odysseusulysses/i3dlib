#ifndef javax_accessibility_AccessibleAction_H
#define javax_accessibility_AccessibleAction_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace lang {
class String;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace javax {
namespace accessibility {
class AccessibleAction : public java::lang::Object {
  public:
    AccessibleAction(JavaMarker* dummy);
    AccessibleAction(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual bool doAccessibleAction(int arg1); // public abstract boolean javax.accessibility.AccessibleAction.doAccessibleAction(int)
    virtual int getAccessibleActionCount(); // public abstract int javax.accessibility.AccessibleAction.getAccessibleActionCount()
    virtual const char* getAccessibleActionDescription(int arg1); // public abstract java.lang.String javax.accessibility.AccessibleAction.getAccessibleActionDescription(int)

    const char* TOGGLE_EXPAND; // public static final java.lang.String javax.accessibility.AccessibleAction.TOGGLE_EXPAND
    const char* INCREMENT; // public static final java.lang.String javax.accessibility.AccessibleAction.INCREMENT
    const char* DECREMENT; // public static final java.lang.String javax.accessibility.AccessibleAction.DECREMENT
    const char* CLICK; // public static final java.lang.String javax.accessibility.AccessibleAction.CLICK
    const char* TOGGLE_POPUP; // public static final java.lang.String javax.accessibility.AccessibleAction.TOGGLE_POPUP
};
}
}
#endif
