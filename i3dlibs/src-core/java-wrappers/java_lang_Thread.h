#ifndef java_lang_Thread_H
#define java_lang_Thread_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace lang {
class Runnable;
}
}

namespace java {
namespace lang {
class ThreadGroup;
}
}

namespace java {
namespace lang {
class String;
}
}

namespace java {
namespace lang {
class StackTraceElement;
}
}

namespace java {
namespace util {
class Map;
}
}

namespace java {
namespace lang {
class ClassLoader;
}
}

namespace java {
namespace lang {
class Thread__UncaughtExceptionHandler;
}
}

namespace java {
namespace lang {
class Thread__State;
}
}

namespace java {
namespace lang {
class Throwable;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace lang {
class Thread : public java::lang::Object {
  public:
    Thread(JavaMarker* dummy);
    Thread(jobject obj);
    Thread(java::lang::ThreadGroup* arg1, const char* arg2); // public java.lang.Thread(java.lang.ThreadGroup,java.lang.String)
    Thread(java::lang::Runnable* arg1, const char* arg2); // public java.lang.Thread(java.lang.Runnable,java.lang.String)
    Thread(java::lang::ThreadGroup* arg1, java::lang::Runnable* arg2, const char* arg3, long arg4); // public java.lang.Thread(java.lang.ThreadGroup,java.lang.Runnable,java.lang.String,long)
    Thread(const char* arg1); // public java.lang.Thread(java.lang.String)
    Thread(java::lang::ThreadGroup* arg1, java::lang::Runnable* arg2); // public java.lang.Thread(java.lang.ThreadGroup,java.lang.Runnable)
    Thread(java::lang::ThreadGroup* arg1, java::lang::Runnable* arg2, const char* arg3); // public java.lang.Thread(java.lang.ThreadGroup,java.lang.Runnable,java.lang.String)
    Thread(); // public java.lang.Thread()
    Thread(java::lang::Runnable* arg1); // public java.lang.Thread(java.lang.Runnable)

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual java::lang::Thread* currentThread(); // public static native java.lang.Thread java.lang.Thread.currentThread()
    virtual bool isInterrupted(); // public boolean java.lang.Thread.isInterrupted()
    virtual void run(); // public void java.lang.Thread.run()
    virtual const char* toString(); // public java.lang.String java.lang.Thread.toString()
    virtual const char* getName(); // public final java.lang.String java.lang.Thread.getName()
    virtual java::lang::ThreadGroup* getThreadGroup(); // public final java.lang.ThreadGroup java.lang.Thread.getThreadGroup()
    virtual JavaObjectArray* getStackTrace(); // public java.lang.StackTraceElement[] java.lang.Thread.getStackTrace()
    virtual void dumpStack(); // public static void java.lang.Thread.dumpStack()
    virtual void setDaemon(bool arg1); // public final void java.lang.Thread.setDaemon(boolean)
    virtual void setPriority(int arg1); // public final void java.lang.Thread.setPriority(int)
    virtual void start(); // public synchronized void java.lang.Thread.start()
    virtual int activeCount(); // public static int java.lang.Thread.activeCount()
    virtual void checkAccess(); // public final void java.lang.Thread.checkAccess()
    virtual int countStackFrames(); // public native int java.lang.Thread.countStackFrames()
    virtual void destroy(); // public void java.lang.Thread.destroy()
    virtual int enumerate(JavaObjectArray* arg1); // public static int java.lang.Thread.enumerate(java.lang.Thread[])
    virtual java::util::Map* getAllStackTraces(); // public static java.util.Map java.lang.Thread.getAllStackTraces()
    virtual java::lang::ClassLoader* getContextClassLoader(); // public java.lang.ClassLoader java.lang.Thread.getContextClassLoader()
    virtual java::lang::Thread__UncaughtExceptionHandler* getDefaultUncaughtExceptionHandler(); // public static java.lang.Thread$UncaughtExceptionHandler java.lang.Thread.getDefaultUncaughtExceptionHandler()
    virtual long getId(); // public long java.lang.Thread.getId()
    virtual int getPriority(); // public final int java.lang.Thread.getPriority()
    virtual java::lang::Thread__State* getState(); // public java.lang.Thread$State java.lang.Thread.getState()
    virtual java::lang::Thread__UncaughtExceptionHandler* getUncaughtExceptionHandler(); // public java.lang.Thread$UncaughtExceptionHandler java.lang.Thread.getUncaughtExceptionHandler()
    virtual bool holdsLock(java::lang::Object* arg1); // public static native boolean java.lang.Thread.holdsLock(java.lang.Object)
    virtual void interrupt(); // public void java.lang.Thread.interrupt()
    virtual bool interrupted(); // public static boolean java.lang.Thread.interrupted()
    virtual bool isAlive(); // public final native boolean java.lang.Thread.isAlive()
    virtual bool isDaemon(); // public final boolean java.lang.Thread.isDaemon()
    virtual void join(long arg1, int arg2); // public final synchronized void java.lang.Thread.join(long,int) throws java.lang.InterruptedException
    virtual void join(); // public final void java.lang.Thread.join() throws java.lang.InterruptedException
    virtual void join(long arg1); // public final synchronized void java.lang.Thread.join(long) throws java.lang.InterruptedException
    virtual void resume(); // public final void java.lang.Thread.resume()
    virtual void setContextClassLoader(java::lang::ClassLoader* arg1); // public void java.lang.Thread.setContextClassLoader(java.lang.ClassLoader)
    virtual void setDefaultUncaughtExceptionHandler(java::lang::Thread__UncaughtExceptionHandler* arg1); // public static void java.lang.Thread.setDefaultUncaughtExceptionHandler(java.lang.Thread$UncaughtExceptionHandler)
    virtual void setName(const char* arg1); // public final void java.lang.Thread.setName(java.lang.String)
    virtual void setUncaughtExceptionHandler(java::lang::Thread__UncaughtExceptionHandler* arg1); // public void java.lang.Thread.setUncaughtExceptionHandler(java.lang.Thread$UncaughtExceptionHandler)
    virtual void sleep(long arg1, int arg2); // public static void java.lang.Thread.sleep(long,int) throws java.lang.InterruptedException
    virtual void sleep(long arg1); // public static native void java.lang.Thread.sleep(long) throws java.lang.InterruptedException
    virtual void stop(); // public final void java.lang.Thread.stop()
    virtual void stop(java::lang::Throwable* arg1); // public final synchronized void java.lang.Thread.stop(java.lang.Throwable)
    virtual void suspend(); // public final void java.lang.Thread.suspend()
    virtual void yield(); // public static native void java.lang.Thread.yield()

    int MIN_PRIORITY; // public static final int java.lang.Thread.MIN_PRIORITY
    int NORM_PRIORITY; // public static final int java.lang.Thread.NORM_PRIORITY
    int MAX_PRIORITY; // public static final int java.lang.Thread.MAX_PRIORITY
};
}
}
#endif
