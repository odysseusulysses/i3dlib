#ifndef loci_formats_in_FV1000Reader_H
#define loci_formats_in_FV1000Reader_H
#include <jni.h>
#include <java_marker.h>
#include <loci_formats_FormatReader.h>

namespace java {
namespace lang {
class String;
}
}

namespace java {
namespace awt {
namespace image {
class BufferedImage;
}
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace loci {
namespace formats {
namespace in {
class FV1000Reader : public loci::formats::FormatReader {
  public:
    FV1000Reader(JavaMarker* dummy);
    FV1000Reader(jobject obj);
    FV1000Reader(); // public loci.formats.in.FV1000Reader()

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual void close(); // public void loci.formats.in.FV1000Reader.close() throws java.io.IOException
    virtual void close(bool arg1); // public void loci.formats.in.FV1000Reader.close(boolean) throws java.io.IOException
    virtual bool isThisType(JavaByteArray* arg1); // public boolean loci.formats.in.FV1000Reader.isThisType(byte[])
    virtual int fileGroupOption(const char* arg1); // public int loci.formats.in.FV1000Reader.fileGroupOption(java.lang.String) throws loci.formats.FormatException,java.io.IOException
    virtual JavaByteArray* openBytes(int arg1, JavaByteArray* arg2, int arg3, int arg4, int arg5, int arg6); // public byte[] loci.formats.in.FV1000Reader.openBytes(int,byte[],int,int,int,int) throws loci.formats.FormatException,java.io.IOException
    virtual java::awt::image::BufferedImage* openThumbImage(int arg1); // public java.awt.image.BufferedImage loci.formats.in.FV1000Reader.openThumbImage(int) throws loci.formats.FormatException,java.io.IOException
    virtual JavaObjectArray* getUsedFiles(); // public java.lang.String[] loci.formats.in.FV1000Reader.getUsedFiles()

};
}
}
}
#endif
