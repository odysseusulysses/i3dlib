#ifndef java_awt_PaintContext_H
#define java_awt_PaintContext_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace awt {
namespace image {
class ColorModel;
}
}
}

namespace java {
namespace awt {
namespace image {
class Raster;
}
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
class PaintContext : public java::lang::Object {
  public:
    PaintContext(JavaMarker* dummy);
    PaintContext(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual java::awt::image::ColorModel* getColorModel(); // public abstract java.awt.image.ColorModel java.awt.PaintContext.getColorModel()
    virtual java::awt::image::Raster* getRaster(int arg1, int arg2, int arg3, int arg4); // public abstract java.awt.image.Raster java.awt.PaintContext.getRaster(int,int,int,int)
    virtual void dispose(); // public abstract void java.awt.PaintContext.dispose()

};
}
}
#endif
