#ifndef java_awt_geom_AffineTransform_H
#define java_awt_geom_AffineTransform_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace lang {
class Cloneable;
}
}

namespace java {
namespace io {
class Serializable;
}
}

namespace java {
namespace lang {
class String;
}
}

namespace java {
namespace awt {
namespace geom {
class Point2D;
}
}
}

namespace java {
namespace awt {
class Shape;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace geom {
class AffineTransform : public java::lang::Object {
  public:
    AffineTransform(JavaMarker* dummy);
    AffineTransform(jobject obj);
    AffineTransform(java::awt::geom::AffineTransform* arg1); // public java.awt.geom.AffineTransform(java.awt.geom.AffineTransform)
    AffineTransform(float arg1, float arg2, float arg3, float arg4, float arg5, float arg6); // public java.awt.geom.AffineTransform(float,float,float,float,float,float)
    AffineTransform(JavaFloatArray* arg1); // public java.awt.geom.AffineTransform(float[])
    AffineTransform(double arg1, double arg2, double arg3, double arg4, double arg5, double arg6); // public java.awt.geom.AffineTransform(double,double,double,double,double,double)
    AffineTransform(JavaDoubleArray* arg1); // public java.awt.geom.AffineTransform(double[])
    AffineTransform(); // public java.awt.geom.AffineTransform()

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual int hashCode(); // public int java.awt.geom.AffineTransform.hashCode()
    virtual java::lang::Object* clone(); // public java.lang.Object java.awt.geom.AffineTransform.clone()
    virtual bool equals(java::lang::Object* arg1); // public boolean java.awt.geom.AffineTransform.equals(java.lang.Object)
    virtual const char* toString(); // public java.lang.String java.awt.geom.AffineTransform.toString()
    virtual void transform(JavaDoubleArray* arg1, int arg2, JavaDoubleArray* arg3, int arg4, int arg5); // public void java.awt.geom.AffineTransform.transform(double[],int,double[],int,int)
    virtual void transform(JavaFloatArray* arg1, int arg2, JavaFloatArray* arg3, int arg4, int arg5); // public void java.awt.geom.AffineTransform.transform(float[],int,float[],int,int)
    virtual void transform(JavaObjectArray* arg1, int arg2, JavaObjectArray* arg3, int arg4, int arg5); // public void java.awt.geom.AffineTransform.transform(java.awt.geom.Point2D[],int,java.awt.geom.Point2D[],int,int)
    virtual java::awt::geom::Point2D* transform(java::awt::geom::Point2D* arg1, java::awt::geom::Point2D* arg2); // public java.awt.geom.Point2D java.awt.geom.AffineTransform.transform(java.awt.geom.Point2D,java.awt.geom.Point2D)
    virtual void transform(JavaDoubleArray* arg1, int arg2, JavaFloatArray* arg3, int arg4, int arg5); // public void java.awt.geom.AffineTransform.transform(double[],int,float[],int,int)
    virtual void transform(JavaFloatArray* arg1, int arg2, JavaDoubleArray* arg3, int arg4, int arg5); // public void java.awt.geom.AffineTransform.transform(float[],int,double[],int,int)
    virtual int getType(); // public int java.awt.geom.AffineTransform.getType()
    virtual void rotate(double arg1, double arg2); // public void java.awt.geom.AffineTransform.rotate(double,double)
    virtual void rotate(double arg1, double arg2, double arg3, double arg4); // public void java.awt.geom.AffineTransform.rotate(double,double,double,double)
    virtual void rotate(double arg1, double arg2, double arg3); // public void java.awt.geom.AffineTransform.rotate(double,double,double)
    virtual void rotate(double arg1); // public void java.awt.geom.AffineTransform.rotate(double)
    virtual void scale(double arg1, double arg2); // public void java.awt.geom.AffineTransform.scale(double,double)
    virtual void translate(double arg1, double arg2); // public void java.awt.geom.AffineTransform.translate(double,double)
    virtual void setTransform(double arg1, double arg2, double arg3, double arg4, double arg5, double arg6); // public void java.awt.geom.AffineTransform.setTransform(double,double,double,double,double,double)
    virtual void setTransform(java::awt::geom::AffineTransform* arg1); // public void java.awt.geom.AffineTransform.setTransform(java.awt.geom.AffineTransform)
    virtual void shear(double arg1, double arg2); // public void java.awt.geom.AffineTransform.shear(double,double)
    virtual void concatenate(java::awt::geom::AffineTransform* arg1); // public void java.awt.geom.AffineTransform.concatenate(java.awt.geom.AffineTransform)
    virtual java::awt::geom::AffineTransform* createInverse(); // public java.awt.geom.AffineTransform java.awt.geom.AffineTransform.createInverse() throws java.awt.geom.NoninvertibleTransformException
    virtual java::awt::Shape* createTransformedShape(java::awt::Shape* arg1); // public java.awt.Shape java.awt.geom.AffineTransform.createTransformedShape(java.awt.Shape)
    virtual java::awt::geom::Point2D* deltaTransform(java::awt::geom::Point2D* arg1, java::awt::geom::Point2D* arg2); // public java.awt.geom.Point2D java.awt.geom.AffineTransform.deltaTransform(java.awt.geom.Point2D,java.awt.geom.Point2D)
    virtual void deltaTransform(JavaDoubleArray* arg1, int arg2, JavaDoubleArray* arg3, int arg4, int arg5); // public void java.awt.geom.AffineTransform.deltaTransform(double[],int,double[],int,int)
    virtual double getDeterminant(); // public double java.awt.geom.AffineTransform.getDeterminant()
    virtual void getMatrix(JavaDoubleArray* arg1); // public void java.awt.geom.AffineTransform.getMatrix(double[])
    virtual java::awt::geom::AffineTransform* getQuadrantRotateInstance(int arg1, double arg2, double arg3); // public static java.awt.geom.AffineTransform java.awt.geom.AffineTransform.getQuadrantRotateInstance(int,double,double)
    virtual java::awt::geom::AffineTransform* getQuadrantRotateInstance(int arg1); // public static java.awt.geom.AffineTransform java.awt.geom.AffineTransform.getQuadrantRotateInstance(int)
    virtual java::awt::geom::AffineTransform* getRotateInstance(double arg1, double arg2, double arg3); // public static java.awt.geom.AffineTransform java.awt.geom.AffineTransform.getRotateInstance(double,double,double)
    virtual java::awt::geom::AffineTransform* getRotateInstance(double arg1, double arg2, double arg3, double arg4); // public static java.awt.geom.AffineTransform java.awt.geom.AffineTransform.getRotateInstance(double,double,double,double)
    virtual java::awt::geom::AffineTransform* getRotateInstance(double arg1); // public static java.awt.geom.AffineTransform java.awt.geom.AffineTransform.getRotateInstance(double)
    virtual java::awt::geom::AffineTransform* getRotateInstance(double arg1, double arg2); // public static java.awt.geom.AffineTransform java.awt.geom.AffineTransform.getRotateInstance(double,double)
    virtual java::awt::geom::AffineTransform* getScaleInstance(double arg1, double arg2); // public static java.awt.geom.AffineTransform java.awt.geom.AffineTransform.getScaleInstance(double,double)
    virtual double getScaleX(); // public double java.awt.geom.AffineTransform.getScaleX()
    virtual double getScaleY(); // public double java.awt.geom.AffineTransform.getScaleY()
    virtual java::awt::geom::AffineTransform* getShearInstance(double arg1, double arg2); // public static java.awt.geom.AffineTransform java.awt.geom.AffineTransform.getShearInstance(double,double)
    virtual double getShearX(); // public double java.awt.geom.AffineTransform.getShearX()
    virtual double getShearY(); // public double java.awt.geom.AffineTransform.getShearY()
    virtual java::awt::geom::AffineTransform* getTranslateInstance(double arg1, double arg2); // public static java.awt.geom.AffineTransform java.awt.geom.AffineTransform.getTranslateInstance(double,double)
    virtual double getTranslateX(); // public double java.awt.geom.AffineTransform.getTranslateX()
    virtual double getTranslateY(); // public double java.awt.geom.AffineTransform.getTranslateY()
    virtual java::awt::geom::Point2D* inverseTransform(java::awt::geom::Point2D* arg1, java::awt::geom::Point2D* arg2); // public java.awt.geom.Point2D java.awt.geom.AffineTransform.inverseTransform(java.awt.geom.Point2D,java.awt.geom.Point2D) throws java.awt.geom.NoninvertibleTransformException
    virtual void inverseTransform(JavaDoubleArray* arg1, int arg2, JavaDoubleArray* arg3, int arg4, int arg5); // public void java.awt.geom.AffineTransform.inverseTransform(double[],int,double[],int,int) throws java.awt.geom.NoninvertibleTransformException
    virtual void invert(); // public void java.awt.geom.AffineTransform.invert() throws java.awt.geom.NoninvertibleTransformException
    virtual bool isIdentity(); // public boolean java.awt.geom.AffineTransform.isIdentity()
    virtual void preConcatenate(java::awt::geom::AffineTransform* arg1); // public void java.awt.geom.AffineTransform.preConcatenate(java.awt.geom.AffineTransform)
    virtual void quadrantRotate(int arg1, double arg2, double arg3); // public void java.awt.geom.AffineTransform.quadrantRotate(int,double,double)
    virtual void quadrantRotate(int arg1); // public void java.awt.geom.AffineTransform.quadrantRotate(int)
    virtual void setToIdentity(); // public void java.awt.geom.AffineTransform.setToIdentity()
    virtual void setToQuadrantRotation(int arg1, double arg2, double arg3); // public void java.awt.geom.AffineTransform.setToQuadrantRotation(int,double,double)
    virtual void setToQuadrantRotation(int arg1); // public void java.awt.geom.AffineTransform.setToQuadrantRotation(int)
    virtual void setToRotation(double arg1, double arg2); // public void java.awt.geom.AffineTransform.setToRotation(double,double)
    virtual void setToRotation(double arg1, double arg2, double arg3); // public void java.awt.geom.AffineTransform.setToRotation(double,double,double)
    virtual void setToRotation(double arg1, double arg2, double arg3, double arg4); // public void java.awt.geom.AffineTransform.setToRotation(double,double,double,double)
    virtual void setToRotation(double arg1); // public void java.awt.geom.AffineTransform.setToRotation(double)
    virtual void setToScale(double arg1, double arg2); // public void java.awt.geom.AffineTransform.setToScale(double,double)
    virtual void setToShear(double arg1, double arg2); // public void java.awt.geom.AffineTransform.setToShear(double,double)
    virtual void setToTranslation(double arg1, double arg2); // public void java.awt.geom.AffineTransform.setToTranslation(double,double)

    int TYPE_IDENTITY; // public static final int java.awt.geom.AffineTransform.TYPE_IDENTITY
    int TYPE_TRANSLATION; // public static final int java.awt.geom.AffineTransform.TYPE_TRANSLATION
    int TYPE_UNIFORM_SCALE; // public static final int java.awt.geom.AffineTransform.TYPE_UNIFORM_SCALE
    int TYPE_GENERAL_SCALE; // public static final int java.awt.geom.AffineTransform.TYPE_GENERAL_SCALE
    int TYPE_MASK_SCALE; // public static final int java.awt.geom.AffineTransform.TYPE_MASK_SCALE
    int TYPE_FLIP; // public static final int java.awt.geom.AffineTransform.TYPE_FLIP
    int TYPE_QUADRANT_ROTATION; // public static final int java.awt.geom.AffineTransform.TYPE_QUADRANT_ROTATION
    int TYPE_GENERAL_ROTATION; // public static final int java.awt.geom.AffineTransform.TYPE_GENERAL_ROTATION
    int TYPE_MASK_ROTATION; // public static final int java.awt.geom.AffineTransform.TYPE_MASK_ROTATION
    int TYPE_GENERAL_TRANSFORM; // public static final int java.awt.geom.AffineTransform.TYPE_GENERAL_TRANSFORM
};
}
}
}
#endif
