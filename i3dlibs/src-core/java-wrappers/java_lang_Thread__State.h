#ifndef java_lang_Thread__State_H
#define java_lang_Thread__State_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Enum.h>

namespace java {
namespace lang {
class String;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace lang {
class Thread__State : public java::lang::Enum {
  public:
    Thread__State(JavaMarker* dummy);
    Thread__State(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual java::lang::Thread__State* valueOf(const char* arg1); // public static java.lang.Thread$State java.lang.Thread$State.valueOf(java.lang.String)
    virtual JavaObjectArray* values(); // public static java.lang.Thread$State[] java.lang.Thread$State.values()

    java::lang::Thread__State* NEW; // public static final java.lang.Thread$State java.lang.Thread$State.NEW
    java::lang::Thread__State* RUNNABLE; // public static final java.lang.Thread$State java.lang.Thread$State.RUNNABLE
    java::lang::Thread__State* BLOCKED; // public static final java.lang.Thread$State java.lang.Thread$State.BLOCKED
    java::lang::Thread__State* WAITING; // public static final java.lang.Thread$State java.lang.Thread$State.WAITING
    java::lang::Thread__State* TIMED_WAITING; // public static final java.lang.Thread$State java.lang.Thread$State.TIMED_WAITING
    java::lang::Thread__State* TERMINATED; // public static final java.lang.Thread$State java.lang.Thread$State.TERMINATED
};
}
}
#endif
