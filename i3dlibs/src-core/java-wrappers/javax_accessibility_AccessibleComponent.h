#ifndef javax_accessibility_AccessibleComponent_H
#define javax_accessibility_AccessibleComponent_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace awt {
class Point;
}
}

namespace java {
namespace awt {
class Dimension;
}
}

namespace java {
namespace awt {
class Rectangle;
}
}

namespace java {
namespace awt {
class Color;
}
}

namespace java {
namespace awt {
class Font;
}
}

namespace java {
namespace awt {
class FontMetrics;
}
}

namespace java {
namespace awt {
class Cursor;
}
}

namespace javax {
namespace accessibility {
class Accessible;
}
}

namespace java {
namespace awt {
namespace event {
class FocusListener;
}
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace javax {
namespace accessibility {
class AccessibleComponent : public java::lang::Object {
  public:
    AccessibleComponent(JavaMarker* dummy);
    AccessibleComponent(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual bool contains(java::awt::Point* arg1); // public abstract boolean javax.accessibility.AccessibleComponent.contains(java.awt.Point)
    virtual java::awt::Point* getLocation(); // public abstract java.awt.Point javax.accessibility.AccessibleComponent.getLocation()
    virtual java::awt::Dimension* getSize(); // public abstract java.awt.Dimension javax.accessibility.AccessibleComponent.getSize()
    virtual void setSize(java::awt::Dimension* arg1); // public abstract void javax.accessibility.AccessibleComponent.setSize(java.awt.Dimension)
    virtual java::awt::Rectangle* getBounds(); // public abstract java.awt.Rectangle javax.accessibility.AccessibleComponent.getBounds()
    virtual void setBounds(java::awt::Rectangle* arg1); // public abstract void javax.accessibility.AccessibleComponent.setBounds(java.awt.Rectangle)
    virtual void setLocation(java::awt::Point* arg1); // public abstract void javax.accessibility.AccessibleComponent.setLocation(java.awt.Point)
    virtual java::awt::Color* getBackground(); // public abstract java.awt.Color javax.accessibility.AccessibleComponent.getBackground()
    virtual void setBackground(java::awt::Color* arg1); // public abstract void javax.accessibility.AccessibleComponent.setBackground(java.awt.Color)
    virtual java::awt::Font* getFont(); // public abstract java.awt.Font javax.accessibility.AccessibleComponent.getFont()
    virtual java::awt::FontMetrics* getFontMetrics(java::awt::Font* arg1); // public abstract java.awt.FontMetrics javax.accessibility.AccessibleComponent.getFontMetrics(java.awt.Font)
    virtual void setFont(java::awt::Font* arg1); // public abstract void javax.accessibility.AccessibleComponent.setFont(java.awt.Font)
    virtual void setVisible(bool arg1); // public abstract void javax.accessibility.AccessibleComponent.setVisible(boolean)
    virtual java::awt::Color* getForeground(); // public abstract java.awt.Color javax.accessibility.AccessibleComponent.getForeground()
    virtual void setForeground(java::awt::Color* arg1); // public abstract void javax.accessibility.AccessibleComponent.setForeground(java.awt.Color)
    virtual java::awt::Point* getLocationOnScreen(); // public abstract java.awt.Point javax.accessibility.AccessibleComponent.getLocationOnScreen()
    virtual bool isEnabled(); // public abstract boolean javax.accessibility.AccessibleComponent.isEnabled()
    virtual bool isShowing(); // public abstract boolean javax.accessibility.AccessibleComponent.isShowing()
    virtual bool isVisible(); // public abstract boolean javax.accessibility.AccessibleComponent.isVisible()
    virtual void requestFocus(); // public abstract void javax.accessibility.AccessibleComponent.requestFocus()
    virtual void setCursor(java::awt::Cursor* arg1); // public abstract void javax.accessibility.AccessibleComponent.setCursor(java.awt.Cursor)
    virtual javax::accessibility::Accessible* getAccessibleAt(java::awt::Point* arg1); // public abstract javax.accessibility.Accessible javax.accessibility.AccessibleComponent.getAccessibleAt(java.awt.Point)
    virtual void addFocusListener(java::awt::event::FocusListener* arg1); // public abstract void javax.accessibility.AccessibleComponent.addFocusListener(java.awt.event.FocusListener)
    virtual java::awt::Cursor* getCursor(); // public abstract java.awt.Cursor javax.accessibility.AccessibleComponent.getCursor()
    virtual bool isFocusTraversable(); // public abstract boolean javax.accessibility.AccessibleComponent.isFocusTraversable()
    virtual void removeFocusListener(java::awt::event::FocusListener* arg1); // public abstract void javax.accessibility.AccessibleComponent.removeFocusListener(java.awt.event.FocusListener)
    virtual void setEnabled(bool arg1); // public abstract void javax.accessibility.AccessibleComponent.setEnabled(boolean)

};
}
}
#endif
