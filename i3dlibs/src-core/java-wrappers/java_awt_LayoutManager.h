#ifndef java_awt_LayoutManager_H
#define java_awt_LayoutManager_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace lang {
class String;
}
}

namespace java {
namespace awt {
class Component;
}
}

namespace java {
namespace awt {
class Container;
}
}

namespace java {
namespace awt {
class Dimension;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
class LayoutManager : public java::lang::Object {
  public:
    LayoutManager(JavaMarker* dummy);
    LayoutManager(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual void addLayoutComponent(const char* arg1, java::awt::Component* arg2); // public abstract void java.awt.LayoutManager.addLayoutComponent(java.lang.String,java.awt.Component)
    virtual void layoutContainer(java::awt::Container* arg1); // public abstract void java.awt.LayoutManager.layoutContainer(java.awt.Container)
    virtual java::awt::Dimension* minimumLayoutSize(java::awt::Container* arg1); // public abstract java.awt.Dimension java.awt.LayoutManager.minimumLayoutSize(java.awt.Container)
    virtual java::awt::Dimension* preferredLayoutSize(java::awt::Container* arg1); // public abstract java.awt.Dimension java.awt.LayoutManager.preferredLayoutSize(java.awt.Container)
    virtual void removeLayoutComponent(java::awt::Component* arg1); // public abstract void java.awt.LayoutManager.removeLayoutComponent(java.awt.Component)

};
}
}
#endif
