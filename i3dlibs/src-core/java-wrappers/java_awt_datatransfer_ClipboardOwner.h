#ifndef java_awt_datatransfer_ClipboardOwner_H
#define java_awt_datatransfer_ClipboardOwner_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace awt {
namespace datatransfer {
class Clipboard;
}
}
}

namespace java {
namespace awt {
namespace datatransfer {
class Transferable;
}
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace datatransfer {
class ClipboardOwner : public java::lang::Object {
  public:
    ClipboardOwner(JavaMarker* dummy);
    ClipboardOwner(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual void lostOwnership(java::awt::datatransfer::Clipboard* arg1, java::awt::datatransfer::Transferable* arg2); // public abstract void java.awt.datatransfer.ClipboardOwner.lostOwnership(java.awt.datatransfer.Clipboard,java.awt.datatransfer.Transferable)

};
}
}
}
#endif
