#ifndef java_nio_channels_Channel_H
#define java_nio_channels_Channel_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace io {
class Closeable;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace nio {
namespace channels {
class Channel : public java::lang::Object {
  public:
    Channel(JavaMarker* dummy);
    Channel(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual void close(); // public abstract void java.nio.channels.Channel.close() throws java.io.IOException
    virtual bool isOpen(); // public abstract boolean java.nio.channels.Channel.isOpen()

};
}
}
}
#endif
