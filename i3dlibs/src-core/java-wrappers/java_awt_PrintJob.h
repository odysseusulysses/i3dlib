#ifndef java_awt_PrintJob_H
#define java_awt_PrintJob_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace awt {
class Graphics;
}
}

namespace java {
namespace awt {
class Dimension;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
class PrintJob : public java::lang::Object {
  public:
    PrintJob(JavaMarker* dummy);
    PrintJob(jobject obj);
    PrintJob(); // public java.awt.PrintJob()

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual void finalize(); // public void java.awt.PrintJob.finalize()
    virtual void end(); // public abstract void java.awt.PrintJob.end()
    virtual java::awt::Graphics* getGraphics(); // public abstract java.awt.Graphics java.awt.PrintJob.getGraphics()
    virtual java::awt::Dimension* getPageDimension(); // public abstract java.awt.Dimension java.awt.PrintJob.getPageDimension()
    virtual int getPageResolution(); // public abstract int java.awt.PrintJob.getPageResolution()
    virtual bool lastPageFirst(); // public abstract boolean java.awt.PrintJob.lastPageFirst()

};
}
}
#endif
