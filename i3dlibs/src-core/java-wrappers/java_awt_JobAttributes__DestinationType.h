#ifndef java_awt_JobAttributes__DestinationType_H
#define java_awt_JobAttributes__DestinationType_H
#include <jni.h>
#include <java_marker.h>
#include <java_awt_AttributeValue.h>

namespace java {
namespace lang {
class String;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
class JobAttributes__DestinationType : public java::awt::AttributeValue {
  public:
    JobAttributes__DestinationType(JavaMarker* dummy);
    JobAttributes__DestinationType(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual int hashCode(); // public int java.awt.JobAttributes$DestinationType.hashCode()
    virtual const char* toString(); // public java.lang.String java.awt.JobAttributes$DestinationType.toString()

    java::awt::JobAttributes__DestinationType* FILE; // public static final java.awt.JobAttributes$DestinationType java.awt.JobAttributes$DestinationType.FILE
    java::awt::JobAttributes__DestinationType* PRINTER; // public static final java.awt.JobAttributes$DestinationType java.awt.JobAttributes$DestinationType.PRINTER
};
}
}
#endif
