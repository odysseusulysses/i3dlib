#ifndef java_awt_event_HierarchyBoundsListener_H
#define java_awt_event_HierarchyBoundsListener_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace util {
class EventListener;
}
}

namespace java {
namespace awt {
namespace event {
class HierarchyEvent;
}
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace event {
class HierarchyBoundsListener : public java::lang::Object {
  public:
    HierarchyBoundsListener(JavaMarker* dummy);
    HierarchyBoundsListener(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual void ancestorMoved(java::awt::event::HierarchyEvent* arg1); // public abstract void java.awt.event.HierarchyBoundsListener.ancestorMoved(java.awt.event.HierarchyEvent)
    virtual void ancestorResized(java::awt::event::HierarchyEvent* arg1); // public abstract void java.awt.event.HierarchyBoundsListener.ancestorResized(java.awt.event.HierarchyEvent)

};
}
}
}
#endif
