#ifndef java_awt_event_WindowListener_H
#define java_awt_event_WindowListener_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace util {
class EventListener;
}
}

namespace java {
namespace awt {
namespace event {
class WindowEvent;
}
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace event {
class WindowListener : public java::lang::Object {
  public:
    WindowListener(JavaMarker* dummy);
    WindowListener(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual void windowActivated(java::awt::event::WindowEvent* arg1); // public abstract void java.awt.event.WindowListener.windowActivated(java.awt.event.WindowEvent)
    virtual void windowClosed(java::awt::event::WindowEvent* arg1); // public abstract void java.awt.event.WindowListener.windowClosed(java.awt.event.WindowEvent)
    virtual void windowClosing(java::awt::event::WindowEvent* arg1); // public abstract void java.awt.event.WindowListener.windowClosing(java.awt.event.WindowEvent)
    virtual void windowDeactivated(java::awt::event::WindowEvent* arg1); // public abstract void java.awt.event.WindowListener.windowDeactivated(java.awt.event.WindowEvent)
    virtual void windowDeiconified(java::awt::event::WindowEvent* arg1); // public abstract void java.awt.event.WindowListener.windowDeiconified(java.awt.event.WindowEvent)
    virtual void windowIconified(java::awt::event::WindowEvent* arg1); // public abstract void java.awt.event.WindowListener.windowIconified(java.awt.event.WindowEvent)
    virtual void windowOpened(java::awt::event::WindowEvent* arg1); // public abstract void java.awt.event.WindowListener.windowOpened(java.awt.event.WindowEvent)

};
}
}
}
#endif
