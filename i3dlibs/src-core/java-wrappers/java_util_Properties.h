#ifndef java_util_Properties_H
#define java_util_Properties_H
#include <jni.h>
#include <java_marker.h>
#include <java_util_Hashtable.h>

namespace java {
namespace lang {
class Object;
}
}

namespace java {
namespace lang {
class String;
}
}

namespace java {
namespace io {
class InputStream;
}
}

namespace java {
namespace io {
class Reader;
}
}

namespace java {
namespace io {
class PrintStream;
}
}

namespace java {
namespace io {
class PrintWriter;
}
}

namespace java {
namespace util {
class Enumeration;
}
}

namespace java {
namespace io {
class OutputStream;
}
}

namespace java {
namespace io {
class Writer;
}
}

namespace java {
namespace util {
class Set;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace util {
class Properties : public java::util::Hashtable {
  public:
    Properties(JavaMarker* dummy);
    Properties(jobject obj);
    Properties(java::util::Properties* arg1); // public java.util.Properties(java.util.Properties)
    Properties(); // public java.util.Properties()

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual java::lang::Object* setProperty(const char* arg1, const char* arg2); // public synchronized java.lang.Object java.util.Properties.setProperty(java.lang.String,java.lang.String)
    virtual const char* getProperty(const char* arg1); // public java.lang.String java.util.Properties.getProperty(java.lang.String)
    virtual const char* getProperty(const char* arg1, const char* arg2); // public java.lang.String java.util.Properties.getProperty(java.lang.String,java.lang.String)
    virtual void load(java::io::InputStream* arg1); // public synchronized void java.util.Properties.load(java.io.InputStream) throws java.io.IOException
    virtual void load(java::io::Reader* arg1); // public synchronized void java.util.Properties.load(java.io.Reader) throws java.io.IOException
    virtual void list(java::io::PrintStream* arg1); // public void java.util.Properties.list(java.io.PrintStream)
    virtual void list(java::io::PrintWriter* arg1); // public void java.util.Properties.list(java.io.PrintWriter)
    virtual void loadFromXML(java::io::InputStream* arg1); // public synchronized void java.util.Properties.loadFromXML(java.io.InputStream) throws java.io.IOException,java.util.InvalidPropertiesFormatException
    virtual java::util::Enumeration* propertyNames(); // public java.util.Enumeration java.util.Properties.propertyNames()
    virtual void save(java::io::OutputStream* arg1, const char* arg2); // public synchronized void java.util.Properties.save(java.io.OutputStream,java.lang.String)
    virtual void store(java::io::OutputStream* arg1, const char* arg2); // public void java.util.Properties.store(java.io.OutputStream,java.lang.String) throws java.io.IOException
    virtual void store(java::io::Writer* arg1, const char* arg2); // public void java.util.Properties.store(java.io.Writer,java.lang.String) throws java.io.IOException
    virtual void storeToXML(java::io::OutputStream* arg1, const char* arg2, const char* arg3); // public synchronized void java.util.Properties.storeToXML(java.io.OutputStream,java.lang.String,java.lang.String) throws java.io.IOException
    virtual void storeToXML(java::io::OutputStream* arg1, const char* arg2); // public synchronized void java.util.Properties.storeToXML(java.io.OutputStream,java.lang.String) throws java.io.IOException
    virtual java::util::Set* stringPropertyNames(); // public java.util.Set java.util.Properties.stringPropertyNames()

};
}
}
#endif
