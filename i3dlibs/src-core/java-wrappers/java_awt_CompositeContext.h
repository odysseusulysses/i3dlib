#ifndef java_awt_CompositeContext_H
#define java_awt_CompositeContext_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace awt {
namespace image {
class Raster;
}
}
}

namespace java {
namespace awt {
namespace image {
class WritableRaster;
}
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
class CompositeContext : public java::lang::Object {
  public:
    CompositeContext(JavaMarker* dummy);
    CompositeContext(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual void dispose(); // public abstract void java.awt.CompositeContext.dispose()
    virtual void compose(java::awt::image::Raster* arg1, java::awt::image::Raster* arg2, java::awt::image::WritableRaster* arg3); // public abstract void java.awt.CompositeContext.compose(java.awt.image.Raster,java.awt.image.Raster,java.awt.image.WritableRaster)

};
}
}
#endif
