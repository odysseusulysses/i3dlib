#ifndef javax_accessibility_Accessible_H
#define javax_accessibility_Accessible_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace javax {
namespace accessibility {
class AccessibleContext;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace javax {
namespace accessibility {
class Accessible : public java::lang::Object {
  public:
    Accessible(JavaMarker* dummy);
    Accessible(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual javax::accessibility::AccessibleContext* getAccessibleContext(); // public abstract javax.accessibility.AccessibleContext javax.accessibility.Accessible.getAccessibleContext()

};
}
}
#endif
