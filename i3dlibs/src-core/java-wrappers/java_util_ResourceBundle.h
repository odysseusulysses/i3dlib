#ifndef java_util_ResourceBundle_H
#define java_util_ResourceBundle_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace lang {
class String;
}
}

namespace java {
namespace util {
class Set;
}
}

namespace java {
namespace util {
class Locale;
}
}

namespace java {
namespace util {
class Enumeration;
}
}

namespace java {
namespace lang {
class ClassLoader;
}
}

namespace java {
namespace util {
class ResourceBundle__Control;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace util {
class ResourceBundle : public java::lang::Object {
  public:
    ResourceBundle(JavaMarker* dummy);
    ResourceBundle(jobject obj);
    ResourceBundle(); // public java.util.ResourceBundle()

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual java::lang::Object* getObject(const char* arg1); // public final java.lang.Object java.util.ResourceBundle.getObject(java.lang.String)
    virtual java::util::Set* keySet(); // public java.util.Set java.util.ResourceBundle.keySet()
    virtual bool containsKey(const char* arg1); // public boolean java.util.ResourceBundle.containsKey(java.lang.String)
    virtual const char* getString(const char* arg1); // public final java.lang.String java.util.ResourceBundle.getString(java.lang.String)
    virtual java::util::Locale* getLocale(); // public java.util.Locale java.util.ResourceBundle.getLocale()
    virtual java::util::Enumeration* getKeys(); // public abstract java.util.Enumeration java.util.ResourceBundle.getKeys()
    virtual void clearCache(); // public static final void java.util.ResourceBundle.clearCache()
    virtual void clearCache(java::lang::ClassLoader* arg1); // public static final void java.util.ResourceBundle.clearCache(java.lang.ClassLoader)
    virtual java::util::ResourceBundle* getBundle(const char* arg1, java::util::Locale* arg2, java::lang::ClassLoader* arg3, java::util::ResourceBundle__Control* arg4); // public static java.util.ResourceBundle java.util.ResourceBundle.getBundle(java.lang.String,java.util.Locale,java.lang.ClassLoader,java.util.ResourceBundle$Control)
    virtual java::util::ResourceBundle* getBundle(const char* arg1, java::util::Locale* arg2, java::lang::ClassLoader* arg3); // public static java.util.ResourceBundle java.util.ResourceBundle.getBundle(java.lang.String,java.util.Locale,java.lang.ClassLoader)
    virtual java::util::ResourceBundle* getBundle(const char* arg1, java::util::ResourceBundle__Control* arg2); // public static final java.util.ResourceBundle java.util.ResourceBundle.getBundle(java.lang.String,java.util.ResourceBundle$Control)
    virtual java::util::ResourceBundle* getBundle(const char* arg1); // public static final java.util.ResourceBundle java.util.ResourceBundle.getBundle(java.lang.String)
    virtual java::util::ResourceBundle* getBundle(const char* arg1, java::util::Locale* arg2); // public static final java.util.ResourceBundle java.util.ResourceBundle.getBundle(java.lang.String,java.util.Locale)
    virtual java::util::ResourceBundle* getBundle(const char* arg1, java::util::Locale* arg2, java::util::ResourceBundle__Control* arg3); // public static final java.util.ResourceBundle java.util.ResourceBundle.getBundle(java.lang.String,java.util.Locale,java.util.ResourceBundle$Control)
    virtual JavaObjectArray* getStringArray(const char* arg1); // public final java.lang.String[] java.util.ResourceBundle.getStringArray(java.lang.String)

};
}
}
#endif
