#ifndef java_awt_dnd_DropTarget_H
#define java_awt_dnd_DropTarget_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace awt {
namespace dnd {
class DropTargetListener;
}
}
}

namespace java {
namespace io {
class Serializable;
}
}

namespace java {
namespace awt {
class Component;
}
}

namespace java {
namespace awt {
namespace datatransfer {
class FlavorMap;
}
}
}

namespace java {
namespace awt {
namespace peer {
class ComponentPeer;
}
}
}

namespace java {
namespace awt {
namespace dnd {
class DropTargetDragEvent;
}
}
}

namespace java {
namespace awt {
namespace dnd {
class DropTargetEvent;
}
}
}

namespace java {
namespace awt {
namespace dnd {
class DropTargetDropEvent;
}
}
}

namespace java {
namespace awt {
namespace dnd {
class DropTargetContext;
}
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace dnd {
class DropTarget : public java::lang::Object {
  public:
    DropTarget(JavaMarker* dummy);
    DropTarget(jobject obj);
    DropTarget(java::awt::Component* arg1, int arg2, java::awt::dnd::DropTargetListener* arg3, bool arg4); // public java.awt.dnd.DropTarget(java.awt.Component,int,java.awt.dnd.DropTargetListener,boolean) throws java.awt.HeadlessException
    DropTarget(); // public java.awt.dnd.DropTarget() throws java.awt.HeadlessException
    DropTarget(java::awt::Component* arg1, java::awt::dnd::DropTargetListener* arg2); // public java.awt.dnd.DropTarget(java.awt.Component,java.awt.dnd.DropTargetListener) throws java.awt.HeadlessException
    DropTarget(java::awt::Component* arg1, int arg2, java::awt::dnd::DropTargetListener* arg3); // public java.awt.dnd.DropTarget(java.awt.Component,int,java.awt.dnd.DropTargetListener) throws java.awt.HeadlessException
    DropTarget(java::awt::Component* arg1, int arg2, java::awt::dnd::DropTargetListener* arg3, bool arg4, java::awt::datatransfer::FlavorMap* arg5); // public java.awt.dnd.DropTarget(java.awt.Component,int,java.awt.dnd.DropTargetListener,boolean,java.awt.datatransfer.FlavorMap) throws java.awt.HeadlessException

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual void addNotify(java::awt::peer::ComponentPeer* arg1); // public void java.awt.dnd.DropTarget.addNotify(java.awt.peer.ComponentPeer)
    virtual bool isActive(); // public boolean java.awt.dnd.DropTarget.isActive()
    virtual void removeNotify(java::awt::peer::ComponentPeer* arg1); // public void java.awt.dnd.DropTarget.removeNotify(java.awt.peer.ComponentPeer)
    virtual java::awt::Component* getComponent(); // public synchronized java.awt.Component java.awt.dnd.DropTarget.getComponent()
    virtual void setComponent(java::awt::Component* arg1); // public synchronized void java.awt.dnd.DropTarget.setComponent(java.awt.Component)
    virtual void addDropTargetListener(java::awt::dnd::DropTargetListener* arg1); // public synchronized void java.awt.dnd.DropTarget.addDropTargetListener(java.awt.dnd.DropTargetListener) throws java.util.TooManyListenersException
    virtual void dragEnter(java::awt::dnd::DropTargetDragEvent* arg1); // public synchronized void java.awt.dnd.DropTarget.dragEnter(java.awt.dnd.DropTargetDragEvent)
    virtual void dragExit(java::awt::dnd::DropTargetEvent* arg1); // public synchronized void java.awt.dnd.DropTarget.dragExit(java.awt.dnd.DropTargetEvent)
    virtual void dragOver(java::awt::dnd::DropTargetDragEvent* arg1); // public synchronized void java.awt.dnd.DropTarget.dragOver(java.awt.dnd.DropTargetDragEvent)
    virtual void drop(java::awt::dnd::DropTargetDropEvent* arg1); // public synchronized void java.awt.dnd.DropTarget.drop(java.awt.dnd.DropTargetDropEvent)
    virtual void dropActionChanged(java::awt::dnd::DropTargetDragEvent* arg1); // public synchronized void java.awt.dnd.DropTarget.dropActionChanged(java.awt.dnd.DropTargetDragEvent)
    virtual int getDefaultActions(); // public int java.awt.dnd.DropTarget.getDefaultActions()
    virtual java::awt::dnd::DropTargetContext* getDropTargetContext(); // public java.awt.dnd.DropTargetContext java.awt.dnd.DropTarget.getDropTargetContext()
    virtual java::awt::datatransfer::FlavorMap* getFlavorMap(); // public java.awt.datatransfer.FlavorMap java.awt.dnd.DropTarget.getFlavorMap()
    virtual void removeDropTargetListener(java::awt::dnd::DropTargetListener* arg1); // public synchronized void java.awt.dnd.DropTarget.removeDropTargetListener(java.awt.dnd.DropTargetListener)
    virtual void setActive(bool arg1); // public synchronized void java.awt.dnd.DropTarget.setActive(boolean)
    virtual void setDefaultActions(int arg1); // public void java.awt.dnd.DropTarget.setDefaultActions(int)
    virtual void setFlavorMap(java::awt::datatransfer::FlavorMap* arg1); // public void java.awt.dnd.DropTarget.setFlavorMap(java.awt.datatransfer.FlavorMap)

};
}
}
}
#endif
