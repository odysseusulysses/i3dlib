#ifndef java_awt_event_ContainerEvent_H
#define java_awt_event_ContainerEvent_H
#include <jni.h>
#include <java_marker.h>
#include <java_awt_event_ComponentEvent.h>

namespace java {
namespace awt {
class Component;
}
}

namespace java {
namespace lang {
class String;
}
}

namespace java {
namespace awt {
class Container;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace event {
class ContainerEvent : public java::awt::event::ComponentEvent {
  public:
    ContainerEvent(JavaMarker* dummy);
    ContainerEvent(jobject obj);
    ContainerEvent(java::awt::Component* arg1, int arg2, java::awt::Component* arg3); // public java.awt.event.ContainerEvent(java.awt.Component,int,java.awt.Component)

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual const char* paramString(); // public java.lang.String java.awt.event.ContainerEvent.paramString()
    virtual java::awt::Component* getChild(); // public java.awt.Component java.awt.event.ContainerEvent.getChild()
    virtual java::awt::Container* getContainer(); // public java.awt.Container java.awt.event.ContainerEvent.getContainer()

    int CONTAINER_FIRST; // public static final int java.awt.event.ContainerEvent.CONTAINER_FIRST
    int CONTAINER_LAST; // public static final int java.awt.event.ContainerEvent.CONTAINER_LAST
    int COMPONENT_ADDED; // public static final int java.awt.event.ContainerEvent.COMPONENT_ADDED
    int COMPONENT_REMOVED; // public static final int java.awt.event.ContainerEvent.COMPONENT_REMOVED
};
}
}
}
#endif
