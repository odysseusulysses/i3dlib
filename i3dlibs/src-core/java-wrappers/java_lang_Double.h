#ifndef java_lang_Double_H
#define java_lang_Double_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Number.h>

namespace java {
namespace lang {
class Comparable;
}
}

namespace java {
namespace lang {
class String;
}
}

namespace java {
namespace lang {
class Object;
}
}

namespace java {
namespace lang {
class Class;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace lang {
class Double : public java::lang::Number {
  public:
    Double(JavaMarker* dummy);
    Double(jobject obj);
    Double(double arg1); // public java.lang.Double(double)
    Double(const char* arg1); // public java.lang.Double(java.lang.String) throws java.lang.NumberFormatException

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual long doubleToRawLongBits(double arg1); // public static native long java.lang.Double.doubleToRawLongBits(double)
    virtual long doubleToLongBits(double arg1); // public static long java.lang.Double.doubleToLongBits(double)
    virtual double longBitsToDouble(long arg1); // public static native double java.lang.Double.longBitsToDouble(long)
    virtual int hashCode(); // public int java.lang.Double.hashCode()
    virtual int compareTo(java::lang::Object* arg1); // public int java.lang.Double.compareTo(java.lang.Object)
    virtual int compareTo(java::lang::Double* arg1); // public int java.lang.Double.compareTo(java.lang.Double)
    virtual bool equals(java::lang::Object* arg1); // public boolean java.lang.Double.equals(java.lang.Object)
    virtual const char* toString(double arg1); // public static java.lang.String java.lang.Double.toString(double)
    virtual const char* toString(); // public java.lang.String java.lang.Double.toString()
    virtual const char* toHexString(double arg1); // public static java.lang.String java.lang.Double.toHexString(double)
    virtual int compare(double arg1, double arg2); // public static int java.lang.Double.compare(double,double)
    virtual java::lang::Double* valueOf(double arg1); // public static java.lang.Double java.lang.Double.valueOf(double)
    virtual java::lang::Double* valueOf(const char* arg1); // public static java.lang.Double java.lang.Double.valueOf(java.lang.String) throws java.lang.NumberFormatException
    virtual bool isNaN(double arg1); // public static boolean java.lang.Double.isNaN(double)
    virtual bool isNaN(); // public boolean java.lang.Double.isNaN()
    virtual char byteValue(); // public byte java.lang.Double.byteValue()
    virtual double doubleValue(); // public double java.lang.Double.doubleValue()
    virtual float floatValue(); // public float java.lang.Double.floatValue()
    virtual int intValue(); // public int java.lang.Double.intValue()
    virtual bool isInfinite(); // public boolean java.lang.Double.isInfinite()
    virtual bool isInfinite(double arg1); // public static boolean java.lang.Double.isInfinite(double)
    virtual long longValue(); // public long java.lang.Double.longValue()
    virtual short shortValue(); // public short java.lang.Double.shortValue()
    virtual double parseDouble(const char* arg1); // public static double java.lang.Double.parseDouble(java.lang.String) throws java.lang.NumberFormatException

    double POSITIVE_INFINITY; // public static final double java.lang.Double.POSITIVE_INFINITY
    double NEGATIVE_INFINITY; // public static final double java.lang.Double.NEGATIVE_INFINITY
    double NaN; // public static final double java.lang.Double.NaN
    double MAX_VALUE; // public static final double java.lang.Double.MAX_VALUE
    double MIN_NORMAL; // public static final double java.lang.Double.MIN_NORMAL
    double MIN_VALUE; // public static final double java.lang.Double.MIN_VALUE
    int MAX_EXPONENT; // public static final int java.lang.Double.MAX_EXPONENT
    int MIN_EXPONENT; // public static final int java.lang.Double.MIN_EXPONENT
    int SIZE; // public static final int java.lang.Double.SIZE
    java::lang::Class* TYPE; // public static final java.lang.Class java.lang.Double.TYPE
};
}
}
#endif
