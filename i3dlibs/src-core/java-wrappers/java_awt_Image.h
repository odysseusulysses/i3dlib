#ifndef java_awt_Image_H
#define java_awt_Image_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace lang {
class String;
}
}

namespace java {
namespace awt {
namespace image {
class ImageObserver;
}
}
}

namespace java {
namespace awt {
class Graphics;
}
}

namespace java {
namespace awt {
namespace image {
class ImageProducer;
}
}
}

namespace java {
namespace awt {
class ImageCapabilities;
}
}

namespace java {
namespace awt {
class GraphicsConfiguration;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
class Image : public java::lang::Object {
  public:
    Image(JavaMarker* dummy);
    Image(jobject obj);
    Image(); // public java.awt.Image()

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual java::lang::Object* getProperty(const char* arg1, java::awt::image::ImageObserver* arg2); // public abstract java.lang.Object java.awt.Image.getProperty(java.lang.String,java.awt.image.ImageObserver)
    virtual void flush(); // public void java.awt.Image.flush()
    virtual java::awt::Graphics* getGraphics(); // public abstract java.awt.Graphics java.awt.Image.getGraphics()
    virtual int getHeight(java::awt::image::ImageObserver* arg1); // public abstract int java.awt.Image.getHeight(java.awt.image.ImageObserver)
    virtual java::awt::image::ImageProducer* getSource(); // public abstract java.awt.image.ImageProducer java.awt.Image.getSource()
    virtual int getWidth(java::awt::image::ImageObserver* arg1); // public abstract int java.awt.Image.getWidth(java.awt.image.ImageObserver)
    virtual float getAccelerationPriority(); // public float java.awt.Image.getAccelerationPriority()
    virtual java::awt::ImageCapabilities* getCapabilities(java::awt::GraphicsConfiguration* arg1); // public java.awt.ImageCapabilities java.awt.Image.getCapabilities(java.awt.GraphicsConfiguration)
    virtual java::awt::Image* getScaledInstance(int arg1, int arg2, int arg3); // public java.awt.Image java.awt.Image.getScaledInstance(int,int,int)
    virtual void setAccelerationPriority(float arg1); // public void java.awt.Image.setAccelerationPriority(float)

    java::lang::Object* UndefinedProperty; // public static final java.lang.Object java.awt.Image.UndefinedProperty
    int SCALE_DEFAULT; // public static final int java.awt.Image.SCALE_DEFAULT
    int SCALE_FAST; // public static final int java.awt.Image.SCALE_FAST
    int SCALE_SMOOTH; // public static final int java.awt.Image.SCALE_SMOOTH
    int SCALE_REPLICATE; // public static final int java.awt.Image.SCALE_REPLICATE
    int SCALE_AREA_AVERAGING; // public static final int java.awt.Image.SCALE_AREA_AVERAGING
};
}
}
#endif
