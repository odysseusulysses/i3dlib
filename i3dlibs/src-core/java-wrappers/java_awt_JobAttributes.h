#ifndef java_awt_JobAttributes_H
#define java_awt_JobAttributes_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace lang {
class Cloneable;
}
}

namespace java {
namespace awt {
class JobAttributes__DefaultSelectionType;
}
}

namespace java {
namespace awt {
class JobAttributes__DestinationType;
}
}

namespace java {
namespace awt {
class JobAttributes__DialogType;
}
}

namespace java {
namespace lang {
class String;
}
}

namespace java {
namespace awt {
class JobAttributes__MultipleDocumentHandlingType;
}
}

namespace java {
namespace awt {
class JobAttributes__SidesType;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
class JobAttributes : public java::lang::Object {
  public:
    JobAttributes(JavaMarker* dummy);
    JobAttributes(jobject obj);
    JobAttributes(int arg1, java::awt::JobAttributes__DefaultSelectionType* arg2, java::awt::JobAttributes__DestinationType* arg3, java::awt::JobAttributes__DialogType* arg4, const char* arg5, int arg6, int arg7, java::awt::JobAttributes__MultipleDocumentHandlingType* arg8, JavaObjectArray* arg9, const char* arg10, java::awt::JobAttributes__SidesType* arg11); // public java.awt.JobAttributes(int,java.awt.JobAttributes$DefaultSelectionType,java.awt.JobAttributes$DestinationType,java.awt.JobAttributes$DialogType,java.lang.String,int,int,java.awt.JobAttributes$MultipleDocumentHandlingType,int[][],java.lang.String,java.awt.JobAttributes$SidesType)
    JobAttributes(java::awt::JobAttributes* arg1); // public java.awt.JobAttributes(java.awt.JobAttributes)
    JobAttributes(); // public java.awt.JobAttributes()

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual int hashCode(); // public int java.awt.JobAttributes.hashCode()
    virtual java::lang::Object* clone(); // public java.lang.Object java.awt.JobAttributes.clone()
    virtual bool equals(java::lang::Object* arg1); // public boolean java.awt.JobAttributes.equals(java.lang.Object)
    virtual const char* toString(); // public java.lang.String java.awt.JobAttributes.toString()
    virtual void set(java::awt::JobAttributes* arg1); // public void java.awt.JobAttributes.set(java.awt.JobAttributes)
    virtual const char* getFileName(); // public java.lang.String java.awt.JobAttributes.getFileName()
    virtual int getCopies(); // public int java.awt.JobAttributes.getCopies()
    virtual java::awt::JobAttributes__DefaultSelectionType* getDefaultSelection(); // public java.awt.JobAttributes$DefaultSelectionType java.awt.JobAttributes.getDefaultSelection()
    virtual java::awt::JobAttributes__DestinationType* getDestination(); // public java.awt.JobAttributes$DestinationType java.awt.JobAttributes.getDestination()
    virtual java::awt::JobAttributes__DialogType* getDialog(); // public java.awt.JobAttributes$DialogType java.awt.JobAttributes.getDialog()
    virtual int getFromPage(); // public int java.awt.JobAttributes.getFromPage()
    virtual int getMaxPage(); // public int java.awt.JobAttributes.getMaxPage()
    virtual int getMinPage(); // public int java.awt.JobAttributes.getMinPage()
    virtual java::awt::JobAttributes__MultipleDocumentHandlingType* getMultipleDocumentHandling(); // public java.awt.JobAttributes$MultipleDocumentHandlingType java.awt.JobAttributes.getMultipleDocumentHandling()
    virtual JavaObjectArray* getPageRanges(); // public int[][] java.awt.JobAttributes.getPageRanges()
    virtual const char* getPrinter(); // public java.lang.String java.awt.JobAttributes.getPrinter()
    virtual java::awt::JobAttributes__SidesType* getSides(); // public java.awt.JobAttributes$SidesType java.awt.JobAttributes.getSides()
    virtual int getToPage(); // public int java.awt.JobAttributes.getToPage()
    virtual void setCopies(int arg1); // public void java.awt.JobAttributes.setCopies(int)
    virtual void setCopiesToDefault(); // public void java.awt.JobAttributes.setCopiesToDefault()
    virtual void setDefaultSelection(java::awt::JobAttributes__DefaultSelectionType* arg1); // public void java.awt.JobAttributes.setDefaultSelection(java.awt.JobAttributes$DefaultSelectionType)
    virtual void setDestination(java::awt::JobAttributes__DestinationType* arg1); // public void java.awt.JobAttributes.setDestination(java.awt.JobAttributes$DestinationType)
    virtual void setDialog(java::awt::JobAttributes__DialogType* arg1); // public void java.awt.JobAttributes.setDialog(java.awt.JobAttributes$DialogType)
    virtual void setFileName(const char* arg1); // public void java.awt.JobAttributes.setFileName(java.lang.String)
    virtual void setFromPage(int arg1); // public void java.awt.JobAttributes.setFromPage(int)
    virtual void setMaxPage(int arg1); // public void java.awt.JobAttributes.setMaxPage(int)
    virtual void setMinPage(int arg1); // public void java.awt.JobAttributes.setMinPage(int)
    virtual void setMultipleDocumentHandling(java::awt::JobAttributes__MultipleDocumentHandlingType* arg1); // public void java.awt.JobAttributes.setMultipleDocumentHandling(java.awt.JobAttributes$MultipleDocumentHandlingType)
    virtual void setMultipleDocumentHandlingToDefault(); // public void java.awt.JobAttributes.setMultipleDocumentHandlingToDefault()
    virtual void setPageRanges(JavaObjectArray* arg1); // public void java.awt.JobAttributes.setPageRanges(int[][])
    virtual void setPrinter(const char* arg1); // public void java.awt.JobAttributes.setPrinter(java.lang.String)
    virtual void setSides(java::awt::JobAttributes__SidesType* arg1); // public void java.awt.JobAttributes.setSides(java.awt.JobAttributes$SidesType)
    virtual void setSidesToDefault(); // public void java.awt.JobAttributes.setSidesToDefault()
    virtual void setToPage(int arg1); // public void java.awt.JobAttributes.setToPage(int)

};
}
}
#endif
