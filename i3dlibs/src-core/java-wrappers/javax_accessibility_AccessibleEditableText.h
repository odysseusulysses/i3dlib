#ifndef javax_accessibility_AccessibleEditableText_H
#define javax_accessibility_AccessibleEditableText_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace javax {
namespace accessibility {
class AccessibleText;
}
}

namespace java {
namespace lang {
class String;
}
}

namespace javax {
namespace swing {
namespace text {
class AttributeSet;
}
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace javax {
namespace accessibility {
class AccessibleEditableText : public java::lang::Object {
  public:
    AccessibleEditableText(JavaMarker* dummy);
    AccessibleEditableText(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual void delete_(int arg1, int arg2); // public abstract void javax.accessibility.AccessibleEditableText.delete(int,int)
    virtual void cut(int arg1, int arg2); // public abstract void javax.accessibility.AccessibleEditableText.cut(int,int)
    virtual const char* getTextRange(int arg1, int arg2); // public abstract java.lang.String javax.accessibility.AccessibleEditableText.getTextRange(int,int)
    virtual void insertTextAtIndex(int arg1, const char* arg2); // public abstract void javax.accessibility.AccessibleEditableText.insertTextAtIndex(int,java.lang.String)
    virtual void paste(int arg1); // public abstract void javax.accessibility.AccessibleEditableText.paste(int)
    virtual void replaceText(int arg1, int arg2, const char* arg3); // public abstract void javax.accessibility.AccessibleEditableText.replaceText(int,int,java.lang.String)
    virtual void selectText(int arg1, int arg2); // public abstract void javax.accessibility.AccessibleEditableText.selectText(int,int)
    virtual void setAttributes(int arg1, int arg2, javax::swing::text::AttributeSet* arg3); // public abstract void javax.accessibility.AccessibleEditableText.setAttributes(int,int,javax.swing.text.AttributeSet)
    virtual void setTextContents(const char* arg1); // public abstract void javax.accessibility.AccessibleEditableText.setTextContents(java.lang.String)

};
}
}
#endif
