#ifndef java_awt_color_ColorSpace_H
#define java_awt_color_ColorSpace_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace io {
class Serializable;
}
}

namespace java {
namespace lang {
class String;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace color {
class ColorSpace : public java::lang::Object {
  public:
    ColorSpace(JavaMarker* dummy);
    ColorSpace(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual const char* getName(int arg1); // public java.lang.String java.awt.color.ColorSpace.getName(int)
    virtual java::awt::color::ColorSpace* getInstance(int arg1); // public static java.awt.color.ColorSpace java.awt.color.ColorSpace.getInstance(int)
    virtual int getType(); // public int java.awt.color.ColorSpace.getType()
    virtual int getNumComponents(); // public int java.awt.color.ColorSpace.getNumComponents()
    virtual JavaFloatArray* fromCIEXYZ(JavaFloatArray* arg1); // public abstract float[] java.awt.color.ColorSpace.fromCIEXYZ(float[])
    virtual JavaFloatArray* fromRGB(JavaFloatArray* arg1); // public abstract float[] java.awt.color.ColorSpace.fromRGB(float[])
    virtual float getMaxValue(int arg1); // public float java.awt.color.ColorSpace.getMaxValue(int)
    virtual float getMinValue(int arg1); // public float java.awt.color.ColorSpace.getMinValue(int)
    virtual bool isCS_sRGB(); // public boolean java.awt.color.ColorSpace.isCS_sRGB()
    virtual JavaFloatArray* toCIEXYZ(JavaFloatArray* arg1); // public abstract float[] java.awt.color.ColorSpace.toCIEXYZ(float[])
    virtual JavaFloatArray* toRGB(JavaFloatArray* arg1); // public abstract float[] java.awt.color.ColorSpace.toRGB(float[])

    int TYPE_XYZ; // public static final int java.awt.color.ColorSpace.TYPE_XYZ
    int TYPE_Lab; // public static final int java.awt.color.ColorSpace.TYPE_Lab
    int TYPE_Luv; // public static final int java.awt.color.ColorSpace.TYPE_Luv
    int TYPE_YCbCr; // public static final int java.awt.color.ColorSpace.TYPE_YCbCr
    int TYPE_Yxy; // public static final int java.awt.color.ColorSpace.TYPE_Yxy
    int TYPE_RGB; // public static final int java.awt.color.ColorSpace.TYPE_RGB
    int TYPE_GRAY; // public static final int java.awt.color.ColorSpace.TYPE_GRAY
    int TYPE_HSV; // public static final int java.awt.color.ColorSpace.TYPE_HSV
    int TYPE_HLS; // public static final int java.awt.color.ColorSpace.TYPE_HLS
    int TYPE_CMYK; // public static final int java.awt.color.ColorSpace.TYPE_CMYK
    int TYPE_CMY; // public static final int java.awt.color.ColorSpace.TYPE_CMY
    int TYPE_2CLR; // public static final int java.awt.color.ColorSpace.TYPE_2CLR
    int TYPE_3CLR; // public static final int java.awt.color.ColorSpace.TYPE_3CLR
    int TYPE_4CLR; // public static final int java.awt.color.ColorSpace.TYPE_4CLR
    int TYPE_5CLR; // public static final int java.awt.color.ColorSpace.TYPE_5CLR
    int TYPE_6CLR; // public static final int java.awt.color.ColorSpace.TYPE_6CLR
    int TYPE_7CLR; // public static final int java.awt.color.ColorSpace.TYPE_7CLR
    int TYPE_8CLR; // public static final int java.awt.color.ColorSpace.TYPE_8CLR
    int TYPE_9CLR; // public static final int java.awt.color.ColorSpace.TYPE_9CLR
    int TYPE_ACLR; // public static final int java.awt.color.ColorSpace.TYPE_ACLR
    int TYPE_BCLR; // public static final int java.awt.color.ColorSpace.TYPE_BCLR
    int TYPE_CCLR; // public static final int java.awt.color.ColorSpace.TYPE_CCLR
    int TYPE_DCLR; // public static final int java.awt.color.ColorSpace.TYPE_DCLR
    int TYPE_ECLR; // public static final int java.awt.color.ColorSpace.TYPE_ECLR
    int TYPE_FCLR; // public static final int java.awt.color.ColorSpace.TYPE_FCLR
    int CS_sRGB; // public static final int java.awt.color.ColorSpace.CS_sRGB
    int CS_LINEAR_RGB; // public static final int java.awt.color.ColorSpace.CS_LINEAR_RGB
    int CS_CIEXYZ; // public static final int java.awt.color.ColorSpace.CS_CIEXYZ
    int CS_PYCC; // public static final int java.awt.color.ColorSpace.CS_PYCC
    int CS_GRAY; // public static final int java.awt.color.ColorSpace.CS_GRAY
};
}
}
}
#endif
