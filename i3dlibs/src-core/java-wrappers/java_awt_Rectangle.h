#ifndef java_awt_Rectangle_H
#define java_awt_Rectangle_H
#include <jni.h>
#include <java_marker.h>
#include <java_awt_geom_Rectangle2D.h>

namespace java {
namespace awt {
class Shape;
}
}

namespace java {
namespace io {
class Serializable;
}
}

namespace java {
namespace awt {
class Point;
}
}

namespace java {
namespace awt {
class Dimension;
}
}

namespace java {
namespace lang {
class Object;
}
}

namespace java {
namespace lang {
class String;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
class Rectangle : public java::awt::geom::Rectangle2D {
  public:
    Rectangle(JavaMarker* dummy);
    Rectangle(jobject obj);
    Rectangle(); // public java.awt.Rectangle()
    Rectangle(java::awt::Rectangle* arg1); // public java.awt.Rectangle(java.awt.Rectangle)
    Rectangle(int arg1, int arg2, int arg3, int arg4); // public java.awt.Rectangle(int,int,int,int)
    Rectangle(int arg1, int arg2); // public java.awt.Rectangle(int,int)
    Rectangle(java::awt::Point* arg1, java::awt::Dimension* arg2); // public java.awt.Rectangle(java.awt.Point,java.awt.Dimension)
    Rectangle(java::awt::Point* arg1); // public java.awt.Rectangle(java.awt.Point)
    Rectangle(java::awt::Dimension* arg1); // public java.awt.Rectangle(java.awt.Dimension)

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual void add(int arg1, int arg2); // public void java.awt.Rectangle.add(int,int)
    virtual void add(java::awt::Point* arg1); // public void java.awt.Rectangle.add(java.awt.Point)
    virtual void add(java::awt::Rectangle* arg1); // public void java.awt.Rectangle.add(java.awt.Rectangle)
    virtual bool equals(java::lang::Object* arg1); // public boolean java.awt.Rectangle.equals(java.lang.Object)
    virtual const char* toString(); // public java.lang.String java.awt.Rectangle.toString()
    virtual bool contains(int arg1, int arg2, int arg3, int arg4); // public boolean java.awt.Rectangle.contains(int,int,int,int)
    virtual bool contains(java::awt::Rectangle* arg1); // public boolean java.awt.Rectangle.contains(java.awt.Rectangle)
    virtual bool contains(int arg1, int arg2); // public boolean java.awt.Rectangle.contains(int,int)
    virtual bool contains(java::awt::Point* arg1); // public boolean java.awt.Rectangle.contains(java.awt.Point)
    virtual bool isEmpty(); // public boolean java.awt.Rectangle.isEmpty()
    virtual java::awt::Point* getLocation(); // public java.awt.Point java.awt.Rectangle.getLocation()
    virtual java::awt::Dimension* getSize(); // public java.awt.Dimension java.awt.Rectangle.getSize()
    virtual void setSize(java::awt::Dimension* arg1); // public void java.awt.Rectangle.setSize(java.awt.Dimension)
    virtual void setSize(int arg1, int arg2); // public void java.awt.Rectangle.setSize(int,int)
    virtual void resize(int arg1, int arg2); // public void java.awt.Rectangle.resize(int,int)
    virtual void grow(int arg1, int arg2); // public void java.awt.Rectangle.grow(int,int)
    virtual bool intersects(java::awt::Rectangle* arg1); // public boolean java.awt.Rectangle.intersects(java.awt.Rectangle)
    virtual java::awt::Rectangle* intersection(java::awt::Rectangle* arg1); // public java.awt.Rectangle java.awt.Rectangle.intersection(java.awt.Rectangle)
    virtual java::awt::Rectangle* union_(java::awt::Rectangle* arg1); // public java.awt.Rectangle java.awt.Rectangle.union(java.awt.Rectangle)
    virtual double getHeight(); // public double java.awt.Rectangle.getHeight()
    virtual double getWidth(); // public double java.awt.Rectangle.getWidth()
    virtual void setRect(double arg1, double arg2, double arg3, double arg4); // public void java.awt.Rectangle.setRect(double,double,double,double)
    virtual java::awt::Rectangle* getBounds(); // public java.awt.Rectangle java.awt.Rectangle.getBounds()
    virtual java::awt::geom::Rectangle2D* createIntersection(java::awt::geom::Rectangle2D* arg1); // public java.awt.geom.Rectangle2D java.awt.Rectangle.createIntersection(java.awt.geom.Rectangle2D)
    virtual java::awt::geom::Rectangle2D* createUnion(java::awt::geom::Rectangle2D* arg1); // public java.awt.geom.Rectangle2D java.awt.Rectangle.createUnion(java.awt.geom.Rectangle2D)
    virtual java::awt::geom::Rectangle2D* getBounds2D(); // public java.awt.geom.Rectangle2D java.awt.Rectangle.getBounds2D()
    virtual double getX(); // public double java.awt.Rectangle.getX()
    virtual double getY(); // public double java.awt.Rectangle.getY()
    virtual bool inside(int arg1, int arg2); // public boolean java.awt.Rectangle.inside(int,int)
    virtual void move(int arg1, int arg2); // public void java.awt.Rectangle.move(int,int)
    virtual int outcode(double arg1, double arg2); // public int java.awt.Rectangle.outcode(double,double)
    virtual void reshape(int arg1, int arg2, int arg3, int arg4); // public void java.awt.Rectangle.reshape(int,int,int,int)
    virtual void setBounds(int arg1, int arg2, int arg3, int arg4); // public void java.awt.Rectangle.setBounds(int,int,int,int)
    virtual void setBounds(java::awt::Rectangle* arg1); // public void java.awt.Rectangle.setBounds(java.awt.Rectangle)
    virtual void setLocation(int arg1, int arg2); // public void java.awt.Rectangle.setLocation(int,int)
    virtual void setLocation(java::awt::Point* arg1); // public void java.awt.Rectangle.setLocation(java.awt.Point)
    virtual void translate(int arg1, int arg2); // public void java.awt.Rectangle.translate(int,int)

    int x; // public int java.awt.Rectangle.x
    int y; // public int java.awt.Rectangle.y
    int width; // public int java.awt.Rectangle.width
    int height; // public int java.awt.Rectangle.height
};
}
}
#endif
