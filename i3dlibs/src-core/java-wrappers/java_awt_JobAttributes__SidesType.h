#ifndef java_awt_JobAttributes__SidesType_H
#define java_awt_JobAttributes__SidesType_H
#include <jni.h>
#include <java_marker.h>
#include <java_awt_AttributeValue.h>

namespace java {
namespace lang {
class String;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
class JobAttributes__SidesType : public java::awt::AttributeValue {
  public:
    JobAttributes__SidesType(JavaMarker* dummy);
    JobAttributes__SidesType(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual int hashCode(); // public int java.awt.JobAttributes$SidesType.hashCode()
    virtual const char* toString(); // public java.lang.String java.awt.JobAttributes$SidesType.toString()

    java::awt::JobAttributes__SidesType* ONE_SIDED; // public static final java.awt.JobAttributes$SidesType java.awt.JobAttributes$SidesType.ONE_SIDED
    java::awt::JobAttributes__SidesType* TWO_SIDED_LONG_EDGE; // public static final java.awt.JobAttributes$SidesType java.awt.JobAttributes$SidesType.TWO_SIDED_LONG_EDGE
    java::awt::JobAttributes__SidesType* TWO_SIDED_SHORT_EDGE; // public static final java.awt.JobAttributes$SidesType java.awt.JobAttributes$SidesType.TWO_SIDED_SHORT_EDGE
};
}
}
#endif
