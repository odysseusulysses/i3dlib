#ifndef java_awt_image_ImageConsumer_H
#define java_awt_image_ImageConsumer_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace util {
class Hashtable;
}
}

namespace java {
namespace awt {
namespace image {
class ColorModel;
}
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace image {
class ImageConsumer : public java::lang::Object {
  public:
    ImageConsumer(JavaMarker* dummy);
    ImageConsumer(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual void setProperties(java::util::Hashtable* arg1); // public abstract void java.awt.image.ImageConsumer.setProperties(java.util.Hashtable)
    virtual void setPixels(int arg1, int arg2, int arg3, int arg4, java::awt::image::ColorModel* arg5, JavaByteArray* arg6, int arg7, int arg8); // public abstract void java.awt.image.ImageConsumer.setPixels(int,int,int,int,java.awt.image.ColorModel,byte[],int,int)
    virtual void setPixels(int arg1, int arg2, int arg3, int arg4, java::awt::image::ColorModel* arg5, JavaIntArray* arg6, int arg7, int arg8); // public abstract void java.awt.image.ImageConsumer.setPixels(int,int,int,int,java.awt.image.ColorModel,int[],int,int)
    virtual void imageComplete(int arg1); // public abstract void java.awt.image.ImageConsumer.imageComplete(int)
    virtual void setColorModel(java::awt::image::ColorModel* arg1); // public abstract void java.awt.image.ImageConsumer.setColorModel(java.awt.image.ColorModel)
    virtual void setDimensions(int arg1, int arg2); // public abstract void java.awt.image.ImageConsumer.setDimensions(int,int)
    virtual void setHints(int arg1); // public abstract void java.awt.image.ImageConsumer.setHints(int)

    int RANDOMPIXELORDER; // public static final int java.awt.image.ImageConsumer.RANDOMPIXELORDER
    int TOPDOWNLEFTRIGHT; // public static final int java.awt.image.ImageConsumer.TOPDOWNLEFTRIGHT
    int COMPLETESCANLINES; // public static final int java.awt.image.ImageConsumer.COMPLETESCANLINES
    int SINGLEPASS; // public static final int java.awt.image.ImageConsumer.SINGLEPASS
    int SINGLEFRAME; // public static final int java.awt.image.ImageConsumer.SINGLEFRAME
    int IMAGEERROR; // public static final int java.awt.image.ImageConsumer.IMAGEERROR
    int SINGLEFRAMEDONE; // public static final int java.awt.image.ImageConsumer.SINGLEFRAMEDONE
    int STATICIMAGEDONE; // public static final int java.awt.image.ImageConsumer.STATICIMAGEDONE
    int IMAGEABORTED; // public static final int java.awt.image.ImageConsumer.IMAGEABORTED
};
}
}
}
#endif
