#ifndef java_awt_event_MouseWheelListener_H
#define java_awt_event_MouseWheelListener_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace util {
class EventListener;
}
}

namespace java {
namespace awt {
namespace event {
class MouseWheelEvent;
}
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace event {
class MouseWheelListener : public java::lang::Object {
  public:
    MouseWheelListener(JavaMarker* dummy);
    MouseWheelListener(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual void mouseWheelMoved(java::awt::event::MouseWheelEvent* arg1); // public abstract void java.awt.event.MouseWheelListener.mouseWheelMoved(java.awt.event.MouseWheelEvent)

};
}
}
}
#endif
