#ifndef java_awt_PageAttributes_H
#define java_awt_PageAttributes_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace lang {
class Cloneable;
}
}

namespace java {
namespace awt {
class PageAttributes__ColorType;
}
}

namespace java {
namespace awt {
class PageAttributes__MediaType;
}
}

namespace java {
namespace awt {
class PageAttributes__OrientationRequestedType;
}
}

namespace java {
namespace awt {
class PageAttributes__OriginType;
}
}

namespace java {
namespace awt {
class PageAttributes__PrintQualityType;
}
}

namespace java {
namespace lang {
class String;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
class PageAttributes : public java::lang::Object {
  public:
    PageAttributes(JavaMarker* dummy);
    PageAttributes(jobject obj);
    PageAttributes(java::awt::PageAttributes__ColorType* arg1, java::awt::PageAttributes__MediaType* arg2, java::awt::PageAttributes__OrientationRequestedType* arg3, java::awt::PageAttributes__OriginType* arg4, java::awt::PageAttributes__PrintQualityType* arg5, JavaIntArray* arg6); // public java.awt.PageAttributes(java.awt.PageAttributes$ColorType,java.awt.PageAttributes$MediaType,java.awt.PageAttributes$OrientationRequestedType,java.awt.PageAttributes$OriginType,java.awt.PageAttributes$PrintQualityType,int[])
    PageAttributes(java::awt::PageAttributes* arg1); // public java.awt.PageAttributes(java.awt.PageAttributes)
    PageAttributes(); // public java.awt.PageAttributes()

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual int hashCode(); // public int java.awt.PageAttributes.hashCode()
    virtual java::lang::Object* clone(); // public java.lang.Object java.awt.PageAttributes.clone()
    virtual bool equals(java::lang::Object* arg1); // public boolean java.awt.PageAttributes.equals(java.lang.Object)
    virtual const char* toString(); // public java.lang.String java.awt.PageAttributes.toString()
    virtual void set(java::awt::PageAttributes* arg1); // public void java.awt.PageAttributes.set(java.awt.PageAttributes)
    virtual java::awt::PageAttributes__ColorType* getColor(); // public java.awt.PageAttributes$ColorType java.awt.PageAttributes.getColor()
    virtual void setColor(java::awt::PageAttributes__ColorType* arg1); // public void java.awt.PageAttributes.setColor(java.awt.PageAttributes$ColorType)
    virtual java::awt::PageAttributes__MediaType* getMedia(); // public java.awt.PageAttributes$MediaType java.awt.PageAttributes.getMedia()
    virtual java::awt::PageAttributes__OrientationRequestedType* getOrientationRequested(); // public java.awt.PageAttributes$OrientationRequestedType java.awt.PageAttributes.getOrientationRequested()
    virtual java::awt::PageAttributes__OriginType* getOrigin(); // public java.awt.PageAttributes$OriginType java.awt.PageAttributes.getOrigin()
    virtual java::awt::PageAttributes__PrintQualityType* getPrintQuality(); // public java.awt.PageAttributes$PrintQualityType java.awt.PageAttributes.getPrintQuality()
    virtual JavaIntArray* getPrinterResolution(); // public int[] java.awt.PageAttributes.getPrinterResolution()
    virtual void setMedia(java::awt::PageAttributes__MediaType* arg1); // public void java.awt.PageAttributes.setMedia(java.awt.PageAttributes$MediaType)
    virtual void setMediaToDefault(); // public void java.awt.PageAttributes.setMediaToDefault()
    virtual void setOrientationRequested(int arg1); // public void java.awt.PageAttributes.setOrientationRequested(int)
    virtual void setOrientationRequested(java::awt::PageAttributes__OrientationRequestedType* arg1); // public void java.awt.PageAttributes.setOrientationRequested(java.awt.PageAttributes$OrientationRequestedType)
    virtual void setOrientationRequestedToDefault(); // public void java.awt.PageAttributes.setOrientationRequestedToDefault()
    virtual void setOrigin(java::awt::PageAttributes__OriginType* arg1); // public void java.awt.PageAttributes.setOrigin(java.awt.PageAttributes$OriginType)
    virtual void setPrintQuality(int arg1); // public void java.awt.PageAttributes.setPrintQuality(int)
    virtual void setPrintQuality(java::awt::PageAttributes__PrintQualityType* arg1); // public void java.awt.PageAttributes.setPrintQuality(java.awt.PageAttributes$PrintQualityType)
    virtual void setPrintQualityToDefault(); // public void java.awt.PageAttributes.setPrintQualityToDefault()
    virtual void setPrinterResolution(int arg1); // public void java.awt.PageAttributes.setPrinterResolution(int)
    virtual void setPrinterResolution(JavaIntArray* arg1); // public void java.awt.PageAttributes.setPrinterResolution(int[])
    virtual void setPrinterResolutionToDefault(); // public void java.awt.PageAttributes.setPrinterResolutionToDefault()

};
}
}
#endif
