#ifndef java_awt_event_HierarchyEvent_H
#define java_awt_event_HierarchyEvent_H
#include <jni.h>
#include <java_marker.h>
#include <java_awt_AWTEvent.h>

namespace java {
namespace awt {
class Component;
}
}

namespace java {
namespace awt {
class Container;
}
}

namespace java {
namespace lang {
class String;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace event {
class HierarchyEvent : public java::awt::AWTEvent {
  public:
    HierarchyEvent(JavaMarker* dummy);
    HierarchyEvent(jobject obj);
    HierarchyEvent(java::awt::Component* arg1, int arg2, java::awt::Component* arg3, java::awt::Container* arg4); // public java.awt.event.HierarchyEvent(java.awt.Component,int,java.awt.Component,java.awt.Container)
    HierarchyEvent(java::awt::Component* arg1, int arg2, java::awt::Component* arg3, java::awt::Container* arg4, long arg5); // public java.awt.event.HierarchyEvent(java.awt.Component,int,java.awt.Component,java.awt.Container,long)

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual java::awt::Component* getComponent(); // public java.awt.Component java.awt.event.HierarchyEvent.getComponent()
    virtual const char* paramString(); // public java.lang.String java.awt.event.HierarchyEvent.paramString()
    virtual long getChangeFlags(); // public long java.awt.event.HierarchyEvent.getChangeFlags()
    virtual java::awt::Component* getChanged(); // public java.awt.Component java.awt.event.HierarchyEvent.getChanged()
    virtual java::awt::Container* getChangedParent(); // public java.awt.Container java.awt.event.HierarchyEvent.getChangedParent()

    int HIERARCHY_FIRST; // public static final int java.awt.event.HierarchyEvent.HIERARCHY_FIRST
    int HIERARCHY_CHANGED; // public static final int java.awt.event.HierarchyEvent.HIERARCHY_CHANGED
    int ANCESTOR_MOVED; // public static final int java.awt.event.HierarchyEvent.ANCESTOR_MOVED
    int ANCESTOR_RESIZED; // public static final int java.awt.event.HierarchyEvent.ANCESTOR_RESIZED
    int HIERARCHY_LAST; // public static final int java.awt.event.HierarchyEvent.HIERARCHY_LAST
    int PARENT_CHANGED; // public static final int java.awt.event.HierarchyEvent.PARENT_CHANGED
    int DISPLAYABILITY_CHANGED; // public static final int java.awt.event.HierarchyEvent.DISPLAYABILITY_CHANGED
    int SHOWING_CHANGED; // public static final int java.awt.event.HierarchyEvent.SHOWING_CHANGED
};
}
}
}
#endif
