#ifndef java_awt_GraphicsDevice_H
#define java_awt_GraphicsDevice_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace awt {
class GraphicsConfiguration;
}
}

namespace java {
namespace awt {
class GraphicsConfigTemplate;
}
}

namespace java {
namespace awt {
class DisplayMode;
}
}

namespace java {
namespace awt {
class Window;
}
}

namespace java {
namespace lang {
class String;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
class GraphicsDevice : public java::lang::Object {
  public:
    GraphicsDevice(JavaMarker* dummy);
    GraphicsDevice(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual int getType(); // public abstract int java.awt.GraphicsDevice.getType()
    virtual int getAvailableAcceleratedMemory(); // public int java.awt.GraphicsDevice.getAvailableAcceleratedMemory()
    virtual java::awt::GraphicsConfiguration* getBestConfiguration(java::awt::GraphicsConfigTemplate* arg1); // public java.awt.GraphicsConfiguration java.awt.GraphicsDevice.getBestConfiguration(java.awt.GraphicsConfigTemplate)
    virtual JavaObjectArray* getConfigurations(); // public abstract java.awt.GraphicsConfiguration[] java.awt.GraphicsDevice.getConfigurations()
    virtual java::awt::GraphicsConfiguration* getDefaultConfiguration(); // public abstract java.awt.GraphicsConfiguration java.awt.GraphicsDevice.getDefaultConfiguration()
    virtual java::awt::DisplayMode* getDisplayMode(); // public java.awt.DisplayMode java.awt.GraphicsDevice.getDisplayMode()
    virtual JavaObjectArray* getDisplayModes(); // public java.awt.DisplayMode[] java.awt.GraphicsDevice.getDisplayModes()
    virtual java::awt::Window* getFullScreenWindow(); // public java.awt.Window java.awt.GraphicsDevice.getFullScreenWindow()
    virtual const char* getIDstring(); // public abstract java.lang.String java.awt.GraphicsDevice.getIDstring()
    virtual bool isDisplayChangeSupported(); // public boolean java.awt.GraphicsDevice.isDisplayChangeSupported()
    virtual bool isFullScreenSupported(); // public boolean java.awt.GraphicsDevice.isFullScreenSupported()
    virtual void setDisplayMode(java::awt::DisplayMode* arg1); // public void java.awt.GraphicsDevice.setDisplayMode(java.awt.DisplayMode)
    virtual void setFullScreenWindow(java::awt::Window* arg1); // public void java.awt.GraphicsDevice.setFullScreenWindow(java.awt.Window)

    int TYPE_RASTER_SCREEN; // public static final int java.awt.GraphicsDevice.TYPE_RASTER_SCREEN
    int TYPE_PRINTER; // public static final int java.awt.GraphicsDevice.TYPE_PRINTER
    int TYPE_IMAGE_BUFFER; // public static final int java.awt.GraphicsDevice.TYPE_IMAGE_BUFFER
};
}
}
#endif
