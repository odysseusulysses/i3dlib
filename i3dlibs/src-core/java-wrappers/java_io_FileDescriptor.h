#ifndef java_io_FileDescriptor_H
#define java_io_FileDescriptor_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace io {
class FileDescriptor : public java::lang::Object {
  public:
    FileDescriptor(JavaMarker* dummy);
    FileDescriptor(jobject obj);
    FileDescriptor(); // public java.io.FileDescriptor()

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual void sync(); // public native void java.io.FileDescriptor.sync() throws java.io.SyncFailedException
    virtual bool valid(); // public boolean java.io.FileDescriptor.valid()

    java::io::FileDescriptor* in; // public static final java.io.FileDescriptor java.io.FileDescriptor.in
    java::io::FileDescriptor* out; // public static final java.io.FileDescriptor java.io.FileDescriptor.out
    java::io::FileDescriptor* err; // public static final java.io.FileDescriptor java.io.FileDescriptor.err
};
}
}
#endif
