#ifndef java_awt_dnd_peer_DropTargetContextPeer_H
#define java_awt_dnd_peer_DropTargetContextPeer_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace awt {
namespace dnd {
class DropTarget;
}
}
}

namespace java {
namespace awt {
namespace datatransfer {
class DataFlavor;
}
}
}

namespace java {
namespace awt {
namespace datatransfer {
class Transferable;
}
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace dnd {
namespace peer {
class DropTargetContextPeer : public java::lang::Object {
  public:
    DropTargetContextPeer(JavaMarker* dummy);
    DropTargetContextPeer(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual java::awt::dnd::DropTarget* getDropTarget(); // public abstract java.awt.dnd.DropTarget java.awt.dnd.peer.DropTargetContextPeer.getDropTarget()
    virtual void rejectDrop(); // public abstract void java.awt.dnd.peer.DropTargetContextPeer.rejectDrop()
    virtual void setTargetActions(int arg1); // public abstract void java.awt.dnd.peer.DropTargetContextPeer.setTargetActions(int)
    virtual JavaObjectArray* getTransferDataFlavors(); // public abstract java.awt.datatransfer.DataFlavor[] java.awt.dnd.peer.DropTargetContextPeer.getTransferDataFlavors()
    virtual void acceptDrag(int arg1); // public abstract void java.awt.dnd.peer.DropTargetContextPeer.acceptDrag(int)
    virtual void acceptDrop(int arg1); // public abstract void java.awt.dnd.peer.DropTargetContextPeer.acceptDrop(int)
    virtual void dropComplete(bool arg1); // public abstract void java.awt.dnd.peer.DropTargetContextPeer.dropComplete(boolean)
    virtual int getTargetActions(); // public abstract int java.awt.dnd.peer.DropTargetContextPeer.getTargetActions()
    virtual java::awt::datatransfer::Transferable* getTransferable(); // public abstract java.awt.datatransfer.Transferable java.awt.dnd.peer.DropTargetContextPeer.getTransferable() throws java.awt.dnd.InvalidDnDOperationException
    virtual bool isTransferableJVMLocal(); // public abstract boolean java.awt.dnd.peer.DropTargetContextPeer.isTransferableJVMLocal()
    virtual void rejectDrag(); // public abstract void java.awt.dnd.peer.DropTargetContextPeer.rejectDrag()

};
}
}
}
}
#endif
