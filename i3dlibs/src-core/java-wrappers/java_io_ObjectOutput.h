#ifndef java_io_ObjectOutput_H
#define java_io_ObjectOutput_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace io {
class DataOutput;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace io {
class ObjectOutput : public java::lang::Object {
  public:
    ObjectOutput(JavaMarker* dummy);
    ObjectOutput(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual void write(int arg1); // public abstract void java.io.ObjectOutput.write(int) throws java.io.IOException
    virtual void write(JavaByteArray* arg1); // public abstract void java.io.ObjectOutput.write(byte[]) throws java.io.IOException
    virtual void write(JavaByteArray* arg1, int arg2, int arg3); // public abstract void java.io.ObjectOutput.write(byte[],int,int) throws java.io.IOException
    virtual void writeObject(java::lang::Object* arg1); // public abstract void java.io.ObjectOutput.writeObject(java.lang.Object) throws java.io.IOException
    virtual void close(); // public abstract void java.io.ObjectOutput.close() throws java.io.IOException
    virtual void flush(); // public abstract void java.io.ObjectOutput.flush() throws java.io.IOException

};
}
}
#endif
