#ifndef javax_accessibility_AccessibleBundle_H
#define javax_accessibility_AccessibleBundle_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace lang {
class String;
}
}

namespace java {
namespace util {
class Locale;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace javax {
namespace accessibility {
class AccessibleBundle : public java::lang::Object {
  public:
    AccessibleBundle(JavaMarker* dummy);
    AccessibleBundle(jobject obj);
    AccessibleBundle(); // public javax.accessibility.AccessibleBundle()

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual const char* toString(); // public java.lang.String javax.accessibility.AccessibleBundle.toString()
    virtual const char* toDisplayString(java::util::Locale* arg1); // public java.lang.String javax.accessibility.AccessibleBundle.toDisplayString(java.util.Locale)
    virtual const char* toDisplayString(); // public java.lang.String javax.accessibility.AccessibleBundle.toDisplayString()

};
}
}
#endif
