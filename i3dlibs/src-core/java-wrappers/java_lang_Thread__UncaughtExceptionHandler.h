#ifndef java_lang_Thread__UncaughtExceptionHandler_H
#define java_lang_Thread__UncaughtExceptionHandler_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace lang {
class Thread;
}
}

namespace java {
namespace lang {
class Throwable;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace lang {
class Thread__UncaughtExceptionHandler : public java::lang::Object {
  public:
    Thread__UncaughtExceptionHandler(JavaMarker* dummy);
    Thread__UncaughtExceptionHandler(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual void uncaughtException(java::lang::Thread* arg1, java::lang::Throwable* arg2); // public abstract void java.lang.Thread$UncaughtExceptionHandler.uncaughtException(java.lang.Thread,java.lang.Throwable)

};
}
}
#endif
