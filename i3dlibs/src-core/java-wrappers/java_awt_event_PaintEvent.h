#ifndef java_awt_event_PaintEvent_H
#define java_awt_event_PaintEvent_H
#include <jni.h>
#include <java_marker.h>
#include <java_awt_event_ComponentEvent.h>

namespace java {
namespace awt {
class Component;
}
}

namespace java {
namespace awt {
class Rectangle;
}
}

namespace java {
namespace lang {
class String;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace event {
class PaintEvent : public java::awt::event::ComponentEvent {
  public:
    PaintEvent(JavaMarker* dummy);
    PaintEvent(jobject obj);
    PaintEvent(java::awt::Component* arg1, int arg2, java::awt::Rectangle* arg3); // public java.awt.event.PaintEvent(java.awt.Component,int,java.awt.Rectangle)

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual const char* paramString(); // public java.lang.String java.awt.event.PaintEvent.paramString()
    virtual java::awt::Rectangle* getUpdateRect(); // public java.awt.Rectangle java.awt.event.PaintEvent.getUpdateRect()
    virtual void setUpdateRect(java::awt::Rectangle* arg1); // public void java.awt.event.PaintEvent.setUpdateRect(java.awt.Rectangle)

    int PAINT_FIRST; // public static final int java.awt.event.PaintEvent.PAINT_FIRST
    int PAINT_LAST; // public static final int java.awt.event.PaintEvent.PAINT_LAST
    int PAINT; // public static final int java.awt.event.PaintEvent.PAINT
    int UPDATE; // public static final int java.awt.event.PaintEvent.UPDATE
};
}
}
}
#endif
