#ifndef java_awt_event_MouseWheelEvent_H
#define java_awt_event_MouseWheelEvent_H
#include <jni.h>
#include <java_marker.h>
#include <java_awt_event_MouseEvent.h>

namespace java {
namespace awt {
class Component;
}
}

namespace java {
namespace lang {
class String;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace event {
class MouseWheelEvent : public java::awt::event::MouseEvent {
  public:
    MouseWheelEvent(JavaMarker* dummy);
    MouseWheelEvent(jobject obj);
    MouseWheelEvent(java::awt::Component* arg1, int arg2, long arg3, int arg4, int arg5, int arg6, int arg7, int arg8, int arg9, bool arg10, int arg11, int arg12, int arg13); // public java.awt.event.MouseWheelEvent(java.awt.Component,int,long,int,int,int,int,int,int,boolean,int,int,int)
    MouseWheelEvent(java::awt::Component* arg1, int arg2, long arg3, int arg4, int arg5, int arg6, int arg7, bool arg8, int arg9, int arg10, int arg11); // public java.awt.event.MouseWheelEvent(java.awt.Component,int,long,int,int,int,int,boolean,int,int,int)

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual const char* paramString(); // public java.lang.String java.awt.event.MouseWheelEvent.paramString()
    virtual int getScrollAmount(); // public int java.awt.event.MouseWheelEvent.getScrollAmount()
    virtual int getScrollType(); // public int java.awt.event.MouseWheelEvent.getScrollType()
    virtual int getWheelRotation(); // public int java.awt.event.MouseWheelEvent.getWheelRotation()
    virtual int getUnitsToScroll(); // public int java.awt.event.MouseWheelEvent.getUnitsToScroll()

    int WHEEL_UNIT_SCROLL; // public static final int java.awt.event.MouseWheelEvent.WHEEL_UNIT_SCROLL
    int WHEEL_BLOCK_SCROLL; // public static final int java.awt.event.MouseWheelEvent.WHEEL_BLOCK_SCROLL
};
}
}
}
#endif
