#ifndef java_util_AbstractCollection_H
#define java_util_AbstractCollection_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace util {
class Collection;
}
}

namespace java {
namespace lang {
class String;
}
}

namespace java {
namespace util {
class Iterator;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace util {
class AbstractCollection : public java::lang::Object {
  public:
    AbstractCollection(JavaMarker* dummy);
    AbstractCollection(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual bool add(java::lang::Object* arg1); // public boolean java.util.AbstractCollection.add(java.lang.Object)
    virtual void clear(); // public void java.util.AbstractCollection.clear()
    virtual const char* toString(); // public java.lang.String java.util.AbstractCollection.toString()
    virtual bool contains(java::lang::Object* arg1); // public boolean java.util.AbstractCollection.contains(java.lang.Object)
    virtual bool isEmpty(); // public boolean java.util.AbstractCollection.isEmpty()
    virtual bool addAll(java::util::Collection* arg1); // public boolean java.util.AbstractCollection.addAll(java.util.Collection)
    virtual java::util::Iterator* iterator(); // public abstract java.util.Iterator java.util.AbstractCollection.iterator()
    virtual int size(); // public abstract int java.util.AbstractCollection.size()
    virtual JavaObjectArray* toArray(); // public java.lang.Object[] java.util.AbstractCollection.toArray()
    virtual JavaObjectArray* toArray(JavaObjectArray* arg1); // public java.lang.Object[] java.util.AbstractCollection.toArray(java.lang.Object[])
    virtual bool remove(java::lang::Object* arg1); // public boolean java.util.AbstractCollection.remove(java.lang.Object)
    virtual bool containsAll(java::util::Collection* arg1); // public boolean java.util.AbstractCollection.containsAll(java.util.Collection)
    virtual bool removeAll(java::util::Collection* arg1); // public boolean java.util.AbstractCollection.removeAll(java.util.Collection)
    virtual bool retainAll(java::util::Collection* arg1); // public boolean java.util.AbstractCollection.retainAll(java.util.Collection)

};
}
}
#endif
