#ifndef java_awt_event_MouseEvent_H
#define java_awt_event_MouseEvent_H
#include <jni.h>
#include <java_marker.h>
#include <java_awt_event_InputEvent.h>

namespace java {
namespace awt {
class Component;
}
}

namespace java {
namespace awt {
class Point;
}
}

namespace java {
namespace lang {
class String;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace event {
class MouseEvent : public java::awt::event::InputEvent {
  public:
    MouseEvent(JavaMarker* dummy);
    MouseEvent(jobject obj);
    MouseEvent(java::awt::Component* arg1, int arg2, long arg3, int arg4, int arg5, int arg6, int arg7, bool arg8, int arg9); // public java.awt.event.MouseEvent(java.awt.Component,int,long,int,int,int,int,boolean,int)
    MouseEvent(java::awt::Component* arg1, int arg2, long arg3, int arg4, int arg5, int arg6, int arg7, bool arg8); // public java.awt.event.MouseEvent(java.awt.Component,int,long,int,int,int,int,boolean)
    MouseEvent(java::awt::Component* arg1, int arg2, long arg3, int arg4, int arg5, int arg6, int arg7, int arg8, int arg9, bool arg10, int arg11); // public java.awt.event.MouseEvent(java.awt.Component,int,long,int,int,int,int,int,int,boolean,int)

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual int getX(); // public int java.awt.event.MouseEvent.getX()
    virtual int getY(); // public int java.awt.event.MouseEvent.getY()
    virtual java::awt::Point* getLocationOnScreen(); // public java.awt.Point java.awt.event.MouseEvent.getLocationOnScreen()
    virtual const char* paramString(); // public java.lang.String java.awt.event.MouseEvent.paramString()
    virtual int getClickCount(); // public int java.awt.event.MouseEvent.getClickCount()
    virtual int getXOnScreen(); // public int java.awt.event.MouseEvent.getXOnScreen()
    virtual int getYOnScreen(); // public int java.awt.event.MouseEvent.getYOnScreen()
    virtual bool isPopupTrigger(); // public boolean java.awt.event.MouseEvent.isPopupTrigger()
    virtual int getButton(); // public int java.awt.event.MouseEvent.getButton()
    virtual const char* getMouseModifiersText(int arg1); // public static java.lang.String java.awt.event.MouseEvent.getMouseModifiersText(int)
    virtual java::awt::Point* getPoint(); // public java.awt.Point java.awt.event.MouseEvent.getPoint()
    virtual void translatePoint(int arg1, int arg2); // public synchronized void java.awt.event.MouseEvent.translatePoint(int,int)

    int MOUSE_FIRST; // public static final int java.awt.event.MouseEvent.MOUSE_FIRST
    int MOUSE_LAST; // public static final int java.awt.event.MouseEvent.MOUSE_LAST
    int MOUSE_CLICKED; // public static final int java.awt.event.MouseEvent.MOUSE_CLICKED
    int MOUSE_PRESSED; // public static final int java.awt.event.MouseEvent.MOUSE_PRESSED
    int MOUSE_RELEASED; // public static final int java.awt.event.MouseEvent.MOUSE_RELEASED
    int MOUSE_MOVED; // public static final int java.awt.event.MouseEvent.MOUSE_MOVED
    int MOUSE_ENTERED; // public static final int java.awt.event.MouseEvent.MOUSE_ENTERED
    int MOUSE_EXITED; // public static final int java.awt.event.MouseEvent.MOUSE_EXITED
    int MOUSE_DRAGGED; // public static final int java.awt.event.MouseEvent.MOUSE_DRAGGED
    int MOUSE_WHEEL; // public static final int java.awt.event.MouseEvent.MOUSE_WHEEL
    int NOBUTTON; // public static final int java.awt.event.MouseEvent.NOBUTTON
    int BUTTON1; // public static final int java.awt.event.MouseEvent.BUTTON1
    int BUTTON2; // public static final int java.awt.event.MouseEvent.BUTTON2
    int BUTTON3; // public static final int java.awt.event.MouseEvent.BUTTON3
};
}
}
}
#endif
