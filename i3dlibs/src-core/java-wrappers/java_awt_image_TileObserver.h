#ifndef java_awt_image_TileObserver_H
#define java_awt_image_TileObserver_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace awt {
namespace image {
class WritableRenderedImage;
}
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace image {
class TileObserver : public java::lang::Object {
  public:
    TileObserver(JavaMarker* dummy);
    TileObserver(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual void tileUpdate(java::awt::image::WritableRenderedImage* arg1, int arg2, int arg3, bool arg4); // public abstract void java.awt.image.TileObserver.tileUpdate(java.awt.image.WritableRenderedImage,int,int,boolean)

};
}
}
}
#endif
