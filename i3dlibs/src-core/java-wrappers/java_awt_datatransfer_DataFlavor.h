#ifndef java_awt_datatransfer_DataFlavor_H
#define java_awt_datatransfer_DataFlavor_H
#include <jni.h>
#include <java_marker.h>
#include <java_lang_Object.h>

namespace java {
namespace io {
class Externalizable;
}
}

namespace java {
namespace lang {
class Cloneable;
}
}

namespace java {
namespace lang {
class String;
}
}

namespace java {
namespace lang {
class ClassLoader;
}
}

namespace java {
namespace lang {
class Class;
}
}

namespace java {
namespace io {
class ObjectOutput;
}
}

namespace java {
namespace io {
class ObjectInput;
}
}

namespace java {
namespace io {
class Reader;
}
}

namespace java {
namespace awt {
namespace datatransfer {
class Transferable;
}
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
namespace datatransfer {
class DataFlavor : public java::lang::Object {
  public:
    DataFlavor(JavaMarker* dummy);
    DataFlavor(jobject obj);
    DataFlavor(const char* arg1, const char* arg2, java::lang::ClassLoader* arg3); // public java.awt.datatransfer.DataFlavor(java.lang.String,java.lang.String,java.lang.ClassLoader) throws java.lang.ClassNotFoundException
    DataFlavor(); // public java.awt.datatransfer.DataFlavor()
    DataFlavor(const char* arg1); // public java.awt.datatransfer.DataFlavor(java.lang.String) throws java.lang.ClassNotFoundException
    DataFlavor(java::lang::Class* arg1, const char* arg2); // public java.awt.datatransfer.DataFlavor(java.lang.Class,java.lang.String)
    DataFlavor(const char* arg1, const char* arg2); // public java.awt.datatransfer.DataFlavor(java.lang.String,java.lang.String)

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual int hashCode(); // public int java.awt.datatransfer.DataFlavor.hashCode()
    virtual java::lang::Object* clone(); // public java.lang.Object java.awt.datatransfer.DataFlavor.clone() throws java.lang.CloneNotSupportedException
    virtual bool equals(const char* arg1); // public boolean java.awt.datatransfer.DataFlavor.equals(java.lang.String)
    virtual bool equals(java::awt::datatransfer::DataFlavor* arg1); // public boolean java.awt.datatransfer.DataFlavor.equals(java.awt.datatransfer.DataFlavor)
    virtual bool equals(java::lang::Object* arg1); // public boolean java.awt.datatransfer.DataFlavor.equals(java.lang.Object)
    virtual const char* toString(); // public java.lang.String java.awt.datatransfer.DataFlavor.toString()
    virtual bool match(java::awt::datatransfer::DataFlavor* arg1); // public boolean java.awt.datatransfer.DataFlavor.match(java.awt.datatransfer.DataFlavor)
    virtual void writeExternal(java::io::ObjectOutput* arg1); // public synchronized void java.awt.datatransfer.DataFlavor.writeExternal(java.io.ObjectOutput) throws java.io.IOException
    virtual void readExternal(java::io::ObjectInput* arg1); // public synchronized void java.awt.datatransfer.DataFlavor.readExternal(java.io.ObjectInput) throws java.io.IOException,java.lang.ClassNotFoundException
    virtual java::lang::Class* getDefaultRepresentationClass(); // public final java.lang.Class java.awt.datatransfer.DataFlavor.getDefaultRepresentationClass()
    virtual const char* getDefaultRepresentationClassAsString(); // public final java.lang.String java.awt.datatransfer.DataFlavor.getDefaultRepresentationClassAsString()
    virtual const char* getHumanPresentableName(); // public java.lang.String java.awt.datatransfer.DataFlavor.getHumanPresentableName()
    virtual const char* getMimeType(); // public java.lang.String java.awt.datatransfer.DataFlavor.getMimeType()
    virtual const char* getParameter(const char* arg1); // public java.lang.String java.awt.datatransfer.DataFlavor.getParameter(java.lang.String)
    virtual const char* getPrimaryType(); // public java.lang.String java.awt.datatransfer.DataFlavor.getPrimaryType()
    virtual java::io::Reader* getReaderForText(java::awt::datatransfer::Transferable* arg1); // public java.io.Reader java.awt.datatransfer.DataFlavor.getReaderForText(java.awt.datatransfer.Transferable) throws java.awt.datatransfer.UnsupportedFlavorException,java.io.IOException
    virtual java::lang::Class* getRepresentationClass(); // public java.lang.Class java.awt.datatransfer.DataFlavor.getRepresentationClass()
    virtual const char* getSubType(); // public java.lang.String java.awt.datatransfer.DataFlavor.getSubType()
    virtual java::awt::datatransfer::DataFlavor* getTextPlainUnicodeFlavor(); // public static final java.awt.datatransfer.DataFlavor java.awt.datatransfer.DataFlavor.getTextPlainUnicodeFlavor()
    virtual bool isFlavorJavaFileListType(); // public boolean java.awt.datatransfer.DataFlavor.isFlavorJavaFileListType()
    virtual bool isFlavorRemoteObjectType(); // public boolean java.awt.datatransfer.DataFlavor.isFlavorRemoteObjectType()
    virtual bool isFlavorSerializedObjectType(); // public boolean java.awt.datatransfer.DataFlavor.isFlavorSerializedObjectType()
    virtual bool isFlavorTextType(); // public boolean java.awt.datatransfer.DataFlavor.isFlavorTextType()
    virtual bool isMimeTypeEqual(const char* arg1); // public boolean java.awt.datatransfer.DataFlavor.isMimeTypeEqual(java.lang.String)
    virtual bool isMimeTypeEqual(java::awt::datatransfer::DataFlavor* arg1); // public final boolean java.awt.datatransfer.DataFlavor.isMimeTypeEqual(java.awt.datatransfer.DataFlavor)
    virtual bool isMimeTypeSerializedObject(); // public boolean java.awt.datatransfer.DataFlavor.isMimeTypeSerializedObject()
    virtual bool isRepresentationClassByteBuffer(); // public boolean java.awt.datatransfer.DataFlavor.isRepresentationClassByteBuffer()
    virtual bool isRepresentationClassCharBuffer(); // public boolean java.awt.datatransfer.DataFlavor.isRepresentationClassCharBuffer()
    virtual bool isRepresentationClassInputStream(); // public boolean java.awt.datatransfer.DataFlavor.isRepresentationClassInputStream()
    virtual bool isRepresentationClassReader(); // public boolean java.awt.datatransfer.DataFlavor.isRepresentationClassReader()
    virtual bool isRepresentationClassRemote(); // public boolean java.awt.datatransfer.DataFlavor.isRepresentationClassRemote()
    virtual bool isRepresentationClassSerializable(); // public boolean java.awt.datatransfer.DataFlavor.isRepresentationClassSerializable()
    virtual java::awt::datatransfer::DataFlavor* selectBestTextFlavor(JavaObjectArray* arg1); // public static final java.awt.datatransfer.DataFlavor java.awt.datatransfer.DataFlavor.selectBestTextFlavor(java.awt.datatransfer.DataFlavor[])
    virtual void setHumanPresentableName(const char* arg1); // public void java.awt.datatransfer.DataFlavor.setHumanPresentableName(java.lang.String)

    java::awt::datatransfer::DataFlavor* stringFlavor; // public static final java.awt.datatransfer.DataFlavor java.awt.datatransfer.DataFlavor.stringFlavor
    java::awt::datatransfer::DataFlavor* imageFlavor; // public static final java.awt.datatransfer.DataFlavor java.awt.datatransfer.DataFlavor.imageFlavor
    java::awt::datatransfer::DataFlavor* plainTextFlavor; // public static final java.awt.datatransfer.DataFlavor java.awt.datatransfer.DataFlavor.plainTextFlavor
    const char* javaSerializedObjectMimeType; // public static final java.lang.String java.awt.datatransfer.DataFlavor.javaSerializedObjectMimeType
    java::awt::datatransfer::DataFlavor* javaFileListFlavor; // public static final java.awt.datatransfer.DataFlavor java.awt.datatransfer.DataFlavor.javaFileListFlavor
    const char* javaJVMLocalObjectMimeType; // public static final java.lang.String java.awt.datatransfer.DataFlavor.javaJVMLocalObjectMimeType
    const char* javaRemoteObjectMimeType; // public static final java.lang.String java.awt.datatransfer.DataFlavor.javaRemoteObjectMimeType
};
}
}
}
#endif
