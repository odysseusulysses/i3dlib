#ifndef java_awt_PageAttributes__OriginType_H
#define java_awt_PageAttributes__OriginType_H
#include <jni.h>
#include <java_marker.h>
#include <java_awt_AttributeValue.h>

namespace java {
namespace lang {
class String;
}
}
class JavaByteArray;
class JavaBooleanArray;
class JavaCharArray;
class JavaIntArray;
class JavaShortArray;
class JavaLongArray;
class JavaDoubleArray;
class JavaFloatArray;
class JavaObjectArray;

namespace java {
namespace awt {
class PageAttributes__OriginType : public java::awt::AttributeValue {
  public:
    PageAttributes__OriginType(JavaMarker* dummy);
    PageAttributes__OriginType(jobject obj);

    virtual void updateAllVariables(JavaMarker* dummy);
    virtual void updateAllNonFinalVariables(JavaMarker* dummy);
    virtual int hashCode(); // public int java.awt.PageAttributes$OriginType.hashCode()
    virtual const char* toString(); // public java.lang.String java.awt.PageAttributes$OriginType.toString()

    java::awt::PageAttributes__OriginType* PHYSICAL; // public static final java.awt.PageAttributes$OriginType java.awt.PageAttributes$OriginType.PHYSICAL
    java::awt::PageAttributes__OriginType* PRINTABLE; // public static final java.awt.PageAttributes$OriginType java.awt.PageAttributes$OriginType.PRINTABLE
};
}
}
#endif
