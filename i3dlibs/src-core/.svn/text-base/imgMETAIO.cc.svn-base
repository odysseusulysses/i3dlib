/*
 * i3dlib - image manipulation library
 *
 * Copyright (C) 2000-2006   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 * FILE: imgMETAIO.cc
 *
 * I/O routines for METAIO images
 *
 * Honza Huben� (xhubeny@fi.muni.cz) 2004
 * David Svoboda <svoboda@fi.muni.cz> 2005
 *
 */

/*Suppression of Deprecated warning for Visual studio 8*/
#ifdef _MSC_VER
#if _MSC_VER == 1400
#define _CRT_SECURE_NO_DEPRECATE 1
#endif
#endif

#include "i3d_config.h"

#ifdef WITH_METAIO

#include <iostream>
#include <fstream>
#include <string>
#include <cctype>
#include <algorithm>
#include <sstream>
#include <iomanip>
#include <map>

#include "imgMETAIO.h"
#include "toolbox.h"


#if defined(__GNUG__) && __GNUG__ < 3
	#include <stdio.h>
#endif

namespace i3d {

////////////////////////////////////////////////////////////////////////////
/** enum with definition of METAIO types */
////////////////////////////////////////////////////////////////////////////
typedef enum
{
  MET_NONE,
  MET_ASCII_CHAR,
  MET_CHAR,
  MET_UCHAR,
  MET_SHORT,
  MET_USHORT,
  MET_INT,
  MET_UINT,
  MET_LONG,
  MET_ULONG,
  MET_FLOAT,
  MET_DOUBLE,
  MET_STRING,
  MET_CHAR_ARRAY,
  MET_UCHAR_ARRAY,
  MET_SHORT_ARRAY,
  MET_USHORT_ARRAY,
  MET_INT_ARRAY,
  MET_UINT_ARRAY,
  MET_FLOAT_ARRAY,
  MET_DOUBLE_ARRAY,
  MET_FLOAT_MATRIX,
  MET_OTHER
} METAIOType;


////////////////////////////////////////////////////////////////////////////
typedef struct {
    int channels;
    METAIOType in;
    ImgVoxelType out;
} METAIOTypeToImgVoxelTypeTable;

////////////////////////////////////////////////////////////////////////////
/** const table to convert METAIOType to ImgVoxelType */
////////////////////////////////////////////////////////////////////////////
const METAIOTypeToImgVoxelTypeTable TypeConv[] = {
    {1, MET_UCHAR, Gray8Voxel },
    {3, MET_UCHAR, RGBVoxel },
    {1, MET_USHORT, Gray16Voxel },
    {3, MET_USHORT, RGB16Voxel },
    {1, MET_FLOAT, FloatVoxel },
    {3, MET_FLOAT, VectFloatVoxel }, 
    {3, MET_DOUBLE, VectDoubleVoxel },
    {1, MET_OTHER, UnknownVoxel } 
};

////////////////////////////////////////////////////////////////////////////
typedef struct {
    const char *name;
    METAIOType type;
} METAIOTypeNameToEnum;

////////////////////////////////////////////////////////////////////////////
/** const table to convert  String to METAIOType */
////////////////////////////////////////////////////////////////////////////
const METAIOTypeNameToEnum METAIONameToTypeConv [] = {
    {"MET_NONE",MET_OTHER},
    {"MET_ASCII_CHAR",MET_OTHER},
    {"MET_CHAR",MET_OTHER},
    {"MET_UCHAR",MET_UCHAR},
    {"MET_SHORT",MET_OTHER},
    {"MET_USHORT",MET_USHORT},
    {"MET_INT",MET_OTHER},
    {"MET_UINT",MET_OTHER},
    {"MET_LONG",MET_OTHER},
    {"MET_ULONG",MET_OTHER},
    {"MET_FLOAT",MET_FLOAT},
    {"MET_DOUBLE",MET_DOUBLE},
    {"MET_STRING",MET_OTHER},
    {"MET_CHAR_ARRAY",MET_OTHER},
    {"MET_UCHAR_ARRAY",MET_OTHER},
    {"MET_SHORT_ARRAY",MET_OTHER},
    {"MET_USHORT_ARRAY",MET_OTHER},
    {"MET_INT_ARRAY",MET_OTHER},
    {"MET_UINT_ARRAY",MET_OTHER},
    {"MET_FLOAT_ARRAY",MET_OTHER},
    {"MET_DOUBLE_ARRAY",MET_OTHER},
    {"MET_FLOAT_MATRIX",MET_OTHER},
    {"MET_OTHER",MET_OTHER}
};


/*************************************************************************
* convert METAIO type enum to ImgVoxelType enum 
****************************************************************************/
ImgVoxelType METAIOTypeToImgVoxelType (int channels, METAIOType in)
{
    size_t i;
    i = 0;
    while (TypeConv[i].out != UnknownVoxel)
    {
	if ((channels == TypeConv[i].channels) && (in == TypeConv[i].in))
	    return TypeConv[i].out;
	i++;
    }
    return UnknownVoxel;
}

/*************************************************************************
* convert ImgVoxelType enum to METAIOType enum 
****************************************************************************/
METAIOType ImgVoxelTypeToMETAIOType (int & channels, ImgVoxelType in)
{
    size_t i;
    i = 0;
    while (TypeConv[i].out != UnknownVoxel)
    {
	if ((in == TypeConv[i].out))
	{
	    channels = TypeConv[i].channels;
	    return TypeConv[i].in;
	}
	i++;
    }
    return MET_OTHER;
}


/*************************************************************************
* convert name in string to METAIOType enum 
****************************************************************************/
METAIOType StrToMETAIOType (const std::string &s)
{
    size_t i = 0;
    while (strcmp (METAIONameToTypeConv[i].name, "MET_OTHER"))
    {
	    if (s == METAIONameToTypeConv[i].name)
	        return METAIONameToTypeConv[i].type;
	    i++;
    }
    return MET_OTHER;

}

/*************************************************************************
* convert METAIOType enum to name in string 
****************************************************************************/
std::string METAIOTypeToStr (const METAIOType in)
{
    size_t i = 0;
    while (strcmp (METAIONameToTypeConv[i].name, "MET_OTHER"))
    {
	    if ((in == METAIONameToTypeConv[i].type))
	        return METAIONameToTypeConv[i].name;
	    i++;
    }
    return METAIONameToTypeConv[i].name;
}


/*************************************************************************
* converts string to int with exception handling and error message 
****************************************************************************/
void ReadInt (std::string &s, int &i, int def, std::string errormsg)
{
  int pomint;
  if (s != "")
    {
      if (StrToInt (s, pomint))
        i = pomint;
      else
        throw IOException (errormsg);
    }
  else
    {
      i = def;
    }
}
 
/*************************************************************************
 * converts string to float with exception handling and error message
****************************************************************************/
void ReadFloat (std::string & s, float &i, float def, std::string  errormsg)
{
  float pom;
  if (s != "")
    {
      if (StrToFloat (s, pom))
        i = pom;
      else
        throw IOException (errormsg);
    }
  else
    {
      i = def;
    }
}

/*************************************************************************
* true if the machine on which is the code running is MSB -- BIg Endian 
****************************************************************************/
bool IsByteOrderMSB()
{
    unsigned int word = 0x0001;
    unsigned char * byte = (unsigned char *) &word;
    return (((*byte) & 1) ? false: true);
}

/*************************************************************************
* swap byte order of 2BYTEs size
****************************************************************************/
inline unsigned short ByteOrderSwapShort (unsigned short x)
{
  return (unsigned short) ((unsigned short) (x << 8) |
                           (unsigned short) (x >> 8));
}

/*************************************************************************
* swap byte order of 4BYTEs size 
****************************************************************************/
//poupravena verze, puvodni mela parametr unsigned int
inline unsigned long ByteOrderSwapLong (unsigned long x)
{
  return (((x << 24) & 0xff000000) |
          ((x << 8)  & 0x00ff0000) |
          ((x >> 8)  & 0x0000ff00) |
		  ((x >> 24) & 0x000000ff));
}

/*************************************************************************
* swap byte order of 8 BYTEs size 
****************************************************************************/
/*inline unsigned long ByteOrderSwap8 (unsigned long x)
{
  return (((x << 24) & 0xf000f000) |
          ((x << 8) & 0x0f000f00) |
          ((x >> 8) & 0x00f000f0) | ((x >> 24) & 0x000f000f));
}
*/

//poupravena verze, puvodni provadela bitovy AND pouze na 4 bytech 
//funkce mapuje osmice bytu 'ABCD EFGH' na 'HGFE DCBA'
double  ByteOrderSwap8 (double x){
  unsigned long *p= (unsigned long *) &x;    
  unsigned long l = ByteOrderSwapLong(*p); //flip leftmost bytes   
  *(p) = ByteOrderSwapLong(*(p+1)); //flip the rightmost and put them to the left
  *(p+1) = l; //put leftmost to right;
  return *(double*)p;
}
/*************************************************************************
 * swap byte order of the image in the memory to LSB from MSB or reverse 
****************************************************************************/
/*
void SwapByteOrder (ImgVoxelType  ElementType, void * data, size_t size)
{
    size_t i;
    switch (ElementType)
    {
	case Gray16Voxel: case RGB16Voxel: case FloatVoxel:
	    for (i = 0; i < size; i++)
		((unsigned short *)  data)[i] = ByteOrderSwapShort(((unsigned short *)data)[i]);
	    break;
	default : break;
    }
}*/
// 
void SwapByteOrder (ImgVoxelType  ElementType, void * data, size_t size)
{
	size_t i;
	switch (ElementType)
	{
	case Gray16Voxel: case RGB16Voxel: 
		if (sizeof (unsigned short) != 2) 
			throw InternalException ("imgMETAIO.cc: Indianity conversion is required, but current function supports only sizeof(short)==2 implementations.");
		for (i = 0; i < size; i++)
		((unsigned short *)  data)[i] = ByteOrderSwapShort(((unsigned short *)data)[i]);
		break;
	case FloatVoxel: case VectFloatVoxel:
		if (sizeof (unsigned long) != 4 || sizeof (float) != 4) 
			throw InternalException ("imgMETAIO.cc: Indianity conversion is required, but current function supports only 'sizeof(long)==sizeof(float)==4' implementations.");
		for (i = 0; i < size; i++)
		((unsigned long *)  data)[i] = ByteOrderSwapLong(((unsigned long *)data)[i]);
		break;
	case VectDoubleVoxel:
		if (sizeof (unsigned long) != 4 || sizeof (double) != 8) 
			throw InternalException ("imgMETAIO.cc: Indianity conversion is required, but current function supports only 'sizeof(long)==4 && sizeof(double)==8' implementations.");
		for (i = 0; i < size; i++)
		((double *)  data)[i] = ByteOrderSwap8(((double *)data)[i]);
		break;
	default : break;
    }
}
/*************************************************************************
* compare byte order of the data in raw file and 
* the byte order used on the running machine 
****************************************************************************/
bool CompareByteOrder(bool ByteOrderMSB) 
{
#ifdef I3D_DEBUG
	std::cout << "Comparison of byte order: " << 
		 IsByteOrderMSB() << " ?= " << ByteOrderMSB << std::endl;
#endif
    if (IsByteOrderMSB() == ByteOrderMSB)
	return true;
    else
	return false;
}

/****************************************************************************
 * constructor
 ****************************************************************************/
METAIOReader::METAIOReader(const char *fname):ImageReader(fname) 
    {
		 f.open(fname);

		 if (f.fail())
       	throw IOException("METAIOReader: Unable to open " + 
					std::string(fname) + ".");
    };


/****************************************************************************
 * load the raw image data
 ****************************************************************************/
template <class T> void METAIOReader::LoadData(T *data)
{
	/// evalaute the image size
	size_t sz = header.size.x * header.size.y * header.size.z;
	std::ostringstream os;

	/// is there list of raw files?
   if (meta_tags.FileNameMax != 0)
	{
		size_t shift = 0;
	   char *sname = new char[meta_tags.ElementDataFile.length() + 1];

  		for (int i = meta_tags.FileNameMin; 
		 			i <= meta_tags.FileNameMax; 
					 i += meta_tags.FileNameStep)
  		{
			 // set up the name
			 sprintf(sname, meta_tags.ElementDataFile.c_str(), i);

	 		 #ifdef I3D_DEBUG
			 	std::cout << "Reading file: " << sname << std::endl;
			 #endif
		
			 // open the file
	 		 std::ifstream fslice(sname, std::ios::binary);
	
			 shift += ReadRawData(fslice, data + shift); 

	 		 fslice.close();
		}

		delete []sname;
	}
	/// no, there is only one file which contains the data
   else
	{
		/// open the specified file
		std::ifstream ff(meta_tags.ElementDataFile.c_str(), std::ios::binary);

		/// skip the header
		if (meta_tags.HeaderSize != -1) 
		{
			ff.seekg(meta_tags.HeaderSize, std::ios::beg);
		}
		else
		{
			ff.seekg(0, std::ios::end);
			size_t length = ff.tellg();
			size_t shift = length - sz*sizeof(T);
			ff.seekg(shift, std::ios::beg);
		}
		
		ReadRawData(ff, data);

		ff.close();
	}

	/// Check the byte order. Change if necessary.
   if (!CompareByteOrder(meta_tags.ElementByteOrderMSB))
   {
  		SwapByteOrder(header.itype, data, sz);
	}

}

/***************************************************************************
 * read some header information
 * ************************************************************************/
size_t METAIOReader::GetFileNames(FileList &fl)
{
	fl.clear();
	fl.push_back(header.name);

	if (meta_tags.ElementDataFile != header.name)
	{
		if (meta_tags.FileNameMax == 0)
		{
			fl.push_back(meta_tags.ElementDataFile);
		}
		else
		{
			char *sname = new char[meta_tags.ElementDataFile.length() + 1];
			
			for (int i = meta_tags.FileNameMin; 
					i <= meta_tags.FileNameMax; 
					i += meta_tags.FileNameStep)
			{
			 	sprintf(sname, meta_tags.ElementDataFile.c_str(), i);
				fl.push_back(sname);
			}

			delete [] sname;
		}
	}

	return fl.size();
}
	
/*************************************************************************
 * read one raw file to the memory
 ***************************************************************************/
template <class T> size_t METAIOReader::ReadRawData(std::ifstream &ff, T* data)
{
	 /// size of one slice
	 size_t size = header.size.x * header.size.y;

	 /// 3D image - not slices
    if (meta_tags.FileNameMax == 0)
		 size *= header.size.z;

    ff.read((char *)data, size*sizeof(T));

    if ((size_t)ff.gcount() != size*sizeof(T))
	 	throw IOException("ReadRawData: File not completely read!");

	 return size;
}

/***************************************************************************
 * Parse metaio header file and save relevat values to header 
 ***************************************************************************/
void METAIOReader::LoadImageInfo()
{
  const char sep = '=';
  bool end = false;
  std::map < std::string, std::string > items;
  size_t line_no = 1;

  header.offset = 0;

  while ((!f.eof ()) && (end != true))
    {
      std::string s;
      std::string key, val;
      getline (f, s, '\n');

      size_t pos = s.find (sep);
      key = Trim (s.substr (0, pos));
      val = (pos == std::string::npos)
        ? std::string ("") : Trim (s.substr (pos + 1, std::string::npos));
      transform (key.begin (), key.end (), key.begin (),
                 (int (*)(int)) tolower);
      if (key == "")
        {
          if (val == "")
            continue;
          else
            throw IOException ("METAIOReader::LoadImageInfo:"\
						" Keyword not specified in file " + header.name
						+ " at line: " + itos (line_no));
        }

      if (key == "elementdatafile")
        {
          if (val == "LOCAL")
            end = true;
          if (val.find ("LIST", 0) != std::string::npos)
            throw IOException ("METAIOReader::LoadImageInfo:"\
						  " i3dlib does not support the ElementDataFile"\
						  " LIST[X] feature: " + header.name + " at line: " 
						  + itos (line_no));
        }
      items[key] = val;
      ++line_no;
    }
  
  
  ReadInt(items["headersize"], meta_tags.HeaderSize, -1,
		  "METAIOReader::LoadImageInfo: HeaderSize argument is not integer: " + 
		  header.name + " at line: " + itos (line_no));

  ReadInt(items["ndims"], meta_tags.NDims, 2, 
		  "METAIOReader::LoadImageInfo: NDims argument is not integer: " + 
		  header.name + " at line: " + itos (line_no));
  
  ReadInt(items["elementnumberofchannels"], 
		  meta_tags.ElementNumberOfChannels, 1, 
		  "METAIOReader::LoadImageInfo: ElementNumberOfChannels argument"\
		  " is not integer: " + header.name + " at line: " + itos (line_no));
  
  ReadFloat(items["elementmin"], meta_tags.ElementMin, 0,
		  "METAIOReader::LoadImageInfo: ElementMin argument is not float: " + 
		  header.name + " at line: " + itos (line_no));

  ReadFloat(items["elementmax"], meta_tags.ElementMax, 0, 
		  "METAIOReader::LoadImageInfo: ElementMax argument is not float: " + 
		  header.name + " at line: " + itos (line_no));

  header.itype = METAIOTypeToImgVoxelType(
		  meta_tags.ElementNumberOfChannels,
		  StrToMETAIOType(items["elementtype"]));
  
  transform (
		  items["elementbyteordermsb"].begin (), 
		  items["elementbyteordermsb"].end (), 
		  items["elementbyteordermsb"].begin (),(int (*)(int)) tolower);

  if (items["elementbyteordermsb"] == "true")
      meta_tags.ElementByteOrderMSB = true;
  else
      meta_tags.ElementByteOrderMSB = false;
  
  std::vector<std::string> tokens;
  std::string delimiters;
  int i;
  delimiters += ' ';
  delimiters += '\t';

  /// reading the image size
  if (items["dimsize"] != "" )
  {
	  Tokenize(items["dimsize"],tokens,delimiters);
  	  if (tokens.size() != (size_t)meta_tags.NDims)
	  {
		  throw IOException ("METAIOReader::LoadImageInfo:"\
				  " The Number of DimSize != NDims: " + header.name + 
				  " at line: " + itos (line_no));
	  }
	  else
	  {
		  int dims[3] = {1,1,1};
		  for (i=0; i < meta_tags.NDims; i++)
        {
	   		ReadInt(tokens[i],dims[i], 1, 
						"METAIOReader::LoadImageInfo:  DimSize is not integer: " + 
						header.name + " at line: " + itos (line_no));
        }


		  header.size.x = dims[0];
		  header.size.y = dims[1];
		  header.size.z = dims[2];
     }
  }
  else
  {
        throw IOException ("METAIOReader::LoadImageInfo:"\
				  " There is no DimSize in the file: " + header.name);
  }
  tokens.clear();

  /// reading element spacing
  if (items["elementspacing"] != "")
  {
      Tokenize(items["elementspacing"],tokens,delimiters);
      if (tokens.size() != (size_t)meta_tags.NDims)
      {
	  		throw IOException ("METAIOReader::LoadImageInfo:"\
					" The Number of ElementSpacing != NDims: " + header.name + 
					" at line: " + itos (line_no));
      }
      else
      {
		  for (i=0; i < meta_tags.NDims; i++)
			  {
			      ReadFloat(tokens[i], meta_tags.ElementSpacing[i], 1, 
							"METAIOReader::LoadImageInfo:"\
							" ElementSpacing is not float: " + header.name + 
							" at line: " + itos (line_no));
	 		  }
      }
      tokens.clear();

  		/// setting the image resolution
		header.resolution = new Vector3d<float>;
	   header.resolution->x = 1.0f / meta_tags.ElementSpacing[0];
		header.resolution->y = 1.0f / meta_tags.ElementSpacing[1];
		header.resolution->z = 1.0f / meta_tags.ElementSpacing[2];
  }
  else
  {
      for (i=0; i < meta_tags.NDims; i++)
      {
	      meta_tags.ElementSpacing[i] = 1;
      }
  }

  /// reading image position
  if (items["position"] != "")
  {
      Tokenize(items["position"],tokens,delimiters);
      if (tokens.size() != (size_t)meta_tags.NDims)
      {
	  		throw IOException ("METAIOReader::LoadImageInfo:"\
					" The Number of Position != NDims: " + header.name + 
					" at line: " + itos (line_no));
      }
      else
      {
		  for (i=0; i < meta_tags.NDims; i++)
			  {
			      ReadFloat(tokens[i], meta_tags.Position[i], 1, 
							"METAIOReader::LoadImageInfo:"\
							" Position is not float: " + header.name + 
							" at line: " + itos (line_no));
	 		  }
      }
      tokens.clear();

  		/// setting the image position
	   header.offset.x = meta_tags.Position[0];
		header.offset.y = meta_tags.Position[1];
		header.offset.z = meta_tags.Position[2];
  }
  else
  {
  		/// setting the image position
	   header.offset.x = 0;
		header.offset.y = 0;
		header.offset.z = 0;
  }

  /// reading the element size
  if (items["elementsize"] != "")
  {
      Tokenize(items["elementsize"],tokens,delimiters);
      if (tokens.size() != (size_t) meta_tags.NDims)
      {
		  throw IOException ("METAIOReader::LoadImageInfo:"\
				  " The Number of ElementSize != NDims: " + header.name + 
				  " at line: " + itos (line_no));
      }
      else
      {
		  for (i=0; i < meta_tags.NDims; i++)
		  {
	   	   ReadFloat(tokens[i],meta_tags.ElementSize[i], 1, 
						"METAIOReader::LoadImageInfo: ElementSize is not float: " + 
						header.name + " at line: " + itos (line_no));
	      }
      }
      tokens.clear();
  }
  else
  {
      for (i=0; i < meta_tags.NDims; i++)
      {
	      meta_tags.ElementSize[i] = 1;
      }
  }

  /* if neither element size is set nor the element spacing, both are
	* given the implicit value (1,1,1) */
  if ((items["elementsize"] == "") && (items["elementspacing"] != ""))
  {
    for (i = 0; i < meta_tags.NDims; i++)
      {
        meta_tags.ElementSize[i] = meta_tags.ElementSpacing[i];
      }
  }

 if ((items["elementsize"] != "") && (items["elementspacing"] == ""))
  {
    for (i = 0; i < meta_tags.NDims; i++)
      {
        meta_tags.ElementSpacing[i] = meta_tags.ElementSize[i];
      }
  }

 /// find out the data source
 if (items["elementdatafile"] != "")
 {
  Tokenize(items["elementdatafile"],tokens,delimiters);

  switch (tokens.size())
  {
	  case 1:
  /** just one token is found. It means, that the binary raw data
	* are stored either in the header file after the header is finished or 
	* in another ONE file */
  		{
      meta_tags.FileNameMin =
      	meta_tags.FileNameMax =
      	meta_tags.FileNameStep = 0;

      if (items["elementdatafile"] == "LOCAL")
      {
		  meta_tags.ElementDataFile = header.name;
      }
      else
      {
		  std::string path, ext, name;
		  SplitFilename (items["elementdatafile"].c_str(), path, name, ext);
	  
		  if (path != "")
		  {
		      meta_tags.ElementDataFile = items["elementdatafile"];
		  }
		  else
		  {
		      SplitFilename (header.name.c_str(), path, name, ext);
		      meta_tags.ElementDataFile = path + items["elementdatafile"];
		  }
      }
  		}
		break;
  case 4:	
  /// the data are stored in the list of raw files
  		{
      if (tokens[0].find('%',0) == std::string::npos)
		  throw IOException ("METAIOReader::LoadImageInfo:"\
				  " The file name has not the printf formatting"\
				  " (missing something like %02d)! " + header.name + 
				  " at line: " + itos (line_no));
      else
	  if (tokens[0].find('%',0) != tokens[0].rfind('%',tokens[0].length()-1))
	      throw IOException ("METAIOReader::LoadImageInfo:"\
					" There is more than one printf formatting characters"\
					" (something like %02d)! " + header.name + 
					" at line: " + itos (line_no));		
      
     std::string path, ext, name;
	  SplitFilename (tokens[0].c_str(), path, name, ext);
	  if (path != "")
	  {
	      meta_tags.ElementDataFile = tokens[0];
	  }
	  else
	  {
	      SplitFilename (header.name.c_str(), path, name, ext);
	      meta_tags.ElementDataFile = path;
	      meta_tags.ElementDataFile += tokens[0];
	  }

	  
      ReadInt (tokens[1], meta_tags.FileNameMin, 1, 
				"METAIOReader::LoadImageInfo:  FileNameMin is not integer: " + 
				header.name + " at line: " + itos (line_no));

      ReadInt (tokens[2], meta_tags.FileNameMax, 1, 
				"METAIOReader::LoadImageInfo:  FileNameMax is not integer: " + 
				header.name + " at line: " + itos (line_no));

      ReadInt (tokens[3], meta_tags.FileNameStep, 1, 
				"METAIOReader::LoadImageInfo:  FileNameStep is not integer: " + 
				header.name + " at line: " + itos (line_no));

      int i,j;
      j = 0;
      i = meta_tags.FileNameMin;
      while (i <= meta_tags.FileNameMax)
      {
	  j++;
	  i+= meta_tags.FileNameStep;
      }
      
      if ((size_t)j != header.size.z)
		  throw IOException ("METAIOReader::LoadImageInfo:"\
				  " The DimSize in z is not equal to number of slices"\
				  " specified by the ElementDatFile tag! " +  header.name + 
				  " at line: " + itos (line_no));
  		}
		break;
  default:
       throw IOException ("METAIOReader::LoadImageInfo:"\
				 " The Number of ElementDataFiles item is not 4,"\
				 " either sprintf_format_string or begin or end or step missing!" +
				 header.name + " at line: " + itos (line_no));
  }
 }
 else
 {
     throw IOException ("METAIOReader::LoadImageInfo:"\
			  " There is no ElementDataFile in the file: " + header.name);
 }
  
#ifdef I3D_DEBUG
    std::cout << "METAIOReader::" << std::endl;
    std::cout << " - HeaderSize: " << meta_tags.HeaderSize << std::endl;
    std::cout << " - NDims: " << meta_tags.NDims << std::endl;
    std::cout << " - ElementByteOrderMSB: " << meta_tags.ElementByteOrderMSB << std::endl;
    std::cout << " - ElementMin: " << meta_tags.ElementMin << std::endl;
    std::cout << " - ElementMax: " << meta_tags.ElementMax << std::endl;
    std::cout << " - ElementNumberOfChannels: " << meta_tags.ElementNumberOfChannels << std::endl;
    std::cout << " - FileNameMin: " << meta_tags.FileNameMin << std::endl;
    std::cout << " - FileNameMax: " << meta_tags.FileNameMax << std::endl;
    std::cout << " - FileNameStep: " << meta_tags.FileNameStep << std::endl;
    std::cout << " - ElementDataFile: " << meta_tags.ElementDataFile << std::endl;
    std::cout << " - ElementType: " << VoxelTypeToString(header.itype) << std::endl;

    std::cout << " - DimSize[" << 0 << "]: " << header.size.x << std::endl;
    std::cout << " - DimSize[" << 1 << "]: " << header.size.y << std::endl;
    std::cout << " - DimSize[" << 2 << "]: " << header.size.z << std::endl;

    for (i = 0; i < meta_tags.NDims; i++)
      {
        std::cout << " - ElementSpacing[" << i << "]: " << 
			  meta_tags.ElementSpacing[i] << std::endl;
      }

    for (i = 0; i < meta_tags.NDims; i++)
      {
        std::cout << " - ElementSize[" << i << "]: " << 
			  meta_tags.ElementSize[i] << std::endl; 
      }
#endif

}

/*************************************************************************
 * destructor
 ************************************************************************/
METAIOReader::~METAIOReader()
{
	f.close();
}

/*************************************************************************
 * constructor
****************************************************************************/
METAIOWriter::METAIOWriter(const char *fname): ImageWriter(fname) 
{
	f.open(fname);
	
	if (f.fail())
		throw IOException("METAIOWriter: Unable to create " + 
				std::string(fname) + ".");
};

/****************************************************************************
 * Save the image to the disk in MetaIO raw format.
 * The data are saved to one raw file if the compression == true
 * otherwise there is one raw file per slice 
 * **************************************************************************/
void METAIOWriter::SaveImageInfo()
{
   int i = 0;

   ImgVoxelTypeToMETAIOType (i, header.itype);
   meta_tags.ElementNumberOfChannels = i;

	meta_tags.NDims = 3;
	if (header.size.z == 1)
	    meta_tags.NDims--;

	meta_tags.ElementByteOrderMSB = IsByteOrderMSB();
	meta_tags.HeaderSize = -1;

	/// get the name of the file (without extension)
	std::string path, name, ext;
	SplitFilename (header.name.c_str(), path, name, ext);
	
	/** If compression is true, all the data are stored in one big
	 * raw file. */

	if (header.compression || (header.size.z == 1))
	{
      meta_tags.FileNameMin = 
		  meta_tags.FileNameMax = 
		  meta_tags.FileNameStep = 0;
	  
      meta_tags.ElementDataFile = name + ".raw"; 
	}
	/// Otherwise, each slice (of 3D image) is saved in separate file
	else
	{
	     meta_tags.FileNameMin = 0;
		  meta_tags.FileNameMax = header.size.z-1;
	 	  meta_tags.FileNameStep = 1;

		  size_t num = num_digits(meta_tags.FileNameMax);

		  std::ostringstream os;
		  os << name << "_%0" << num << "d.raw " << 
				meta_tags.FileNameMin << " " << 
				meta_tags.FileNameMax << " " << 
				meta_tags.FileNameStep;

		  meta_tags.ElementDataFile = os.str();
	}

	meta_tags.ElementMin = 
		meta_tags.ElementMax = 
		meta_tags.ElementSize[0] = 
		meta_tags.ElementSize[1] = 
		meta_tags.ElementSize[2] = 0;

	 if (header.resolution != NULL)
	 {
		 meta_tags.ElementSpacing[0] = 1.0f/header.resolution->x;
		 meta_tags.ElementSpacing[1] = 1.0f/header.resolution->y;
		 meta_tags.ElementSpacing[2] = 1.0f/header.resolution->z;
	 }
	 else
	 {
		 meta_tags.ElementSpacing[0] = 
		 	 meta_tags.ElementSpacing[1] = 
			 meta_tags.ElementSpacing[2] = 0.1;
	 }

	 // set position
	 meta_tags.Position[0] = header.offset.x;
	 meta_tags.Position[1] = header.offset.y;
	 meta_tags.Position[2] = header.offset.z;

#ifdef I3D_DEBUG
    std::cout << "Name of the file: " << header.name << std::endl;
    std::cout << "HeaderSize: " << meta_tags.HeaderSize << std::endl;
    std::cout << "NDims: " << meta_tags.NDims << std::endl;
    std::cout << "ElementByteOrderMSB: " << meta_tags.ElementByteOrderMSB << std::endl;
    std::cout << "ElementMin: " << meta_tags.ElementMin << std::endl;
    std::cout << "ElementMax: " << meta_tags.ElementMax << std::endl;
    std::cout << "ElementNumberOfChannels: " << 
		 meta_tags.ElementNumberOfChannels << std::endl;
    std::cout << "FileNameMin: " << meta_tags.FileNameMin << std::endl;
    std::cout << "FileNameMax: " << meta_tags.FileNameMax << std::endl;
    std::cout << "FileNameStep: " << meta_tags.FileNameStep << std::endl;
    std::cout << "ElementDataFile: " << meta_tags.ElementDataFile << std::endl;
    std::cout << "ElementType: " << header.itype << std::endl;
    std::cout << "DimSize[" << 0 << "]: " << header.size.x << std::endl;
    std::cout << "DimSize[" << 1 << "]: " << header.size.y << std::endl;
    std::cout << "DimSize[" << 2 << "]: " << header.size.z << std::endl;

    for (i = 0; i < meta_tags.NDims; i++)
      {
        std::cout << "ElementSpacing[" << i << "]: " << 
			  meta_tags.ElementSpacing[i] << std::endl;
      }

    for (i = 0; i < meta_tags.NDims; i++)
      {
        std::cout << "ElementSize[" << i << "]: " << 
			  meta_tags.ElementSize[i] << std::endl; 
      }

    for (i = 0; i < meta_tags.NDims; i++)
      {
        std::cout << "Position[" << i << "]: " << 
			  meta_tags.Position[i] << std::endl; 
      }
#endif

   f << "NDims = " << meta_tags.NDims << std::endl;
   f << "DimSize = " << header.size.x << " " << header.size.y;

   if (meta_tags.NDims == 3)
		f << " "<< header.size.z << std::endl;
   else
		f << std::endl;

    f << "ElementSpacing = " << meta_tags.ElementSpacing[0] << 
		 " " << meta_tags.ElementSpacing[1];

    if (meta_tags.NDims == 3)
		f << " "<< meta_tags.ElementSpacing[2] << std::endl;
    else
		f << std::endl;

    f << "Position = " << meta_tags.Position[0] << 
		 " " << meta_tags.Position[1];

    if (meta_tags.NDims == 3)
		f << " "<< meta_tags.Position[2] << std::endl;
    else
		f << std::endl;

    f << "HeaderSize = " << meta_tags.HeaderSize << std::endl;
    f << "ElementByteOrderMSB = ";

    if (meta_tags.ElementByteOrderMSB)
		f << "True" << std::endl;
    else
		f << "False" << std::endl;

    std::string pomstr = 
		 METAIOTypeToStr(ImgVoxelTypeToMETAIOType (i, header.itype));
    f << "ElementType = " << pomstr << std::endl;
    f << "ElementNumberOfChannels = " << i << std::endl;
	 f << "ElementDataFile = " << meta_tags.ElementDataFile << std::endl;
}

/*************************************************************************
 * save raw image data 
 *************************************************************************/
template <class T> void METAIOWriter::SaveData(T *data)
{
   if (meta_tags.FileNameMax == 0)
   {
		std::ofstream ff(meta_tags.ElementDataFile.c_str(), std::ios::binary);

		WriteRawData (ff, data);

		ff.close();
   }
    else
    {
		std::string path, name, ext;
		SplitFilename (header.name.c_str(), path, name, ext);
		std::ostringstream os;
		size_t shift = 0;
		size_t num = num_digits(meta_tags.FileNameMax);
		
	   for( int i = meta_tags.FileNameMin; 
			 		i <= meta_tags.FileNameMax; 
					 i += meta_tags.FileNameStep)
	    {
			 os.str("");
			 os.fill('0');
			 os << name << "_" << std::setw(num) << i << ".raw";

		 	 std::ofstream ff(os.str().c_str(), std::ios::binary);

	 		 #ifdef I3D_DEBUG
			   std::cout << "filename: " << os.str() << std::endl;
	 		 #endif

		 	 shift += WriteRawData(ff, data + shift); 

			 ff.close();
    		}
    }

}

/*************************************************************************
 * write one raw file from data
 ************************************************************************/
template <class T> size_t METAIOWriter::WriteRawData(
		std::ofstream &ff, 
		const T* data)
{
    size_t size = header.size.x * header.size.y;
	 
    if (meta_tags.FileNameMax == 0)
	 	size *= header.size.z;

/// to by nemelo byt potreba, protoze to obstara typ T
///    size *= meta_tags.ElementNumberOfChannels;

    ff.write((char *)data, size*sizeof(T));

    if (!ff.good())
	 		throw IOException("WriteRawData: An Error occured while"\
				 " saving RAW data.");

	 return size;
}

/*************************************************************************
 * destructor
 *************************************************************************/
METAIOWriter::~METAIOWriter()
{
	f.close();
}

}

#endif
