/*
 * i3dlib - image manipulation library
 *
 * Copyright (C) 2000-2006   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** \file toolbox.h
	\brief General purpose functions - header.
 
	The intended purpose of this file is to provide several useful (often
	simple) functions of general use that can slightly simplify programming. 

	\todo 
 
	\author Petr Mejzl�k (mejzlik@fi.muni.cz) 2000
	\author Petr Matula (pem@fi.muni.cz) 2001 - added functions: Trim, BeforeLast, AfterLast, 
	GetExtension, GetPath, SplitFilename, num_digits, cmp_nocase, ScanDir

 */
#ifndef __TOOLBOX_H__
#define __TOOLBOX_H__

#ifdef __GNUG__
#pragma interface
#endif

#include <string>
#include <vector>
#include <iostream>

#include "basic.h"
#include "imgfiles.h"


namespace i3d {
	/***************************************************************************\
	*
	*                           String related functions
	*
	\***************************************************************************/
	/** Int to string conversion. 
	Write integer value to a string. */
	CORE_I3D_DLLEXPORT std::string itos(int n);

	/** String to int conversion. 
	Convert string to an integer value. \returns \p true on success*/
	CORE_I3D_DLLEXPORT bool StrToInt (const std::string & s, int &i);
	
	/** String to float conversion.
	Convert string to a float value. \returns \p true on success*/
	CORE_I3D_DLLEXPORT bool StrToFloat (const std::string & s, float &f);

	/** Split a string to the vector of tokens. */
	CORE_I3D_DLLEXPORT void Tokenize (
		const std::string & str, 
		std::vector < std::string > &tokens, 
		const std::string & delimiters = " "
		);

	/** \returns string without leading and tailing whitespaces */
	CORE_I3D_DLLEXPORT std::string Trim(const std::string &s);

	/** \returns string after last occurence of \p sep. If \p sep is not found, "" is returned*/
	CORE_I3D_DLLEXPORT std::string AfterLast(const std::string &s, char sep);

	/** \returns string before last occurence of \p sep. If \p sep is not found, "" is returned */
	CORE_I3D_DLLEXPORT std::string BeforeLast(const std::string &s, char sep);

	/** No case string comparison. 
	\returns -1 if \p s1 < \p s2
	\returns 1 if \p s1 > \p s2
	\returns 0 if \p s1 == \p s2.*/
	CORE_I3D_DLLEXPORT int cmp_nocase(const std::string &s1, const std::string &s2);

	/** \returns number of digits of \p number in specified base
	if number==0 then 1 is returned */
	CORE_I3D_DLLEXPORT int num_digits(int number,int base = 10);

	/** \return the given number formatted in human readable code */
	CORE_I3D_DLLEXPORT std::string human_readable (double number, unsigned precision = 2);

	/** \return the given number formatted in human readable code */
	CORE_I3D_DLLEXPORT std::string human_readable (unsigned long long number);

	/**************************************************************************\
	*
	*                   File and directory related functions
	*
	\**************************************************************************/
	/** Returns the extension of given filename */
	CORE_I3D_DLLEXPORT void GetExtension(const std::string &fn, std::string &ext);

	/** Returns the path part of given filename - i.e. the string before and including 
        the last slash. Both types of slashes are accepted as path deliminators. */
	CORE_I3D_DLLEXPORT void GetPath(const std::string &fn, std::string &path);

	/** Splits filename fn using functions above, name is string between
	last dot and last slash */
	CORE_I3D_DLLEXPORT void SplitFilename(
		const std::string &fn, 
		std::string &path, 
		std::string &name, 
		std::string &ext);

	/** Get current working directory. 
	\throws IOException if there is a problem.  */
	CORE_I3D_DLLEXPORT std::string GetDir();

	/** Set working directory */
	CORE_I3D_DLLEXPORT void SetDir(const char* dir);

	/** \todo Documentation */
	CORE_I3D_DLLEXPORT int MaskExpand(
		const char *mask, 
		FileList &namelist, 
		bool expand = true);


	/**************************************************************************\
	*
	*                           Stream functions
	*
	\**************************************************************************/
	/** Set a stream to throw ios::failure after an unsuccessful IO operation. */
	CORE_I3D_DLLEXPORT void SetBadIOException(std::ios& io);

	/** Read 4 bytes in least-significant byte first order into a long. */
	CORE_I3D_DLLEXPORT unsigned long LSBFirstReadLong(std::istream& file);

	/** Write a long value as 4 bytes in the least-significant byte first order. */
	CORE_I3D_DLLEXPORT void LSBFirstWriteLong(std::ostream& file, unsigned long value);

	/** Read 2 bytes in least-significant byte first order into a short int. */
	CORE_I3D_DLLEXPORT unsigned short LSBFirstReadShort(std::istream& file);

	/** Write a short int as 2 bytes in least-significant byte first order. */
	CORE_I3D_DLLEXPORT void LSBFirstWriteShort(std::ostream& file, unsigned short value);

	/**************************************************************************\
	*
	*                   CPU management
	*
	\**************************************************************************/
	/** The function returns the number of available processros/cores. */
	CORE_I3D_DLLEXPORT size_t GetNumberOfProcessors();

	/**************************************************************************\
	*
	*                   Memory management
	*
	\**************************************************************************/
	/** The function returns the amount of all available memory. */
	CORE_I3D_DLLEXPORT unsigned long long GetFreeMemory();
	
	/** The function returns the amount of all physical memory (RAM). */
	CORE_I3D_DLLEXPORT unsigned long long GetTotalMemory();

	/**************************************************************************\
	*
	*                            Conversion functions
	*
	\**************************************************************************/
	/** Convert 4 bytes in least-significant byte first order into a long int. */
	CORE_I3D_DLLEXPORT unsigned long LSBFirstToLong(byte buf[4]);

	/** Convert a long int into 4 bytes in least-significant byte first order. */
	CORE_I3D_DLLEXPORT void LongToLSBFirst(byte buf[4], unsigned long value);

	/** Convert 2 bytes in least-significant byte first order into a short int. */
	CORE_I3D_DLLEXPORT unsigned short LSBFirstToShort(byte buf[2]);

	/** Convert a short int into 2 bytes in least-significant byte first order. */
	CORE_I3D_DLLEXPORT void ShortToLSBFirst(byte buf[2], unsigned short value);


	//--------------------------------------------------------------------------
	// Container related functions
	//--------------------------------------------------------------------------
	/** Normalization of containers. 
	This template divides values between iterators \p first (included) and \p last 
	(excluded) by the sum of all values between these iterators. */
	template <typename ITERATOR> inline
		void normalize(ITERATOR first, ITERATOR last)
	{
		long double sum=0.0;
		ITERATOR it;

		for (it=first; it<last; it++)
			sum += *it;

		for (it=first; it<last; it++)
			*it /= sum;
	}

	/** Normalization of containers. 
	It is equivalent to normalize(first, first + count). */
	template <typename ITERATOR> inline
		void normalize(ITERATOR first, const long int count)
	{
		normalize(first, first+count);
	}


	//--------------------------------------------------------------------------
	// Image data organization related functions
	//--------------------------------------------------------------------------
	/** Changes the order of lines in the memory. */
	template < class T > CORE_I3D_DLLEXPORT 
	void SwapVert (size_t width, size_t height, T * data);

	/** Changes the order of columns in the memory. */
	template < class T > CORE_I3D_DLLEXPORT 
	void SwapHoriz (size_t width, size_t height, T * data);

	/** Reversing colors in image */
	template <class T> CORE_I3D_DLLEXPORT 
	void ReverseColors(size_t width, size_t height, T *data);

} // i3d namespace

#endif
