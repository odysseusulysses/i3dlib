/*
 * i3dlib - image manipulation library
 *
 * Copyright (C) 2000-2006   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifdef __GNUG__
#pragma implementation
#endif

#include "resolution.h"

namespace i3d {

	/***************************************************************************\
	*
	*                               Resolution
	*
	\***************************************************************************/
	//--------------------------------------------------------------------------
	// Constructors
	//--------------------------------------------------------------------------
	Resolution::Resolution (float x, float y, float z)
		: res (x, y, z)
	{
		*this = res;
	}

	Resolution::Resolution (const Vector3d < float >&v)
	{
		*this = v;
	}

	//--------------------------------------------------------------------------
	// Operators
	//--------------------------------------------------------------------------
	Resolution & Resolution::operator= (const Vector3d < float >&v)
	{
		if (v.x == 0 || v.y == 0 || v.z == 0) {
			Undefine ();
		}
		else {
			defined = true;
			res = v;
		}

		return *this;
	}

	//--------------------------------------------------------------------------
	// Undefine
	//--------------------------------------------------------------------------
	void Resolution::Undefine ()
	{
		defined = false;
		res = DefaultResolution;
	}

	//--------------------------------------------------------------------------
	// Set functions
	//--------------------------------------------------------------------------
	void Resolution::SetX (float x)
	{
		if (defined && x != 0) {
			res.x = x;
		} else {
			Undefine ();
		}
	}

	void Resolution::SetY (float y)
	{
		if (defined && y != 0) {
			res.y = y;
		} else {
			Undefine ();
		}
	}

	void Resolution::SetZ (float z)
	{
		if (defined && z != 0) {
			res.z = z;
		} else {
			Undefine ();
		}
	}

} // i3d namespace
