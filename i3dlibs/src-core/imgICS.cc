/*
 * i3dlib - image manipulation library
 *
 * Copyright (C) 2000-2006   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*	
 * FILE: imgICS.cc
 *
 * I/O routines for ICS images
 *
 * David Svoboda <svoboda@fi.muni.cz> 2005
 * Petr Matula <pem@fi.muni.cz> 2005
 *
 */

/*Suppression of Deprecated warning for Visual studio 8*/
#ifdef _MSC_VER
#if _MSC_VER == 1400
#define _CRT_SECURE_NO_DEPRECATE 1
#endif
#endif

#include "i3d_config.h"

#ifdef WITH_ICS

#include <iostream>
#include "imgICS.h"
#include "toolbox.h"
extern "C" {
#include "libics.h"
}

using namespace std;

namespace i3d {

/***************************************************************************/

// A simple structure that holds the numerical values needed when converting
// from standard (well known) units (cm,mm,inch) to microns. The names of 
// corresponding units are stored as well.
struct T_units2microns
{
	 std::string name;
	 double conversion_factor;
};

// An array instantiating the above structure with specified units names and
// conversion factors. You can clearly see that the ambiguity of the unit names
// is solved by adding a new array item. 
const T_units2microns units[] = 
{
	 {"micrometers", 1.0},
	 {"microns", 1.0},
	 {"um", 1.0},
	 {"cm", 10000.0},
	 {"mm", 1000.0},
	 {"inch", 25400.0}
};

// A constant defining the length of the array above.
const size_t num_of_recognized_units = sizeof(units)/sizeof(T_units2microns);


/***************************************************************************/
void IcsError2IOException(Ics_Error retval)
	{
		if (retval != IcsErr_Ok)
			  throw IOException(std::string("ICS error: ") + 
					 	 std::string(IcsGetErrorText(retval)));
	}

/***************************************************************************/
bool IsRGB(char *str)
{
	if (std::string(str) == "rgb" || std::string(str) == "RGB")
		return true;
	
	return false;
}

/***************************************************************************
 * read the image header information
 * ************************************************************************/
void ICSReader::LoadImageInfo()
{		 
	Ics_DataType dt;
	size_t size[ICS_MAXDIM];
	char label[ICS_STRLEN_TOKEN], order[ICS_STRLEN_TOKEN];
	bool rgb;

	/// the the basic image information
	Ics_Error err = IcsGetLayout(ip, &dt, &ndims, size);
	IcsError2IOException(err);

	size_t bits;
	err = IcsGetSignificantBits(ip, &bits);
	IcsError2IOException(err);

	// initialize the data block
	if (crop)
	{
		for (int i=0; i<ndims; i++)
		{
			 ics_crop_offset[i] = 0; // no offset
			 ics_crop_size[i] = size[i]; // max possible size
		}
	}

	if (ndims > 4)
	{
		throw IOException("ICSReader: At most 3D multi-channel images"\
								" are supported.");
	}

	if (ndims < 2)
	{
		throw IOException("ICSReader: Less than 2D image!?");
	}

	// reading the last label string, find out whether the image is RGB or not
	IcsGetOrder(ip, ndims-1, order, label);
   rgb = IsRGB(order);

	// check the image data type
	switch (dt) 
		{	
			/* integer, unsigned,  8 bpp */
		 	/* integer, signed,    8 bpp */  
		  case Ics_uint8:
		  case Ics_sint8:          
				if (bits == 1)
					 header.itype = BinaryVoxel;
				else if (rgb)
					header.itype = RGBVoxel;
				else
					header.itype = Gray8Voxel;
				break;

			/* integer, unsigned, 16 bpp */
			/* integer, signed,   16 bpp */
			case Ics_uint16:
			case Ics_sint16:         
				if (rgb)
					header.itype = RGB16Voxel;
				else 
					header.itype = Gray16Voxel;
				break;

			/* real,    signed,   32 bpp */
			case Ics_real32:         
				header.itype = FloatVoxel;
				break;

			/* real,    signed,   64 bpp */
			case Ics_real64:         
				header.itype = DoubleVoxel;
				break;

			case Ics_complex32:
				header.itype = Complex32Voxel;
				break;

			case Ics_complex64:
				header.itype = Complex64Voxel;
				break;

			default:
				header.itype = UnknownVoxel;
		}

	if (rgb)
		 ndims--;

	// read out the dimension, resolution, units, and offset
	for (int i=0; i<ndims; i++)
	{
		char ics_units[ICS_STRLEN_TOKEN];
		double offset, spacing;

		err = IcsGetPosition(ip, i, &offset, &spacing, ics_units);
		IcsError2IOException(err);
		
	   header.size[i] = size[i];
		header.offset[i] = offset;

		// set the image resolution (convert units if needed)
		bool found = false;
		for (size_t j=0; j<num_of_recognized_units && !found; j++)
		{
			 if (units[j].name == std::string(ics_units))
			 {
				  // if resolution had not been defined yet, create it
				  if (header.resolution == NULL)
				  {
						header.resolution = new Vector3d<float>(1.0f,1.0f,1.0f);
				  }

				  // conversion between units:
				 (*(header.resolution))[i] = 
							1.0/(spacing*units[j].conversion_factor);

				 found = true;
			 }
		}

	}

	#ifdef I3D_DEBUG
	    std::cerr << "ICSReader::" << std::endl;
		std::cerr << " - voxel type: " << VoxelTypeToString(header.itype) << std::endl;
		std::cerr << " - ndims: " << ndims << std::endl;

		for (int i=0; i<ndims; i++)
		{
			IcsGetOrder(ip, i, order, label);
			std::cerr << "  ---> " << order << " : " << label << std::endl;
		}
		std::cerr << " - size: " << header.size << std::endl;
		std::cerr << " - offset: " << header.offset << std::endl;

		if (header.resolution)
			std::cerr << " - resolution: " << *(header.resolution) << std::endl;

		if (crop)
		{
			 std::cerr << " - crop offset: " << crop->offset << std::endl;
			 std::cerr << " - crop size: " << crop->size << std::endl;
		}
	#endif
}

/***************************************************************************
 * load the RGB data
 * ************************************************************************/
template <class C> void ICSReader::LoadRGBData(RGB_generic<C> *data)
{
  Ics_Error err;
  size_t channelsize;
  
   if (crop)
		 channelsize = crop->size.x * crop->size.y * crop->size.z;
	else
		 channelsize = header.size.x * header.size.y * header.size.z;

  size_t bufsize = 3 * channelsize * sizeof(C);

  // prepare the memory buffer
  C *aux = new C[3*channelsize],
    *R_buf = aux, 
	 *G_buf = R_buf + channelsize, 
	 *B_buf = G_buf + channelsize;

  if (crop)
    {
		  // change the crop settings
		for (int k=0; k<ndims; k++)
		{
			 ics_crop_offset[k] = crop->offset[k];
			 ics_crop_size[k] = crop->size[k];
		}

      err = IcsGetROIData (ip, ics_crop_offset, ics_crop_size, 
									NULL, aux, bufsize);
    }
  else
    {
      err = IcsGetData (ip, aux, bufsize);
    }

  // reshape the data in the memory buffer
  for (size_t i = 0; i < channelsize; i++)
    {
		data->red = *(R_buf++);
		data->green = *(G_buf++);
		data->blue = *(B_buf++);

		data++;
    }

  // dispose the memory buffer
  delete [] aux;
}

/***************************************************************************
 * load the data
 * ************************************************************************/
template <class T> void ICSReader::LoadData(T *data)
{
	Ics_Error err;
   size_t total_size;
  
   if (crop)
		 total_size = crop->size.x * crop->size.y * crop->size.z;
	else
		 total_size = header.size.x * header.size.y * header.size.z;
	
	size_t bufsize = total_size*sizeof(T);

	if (channel != -1)
	{
		err = IcsSkipDataBlock(ip, bufsize*channel);
		IcsError2IOException(err);
	} 

	if (crop)
	{
		// change the crop settings
		for (int k=0; k<ndims; k++)
		{
			 ics_crop_offset[k] = crop->offset[k];
			 ics_crop_size[k] = crop->size[k];
		}
		
		err = IcsGetROIData(ip, ics_crop_offset, ics_crop_size, 
								  NULL, data, bufsize);
	}
	else
	{ 
		 err = IcsGetData(ip, data, bufsize);
	}

	IcsError2IOException(err);
}

//--------------------------------------------------------------------
// specialization for BINARY type
//--------------------------------------------------------------------
void ICSReader::LoadBinData(BINARY *data)
{
	Ics_Error err;
   size_t total_size;
  
   if (crop)
		 total_size = crop->size.x * crop->size.y * crop->size.z;
	else
		 total_size = header.size.x * header.size.y * header.size.z;
	
	size_t bufsize = total_size*sizeof(unsigned char);
	unsigned char *buffer = new unsigned char [total_size];

	if (channel != -1)
	{
		err = IcsSkipDataBlock(ip, bufsize*channel);
		IcsError2IOException(err);
	} 

	if (crop)
	{
		// change the crop settings
		for (int k=0; k<ndims; k++)
		{
			 ics_crop_offset[k] = crop->offset[k];
			 ics_crop_size[k] = crop->size[k];
		}
		
		err = IcsGetROIData(ip, ics_crop_offset, ics_crop_size, 
								  NULL, buffer, bufsize);
	}
	else
	{ 
		 err = IcsGetData(ip, buffer, bufsize);
	}

	IcsError2IOException(err);

	for (size_t i=0; i<total_size; i++)
	{
		 data[i] = ((buffer[i] == 1) ? BINARY(1) : BINARY(0));
	}

	delete [] buffer;
}

/***************************************************************************
 * constructor
 * ************************************************************************/
ICSReader::ICSReader(const char *fname, const VOI<PIXELS> *voi) : 
	ImageReader(fname, voi) 
{
	Ics_Error err = IcsOpen(&ip, fname, "r");
	IcsError2IOException(err);

	if (crop)
	{
		ics_crop_offset = new size_t[ICS_MAXDIM];
		ics_crop_size = new size_t[ICS_MAXDIM];
	}

	/** Be careful about system locales! If these are set
	* incorrectly, the reading of ICS will fail! */
};

/***************************************************************************
 * destructor
 * ************************************************************************/
ICSReader::~ICSReader()
{	
	Ics_Error err = IcsClose(ip);
	IcsError2IOException(err);	

	if (crop)
	{
		 delete [] ics_crop_offset;
		 delete [] ics_crop_size;
	}
}

/***************************************************************************
 * read some header information
 * ************************************************************************/
size_t ICSReader::GetFileNames(FileList &fl)
{
	fl.clear();

	// anyway push the IDS name
	fl.push_back(header.name);

	if (ip->version == 0)
		throw IOException("ICSReader: bad ICS file");

	if (ip->version == 1)
	{
		// in version 1.0 the data IDS file was requested:
		std::string path, name, ext;
		SplitFilename(header.name.c_str(), path, name, ext);
		fl.push_back(path+name+".ids");
	}
	else // version 2.0 or newer
	{
		std::string datafile(ip->srcFile);
		if (datafile != header.name)
		{
			fl.push_back(datafile);
		}
	}
	
	return fl.size();
}

/***************************************************************************
 * save image header information
 * ************************************************************************/
void ICSWriter::SaveImageInfo()
{
	size_t size[ICS_MAXDIM];
	bool rgb = false;
	Ics_Error err;

	// implicit image dimension
	ndims = 3;

	if (header.size.z == 1)
	{
		ndims--;

		if (header.size.y == 1)
			 ndims--;
	}

	for (int i=0; i<ndims; i++)
		 size[i] = header.size[i];

	// select the voxel type
	Ics_DataType dt;
	switch (header.itype)
	{
		case BinaryVoxel:
			dt = Ics_uint8;
			break;
		case Gray8Voxel: 
			dt = Ics_uint8;
			break;
		case Gray16Voxel:
			dt = Ics_uint16;
			break;
		case FloatVoxel:
			dt = Ics_real32;
			break;
		case DoubleVoxel:
			dt = Ics_real64;
			break;
		case RGBVoxel:
			dt = Ics_uint8;
			rgb = true;
			break;
		case RGB16Voxel:
			dt = Ics_uint16;
			rgb = true;
			break;
		case Complex32Voxel:
			dt = Ics_complex32;
			break;
		case Complex64Voxel:
			dt = Ics_complex64;
			break;
		default:
			dt = Ics_unknown;
	}

	// If the image contains RGB channels, one more channel (color) has to
	// be added
	if (rgb)
	{
	   size[ndims] = 3;
		err = IcsSetLayout (ip, dt, ndims+1, size);
		IcsError2IOException(err);

		err = IcsSetPosition(ip, ndims, 0.0, 1.0, "undefined");
		IcsError2IOException(err);

		err = IcsSetOrder(ip, ndims, "RGB", "RGB");
		IcsError2IOException(err);
	}
	else
	{
		err = IcsSetLayout (ip, dt, ndims, size);
		IcsError2IOException(err);
	}

	// Select the significant bits
	switch (header.itype)
	{
		case BinaryVoxel:
				  err = IcsSetSignificantBits(ip, 1);
				  break;
		case Gray8Voxel: 
				  err = IcsSetSignificantBits(ip, 8);
				  break;
		case Gray16Voxel:
				  err = IcsSetSignificantBits(ip, 16);
				  break;
		case FloatVoxel:
				  err = IcsSetSignificantBits(ip, 32);
				  break;
		case RGBVoxel:
				  err = IcsSetSignificantBits(ip, 8);
				  break;
		case RGB16Voxel:
				  err = IcsSetSignificantBits(ip, 16);
				  break;
		case Complex32Voxel:
				  err = IcsSetSignificantBits(ip, 32);
				  break;
		case Complex64Voxel:
				  err = IcsSetSignificantBits(ip, 64);
				  break;
		default:;
	}

	IcsError2IOException(err);

	// Save the resolution, unit and offset of the image
	for (int i=0; i<ndims; i++)
	{
		 double spacing, offset = header.offset[i];

		 if (header.resolution)
		 {
			  spacing = 1.0/((*(header.resolution))[i]);
			  err = IcsSetPosition(ip, i, offset, spacing, "micrometers");
		 }
		 else
		 {
			  spacing = 1.0;
			  err = IcsSetPosition(ip, i, offset, spacing, NULL);
		 }
					
		IcsError2IOException(err);
	}

	// whether the image should be compressed (implicitly) or not
	if (header.compression)
	{
		err = IcsSetCompression(ip, IcsCompr_gzip, 6);
		IcsError2IOException(err);
	}

	// add the string who created this image
	err = IcsAddHistoryString(ip, "software", "i3dlib");
	IcsError2IOException(err);
}

/***************************************************************************
 * save RGB image data
 * ************************************************************************/
template <class C> void ICSWriter::SaveRGBData(const RGB_generic<C> *data)
{
	size_t channelsize = header.size.x * header.size.y * header.size.z;
	size_t bufsize = 3*channelsize*sizeof(C);

  // prepare the memory buffer
  C *aux = new C[3*channelsize],
    *R_buf = aux, 
	 *G_buf = R_buf + channelsize, 
	 *B_buf = G_buf + channelsize;

  // reshape the data in the memory buffer
  for (size_t i = 0; i < channelsize; i++)
    {
		*(R_buf++) = data->red;
		*(G_buf++) = data->green;
		*(B_buf++) = data->blue;

		data++;
    }

	Ics_Error err = IcsSetData(ip, aux, bufsize);
	IcsError2IOException(err);

	err = IcsClose(ip);
	IcsError2IOException(err);

	// release memory buffer
	delete[]aux;
}

/***************************************************************************
 * save image data
 * ************************************************************************/
template <class T> void ICSWriter::SaveData(const T *data)
{
	size_t sz = header.size.x * header.size.y * header.size.z;
	size_t bufsize = sz * sizeof(T);

	Ics_Error err = IcsSetData(ip, data, bufsize);
	IcsError2IOException(err);

	err = IcsClose(ip);
	IcsError2IOException(err);
}

//---------------------------------------------------------------------
// specialization for BINARY image data
//---------------------------------------------------------------------
void ICSWriter::SaveBinData(const i3d::BINARY *data)
{
	size_t sz = header.size.x * header.size.y * header.size.z;
	unsigned char *buffer = new unsigned char [sz];

	for (size_t i=0; i<sz; i++)
	{
		 buffer[i] = ((data[i] == i3d::BINARY(1)) ? 1 : 0);
	}

	Ics_Error err = IcsSetData(ip, buffer, sz*sizeof(unsigned char));
	IcsError2IOException(err);

	err = IcsClose(ip);
	IcsError2IOException(err);

	delete [] buffer;
}

/***************************************************************************
 * constructor
 * ************************************************************************/
ICSWriter::ICSWriter(const char *fname): ImageWriter(fname) 
{
	Ics_Error err = IcsOpen(&ip, fname, "w2");
	IcsError2IOException(err);
};

/***************************************************************************
 * destructor
 * ************************************************************************/
ICSWriter::~ICSWriter()
{
	 // empty function now
}


}

#endif
