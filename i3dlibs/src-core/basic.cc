/*
 * i3dlib - image manipulation library
 *
 * Copyright (C) 2000-2006   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


/** \file  basic.cc  
\brief Basic types and macros - implementation. 

\author Petr Mejzl�k (mejzlik@fi.muni.cz) 2000
\author Petr Matula (pem@fi.muni.cz) 2006
*/


#ifdef __GNUG__
#pragma implementation
#endif

#include "basic.h"
#include "toolbox.h"
#include <iostream>

namespace i3d {
	using namespace std;

	/***************************************************************************\
	*
	*                            Basic voxel types
	*
	\***************************************************************************/
	//--------------------------------------------------------------------------
	// ColorInversion
	//--------------------------------------------------------------------------
	template <class T> T ColorInversion(const T &v)
	{
		 return T(std::numeric_limits<T>::max() - v);
	} 

	template <> CORE_I3D_DLLEXPORT BINARY ColorInversion(const BINARY &v)
	{
		 return ((v == BINARY(1)) ? BINARY(0) : BINARY(1));
	}

	//--------------------------------------------------------------------------
	// Explicit instantiations
	//--------------------------------------------------------------------------
	template CORE_I3D_DLLEXPORT GRAY8 ColorInversion(const GRAY8 &);
	template CORE_I3D_DLLEXPORT GRAY16 ColorInversion(const GRAY16 &);
	template CORE_I3D_DLLEXPORT RGB ColorInversion(const RGB &);
	template CORE_I3D_DLLEXPORT RGB16 ColorInversion(const RGB16 &);
	template CORE_I3D_DLLEXPORT float ColorInversion(const float &);

	//--------------------------------------------------------------------------
	// RGB generic
	//--------------------------------------------------------------------------
	template <class T>
		T RGB_generic<T>::GetComponent(int i)
	{
		stringstream str;
		switch(i)
		{
			case 0: return red;
			case 1: return green;
			case 2: return blue;
			default: str << "RGB_generic::GetComponent: The component with index " << i 
						 << " is not valid!";
				     throw InternalException(str.str());
		}
	}

	//--------------------------------------------------------------------------
	// RGB generic - explicit instantiations
	//--------------------------------------------------------------------------
	template class CORE_I3D_DLLEXPORT RGB_generic<GRAY8>;
	template class CORE_I3D_DLLEXPORT RGB_generic<GRAY16>;

	/***************************************************************************\
	*
	*                                 Exceptions
	*
	\***************************************************************************/
	//--------------------------------------------------------------------------
	// Output operator - IOException
	//--------------------------------------------------------------------------
	ostream& operator<<(ostream& o, const IOException& e) 
	{
		o << "I3D library I/O error. " << e.what; 
		return o;
	}

	//--------------------------------------------------------------------------
	// Output operator - InternalException
	//--------------------------------------------------------------------------
	ostream& operator<<(ostream& o, const InternalException& e) 
	{
		o << "I3D library internal error. " << e.what; 
		return o;
	}

}
