cmake_minimum_required(VERSION 2.4)
############################################################################
#
#  i3dlib - image manipulation library
#
#  Copyright (C) 2000-2006   Centre for Biomedical Image Analysis (CBIA)
# 
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Library General Public
#  License as published by the Free Software Foundation; either
#  version 2 of the License, or (at your option) any later version.
# 
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Library General Public License for more details.
# 
#  You should have received a copy of the GNU Library General Public
#  License along with this library; if not, write to the Free
#  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#
#############################################################################
#
# FILE: cross platform CMake configuration script
#
# David Svoboda <svoboda@fi.muni.cz>
#
# i3dlib - image manipulation library
#############################################################################

# project name
PROJECT (I3D)

# set the compilation way
SET(CMAKE_VERBOSE_MAKEFILE ON CACHE STRING 
	"Tracing the compilation process." FORCE)


############################################################################
# basic decisions
############################################################################

# to know the names of the targets so that we can use them later
SET (CORE_LIBRARY_NAME "i3dcore" CACHE STRING "The name of the core library.")
SET (ALGO_LIBRARY_NAME "i3dalgo" CACHE STRING "The name of the algorithms library.")

# choose between static/shared versions of libraries
OPTION (CORE_BUILD_SHARED_LIBS "Static/Shared core library? In Windows both CORE_BUILD_ and ALGO_BUILD_ are bond together, the latter has higher priority." ON)
OPTION (ALGO_BUILD_SHARED_LIBS "Static/Shared algorithms library? In Windows both CORE_BUILD_ and ALGO_BUILD_ are bond together, the latter has higher priority." ON)
IF (WIN32 AND NOT ${ALGO_BUILD_SHARED_LIBS} MATCHES ${CORE_BUILD_SHARED_LIBS})
	# i3dalgo versioning has higher priority and controls the behaviour
	SET (CORE_BUILD_SHARED_LIBS ${ALGO_BUILD_SHARED_LIBS} CACHE BOOL
		"Static/Shared algorithms library? In Windows both CORE_BUILD_ and ALGO_BUILD_ are bond together, the later has higher priority." FORCE)
	MESSAGE("Changing ALGO/CORE_BUILD_SHARED_LIBS to: CORE=" ${CORE_BUILD_SHARED_LIBS} ", ALGO=" ${ALGO_BUILD_SHARED_LIBS})
ENDIF (WIN32 AND NOT ${ALGO_BUILD_SHARED_LIBS} MATCHES ${CORE_BUILD_SHARED_LIBS})

# set these paths for general environment
SET (GLOBAL_ALT_INC_DIR "not_used" CACHE PATH "Path to headers of supplementary libraries.")
SET (GLOBAL_ALT_LIB_DIR "not_used" CACHE PATH "Directory with supplementary libraries.")
FIND_PROGRAM(GLOBAL_DOXYGEN_PROGRAM doxygen PATHS ${BIN_DIRS} DOC "Path to doxygen program.")

# unix-related features
IF (UNIX)
	# we can be a bit more specific in unix
	SET(GLOBAL_ALT_INC_DIR /usr/local/include /usr/include)
	SET(GLOBAL_ALT_LIB_DIR /usr/local/lib /usr/lib)

	# Electric fence library
	OPTION(GLOBAL_USE_EFENCE "Do you want to use electric fence tool (efence library)?" OFF)
	IF (GLOBAL_USE_EFENCE)
		FIND_LIBRARY(GLOBAL_LIB_EFENCE "efence" ${GLOBAL_ALT_LIB_DIR}
			DOC "Localization of efence library.")

		SET(LIBS ${LIBS} ${GLOBAL_LIB_EFENCE})
	ENDIF (GLOBAL_USE_EFENCE)
ENDIF (UNIX)

# install options
SET (RIGHTS_FILE OWNER_READ OWNER_WRITE GROUP_READ GROUP_WRITE WORLD_READ)
SET (RIGHTS_LIB ${RIGHTS_FILE} OWNER_EXECUTE GROUP_EXECUTE WORLD_EXECUTE)
SET (RIGHTS_DIR ${RIGHTS_LIB} SETGID)

# presume we don't want to build shared/dynamic version of any library
# if shared version of some library is demanded, it is turned on _locally_ in its CMakeLists file
#SET(BUILD_SHARED_LIBS OFF)

# build the core library anyway
SUBDIRS (src-core)

# build the algorithm library and (optionally) examples?
OPTION (GLOBAL_BUILD_ALGO "Do you want to build the algorithms library?" ON)
IF (GLOBAL_BUILD_ALGO)
	OPTION(GLOBAL_BUILD_EXAMPLES "Do you want to build examples as well?" OFF)
	IF (GLOBAL_BUILD_EXAMPLES)
		SUBDIRS (src examples)
	ELSE (GLOBAL_BUILD_EXAMPLES)
		SUBDIRS (src)
	ENDIF (GLOBAL_BUILD_EXAMPLES)
ENDIF (GLOBAL_BUILD_ALGO)

