#ifndef WATERSHED_CGS_H
#define WATERSHED_CGS_H

namespace ivLab3D {
	template<class T>
	void WatershedCGS(const T* Gradient, const int Width, const int Height, const int Depth, T* Output, const int Connectivity);
}

#endif // #ifndef WATERSHE_CGS_H
