#ifdef I3D_DEBUG
#include <iostream>
#endif // #ifdef I3D_DEBUG

#include <vector>
#include <limits>
#include <set>
#include <map>
#include <queue>
#include "i3dassert.h"
#include "watershed_cgs.h"
#include "neighbor.h"
#include "queue.h"
#include "watershed_common.h"

namespace ivLab3D {

	// -------------------------------------------------------------------------- //

	/// handles all the operations necessary for the contour dynamics computation
	template<typename T>
	class CBasinManager
	{
	private:
		/// holds all the information about one basin
		struct SBasin
		{
			/// minimum at witch this basin was created
			T minimum;
			/// dynamics of this basin computed when it has been flooded by some other basin
			T dynamics;
			/// pointer to the basin with the lower minimum that flooded this one
			SBasin *floodedBy;
			/// helper flag used while searching for the common flooder of some two basins
			bool visited;
			/// set of basins with higher pointer that this basin meat already
			std::set<SBasin*> joined;

			/// initializes new basin on specified level
			SBasin(T minimum_): minimum(minimum_), dynamics(std::numeric_limits<T>::max()), floodedBy(NULL), visited(false) {}

			/// returns by witch basin is this one flooded (recursively)
			SBasin* FloodedBy()
			{
				if (floodedBy)
					return floodedBy->FloodedBy();
				else
					return this;
			}

			/// returns by witch basin is this one flooded (recursively) requesting minimum dynamics value
			SBasin* FloodedBy(T minDyn)
			{
				// allow the same dynamics just in the case we whoud have to return noone - be careful while connecting the tree not to connect some node to itself
				if (minDyn < dynamics)
					return this;
				else
				{
					if (!floodedBy)
					{
						if (minDyn == dynamics)
							return this;
						else
							return NULL;
					}
					else
						return floodedBy->FloodedBy(minDyn);
				}
			}

			/// returns predecessor of given basin or basin itself when we have no other choice, it also clears the visited flag
			SBasin* GetPredecessorOf(SBasin *basin)
			{
				visited = false;
				if (floodedBy == basin || this == basin)
					return this;
				if (floodedBy)
					return floodedBy->GetPredecessorOf(basin);
				return NULL;
			}

			/// clears the visited flag as long as it is set while walking the flooding list
			void ClearVisited()
			{
				visited = false;
				if (floodedBy && floodedBy->visited)
					floodedBy->ClearVisited();				
			}
		};

		/// vector of all the basins (label - 1 == index into the vector)
		std::vector<SBasin*> basins;
		/// map of all the already computed contour dynamics
		std::map<std::pair<int, int>, T> dynamics;
		// TODO zkusit ud�lat jako hodnotu p�ry, kdy to bude ��kat, jestli se u� baz�ny potkaly a druh� ��st hodnota a pou��t to na zapamatov�n� toho, co je te� v joined u baz�n�
		// TODO zkusit hash mapu

	public:
		/// frees all the allocated resources
		~CBasinManager()
		{
			for (typename std::vector<SBasin*>::size_type i = basins.size(); i > 0; --i)
				delete basins[i - 1];
		}

		/// creates new basin - basins should be created in the same order as their labels raise
		void CreateBasin(T level)
		{
			basins.push_back(new SBasin(level));
		}

		/// computes basin dynamics for one of the basins when two basins meat and creates the part of the flooding list
		void ComputeDynamics(int label1, int label2, T level)
		{
			I3DASSERT_BT(false, "text of the message");
			SBasin *p0 = basins[label1 - 1];
			SBasin *q0 = basins[label2 - 1];
			// do not do the computation for basins that meat before - basin with the lower pointer stores the basin with the higher one
			if (p0 < q0)
			{
				if (p0->joined.find(q0) != p0->joined.end())
					return;
				else
					p0->joined.insert(q0);
			}
			else
			{
				if (q0->joined.find(p0) != q0->joined.end())
					return;
				else
					q0->joined.insert(p0);
			}
			// find proper members of connected componentes and do the computations for them
			SBasin *pn, *qn, *flooding0, *floodedn;
			pn = p0->FloodedBy();
			qn = q0->FloodedBy();
			// when parts of the two components that meat before meat again, we do not need to do anything
			if (pn == qn)
				return;
			if (pn->minimum < qn->minimum)
			{
				floodedn = qn;
				flooding0 = p0;
			}
			else if (pn->minimum > qn->minimum)
			{
				floodedn = pn;
				flooding0 = q0;
			}
			else
			{
				// lets do the choice that when two basins have the same minimum, the one with the lower pointer floods the one with the higher pointer
				// it doesn't matter that during the next run, situation can be completely different, final result should be the same
				if (pn < qn)
				{
					floodedn = qn;
					flooding0 = p0;
				}
				else
				{
					floodedn = pn;
					flooding0 = q0;
				}
			}
			I3DASSERT_B(floodedn->dynamics == std::numeric_limits<T>::max());
			I3DASSERT_B(floodedn->floodedBy == NULL);
			T candidateDynamics = level - floodedn->minimum; 
			SBasin *candidateBasin = flooding0->FloodedBy(candidateDynamics);
			if (candidateBasin != floodedn)
			{
				// this check is maybe necessary no more when we check earlier that pn != qn
				floodedn->dynamics = candidateDynamics;
				floodedn->floodedBy = candidateBasin;
			}
		}

		/// returns contour dynamics for the given set of basins touching the contour
		T GetDynamics(const std::set<int> &labels)
		{
			T result = 0;
			// TODO zkusit pro dvouprvkov� mno�iny separ�tn� v�tev, kter� bude rychlej��
			for (std::set<int>::const_iterator first = labels.begin(); first != --labels.end(); ++first)
			{
				std::set<int>::const_iterator second = first;
				for (++second; second != labels.end(); ++second)
				{
					// try to lookup the value in already computed ones
					typename std::map<std::pair<int, int>, T>::iterator singleResult = dynamics.find(std::pair<int, int>(*first, *second));
					if (singleResult != dynamics.end())
					{
						if (result < singleResult->second)
							result = singleResult->second;
					}
					// when no result found, compute it
					else
					{
						SBasin *basin1 = basins[*first - 1];
						SBasin *basin2 = basins[*second - 1];
						// search for the common flooder of the two basins and it's predecessors
						SBasin *flooding1 = basin1;
						SBasin *flooding2 = basin2;
						SBasin *commonFlooder;
						while (true)
						{
							flooding1->visited = flooding2->visited = true;
							if (flooding1->floodedBy)
							{
								if (flooding1->floodedBy->visited)
								{
									commonFlooder = flooding1->floodedBy;
									flooding2 = basin2->GetPredecessorOf(commonFlooder);
									basin1->GetPredecessorOf(commonFlooder);
									break;
								}
								flooding1 = flooding1->floodedBy;
							}
							if (flooding2->floodedBy)
							{
								if (flooding2->floodedBy->visited)
								{
									commonFlooder = flooding2->floodedBy;
									flooding1 = basin1->GetPredecessorOf(commonFlooder);
									basin2->GetPredecessorOf(commonFlooder);
									break;
								}
								flooding2 = flooding2->floodedBy;
							}
							// the case when the flooding list depth is the same from the both sides
							if (flooding1 == flooding2)
							{
								commonFlooder = flooding1;
								flooding1 = basin1->GetPredecessorOf(commonFlooder);
								flooding2 = basin2->GetPredecessorOf(commonFlooder);
								break;
							}
						}
						I3DASSERT_B(flooding1);
						I3DASSERT_B(flooding2);
						I3DASSERT_B(commonFlooder);
						commonFlooder->ClearVisited();
						// use the higher dynamics found
						T singleResult = std::max(flooding1->dynamics, flooding2->dynamics);
						if (result < singleResult)
							result = singleResult;
						dynamics[std::pair<int, int>(*first, *second)] = singleResult;
					}
				}
			}
			return result;
		}

#ifdef I3D_DEBUG
		/// checks the structure of the tree - prints out the labels of the basins which are not flooded and the labels of the basins which are not flooded and not flooding
		void CeckTree()
		{
			std::set<SBasin*> notFlooded;
			std::cout << "Checking tree integrity, printing the list of labels of basins which are not flooded and the labels of the basins which are not flooded and not flooding" << std::endl;
			for (typename std::vector<SBasin*>::size_type i = basins.size(); i > 0; --i)
			{
				if (!basins[i - 1]->floodedBy)
				{
					notFlooded.insert(basins[i - 1]);
					std::cout << i << ", ";
				}
			}
			std::cout << "end" << std::endl;
			for (typename std::vector<SBasin*>::size_type i = basins.size(); i > 0; --i)
				notFlooded.erase(basins[i - 1]->floodedBy);
			for (typename std::vector<SBasin*>::size_type i = basins.size(); i > 0; --i)
			{
				if (notFlooded.find(basins[i - 1]) != notFlooded.end())
				{
					std::cout << i << ", ";
				}
			}
			std::cout << "end" << std::endl;
		}

		/// returns the number of the labels we know about
		size_t GetNumOfLabels()
		{
			return basins.size();
		}
#endif // #ifdef I3D_DEBUG
	};

	// ---------------------------------------------------------------------------//

	template<class T, int Connectivity> static
		void GenericWatershedCGS(const T* OldGradient, const int Width, const int Height, const int Depth, T* Output)
	{
		TNeighbor<Connectivity> Neighbor(1, Width+2, (Width+2)*(Height+2));
		TQueue<int> Queue(Width*Height*Depth);

		T *Gradient, level, min_level, gradient;
		int *Rank, *Dist = 0, *Label, LabelCount;
		int offset, index, begin, end, pixel, dist, label, neighbor;

		// Surrounding the gradient image with sentinesl.
		Gradient = MakeNewGradient<T>(OldGradient, Width, Height, Depth);
		Rank = Sort<T>(Gradient, Width, Height, Depth);

		// Memory allocation
		Label = new int[(Width+2)*(Height+2)*(Depth+2)];
		Dist  = new int[(Width+2)*(Height+2)*(Depth+2)];
		I3DASSERT_ALWAYS_ET(Label && Dist, "Cannot allocate memory for labels or distances on plateaus");

		// Initialization
		// Setting the sentinels as ridges
		memset(Label+0, -1, sizeof(int)*(Width+2)*(Height+2)); // RIDGE_LABEL == -1
		offset = (Width+2)*(Height+2);
		for (int index_z = 1; index_z<=Depth; index_z++)
		{
			memset(Label+offset, -1, sizeof(int)*(Width+2)); // RIDGE_LABEL == -1
			offset += Width+2;

			for(index=1; index<=Height; index++) {
				memset(Label+offset+1, 0, sizeof(int)*Width); // NULL_LABEL == 0
				Label[offset+0] = Label[offset+Width+1] = RIDGE_LABEL;
				offset += Width+2;
			}

			memset(Label+offset, -1, sizeof(int)*(Width+2)); // RIDGE_LABEL == -1
			offset += Width+2;
		}
		memset(Label+offset, -1, sizeof(int)*(Width+2)*(Height+2)); // RIDGE_LABEL == -1
		memcpy(Dist, Label, sizeof(int)*(Width+2)*(Height+2)*(Depth+2));
		LabelCount = 0;
		CBasinManager<T> basinManager;

		// For easy computation of the flooding list, order invariancy causes problems, so we do two runs
		// First run for the flooding list computation
		for(begin=0; begin<Width*Height*Depth; begin=end) {
			Queue.Reset();
			// Search for the growable pixels
			level = Gradient[Rank[begin]];
			for(index=begin; index<Width*Height*Depth; index++) {
				if(Gradient[pixel=Rank[index]] != level) {
					break;
				}
				min_level = level;
				label = NULL_LABEL;
				for(Neighbor.Begin(); !Neighbor.End(); Neighbor.Next()) {
					neighbor = Neighbor.Where(pixel);
					gradient = Gradient[neighbor];
					if(gradient >= level) {
						continue;
					}
					// two basins meet? - compute their dynamics
					if(label != NULL_LABEL && label != Label[neighbor] && Label[neighbor] != RIDGE_LABEL) {
						// yes, basin label and Label[neighbor] meat each other
						basinManager.ComputeDynamics(label, Label[neighbor], level);
					}
					if(gradient < min_level) {
						// The label of the steepest direction should be chosen.
						label = Label[neighbor];
						min_level = gradient;
						continue;
					}
				}
				if(label != NULL_LABEL) {
					Label[pixel] = label;
					Queue.In(pixel);
				}
			}
			end = index;

			// Flooding in the flat regions
			while(!Queue.IsEmpty()) {
				Queue.Out(pixel);
				label = Label[pixel];
				for(Neighbor.Begin(); !Neighbor.End(); Neighbor.Next()) {
					neighbor = Neighbor.Where(pixel);
					if(Label[neighbor] != NULL_LABEL && Label[neighbor] != label && Label[neighbor] != RIDGE_LABEL) {
						// compute dynamics for basins with the different labels that meat each other
						basinManager.ComputeDynamics(label, Label[neighbor], level);
					}
					if(Gradient[neighbor] == level) { 
						// `neighbor' is in the flat regions.
						if(Label[neighbor] == NULL_LABEL) { 
							// `neighbor' has not been visited.
							Label[neighbor] = label;
							Queue.In(neighbor);
							continue;
						}
					}
				}
			}

			// Creating new markers in the minimal regions
			for(index=begin; index<end; index++) {
				pixel = Rank[index];
				if(Label[pixel] != NULL_LABEL) {
					continue;
				}
				label = ++LabelCount;
				Label[pixel] = label;
				basinManager.CreateBasin(level);
				Queue.Reset();
				Queue.In(pixel);
				while(!Queue.IsEmpty()) {
					Queue.Out(pixel);
					for(Neighbor.Begin(); !Neighbor.End(); Neighbor.Next()) {
						neighbor = Neighbor.Where(pixel);
						if(Label[neighbor] == NULL_LABEL && Gradient[neighbor] == level) {
							Label[neighbor] = label;
							Queue.In(neighbor);
						}
					}                
				}
			}
		}

		// Second run for the proper ridge labeling
		LabelCount = 0;
		memcpy(Label, Dist, sizeof(int)*(Width+2)*(Height+2)*(Depth+2));
		for(begin=0; begin<Width*Height*Depth; begin=end) {
			Queue.Reset();
			// Search for the growable pixels
			level = Gradient[Rank[begin]];
			for(index=begin; index<Width*Height*Depth; index++) {
				if(Gradient[pixel=Rank[index]] != level) {
					break;
				}
				min_level = level;
				label = NULL_LABEL;
				for(Neighbor.Begin(); !Neighbor.End(); Neighbor.Next()) {
					neighbor = Neighbor.Where(pixel);
					gradient = Gradient[neighbor];
					if(gradient >= level) {
						continue;
					}
					if(gradient < min_level) {
						// The label of the steepest direction should be chosen.
						label = Label[neighbor];
						min_level = gradient;
						continue;
					}
					if(gradient == min_level && Label[neighbor] != label) {
						// There are multiple markers reachable to `pixel'.
						label = RIDGE_LABEL;
					}
				}
				if(label != NULL_LABEL) {
					Label[pixel] = label;
					Queue.In(pixel);
				}
			}
			end = index;

			// Flooding in the flat regions
			while(!Queue.IsEmpty()) {
				Queue.Out(pixel);
				label = Label[pixel];
				dist = Dist[pixel] + 1;
				for(Neighbor.Begin(); !Neighbor.End(); Neighbor.Next()) {
					neighbor = Neighbor.Where(pixel);
					if(Gradient[neighbor] == level) { 
						// `neighbor' is in the flat regions.
						if(Label[neighbor] == NULL_LABEL) { 
							// `neighbor' has not been visited.
							Label[neighbor] = label;
							Queue.In(neighbor);
							Dist[neighbor] = dist;
							continue;
						}
						if(Dist[neighbor] == dist && Label[neighbor] != label) {
							// There are multiple markers in the same distance to `neighbor'.
							Label[neighbor] = RIDGE_LABEL;
						}
					}
				}
			}

			// Creating new markers in the minimal regions
			for(index=begin; index<end; index++) {
				pixel = Rank[index];
				if(Label[pixel] != NULL_LABEL) {
					continue;
				}
				label = ++ LabelCount;
				Label[pixel] = label;
				Queue.Reset();
				Queue.In(pixel);
				while(!Queue.IsEmpty()) {
					Queue.Out(pixel);
					for(Neighbor.Begin(); !Neighbor.End(); Neighbor.Next()) {
						neighbor = Neighbor.Where(pixel);
						if(Label[neighbor] == NULL_LABEL && Gradient[neighbor] == level) {
							Label[neighbor] = label;
							Queue.In(neighbor);
						}
					}                
				}
			}
		}
		I3DASSERT_B(LabelCount == static_cast<int>(basinManager.GetNumOfLabels()));

		// Create nice borders with contour dynamics valuation between the components
		// When I am on the edge, I give the RIDGE_LABEL to the neighbour with the higher function value
		// Ridges are not directly written into the labels, because we do not want to lost the information about the label
		// We assume that most of the voxels won't be ridges, this is one of the reasons why we do not create labels set for voxel during the first scan
#ifdef I3D_DEBUG
		basinManager.CeckTree();
#endif // #ifdef I3D_DEBUG
		// Using Ridges for additional newly created ridges on different places than we are just checking
		int *Ridges = new int[(Width+2)*(Height+2)*(Depth+2)];
		memset(Ridges, 0, sizeof(int) * (Width+2)*(Height+2)*(Depth+2));
		int *src = Label+(Width+2)*((Height+2)+1)+1;
		int *srcRidges = Ridges+(Width+2)*((Height+2)+1)+1;
		std::set<int> labels;
		std::queue<int> ridgeGrowing;
		std::set<int> ridgeGrowingUsed;
		bool isRidge;
		for (int index_z=0; index_z<Depth; ++index_z) {
			for(int index_y=0; index_y<Height; ++index_y) {
				for(int index_x=0; index_x<Width; ++index_x) {
					label = *src;
					isRidge = false;
					if (label != RIDGE_LABEL && *srcRidges != RIDGE_LABEL) {
						for(Neighbor.BeginForward(); !Neighbor.EndForward(); Neighbor.Next()) {
							neighbor = Neighbor.Where(src - Label);
							if(Label[neighbor] != label && Label[neighbor] != RIDGE_LABEL && Ridges[neighbor] != RIDGE_LABEL) {
								if(Gradient[neighbor] > Gradient[src - Label]) {
									Ridges[neighbor] = RIDGE_LABEL;
								} else {
									isRidge = true;
									break;
								}
							}
						}
					} else {
						isRidge = true;
					}
					// Looking into the neighbourhood of the ridges and getting their dynamics - looking for the closest basins for the dynamics computation
					if(isRidge) {
						labels.clear();
						if(*src != RIDGE_LABEL) {
							labels.insert(*src);
						}
						ridgeGrowing.push(src - Label);
						ridgeGrowingUsed.insert(src - Label);
						while(labels.size() <= 1) {
							int unqueueNo = ridgeGrowing.size();
							for(int i = 0; i < unqueueNo; ++i) {
								int seed = ridgeGrowing.front();
								ridgeGrowing.pop();
								for(Neighbor.Begin(); !Neighbor.End(); Neighbor.Next()) {
									neighbor = Neighbor.Where(seed);
									if(ridgeGrowingUsed.find(neighbor) == ridgeGrowingUsed.end()) {
										ridgeGrowing.push(neighbor);
										ridgeGrowingUsed.insert(neighbor);
									}
									if(Label[neighbor] != RIDGE_LABEL) {
										labels.insert(Label[neighbor]);
									}
								}
							}
						}
						while(!ridgeGrowing.empty())
							ridgeGrowing.pop();
						ridgeGrowingUsed.clear();
						*(Output++) = basinManager.GetDynamics(labels);
					} else {
						*(Output++) = 0;
					}
					++src;
					++srcRidges;
				}
				src += 2;
				srcRidges += 2;
			}
			src += (Width+2)*2+2;
			srcRidges += (Width+2)*2+2;
		}

		// Memory allocated by other functions
		delete[] Rank;
		delete[] Gradient;

		// Memory allocated by this function
		delete[] Dist;
		delete[] Label;
		delete[] Ridges;
	}

	// -------------------------------------------------------------------------- //

	template<class T>
	void WatershedCGS(const T* Gradient, const int Width, const int Height, const int Depth, T* Output, const int Connectivity)
	{
		if(Connectivity == 4) {
			GenericWatershedCGS<T, 4>(Gradient, Width, Height, Depth, Output);
		}
		else if(Connectivity == 8) {
			GenericWatershedCGS<T, 8>(Gradient, Width, Height, Depth, Output);
		}
		else if(Connectivity == 6) {
			GenericWatershedCGS<T, 6>(Gradient, Width, Height, Depth, Output);
		}
		else if(Connectivity == 18) {
			GenericWatershedCGS<T, 18>(Gradient, Width, Height, Depth, Output);
		}
		else if(Connectivity == 26) {
			GenericWatershedCGS<T, 26>(Gradient, Width, Height, Depth, Output);
		}
		else {
			throw "Watershed => invalid connectivity";
		}
	}

	// -------------------------------------------------------------------------- //
	// Instantiation 

	template
		void WatershedCGS<unsigned short>
		(const unsigned short*, const int, const int, const int, unsigned short*, const int);

	template
		void WatershedCGS<unsigned char>
		(const unsigned char*, const int, const int, const int, unsigned char*, const int);

	template
		void WatershedCGS<float>
		(const float*, const int, const int, const int, float*, const int);

	// -------------------------------------------------------------------------- //
}
