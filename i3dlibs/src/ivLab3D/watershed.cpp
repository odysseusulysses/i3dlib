#include "watershed.h"
#include "neighbor.h"
#include "queue.h"
#include "watershed_common.h"

namespace ivLab3D {

// -------------------------------------------------------------------------- //
// -- !!! WARNING !!! -------------------------------------------------------
// If a function template is used instead of the following class template,
// Microsoft Visual C++ 6.0 will generate only one instantiation for a class
// T, no matter `OrderInvariant' is true or false.
// ---------------------------------------------------------------------------//

template<class T, bool OrderInvariant, int Connectivity>
struct GenericWatershed {
    static int* GetLabelImage(const T* OldGradient, const int Width, const int Height, const int Depth, const bool DrawLines);
};

template<class T, bool OrderInvariant, int Connectivity>
int* GenericWatershed<T, OrderInvariant, Connectivity>::
GetLabelImage(const T* OldGradient, const int Width, const int Height, const int Depth, const bool DrawLines)
{
    TNeighbor<Connectivity> Neighbor(1, Width+2, (Width+2)*(Height+2));
    TQueue<int> Queue(Width*Height*Depth);

    T *Gradient, level, min_level, gradient;
    int *Rank, *Dist = 0, *Label, *int_scan, LabelCount;
    int offset, index, begin, end, pixel, dist, label, neighbor;
    
    // Surrounding the gradient image with sentinesl.
    Gradient = MakeNewGradient<T>(OldGradient, Width, Height, Depth);
    Rank = Sort<T>(Gradient, Width, Height, Depth);

    // Memory allocation
    Label = new int[(Width+2)*(Height+2)*(Depth+2)];
    if(OrderInvariant) { // ---> ADDITIONAL CODE FOR ORDER-INVARIANT ALGORITHM
        Dist  = new int[(Width+2)*(Height+2)*(Depth+2)];
    } ////////////////////////////////////////////////////////////////////////
    if(!Label || (!Dist && OrderInvariant)) {
        throw "GenericWatershed => cannot allocate memory";
    }

    // Initialization
    // Setting the sentinels as ridges
    memset(Label+0, -1, sizeof(int)*(Width+2)*(Height+2)); // RIDGE_LABEL == -1
    offset = (Width+2)*(Height+2);
	for (int index_z = 1; index_z<=Depth; index_z++)
	{
		memset(Label+offset, -1, sizeof(int)*(Width+2)); // RIDGE_LABEL == -1
		offset += Width+2;

		for(index=1; index<=Height; index++) {
			memset(Label+offset+1, 0, sizeof(int)*Width); // NULL_LABEL == 0
			Label[offset+0] = Label[offset+Width+1] = RIDGE_LABEL;
			offset += Width+2;
		}

		memset(Label+offset, -1, sizeof(int)*(Width+2)); // RIDGE_LABEL == -1
		offset += Width+2;
	}
    memset(Label+offset, -1, sizeof(int)*(Width+2)*(Height+2)); // RIDGE_LABEL == -1

    if(OrderInvariant) { // ---> ADDITIONAL CODE FOR ORDER-INVARIANT ALGORITHM
        memcpy(Dist, Label, sizeof(int)*(Width+2)*(Height+2)*(Depth+2));
    } ////////////////////////////////////////////////////////////////////////
    LabelCount = 0;

    for(begin=0; begin<Width*Height*Depth; begin=end) {
        
        Queue.Reset();
        // Search for the growable pixels
        level = Gradient[Rank[begin]];
        for(index=begin; index<Width*Height*Depth; index++) {
            if(Gradient[pixel=Rank[index]] != level) {
                break;
            }
            min_level = level;
            label = NULL_LABEL;
            for(Neighbor.Begin(); !Neighbor.End(); Neighbor.Next()) {
                neighbor = Neighbor.Where(pixel);
                gradient = Gradient[neighbor];
                if(gradient >= level) {
                    continue;
                }
                if(gradient < min_level) {
                    // The label of the steepest direction should be chosen.
                    label = Label[neighbor];
                    min_level = gradient;
                    continue;
                }
                if(OrderInvariant) { // ---> ADDITIONAL CODE FOR ORDER-INVARIANT ALGORITHM
                    if(gradient == min_level && Label[neighbor] != label) {
                        // There are multiple markers reachable to `pixel'.
                        label = RIDGE_LABEL;
                    }
                } ////////////////////////////////////////////////////////////////////////
            }
            if(label != NULL_LABEL) {
                Label[pixel] = label;
                Queue.In(pixel);
            }
        }
        end = index;

        // Flooding in the flat regions
        while(!Queue.IsEmpty()) {
            Queue.Out(pixel);
            label = Label[pixel];
            if(OrderInvariant) { // ---> ADDITIONAL CODE FOR ORDER-INVARIANT ALGORITHM
                dist = Dist[pixel] + 1;
            } ////////////////////////////////////////////////////////////////////////
            for(Neighbor.Begin(); !Neighbor.End(); Neighbor.Next()) {
                neighbor = Neighbor.Where(pixel);
                if(Gradient[neighbor] == level) { 
                    // `neighbor' is in the flat regions.
                    if(Label[neighbor] == NULL_LABEL) { 
                        // `neighbor' has not been visited.
                        Label[neighbor] = label;
                        Queue.In(neighbor);
                        if(OrderInvariant) { // ---> ADDITIONAL CODE FOR ORDER-INVARIANT ALGORITHM
                            Dist[neighbor] = dist;
                        } ////////////////////////////////////////////////////////////////////////
                        continue;
                    }
                    if(OrderInvariant) { // ---> ADDITIONAL CODE FOR ORDER-INVARIANT ALGORITHM
                        if(Dist[neighbor] == dist && Label[neighbor] != label) {
                            // There are multiple markers in the same distance to `neighbor'.
                            Label[neighbor] = RIDGE_LABEL;
                        }
                    } ////////////////////////////////////////////////////////////////////////
                }
            }
        }

        // Creating new markers in the minimal regions
        for(index=begin; index<end; index++) {
            pixel = Rank[index];
            if(Label[pixel] != NULL_LABEL) {
                continue;
            }
            label = ++ LabelCount;
            Label[pixel] = label;
            Queue.Reset();
            Queue.In(pixel);
            while(!Queue.IsEmpty()) {
                Queue.Out(pixel);
                for(Neighbor.Begin(); !Neighbor.End(); Neighbor.Next()) {
                    neighbor = Neighbor.Where(pixel);
                    if(Label[neighbor] == NULL_LABEL && Gradient[neighbor] == level) {
                        Label[neighbor] = label;
                        Queue.In(neighbor);
                    }
                }                
            }
        }
    }

    // Memory allocated by other functions
    delete[] Rank;
    delete[] Gradient;

    // Memory allocated by this function
    if(OrderInvariant) { // ---> ADDITIONAL CODE FOR ORDER-INVARIANT ALGORITHM
        delete[] Dist;
    } ////////////////////////////////////////////////////////////////////////

	// Removing the sentinels and drawing watershed lines or just removing the sentinels
	int_scan = Label;
	offset = (Width+2)*(Height+2);
	if (DrawLines) {
		int *src;
		int index_x, index_y;
		for(int index_z = 0; index_z < Depth; ++index_z) {
			offset += Width + 3;
			for(index_y=0; index_y<Height; ++index_y) {
				src = int_scan+offset;
				for(index_x=0; index_x<Width; ++index_x) {
					label = *src;
					if (label != RIDGE_LABEL) {
						for (Neighbor.BeginForward(); !Neighbor.EndForward(); Neighbor.Next()) {
							neighbor = Neighbor.Where(src - Label);
							if (Label[neighbor] != label && Label[neighbor] != RIDGE_LABEL) {
								if (Gradient[neighbor] > Gradient[src - Label]) {
									Label[neighbor] = RIDGE_LABEL;
								} else {
									*src = RIDGE_LABEL;
									break;
								}
							}
						}
					}
					*int_scan = *src;
					int_scan++;
					src++;
				}
				offset += 2;
			}
			offset += Width + 1;
		}
	}
	else {
		for (int index_z = 0; index_z < Depth; index_z++)
		{
			offset += Width + 3;
			for(index=0; index<Height; index++) 
			{
				memcpy(int_scan, int_scan+offset, sizeof(int) * Width);
				offset += 2;
				int_scan += Width;
			}
			offset += Width + 1;
		}
	}

    return Label;
}

// -------------------------------------------------------------------------- //

template<class T>
int* Watershed(const T* Gradient, const int Width, const int Height, const int Depth, const bool OrderInvariant, const int Connectivity, const bool DrawLines)
{
    if(Connectivity == 4) {
        if(OrderInvariant) {
            return GenericWatershed<T, true, 4>::GetLabelImage(Gradient, Width, Height, Depth, DrawLines);
        }
        else {
            return GenericWatershed<T, false, 4>::GetLabelImage(Gradient, Width, Height, Depth, DrawLines);
        }
    }
    else if(Connectivity == 8) {
        if(OrderInvariant) {
            return GenericWatershed<T, true, 8>::GetLabelImage(Gradient, Width, Height, Depth, DrawLines);
        }
        else {
            return GenericWatershed<T, false, 8>::GetLabelImage(Gradient, Width, Height, Depth, DrawLines);
        }
	}
	else if(Connectivity == 6) {
        if(OrderInvariant) {
            return GenericWatershed<T, true, 6>::GetLabelImage(Gradient, Width, Height, Depth, DrawLines);
        }
        else {
            return GenericWatershed<T, false, 6>::GetLabelImage(Gradient, Width, Height, Depth, DrawLines);
        }
    }
	else if(Connectivity == 18) {
        if(OrderInvariant) {
            return GenericWatershed<T, true, 18>::GetLabelImage(Gradient, Width, Height, Depth, DrawLines);
        }
        else {
            return GenericWatershed<T, false, 18>::GetLabelImage(Gradient, Width, Height, Depth, DrawLines);
        }
    }
	else if(Connectivity == 26) {
        if(OrderInvariant) {
            return GenericWatershed<T, true, 26>::GetLabelImage(Gradient, Width, Height, Depth, DrawLines);
        }
        else {
            return GenericWatershed<T, false, 26>::GetLabelImage(Gradient, Width, Height, Depth, DrawLines);
        }
    }
    else {
        throw "Watershed => invalid connectivity";
    }
}

// -------------------------------------------------------------------------- //
// Instantiation 

// unsigned short
template
int* Watershed<unsigned short>
(const unsigned short*, const int, const int, const int, const bool, const int, const bool);

template
int* Watershed<unsigned char>
(const unsigned char*, const int, const int, const int, const bool, const int, const bool);

// unsigned float
template
int* Watershed<float>
(const float*, const int, const int, const int, const bool, const int, const bool);

// -------------------------------------------------------------------------- //
}
