#ifndef WATERSHED_COMMON_H
#define WATERSHED_COMMON_H

#include <cstring>
#include <cstdlib>
#include "stack.h"
#include "sentinel.h"

namespace ivLab3D {

// -------------------------------------------------------------------------- //
template<class T> inline
void Swap(T& a, T& b)
{
    T c(a);
    a = b, b = c;
}
// -------------------------------------------------------------------------- //

template<class T>
int* CountingSort(const T* Gradient, const int Width, const int Height, const int Depth)
// This gradient image must be surrounded with sentinels, so that its actual size is (Width+2)*(Height+2).
{
    const T* scan;
    const int Range(1<<(8*sizeof(T)));
    T MinVal, MaxVal;
    int *Rank, *Hist0, *Hist, index, offset, count, x, y;

    GetMin(MinVal);
    GetMax(MaxVal);

    Rank = new int[Width * Height * Depth];
    Hist0 = new int[Range];
    if(!Rank || !Hist0) {
        throw "CountingSort => cannot allocate memory";
    }
    memset(Hist0, 0, sizeof(int) * Range);
    Hist = Hist0 - (int) MinVal;

    // Counting
    scan = Gradient + (Width+2)*(Height+2);
	for (int z = 1; z <= Depth; z++)
	{
		scan += Width + 2;
		for(y=1; y<=Height; y++) {
			for(x=1; x<=Width; x++) {
				Hist[(int) scan[x]]++;
			}
			scan += Width + 2;
		}

		scan += Width + 2;
    }

    // Accumulation
    count = 0;
    for(index=(int)MinVal; index<=(int)MaxVal; index++) {
        int temp;
        temp = Hist[index];
        Hist[index] = count;
        count += temp;
    }

    // Indexing
    /*offset = Width + 2;
    for(y=1; y<=Height; y++) {
        for(x=1; x<=Width; x++) {
            Rank[Hist[(int)Gradient[offset+x]]++] = offset+x;
        }
        offset += Width + 2;
    }*/

	offset = (Width+2)*(Height+2);
	for (int z = 1; z <= Depth; z++)
	{
		offset += Width + 2;
		for(y=1; y<=Height; y++) {
			for(x=1; x<=Width; x++) {
				Rank[Hist[(int)Gradient[offset+x]]++] = offset+x;
			}
			offset += Width + 2;
		}

		offset += Width + 2;
    }

    delete[] Hist0;
    return Rank;
}

// -------------------------------------------------------------------------- //

template<class T> inline
int SortGreater(const T* Gradient, const int Index1, const int Index2)
{
    T x1, x2;

    x1 = Gradient[Index1];
    x2 = Gradient[Index2];

    if(x1 == x2) {
// -------------------------------------------------------------------
// The following code is to make the sorting stable.
// It can make the non-order-invariant watershed algorithm generate
// the same segmentation as the non-order-invariant toboggan algorithm
// in this package.
// -------------------------------------------------------------------
       return Index1 - Index2;
   }
// -------------------------------------------------------------------
    return (x1 > x2) ? +1 : -1;
}

// -------------------------------------------------------------------------- //

template<class T>
int* QuickSort(const T* Gradient, const int Width, const int Height, const int Depth)
// This gradient image must be surrounded with sentinels, so that its actual size is (Width+2)*(Height+2).
{
    const int QuickSortStackSize = 64;
    const int InsertionSortLength = 16;

    TStack<int> Stack(QuickSortStackSize);
    const int Size(Width * Height * Depth);
    int *Rank, i, j, temp, x, y, index, offset, left, right, mid;
    
    // Memory allocation
    Rank = new int[Size];
    if(!Rank) {
        throw "QuickSort => cannot allocate memory";
    }

    // Making indecies
    /*index = 0;
    offset = Width+3;
    for(y=0; y<Height; y++) {
        for(x=0; x<Width; x++) {
            Rank[index+x] = index+x+offset;
        }
        index += Width;
        offset += 2;
    }*/

	index = 0;
	for (int z = 0; z<Depth; z++)
	{
		offset = (z+1)*(Width+2)*(Height+2) + Width + 3;
		for(y=0; y<Height; y++) {
			for(x=0; x<Width; x++) {
				Rank[index+x] = index+x+offset;
			}
			index += Width;
			offset += 2;
		}
	}

    // Initiation
    Stack.Reset();
    Stack.Push(left = 0);
    Stack.Push(right = Size - 1);

    while(!Stack.IsEmpty()) {

        if(right - left < InsertionSortLength) {
            // InsertionSort
            for(i=left+1; i<=right; i++) {
                temp = Rank[i];
                for(j=i; j>left; j--) {
                    // Search for the proper position of `temp'
                    if(SortGreater(Gradient, Rank[j-1], temp) > 0) {
                        Rank[j] = Rank[j-1];
                    }
                    else {
                        break;
                    }
                }
                Rank[j] = temp;
            }
            Stack.Pop(right);
            Stack.Pop(left);
            continue;
        }

        // QuickSort
        mid = right - 1;
        if(SortGreater(Gradient, Rank[left], Rank[right]) > 0) {
            Swap(Rank[left], Rank[right]);
        }
        if(SortGreater(Gradient, Rank[mid], Rank[right])  > 0) {
            Swap(Rank[mid], Rank[right]);
        }
        if(SortGreater(Gradient, Rank[left], Rank[mid])   > 0) {
            Swap(Rank[left], Rank[mid]);
        }
        // --> Gradient[Rank[left]] <= Gradient[Rank[mid]] <= Gradient[Rank[right]]

        i = left;
        j = mid;
        temp = Rank[mid];
        do {
            while(SortGreater(Gradient, Rank[++i], temp) < 0);
            while(SortGreater(Gradient, Rank[--j], temp) > 0);
            Swap(Rank[i], Rank[j]);
        } while(i <= j);

		temp = Rank[j];
        Rank[j] = Rank[i];
		Rank[i] = Rank[mid];
        Rank[mid] = temp;

        if(j-left+1 > right-i) {
            Stack.Push(left);
            Stack.Push(j);
            left = i+1;
        }
        else {
            Stack.Push(i+1);
            Stack.Push(right);
            right = j;
        }
    }

    return Rank;
}

// -------------------------------------------------------------------------- //

/*template<class T>
class StdLibSort {
private:
    static const T* Gradient;
public:
    static int CompareFunction(const void* p1, const void* p2);
    static int* Execute(const T* Gradient, const int Width, const int Height);
};

template<class T>
const T* StdLibSort<T>::Gradient;

template<class T>
int StdLibSort<T>::CompareFunction(const void* p1, const void* p2)
{
    return SortGreater(StdLibSort<T>::Gradient, *(const int*)p1, *(const int*)p2);
}

template<class T>
int* StdLibSort<T>::Execute(const T* Gradient, const int Width, const int Height) 
{
    const int Size(Width*Height);
    int *Rank, x, y, index, offset;

    // Memory allocation
    Rank = new int[Size];
    if(!Rank) {
        throw "StdLibSort::Execute => cannot allocate memory";
    }

    // Making indecies
    index = 0;
    offset = Width+3;
    for(y=0; y<Height; y++) {
        for(x=0; x<Width; x++) {
            Rank[index+x] = index+x+offset;
        }
        index += Width;
        offset += 2;
    }

    StdLibSort<T>::Gradient = Gradient;
    qsort(Rank, Size, sizeof(int), StdLibSort<T>::CompareFunction);

    return Rank;
}*/

// -------------------------------------------------------------------------- //

// to avoid warning in CountingSort while floating-point type is used, original Sort has been broken and partial template specialization has been used
/*template<class T> static
int* Sort(const T* Gradient, const int Width, const int Height, const int Depth)
// This gradient image must be surrounded with sentinels, so that its actual size is (Width+2)*(Height+2).
{
    if(CountableAndSmallRange(Gradient)) {
        return CountingSort<T>(Gradient, Width, Height, Depth);
    }
    else
    {
        return QuickSort<T>(Gradient, Width, Height, Depth);
        // Another choice is to use the standard C library ---------
        // -- WARNING ----------------------------------------------
        // Because this sorting function uses a static member
        // variable, it is not a thread-safe operation.
        // --------------------------------------------------------
        // return StdLibSort<T>::Execute(Gradient, Width, Height);
        // --------------------------------------------------------
    }
}*/

template<typename T, bool countableAndSmallRange = CountableAndSmallRangeT<T>::value>
struct SSort
{
	static int* Sort(const T* Gradient, const int Width, const int Height, const int Depth);
};

template<typename T>
struct SSort<T, true>
{
	static int* Sort(const T* Gradient, const int Width, const int Height, const int Depth)	
	{
		return CountingSort<T>(Gradient, Width, Height, Depth);
	}
};

template<typename T>
struct SSort<T, false>
{
	static int* Sort(const T* Gradient, const int Width, const int Height, const int Depth)	
	{
		return QuickSort<T>(Gradient, Width, Height, Depth);
		// Another choice is to use the standard C library ---------
		// -- WARNING ----------------------------------------------
		// Because this sorting function uses a static member
		// variable, it is not a thread-safe operation.
		// --------------------------------------------------------
		// return StdLibSort<T>::Execute(Gradient, Width, Height);
		// --------------------------------------------------------
	}
};

template<typename T>
int* Sort(const T* Gradient, const int Width, const int Height, const int Depth)
{
	return SSort<T>::Sort(Gradient, Width, Height, Depth);
}

} // namespace ivLab3D {
#endif // #ifndef WATERSHED_COMMON_H
