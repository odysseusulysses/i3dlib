/*
 * i3dlib - image manipulation library
 *
 * Copyright (C) 2000-2006   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 * FastLevelSet.h
 *
 * Fast approximations of the Level Set method.
 *
 * Martin Ma�ka (xmaska@fi.muni.cz) 2006
 */

#ifndef __FAST_LEVELSET_H__
#define __FAST_LEVELSET_H__

#include "image3d.h"
#include <list>

/** Supported types of image segmentation. */
enum SegmentationType
{
  /** A segmentation of image background. */
  SEG_BGR = 0, 
  /** A segmentation of image foreground. */
  SEG_FGR = 1
};

/////////////////////////////////////////////////////////////////
//                                                             //
//                    Struct SwedenHeapNode                    //
//                                                             //
/////////////////////////////////////////////////////////////////

/** \brief A helper structure that represents a heap node of the Sweden fast level set method. This method is used for the
  * image segmentation. See details in the \c i3d::SwedenFastLevelSet function.
  *
  * This structure encapsulates all needed atributes of the interface point for the Sweden fast level set method. */
struct SwedenHeapNode
{
  /** The index of the image point. */  
  size_t position;
  /** The time at which the interface arrived to the image point. */
  float arrival_time;
  /** The time at which the interface is propagated from the image point to its neighbors. 
    * The departure time is computed as \f$ departure\_time = arrival\_time + \frac{1}{max\{|F|,\varepsilon\}} \f$. */
  float departure_time;
  /** The value of the speed term \f$F\f$ at the image point. */
  float speed;
  
  /** Constructor. */
  SwedenHeapNode(size_t pos, float atime, float dtime, float sp): position(pos), arrival_time(atime), 
	                                                              departure_time(dtime), speed(sp) {};
};

/////////////////////////////////////////////////////////////////
//                                                             //
//                   Struct ChineseHeapNode                    //
//                                                             //
/////////////////////////////////////////////////////////////////

/** \brief A helper structure that represents a heap node of the Chinese fast level set method. This method is used for the
  * image segmentation. See details in the \c i3d::ChineseFastLevelSet function.
  *
  * This structure encapsulates all needed atributes of the active point for the Chinese fast level set method. The active 
  * point is the image point that is directly connected to the moving interface. */
struct ChineseHeapNode
{
  /** The index of the image point. */  
  size_t position;
  /** The time at which the moving interface arrives to the image point. */
  float arrival_time;
    
  /** Constructor. */
  ChineseHeapNode(size_t pos, float atime): position(pos), arrival_time(atime) {};
};

/////////////////////////////////////////////////////////////////
//                                                             //
//                       Class HeapBase                        //
//                                                             //
/////////////////////////////////////////////////////////////////

/** \brief An abstract base class that represents the binary min-heap used in the Sweden and Chinese fast level set 
  * methods. Both two methods are used for the image segmentation. See details in \c i3d::SwedenFastLevelSet and 
  * \c i3d::ChineseFastLevelSet functions. 
  * 
  * Derived classes \c SwedenHeap and \c ChineseHeap implement the binary min-heap used in the respective image 
  * segmentation method. */
template <class T> class HeapBase
{
  protected:
    /** A list of nodes on the heap. There is one-to-one correspondence between the node on the heap and the point of the 
	  * moving interface in the \c i3d::SwedenFastLevelSet function and there is one-to-one correspondence between the node
	  * on the heap and the active point in the \c i3d::ChineseFastLevelSet function. The list is sorted by the 
	  * \c HeapBase::Compare function. */
	std::vector<T *> Nodes; 
	/** A reference to the array of back pointers to the heap. In this array is saved whether and where on the heap is 
	  * located the image point. If \f$BackPointers[i]=-1\f$ then the image point with the index \f$i\f$ isn't on the heap. 
	  * But if \f$BackPointers[i]>-1\f$ then the image point with the index \f$i\f$ is on the heap and corresponds to the node 
	  * \f$Nodes[BackPointers[i]]\f$. */
	std::valarray<int> &BackPointers; 

  public:
    /** Constructor. */
	HeapBase(std::valarray<int> &BPointers): Nodes(), BackPointers(BPointers) {};
	/** Destructor. */
	virtual ~HeapBase() { for (size_t i = 0; i < Nodes.size(); i++) delete Nodes[i]; Nodes.clear(); };
	/** Update atributes of the existing node on the heap. */
	virtual void Update(size_t index, float new_key) = 0;
	/** Remove the root of the heap. After removing the heap is rebalanced. The function returns \c NULL if the heap is 
	  * empty. */
	T * Pop();
	/** Return size of the heap. */
	size_t HeapSize() { return Nodes.size(); };
	/** Return true if the heap is empty. */
	bool IsEmpty() { return (Nodes.size() == 0); };

  protected:
	/** Rebalance the heap upward. */
	void BalanceUp(size_t index);
	/** Rebalance the heap downward. */
	void BalanceDown(size_t index);

  private:
	/** Compare two existing nodes on the heap. These two nodes correspond to the nodes \f$Nodes[i]\f$ and \f$Nodes[j]\f$. 
	  * This function is used for the heap rebalancing. */
	virtual bool Compare(size_t i, size_t j) = 0;
};

/////////////////////////////////////////////////////////////////
//                                                             //
//                     Class SwedenHeap                        //
//                                                             //
/////////////////////////////////////////////////////////////////

/** \brief This is derived class of the base class \c HeapBase that represents the binary min-heap for the Sweden fast level
  * set method. This method is used for the image segmentation. See details in the \c i3d::SwedenFastLevelSet function. 
  *
  * The heap is sorted by the \c SwedenHeapNode::departure_time of nodes. There is one-to-one correspondence between the 
  * node on the heap and the point of the moving interface. */
class I3D_DLLEXPORT SwedenHeap : public HeapBase<SwedenHeapNode>
{
  public:
    /** Constructor. */
	SwedenHeap(std::valarray<int> &BPointers): HeapBase<SwedenHeapNode>(BPointers) {};
	/** Insert a new \c SwedenHeapNode node into the heap.*/
    void Insert(size_t pos, float atime, float speed);
	/** Update the speed of the existing node on the heap. This change claims recomputing of the departure time and therefore
	  * the heap have to be rebalanced. */
	virtual void Update(size_t index, float new_speed);
	
  private:
  	/** Compare two existing \c SwedenHeapNode nodes on the heap. These two nodes correspond to the nodes \f$Nodes[i]\f$ and 
	  * \f$Nodes[j]\f$. This function is used for the heap rebalancing and compares the departure times of nodes. The function
	  * returns \c true if the departure time of the node \f$Nodes[i]\f$ is less than the departure time of the node
	  * \f$Nodes[j]\f$, otherwise returns \c false. */ 
	virtual inline bool Compare(size_t i, size_t j) { return Nodes[i] -> departure_time < Nodes[j] -> departure_time; }
};

/////////////////////////////////////////////////////////////////
//                                                             //
//                     Class ChineseHeap                       //
//                                                             //
/////////////////////////////////////////////////////////////////

/** \brief This is derived class of the base class \c HeapBase that represents the binary min-heap for the Chinese fast level
  * set method. This method is used for the image segmentation. See details in the \c i3d::ChineseFastLevelSet function. 
  *
  * The heap is sorted by the \c ChineseHeapNode::arrival_time of the nodes. There is one-to-one correspondence between the 
  * node on the heap and the active point. The active point is the image point that is directly connected to the moving 
  * interface. */
class ChineseHeap : public HeapBase<ChineseHeapNode>
{
  public:
    /** Constructor. */
	ChineseHeap(std::valarray<int> &BPointers): HeapBase<ChineseHeapNode>(BPointers) {};
	/** Insert a new \c ChineseHeapNode node into the heap.*/
    void Insert(size_t pos, float atime);
	/** Update the arrival_time of the existing node on the heap. After updating the heap have to be rebalanced. */
	virtual void Update(size_t index, float new_atime);
	/** Remove node from the heap. After removing the heap is rebalanced. */
	void Delete(size_t index);
    /** Return arrival_time of the root of the heap. */
	float TimeOfRoot() { return Nodes[0] -> arrival_time; }

  private:
  	/** Compare two existing \c ChineseHeapNode nodes on the heap. These two nodes correspond to the nodes \f$Nodes[i]\f$ and 
	  * \f$Nodes[j]\f$. This function is used for the heap rebalancing and compares the arrival times of nodes. The function
	  * returns \c true if the arrival time of the node \f$Nodes[i]\f$ is less than the arrival time of the node 
	  * \f$Nodes[j]\f$, otherwise returns \c false. */ 
	virtual inline bool Compare(size_t i, size_t j) { return Nodes[i] -> arrival_time < Nodes[j] -> arrival_time; }
};

struct ImageProperties
{
	int width;
	int height;
	int depth;

	float spacing_x;
	float spacing_y;
	float spacing_z;

	/** Constructor. */
	ImageProperties(int w = 0, int h = 0, int d = 0, float sx = 0.0f, float sy = 0.0f, float sz = 0.0f)
		: width(w), height(h), depth(d), spacing_x(sx), spacing_y(sy), spacing_z(sz) {};
};

struct SpeedFunctionParams
{
	float inflation_term_coef;
	float data_term_coef;
	float curvature_term_coef;
	int edge_sensitivity_coef;
	int neighbourhood_radius;
	int neighbourhood_size;

	/** Constructor. */
	SpeedFunctionParams(float itc = 0.0f, float dtc = 0.0f, float ctc = 0.0f, int esc = 0, int nr = 0, int ns = 0)
		: inflation_term_coef(itc), data_term_coef(dtc), curvature_term_coef(ctc), edge_sensitivity_coef(esc),
	      neighbourhood_radius(nr), neighbourhood_size(ns) {};
};

/////////////////////////////////////////////////////////////////
//                                                             //
//                   Struct RoutineBase                        //
//                                                             //
/////////////////////////////////////////////////////////////////

struct I3D_DLLEXPORT RoutineBase
{
	std::valarray<int> m_implicit_function;
	std::valarray<float> m_curvature;
	std::valarray<int> m_back_pointers;
	SwedenHeap *m_heap;

	int m_width;
	int m_height;
	int m_depth;

	/** Constructor. */
	RoutineBase(int width, int height, int depth);
	/** Destructor. */
	virtual ~RoutineBase();
	
	virtual void Initialize();

	inline void Get2DCoordinates(int i, int &x, int &y) const;
	inline void Get3DCoordinates(int i, int &x, int &y, int &z) const;
};

/////////////////////////////////////////////////////////////////
//                                                             //
//           Struct TopologyPreserving_RoutineBase             //
//                                                             //
/////////////////////////////////////////////////////////////////

struct I3D_DLLEXPORT TopologyPreserving_RoutineBase : public RoutineBase
{
	std::valarray<int> m_component;

	/** Constructor. */
	TopologyPreserving_RoutineBase(int width, int height, int depth);
	
	virtual void Initialize();

	inline void Get2DComponentIndex(int i, int &index) const;
	inline void Get3DComponentIndex(int i, int &index) const;
};

/////////////////////////////////////////////////////////////////
//                                                             //
//                Class ComputationalDomain                    //
//                                                             //
/////////////////////////////////////////////////////////////////

class I3D_DLLEXPORT ComputationalDomain
{
	private:
		const i3d::BINARY *m_domain;

	public:
		/** Constructor. */
		ComputationalDomain(const i3d::BINARY *domain);
		/** Return true if a point with given index belongs to the computational domain. */
		bool IsInDomain(int i) const;
};

/////////////////////////////////////////////////////////////////
//                                                             //
//                     Class Neighbourhood                     //
//                                                             //
/////////////////////////////////////////////////////////////////

class I3D_DLLEXPORT Neighbourhood
{
	protected:
		const RoutineBase *m_base;
		int *m_neighbors;

	public:
		/** Constructor. */
		Neighbourhood(const RoutineBase *base, int size);
		/** Destructor. */
		virtual ~Neighbourhood();
		/** Compute neighbors of point with index i. */
		virtual const int *GetNeighbors(int i, int value, int &num) = 0;
};

/////////////////////////////////////////////////////////////////
//                                                             //
//             Class Standard_Neighbourhood2D                  //
//                                                             //
/////////////////////////////////////////////////////////////////

class I3D_DLLEXPORT Standard_Neighbourhood2D : public Neighbourhood
{
	public:
		/** Constructor. */
		Standard_Neighbourhood2D(const RoutineBase *base);
		/** Compute neighbors of point with index i. */
		virtual const int *GetNeighbors(int i, int value, int &num);
};

/////////////////////////////////////////////////////////////////
//                                                             //
//             Class Standard_Neighbourhood3D                  //
//                                                             //
/////////////////////////////////////////////////////////////////

class I3D_DLLEXPORT Standard_Neighbourhood3D : public Neighbourhood
{
	public:
		/** Constructor. */
		Standard_Neighbourhood3D(const RoutineBase *base);
		/** Compute neighbors of point with index i. */
		virtual const int *GetNeighbors(int i, int value, int &num);
};

/////////////////////////////////////////////////////////////////
//                                                             //
//         Class Standard_Neighbourhood2D_WithDomain           //
//                                                             //
/////////////////////////////////////////////////////////////////

class I3D_DLLEXPORT Standard_Neighbourhood2D_WithDomain : public Neighbourhood, public ComputationalDomain
{
	public:
		/** Constructor. */
		Standard_Neighbourhood2D_WithDomain(const RoutineBase *base, const i3d::BINARY *domain);
		/** Compute neighbors of point with index i. */
		virtual const int *GetNeighbors(int i, int value, int &num);
};

/////////////////////////////////////////////////////////////////
//                                                             //
//         Class Standard_Neighbourhood3D_WithDomain           //
//                                                             //
/////////////////////////////////////////////////////////////////

class I3D_DLLEXPORT Standard_Neighbourhood3D_WithDomain : public Neighbourhood, public ComputationalDomain
{
	public:
		/** Constructor. */
		Standard_Neighbourhood3D_WithDomain(const RoutineBase *base, const i3d::BINARY *domain);
		/** Compute neighbors of point with index i. */
		virtual const int *GetNeighbors(int i, int value, int &num);
};

/////////////////////////////////////////////////////////////////
//                                                             //
//         Class TopologyPreserving_Neighbourhood2D            //
//                                                             //
/////////////////////////////////////////////////////////////////

class I3D_DLLEXPORT TopologyPreserving_Neighbourhood2D : public Neighbourhood
{
	public:
		/** Constructor. */
		TopologyPreserving_Neighbourhood2D(const RoutineBase *base);
		/** Compute neighbors of point with index i. */
		virtual const int *GetNeighbors(int i, int value, int &num);
};

/////////////////////////////////////////////////////////////////
//                                                             //
//         Class TopologyPreserving_Neighbourhood3D            //
//                                                             //
/////////////////////////////////////////////////////////////////

class I3D_DLLEXPORT TopologyPreserving_Neighbourhood3D : public Neighbourhood
{
	public:
		/** Constructor. */
		TopologyPreserving_Neighbourhood3D(const RoutineBase *base);
		/** Compute neighbors of point with index i. */
		virtual const int *GetNeighbors(int i, int value, int &num);
};

/////////////////////////////////////////////////////////////////
//                                                             //
//     Class TopologyPreserving_Neighbourhood2D_WithDomain     //
//                                                             //
/////////////////////////////////////////////////////////////////

class I3D_DLLEXPORT TopologyPreserving_Neighbourhood2D_WithDomain : public Neighbourhood, public ComputationalDomain
{
	public:
		/** Constructor. */
		TopologyPreserving_Neighbourhood2D_WithDomain(const RoutineBase *base, const i3d::BINARY *domain);
		/** Compute neighbors of point with index i. */
		virtual const int *GetNeighbors(int i, int value, int &num);
};

/////////////////////////////////////////////////////////////////
//                                                             //
//     Class TopologyPreserving_Neighbourhood3D_WithDomain     //
//                                                             //
/////////////////////////////////////////////////////////////////

class I3D_DLLEXPORT TopologyPreserving_Neighbourhood3D_WithDomain : public Neighbourhood, public ComputationalDomain
{
	public:
		/** Constructor. */
		TopologyPreserving_Neighbourhood3D_WithDomain(const RoutineBase *base, const i3d::BINARY *domain);
		/** Compute neighbors of point with index i. */
		virtual const int *GetNeighbors(int i, int value, int &num);
};

/////////////////////////////////////////////////////////////////
//                                                             //
//                    Class SpeedFunction                      //
//                                                             //
/////////////////////////////////////////////////////////////////

class I3D_DLLEXPORT SpeedFunction
{
	protected:
		const RoutineBase *m_base;

	public:
		/** Constructor. */
		SpeedFunction(const RoutineBase *base);
		/** Destructor. */
		virtual ~SpeedFunction();
		/** Compute a value of the speed function in a point with index i. */
		virtual float GetSpeed(int i) const = 0;
};

/////////////////////////////////////////////////////////////////
//                                                             //
//               Class Segmentation_SpeedFunction              //
//                                                             //
/////////////////////////////////////////////////////////////////

class I3D_DLLEXPORT Segmentation_SpeedFunction : public SpeedFunction
{
	protected:
		std::valarray<float> m_inflation_term;
		std::valarray<float> m_data_term;

		float m_spacing_x;
		float m_spacing_y;
		float m_spacing_z;

		SpeedFunctionParams m_speed_params;

	public:
		/** Constructor. */
		Segmentation_SpeedFunction(const RoutineBase *base, const std::valarray<float> &inflation_term, const std::valarray<float> &data_term, 
			                       float spacing_x, float spacing_y, float spacing_z, const SpeedFunctionParams &speed_params);
};

/////////////////////////////////////////////////////////////////
//                                                             //
//               Class Segmentation_SpeedFunction2D            //
//                                                             //
/////////////////////////////////////////////////////////////////

class I3D_DLLEXPORT Segmentation_SpeedFunction2D : public Segmentation_SpeedFunction
{
	public:
		/** Constructor. */
		Segmentation_SpeedFunction2D(const RoutineBase *base, const std::valarray<float> &inflation_term, const std::valarray<float> &data_term, 
									 float spacing_x, float spacing_y, const SpeedFunctionParams &speed_params);
		/** Compute a value of the speed function in a point with index i. */
		virtual float GetSpeed(int i) const;
};

/////////////////////////////////////////////////////////////////
//                                                             //
//               Class Segmentation_SpeedFunction3D            //
//                                                             //
/////////////////////////////////////////////////////////////////

class I3D_DLLEXPORT Segmentation_SpeedFunction3D : public Segmentation_SpeedFunction
{
	public:
		/** Constructor. */
		Segmentation_SpeedFunction3D(const RoutineBase *base, const std::valarray<float> &inflation_term, const std::valarray<float> &data_term, 
									 float spacing_x, float spacing_y, float spacing_z, const SpeedFunctionParams &speed_params);
		/** Compute a value of the speed function in a point with index i. */
		virtual float GetSpeed(int i) const;
};

/////////////////////////////////////////////////////////////////
//                                                             //
//                 Class Binary_SpeedFunction                  //
//                                                             //
/////////////////////////////////////////////////////////////////

class I3D_DLLEXPORT Binary_SpeedFunction : public SpeedFunction
{
	private:
		std::valarray<i3d::BINARY> m_binary_speed;

	public:
		/** Constructor. */
		Binary_SpeedFunction(const RoutineBase *base, const std::valarray<i3d::BINARY> &binary_speed);
		/** Compute a value of the speed function in a point with index i. */
		virtual float GetSpeed(int i) const;
};


/////////////////////////////////////////////////////////////////
//                                                             //
//         Class Segmentation_SpeedFunction2D_WithDomain       //
//                                                             //
/////////////////////////////////////////////////////////////////

class I3D_DLLEXPORT Segmentation_SpeedFunction2D_WithDomain : public Segmentation_SpeedFunction, public ComputationalDomain
{
	public:
		/** Constructor. */
		Segmentation_SpeedFunction2D_WithDomain(const RoutineBase *base, const i3d::BINARY *domain, const std::valarray<float> &inflation_term, 
			                                    const std::valarray<float> &data_term, float spacing_x, float spacing_y, const SpeedFunctionParams &speed_params);
		/** Compute a value of the speed function in a point with index i. */
		virtual float GetSpeed(int i) const;
};

/////////////////////////////////////////////////////////////////
//                                                             //
//         Class Segmentation_SpeedFunction3D_WithDomain       //
//                                                             //
/////////////////////////////////////////////////////////////////

class I3D_DLLEXPORT Segmentation_SpeedFunction3D_WithDomain : public Segmentation_SpeedFunction, public ComputationalDomain
{
	public:
		/** Constructor. */
		Segmentation_SpeedFunction3D_WithDomain(const RoutineBase *base, const i3d::BINARY *domain, const std::valarray<float> &inflation_term, 
			                                    const std::valarray<float> &data_term, float spacing_x, float spacing_y, float spacing_z, 
												const SpeedFunctionParams &speed_params);
		/** Compute a value of the speed function in a point with index i. */
		virtual float GetSpeed(int i) const;
};

/////////////////////////////////////////////////////////////////
//                                                             //
//                        Class Curvature                      //
//                                                             //
/////////////////////////////////////////////////////////////////

class I3D_DLLEXPORT Curvature
{
	protected:
		RoutineBase *m_base;
		const SpeedFunction *m_speed_function;
		int m_radius;
		float m_size;

	public:
		/** Constructor. */
		Curvature(RoutineBase *base, const SpeedFunction *speed_function, int radius);
		/** Destructor. */
		virtual ~Curvature();
		/** Set a curvature of the point i to undefined value. */
		void Undefine(int i);

		virtual void Update(int i, float change) = 0;
		virtual void Estimate(int i) = 0;
};

/////////////////////////////////////////////////////////////////
//                                                             //
//                      Class Curvature2D                      //
//                                                             //
/////////////////////////////////////////////////////////////////

class I3D_DLLEXPORT Curvature2D : public Curvature
{
	public:
		/** Constructor. */
		Curvature2D(RoutineBase *base, const SpeedFunction *speed_function, int radius);
		
		virtual void Update(int i, float change);
		virtual void Estimate(int i);

	protected:
		inline void GetBounds(int x, int y, int &lx, int &ux, int &ly, int &uy);
};

/////////////////////////////////////////////////////////////////
//                                                             //
//                      Class Curvature3D                      //
//                                                             //
/////////////////////////////////////////////////////////////////

class I3D_DLLEXPORT Curvature3D : public Curvature
{
	public:
		/** Constructor. */
		Curvature3D(RoutineBase *base, const SpeedFunction *speed_function, int radius);
		
		virtual void Update(int i, float change);
		virtual void Estimate(int i);

	protected:
		inline void GetBounds(int x, int y, int z, int &lx, int &ux, int &ly, int &uy, int &lz, int &uz);
};

/////////////////////////////////////////////////////////////////
//                                                             //
//                    Class BorderPointDecider                 //
//                                                             //
/////////////////////////////////////////////////////////////////

class I3D_DLLEXPORT BorderPointDecider
{
	protected:
		const RoutineBase *m_base;
		const i3d::BINARY *m_mask;
				
	public:
		/** Constructor. */
		BorderPointDecider(const RoutineBase *base, const i3d::BINARY *mask);
		/** Destructor. */
		virtual ~BorderPointDecider();
		
		virtual bool IsBorder(int i) const = 0;
};

/////////////////////////////////////////////////////////////////
//                                                             //
//                  Class BorderPointDecider2D                 //
//                                                             //
/////////////////////////////////////////////////////////////////

class I3D_DLLEXPORT BorderPointDecider2D : public BorderPointDecider
{
	public:
		/** Constructor. */
		BorderPointDecider2D(const RoutineBase *base, const i3d::BINARY *mask);
		
		virtual bool IsBorder(int i) const;
};

/////////////////////////////////////////////////////////////////
//                                                             //
//                  Class BorderPointDecider3D                 //
//                                                             //
/////////////////////////////////////////////////////////////////

class I3D_DLLEXPORT BorderPointDecider3D : public BorderPointDecider
{
	public:
		/** Constructor. */
		BorderPointDecider3D(const RoutineBase *base, const i3d::BINARY *mask);
		
		virtual bool IsBorder(int i) const;
};

/////////////////////////////////////////////////////////////////
//                                                             //
//           Class BorderPointDecider2D_WithDomain             //
//                                                             //
/////////////////////////////////////////////////////////////////

class I3D_DLLEXPORT BorderPointDecider2D_WithDomain : public BorderPointDecider, public ComputationalDomain
{
	public:
		/** Constructor. */
		BorderPointDecider2D_WithDomain(const RoutineBase *base, const i3d::BINARY *mask, const i3d::BINARY *domain);
		
		virtual bool IsBorder(int i) const;
};

/////////////////////////////////////////////////////////////////
//                                                             //
//           Class BorderPointDecider3D_WithDomain             //
//                                                             //
/////////////////////////////////////////////////////////////////

class I3D_DLLEXPORT BorderPointDecider3D_WithDomain : public BorderPointDecider, public ComputationalDomain
{
	public:
		/** Constructor. */
		BorderPointDecider3D_WithDomain(const RoutineBase *base, const i3d::BINARY *mask, const i3d::BINARY *domain);
		
		virtual bool IsBorder(int i) const;
};

/////////////////////////////////////////////////////////////////
//                                                             //
//                    Class SimplePointDecider                 //
//                                                             //
/////////////////////////////////////////////////////////////////

class I3D_DLLEXPORT SimplePointDecider
{
	protected:
		TopologyPreserving_RoutineBase *m_base;
		
	public:
		/** Constructor. */
		SimplePointDecider(TopologyPreserving_RoutineBase *base);
		/** Destructor. */
		virtual ~SimplePointDecider();

		virtual void SetComponent(int i, int value) = 0;
		virtual int GetComponent(int i) = 0;
		
		virtual bool IsSimple(int i) = 0;
};

/////////////////////////////////////////////////////////////////
//                                                             //
//                  Class SimplePointDecider2D                 //
//                                                             //
/////////////////////////////////////////////////////////////////

class I3D_DLLEXPORT SimplePointDecider2D : public SimplePointDecider
{
	public:
		/** Constructor. */
		SimplePointDecider2D(TopologyPreserving_RoutineBase *base);
		
		virtual void SetComponent(int i, int value);
		virtual int GetComponent(int i);
		
		virtual bool IsSimple(int i);
};

/////////////////////////////////////////////////////////////////
//                                                             //
//                  Class SimplePointDecider3D                 //
//                                                             //
/////////////////////////////////////////////////////////////////

class I3D_DLLEXPORT SimplePointDecider3D : public SimplePointDecider
{
	public:
		/** Constructor. */
		SimplePointDecider3D(TopologyPreserving_RoutineBase *base);
		
		virtual void SetComponent(int i, int value);
		virtual int GetComponent(int i);
		
		virtual bool IsSimple(int i);
};

/////////////////////////////////////////////////////////////////
//                                                             //
//                        Class Initiator                      //
//                                                             //
/////////////////////////////////////////////////////////////////

class I3D_DLLEXPORT Initiator
{
	protected:
		RoutineBase *m_base;
		const SpeedFunction *m_speed_function;
		Curvature *m_curvature;
		const BorderPointDecider *m_border_point_decider;

	public:
		/** Constructor. */
		Initiator(RoutineBase *base, const SpeedFunction *speed_function, Curvature *curvature, 
			      const BorderPointDecider *border_point_decider);
		/** Destructor. */
		virtual ~Initiator();

		virtual void Initialize() = 0;
};

/////////////////////////////////////////////////////////////////
//                                                             //
//                  Class Standard_Initiator                   //
//                                                             //
/////////////////////////////////////////////////////////////////

class I3D_DLLEXPORT Standard_Initiator : public Initiator
{
	protected:
		const i3d::BINARY *m_mask;

	public:
		/** Constructor. */
		Standard_Initiator(RoutineBase *base, const SpeedFunction *speed_function, Curvature *curvature, 
			               const BorderPointDecider *border_point_decider, const i3d::BINARY *mask);

		virtual void Initialize();
};

/////////////////////////////////////////////////////////////////
//                                                             //
//           Class Standard_Initiator_WithDomain               //
//                                                             //
/////////////////////////////////////////////////////////////////

class I3D_DLLEXPORT Standard_Initiator_WithDomain : public Initiator, public ComputationalDomain
{
	protected:
		const i3d::BINARY *m_mask;

	public:
		/** Constructor. */
		Standard_Initiator_WithDomain(RoutineBase *base, const SpeedFunction *speed_function, Curvature *curvature, 
									  const BorderPointDecider *border_point_decider, const i3d::BINARY *mask, 
									  const i3d::BINARY *domain);

		virtual void Initialize();
};

/////////////////////////////////////////////////////////////////
//                                                             //
//             Class TopologyPreserving_Initiator              //
//                                                             //
/////////////////////////////////////////////////////////////////

class I3D_DLLEXPORT TopologyPreserving_Initiator : public Initiator
{
	protected:
		const i3d::Image3d<i3d::BINARY> *m_mask;
		SimplePointDecider *m_simple_point_decider;

	public:
		/** Constructor. */
		TopologyPreserving_Initiator(RoutineBase *base, const SpeedFunction *speed_function, Curvature *curvature, 
			                         const BorderPointDecider *border_point_decider, SimplePointDecider *simple_point_decider, 
									 const i3d::Image3d<i3d::BINARY> *mask);

		virtual void Initialize();
};

/////////////////////////////////////////////////////////////////
//                                                             //
//        Class TopologyPreserving_Initiator_WithDomain        //
//                                                             //
/////////////////////////////////////////////////////////////////

class I3D_DLLEXPORT TopologyPreserving_Initiator_WithDomain : public Initiator, public ComputationalDomain
{
	protected:
		const i3d::Image3d<i3d::BINARY> *m_mask;
		SimplePointDecider *m_simple_point_decider;

	public:
		/** Constructor. */
		TopologyPreserving_Initiator_WithDomain(RoutineBase *base, const SpeedFunction *speed_function, Curvature *curvature, 
			                                    const BorderPointDecider *border_point_decider, SimplePointDecider *simple_point_decider, 
												const i3d::Image3d<i3d::BINARY> *mask, const i3d::BINARY *domain);

		virtual void Initialize();
};

/////////////////////////////////////////////////////////////////
//                                                             //
//                       Enum ResultType                       //
//                                                             //
/////////////////////////////////////////////////////////////////

enum ResultType
{
	ResultType_Interior,
	ResultType_Interface,
	ResultType_Exterior,
	ResultType_InteriorAndInterface,
	ResultType_ExteriorAndInterface
};

/////////////////////////////////////////////////////////////////
//                                                             //
//                    Class ResultCreator                      //
//                                                             //
/////////////////////////////////////////////////////////////////

class I3D_DLLEXPORT ResultCreator
{
	protected:
		const RoutineBase *m_base;
		i3d::Offset m_offset;
		i3d::Resolution m_resolution;

	public:
		/** Constructor. */
		ResultCreator(const RoutineBase *base, const i3d::Offset &offset, const i3d::Resolution &resolution);
		/** Destructor. */
		virtual ~ResultCreator();
		
		virtual void GetResult(i3d::Image3d<i3d::BINARY> &output, ResultType type) = 0;
};

/////////////////////////////////////////////////////////////////
//                                                             //
//               Class Segmentation_ResultCreator              //
//                                                             //
/////////////////////////////////////////////////////////////////

class I3D_DLLEXPORT Segmentation_ResultCreator : public ResultCreator
{
	public:
		/** Constructor. */
		Segmentation_ResultCreator(const RoutineBase *base, const i3d::Offset &offset, const i3d::Resolution &resolution);

		virtual void GetResult(i3d::Image3d<i3d::BINARY> &output, ResultType type);
};

/////////////////////////////////////////////////////////////////
//                                                             //
//         Class Segmentation_ResultCreator_WithDomain         //
//                                                             //
/////////////////////////////////////////////////////////////////

class I3D_DLLEXPORT Segmentation_ResultCreator_WithDomain : public ResultCreator, public ComputationalDomain
{
	public:
		/** Constructor. */
		Segmentation_ResultCreator_WithDomain(const RoutineBase *base, const i3d::Offset &offset, const i3d::Resolution &resolution, const i3d::BINARY *domain);

		virtual void GetResult(i3d::Image3d<i3d::BINARY> &output, ResultType type);
};

/////////////////////////////////////////////////////////////////
//                                                             //
//                    Class SwedenLevelSetRoutine              //
//                                                             //
/////////////////////////////////////////////////////////////////

class I3D_DLLEXPORT SwedenLevelSetRoutine
{
	protected:
		RoutineBase *m_base;
		Neighbourhood *m_neighbourhood;
		SpeedFunction *m_speed_function;
		Curvature *m_curvature;
		Initiator *m_initiator;
		ResultCreator *m_result_creator;
		
	public:
		/** Constructor. */
		SwedenLevelSetRoutine(RoutineBase *base, Neighbourhood *neighbourhood, SpeedFunction *speed_function, Curvature *curvature, 
			                  Initiator *initiator, ResultCreator *result_creator);
		/** Destructor. */
		virtual ~SwedenLevelSetRoutine();
		
		void Initialize();

		size_t GetInterfaceSize();

		bool IsOk();

		virtual void ComputeIteration() = 0;

		void Iterate(int num_iterations);

		const int *GetImplicitFunction();

		void GetResult(i3d::Image3d<i3d::BINARY> &output, ResultType type);
};

/////////////////////////////////////////////////////////////////
//                                                             //
//             Class Standard_SwedenLevelSetRoutine            //
//                                                             //
/////////////////////////////////////////////////////////////////

class I3D_DLLEXPORT Standard_SwedenLevelSetRoutine : public SwedenLevelSetRoutine
{
	public:
		/** Constructor. */
		Standard_SwedenLevelSetRoutine(RoutineBase *base, Neighbourhood *neighbourhood, SpeedFunction *speed_function, Curvature *curvature,
							           Initiator *initiator, ResultCreator *result_creator);

		virtual void ComputeIteration();
};

/////////////////////////////////////////////////////////////////
//                                                             //
//        Class TopologyPreserving_SwedenLevelSetRoutine       //
//                                                             //
/////////////////////////////////////////////////////////////////

class I3D_DLLEXPORT TopologyPreserving_SwedenLevelSetRoutine : public SwedenLevelSetRoutine
{
	protected:
		SimplePointDecider *m_simple_point_decider;

	public:
		/** Constructor. */
		TopologyPreserving_SwedenLevelSetRoutine(TopologyPreserving_RoutineBase *base, Neighbourhood *neighbourhood, SpeedFunction *speed_function, Curvature *curvature,
						 	                     Initiator *initiator, ResultCreator *result_creator, SimplePointDecider *simple_point_decider);
		/** Destructor. */
		virtual ~TopologyPreserving_SwedenLevelSetRoutine();

		virtual void ComputeIteration();
};

template <class T> I3D_DLLEXPORT 
	SwedenLevelSetRoutine *CreateSegmentationRoutine(const i3d::Image3d<T> &input, const i3d::Image3d<i3d::BINARY> *mask, 
	                                                 const i3d::Image3d<i3d::BINARY> *domain, bool preserve_topology, 
													 float sigma, int radius, const SpeedFunctionParams &params, 
													 bool use_distance);



/////////////////////////////////////////////////////////////////
//                                                             //
//                       Class CompSwedenLevelSet              //
//                                                             //
/////////////////////////////////////////////////////////////////

/** Abstract class that represents routine for sweden level set method. */
template <class T> class CompSwedenLevelSet
{
  protected:
	SwedenHeap *heap;
	std::valarray<int> ImplicitFunction;
	std::valarray<int> BackPointers;
	std::valarray<float> Curvature;
	const i3d::Image3d<i3d::BINARY> *Domain;
	std::valarray<float> ImageGradient;

	i3d::Image3d<float> Inflation;
		
	i3d::Vector3d<size_t> ImgSize;
	i3d::Vector3d<float> Spacing;
	float alpha, beta, c;  // parameters of speed term
	int p; // parameter of propagation speed
	size_t N, R;  // parameters of circular neighbourhood for curvature estimation

  public:
    /** Constructor. */
	CompSwedenLevelSet(const i3d::Image3d<i3d::BINARY> *_Domain, std::valarray<float> *ImageGrad, i3d::Vector3d<size_t> _ImgSize, 
	                   i3d::Vector3d<float> _Spacing, float _alpha, float _beta, float _c, int _p, 
	    			   size_t _N, size_t _R);
	/** Destructor. */
	virtual ~CompSwedenLevelSet() { if (heap) delete heap; }
	/** Set initial state of ImplicitFunction and heap from the list of starting seeds or from the image mask. */
	virtual void Init(const std::vector<size_t> *Seeds, const i3d::Image3d<T> *mask);
	/** Compute one iteration of sweden fast level set method. */
	virtual void ComputeIteration();
	/** Return pointer to the first position of ImplicitFunction. */
	int * GetFirstPoint() { return &ImplicitFunction[0]; }
	/** Get heap size */
	size_t GetHeapSize() { return heap -> HeapSize(); }
	/** Compute the segmentated image from current state of ImplicitFunction. */
	virtual void ComputeSegmentatedImage(i3d::Image3d<T> &output, SegmentationType type); 
	/** Return true if another iteration can be computed. */
	bool IsOk() { return !(heap -> IsEmpty()); };

  protected:
	/** Compute the value of speed function in point with index i. */
	virtual float ComputeSpeedFunction(size_t i) = 0;
	/** Compute neighbors of point with index i. The flag which signalized if we 
	  * want to get exterior(which is true) or interior(which is false) neighbors. */
	virtual void GetNeighbors(size_t i, std::vector<size_t> &neigh, int value) = 0;
	/** Estimate curvature of the point with index i. */
	virtual float EstimateCurvature(size_t i) = 0;
	/** Update curvature in the circular neighbourhood of the point with index i. */
	virtual void UpdateCurvature(size_t i, float change) = 0;
	/** Return true if the point with index i lies on the initial interface. */
	virtual bool IsBorderPoint(size_t i, const std::valarray<T> &data) = 0;
	/** Return true if the point with index i belongs to the computational domain (possibly given by binary mask). */
	virtual bool IsInDomain(size_t i);

};

/////////////////////////////////////////////////////////////////
//                                                             //
//         Class CompSwedenLevelSet_PreserveTopology           //
//                                                             //
/////////////////////////////////////////////////////////////////

/** Abstract class that represents routine for sweden level set method that 
  * preserves topology of the initial interface. */
template <class T> class CompSwedenLevelSet_PreserveTopology : virtual public CompSwedenLevelSet<T>
{
  protected:
	std::valarray<int> Component;

	using CompSwedenLevelSet<T>::ImgSize;
	using CompSwedenLevelSet<T>::ImplicitFunction;
	using CompSwedenLevelSet<T>::Curvature;
	using CompSwedenLevelSet<T>::heap;
	using CompSwedenLevelSet<T>::N;
	using CompSwedenLevelSet<T>::EstimateCurvature;
	using CompSwedenLevelSet<T>::UpdateCurvature;
	using CompSwedenLevelSet<T>::ComputeSpeedFunction;
	using CompSwedenLevelSet<T>::GetNeighbors;
	using CompSwedenLevelSet<T>::IsInDomain;
	using CompSwedenLevelSet<T>::Inflation;
	using CompSwedenLevelSet<T>::Domain;
	using CompSwedenLevelSet<T>::c;

  public:
    /** Constructor. */
	CompSwedenLevelSet_PreserveTopology(const i3d::Image3d<i3d::BINARY> *Domain, std::valarray<float> *ImageGrad, i3d::Vector3d<size_t> ImgSize, 
		                                i3d::Vector3d<float> Spacing, float alpha, float beta, float c, int p, 
					                    size_t N, size_t R);
	/** Set initial state of ImplicitFunction, Component and heap from the list of starting seeds or 
	  * from the image mask. */
	virtual void Init(const std::vector<size_t> *Seeds, const i3d::Image3d<T> *mask);
	/** Compute one iteration of sweden fast level set method that preserve topology of the interface. */
	virtual void ComputeIteration();
	/** Compute the segmentated image from current state of ImplicitFunction. */
	virtual void ComputeSegmentatedImage(i3d::Image3d<T> &output, SegmentationType type); 

  protected:
	/** Return true if the point with index i in Component is simple. */
	virtual bool IsSimplePoint(size_t i) = 0;
	/** Get the value of component which the point with index i belongs to. */
	virtual int GetComponent(size_t i) = 0;
	/** Set the value of component for the point with index i. */
	virtual void SetComponent(size_t i, int value) = 0;
	/** Get the index in Component of the point with index i in ImplicitFunction. */
	virtual size_t GetComponentIndex(size_t i, int move_x, int move_y, int move_z) = 0;
	/** Get the index in ImplicitFunction of the point with index i in Component. */
	virtual size_t GetIFIndex(size_t i) = 0;
};

/////////////////////////////////////////////////////////////////
//                                                             //
//                       Class CompSweden2D                    //
//                                                             //
/////////////////////////////////////////////////////////////////

template <class T> class CompSweden2D : virtual public CompSwedenLevelSet<T>
{
  public:
    /** Constructor. */
	CompSweden2D(const i3d::Image3d<i3d::BINARY> *Domain, std::valarray<float> *ImageGrad, i3d::Vector3d<size_t> ImgSize, i3d::Vector3d<float> Spacing, 
		float alpha, float beta, float c, int p, size_t N, size_t R): 
		CompSwedenLevelSet<T>(Domain, ImageGrad, ImgSize, Spacing, alpha, beta, c, p, N, R) {};

  protected:
	using CompSwedenLevelSet<T>::ImageGradient;
	using CompSwedenLevelSet<T>::alpha;
	using CompSwedenLevelSet<T>::Curvature;
	using CompSwedenLevelSet<T>::ImgSize;
	using CompSwedenLevelSet<T>::ImplicitFunction;
	using CompSwedenLevelSet<T>::N;
	using CompSwedenLevelSet<T>::R;
	using CompSwedenLevelSet<T>::BackPointers;
	using CompSwedenLevelSet<T>::heap;
	using CompSwedenLevelSet<T>::Spacing;
	using CompSwedenLevelSet<T>::p;
	using CompSwedenLevelSet<T>::beta;
	using CompSwedenLevelSet<T>::IsInDomain;
	using CompSwedenLevelSet<T>::Inflation;

  protected:
	/** Compute the value of speed function in point with index i. */
	virtual float ComputeSpeedFunction(size_t i);
	/** Compute neighbors of point with index i. The flag which signalized if we 
	  * want to get exterior(which is true) or interior(which is false) neighbors. */
	virtual void GetNeighbors(size_t i, std::vector<size_t> &neigh, int value);
	/** Estimate curvature of the point with index i. */
	virtual float EstimateCurvature(size_t i);
	/** Update curvature in the circular neighbourhood of the point with index i. */
	virtual void UpdateCurvature(size_t i, float change);
	/** It's helper function for estimation of curvature. Computes coordinates of left upper(lx,ly) and right
	  * lower(ux,uy) corner of circular neighbourhood of point with cordinates x,y. */
	inline void GetBounds(int x, int y, int &lx, int &ux, int &ly, int &uy);
	/** Return true if the point with index i lies on the initial interface. */
	virtual bool IsBorderPoint(size_t i, const std::valarray<T> &data);
};

/////////////////////////////////////////////////////////////////
//                                                             //
//            Class CompSweden2D_PreserveTopology              //
//                                                             //
/////////////////////////////////////////////////////////////////

template <class T> class CompSweden2D_PreserveTopology : public CompSwedenLevelSet_PreserveTopology<T>,
                                                         public CompSweden2D<T>
{
  public:
    /** Constructor. */
	CompSweden2D_PreserveTopology(const i3d::Image3d<i3d::BINARY> *Domain, std::valarray<float> *ImageGrad, i3d::Vector3d<size_t> ImgSize, 
		i3d::Vector3d<float> Spacing, float alpha, float beta, float c, int p, size_t N, size_t R):
	    CompSwedenLevelSet<T>(Domain, ImageGrad, ImgSize, Spacing, alpha, beta, c, p, N, R),
		CompSwedenLevelSet_PreserveTopology<T>(Domain, ImageGrad, ImgSize, Spacing, alpha, beta, c, p, N, R),
		CompSweden2D<T>(Domain, ImageGrad, ImgSize, Spacing, alpha, beta, c, p, N, R) {};
  
  protected:
	using CompSwedenLevelSet<T>::ImgSize;
	using CompSwedenLevelSet<T>::ImplicitFunction;
	using CompSwedenLevelSet<T>::IsInDomain;
	using CompSwedenLevelSet_PreserveTopology<T>::Component;
		
  protected:
	/** Compute neighbors of point with index i. The flag which signalized if we 
	  * want to get exterior(which is true) or interior(which is false) neighbors. */
	virtual void GetNeighbors(size_t i, std::vector<size_t> &neigh, int value);
	/** Return true if the point with index i in Component is simple. */
	virtual bool IsSimplePoint(size_t i);
	/** Get the value of component which the point with index i belongs to. */
	virtual int GetComponent(size_t i);
	/** Set the value of component for the point with index i. */
	virtual void SetComponent(size_t i, int value);
	/** Get the index in Component of the point with index i in ImplicitFunction. */
	virtual size_t GetComponentIndex(size_t i, int move_x, int move_y, int move_z);
	/** Get the index in ImplicitFunction of the point with index i in Component. */
	virtual size_t GetIFIndex(size_t i);
};

/////////////////////////////////////////////////////////////////
//                                                             //
//                       Class CompSweden3D                    //
//                                                             //
/////////////////////////////////////////////////////////////////

template <class T> class CompSweden3D : virtual public CompSwedenLevelSet<T>
{
  public:
    /** Constructor. */
	CompSweden3D(const i3d::Image3d<i3d::BINARY> *Domain, std::valarray<float> *ImageGrad, i3d::Vector3d<size_t> ImgSize, i3d::Vector3d<float> Spacing, 
		float alpha, float beta, float c, int p, size_t N, size_t R): 
		CompSwedenLevelSet<T>(Domain, ImageGrad, ImgSize, Spacing, alpha, beta, c, p, N, R) {};

  protected:
	using CompSwedenLevelSet<T>::ImageGradient;
	using CompSwedenLevelSet<T>::alpha;
	using CompSwedenLevelSet<T>::Curvature;
	using CompSwedenLevelSet<T>::ImgSize;
	using CompSwedenLevelSet<T>::ImplicitFunction;
	using CompSwedenLevelSet<T>::N;
	using CompSwedenLevelSet<T>::R;
	using CompSwedenLevelSet<T>::BackPointers;
	using CompSwedenLevelSet<T>::heap;
	using CompSwedenLevelSet<T>::Spacing;
	using CompSwedenLevelSet<T>::p;
	using CompSwedenLevelSet<T>::beta;
	using CompSwedenLevelSet<T>::IsInDomain;
	using CompSwedenLevelSet<T>::Inflation;

  protected:
	/** Compute the value of speed function in point with index i. */
	virtual float ComputeSpeedFunction(size_t i);
	/** Compute neighbors of point with index i. The flag which signalized if we 
	  * want to get exterior(which is true) or interior(which is false) neighbors. */
	virtual void GetNeighbors(size_t i, std::vector<size_t> &neigh, int value);
	/** Estimate curvature of the point with index i. */
	virtual float EstimateCurvature(size_t i);
	/** Update curvature in the circular neighbourhood of the point with index i. */
	virtual void UpdateCurvature(size_t i, float change);
	/** It's helper function for estimation of curvature. Compute coordinates of left upper(lx,ly,lz) and right 
	  * lower(ux,uy,uz) corner of circular neighbourhood of point with cordinates x,y,z. */
	inline void GetBounds(int x, int y, int z, int &lx, int &ux, int &ly, int &uy, int &lz, int &uz);
	/** Return true if the point with index i lies on the initial interface. */
	virtual bool IsBorderPoint(size_t i, const std::valarray<T> &data);
};

/////////////////////////////////////////////////////////////////
//                                                             //
//            Class CompSweden3D_PreserveTopology              //
//                                                             //
/////////////////////////////////////////////////////////////////

template <class T> class CompSweden3D_PreserveTopology : public CompSwedenLevelSet_PreserveTopology<T>,
                                                         public CompSweden3D<T>  
{
  public:
    /** Constructor. */
    CompSweden3D_PreserveTopology(const i3d::Image3d<i3d::BINARY> *Domain, std::valarray<float> *ImageGrad, i3d::Vector3d<size_t> ImgSize, 
		i3d::Vector3d<float> Spacing, float alpha, float beta, float c, int p, size_t N, size_t R):
		CompSwedenLevelSet<T>(Domain, ImageGrad, ImgSize, Spacing, alpha, beta, c, p, N, R),
		CompSwedenLevelSet_PreserveTopology<T>(Domain, ImageGrad, ImgSize, Spacing, alpha, beta, c, p, N, R),
		CompSweden3D<T>(Domain, ImageGrad, ImgSize, Spacing, alpha, beta, c, p, N, R) {};

  protected:
	using CompSwedenLevelSet<T>::ImgSize;
	using CompSwedenLevelSet<T>::ImplicitFunction;
	using CompSwedenLevelSet<T>::IsInDomain;
	using CompSwedenLevelSet_PreserveTopology<T>::Component;

  protected:
	/** Compute neighbors of point with index i. The flag which signalized if we 
	  * want to get exterior(which is true) or interior(which is false) neighbors. */
	virtual void GetNeighbors(size_t i, std::vector<size_t> &neigh, int value);
	/** Return true if the point with index i in Component is simple. */
	virtual bool IsSimplePoint(size_t i);
	/** Get the value of component which the point with index i belongs to. */
	virtual int GetComponent(size_t i);
	/** Set the value of component for the point with index i. */
	virtual void SetComponent(size_t i, int value);
	/** Get the index in Component of the point with index i in ImplicitFunction. */
	virtual size_t GetComponentIndex(size_t i, int move_x, int move_y, int move_z);
	/** Get the index in ImplicitFunction of the point with index i in Component. */
	virtual size_t GetIFIndex(size_t i);
};

/////////////////////////////////////////////////////////////////
//                                                             //
//                   Class SwedenFastLevelSet                  //
//                                                             //
/////////////////////////////////////////////////////////////////

template <class VOXELIN, class VOXELOUT> class SwedenFastLevelSet
{
  protected:
	i3d::Image3d<VOXELOUT> &output;
	CompSwedenLevelSet<VOXELOUT> *Comp;
	SegmentationType type;

  public:
    /** Constructor. */
	SwedenFastLevelSet(const i3d::Image3d<VOXELIN> &input, i3d::Image3d<VOXELOUT> &out, 
					   const std::vector<size_t> *Seeds, const i3d::Image3d<VOXELIN> *mask, const i3d::Image3d<i3d::BINARY> *Domain,
					   i3d::Vector3d<float> Spacing, float alpha, float beta, float c, int p, size_t R, 
					   double sigma_x, double sigma_y, double sigma_z, double width, 
					   SegmentationType type, bool preserve_topology);
	/** Destructor. */
	~SwedenFastLevelSet() { if (Comp) delete Comp; }
    /** Execute one iteration of sweden fast level set method. */
	void ComputeIteration() { Comp -> ComputeIteration(); }
	/** Return true if another iteration can be computed. */
	bool IsOk() { return Comp -> IsOk(); };
	/** Return pointer to the first position of ImplicitFunction. */
	int * GetFirstPoint() { return Comp -> GetFirstPoint(); }
	/** Compute the segmentated image from current state of ImplicitFunction. */
	void ComputeSegmentatedImage() { Comp -> ComputeSegmentatedImage(output, type); } 
	/** Get heap size. */
	size_t GetHeapSize() { return Comp -> GetHeapSize(); }
};


/////////////////////////////////////////////////////////////////
//                                                             //
//                   Class CompChineseLevelSet                 //
//                                                             //
/////////////////////////////////////////////////////////////////

template <class T> class CompChineseLevelSet
{
  protected:
    ChineseHeap *heap;
	std::valarray<float> ImplicitFunction;
	std::valarray<int> BackPointers;
	std::valarray<float> KindOfPoint;
	std::valarray<float> SpeedFunction;
	std::valarray<float> ImageGradient;
	
	i3d::Vector3d<size_t> ImgSize;
	i3d::Vector3d<float> Spacing;
    float Global_time;
	float alpha, beta; // parameters of speed term
	int p; // parameter of propagation speed
	
  public:
    /** Constructor */
	CompChineseLevelSet(std::valarray<float> *ImageGrad, std::vector<size_t> &Seeds, i3d::Vector3d<size_t> _ImgSize, 
		                i3d::Vector3d<float> _Spacing, float _alpha, float _beta, int _p);
	/** Destructor */
	virtual ~CompChineseLevelSet() { delete heap; }
	/** Compute one iteration of chinese fast level set method. */
	void ComputeIteration();
	/** Return pointer to the first position of ImplicitFunction. */
	float * GetFirstPoint() { return &KindOfPoint[0]; }
	/** Compute the segmentated image from current state of ImplicitFunction. */
	void ComputeSegmentatedImage(i3d::Image3d<T> &output, SegmentationType type); 

  protected:
	/** Set initial state of ImplicitFunction and heap from list of starting seeds. */
	void Init(std::vector<size_t> &Seeds); 

  private:
    /** Compute signed distance function in 5x5 neighbourhood of point with index i. */
    virtual void ComputeSignedDistance(size_t i) = 0;
	/** Compute gradient of implicit function in point with index i. */
	virtual float ComputeGradient(size_t i) = 0;
	/** Compute the value of speed function in point with index i. */
	virtual float ComputeSpeedFunction(size_t i) = 0;
	/** Compute neighbors of point with index i. The size of neighbourhood is size.
	  * The flag all signalized if we want to get all neighbors or only active points in neighbourhood. */
	virtual void GetNeighbors(size_t size, size_t i, std::vector<size_t> &neigh, bool all) = 0;
	/** Test if the point with index i is an active point. */
	virtual bool IsActivePoint(size_t i) = 0;
};

/////////////////////////////////////////////////////////////////
//                                                             //
//                   Class CompChinese2D                       //
//                                                             //
/////////////////////////////////////////////////////////////////

template <class T> class CompChinese2D : public CompChineseLevelSet<T>
{
  public:
    /** Constructor. */
	CompChinese2D(std::valarray<float> *ImageGrad, std::vector<size_t> &Seeds, i3d::Vector3d<size_t> ImgSize, 
		          i3d::Vector3d<float> Spacing, float alpha, float beta, int p);

	using CompChineseLevelSet<T>::heap;
	using CompChineseLevelSet<T>::ImplicitFunction;
	using CompChineseLevelSet<T>::BackPointers;
	using CompChineseLevelSet<T>::KindOfPoint;
	using CompChineseLevelSet<T>::SpeedFunction;
	using CompChineseLevelSet<T>::ImageGradient;
	using CompChineseLevelSet<T>::ImgSize;
	using CompChineseLevelSet<T>::Spacing;
	using CompChineseLevelSet<T>::alpha;
	using CompChineseLevelSet<T>::beta;
	using CompChineseLevelSet<T>::p;
	using CompChineseLevelSet<T>::Init;
	
  private:
    /** Compute signed distance function in 5x5 neighbourhood of point with index i. */
    virtual void ComputeSignedDistance(size_t i);
	/** Compute gradient of implicit function in point with index i. */
	virtual float ComputeGradient(size_t i);
	/** Compute the value of speed function in point with index i. */
	virtual float ComputeSpeedFunction(size_t i);
	/** Compute neighbors of point with index i. The size of neighbourhood is size.
	  * The flag all signalized if we want to get all neighbors or only active points in neighbourhood. */
	virtual void GetNeighbors(size_t size, size_t i, std::vector<size_t> &neigh, bool all);
	/** Test if the point with index i is an active point. */
	virtual bool IsActivePoint(size_t i);
};

/////////////////////////////////////////////////////////////////
//                                                             //
//                   Class CompChinese3D                       //
//                                                             //
/////////////////////////////////////////////////////////////////

template <class T> class CompChinese3D : public CompChineseLevelSet<T>
{
  public:
    /** Constructor. */
	CompChinese3D(std::valarray<float> *ImageGrad, std::vector<size_t> &Seeds, i3d::Vector3d<size_t> ImgSize, 
		          i3d::Vector3d<float> Spacing, float alpha, float beta, int p);

	using CompChineseLevelSet<T>::heap;
	using CompChineseLevelSet<T>::ImplicitFunction;
	using CompChineseLevelSet<T>::BackPointers;
	using CompChineseLevelSet<T>::KindOfPoint;
	using CompChineseLevelSet<T>::SpeedFunction;
	using CompChineseLevelSet<T>::ImageGradient;
	using CompChineseLevelSet<T>::ImgSize;
	using CompChineseLevelSet<T>::Spacing;
	using CompChineseLevelSet<T>::alpha;
	using CompChineseLevelSet<T>::beta;
	using CompChineseLevelSet<T>::p;
	using CompChineseLevelSet<T>::Init;

  private:
    /** Compute signed distance function in 5x5 neighbourhood of point with index i. */
    virtual void ComputeSignedDistance(size_t i);
    /** Compute gradient of implicit function in point with index i. */
	virtual float ComputeGradient(size_t i);
	/** Compute the value of speed function in point with index i. */
	virtual float ComputeSpeedFunction(size_t i);
	/** Compute neighbors of point with index i. The size of neighbourhood is size.
	  * The flag all signalized if we want to get all neighbors or only active points in neighbourhood. */
	virtual void GetNeighbors(size_t size, size_t i, std::vector<size_t> &neigh, bool all);
	/** Test if the point with index i is an active point. */
	virtual bool IsActivePoint(size_t i);
};


/////////////////////////////////////////////////////////////////
//                                                             //
//                   Class ChineseFastLevelSet                 //
//                                                             //
/////////////////////////////////////////////////////////////////

template <class VOXELIN, class VOXELOUT> class ChineseFastLevelSet
{
  private:
	i3d::Image3d<VOXELOUT> &output;
	CompChineseLevelSet<VOXELOUT> *Comp;
	SegmentationType type;

  public:
    /** Constructor. */
	ChineseFastLevelSet(const i3d::Image3d<VOXELIN> &input, i3d::Image3d<VOXELOUT> &out, std::vector<size_t> &Seeds, 
		                i3d::Vector3d<float> Spacing, float alpha, float beta, int p, double sigma_x, double sigma_y, 
						double sigma_z, double width, SegmentationType type);
	/** Destructor. */
	~ChineseFastLevelSet() { delete Comp; }
	/** Execute one iteration of sweden fast level set method. */
	void ComputeIteration() { Comp -> ComputeIteration(); }	  
	/** Return pointer to the first position of ImplicitFunction. */
	float * GetFirstPoint() { return Comp -> GetFirstPoint(); }
	/** Compute the segmentated image from current state of ImplicitFunction. */
	void ComputeSegmentatedImage() { Comp -> ComputeSegmentatedImage(output, type); } 
};

/////////////////////////////////////////////////////////////////
//                                                             //
//                   Class SparseFieldRoutine                  //
//                                                             //
/////////////////////////////////////////////////////////////////

template <class T> class SparseFieldRoutine
{
  protected:
    /** In this array are saved values of the implicit function for each image point. During each iteration some values are
	  * changed according to the move of the interface. */
	std::valarray<float> ImplicitFunction;
	/** A reference to the array with the gradient magnitude at each image point. The gradient is estimated by simple central
	  * differences in the direction of each axe. The magnitude is computed as a square root of the sum of squares of 
	  * the individual partial derivatives. */
	std::valarray<float> ImageGradient;
    /** In this array are saved values of the status for each image point. The status gives a number of layer in which the 
	  * image point is located. We use odd numbers for the inner layers and even numbers for the outer layers. For the active
	  * layer we use a status \f$0\f$. If the image point doesn't belong to any layer it has a status 
	  * \c SparseFieldRoutine::StatusNull. */ 
	std::valarray<int> Status;
	/** In this vector are saved the individual layers as the lists of the image points that belong to them. We use only five
	  * layers - the active layer, two inner and two outer layers. */
	std::vector<std::list<size_t> > Layers;
	/** This constant gives a status of the image point that doesn't belong to any layer. */
	static const int StatusNull = -1;
	/** This constant gives a status of the image point that changes its status. */
	static const int StatusChange = -2;
	/** This vector represents the image size in each direction. */
	const i3d::Vector3d<size_t> ImgSize;
	/** This constant gives a distance between two adjoining image points in all directions. */
	const float spacing;
	/** This constant gives a inverse value of the \c i3d::SparseFieldRoutine::spacing. It's used to limit the curvature. */
	const float CurvatureLimit;
	/** This constant gives a bound for the active range. The active range is a interval 
	  * \f$[-\frac{spacing}{2},\frac{spacing}{2}]\f$. */
	const float ActiveLayerBound;
	/** This constant gives a relative impact of the interface smoothing by the curvature in a speed term. Details can be seen
	  * in the \c i3d::SparseField function. */
	const float alpha;
	/** This constant gives a relative impact of the interface attraction towards the edges in the processed image. It's used 
	  * in a speed term and details can be seen in the \c i3d::SparseField function. */
	const float beta;
	/** Parameter of propagation speed. */
	const int p;
    /** This variable gives a time step for each iteration of the routine. */
    float TimeStep;
	float MaxAdv, MaxProp;
	
  public:
	/** Constructor. */
	SparseFieldRoutine(std::valarray<float> *ImageGrad, std::vector<size_t> &Seeds, i3d::Vector3d<size_t> _ImgSize, 
		               float _spacing, float _alpha, float _beta, int _p);
	/** Destructor. */
	virtual ~SparseFieldRoutine() {};
	/** Compute one iteration of sparse-field method. */
	void ComputeIteration();
	/** Return pointer to the first position of ImplicitFunction. */
	float * GetFirstPoint() { return &ImplicitFunction[0]; }
	/** Compute the segmentated image from current state of ImplicitFunction. */
	void ComputeSegmentatedImage(i3d::Image3d<T> &output, SegmentationType type); 

  protected:
	/** Set an initial state of the \c SparseFieldRoutine::ImplicitFunction, the \c SparseFieldRoutine::Status and the 
	  * \c SparseFieldRoutine::Layers from the list of starting points. These points define an initial interface. */
	void Init(std::vector<size_t> &Seeds);

  private:
    /** Compute the value \f$F|\nabla \phi|\f$ in the image point with the index \f$i\f$. */
	virtual float ComputeSpeedFunction(size_t i) = 0;
	/** Compute the neighbors of the image point with the index \f$i\f$. We find only the neighbors from the layer specified
	  * by the parameter \c layer. */ 
	virtual void GetNeighbors(size_t i, std::vector<size_t> &neigh, int layer) = 0;
	/** Update the \c SparseFieldRoutine::ImplicitFunction of the active points and insert the image point that fall out the
	  * active range into the list \c up or \c down according to its new value of \c SparseFieldRoutine::ImplicitFunction. */
	void UpdateInterface(std::list<size_t> &up, std::list<size_t> &down);
	/** Update the status of each image point from the list \c input to the status \c ChangeTo. The parameter \c SearchFor
	  * gives the status of its neighbors that will be added to the list \c output and its status will be updated later. */
	void UpdateStatus(std::list<size_t> &input, std::list<size_t> &output, size_t ChangeTo, int SearchFor);
    /** Update the status of each image point from the list \c input to the status \c ChangeTo. It's used only for the image
	  * points that don't belong to any layer and move to the most inner or outer layer. */
	void UpdateBorderLayer(std::list<size_t> &input, size_t ChangeTo);
	/** Update the \c SparseFieldRoutine::ImplicitFunction of the image points from the layer \c to. For this purpose we use
	  * the \c SparseFieldRoutine::ImplicitFunction of the image points from the layer \c from. Any image point from the layer
	  * \c to without the neighbors in the layer \c from is moved to the layer \c promote or deleted if \c promote is greater
	  * than a number of the most inner or outer layer, i.e. \c promote is the same as a number of layers. */
    void UpdateImplicitFunction(size_t from, size_t to, size_t promote);
};

/////////////////////////////////////////////////////////////////
//                                                             //
//                   Class SparseFieldRoutine2D                //
//                                                             //
/////////////////////////////////////////////////////////////////

template <class T> class SparseFieldRoutine2D : public SparseFieldRoutine<T>
{
  public:
    /** Constructor. */
	SparseFieldRoutine2D(std::valarray<float> *ImageGrad, std::vector<size_t> &Seeds, i3d::Vector3d<size_t> ImgSize, 
	    	             float spacing, float alpha, float beta, int p);

	using SparseFieldRoutine<T>::ImgSize;
	using SparseFieldRoutine<T>::ImplicitFunction;
	using SparseFieldRoutine<T>::spacing;
	using SparseFieldRoutine<T>::ImageGradient;
	using SparseFieldRoutine<T>::p;
	using SparseFieldRoutine<T>::MaxProp;
	using SparseFieldRoutine<T>::MaxAdv;
	using SparseFieldRoutine<T>::CurvatureLimit;
	using SparseFieldRoutine<T>::alpha;
	using SparseFieldRoutine<T>::beta;
	using SparseFieldRoutine<T>::Status;
	using SparseFieldRoutine<T>::Init;

  private:
	/** Compute the value \f$F|\nabla \phi|\f$ in the image point with the index \f$i\f$. */
	virtual float ComputeSpeedFunction(size_t i);
	/** Compute the neighbors of the image point with the index \f$i\f$. We find only the neighbors from the layer specified
	  * by the parameter \c layer. */ 
	virtual void GetNeighbors(size_t i, std::vector<size_t> &neigh, int layer);
};

/////////////////////////////////////////////////////////////////
//                                                             //
//                   Class SparseFieldRoutine3D                //
//                                                             //
/////////////////////////////////////////////////////////////////

template <class T> class SparseFieldRoutine3D : public SparseFieldRoutine<T>
{
  public:
    /** Constructor. */
	SparseFieldRoutine3D(std::valarray<float> *ImageGrad, std::vector<size_t> &Seeds, i3d::Vector3d<size_t> ImgSize, 
	    	             float spacing, float alpha, float beta, int p);

	using SparseFieldRoutine<T>::ImgSize;
	using SparseFieldRoutine<T>::ImplicitFunction;
	using SparseFieldRoutine<T>::spacing;
	using SparseFieldRoutine<T>::ImageGradient;
	using SparseFieldRoutine<T>::p;
	using SparseFieldRoutine<T>::MaxProp;
	using SparseFieldRoutine<T>::MaxAdv;
	using SparseFieldRoutine<T>::CurvatureLimit;
	using SparseFieldRoutine<T>::alpha;
	using SparseFieldRoutine<T>::beta;
	using SparseFieldRoutine<T>::Status;
	using SparseFieldRoutine<T>::Init;

  private:
	/** Compute the value \f$F|\nabla \phi|\f$ in the image point with the index \f$i\f$. */
	virtual float ComputeSpeedFunction(size_t i);
	/** Compute the neighbors of the image point with the index \f$i\f$. We find only the neighbors from the layer specified
	  * by the parameter \c layer. */ 
	virtual void GetNeighbors(size_t i, std::vector<size_t> &neigh, int layer);
};

/////////////////////////////////////////////////////////////////
//                                                             //
//                   Class SparseFieldMethod                   //
//                                                             //
/////////////////////////////////////////////////////////////////

template <class VOXELIN, class VOXELOUT> class SparseFieldMethod
{
  private:
	i3d::Image3d<VOXELOUT> &output;
	SparseFieldRoutine<VOXELOUT> *Routine;
	SegmentationType type;

  public:
    /** Constructor. */
	SparseFieldMethod(const i3d::Image3d<VOXELIN> &input, i3d::Image3d<VOXELOUT> &out, std::vector<size_t> &Seeds, 
		              i3d::Vector3d<float> Spacing, float alpha, float beta, int p, double sigma_x, double sigma_y, 
					  double sigma_z, double width, SegmentationType type);
	/** Destructor. */
	~SparseFieldMethod() { delete Routine; }
	/** Execute one iteration of sweden fast level set method. */
	void ComputeIteration() { Routine -> ComputeIteration(); }	  
	/** Return pointer to the first position of ImplicitFunction. */
	float * GetFirstPoint() { return Routine -> GetFirstPoint(); }
	/** Compute the segmentated image from current state of ImplicitFunction. */
	void ComputeSegmentatedImage() { Routine -> ComputeSegmentatedImage(output, type); } 
};

#endif

