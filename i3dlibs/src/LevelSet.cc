/*
 * i3dlib - image manipulation library
 *
 * Copyright (C) 2000-2006   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 * FILE : LevelSet.cc
 *
 * Jan Hubeny <xhubeny@fi.muni.cz> 2005
 *
 */

#ifdef __GNUG__
#pragma implementation "LevelSet.h"
#endif

#include "LevelSet.h"
#include "histogram.h"
#include "filters.h"
#include "image3d.h"
#include "regions.h"
#include "neighbours.h"
#include "morphology.h"
#include <stdlib.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <cmath>
#endif

#ifdef _MSC_VER
   #include <hash_set>
   #define HASH_SET stdext::hash_set
   #ifdef TIME_MEASUREMENT
   #include <time.h>
   #endif
#else
   #ifdef TIME_MEASUREMENT
   #include <sys/time.h>
   #include <sys/times.h>
   #endif
   #include <ext/hash_set>
   #define HASH_SET __gnu_cxx::hash_set
#endif

#ifdef WITH_LAPACK
	// C++ header of function sptsv_ located in the LAPACK library
	extern "C" void sptsv_(const int &n, const int &nrhs, float *d, float *e, float *b, const int &ldb, int &info);
	// C++ header of function sgtsv_ located in the LAPACK library
	extern "C" void sgtsv_(const int &n, const int &nrhs, float *dl, float *d, float *du, float *b, const int &ldb, int &info);
	// C++ header of function dptsv_ located in the LAPACK library
	extern "C" void dptsv_(const int &n, const int &nrhs, double *d, double *e, double *b, const int &ldb, int &info);
#endif

/* Defines of LevelSet.cc */
#define UNDEFINED (std::numeric_limits<size_t>::max())
#define INFTIME(voxel) (std::numeric_limits<voxel>::max()) 
#define INFTIMEM(voxel) (-std::numeric_limits<voxel>::max()) 
#define SQR(a) ((a)*(a))
#define MEMINCREMENT (ArrivalTimes.size()/10)
#define SIDE(a,b,h) (((b)-(a))/(h))
#define CENT(a,b,h) (((b)-(a))/(2*(h)))
#define MAX(a,b) ((a)>(b)?(a):(b))
#define MIN(a,b) ((a)<(b)?(a):(b))
#define ABSI(a) (a*(1-(2*(a >> std::numeric_limits<int>::digits))))
#define ZERO 0
#define KNOWN 1
#define TRIAL 2
#define FARP 4
#define NEIGHBORS ++Neighbors; *Neighbors = PointId;
#define NEIGH neighbourhood->Neighbors(neighbors, index, size);
#define X_AXE 0
#define Y_AXE 1
#define Z_AXE 2

			       
			       
using namespace std;

/*
 *
 *
 *  Begin of Heap Code
 *
 *
 */


/**
 pomocn� struktura pro haldu. Oper�tor () vrac� TRUE pokud a je v�t�� jak b. V opa�n�m p��pad� vrac� FALSE.
 */
struct sgngreater
{
    public:
	template <class T> bool operator () (const T& a, const T& b) { return (a > b); }
	template <class T> T operator() (T a) {return a;}
};

/*
pomocn� struktura pro haldu. Oper�tor () vrac� TRUE pokud je abs(a) > abs(b). V opa�n�m p��pad� vrac� FALSE. instanciace s double nebo float si vynucuje includovat \<cmath\>. Instanciace s int nebo long si vynucuje include \<cstdlib\>
 */
struct absgreater
{
    public:
	template <class T> bool operator () (const T& a, const T& b) { return (abs(a) > abs(b)) ;}
	template <class T> T operator() (T a) {return abs(a);}
};


/** Heap used to sort trial points in Fast Marching Method */
template <class VOXEL, class COMP> class Heap 
{
    /** Reference to external Arrival Times Array. The heap is sorted by the values in this array */
	const std::valarray<VOXEL> & ArrivalTimes;
    public:
    	COMP compare;
	/** Array of Back pointers to the heap. In this array is saved whether and where on the heap is located the voxel with arrival time saved in the valarray ArrivalTimes */
	std::valarray<size_t> BackPointers;
	/** Vector with indices to the ArrivalTimes valarray, it is the heap*/
	size_t * Data;
	long int HeapBound, HeapSize;
	/* Constructor */
	Heap (const std::valarray<VOXEL> & at);
	/* Destructor */
	~Heap () { free(Data);};
	/** Insert into heap */
	void Push ( size_t Index);
	/** Pop from heap */
	 size_t Pop();
	/** Rebalance the heap upward */
	void BalanceUp(size_t HeapIndex);
	/** Rebalance the heap upward */
	void BalanceTrialUp(size_t PointId);
	/** Rebalance the heap downward */ 
	void BalanceDown(size_t HeapIndex);
	void Print();
	
};

/** Heap constructor */
template  <class VOXEL, class COMP> Heap<VOXEL, COMP>::Heap(const std::valarray<VOXEL> & at)
: ArrivalTimes(at), BackPointers(UNDEFINED,at.size())
{
    Data = (size_t *) malloc(sizeof(size_t) * MEMINCREMENT);
    HeapSize = MEMINCREMENT;
    HeapBound = -1;
}

/** Push an index to arrival times array (arrival time) on the heap */
template  <class VOXEL, class COMP> void Heap<VOXEL, COMP>::Push ( size_t Index)
{
 //   cout << "Pushing point: " << Index << " Value: " << ArrivalTimes[Index] << endl;
    if (HeapBound + 1 == HeapSize)
    {
	HeapSize +=  MEMINCREMENT;
	Data = (size_t*) realloc(Data,HeapSize * sizeof(size_t));
    }
    HeapBound++;
    Data[HeapBound] = Index;
    BackPointers[Index] = HeapBound;
    BalanceUp(HeapBound);
}

/** Pop from the heap , returns index to ArrivalTimes array, or UNDEFINED if the Heap is empty*/
template  <class VOXEL, class COMP> size_t Heap<VOXEL, COMP>::Pop()
{
    size_t pom;
    if (HeapBound == -1)
    {
//	cout << "Heap is empty!\n";
	return UNDEFINED;
    }
    else
    {
	pom = Data[0];
//	cout << "Poping from the idx: " << pom << " value: " << ArrivalTimes[pom] << endl;
	BackPointers[pom] = UNDEFINED; 
	Data[0] = Data[HeapBound];
	HeapBound--;
	BalanceDown(0);
	return pom;
    }     
}

/** This balances The Heap upwards*/
template  <class VOXEL, class COMP> void Heap<VOXEL, COMP>::BalanceUp(size_t HeapIndex)
{
    size_t Parent = (HeapIndex - 1 ) / 2;
    size_t pom;

//    cout << "BalanceUp\n";
    while ((HeapIndex > 0) && ( compare(ArrivalTimes[Data[Parent]], ArrivalTimes[Data[HeapIndex]]) ))
    {
//	Print();
	/* swap the back pointers*/
	BackPointers[Data[HeapIndex]] = Parent;
	BackPointers[Data[Parent]] = HeapIndex;
	/* swap the indices saved on the Heap */
	pom = Data[HeapIndex];
	Data[HeapIndex] = Data[Parent];
	Data[Parent] = pom;
	HeapIndex = Parent;
	Parent = (HeapIndex - 1) / 2;
    }
//	Print();
}

/** This balances The Heap upwards with the index from backpointer*/
template  <class VOXEL, class COMP> void Heap<VOXEL, COMP>::BalanceTrialUp(size_t PointId)
{
    size_t HeapIndex =  BackPointers[PointId];
    size_t Parent = (HeapIndex - 1 ) / 2;
    size_t pom;

    while ((HeapIndex > 0) && ( compare(ArrivalTimes[Data[Parent]], ArrivalTimes[Data[HeapIndex]]) ))
    {
	/* swap the back pointers*/
	BackPointers[Data[HeapIndex]] = Parent;
	BackPointers[Data[Parent]] = HeapIndex;
	/* swap the indices saved on the Heap */
	pom = Data[HeapIndex];
	Data[HeapIndex] = Data[Parent];
	Data[Parent] = pom;
	HeapIndex = Parent;
	Parent = (HeapIndex - 1) / 2;
    }
//	Print();
}


/** This balances the heap downwards*/
template  <class VOXEL, class COMP> void Heap<VOXEL, COMP>::BalanceDown(size_t HeapIndex)
{
    size_t pom;
    size_t SecondChild = (2 * HeapIndex) + 2;
    
    while (SecondChild < static_cast <size_t> (HeapBound+1))
    {
	if (compare(ArrivalTimes[Data[SecondChild]] , ArrivalTimes[Data[SecondChild - 1]] ))
	{
	    SecondChild--;
	}
	if (compare(ArrivalTimes[Data[HeapIndex]] , ArrivalTimes[Data[SecondChild]]))
	{
	    BackPointers[Data[HeapIndex]] = SecondChild;
	    BackPointers[Data[SecondChild]] = HeapIndex;
	    pom = Data[HeapIndex];
	    Data[HeapIndex] = Data[SecondChild];
	    Data[SecondChild] = pom;
	    HeapIndex = SecondChild;
	    SecondChild = (2 * HeapIndex) + 2;
	}
	else
	{
	    break;
	}
    }

    if (SecondChild ==  static_cast <size_t> (HeapBound+1))
    {
	SecondChild--;
	if (compare(ArrivalTimes[Data[HeapIndex]] , ArrivalTimes[Data[SecondChild]]))
	{
	    BackPointers[Data[HeapIndex]] = SecondChild;
	    BackPointers[Data[SecondChild]] = HeapIndex;
	    pom = Data[HeapIndex];
	    Data[HeapIndex] = Data[SecondChild];
	    Data[SecondChild] = pom;
	}

    }
}

// This prints The heap
template  <class VOXEL, class COMP> void Heap<VOXEL, COMP>::Print()
{
    size_t i,j = -1;
    cout << "\nPrinting the Heap\n";
    for ( i = 0; i < (HeapBound+1); i++)
    {
	cout << ArrivalTimes[Data[i]] << " ";
	if (i == (2*j + 2))
	{
	    cout << endl;
	    j = i;
	}
    }
    cout << "\n";
}
/*
 *
 *
 *  End of Heap Code
 *
 *
 */


/*
 *
 *
 * Fast Marching supplemental functions Begin
 *
 *
 */



/* 
     The TwoNeighbors function finds the indices of 4-neighborhood of the point more faster than the standard routines in neighbors.cc
		3                                1 
      will be the neighbors of  1 2  look like 1 1 2
                                3                3
				
*/
void TwoNeighbors(size_t * Neighbors, size_t PointId, i3d::Vector3d<size_t> & sz, int axe = X_AXE )
{
    const size_t sizexy = sz.x * sz.y;
    const size_t size = sizexy * sz.z;
    *Neighbors = PointId;
    switch (axe)
    {
	case X_AXE:
	    // The 0. neighbor
	    if (!((PointId % sz.x) == 0 ))
		*Neighbors  -= 1;
	    // The 1. neighbor
	     NEIGHBORS
	    if (!( (PointId % sz.x) == (sz.x - 1) ))
		*Neighbors  += 1;
	    break;
    	case Y_AXE:
	    // The 2. neighbor
	    if (!(( PointId % sizexy) < sz.x ))
		*Neighbors -= sz.x;
	    // The 3. neighbor
	    NEIGHBORS
	     if ( ( ((PointId % sizexy) + sz.x) < sizexy ) )
		*Neighbors  += sz.x;
	    break;
 	case Z_AXE:
	    {
	     // The 4. neighbor
	    if (!(PointId < sizexy))
		*Neighbors -= sizexy;
	     // The 5. neighbor
	    NEIGHBORS
	     if (!( (PointId + sizexy) >= size))
		*Neighbors += sizexy;
	    }
	    break;
    }
}

/* 
     The FourNeighbors function finds the indices of 4-neighborhood of the point more faster than the standard routines in neighbors.cc
      o3
      |
  0o--S--o1 the indices of Neighbors of th point S PointId are saved in 
      |     Neighbors array [0,1,2,3]
      o2
      On the boundary-voxels neighbors which are out of the image are mirrored 
      So 0 <--> 2 and 1<--->3    -- comented out, now used the Zero Flux Neumann Boundary condition
      So on the border (pixel value on the border is equal to 1)
                1 2 5 8
		3 4
		3                                1 
      will be the neighbors of  1 2  look like 1 1 2
                                3                3
				
*/
void FourNeighbors(size_t * Neighbors, size_t PointId, i3d::Vector3d<size_t> & sz)
{
    size_t size = sz.x * sz.y;
    *Neighbors = PointId;
    // The 0. neighbor
    if (!((PointId % sz.x) == 0 ))
	*Neighbors  -= 1;
    // The 1. neighbor
     NEIGHBORS
    if (!( (PointId % sz.x) == (sz.x - 1) ))
	*Neighbors  += 1;
    // The 2. neighbor
    NEIGHBORS
    if (!( PointId < sz.x ))
	*Neighbors -= sz.x;
    // The 3. neighbor
    NEIGHBORS
     if (!( (PointId + sz.x) >= size ))
	*Neighbors  += sz.x;
}

/* The SixNeighbors function finds the indices of 6-neighborhood of the point more faster than the standard routines in neighbors.cc
     5o o3
      |/
  0o--S--o1 the indices of Neighbors of the point S PointId are saved in 
     /|     Neighbors array [0,1,2,3,4,5]
   2o o4
      On the boundary-voxels neighbors which are out of the image are mirrored 
      So 0 <--> 2 and 1<--->3 and 4<--->5  -- comented out, now used the Zero Flux Neumann Boundary condition
      So on the border (pixel value on the border is equal to 1)
                1 2 5 8
		3 4                                 1
		3                                1 /
      will be the neighbors of  1 2  look like 1 1 2
                                3                3

*/

void SixNeighbors(size_t * Neighbors, size_t PointId, i3d::Vector3d<size_t> & sz)
{
    size_t sizexy = sz.x * sz.y;
    size_t size = sizexy * sz.z;
    // The 0. neighbor
    *Neighbors = PointId;
    if (!( (PointId % sz.x) == 0 ))
	*Neighbors -= 1;
    // The 1. neighbor
     NEIGHBORS
    if (!( (PointId % sz.x) == (sz.x - 1) ))
	*Neighbors = PointId + 1;
     // The 2. neighbor
    NEIGHBORS
    if (!( (PointId % sizexy) < sz.x ))
	*Neighbors -= sz.x;
     // The 3. neighbor
    NEIGHBORS
     if (!( (PointId % sizexy) >=  (sizexy - sz.x) ))
	*Neighbors += sz.x;
     // The 4. neighbor
    NEIGHBORS
    if (!(PointId < sizexy))
	*Neighbors -= sizexy;
     // The 5. neighbor
    NEIGHBORS
     if (!( (PointId + sizexy) >= size))
	*Neighbors += sizexy;
}

/* 
     The EightNeighbors function finds the indices of 4-neighborhood of the point more faster than the standard routines in neighbors.cc
  6o  o3 o7
    \ | /
  0o--S--o1 the indices of Neighbors of th point S PointId are saved in 
    / | \   Neighbors array [0,1,2,3]
  4o  o2 o5
      On the boundary-voxels neighbors which are out of the image are mirrored 
      So 0 <--> 2 and 1<--->3    -- comented out, now used the Zero Flux Neumann Boundary condition
      So on the border (pixel value on the border is equal to 1)
                1 2 5 8
		3 4
		3                              1 1 2
      will be the neighbors of  1 2  look like 1 1 2
                                3              3 3 4
				
*/

void EightNeighbors(size_t * Neighbors, size_t PointId, i3d::Vector3d<size_t> & sz)
{
    size_t size = sz.x * sz.y;
    *Neighbors = PointId;
    // The 0. neighbor
    if (!((PointId % sz.x) == 0 ))
	*Neighbors  -= 1;
    // The 1. neighbor
     NEIGHBORS
    if (!( (PointId % sz.x) == (sz.x - 1) ))
	*Neighbors  += 1;
    // The 2. neighbor
    NEIGHBORS
    if (!( PointId < sz.x ))
	*Neighbors -= sz.x;
    // The 3. neighbor
    NEIGHBORS
     if (!( (PointId + sz.x) >= size ))
	*Neighbors  += sz.x;
    // The 4. and 6. neighbor
    Neighbors++; *Neighbors =  *(Neighbors - 2);
    *(Neighbors + 2) = *(Neighbors - 1);
    if ( !(PointId % sz.x == 0))
    {
        *Neighbors -= 1;
	*(Neighbors +2) -=1;
    }
    // The 5. and 7. neighbor
    Neighbors++; *Neighbors = *(Neighbors - 3);
    *(Neighbors + 2) = *(Neighbors - 2);
    if ( !((PointId % sz.x) == (sz.x - 1) ) )
    {
	*Neighbors += 1;
	*(Neighbors + 2) += 1;
    }
}

/* The EighteenNeighbors function finds the indices of 18-neighborhood of the point more faster than the standard routines in neighbors.cc

   the indices of Neighbors of th point S PointId are saved in Neighbors array [0,1,2,3,..,17] 
   
  The xy plane      The xz plane          The yz plane 
  8o  o3 o9         12o  o5 o13           16o  o5 o17
    \ | /              \ | /                 \ | /   
  0o--S--o1          0o--S--o1             2o--S--o3
    / | \              / | \                 / | \   
  6o  o2 o7         10o  o4 o11           14o  o4 o15
  
      On the boundary-voxels neighbors which are out of the image are mirrored 
      used the Zero Flux Neumann Boundary condition similar as in the 4,6,8 neighbourhood

*/

void EighteenNeighbors(size_t * Neighbors, size_t PointId, i3d::Vector3d<size_t> & sz)
{
    size_t sizexy = sz.x * sz.y;
    size_t size = sizexy * sz.z;
    // The 0. neighbor
    *Neighbors = PointId;
    if (!( (PointId % sz.x) == 0 ))
	*Neighbors -= 1;
    // The 1. neighbor
     NEIGHBORS
    if (!( (PointId % sz.x) == (sz.x - 1) ))
	*Neighbors = PointId + 1;
     // The 2. neighbor
    NEIGHBORS
    if (!( (PointId % sizexy) < sz.x ))
	*Neighbors -= sz.x;
     // The 3. neighbor
    NEIGHBORS
     if (!( (PointId % sizexy) >=  (sizexy - sz.x) ))
	*Neighbors += sz.x;
     // The 4. neighbor
    NEIGHBORS
    if (!(PointId < sizexy))
	*Neighbors -= sizexy;
     // The 5 neighbor
    NEIGHBORS
     if (!( (PointId + sizexy) >= size))
	*Neighbors += sizexy;

    // The 6. 8. 10. 12.  neighbor
    Neighbors++; 
    *Neighbors =  *(Neighbors - 4);
    *(Neighbors + 4) = *(Neighbors - 2);
    *(Neighbors + 2) = *(Neighbors - 3);
    *(Neighbors + 6) = *(Neighbors - 1);
    if ( !(PointId % sz.x == 0))
    {
        *Neighbors -= 1;
	*(Neighbors +4) -=1;
	*(Neighbors +2) -=1;
	*(Neighbors +6) -=1;
    }
    
    // The 7. 9. 10. 11. neighbor
    Neighbors++; 
    *Neighbors = *(Neighbors - 5);
    *(Neighbors + 4) = *(Neighbors - 3);
    *(Neighbors + 2) = *(Neighbors - 4);
    *(Neighbors + 6) = *(Neighbors - 2);
    if ( !((PointId % sz.x) == (sz.x - 1) ) )
    {
	*Neighbors += 1;
	*(Neighbors +4) +=1;
	*(Neighbors +2) +=1;
	*(Neighbors +6) +=1;
   }
    
    // The 10. and 12. neighbor
    Neighbors += 7; *Neighbors =  *(Neighbors - 10);
    *(Neighbors + 2) = *(Neighbors - 9);
    if (!( (PointId % sizexy) < sz.x ))
    {
        *Neighbors -= sz.x;
	*(Neighbors +2) -=sz.x;
    }
   
    // The 11. and 13. neighbor
    Neighbors++; *Neighbors = *(Neighbors - 11);
    *(Neighbors + 2) = *(Neighbors - 10);
    if (!( (PointId % sizexy) >=  (sizexy - sz.x) ))
    {
	*Neighbors += sz.x;
	*(Neighbors + 2) += sz.x;
    }
    
}
/*

           7
           |
           5
           |
     2--0--S--1--3
           |
	   4
           |
	   6
	   
   */

void SecondOrder2DNeighbors(size_t * Neighbors, const size_t PointId, const i3d::Vector3d<size_t> & sz)
{
    size_t size = sz.x * sz.y;
    
    *Neighbors = PointId;
    // The 0. neighbor
    if (!((PointId % sz.x) == 0 ))
	*Neighbors  -= 1;
    *(Neighbors+2) = *Neighbors;
    // The 1. neighbor
     NEIGHBORS
    if (!( (PointId % sz.x) == (sz.x - 1) ))
	*Neighbors  += 1;
     *(Neighbors+2) = *Neighbors;
     ++Neighbors;
     // The 2. neighbor
    if (! ((PointId % sz.x) < 2 ) )
	*Neighbors  -= 1;
    else 
	*Neighbors  += 1 - (PointId % sz.x);
    // The 3. neighbor
     ++Neighbors;
    if (!( (PointId % sz.x) >= (sz.x - 2)))
	*Neighbors  += 1;
    else
	*Neighbors  -= (PointId % sz.x) - (sz.x - 2);
    // The 4. neighbor
    NEIGHBORS
    if (!( PointId < sz.x ))
	*Neighbors -= sz.x;
    *(Neighbors+2) = *Neighbors;
    // The 5. neighbor
    NEIGHBORS
     if (!( (PointId + sz.x) >= size ))
	*Neighbors  += sz.x;
    *(Neighbors+2) = *Neighbors;
    // The 6. neighbor
    ++Neighbors;
    if (!( PointId < 2 * sz.x ))
	*Neighbors -=  sz.x;
    else if (( PointId < sz.x ))
	*Neighbors +=  sz.x;
    // The 7. neighbor
    ++Neighbors;
     if (!( (PointId + 2 * sz.x) >= size ))
	*Neighbors  +=  sz.x;
     else if (( (PointId + sz.x) >= size ))
	*Neighbors  -=  sz.x;

}
  /*
void SecondOrder2DNeighbors(size_t * Neighbors, size_t PointId, i3d::Vector3d<size_t> & sz)
{
    size_t size = sz.x * sz.y;
    *Neighbors = PointId;
    // The 0. neighbor
    if (!((PointId % sz.x) == 0 ))
	*Neighbors  -= 1;
    *(Neighbors+2) = *Neighbors;
    // The 1. neighbor
     NEIGHBORS
    if (!( (PointId % sz.x) == (sz.x - 1) ))
	*Neighbors  += 1;
     *(Neighbors+2) = *Neighbors;
     ++Neighbors;
     // The 2. neighbor
    if (! ((PointId % sz.x) < 2 ) )
	*Neighbors  -= 1;
    // The 3. neighbor
     ++Neighbors;
    if (!( (PointId % sz.x) >= (sz.x - 2) ))
	*Neighbors  += 1;
    // The 4. neighbor
    NEIGHBORS
    if (!( PointId < sz.x ))
	*Neighbors -= sz.x;
    *(Neighbors+2) = *Neighbors;
    // The 5. neighbor
    NEIGHBORS
     if (!( (PointId + sz.x) >= size ))
	*Neighbors  += sz.x;
    *(Neighbors+2) = *Neighbors;
    // The 6. neighbor
    ++Neighbors;
    if (!( PointId < 2 * sz.x ))
	*Neighbors -=  sz.x;
    // The 7. neighbor
    ++Neighbors;
     if (!( (PointId + 2 * sz.x) >= size ))
	*Neighbors  +=  sz.x;

}
*/

/*

           11  7
           |  /
           9 5
           |/
     2--0--S--1--3
          /|
	 4 8
        /  |
       6   10
	   
   */

void SecondOrder3DNeighbors(size_t * Neighbors, const size_t PointId, const i3d::Vector3d<size_t> & sz)
{
    const size_t sizexy = sz.x * sz.y;
    const size_t size = sizexy * sz.z;

    *Neighbors = PointId;
    // The 0. neighbor
    if (!((PointId % sz.x) == 0 ))
	*Neighbors  -= 1;
    *(Neighbors+2) = *Neighbors;
    // The 1. neighbor
     NEIGHBORS
    if (!( (PointId % sz.x) == (sz.x - 1) ))
	*Neighbors  += 1;
    *(Neighbors+2) = *Neighbors;
     ++Neighbors;
     // The 2. neighbor
    if (! ((PointId % sz.x) < 2 ) )
	*Neighbors  -= 1;
    else
	*Neighbors  += 1  - (PointId % sz.x);
    // The 3. neighbor
     ++Neighbors;
    if (!( (PointId % sz.x) >= (sz.x - 2) ))
	*Neighbors  += 1;
    else
	*Neighbors  -=(PointId % sz.x) - (sz.x - 2);
    // The 4. neighbor
    NEIGHBORS
    if (!( (PointId % sizexy) < sz.x ))
	*Neighbors -= sz.x;
    *(Neighbors+2) = *Neighbors;
     // The 5. neighbor
    NEIGHBORS
     if (!( (PointId % sizexy) >=  (sizexy - sz.x) ))
	*Neighbors += sz.x;
    *(Neighbors+2) = *Neighbors;
    // The 6. neighbor
    ++Neighbors;
    if (!( (PointId % sizexy) < 2*sz.x ))
	*Neighbors -= sz.x;
    else if (( PointId % sizexy < sz.x ))
	*Neighbors += sz.x;
     // The 7. neighbor
    ++Neighbors;
     if (!( (PointId % sizexy) >=  (sizexy - 2*sz.x) ))
	*Neighbors += sz.x;
     else  if (( (PointId % sizexy) >=  (sizexy - sz.x) ))
	*Neighbors -= sz.x;

     // The 8. neighbor
    NEIGHBORS
    if (!(PointId < sizexy))
	*Neighbors -= sizexy;
    *(Neighbors+2) = *Neighbors;
     // The 9. neighbor
    NEIGHBORS
     if (!( (PointId + sizexy) >= size))
	*Neighbors += sizexy;
    *(Neighbors+2) = *Neighbors;
    
    // The 10. neighbor
    ++Neighbors;
    if (!(PointId < 2*sizexy))
	*Neighbors -= sizexy;
    else  if ((PointId < sizexy))
	*Neighbors += sizexy ;
     // The 11. neighbor
    ++Neighbors;
     if (!( (PointId + 2*sizexy) >= size))
	*Neighbors += sizexy;
     else if (( (PointId + sizexy) >= size))
	*Neighbors -= sizexy;

}

/*
void SecondOrder3DNeighbors(size_t * Neighbors, size_t PointId, i3d::Vector3d<size_t> & sz)
{
    size_t sizexy = sz.x * sz.y;
    size_t size = sizexy * sz.z;

    *Neighbors = PointId;
    // The 0. neighbor
    if (!((PointId % sz.x) == 0 ))
	*Neighbors  -= 1;
    *(Neighbors+2) = *Neighbors;
    // The 1. neighbor
     NEIGHBORS
    if (!( (PointId % sz.x) == (sz.x - 1) ))
	*Neighbors  += 1;
    *(Neighbors+2) = *Neighbors;
     ++Neighbors;
     // The 2. neighbor
    if (! ((PointId % sz.x) < 2 ) )
	*Neighbors  -= 1;
    // The 3. neighbor
     ++Neighbors;
    if (!( (PointId % sz.x) >= (sz.x - 2) ))
	*Neighbors  += 1;
    // The 4. neighbor
    NEIGHBORS
    if (!( (PointId % sizexy) < sz.x ))
	*Neighbors -= sz.x;
    *(Neighbors+2) = *Neighbors;
     // The 5. neighbor
    NEIGHBORS
     if (!( (PointId % sizexy) >=  (sizexy - sz.x) ))
	*Neighbors += sz.x;
    *(Neighbors+2) = *Neighbors;
    // The 6. neighbor
    ++Neighbors;
    if (!( (PointId % sizexy) < 2*sz.x ))
	*Neighbors -= sz.x;
     // The 7. neighbor
    ++Neighbors;
     if (!( (PointId % sizexy) >=  (sizexy - 2*sz.x) ))
	*Neighbors += sz.x;
     // The 8. neighbor
    NEIGHBORS
    if (!(PointId < sizexy))
	*Neighbors -= sizexy;
    *(Neighbors+2) = *Neighbors;
     // The 9. neighbor
    NEIGHBORS
     if (!( (PointId + sizexy) >= size))
	*Neighbors += sizexy;
    *(Neighbors+2) = *Neighbors;
    // The 10. neighbor
    ++Neighbors;
    if (!(PointId < 2*sizexy))
	*Neighbors -= sizexy;
     // The 11. neighbor
    ++Neighbors;
     if (!( (PointId + 2*sizexy) >= size))
	*Neighbors += sizexy;

}


*/



class LSNeighbourhood
{
    public:
	LSNeighbourhood(i3d::Vector3d<size_t> s){size.x = s.x;size.y = s.y; size.z = s.z;
	};
	virtual ~LSNeighbourhood (){};
    virtual void Neighbors (size_t * neigh, size_t pointId, i3d::Vector3d<size_t> & s) const {};
    virtual size_t GetSize() const { return 0;};
    virtual void GetIncrements(size_t * data) const {};
    virtual void GetProportions (size_t * data) const {};
    protected:
    i3d::Vector3d<size_t> size;
};

class FourLSNeighbourhood : public LSNeighbourhood
{
    public:
	FourLSNeighbourhood(i3d::Vector3d<size_t> s):LSNeighbourhood(s){};
	~FourLSNeighbourhood(){};
  void Neighbors (size_t * neigh, size_t pointId, i3d::Vector3d<size_t> & s) const {FourNeighbors(neigh, pointId, s);};
  size_t GetSize() const {return 4;};
  void GetIncrements(size_t * data) const {data[0] = 1; data[1] = 2;};
  void GetProportions (size_t * data) const {data[0] = 1; data[1] = 1;};

};


class SixLSNeighbourhood : public LSNeighbourhood
{
    public:

	SixLSNeighbourhood(i3d::Vector3d<size_t> s):LSNeighbourhood(s){ zIncrement = 2 * size.x;};
        ~SixLSNeighbourhood(){};
  void Neighbors (size_t * neigh, size_t pointId, i3d::Vector3d<size_t> & s) const {SixNeighbors(neigh, pointId, s);};
  size_t GetSize() const {return 6;};
  void GetIncrements(size_t * data) const {data[0] = 1; data[1] = 2; data[2] = zIncrement;};
  void GetProportions (size_t * data) const {data[0] = 1; data[1] = 1; data[2] = 1;};
    protected: 
  size_t zIncrement;

};

class EightLSNeighbourhood : public LSNeighbourhood
{
    public:
	EightLSNeighbourhood(i3d::Vector3d<size_t> s):LSNeighbourhood(s){};
	~EightLSNeighbourhood(){};
  void Neighbors (size_t * neigh, size_t pointId, i3d::Vector3d<size_t> & s) const {EightNeighbors(neigh, pointId, s);};
  size_t GetSize() const {return 8;};
  void GetIncrements(size_t * data) const {data[0] = 1; data[1] = 2;};
  void GetProportions (size_t * data) const {data[0] = 1; data[1] = 1;};

};

class EighteenLSNeighbourhood : public LSNeighbourhood
{
    public:

	EighteenLSNeighbourhood(i3d::Vector3d<size_t> s):LSNeighbourhood(s){ zIncrement = 2 * size.x;};
        ~EighteenLSNeighbourhood(){};
  void Neighbors (size_t * neigh, size_t pointId, i3d::Vector3d<size_t> & s) const {EighteenNeighbors(neigh, pointId, s);};
  size_t GetSize() const {return 18;};
  void GetIncrements(size_t * data) const {data[0] = 1; data[1] = 2; data[2] = zIncrement;};
  void GetProportions (size_t * data) const {data[0] = 1; data[1] = 1; data[2] = 1;};
    protected: 
  size_t zIncrement;

};


class SecondOrder2DLSNeighbourhood : public LSNeighbourhood
{
    public:
	SecondOrder2DLSNeighbourhood(i3d::Vector3d<size_t> s):LSNeighbourhood(s){};
	~SecondOrder2DLSNeighbourhood(){};
  void Neighbors (size_t * neigh, size_t pointId, i3d::Vector3d<size_t> & s) const {SecondOrder2DNeighbors(neigh, pointId, s);};
  size_t GetSize() const {return 8;};
  void GetIncrements(size_t * data) const {data[0] = 1; data[1] = 4;};
  void GetProportions (size_t * data) const {data[0] = 2; data[1] = 2;};

};


class SecondOrder3DLSNeighbourhood : public LSNeighbourhood
{
    public:

	SecondOrder3DLSNeighbourhood(i3d::Vector3d<size_t> s):LSNeighbourhood(s){ zIncrement = 4 * size.x;};
        ~SecondOrder3DLSNeighbourhood(){};
  void Neighbors (size_t * neigh, size_t pointId, i3d::Vector3d<size_t> & s) const {SecondOrder3DNeighbors(neigh, pointId, s);};
  size_t GetSize() const {return 12;};
  void GetIncrements(size_t * data) const {data[0] = 1; data[1] = 4; data[2] = zIncrement;};
  void GetProportions (size_t * data) const {data[0] = 2; data[1] = 2; data[2] = 2;};
    protected: 
  size_t zIncrement;
};




/** Compute the finite difference aproximation for Fast Marching method with one known point*/
/*    A-----S
  ArrivalA  Speed
  SpacingA
  */
template <class VOXEL> VOXEL OnePointCase (const VOXEL ArrivalA,const VOXEL SpacingA, const VOXEL Speed)
{
    return ArrivalA + SpacingA/Speed;
}

/** Compute the finite difference aproximation for Fast Marching method with two known points*/
/*
            ArrivalC
	    SpacingC
            C
            |
            |
      A-----S
  ArrivalA  Speed
  SpacingA
  */
template <class VOXEL> VOXEL TwoPointCase (const VOXEL ArrivalA, const VOXEL ArrivalC, const VOXEL SpacingA,const  VOXEL SpacingC, const VOXEL Speed)
{
    return (((SQR(SpacingA)*ArrivalC + SQR(SpacingC)*ArrivalA) + 
     SpacingA*SpacingC*sqrt( (-1.0*(SQR(ArrivalA-ArrivalC))) + ((SQR(SpacingA)+SQR(SpacingC)) / SQR(Speed)))) / (SQR(SpacingA) + SQR(SpacingC)));
}

/** Compute the finite difference aproximation for Fast Marching method with three known points -- 3D situation*/
/*
            ArrivalC
	    SpacingAC
            C  
            | E ArrivalE
            |/  SpacingE
      A-----S
  ArrivalA  Speed
  SpacingAC

  Points A and C are in xy plane.
  */
template <class VOXEL> VOXEL ThreePointCase (const VOXEL ArrivalA, const VOXEL ArrivalC, const VOXEL ArrivalE, const VOXEL SpacingAC,const  VOXEL SpacingE, const VOXEL Speed)
{
    VOXEL a, b, c, disc;
    a = 2.0 * SQR(SpacingE) + SQR(SpacingAC);
    b = -2.0 * (SQR(SpacingE)*(ArrivalA+ArrivalC) + (SQR(SpacingAC)*ArrivalE));
    c = SQR(SpacingE)*(SQR(ArrivalA)+SQR(ArrivalC)) + SQR(SpacingAC)*SQR(ArrivalE) - ((SQR(SpacingAC)*SQR(SpacingE))/SQR(Speed));
    disc = sqrt(SQR(b) - 4*a*c);
    return ((-b+disc) / (2.0*a));
}





template <class VOXEL> void ShiftImage (i3d::Image3d<VOXEL> & img, VOXEL value)
{
    VOXEL * tmp, * last;
    tmp = img.GetVoxelAddr(0);
    last = img.GetVoxelAddr(img.GetImageSize()-1);
    while (tmp != last)
    {
	*tmp  -= value;
	tmp++;
    }
}


template <class VOXEL> I3D_DLLEXPORT VOXEL TukeyNorm (VOXEL x, VOXEL K)
{
    if (fabsf(x) <= K*sqrt(5.0))
	return (SQR(1.0 - SQR(x/(K*sqrt(5.0)))));
//    if (fabsf(x) <= K/sqrt(2.0))
//	return SQR(1.0 - SQR(x/(K/sqrt(2.0))));

    else
	return 0.0;
}

template <class VOXEL> I3D_DLLEXPORT VOXEL OriginalPMNorm (VOXEL x, VOXEL K)
{
	return (1.0 / (1.0 + SQR(x/K)));
}

template <class VOXEL> I3D_DLLEXPORT VOXEL AlternativePMNorm (VOXEL x, VOXEL K)
{
	return (exp(-(SQR(x/K))));
}



template <class VOXEL> inline  void MultiplyRow ( valarray<VOXEL> & d, valarray<VOXEL> & s, slice  sl)
{
    slice_array<VOXEL>  dv = d[sl];
    dv = s;
}



template <class VOXEL> inline  void UpdateRow ( valarray<VOXEL> & d, valarray<VOXEL> & s, slice  sl)
{
    slice_array<VOXEL>  dv = d[sl];
    dv += s;
}

template <class VOXEL> float Moment(i3d::Image3d<VOXEL>& Image, float AverageGrey)
{
   double sum;
  VOXEL * pom, * last;
  pom = Image.GetVoxelAddr(0);
  last = Image.GetVoxelAddr(Image.GetImageSize());

  sum = 0.0;
  while (pom != last)
  {
      sum +=   SQR(*pom - AverageGrey);
      ++pom;
  }
  return (sum / static_cast<float>(Image.GetImageSize()));
   
}

typedef float (* ptFunc) (float, float *);

I3D_DLLEXPORT float RegulaFalsi(float x0, float x1, ptFunc function,float * Par, size_t iter = 15, float epsilon = 0.001)
{
    size_t i = 1,j;
    float  error = std::numeric_limits<float>::max();
    size_t xs =0;
    std::vector<float> x(iter);
    std::vector<float> val(iter);
    x[0] = x0;
    x[1] = x1;
    val[0] = function(x0,Par);
    val[1] = function(x1,Par);

    while ((i < iter) && (error > epsilon))
    {
        for (j = 0; j < i; ++j)
        {
            if (val[i] * val[j] < 0 )
                xs = j;
        }
        x[i+1] =  x[i] -val[i]*((x[i] -x[xs])/(val[i]-val[xs]));
        i++;
        val[i] = (function(x[i],Par));
        error = abs(val[i]);
    }
    return x[i];
}

I3D_DLLEXPORT float pmfce (float x, float * m)
{
     return (1 - exp(-x) - (*m) * exp (-x) * x);
}

I3D_DLLEXPORT float pmfce1 (float x, float * m)
{
     return ( exp(-x) - 2.0*(*m) * exp (-x) + 2.0*x * (*m) * exp (-x));
}

template <class FLOAT> int ThomasAlg(valarray<FLOAT> & SDiag, valarray<FLOAT> & Diag, valarray<FLOAT> &  r, valarray<FLOAT> & u, unsigned long n)
{
    unsigned long j;
    FLOAT bet;
    valarray<FLOAT> gam;
    gam.resize(n);
    if (Diag[1] == 0.0) 
    {
	cerr << "Error 1 in ThomasAlg\n";
	return -1;
    }
/*If this happens then you should rewrite your equations as a set of order N ??? 1, w ith u2
trivially eliminated.*/
    u[1] = r[1] / (bet=Diag[1]);

    for (j=2; j<=n; ++j) // Decomposition and forward substitution.
    {
	gam[j]=SDiag[j-1]/bet;
	bet=Diag[j]-SDiag[j]*gam[j];
	if (bet == 0.0) 
	{
	    cerr << "Error 2 in ThomasAlg\n"; //Algorithm fails; see below
	    return -2;
	}
	u[j]=(r[j]-SDiag[j]*u[j-1])/bet;
   }

    for (j=(n-1);j>=1;j--)
	u[j] -= gam[j+1]*u[j+1]; //Backsubstitution.
    return 0 ;
}

/*
 *
 *
 * Fast Marching supplemental functions End
 *
 *
 */

namespace i3d 
{

template <class VOXEL> i3d::Vector3d<float> ComputeNormedSpacing (i3d::Image3d<VOXEL> & img)
{
    i3d::Vector3d<float> res;
    res = img.GetResolution().GetRes();
    float max;
    if (img.GetSizeZ() > 1)
    {
	max = MAX(MAX(res.x,res.y),res.z);
    }
    else
    {
	max = MAX(res.x,res.y);
    }
    res = max / res;
    return res;
}


/*
 *
 *
 *  Speed Functions Implementation
 *
 *
 */
   /* this computes the standard Speed function as written in the LevelSet.h file */
template <class VOXELIN, class VOXELOUT> void SpeedFunctionStandard (Image3d<VOXELIN>& ImgIn, Image3d<VOXELOUT> & SpeedImg,double sigmax, double sigmay, double sigmaz, double width)
{

    VOXELOUT * max, * i; 
    i3d::Image3d<VOXELOUT> ImgPom;
    SpeedImg.SetOffset(ImgIn.GetOffset());
    SpeedImg.SetResolution(ImgIn.GetResolution());
    SpeedImg.MakeRoom(ImgIn.GetSize());

    GrayToFloat(ImgIn,ImgPom);
    Gauss(ImgPom,sigmax,sigmay,sigmaz,width);
   
    if ( SpeedImg.GetSizeZ() == 1)
	EstimateGradient2_2D(ImgPom,SpeedImg,true);
    else
	EstimateGradient2_3D(ImgPom,SpeedImg,true);
	
    max = SpeedImg.GetVoxelAddr(SpeedImg.GetImageSize()-1);
    i = SpeedImg.GetVoxelAddr(0);
    float mn,mx;
    mx = std::numeric_limits<float>::min();
    mn = std::numeric_limits<float>::max();
    while (i != max)
    {
/*	if ((*i) > mx)
	    mx = (*i);
	if ((*i) < mn)
	    mn = (*i);*/
	*i = 1 / (1+(*i));
	i++;
    }
    *i = 1 / (1+(*i));
//  cout << "Gradient Values min: " << mn << " max: " << mx << endl;
}


   /* this computes the exponential Speed function as written in the LevelSet.h file */
template <class VOXELIN, class VOXELOUT> void SpeedFunctionExponential (Image3d<VOXELIN>& ImgIn,Image3d<VOXELOUT> & SpeedImg,double sigmax , double sigmay, double sigmaz, double width,double alpha)
{
   VOXELOUT * max, * i; 
    i3d::Image3d<VOXELOUT> ImgPom;
//    i3d::Image3d<VOXELIN> C;
    SpeedImg.SetOffset(ImgIn.GetOffset());
    SpeedImg.SetResolution(ImgIn.GetResolution());
    SpeedImg.MakeRoom(ImgIn.GetSize());
  /*  C.SetOffset(ImgIn.GetOffset());
    C.SetResolution(ImgIn.GetResolution());
    C.MakeRoom(ImgIn.GetSize());
    */

    GrayToFloat(ImgIn,ImgPom);
    Gauss(ImgPom,sigmax,sigmay,sigmaz,width);
   
    if ( SpeedImg.GetSizeZ() == 1)
	EstimateGradient2_2D(ImgPom,SpeedImg,true);
    else
	EstimateGradient2_3D(ImgPom,SpeedImg,true);
	
    max = SpeedImg.GetVoxelAddr(SpeedImg.GetImageSize()-1);
    i = SpeedImg.GetVoxelAddr(0);
 /*
    if ( SpeedImg.GetSizeZ() == 1)
	Canny2D(ImgIn,C,2.0,10.0,AXIS_Z);
    else
	Canny3D(ImgIn,C,2.0,10.0);
     GrayToFloat(C,SpeedImg);
     //Gauss(SpeedImg,sigmax,sigmay,sigmaz,widthFactor);
    max = SpeedImg.GetVoxelAddr(SpeedImg.GetImageSize()-1);
    i = SpeedImg.GetVoxelAddr(0);
*/

	
    while (i != max)
    {
	*i = powf(2.7182818284590452354,(-1)*alpha*(*i));
	if (*i < 1e-9)
	    *i = 1e-9;
	i++;
    }
    *i = powf(2.7182818284590452354,(-1)*alpha*(*i));
    if (*i < 1e-9)
	*i = 1e-9;
}


/*
 *
 *
 * Speed Function Implementation End
 *
 *
 */


/*
 *
 *
 * Fast Marching Code Begin
 *
 *
 */

/* This functions update the Arrival Time of the wave to the point  PointId in the 2D Fast Marching Method */
template <class VOXEL, class SF> void UpdatePoint ( std::valarray<VOXEL>& ArrivalTimes, VOXEL Speed, Vector3d<float> & res, Heap<VOXEL, SF> & heap, std::valarray<unsigned char> & Flags, size_t PointId, size_t * Neighbors)
{
   VOXEL xval = 0, yval = 0, new_value = 0;
  size_t * pom = Neighbors;
  unsigned char Fl, Flxyz;
  Fl = 0;
  Flxyz = 0;
  // Save in the bitmap which neighbors of the point are known
   if (Flags[*pom] == KNOWN) //0 neighbor is known
       Fl |= 1;
   pom++;
   if (Flags[*pom] == KNOWN)
       Fl |= 2;  // 1 neighbor is known
   pom++;
   if (Flags[*pom] == KNOWN)
       Fl |= 4; // 2 neighbor is known
   pom++;
   if (Flags[*pom] == KNOWN)
       Fl |= 8; // 3 neighbor is known

   // Find the neighbor on the x-axe which is known and has smaller value of arrival time than his oposite
   switch (Fl & 3) 
   {
       case 3: // both x neighbors are known we must choose one
	   //if (ArrivalTimes[Neighbors[0]] < ArrivalTimes[Neighbors[1]])
	   if (heap.compare(ArrivalTimes[Neighbors[1]] , ArrivalTimes[Neighbors[0]]))
	       xval = heap.compare(ArrivalTimes[Neighbors[0]]);
	   else
	       xval = heap.compare(ArrivalTimes[Neighbors[1]]);
	   Flxyz |= 1; // Set that we have x point known
	   break;
       case 1:  // only one is known
	   xval = heap.compare(ArrivalTimes[Neighbors[0]]);
	   Flxyz |= 1;
	   break;
       case 2: // only one is known
	   xval = heap.compare(ArrivalTimes[Neighbors[1]]);
	   Flxyz |= 1;
	   break;
       default:
	   break;
   }

   // Find the neighbor on the y-axe which is known and has smaller value of arrival time than his oposite, the cases are similar to x case
   switch (Fl & 12)
   {
       case 12:
	   //if (ArrivalTimes[Neighbors[2]] < ArrivalTimes[Neighbors[3]])
	   if (heap.compare(ArrivalTimes[Neighbors[3]] , ArrivalTimes[Neighbors[2]]))
	       yval = heap.compare(ArrivalTimes[Neighbors[2]]);
	   else
	       yval = heap.compare(ArrivalTimes[Neighbors[3]]);
	   Flxyz |= 2;
	   break;
       case 4: 
	   yval = heap.compare(ArrivalTimes[Neighbors[2]]);
	   Flxyz |= 2;
	   break;
       case 8:
	   yval = heap.compare(ArrivalTimes[Neighbors[3]]);
	   Flxyz |= 2;
	   break;
       default:
	   break;
   }
   // Compute the finite diference aproximation of the arrival time from one or twooints known.
   switch (Flxyz)
   {
       case 1:
	new_value =  OnePointCase (xval, (VOXEL) res.x, Speed);   
	   break;
       case 2:
	new_value =  OnePointCase (yval, (VOXEL) res.y, Speed);   
	   break;
       case 3:
	new_value =  TwoPointCase (xval, yval, (VOXEL) res.x, (VOXEL) res.y, Speed);   
	   break;
       default:
	   break;
   }

// Save the value of arrival time on the heap in case that it is lower than the old value
   if (new_value < heap.compare(ArrivalTimes[PointId]))
   {
       if (ArrivalTimes[PointId] < 0)
	   new_value *= -1;
       ArrivalTimes[PointId] = new_value;
       if (Flags[PointId] == FARP)
       {
	   Flags[PointId] = TRIAL;
	   heap.Push(PointId);
       }
       else
       {
	   heap.BalanceTrialUp(PointId);
       }
   }
}


/* This functions update the Arrival Time of The point  PointId in the 3D Fast Marching Method */
template <class VOXEL, class SF> void UpdatePoint3D ( std::valarray<VOXEL>& ArrivalTimes, VOXEL Speed, Vector3d<float> & res, Heap<VOXEL, SF> & heap, std::valarray<unsigned char> & Flags, size_t PointId, size_t * Neighbors)
{
   VOXEL xval = 0, yval = 0,zval = 0, new_value = 0;
  size_t * pom;
  unsigned char Fl, Flxyz;
  pom = Neighbors;
  Fl = 0;
  Flxyz = 0;
  // Save in the bitmap which neighbors of the point are known
   if (Flags[*pom] == KNOWN)
       Fl |= 1;
   pom++;
   if (Flags[*pom] == KNOWN)
       Fl |= 2;
   pom++;
   if (Flags[*pom] == KNOWN)
       Fl |= 4;
   pom++;
   if (Flags[*pom] == KNOWN)
       Fl |= 8;
   pom++;
   if (Flags[*pom] == KNOWN)
       Fl |= 16;
   pom++;
   if (Flags[*pom] == KNOWN)
       Fl |= 32;


   // Find the neighbor on the x-axe which is known and has smaller value of arrival time than his oposite
   switch (Fl & 3)
   {
       case 3:
	 //  if (ArrivalTimes[Neighbors[0]] < ArrivalTimes[Neighbors[1]])
	   if (heap.compare(ArrivalTimes[Neighbors[1]] , ArrivalTimes[Neighbors[0]]))
	       xval = heap.compare(ArrivalTimes[Neighbors[0]]);
	   else
	       xval = heap.compare(ArrivalTimes[Neighbors[1]]);
	   Flxyz |= 1;
	   break;
       case 1: 
	   xval = heap.compare(ArrivalTimes[Neighbors[0]]);
	   Flxyz |= 1;
	   break;
       case 2:
	   xval = heap.compare(ArrivalTimes[Neighbors[1]]);
	   Flxyz |= 1;
	   break;
       default:
	   break;
   }

   // Find the neighbor on the y-axe which is known and has smaller value of arrival time than his oposite
   switch (Fl & 12)
   {
       case 12:
	   //if (ArrivalTimes[Neighbors[2]] < ArrivalTimes[Neighbors[3]])
	   if (heap.compare(ArrivalTimes[Neighbors[3]] , ArrivalTimes[Neighbors[2]]))
	       yval = heap.compare(ArrivalTimes[Neighbors[2]]);
	   else
	       yval = heap.compare(ArrivalTimes[Neighbors[3]]);
	   Flxyz |= 2;
	   break;
       case 4: 
	   yval = heap.compare(ArrivalTimes[Neighbors[2]]);
	   Flxyz |= 2;
	   break;
       case 8:
	   yval = heap.compare(ArrivalTimes[Neighbors[3]]);
	   Flxyz |= 2;
	   break;
       default:
	   break;
   }

   // Find the neighbor on the z-axe which is known and has smaller value of arrival time than his oposite
   switch (Fl & 48)
   {
       case 48:
	   //if (ArrivalTimes[Neighbors[4]] < ArrivalTimes[Neighbors[5]])
	   if (heap.compare(ArrivalTimes[Neighbors[5]] , ArrivalTimes[Neighbors[4]]))
	       zval = heap.compare(ArrivalTimes[Neighbors[4]]);
	   else
	       zval = heap.compare(ArrivalTimes[Neighbors[5]]);
	   Flxyz |= 4;
	   break;
       case 16: 
	   zval = heap.compare(ArrivalTimes[Neighbors[4]]);
	   Flxyz |= 4;
	   break;
       case 32:
	   zval = heap.compare(ArrivalTimes[Neighbors[5]]);
	   Flxyz |= 4;
	   break;
       default:
	   break;
   }
 
   // Compute the finite diference aproximation of the arrival time from one or two or three points known.
   switch (Flxyz)
   {
       case 1:
	new_value =  OnePointCase (xval, (VOXEL) res.x, Speed);   
	   break;
       case 2:
	new_value =  OnePointCase (yval, (VOXEL) res.y, Speed);   
	   break;
       case 3:
	new_value =  TwoPointCase (xval, yval, (VOXEL) res.x, (VOXEL) res.y, Speed);   
	   break;
       case 4:
	new_value =  OnePointCase (zval, (VOXEL) res.z, Speed);   
	   break;  
       case 5:
	new_value =  TwoPointCase (xval, zval, (VOXEL) res.x, (VOXEL) res.z, Speed);   
	   break;
       case 6:
	new_value =  TwoPointCase (yval, zval, (VOXEL) res.y, (VOXEL) res.z, Speed);   
	   break;
       case 7:
	new_value =  ThreePointCase (xval, yval,zval, (VOXEL) res.x, (VOXEL) res.z, Speed);   
	   break;
 
 
       default:
	   break;
   }


// Save the value of arrival time on the heap in case that it is lower than the old value
   if (new_value < heap.compare(ArrivalTimes[PointId]))
   {
       if (ArrivalTimes[PointId] < 0)
       {
	   new_value *= -1;
       }
       ArrivalTimes[PointId] = new_value;
       if (Flags[PointId] == FARP)
       {
	   Flags[PointId] = TRIAL;
	   heap.Push(PointId);
       }
       else
       {
	   heap.BalanceTrialUp(PointId);
       }
   }
}



 /* following two classes are used only for better templating in the FastMarchingMethod function :-)*/
template <class VOXEL, class SF> class FMRoutines2D
{
    public:
	size_t max;
	FMRoutines2D(){max=4;};
	~FMRoutines2D(){};
	void operator()(size_t * neighbors, size_t PointId, i3d::Vector3d<size_t> & sz){ FourNeighbors(neighbors,PointId,sz);} ;
	void operator() ( std::valarray<VOXEL>& ArrivalTimes, VOXEL Speed, Vector3d<float> & res, Heap<VOXEL, SF> & heap, std::valarray<unsigned char> & Flags, size_t PointId, size_t * Neighbors) {UpdatePoint( ArrivalTimes, Speed,res, heap,Flags,PointId,Neighbors);};
};

template <class VOXEL, class SF> class FMRoutines3D
{
    public:
	size_t max;
	FMRoutines3D(){max = 6;};
	~FMRoutines3D(){};
	void operator()(size_t * neighbors, size_t PointId,  i3d::Vector3d<size_t> & sz){ SixNeighbors(neighbors,PointId,sz);} ;
	void operator() ( std::valarray<VOXEL>& ArrivalTimes, VOXEL Speed, Vector3d<float> & res, Heap<VOXEL, SF> & heap, std::valarray<unsigned char> & Flags, size_t PointId, size_t * Neighbors) {UpdatePoint3D( ArrivalTimes, Speed,res, heap,Flags,PointId,Neighbors);};
};




/* Decide if we should make 2D or 3D fast Marching method and select proper FMroutines class and call the FastMarchingMethodComp function */
    template <class VOXELIN, class VOXELOUT> void FastMarchingMethod (Image3d<VOXELIN> & SpeedImg, Image3d<VOXELOUT> & ImgOut, SeedPointsVector & SeedPoints)
{
    FMRoutines2D<VOXELOUT,sgngreater> routines2D;
    FMRoutines3D<VOXELOUT,sgngreater> routines3D;
    if (SpeedImg.GetSizeZ() > 1)
	FastMarchingMethodComp(SpeedImg, ImgOut,SeedPoints,routines3D);
    else
	FastMarchingMethodComp(SpeedImg, ImgOut,SeedPoints,routines2D);
}




/* Main Fast Marching Nethod Routine, it computes 2D or 3D fast marching method (parameter func), the other parameters are same as in the FastMarching Method routine. This parameters are better described in the LevelSet.h file*/
    template <class VOXELIN, class VOXELOUT, class FMROUTINES> void FastMarchingMethodComp (Image3d<VOXELIN> & SpeedImg, Image3d<VOXELOUT> & ImgOut,  SeedPointsVector & SeedPoints, FMROUTINES func)
{
#ifdef I3D_DEBUG
    cout << "Making Room for Output Image\n";
#endif
    ImgOut.SetOffset(SpeedImg.GetOffset());
    ImgOut.SetResolution(SpeedImg.GetResolution());
    ImgOut.MakeRoom(SpeedImg.GetSize());
    std::valarray<VOXELOUT> & ArrivalTimes = ImgOut.GetVoxelData();
    Heap<VOXELOUT,sgngreater> heap(ArrivalTimes);
    std::valarray<unsigned char> Flags( (unsigned char) FARP ,ImgOut.GetVoxelData().size());
    ArrivalTimes = INFTIME(VOXELOUT);
#ifdef I3D_DEBUG
    cout << "Inserting Seed Points\n";
#endif
    size_t index;
    for (SeedPointsVector::iterator iter =  SeedPoints.begin(); iter  < SeedPoints.end(); iter++)
    {
//#ifdef I3D_DEBUG
//	cout << (*iter) << endl;
//#endif
	index = GetIndex((*iter),ImgOut.GetSize());
	ArrivalTimes[index] = 0.0;
	Flags[index] = KNOWN;
	heap.Push(index);
    }
 
    
    std::valarray<VOXELOUT> & SpeedArray = SpeedImg.GetVoxelData();
   
    size_t PointId;
    size_t neighbors[6];
    size_t neighbors2[6],i;
    Vector3d<float> res = ImgOut.GetResolution().GetRes();
    res.x = 1/res.x; res.y = 1/res.y; res.z = 1/res.z;
    i3d::Vector3d<size_t> sz; 
    sz.x = ImgOut.GetSizeX();
    sz.y = ImgOut.GetSizeY();
    sz.z = ImgOut.GetSizeZ();
    
//    cout << "Computing Fast Marching Method\n" << "spacing.x: " << res.x << " spacing.y: " << res.y << " spacing.z: " << res.z << endl;
#ifdef TIME_MEASUREMENT
clock_t  t,t1;
	t = clock();
#endif
	PointId = heap.Pop();
    while (PointId  != UNDEFINED)
    {
//	cout << "Inserting point " << ImgOut.GetCoords(PointId) << " on the heap.\n" ;
	Flags[PointId] = KNOWN;
	func(neighbors, PointId, sz);
	for(i=0; i<func.max; ++i)
            if (Flags[neighbors[i]] != KNOWN )
	    {
		func(neighbors2, neighbors[i], sz);
		func(ArrivalTimes, SpeedArray[neighbors[i]],res, heap, Flags, neighbors[i], neighbors2);
	    }
	PointId = heap.Pop();
    }
#ifdef TIME_MEASUREMENT
  t1 = clock();
  cout << "Execution took appr. " << ((float)(t1 -t)) / ((float)CLOCKS_PER_SEC) << " seconds\n" ;
#endif

}


template <class VOXELIN, class VOXELOUT> void GeodesicDistance (Image3d<VOXELIN> & ImgIn, Image3d<VOXELOUT> & ImgOut, SeedPointsVector & SeedPoints)
{
    FMRoutines2D<VOXELOUT,sgngreater> routines2D;
    FMRoutines3D<VOXELOUT,sgngreater> routines3D;
   
    Image3d<VOXELIN> * ImgTmp = new i3d::Image3d<VOXELIN>();
    HASH_SET<size_t> boundary;
    VOXELIN * ImgInP, * ImgTmpP, * LastP, * FirstP;

    /* Memory allocation for temporary Image */
    ImgTmp->SetOffset(ImgIn.GetOffset());
    ImgTmp->SetResolution(ImgIn.GetResolution());
    ImgTmp->MakeRoom(ImgIn.GetSize());
    /*Memory allocation fo output image */
 #ifdef I3D_DEBUG
    cout << "Performing Dilation\n";
#endif
    if (ImgIn.GetSizeZ() > 1)
	Dilation(ImgIn, *ImgTmp, nb3D_6);
    else
	Dilation(ImgIn,*ImgTmp, nb2D_4);
    
    ImgInP = ImgIn.GetVoxelAddr(0);
    ImgTmpP = ImgTmp->GetVoxelAddr(0);
    LastP = ImgTmp->GetVoxelAddr(ImgTmp->GetImageSize());
    FirstP = ImgInP;
    while (ImgTmpP != LastP)
    {
	if ((*ImgTmpP) != (*ImgInP))
	{
	    boundary.insert((size_t)(ImgInP - FirstP));
//	    cout << "Hranicni pixel: " << ImgInP - FirstP << endl;
	}
	++ImgInP;
	++ImgTmpP;
    }
    delete ImgTmp;
 #ifdef I3D_DEBUG
    cout << "Making Room for Output Image\n";
#endif
    ImgOut.SetOffset(ImgIn.GetOffset());
    ImgOut.SetResolution(ImgIn.GetResolution());
    ImgOut.MakeRoom(ImgIn.GetSize());

    if (ImgIn.GetSizeZ() > 1)
	GeodesicDistanceComp( ImgOut,SeedPoints,boundary,routines3D);
    else
	GeodesicDistanceComp( ImgOut,SeedPoints,boundary,routines2D);
    
}



/* Main Fast Marching Nethod Routine, it computes 2D or 3D fast marching method (parameter func), the other parameters are same as in the FastMarching Method routine. This parameters are better described in the LevelSet.h file*/
template <class VOXELOUT, class FMROUTINES> void GeodesicDistanceComp (Image3d<VOXELOUT> & ImgOut,  SeedPointsVector & SeedPoints, HASH_SET<size_t> & boundary, FMROUTINES func)
{
    HASH_SET<size_t>::iterator boundaryEnd = boundary.end();
    std::valarray<VOXELOUT> & ArrivalTimes = ImgOut.GetVoxelData();
    Heap<VOXELOUT,sgngreater> heap(ArrivalTimes);
    std::valarray<unsigned char> Flags( (unsigned char) FARP ,ImgOut.GetVoxelData().size());
    ArrivalTimes = INFTIME(VOXELOUT);
#ifdef I3D_DEBUG
    cout << "Inserting Seed Points\n";
#endif
    size_t index;
    for (SeedPointsVector::iterator iter =  SeedPoints.begin(); iter  < SeedPoints.end(); iter++)
    {
//#ifdef I3D_DEBUG
//	cout << (*iter) << endl;
//#endif
	index = GetIndex((*iter),ImgOut.GetSize());
	ArrivalTimes[index] = 0.0;
	Flags[index] = KNOWN;
	heap.Push(index);
    }
    size_t PointId;
    size_t neighbors[6];
    size_t neighbors2[6],i;
    Vector3d<float> res = ImgOut.GetResolution().GetRes();
    res.x = 1/res.x; res.y = 1/res.y; res.z = 1/res.z;
//    cout << res << endl;
    i3d::Vector3d<size_t> sz; 
    sz.x = ImgOut.GetSizeX();
    sz.y = ImgOut.GetSizeY();
    sz.z = ImgOut.GetSizeZ();
    PointId = heap.Pop();
    while (PointId  != UNDEFINED)
    {
	Flags[PointId] = KNOWN;
	func(neighbors, PointId, sz);
	for(i=0; i<func.max; ++i)
            if (Flags[neighbors[i]] != KNOWN )
	    {
		if ( boundary.find(neighbors[i]) == boundaryEnd)
		{
		    func(neighbors2, neighbors[i], sz);
		    func(ArrivalTimes, static_cast<VOXELOUT>(1.0),res, heap, Flags, neighbors[i], neighbors2);
		}
	    }
	PointId = heap.Pop();
    }
}
template < class VOXELOUT, class FMROUTINES> void WeightedGeodesicDistanceComp (Image3d<VOXELOUT> & ImgOut,Image3d<VOXELOUT> & Speed,  SeedPointsVector & SeedPoints, HASH_SET<size_t> & boundary, FMROUTINES func)
{
    HASH_SET<size_t>::iterator boundaryEnd = boundary.end();
    std::valarray<VOXELOUT> & ArrivalTimes = ImgOut.GetVoxelData();
    std::valarray<VOXELOUT> & SpeedArray = Speed.GetVoxelData();
    Heap<VOXELOUT,sgngreater> heap(ArrivalTimes);
    std::valarray<unsigned char> Flags( (unsigned char) FARP ,ImgOut.GetVoxelData().size());
    ArrivalTimes = INFTIME(VOXELOUT);
#ifdef I3D_DEBUG
    cout << "Inserting Seed Points\n";
#endif
    size_t index;
    for (SeedPointsVector::iterator iter =  SeedPoints.begin(); iter  < SeedPoints.end(); iter++)
    {
//#ifdef I3D_DEBUG
//	cout << (*iter) << endl;
//#endif
	index = GetIndex((*iter),ImgOut.GetSize());
	ArrivalTimes[index] = 0.0;
	Flags[index] = KNOWN;
	heap.Push(index);
    }
    size_t PointId;
    size_t neighbors[6];
    size_t neighbors2[6],i;
    Vector3d<float> res = ImgOut.GetResolution().GetRes();
    res.x = 1/res.x; res.y = 1/res.y; res.z = 1/res.z;
//    cout << res << endl;
    i3d::Vector3d<size_t> sz; 
    sz.x = ImgOut.GetSizeX();
    sz.y = ImgOut.GetSizeY();
    sz.z = ImgOut.GetSizeZ();
    PointId = heap.Pop();
    while (PointId  != UNDEFINED)
    {
	Flags[PointId] = KNOWN;
	func(neighbors, PointId, sz);
	for(i=0; i<func.max; ++i)
            if (Flags[neighbors[i]] != KNOWN )
	    {
		if ( boundary.find(neighbors[i]) == boundaryEnd)
		{
		    func(neighbors2, neighbors[i], sz);
		    func(ArrivalTimes, SpeedArray[neighbors[i]],res, heap, Flags, neighbors[i], neighbors2);
		}
	    }
	PointId = heap.Pop();
    }
}



template <class VOXELIN, class VOXELOUT> void LocalRadius (const Image3d<VOXELIN> & ImgIn, Image3d<VOXELOUT> & ImgOut, SeedPointsVector & SeedPoints)
{
    FMRoutines2D<VOXELOUT,sgngreater> routines2D;
    FMRoutines3D<VOXELOUT,sgngreater> routines3D;
   
    Image3d<VOXELIN> * ImgTmp;
    Image3d<VOXELOUT> FImg; 
    HASH_SET<size_t> boundary;
    const VOXELIN * ImgInP, * FirstP;
    VOXELIN * ImgTmpP, * LastP;
    VOXELOUT * ImgOutP, * FImgP, * LastFP;
    SeedPointsVector SPV;
    size_t index;
    Vector3d<size_t> pos;

    /* Memory allocation for temporary Image */
    ImgTmp = new i3d::Image3d<VOXELIN>();
    ImgTmp->SetOffset(ImgIn.GetOffset());
    ImgTmp->SetResolution(ImgIn.GetResolution());
    ImgTmp->MakeRoom(ImgIn.GetSize());
    /*Memory allocation fo output image */
 #ifdef I3D_DEBUG
    cout << "Performing Dilation\n";
#endif
    if (ImgIn.GetSizeZ() > 1)
	Dilation(ImgIn, *ImgTmp, nb3D_6);
    else
	Dilation(ImgIn,*ImgTmp, nb2D_4);
    
    ImgInP = ImgIn.GetVoxelAddr(0);
    ImgTmpP = ImgTmp->GetVoxelAddr(0);
    LastP = ImgTmp->GetVoxelAddr(ImgTmp->GetImageSize());
    FirstP = ImgInP;
    while (ImgTmpP != LastP)
    {
	if ((*ImgTmpP) != (*ImgInP))
	{
	    boundary.insert((size_t)(ImgInP - FirstP));
//	    cout << "Hranicni pixel: " << ImgInP - FirstP << endl;
	}
	++ImgInP;
	++ImgTmpP;
    }
#ifdef I3D_DEBUG
    cout << "Performing Erosion\n";
#endif
    if (ImgIn.GetSizeZ() > 1)
	Erosion(ImgIn, *ImgTmp, nb3D_6);
    else
	Erosion(ImgIn,*ImgTmp, nb2D_4);
   

   ImgTmpP = ImgTmp->GetVoxelAddr(0);
   FirstP = ImgTmpP;
   ImgInP = ImgIn.GetVoxelAddr(0);
   

   while (ImgTmpP != LastP)
   {
   if (*ImgTmpP != *ImgInP)
       {
	   index =  ImgTmpP-FirstP;
           SPV.push_back(ImgTmp->GetPos(index));
//         cout << (int) *tmpp << endl;
           }
   else
   {
       index =  ImgTmpP-FirstP;
       pos = ImgTmp->GetPos(index);
       if ((ImgTmp->OnBorder(pos)) && ((*ImgTmpP) == std::numeric_limits<VOXELIN>::max()))
	   SPV.push_back(pos);
   }
   
          // cout << (int) *tmpp << endl;
       ++ImgTmpP;
       ++ImgInP;
   }

   delete ImgTmp;
 #ifdef I3D_DEBUG
    cout << "Making Room for Output Image\n";
#endif
    ImgOut.SetOffset(ImgIn.GetOffset());
    ImgOut.SetResolution(ImgIn.GetResolution());
    ImgOut.MakeRoom(ImgIn.GetSize());
    FImg.SetOffset(ImgIn.GetOffset());
    FImg.SetResolution(ImgIn.GetResolution());
    FImg.MakeRoom(ImgIn.GetSize());

   if (ImgIn.GetSizeZ() > 1)
   {
	GeodesicDistanceComp( ImgOut, SeedPoints, boundary, routines3D);
	GeodesicDistanceComp( FImg,SPV,boundary,routines3D);
   }
    else
    {
	GeodesicDistanceComp( ImgOut, SeedPoints, boundary, routines2D);
	GeodesicDistanceComp( FImg, SPV, boundary, routines2D);
    }

    ImgOutP = ImgOut.GetVoxelAddr(0);
    FImgP = FImg.GetVoxelAddr(0);
    LastFP = ImgOut.GetVoxelAddr(ImgOut.GetImageSize());

    while (ImgOutP != LastFP)
    {
	if (*ImgOutP != std::numeric_limits<VOXELOUT>::max())
	    *ImgOutP = ((*ImgOutP)/ ((*ImgOutP) + (*FImgP))) * 100.0;
	++ImgOutP;
	++FImgP;
    }
}

/** Decide if we should make 2D or 3D fast Marching method and select proper FMroutines class and call the FastMarchingMethodComp function */
    template <class VOXELIN, class VOXELOUT> void FastMarchingSignedDistance (VOXELIN  Speed, Image3d<VOXELOUT> & ImgOut, std::vector<size_t> & indexes, std::vector<VOXELOUT> & values)
{
    FMRoutines2D<VOXELOUT,absgreater> routines2D;
    FMRoutines3D<VOXELOUT,absgreater> routines3D;
    if (ImgOut.GetSizeZ() > 1)
	FastMarchingMethodComp(Speed, ImgOut, indexes, values, routines3D);
    else
	FastMarchingMethodComp(Speed, ImgOut, indexes, values, routines2D);
}




/* Main Fast Marching Nethod Routine, it computes 2D or 3D fast marching method (parameter func), the other parameters are same as in the FastMarching Method routine. This parameters are better described in the LevelSet.h file*/
    template <class VOXELIN, class VOXELOUT, class FMROUTINES> void FastMarchingMethodComp (VOXELIN  Speed, Image3d<VOXELOUT> & ImgOut,  std::vector<size_t> & indexes, std::vector<VOXELOUT> & values, FMROUTINES func)
{
    std::valarray<VOXELOUT> & ArrivalTimes = ImgOut.GetVoxelData();
    Heap<VOXELOUT,absgreater> heap(ArrivalTimes);

    std::valarray<unsigned char> Flags( (unsigned char) FARP ,ImgOut.GetVoxelData().size());
   
    
    //ArrivalTimes = INFTIMEM(VOXELOUT);

//    cout << "Inserting Seed Points\n" << "Auto" <<  INFTIMEM(VOXELOUT) << "asd" << endl ;
    
    size_t index;
    typename std::vector<VOXELOUT>::iterator j;
	 j = values.begin(); 
    for (std::vector<size_t>::iterator i =  indexes.begin(); i  < indexes.end(); i++, j++)
    {
//	cout << (*i)  << " " << (*j)<< endl;
	index = (*i);
	ArrivalTimes[index] = (*j);
	Flags[index] = KNOWN;
	heap.Push(index);
    }

    size_t PointId;
    size_t neighbors[6];
    size_t neighbors2[6],i;
    Vector3d<float> res = ImgOut.GetResolution().GetRes();
    res.x = 1/res.x; res.y = 1/res.y; res.z = 1/res.z;
    i3d::Vector3d<size_t> sz; 
    sz.x = ImgOut.GetSizeX();
    sz.y = ImgOut.GetSizeY();
    sz.z = ImgOut.GetSizeZ();
    
//    cout << "Computing Fast Marching Method\n" << "spacing.x: " << res.x << " spacing.y: " << res.y << " spacing.z: " << res.z << endl;

    PointId = heap.Pop();
    while (PointId  != UNDEFINED)
    {
//	cout << "Inserting point " << ImgOut.GetCoords(PointId) << " on the heap.\n" ;
	Flags[PointId] = KNOWN;
	func(neighbors, PointId, sz);
	for(i=0; i<func.max; i++)
            if (Flags[neighbors[i]] != KNOWN )
	    {
		func(neighbors2, neighbors[i], sz);
		func(ArrivalTimes, Speed, res, heap, Flags, neighbors[i], neighbors2);
	    }
	PointId = heap.Pop();
    }
}













template <class VOXEL> void FindContourLevel(Image3d<VOXEL> & Img, VOXEL& value,size_t Start, VOXEL Threshold, size_t Bins, size_t Step, double Sigma, double Radius)
{
    VOXEL min, max;

    Img.GetRange(min,max);

#ifdef I3D_DEBUG
    cout << "minimum Intensity: " << min << " Maximum Intensity: " << max << endl;
#endif    
    Histogram hist;
    size_t j;
    Filter<double> gauss1d =  MakeGaussFilter1D (Sigma,Radius);
    Buffer buffer(gauss1d.Size()* sizeof(VOXEL));

    /* Make an histogram from arrival times function */
    IntensityHist(Img,hist,Bins,Step);
    valarray<float>tmp;
    
/* copy the histogram to auxiliary variable tmp (fl;oat type) */
    tmp.resize(hist.size());
    for (j = 0; j < tmp.size(); j++)
	tmp[j] = (float) hist[j];
    hist.resize(0);
// for (j = 0; j < tmp.size(); j++)
  //    cout << j << ". " << tmp[j] << ", " << endl;
    
    /* apply 1D gaussian filter for smoothing the histogram*/
    ApplyGauss1D(&tmp[0], std::slice(0,tmp.size(),1),gauss1d,buffer);
//    cout << "*********************\n";
// for (j = 0; j < tmp.size(); j++)
  //    cout << j << ". " << tmp[j] << ", " << endl;
  //  cout << gauss1d.size << endl;
//    for (int i = 0; i < gauss1d.Size(); i++)
//	cout << gauss1d.win[i] << ", ";
  //  cout << endl;

    /* make an filter window for second derivate aproximation 1 -2 1 */
    delete[] gauss1d.win;
    gauss1d.win = new double[3];
    gauss1d.size = Vector3d<size_t>(3,1,1);
    gauss1d.win[2] = gauss1d.win[0] = 1.0;
    gauss1d.win[1] = -2.0;
//     cout << gauss1d.size<< " " << gauss1d.Size() << endl;
 //   for (int i = 0; i < gauss1d.Size(); i++)
//	cout << gauss1d.win[i] << ", ";
 //   cout << endl;

    /* Make second derivatives */
    buffer.allocate(gauss1d.Size()* sizeof(float));
    ApplyGauss1D(&tmp[0], std::slice(0,tmp.size(),1),gauss1d,buffer);
    
//     cout << "*********************\n";
//      for (j = 0; j < tmp.size(); j++)
//      cout << j << ". " << tmp[j] << ", " << endl;
 
//    cout << "*********************\n";
    
      j = Start;
      /* find the threshold value for iso lines */
      while (fabs(tmp[j]) > Threshold )
      {
//	  cout << j << ". " << fabs(tmp[j]) << " > " << Threshold << endl; 
	  j++;
      }
//	  cout << j << ". " << fabs(tmp[j]) << " > " << Threshold << endl; 
      value = (VOXEL) j * Step;
}



/* LeveSet framework code
*
*
*
*
*
*
*/

/****************************************************
 *
 * PDESolver class implementation
 *
*****************************************************/

template <class VOXELIN, class VOXELOUT> PDESolver<VOXELIN,VOXELOUT>::PDESolver(i3d::Image3d<VOXELIN>& in, i3d::Image3d<VOXELOUT> & out):Input(in),Output(out)
{
    InputIsoValue = (VOXELOUT)(0.0);
    ElapsedIteration = 0;
    Function = NULL;
	
    Output.MakeRoom(Input.GetSize());
        // Set number of dimensions
    (Output.GetSizeZ()==1?Dimensions=2: Dimensions = 3);
// Set spacing between pixels (based on the resolution of the image)
    Spacing = i3d::Vector3d<float>(1.0);
    Spacing /= Input.GetResolution().GetRes();
    MaximumIteration = INFTIME(size_t);
    CopyInputToOutput();
}
    
template <class VOXELIN, class VOXELOUT>  PDESolver<VOXELIN,VOXELOUT>::~PDESolver()
{
    if (Function != NULL)
    {
	delete Function;
    }
}

template <class VOXELIN, class VOXELOUT>  void PDESolver<VOXELIN,VOXELOUT>::Initialize()
{}
template <class VOXELIN, class VOXELOUT> void PDESolver<VOXELIN,VOXELOUT>::InitializeIteration()
{}
template <class VOXELIN, class VOXELOUT> void PDESolver<VOXELIN,VOXELOUT>::ComputeChange()
{}
template <class VOXELIN, class VOXELOUT> void PDESolver<VOXELIN,VOXELOUT>::ApplyChange()
{}
template <class VOXELIN, class VOXELOUT> void PDESolver<VOXELIN,VOXELOUT>::SetInputIsoValue (VOXELOUT value) 
{
    InputIsoValue = value;
}
template <class VOXELIN, class VOXELOUT> VOXELOUT PDESolver<VOXELIN,VOXELOUT>::GetInputIsoValue () 
{
    return InputIsoValue; 
}
template <class VOXELIN, class VOXELOUT> void PDESolver<VOXELIN,VOXELOUT>::SetMaximumNumberOfIterations (size_t max) 
{
    MaximumIteration = max;
}
template <class VOXELIN, class VOXELOUT> size_t PDESolver<VOXELIN,VOXELOUT>::GetMaximumNumberOfIterations () 
{
    return MaximumIteration;
}
template <class VOXELIN, class VOXELOUT> void PDESolver<VOXELIN,VOXELOUT>::SetSpacing(float x,float y, float z)
{ 
    Spacing.x = x; 
    Spacing.y = y; 
    Spacing.z = z; 
    if (Function != NULL)
	Function->SetSpacing(GetSpacing());
}

template <class VOXELIN, class VOXELOUT> void PDESolver<VOXELIN,VOXELOUT>::SetSpacing(Vector3d<float> vec)
{
    Spacing = vec;
    if (Function != NULL) 
	Function->SetSpacing(GetSpacing());
}

template <class VOXELIN, class VOXELOUT> Vector3d<float> PDESolver<VOXELIN,VOXELOUT>::GetSpacing()
{
    return Spacing;
}

template <class VOXELIN, class VOXELOUT> void PDESolver<VOXELIN,VOXELOUT>::GetSpacing(float *x, float *y, float *z)
{
    *x = Spacing.x; 
    *y = Spacing.y; 
    *z = Spacing.z;
}

template <class VOXELIN, class VOXELOUT> void PDESolver<VOXELIN,VOXELOUT>::SetGlobalTimeStep(VOXELOUT t) 
{
    GlobalTimeStep = t; 
    if (Function != NULL) 
	Function->SetGlobalTimeStep(t); 
}

template <class VOXELIN, class VOXELOUT> VOXELOUT PDESolver<VOXELIN,VOXELOUT>::GetGlobalTimeStep()
{ 
    if (Function != NULL) 
	return Function->GetGlobalTimeStep(); 
    else 
	return GlobalTimeStep;
}
// Copy Input to Output and convert types if necessary
template <> void PDESolver<GRAY8, float>::CopyInputToOutput()
{
     i3d::GrayToFloat(Input,Output);
#ifdef I3D_DEBUG
     cout << "Copying Gray8 input to float output\n"; 
#endif
}

template <> void PDESolver<GRAY16, float>::CopyInputToOutput()
{
     i3d::GrayToFloat(Input,Output);
#ifdef I3D_DEBUG
     cout << "Copying Gray8 input to float output\n"; 
#endif
}

template <> void PDESolver<float, float>::CopyInputToOutput()
{
    if (Input.GetVoxelAddr(0) != Output.GetVoxelAddr(0))
    {
	valarray<float> & inp = Input.GetVoxelData();
	valarray<float> & out = Output.GetVoxelData();
	out = inp;
        Output.SetOffset(Input.GetOffset());
        Output.SetResolution(Input.GetResolution());
    }
}

template <class VOXELIN, class VOXELOUT> void PDESolver<VOXELIN, VOXELOUT>::CopyInputToOutput()
{
	    throw InternalException ("PDESolver::InitializeOutput: "
		    "Unsupported combination of Input and Output Voxel types!");
}

template <class VOXELIN, class VOXELOUT> void PDESolver<VOXELIN, VOXELOUT>::InitializeOutput()
{
    if (InputIsoValue != (VOXELOUT)(0.0))
	ShiftImage(Output,InputIsoValue);
}

template <class VOXELIN, class VOXELOUT> bool PDESolver<VOXELIN, VOXELOUT>::Convergence()
{
    if (ElapsedIteration < MaximumIteration)
	return false;
    else
	return true;

}

template <class VOXELIN, class VOXELOUT> void PDESolver<VOXELIN, VOXELOUT>::ZeroElapsedIteration()
{
    ElapsedIteration = 0;
}


template <class VOXELIN, class VOXELOUT> size_t PDESolver<VOXELIN, VOXELOUT>::GetNumberOfElapsedIterations()
{
    return ElapsedIteration ;
}

template <class VOXELIN, class VOXELOUT> void PDESolver<VOXELIN, VOXELOUT>::Execute()
{

  //  char s[200];
#ifdef TIME_MEASUREMENT
     clock_t ts, t1s, sums;
#endif
	 InitializeOutput();
    Initialize();
//     Output.SaveImage("out.mha",IMG_METAIO,IMG_UNKNOWN,DefaultComp);
#ifdef TIME_MEASUREMENT
     ts = clock();
#endif
	 while (! Convergence())
    {
#ifdef I3D_DEBUG
	cout << "Iteration No. " << ElapsedIteration << endl;
#endif
	InitializeIteration();
	ComputeChange();
	ApplyChange();
//	sprintf(s,"output%02d.mha",ElapsedIteration);
//	Output.SaveImage(s, IMG_UNKNOWN,IMG_UNKNOWN,DefaultComp);
	++ElapsedIteration;
    }
#ifdef TIME_MEASUREMENT
     t1s = clock();
     sums = t1s -ts;

      cout << "Total PDE: " << ((float) sums) / ((float)CLOCKS_PER_SEC) << "sec." << endl;
#endif
}

/** Input: index - index into image array, neighbors - array with indices of the central point neighborhood 
  output offset - distance of the nearest point on the interface to the point of index
  return value true - is near the interface
               false - is not near the interface 
 */
template <class VOXELIN,class VOXELOUT> bool PDESolver<VOXELIN,VOXELOUT>::
EstimateDistanceInOnePoint (size_t index, size_t * neighbors, VOXELOUT * offset)
{
    size_t i;
    bool close;
    VOXELOUT centerValue;
    valarray<VOXELOUT> & Data = Output.GetVoxelData();


    // Find if the central point of the neighborhood is close the IsoSurface (IsoLine)
    close = false;
    centerValue = Data[index];
    for (i=0; i < Dimensions * 2; ++i) //For all neighbors try if they lie on another side of the boundary
    {
	if (centerValue * Data[neighbors[i]] > 0)
	    continue;
	if ((Data[neighbors[i]] * centerValue) <= 0.0 )
//	if ((Data[neighbors[i]] * centerValue) < 0.0 )
	{
	    close = true;
	    break;
	}
/*	else
	    if (((centerValue == 0.0) && (Data[neighbors[i]] != 0.0)) || 
		((centerValue != 0.0) && (Data[neighbors[i]] == 0.0)))
	    {
		close = true;
		break;
	    } */
    }
    
    if (! close)
    {
	// This point in the lattice is not close the iso surface (or iso line)
//	offset[0] = std::generic_limits<VOXELOUTOUT>::max();
	return false;
	
    }
    else
    {
	// This point in the lattice is close the iso surface
	// The aproximated distance from central point of the neighborhood is 
	// equal to phi / norm(grad(phi))
	// it can be shown that the location of the surface is
	// (x,y,z) - (  (phi(x,y,z) * grad(phi(x,y,z))) / (norm(grad(phi)))^2  )
	// (x,y,z) are lattice coordinates of the central point
	VOXELOUT normGradPhiSquared, forwardValue, backwardValue;
	VOXELOUT forwardD, backwardD; 
	
	normGradPhiSquared = 0.0;
	for (i = 0; i < Dimensions; ++i)
	{ // In this cycle, we aproximate the spatial derivatives stored in offset
	    backwardValue = neighbors[i*2];
	    forwardValue = neighbors[i*2 + 1];
	    if (forwardValue * backwardValue >= 0.0)
	    { //both neighbors lie on the same side of the boundary, or at least on of them is zero
		forwardD = (forwardValue - centerValue) / Spacing[i];
		backwardD = (centerValue - backwardValue) / Spacing[i];
		if (fabs(forwardD) > fabs(backwardD))
		    offset[i] = forwardD;
		else
		    offset[i] = backwardD;
	    }
	    else
	    { // neighbors lies on opposite sides of the embedded boudary
		if (forwardValue * centerValue < 0.0)
		    offset[i] = (forwardValue - centerValue)/Spacing[i];
		else
		    offset[i] = (centerValue - backwardValue)/Spacing[i];
		
	    }
	    normGradPhiSquared += SQR(offset[i]);
	}
	for ( i = 0; i < Dimensions; ++i)
	{
	    offset[i] = (offset[i] * centerValue) / (normGradPhiSquared + MINDIVIDER);
	}
	return true;
    }
}


/****************************************************
 *
 * ExplicitScheme class implementation
 *
*****************************************************/

template <class VOXELIN, class VOXELOUT> ExplicitScheme<VOXELIN, VOXELOUT>::ExplicitScheme(Image3d<VOXELIN>& in, Image3d<VOXELOUT> & out):PDESolver<VOXELIN,VOXELOUT>(in,out)
{};

template <class VOXELIN, class VOXELOUT> ExplicitScheme<VOXELIN, VOXELOUT>::~ExplicitScheme()
{};

template <class VOXELIN, class VOXELOUT> void ExplicitScheme<VOXELIN, VOXELOUT>::ComputeChange()
{
     Function->ComputeUpdateData();
}


template <class VOXELIN, class VOXELOUT> void ExplicitScheme<VOXELIN, VOXELOUT>::ApplyChange()
{
    VOXELOUT timeStep;
    VOXELOUT * outPtr, * buffPtr, * outLastPtr;
    timeStep=Function->GetGlobalTimeStep();

    outPtr = Output.GetVoxelAddr(0);
    outLastPtr = Output.GetVoxelAddr(Output.GetImageSize());
    buffPtr = &(( (ExplicitSchemeFunction<VOXELOUT> * ) Function)->BufferArray[0]);

#ifdef I3D_DEBUG
    cout << "Time Step: " << timeStep << endl;
#endif
    while (outPtr != outLastPtr)
    {
	*outPtr -= timeStep * (*buffPtr);
	outPtr++;
	buffPtr++;
    }
}


/****************************************************
 *
 * PDESolverFunction class implementation
 *
*****************************************************/

template <class VOXEL> PDESolverFunction<VOXEL>::PDESolverFunction(Image3d<VOXEL> & data):
Data(data),DataArray(data.GetVoxelData())
{
    (Data.GetSizeZ()==1?Dimensions = 2: Dimensions = 3);
//    cout << &DataArray << " " << &(data.GetVoxelData()) << endl;
//    cout << Data.GetSize() << endl;
//    cout << (int)Data.GetSizeZ() << " " <<  "Dimensions: " << (int)Dimensions << endl;
// Set spacing between pixels (based on the resolution of the image)
    Spacing = i3d::Vector3d<float>(1.0);
    Spacing /= Data.GetResolution().GetRes();
    GlobalTimeStep = 0.0f;
    FixedTimeStep = false;
    SetEpsilon(0.0);
}

template <class VOXEL> void PDESolverFunction<VOXEL>::ComputeUpdateData()
{}

template <class VOXEL> VOXEL PDESolverFunction<VOXEL>:: GetGlobalTimeStep() 
{
    return GlobalTimeStep; 
}

template <class VOXEL> 	void  PDESolverFunction<VOXEL>::SetGlobalTimeStep(VOXEL ts) 
{ 
    GlobalTimeStep = ts; FixedTimeStep = true; 
}

template <class VOXEL> 	void  PDESolverFunction<VOXEL>::SetSpeedImage(Image3d<VOXEL> * data)
{
    SpeedImage = data;
}

template <class VOXEL>   Image3d<VOXEL> * PDESolverFunction<VOXEL>::GetSpeedImage() 
{
    return SpeedImage;
}

template <class VOXEL> void  PDESolverFunction<VOXEL>::SetSpacing(float x,float y, float z)
{Spacing.x = x; Spacing.y = y; Spacing.z = z;}
	
template <class VOXEL> void  PDESolverFunction<VOXEL>::SetSpacing(Vector3d<float> vec)
{Spacing.x = vec.x; Spacing.y = vec.y; Spacing.z = vec.z;};

template <class VOXEL> Vector3d<float>  PDESolverFunction<VOXEL>::GetSpacing()
{return Spacing;}

template <class VOXEL> void PDESolverFunction<VOXEL>::GetSpacing(float *x, float *y, float *z)
{*x = Spacing.x; *y = Spacing.y; *z = Spacing.z;}

template <class VOXEL> void  PDESolverFunction<VOXEL>::SetEpsilon(VOXEL e) 
{Epsilon = e;}
	
template <class VOXEL> VOXEL  PDESolverFunction<VOXEL>::GetEpsilon() {return Epsilon; }

template <class VOXEL> PDESolverFunction<VOXEL>::~PDESolverFunction(){}


/****************************************************
 *
 * ExplicitSchemeFunction class implementation
 *
*****************************************************/

template <class VOXEL> inline void ExplicitSchemeFunction<VOXEL>::UpdateTimeStep (size_t * neighbors, size_t index, VOXEL tempTimeStep)
{
    if ( GlobalTimeStep > (tempTimeStep = ComputeUpdate(neighbors,index)) )
	GlobalTimeStep = tempTimeStep;
}


template <class VOXEL> VOXEL ExplicitSchemeFunction<VOXEL>::ComputeUpdate(size_t * neighbors,size_t center) 
{ return (static_cast<VOXEL> (0));}

template <class VOXEL> void ExplicitSchemeFunction<VOXEL>::SetConductance (VOXEL c) 
{Conductance = c;};

template <class VOXEL> VOXEL ExplicitSchemeFunction<VOXEL>::GetConductance () 
{return Conductance;};

template <class VOXEL> ExplicitSchemeFunction<VOXEL>::ExplicitSchemeFunction(Image3d<VOXEL> & data) :PDESolverFunction<VOXEL>(data)
{
    Vector3d<size_t> size;
    size.x = Data.GetSizeX();
    size.y = Data.GetSizeY();
    size.z = Data.GetSizeZ();
 
    BufferArray.resize(Data.GetSizeX() * Data.GetSizeY()*Data.GetSizeZ());
    D2Neighbourhood  =  static_cast<LSNeighbourhood *>( new  FourLSNeighbourhood(size));
    D3Neighbourhood  =  (LSNeighbourhood *)( new  SixLSNeighbourhood(size));

}

template <class VOXEL> ExplicitSchemeFunction<VOXEL>::~ExplicitSchemeFunction()
{
    if (D2Neighbourhood != NULL)
	delete D2Neighbourhood;
    if (D3Neighbourhood != NULL)
	delete D3Neighbourhood;

}

template <class VOXEL> void ExplicitSchemeFunction<VOXEL>::SetNeighbourhood(LSNeighbourhood * D2, LSNeighbourhood * D3)
{
    if (D2 != NULL)
    {
	if (D2Neighbourhood != NULL)
	    delete D2Neighbourhood;
	D2Neighbourhood = D2;
    }
    if (D3 != NULL)
    {
	if (D3Neighbourhood != NULL)
	    delete D3Neighbourhood;
	D3Neighbourhood = D3;
    }
}


template <class VOXEL> void ExplicitSchemeFunction<VOXEL>::ComputeUpdateData()
{
    size_t x, y, z, index,i;
    Vector3d<size_t> size;
   
    size.x = Data.GetSizeX();
    size.y = Data.GetSizeY();
    size.z = Data.GetSizeZ();
    
    const size_t xMax = size.x,
                 yMax = size.y,
	         zMax = size.z;

    LSNeighbourhood * neighbourhood = NULL;
    VOXEL tempTimeStep = std::numeric_limits<VOXEL>::max();
    if (! FixedTimeStep)
	GlobalTimeStep = std::numeric_limits<VOXEL>::max();
   
   

    if (this->GetEpsilon() > 0.0)
    {
	if (D2Neighbourhood != NULL)
	    delete D2Neighbourhood;
	if (D3Neighbourhood != NULL)
	    delete D3Neighbourhood;
	D2Neighbourhood  =  (LSNeighbourhood *)( new  EightLSNeighbourhood(size));
	D3Neighbourhood  =  (LSNeighbourhood *)( new  EighteenLSNeighbourhood(size));
    }
    if (zMax > 1 )
    {
	neighbourhood =  D3Neighbourhood; 
    }
    else
    {
	neighbourhood =  D2Neighbourhood;
    }
    
    const size_t nMax = neighbourhood->GetSize();
	size_t * neighbors = new size_t[nMax];
    

    size_t increment[3];
    size_t proportion[3];
    neighbourhood->GetIncrements(increment);
    neighbourhood->GetProportions(proportion);



//    cout << Data.GetSize();
    
 
//    cout << "Tady\n" << zMax << endl;
    if (zMax > 1) //we must compute the level set function in 3 dimensions
    {
//	cout << "Proportion vector: [" << proportion[0] << ", " << proportion[1] << ", " << proportion[2] << "]" << endl;
//	cout << "Increment vector: [" << increment[0] << ", " << increment[1] << ", " << increment[2] << "]" << endl;
	index = proportion[2]*xMax * yMax + proportion[1]*xMax+proportion[0];
	neighbourhood->Neighbors(neighbors, index, size);

	for (z = proportion[2]; z < zMax - proportion[2]; ++z)
	{
	    for (y = proportion[1];  y < yMax - proportion[1]; ++y )
	    {
		for (x = proportion[0]; x < xMax - proportion[0]; ++x)
		{
//		cout << index << " ";
		    UpdateTimeStep (neighbors, index, tempTimeStep);
		    index += increment[0];
		    for (i=0; i < nMax; ++i)
			neighbors[i] += increment[0];
		}
//		cout << endl;
		index += increment[1]; // should be +=2 (3-1)
		for (i=0; i < nMax; ++i)
		    neighbors[i]+= increment[1]; // should be += 2 (3-1)
	    }
//	    cout << endl;
	    index += increment[2]; // should be += 2 * xMax + 2
	    for (i=0; i < nMax; ++i)
		neighbors[i]+= increment[2]; // should be += 2 * xMax + 2
	}
//	cout << endl;
//	cout << "Dolni roviny\n";
	// dolni roviny
	for ( index = 0; 
	      index < proportion[2] * yMax * xMax; 
	      index += increment[0])
	{
	    neighbourhood->Neighbors(neighbors, index, size);
	    UpdateTimeStep (neighbors, index, tempTimeStep);
//		cout << index << " ";
	}
//	cout << endl;
	//horni roviny
//	cout << "Horni roviny\n";
	for (index = xMax * yMax * zMax - proportion[2] * xMax * yMax;
	     index < xMax * yMax * zMax; 
	     index += increment[0])
	{
	    neighbourhood->Neighbors(neighbors, index, size);
	    UpdateTimeStep (neighbors, index, tempTimeStep);
//		cout << index << " ";
	}
//	cout << endl;

	// predni roviny
//	cout << "Predni roviny\n";

	index = xMax * yMax * proportion[2];
	for (z = proportion[2]; z < zMax-proportion[2]; ++z)
	{
	    for (x = 0; x < proportion[1] * xMax; ++x)
	    {
	        neighbourhood->Neighbors(neighbors, index, size);
		UpdateTimeStep (neighbors, index, tempTimeStep);
//		cout << index << " ";
		index += increment[0];
	    }
	    index += xMax * (yMax - proportion[1]);
	}
//	cout << endl;
	
	// zadni roviny
//	cout << "Zadni roviny\n";
	index = xMax * yMax * (proportion[2] + 1) -xMax * proportion[1];
	for (z = proportion[2]; z < zMax - proportion[2]; ++z)
	{
	    for (x = 0; x < proportion[1] * xMax; ++x)
	    {
        	neighbourhood->Neighbors(neighbors, index, size);
		UpdateTimeStep (neighbors, index, tempTimeStep);
//		cout << index << " ";
		index += increment[0];
	    }
	    index += xMax * (yMax - proportion[1]);
	}
	
//	cout << endl;
	// leve roviny
//	cout << "Leve roviny\n";
	index = xMax * yMax * proportion[2] + xMax * proportion[1];
	for (z = proportion[2]; z < zMax - proportion[2]; ++z)
	{
	    for (y = proportion[1]; y < yMax - proportion[1]; ++y)
	    {
		for (x = 0; x < proportion[0]; ++x)
		{
		    neighbourhood->Neighbors(neighbors, index, size);
		    UpdateTimeStep (neighbors, index, tempTimeStep);
//		cout << index << " ";
		    index += increment[0];
		}
		index += xMax - proportion[0];
	    }
	    index += xMax *(proportion[1] * 2);
	}
//	cout << endl;
	// prave roviny
//	cout << "Prave roviny\n";
	index = xMax * yMax * proportion[2] + xMax * (proportion[1] + 1) - proportion[0];
	for (z = proportion[2]; z < zMax - proportion[2]; ++z)
	{
	    for (y = proportion[1]; y < yMax - proportion[1]; ++y)
	    {
		for (x = 0; x < proportion[0]; ++x)
		{
		    neighbourhood->Neighbors(neighbors, index, size);
		    UpdateTimeStep (neighbors, index, tempTimeStep);
//		cout << index << " ";
		    index += increment[0];
		}
		index += xMax - proportion[0];
	    }
	    index += xMax *(proportion[1] * 2) ;
	}
	
//	cout << endl;


    }
    else
    { // two dimensions

	index = proportion[1] * xMax + proportion[0];
	neighbourhood->Neighbors(neighbors, index, size);
	for (y = proportion[1]; y < (yMax - proportion[1]); ++y)
	{
	    for (x = proportion[0]; x < (xMax - proportion[0]); ++x)
	    {
//		cout << neighbors[0] << ", " << neighbors[1] << ", " << neighbors[2] << ", " << neighbors[3] << endl;
		UpdateTimeStep (neighbors, index, tempTimeStep);
		index += increment[0];
		for (i = 0; i < nMax; ++i)
		    neighbors[i] += increment[0];
		    
	    }
//	    cout << endl;
	    index += increment[1];
	    for (i = 0; i < nMax; ++i)
		neighbors[i] += increment[1]; // should be += 2;
	}
//	cout << endl;
	// dolni radky obrazu
	for (index = 0; index < (proportion[1] * xMax); ++index)
	{
	    neighbourhood->Neighbors(neighbors, index, size);
	    UpdateTimeStep (neighbors, index, tempTimeStep);
//		cout << index << " ";
//		cout << neighbors[0] << ", " << neighbors[1] << ", " << neighbors[2] << ", " << neighbors[3] << endl;
	
	}
//	cout << endl;
	// horni radky obrazu
	for (index = (xMax * yMax) - (proportion[1] * xMax); index < (xMax * yMax); ++index)
	{
	    neighbourhood->Neighbors(neighbors, index, size);
	    UpdateTimeStep (neighbors, index, tempTimeStep);
//		cout << index << " ";
//		cout << neighbors[0] << ", " << neighbors[1] << ", " << neighbors[2] << ", " << neighbors[3] << endl;
	
	}
//	cout << endl;

	//leve sloupce obrazu
	for (x = 0; x < proportion[0]; ++x)
	{
	    for (index = (proportion[1] * xMax) + x; 
		 index < (xMax * yMax) - (xMax * proportion[1] + x); 
		 index += xMax )
	    {
		neighbourhood->Neighbors(neighbors, index, size);
		UpdateTimeStep (neighbors, index, tempTimeStep);
//				cout << index << " ";
//		cout << neighbors[0] << ", " << neighbors[1] << ", " << neighbors[2] << ", " << neighbors[3] << endl;
	
	    }
	}
//	cout << endl;
	//leve sloupce obrazu
	for (x = 0; x < proportion[0]; ++x)
	{
	    for (index = (proportion[1]+1) * xMax - 1 - x;
		 index < xMax * yMax  - (proportion[1] - 1)*xMax - 1 - x;
		 index += xMax )
	    {
		neighbourhood->Neighbors(neighbors, index, size);
		UpdateTimeStep (neighbors, index, tempTimeStep);
//		cout << index << " ";
//		cout << neighbors[0] << ", " << neighbors[1] << ", " << neighbors[2] << ", " << neighbors[3] << endl;
	
	    }
	}

    }
	

	delete[] neighbors;
}

/****************************************************
 *
 * MCFFilterFunction  class implementation
 *
*****************************************************/

template <class VOXEL> MCFFilterFunction<VOXEL>::MCFFilterFunction(Image3d<VOXEL> & data):ExplicitSchemeFunction<VOXEL>(data) 
{
    GlobalTimeStep = 1.0/((float) Dimensions * 2.0); FixedTimeStep = true; this->SetEpsilon(1.0);
}
template <class VOXEL> MCFFilterFunction<VOXEL>::~MCFFilterFunction()
{}


template <class VOXEL>  void MCFFilterFunction <VOXEL>::UpdateTimeStep (size_t * neighbors, size_t index, VOXEL tempTimeStep)
{
    ComputeUpdate (neighbors, index); 
}

template <class VOXEL> VOXEL MCFFilterFunction <VOXEL>::ComputeUpdate(size_t * neighbors, size_t center)
{
    VOXEL First[3];
    VOXEL FirstCross[3];
    VOXEL Second[3];
    VOXEL Mixed[3];
    VOXEL GradMagSq, Value, Tmp;
//    VOXEL Speed = -1.0;
    size_t i, j, k,l, D2 = Dimensions * 2;
//    valarray<VOXEL> & orig = OrigImage->GetVoxelData();
//      cout << "Center: " <<center << " Value: " << DataArray[center] << endl;
//    cout << Spacing[0] << ", " << Spacing[1] << ", " << Spacing[2] << endl;
    /*
    for (i=0; i < 18; ++i)
	cout << neighbors[i] << ", " ;
    cout << endl;
      cout << neighbors[8] << " " << neighbors[3] << " " << neighbors[9] << endl;
      cout << neighbors[0] << " " << center << " " << neighbors[1] << endl;
      cout << neighbors[6] << " " << neighbors[2] << " " << neighbors[7] << endl << endl;
      cout << neighbors[12] << " " << neighbors[5] << " " << neighbors[13] << endl;
      cout << neighbors[0] << " " << center << " " << neighbors[1] << endl;
      cout << neighbors[10] << " " << neighbors[4] << " " << neighbors[11] << endl << endl;
      cout << neighbors[16] << " " << neighbors[5] << " " << neighbors[17] << endl;
      cout << neighbors[2] << " " << center << " " << neighbors[3] << endl;
      cout << neighbors[14] << " " << neighbors[4] << " " << neighbors[15] << endl << endl;
  //  cout << center << endl;
  */
    //cout << "\nFirst and second\n";
    for (i = 0; i < Dimensions; ++i)
    {
      //cout << i<< endl;
	//cout << "(" << DataArray[neighbors[2 * i + 1]] << " - " << DataArray[neighbors[2 * i]] << ")/" <<  2.0 * Spacing[i] << endl;
        First[i] = 
            DataArray[neighbors[2 * i + 1]] - DataArray[neighbors[2 * i]];
        First[i] /= 2.0 * Spacing[i];
        Second[i] =
            DataArray[neighbors[2 * i + 1]] + DataArray[neighbors[2 * i]] -
            2 * DataArray[center];
        Second[i] /= SQR(Spacing[i]);
	//cout << "First[" << i << "]: " <<  First[i] << endl;
	//cout << "Second[" << i << "]: " << Second[i] << endl;
	FirstCross[i] = 1.0;
    }
    //cout << "Mixed\n";
    l = ((Dimensions << 1) | 1) & 3;
    //cout << "l: " << l << endl;
    for (i = 0; i < l; ++i)
    {
        k = D2 + i * 4;
	//cout << "k: " << k << endl;
        Mixed[i] =
            DataArray[neighbors[k]] - DataArray[neighbors[k + 1]] -
            DataArray[neighbors[k + 2]] + DataArray[neighbors[k + 3]];
	//cout << DataArray[neighbors[k]] << " - " << DataArray[neighbors[k + 1]] << " - " << DataArray[neighbors[k + 2]] << " + " << DataArray[neighbors[k + 3]] << " = ";
	//cout << Mixed[i] << endl;
        Mixed[i] /= 4.0 * Spacing[i >> 1] * Spacing[2 - ((2 - i) >> 1)];
	//cout << "Mixed[" << i << "]: " << Mixed[i] << endl;
	//cout <<  Spacing[i >> 1] << ", " << Spacing[2 - ((2 - i) >> 1)] << endl;
        //cout << "Cross derivatives: \n";
	//cout << (Dimensions - i) % Dimensions << ": ";
	for (k = 0; k < 2; k++)
	{
		FirstCross[(Dimensions - i) % Dimensions] *= First[(k + i) % Dimensions];
		//cout << (k + i) % Dimensions << " * "; 
	}
	//cout << endl;
	//cout << "CrossDeriv[" <<(Dimensions - i) % Dimensions   << "]: " << FirstCross[(Dimensions - i) % Dimensions] << endl;
    }
    //cout << "ENDMixed\n";
    //cout << "Gradient Magnitude\n";
    GradMagSq = 0.0;
    for (i = 0; i < Dimensions; ++i)
    {
        GradMagSq += SQR(First[i]);
    }
 /*   VOXEL c = .70710678118654752440;
    VOXEL ux,uy,gms;
    ux = 1/(2+4*c) * ( DataArray[neighbors[1]] -  DataArray[neighbors[0]] + c*( DataArray[neighbors[7]] -  DataArray[neighbors[6]] +  DataArray[neighbors[5]] -  DataArray[neighbors[4]]    ));
    
    uy = 1/(2+4*c) * ( DataArray[neighbors[3]] -  DataArray[neighbors[2]] + c*( DataArray[neighbors[7]] -  DataArray[neighbors[5]] +  DataArray[neighbors[6]] -  DataArray[neighbors[4]]    ));
 
    gms = SQR(ux) + SQR(uy);
    cout << GradMagSq << " == " << gms << endl;
//    GradMagSq = gms;*/
    //cout << "GradMaxSq: " << GradMagSq << endl;
    //cout << "Curvature\n";
    Value = 0.0;
    if (!(GradMagSq < 1e-9))
    {
        for (i = 0; i < Dimensions; ++i)
        {
            Tmp = 0.0;
            for (j = 0; j < Dimensions; ++j)
            {
                if (j != i)
                {
                    Tmp += Second[j];
		    //cout << "i: " << i << " j: " << j << " Tmp: " << Tmp << endl;
                }
            }
    //cout << "Value: " << Value<< endl;
    //cout << "Tmp: " << Tmp << " * SQR(First[i]) " << SQR(First[i]) << endl;
            Value += Tmp * SQR(First[i]);
    //cout << "Value: " << Value<< endl;
       }
    for (i = 0; i < l; ++i)
    {
           Value += -2.0 * FirstCross[i] * Mixed[i];
	   //cout << "-2.0 * FirstCross[" << i << "]:  " << -2.0 * FirstCross[i]  << " *Mixed[" << i<<"] " << Mixed[i] << endl;
	   //cout << "Dimensions - i: " << Dimensions - i;
	   //cout << " Value: " << Value<< endl;
    }

    Value /= GradMagSq;
//    cout << Value << " / " <<  powf(GradMagSq,3.0f/4.0f) << endl;
//    Value /= powf(GradMagSq,3.0f/4.0f);
    //cout << DataArray[center] << " == "<<  orig[center] << " ";
//    Value -= sqrt(GradMagSq) * 2 * 0.06 * (DataArray[center] - orig[center]);
    }
    //cout << "Value: " << Value<< endl;
/*    if (-Value > 2000) 
    {
	//cout << "Hodne moc\n";
	//cout << "Center: " <<center << " Value: " << DataArray[center] << endl;
        //cout << neighbors[6] << " " << neighbors[3] << " " << neighbors[7] << endl;
        //cout << neighbors[0] << " " << center << " " << neighbors[1] << endl;
        //cout << neighbors[4] << " " << neighbors[2] << " " << neighbors[5] << endl;
        //cout << -Value << endl;
    }
  */  
    BufferArray[center] = - Value;
    // cout << "Konec\n";
//   return ( fabsf(GradMagSq) < 1e-03f? 0.05f: sqrt(GradMagSq) /4.0f );               
   return 0.125f;               
    // pouze velmi provizorni 
}



/****************************************************
 *
 * MCFFilter  class implementation
 *
*****************************************************/



template <class VOXELIN, class VOXELOUT> MCFFilter<VOXELIN,VOXELOUT>::MCFFilter(Image3d<VOXELIN>& in, Image3d<VOXELOUT> & out):ExplicitScheme<VOXELIN,VOXELOUT>(in,out)
{
    Function =  (new MCFFilterFunction<VOXELOUT>(Output));
    SetSpacing(ComputeNormedSpacing(in));
}

template <class VOXELIN, class VOXELOUT> MCFFilter<VOXELIN,VOXELOUT>::~MCFFilter()
{}

/****************************************************
 *
 * PMFilterFunction  class implementation
 *
*****************************************************/

 template < class VOXEL > PMFilterFunction <VOXEL>::PMFilterFunction (Image3d <VOXEL> &data):
    ExplicitSchemeFunction<VOXEL>(data)
{
  GlobalTimeStep = 1.0 / (((VOXEL) Dimensions) * 2.0);
  this->SetConductance(1.0);
  FixedTimeStep = true;
  SetNorm (TukeyNorm);
};

template < class VOXEL > PMFilterFunction <VOXEL>::~PMFilterFunction()
{}

template <class VOXEL> void PMFilterFunction <VOXEL>::UpdateTimeStep (size_t * neighbors, size_t index, VOXEL tempTimeStep)
{ 
    ComputeUpdate (neighbors, index); 
}
template <class VOXEL> void PMFilterFunction <VOXEL>::SetNorm( VOXEL (*Func) (VOXEL , VOXEL) ) 
{ 
    ptToNormFunction = Func;
}


template < class VOXEL > VOXEL PMFilterFunction <VOXEL>::ComputeUpdate(size_t * neighbors, size_t center)
{
    VOXEL GradEstim, CEstim, Value;
    size_t i;
    
    Value = 0.0;
    
    for (i = 0 ; i < 2*Dimensions; ++i)
    {
	GradEstim = (DataArray[neighbors[i]] - DataArray[center])/Spacing[i/2];
	CEstim = ptToNormFunction(GradEstim,Conductance);
	Value += CEstim * GradEstim;
    }
    
    BufferArray[center] = - Value;
    return .125f;               
}

/****************************************************
 *
 * PMFilter  class implementation
 *
*****************************************************/

template <class VOXELIN, class VOXELOUT> PMFilter<VOXELIN,VOXELOUT>::PMFilter(Image3d<VOXELIN>& in, Image3d<VOXELOUT> & out)
:ExplicitScheme<VOXELIN,VOXELOUT>(in,out){
    Function = (new PMFilterFunction<VOXELOUT>(Output));
    SetSpacing(ComputeNormedSpacing(in));
};

template <class VOXELIN, class VOXELOUT> PMFilter<VOXELIN,VOXELOUT>::~PMFilter()
{}

template <class VOXELIN, class VOXELOUT> void PMFilter<VOXELIN,VOXELOUT>::SetConductance(VOXELOUT c)
{
    (static_cast <PMFilterFunction<VOXELOUT> * > (Function))->SetConductance(c);
}

template <class VOXELIN, class VOXELOUT> VOXELOUT PMFilter<VOXELIN,VOXELOUT>::GetConductance()
{
    return (static_cast <PMFilterFunction<VOXELOUT> * > (Function))->GetConductance();
}

template <class VOXELIN, class VOXELOUT> int PMFilter<VOXELIN,VOXELOUT>::SetNorm (int type)
{
    if (this->Function != NULL)
	switch (type)
	{
	case ORIGINALPM : (static_cast <PMFilterFunction<VOXELOUT> * > (Function))->SetNorm(OriginalPMNorm); 
			  break;
	case ALTERNATIVEPM : (static_cast <PMFilterFunction<VOXELOUT> * > (Function))->SetNorm(AlternativePMNorm); 
			  break;
        case TUKEY:  (static_cast <PMFilterFunction<VOXELOUT> * > (Function))->SetNorm(TukeyNorm);
		     break;
	default:(static_cast <PMFilterFunction<VOXELOUT> * > (Function))->SetNorm(OriginalPMNorm); 
		break;
	}
		     
    else
	return -1;
    return 0;
}

/****************************************************
 *
 * MCDEFilterFunction  class implementation
 *
*****************************************************/
template <class VOXEL>  void MCDEFilterFunction<VOXEL>::UpdateTimeStep (size_t * neighbors, size_t index, VOXEL tempTimeStep)
{ 
    ComputeUpdate (neighbors, index); 
}
	
template <class VOXEL>  MCDEFilterFunction<VOXEL>::~MCDEFilterFunction()
{}
template <class VOXEL> void MCDEFilterFunction<VOXEL>::SetNorm( VOXEL (*Func) (VOXEL , VOXEL) )
{ 
    ptToNormFunction = Func;
}



template <class VOXEL> MCDEFilterFunction<VOXEL>::MCDEFilterFunction(Image3d<VOXEL> & data):ExplicitSchemeFunction<VOXEL>(data) 
{
    GlobalTimeStep = 0.5/((float) powf(2.0,Dimensions)); 
    this->SetConductance(3.0);
    FixedTimeStep = true; 
    this->SetEpsilon(1.0);
    SetNorm (OriginalPMNorm);
};

template <class VOXEL> VOXEL MCDEFilterFunction <VOXEL>::ComputeUpdate(size_t * neighbors, size_t center)
{
    VOXEL Central[3];
    VOXEL CentralF[9];
    VOXEL CentralB[9];
    
    VOXEL Forward[3];
    VOXEL Backward[3];
    
    VOXEL GradMagSqF, GradMagSqB, Tmp, Godunov;
    
    size_t i, i1, i2,j, k,l, D2 = Dimensions * 2;

    
//    for (i=0; i < 8; ++i)
	//cout << neighbors[i] << ", " ;
    //cout << endl;
/*      //cout << neighbors[6] << " " << neighbors[3] << " " << neighbors[7] << endl;
      //cout << neighbors[0] << " " << center << " " << neighbors[1] << endl;
      //cout << neighbors[4] << " " << neighbors[2] << " " << neighbors[5] << endl << endl;
      //cout << endl;*/
      //cout << DataArray[neighbors[8]] << " " << DataArray[neighbors[3]] << " " << DataArray[neighbors[9]] << endl;
      //cout << DataArray[neighbors[0]] << " " << DataArray[center] << " " <<DataArray[ neighbors[1]] << endl;
      //cout << DataArray[neighbors[6]] << " " << DataArray[neighbors[2]] << " " <<DataArray[ neighbors[7]] << endl << endl;
 //cout << endl;
      //cout << DataArray[neighbors[12]] << " " << DataArray[neighbors[5]] << " " << DataArray[neighbors[13]] << endl;
      //cout << DataArray[neighbors[0]] << " " << DataArray[center] << " " <<DataArray[ neighbors[1]] << endl;
      //cout << DataArray[neighbors[10]] << " " << DataArray[neighbors[4]] << " " <<DataArray[ neighbors[11]] << endl << endl;
 //cout << endl;
      //cout << DataArray[neighbors[16]] << " " << DataArray[neighbors[5]] << " " << DataArray[neighbors[17]] << endl;
      //cout << DataArray[neighbors[2]] << " " << DataArray[center] << " " <<DataArray[ neighbors[3]] << endl;
      //cout << DataArray[neighbors[14]] << " " << DataArray[neighbors[4]] << " " <<DataArray[ neighbors[15]] << endl << endl;
 //cout << endl;
      
/*      //cout << neighbors[12] << " " << neighbors[5] << " " << neighbors[13] << endl;
      //cout << neighbors[0] << " " << center << " " << neighbors[1] << endl;
      //cout << neighbors[10] << " " << neighbors[4] << " " << neighbors[11] << endl << endl;
      //cout << neighbors[16] << " " << neighbors[5] << " " << neighbors[17] << endl;
      //cout << neighbors[2] << " " << center << " " << neighbors[3] << endl;
      //cout << neighbors[14] << " " << neighbors[4] << " " << neighbors[15] << endl << endl;
    //cout << center << endl;
  */
 
    for (i = 0; i < Dimensions; ++i)
    {
	Central[i] =  DataArray[neighbors[2 * i + 1]] - DataArray[neighbors[2 * i]];
	Central[i] /= 2.0 * Spacing[i];
	Forward[i] = DataArray[neighbors[2 * i +1]] - DataArray[center];
	Backward[i] = DataArray[center] - DataArray[neighbors[2*i]];
	Forward[i] /= Spacing[i];
	Backward[i] /= Spacing[i];
	//cout << "Backward["<< i << "]: " << Backward[i] << endl;
	//cout << "Central["<< i << "]: " << Central[i] << endl;
	//cout << "Forward["<< i << "]: " << Forward[i] << endl;
//	//cout << i << ": " << Central[i] << endl;
    }

    l = ((Dimensions << 1) | 1) & 3;
    //cout << "l: " << l << endl;
    for (i = 0; i < l; ++i)
    {
        k = D2 + i * 4;
	i1 = i >> 1;
	i2 = 2 - ((2 - i) >> 1);
	//cout << k << " "<< i1 << " "<< i2 << endl;

	//cout << i1* Dimensions + i2 << endl;
	//cout << i2* Dimensions + i1 << endl;
	
	CentralF[i1 * Dimensions + i2] = DataArray[neighbors[k+3]] - DataArray[neighbors[k+1]];
	CentralF[i1 * Dimensions + i2] /= 2.0 * Spacing[i2];
	CentralB[i1 * Dimensions + i2] = DataArray[neighbors[k+2]] - DataArray[neighbors[k]];
	CentralB[i1 * Dimensions + i2] /= 2.0 * Spacing[i2];
	CentralF[i2 * Dimensions + i1] = DataArray[neighbors[k+3]] - DataArray[neighbors[k+2]];
	CentralF[i2 * Dimensions + i1] /= 2.0 * Spacing[i1];
	CentralB[i2 * Dimensions + i1] = DataArray[neighbors[k+1]] - DataArray[neighbors[k]];
	CentralB[i2 * Dimensions + i1] /= 2.0 * Spacing[i1];
	//cout << "CentralF[" << i1<< "," << i2 << "]: " << CentralF[i1* Dimensions + i2] << endl;
	//cout << "CentralF[" << i2<< "," << i1 << "]: " << CentralF[i2* Dimensions + i1] << endl;
	//cout << "CentralB[" << i1<< "," << i2 << "]: " << CentralB[i1* Dimensions + i2] << endl;
	//cout << "CentralB[" << i2<< "," << i1 << "]: " << CentralB[i2* Dimensions + i1] << endl;
	
    }
   Tmp = 0.0;
    for (i = 0; i < Dimensions; ++i)
    {
	GradMagSqF = 0.0;
	GradMagSqB = 0.0;
	for (j = 0; j < Dimensions; ++j)
	{
	    if (j != i)
	    {
		//cout << "GradMagSqF = " << GradMagSqF << endl;
		GradMagSqF += SQR (Central[j] + CentralF[i*Dimensions + j]);
		GradMagSqB += SQR (Central[j] + CentralB[i*Dimensions + j]);
//		//cout << i << "," << j << ": " << Central[j] << ", " << CentralF[i*Dimensions + j] << endl;
//	//cout << CentralF[i* Dimensions + j] << endl;
	
		//cout << "GradMagSqF += SQR(" << Central[j] <<" + " << CentralF[i*Dimensions + j]<< ") " << endl;
		//cout << "GradMagSqF = " << GradMagSqF << endl;

	    }
	   
	}
	GradMagSqF *= 0.25;
	GradMagSqB *= 0.25;
	//cout << "GradMagSqF = " << GradMagSqF << endl;
	GradMagSqF += SQR (Forward[i]);
	//cout << "GradMagSqF += SQR(" << Forward[i] << ") " << endl;
	//cout << "GradMagSqF = " << GradMagSqF << endl;
	GradMagSqB += SQR (Backward[i]);
	GradMagSqF = sqrt(GradMagSqF);
	GradMagSqB = sqrt(GradMagSqB);
	
	//cout << i << ": GradMagSqF: " << GradMagSqF << endl;
	//cout << i << ": GradMagSqB: " << GradMagSqB << endl;
//	//cout << "GradMagSqF: " << GradMagSqF << endl;
//	//cout << "GradMagSqB: " << GradMagSqB << endl;
	
	//cout <<  "(Forward["<< i<<"] / (GradMagSqF + MINDIVIDER)): " << (Forward[i] / (GradMagSqF + MINDIVIDER)) << endl;
	//cout <<  " ptToNormFunction (GradMagSqF, Conductance): " <<  ptToNormFunction (GradMagSqF, Conductance) << endl;
	
	Tmp += ptToNormFunction (GradMagSqF, Conductance) * (Forward[i] / (GradMagSqF + MINDIVIDER));
	Tmp -= ptToNormFunction (GradMagSqB, Conductance) * (Backward[i] / (GradMagSqB + MINDIVIDER));

	//cout <<  "(Backward["<< i<<"] / (GradMagSqB + MINDIVIDER)): " << (Backward[i] / (GradMagSqB + MINDIVIDER)) << endl;
	//cout <<  " ptToNormFunction (GradMagSqB, Conductance): " <<  ptToNormFunction (GradMagSqB, Conductance) << endl;

	//cout << "Tmp: " << Tmp << endl;
	
    }
//    //cout << "Rychlost: " << Tmp<< endl;



Godunov = 0.0;
    if (Tmp > 0.0)
    {
	for (i=0; i < Dimensions; ++i)
	    Godunov += SQR(MIN(Backward[i],0.0)) + SQR(MAX(Forward[i],0.0));
    } else
    {
	for (i=0; i < Dimensions; ++i)
	    Godunov += SQR(MIN(Forward[i],0.0)) + SQR(MAX(Backward[i],0.0));
    }
    Godunov = sqrt (Godunov);
   
   //cout << "Godunov: " << Godunov << endl; 
    
    BufferArray[center] =    - Tmp * Godunov;
//    //cout << "Update: " << BufferArray[center] << endl;
    // //cout << "Konec\n";
//   return ( fabsf(GradMagSq) < 1e-03f? 0.05f: sqrt(GradMagSq) /4.0f );               
//cout << endl << endl << endl; 
   return 0.125f;               
    // pouze velmi provizorni 
}


/****************************************************
 *
 * MCDEFilter  class implementation
 *
*****************************************************/
template <class VOXELIN, class VOXELOUT> MCDEFilter<VOXELIN,VOXELOUT>::~MCDEFilter()
{ }

template <class VOXELIN, class VOXELOUT> void MCDEFilter<VOXELIN,VOXELOUT>::SetConductance(VOXELOUT c)
{
    (static_cast <MCDEFilterFunction<VOXELOUT> * > (Function))->SetConductance(c);
}

template <class VOXELIN, class VOXELOUT> VOXELOUT MCDEFilter<VOXELIN,VOXELOUT>::GetConductance()
{
    return (static_cast <MCDEFilterFunction<VOXELOUT> * > (Function))->GetConductance();
}



template <class VOXELIN, class VOXELOUT> MCDEFilter<VOXELIN,VOXELOUT>::MCDEFilter(Image3d<VOXELIN>& in, Image3d<VOXELOUT> & out)
:ExplicitScheme<VOXELIN,VOXELOUT>(in,out){
    Function = (new MCDEFilterFunction<VOXELOUT>(Output));
    SetSpacing(ComputeNormedSpacing(in));
};




template <class VOXELIN, class VOXELOUT> int MCDEFilter<VOXELIN,VOXELOUT>::SetNorm (int type)
{
    if (this->Function != NULL)
	switch (type)
	{
	case ORIGINALPM : (static_cast <MCDEFilterFunction<VOXELOUT> * > (Function))->SetNorm(OriginalPMNorm); 
			  break;
	case ALTERNATIVEPM : (static_cast <MCDEFilterFunction<VOXELOUT> * > (Function))->SetNorm(AlternativePMNorm); 
			  break;
        case TUKEY:  (static_cast <MCDEFilterFunction<VOXELOUT> * > (Function))->SetNorm(TukeyNorm);
		     break;
	default:(static_cast <MCDEFilterFunction<VOXELOUT> * > (Function))->SetNorm(OriginalPMNorm); 
		break;
	}
		     
    else
	return -1;
    return 0;
}




/****************************************************
 *
 * GradientMagnitudeEstimator  class implementation
 *
*****************************************************/

//Contructor
template < class VOXEL > GradientMagnitudeEstimator<VOXEL>::GradientMagnitudeEstimator(Image3d<VOXEL> & data):ExplicitSchemeFunction<VOXEL>(data) 
{
    LowerDim = 0;
    UpperDim = Dimensions;
}
//Destructor
template < class VOXEL > GradientMagnitudeEstimator<VOXEL>::~GradientMagnitudeEstimator()
{}

template <class VOXEL> void GradientMagnitudeEstimator<VOXEL>::SetLowerAndUpperDim(size_t L,size_t U)
{
    LowerDim = L;
    UpperDim = U;
}

template <class VOXEL> void GradientMagnitudeEstimator<VOXEL>::ApplySqrt()
{
    for (size_t i=0; i < BufferArray.size(); ++i)
	BufferArray[i] = sqrt(BufferArray[i]);
}

template <class VOXEL> void GradientMagnitudeEstimator<VOXEL>::CopyToImage(Image3d<VOXEL> & Img)
{
    valarray<VOXEL> & ImgVa = Img.GetVoxelData();
    ImgVa = BufferArray;
}


template < class VOXEL > VOXEL GradientMagnitudeEstimator<VOXEL>::ComputeUpdate(size_t * neighbors, size_t center)
{
    VOXEL Value;
    size_t i;
    Value = 0.0;
    for (i = LowerDim ; i < UpperDim; ++i)
    {
//	cout << "Neighbor[" << 2*i << "]: " << neighbors[2*i] << " ["<< 2*i+1 << "]: " << neighbors[2*i+1] << endl;
//	cout << DataArray[neighbors[2*i]] << " " << DataArray[neighbors[2*i+1]] << endl;
	Value += SQR(DataArray[neighbors[2*i]] - DataArray[neighbors[2*i+1]])/(2*Spacing[i]);
//	cout << Value << " ";
    }
//  Value *= 0.5;
//    Value = sqrt(0.5*Value);
//    cout << Value << endl;
    BufferArray[center] = Value;
    return .125f;               
}

template <class VOXEL> void GradientMagnitudeEstimator<VOXEL>::UpdateTimeStep (size_t * neighbors, size_t index, VOXEL tempTimeStep)
{
    ComputeUpdate (neighbors, index); 
}


/****************************************************
 *
 * SpatialDerivatives  class implementation
 *
*****************************************************/

//Contructor
template < class VOXEL > SpatialDerivatives<VOXEL>::SpatialDerivatives(Image3d<VOXEL> & data):GradientMagnitudeEstimator<VOXEL>(data) 
{
    Vector3d<size_t> size;
    size.x = Data.GetSizeX();
    size.y = Data.GetSizeY();
    size.z = Data.GetSizeZ();
    SDeriv.x = NULL;
    SDeriv.y = NULL;
    SDeriv.z = NULL;
    this->SetNeighbourhood(static_cast<LSNeighbourhood * >(new SecondOrder2DLSNeighbourhood(size)), static_cast<LSNeighbourhood * >(new SecondOrder3DLSNeighbourhood(size)));
    BufferArray.resize(0);
}

//Destructor
template < class VOXEL > SpatialDerivatives<VOXEL>::~SpatialDerivatives()
{
}

template < class VOXEL > void SpatialDerivatives<VOXEL>::SetArrayPointers( valarray<VOXEL>* x,valarray<VOXEL>* y, valarray<VOXEL>* z)
{
    SDeriv.x = x;
    SDeriv.y = y;
    SDeriv.z = z;
}


template < class VOXEL > VOXEL SpatialDerivatives<VOXEL>::ComputeUpdate(size_t * neighbors, size_t center)
{
    size_t i;
/*    cout << endl<< "Center: " << center << endl;
    cout << "     " <<  neighbors[7] << endl;
     cout << "      |" << endl;
    cout << "     " <<  neighbors[5] << endl;
    cout << "      |" << endl;
    cout << neighbors[2] << "--" <<  neighbors[0] << "--" <<  center <<"--" << neighbors[1] << "--" <<  neighbors[3] << endl;
    cout << "      |" << endl;
    cout << "     " <<  neighbors[4] << endl;
     cout << "      |" << endl;
    cout << "     " <<  neighbors[6] << endl;
  */  
/* 
     cout << endl<< "Center: " << center << endl;
    cout << "         " <<  neighbors[7] << endl;
     cout << "          |" << endl;
    cout << "         " <<  neighbors[5] << endl;
    cout << "          |" << endl;
    cout << neighbors[2] << "--" <<  neighbors[0] << "--" <<  center <<"--" << neighbors[1] << "--" <<  neighbors[3] << endl;
    cout << "          |" << endl;
    cout << "         " <<  neighbors[4] << endl;
     cout << "          |" << endl;
    cout << "         " <<  neighbors[6] << endl;
     cout << "         " <<  neighbors[11] << endl;
     cout << "          |" << endl;
    cout << "         " <<  neighbors[9] << endl;
    cout << "          |" << endl;
    cout << "           " <<  center << endl;
    cout << "          |" << endl;
    cout << "         " <<  neighbors[8] << endl;
     cout << "          |" << endl;
    cout << "         " <<  neighbors[10] << endl;
  */  
/*    for (i = LowerDim ; i < UpperDim; ++i)
    {
	(*(SDeriv[i]))[center] =  
	         (DataArray[neighbors[4*i+2]] - DataArray[neighbors[4*i + 3]]  
	         -8*DataArray[neighbors[4*i]] + 8*DataArray[neighbors[4*i + 1]]) / 
	           (12*Spacing[i]); 
    }*/
/*	(*(SDeriv[0]))[center] =  
	         (DataArray[neighbors[2]] - DataArray[neighbors[3]]  
	         -8*DataArray[neighbors[0]] + 8*DataArray[neighbors[1]]) / 
	           (12*Spacing[0]); 
*/	
    for (i = 0 ; i < UpperDim; ++i)
    {
	(*(SDeriv[i]))[center] =  
	         (+DataArray[neighbors[4*i+2]] - DataArray[neighbors[4*i + 3]]  
	         -static_cast<VOXEL>(8)*DataArray[neighbors[4*i]] + static_cast<VOXEL>(8)*DataArray[neighbors[4*i + 1]]) / 
	           (static_cast<VOXEL>(12)*Spacing[i]); 
    }


    return .125f;               
}

#ifdef WITH_LAPACK
    
/****************************************************
 *
 * ImplicitAOSScheme class implementation
 *
*****************************************************/

template <class VOXELIN,class VOXELOUT> void ImplicitAOSScheme<VOXELIN,VOXELOUT>::ComputeDiffusivity()
{}

template <class VOXELIN,class VOXELOUT> void ImplicitAOSScheme<VOXELIN,VOXELOUT>::SetSymetric(bool s) 
{Symetric = s; }

template <class VOXELIN,class VOXELOUT> bool ImplicitAOSScheme<VOXELIN,VOXELOUT>::GetSymetric(){return Symetric;}

template <class VOXELIN,class VOXELOUT> void ImplicitAOSScheme<VOXELIN,VOXELOUT>::SetSpacing(float x,float y, float z)
{
    Spacing.x = x;
    Spacing.y = y; 
    Spacing.z = z;
    GradMag->SetSpacing(Spacing);
}
template <class VOXELIN,class VOXELOUT> void ImplicitAOSScheme<VOXELIN,VOXELOUT>::SetSpacing(Vector3d<float> vec)
{
    Spacing.x = vec.x; 
    Spacing.y = vec.y; 
    Spacing.z = vec.z;
    GradMag->SetSpacing(Spacing);
}

template <class VOXELIN,class VOXELOUT> void ImplicitAOSScheme<VOXELIN,VOXELOUT>::AllocateDiagonals(size_t Max) 
{
    row.resize(Max);
    Diag.resize(Max);
    UDiag.resize(Max-1);
    if (!(Symetric))
	LDiag.resize(Max-1);
}



template <class VOXELIN, class VOXELOUT> ImplicitAOSScheme<VOXELIN, VOXELOUT>::ImplicitAOSScheme(Image3d<VOXELIN>& in, Image3d<VOXELOUT> & out):PDESolver<VOXELIN,VOXELOUT>(in,out),OutputArray(out.GetVoxelData())
{
  double sum;
  VOXELIN * pom, * last;
  BufferArray.resize(Output.GetSizeX() * Output.GetSizeY()*Output.GetSizeZ());
  GradMag = new GradientMagnitudeEstimator<VOXELOUT>(Output);
  GradMag->SetSpacing(1.0,1.0,1.0);
  GradMagData = &(GradMag->BufferArray);
  SetSymetric(true);
  
  pom = Input.GetVoxelAddr(0);
  last = Input.GetVoxelAddr(Input.GetImageSize());
  sum = 0.0;
  while (pom != last)
  {
      sum += *pom;
      ++pom;
  }
  AverageGrey = (sum / (double) Input.GetImageSize());
  InputMoment = Moment(Input,AverageGrey);
  MaxGlobality = 2.0;
}

template <class VOXELIN, class VOXELOUT> ImplicitAOSScheme<VOXELIN, VOXELOUT>::~ImplicitAOSScheme()
{
    delete GradMag;
}


template <class VOXELIN, class VOXELOUT> void ImplicitAOSScheme<VOXELIN, VOXELOUT>::PrepareDiagonals (size_t start, size_t incr, Vector3d<size_t> & size, size_t axe)
{   
    VOXELOUT m = (VOXELOUT) Dimensions;
    size_t neighbors[2];
        float spacing = 2.0 * SQR(Spacing[axe]);
	size_t Max = size[axe], index;
	
     TwoNeighbors (neighbors, start, size, axe);
	Diag[0] = ( //(*GradMagData)[neighbors[0]] + 
		      (*GradMagData)[neighbors[1]] + 
		      ((*GradMagData)[start])) / (spacing);
	UDiag[0]  = - ((*GradMagData)[neighbors[1]] + (*GradMagData)[start]) / (spacing);

	
	TwoNeighbors (neighbors, start + incr, size, axe);
	for (index = 1; index < Max -1; ++index)
	{
	    Diag[index] = ((*GradMagData)[neighbors[0]]  + 
		    (*GradMagData)[neighbors[1]] + 
		    2.0*((*GradMagData)[start+index*incr])) / (spacing);
	    UDiag[index]  =  - ((*GradMagData)[neighbors[1]] + (*GradMagData)[start+index*incr]) / (spacing);

	    neighbors[0] += incr;
	    neighbors[1] += incr;
	}
	neighbors[1] -= incr;
	Diag[index] = ((*GradMagData)[neighbors[0]] + 
		       //(*GradMagData)[neighbors[1]] + 
		       ((*GradMagData)[start + index*incr])) / (spacing);
	
	Diag *= SQR(m) * GlobalTimeStep;
	UDiag *=  SQR(m) * GlobalTimeStep;
	Diag += m;
}


template <class VOXELIN, class VOXELOUT> void ImplicitAOSScheme<VOXELIN, VOXELOUT>::ComputeChange()
{
    size_t x, z, i, start;
    Vector3d < size_t > size;
    int rad, vec = 1, info;

    size.x = Output.GetSizeX();
    size.y = Output.GetSizeY();
    size.z = Output.GetSizeZ();
  //  valarray<VOXELOUT> reseni;

    const size_t xMax = size.x,
        yMax = size.y, zMax = size.z, imageSize = size.x * size.y * size.z;

    ComputeDiffusivity();
#ifdef I3D_DEBUG
    cout << "Computing AOS scheme\n";
    cout.flush();
#endif
    BufferArray = 0.0;
#define XBLA
#ifdef XBLA
   //Two Dimensions
    AllocateDiagonals(xMax);
    rad = xMax;
//    reseni.resize(rad);
    for (i = 0; i < imageSize / xMax; ++i)
    {
        start = i * xMax;
        PrepareDiagonals(start, 1, size, X_AXE);
        row = OutputArray[slice(start, xMax, 1)];
	
        if (Symetric)
	  //  info = ThomasAlg(Diag, UDiag, row, reseni, rad);
            sptsv_(rad, vec, &Diag[0], &UDiag[0], &row[0], rad, info);
        else
            sgtsv_(rad, vec, &LDiag[0], &Diag[0], &UDiag[0], &row[0], rad,
                   info);
        UpdateRow(BufferArray, row, slice(start, xMax, 1));
#ifdef I3D_DEBUG
        if (info != 0)
            cerr << "AOS X axe: Failed to find the solution of system of linear equations\n";
#endif
    }
#endif
#define YBLA
#ifdef YBLA
    AllocateDiagonals(yMax);
    rad = yMax;

    for (z = 0; z < zMax; ++z)
    {
        for (x = 0; x < xMax; ++x)
        {
            start = (z * xMax * yMax) + x;
            PrepareDiagonals(start, xMax, size, Y_AXE);
            row = OutputArray[slice(start, yMax, xMax)];
            if (Symetric)
                sptsv_(rad, vec, &Diag[0], &UDiag[0], &row[0], rad, info);
            else
                sgtsv_(rad, vec, &LDiag[0], &Diag[0], &UDiag[0], &row[0], rad,
                       info);
            UpdateRow(BufferArray, row, slice(start, yMax, xMax));
#ifdef I3D_DEBUG
            if (info != 0)
                cerr << "AOS Y axe: Failed to find the solution of system of linear equations\n";
#endif
        }
    }
#endif
#define ZBLA
#ifdef ZBLA
    //Three Dimensions
    if (zMax > 1)
    {
        size_t sizexy = size.x * size.y;
        //z rows
        AllocateDiagonals(zMax);
        rad = zMax;
        for (i = 0; i < imageSize / zMax; ++i)
        {
            start = i;
            row = OutputArray[slice(start, zMax, sizexy)];
            PrepareDiagonals(start, sizexy, size, Z_AXE);
            if (Symetric)
                sptsv_(rad, vec, &Diag[0], &UDiag[0], &row[0], rad, info);
            else
                sgtsv_(rad, vec, &LDiag[0], &Diag[0], &UDiag[0], &row[0], rad,
                       info);
            UpdateRow(BufferArray, row, slice(start, zMax, sizexy));
#ifdef I3D_DEBUG
            if (info != 0)
                cerr << "AOS Z axe: Failed to find the solution of system of linear equations\n";
#endif
        }
    }

#endif

}

template <class VOXELIN, class VOXELOUT> bool ImplicitAOSScheme<VOXELIN, VOXELOUT>::Convergence()
{
    if (ElapsedIteration < MaximumIteration) 
    {
	if (MaxGlobality <=1.0)
	{
	    float g =  Globality();
#ifdef I3D_DEBUG
	    cout << g << endl;
#endif
	    return g > MaxGlobality;
	}
	return false;
    }
    else
	return true;

}

template <class VOXELIN, class VOXELOUT> void ImplicitAOSScheme<VOXELIN, VOXELOUT>::SetMaxGlobality(float g)
{
    MaxGlobality = g;
}

template <class VOXELIN, class VOXELOUT> float ImplicitAOSScheme<VOXELIN, VOXELOUT>::Globality()
{
    return ((InputMoment - Moment(Output,AverageGrey)) / (InputMoment));
}






template <class VOXELIN, class VOXELOUT> void ImplicitAOSScheme<VOXELIN, VOXELOUT>::ApplyChange()
{
    OutputArray = BufferArray;
}

template <class VOXELIN, class VOXELOUT> float ImplicitAOSScheme<VOXELIN, VOXELOUT>::ComputeLambdaFromQuantile(float quantile)
{
/*    Image3d<float> * fimg = new Image3d<float>();

//    GrayToFloat(Input, *fimg);
    (*fimg) = Output;
    GradientMagnitudeEstimator<float> * gme = new GradientMagnitudeEstimator <float> (*fimg);
    gme->SetSpacing(GetSpacing());
    gme->ComputeUpdateData();


    valarray < float >&da = fimg->GetVoxelData();
    da = gme->BufferArray;
    da.apply(sqrt);
    delete gme;
    Image3d<VOXELIN> * i = new Image3d<VOXELIN>();
    FloatToGrayNoWeight(*fimg, *i);
    delete fimg;

    Histogram h;
    IntensityHist(*i, h);

    size_t px_no = size_t(((float) h.sum()) * (quantile / 100.0));
    size_t sum, j;

    sum = 0;
    j = 0;
    while (px_no > sum)
    {
        sum += h[j];
        j++;
    }

    float lambda = (j - 1) + ((float) (sum - px_no)) / ((float) (h[j - 1]));
    return lambda;*/
    return 0.0;
}

/****************************************************
 *
 *  AOSCLMCFilter class implementation
 *
*****************************************************/

template <class VOXELIN, class VOXELOUT> void AOSCLMCFilter<VOXELIN,VOXELOUT>::SetSpacing(float x,float y, float z)
{
    Spacing.x = x;
    Spacing.y = y; 
    Spacing.z = z;
    GradMag->SetSpacing(Spacing);
    GaussEstimator->SetSpacing(Spacing);
}
template <class VOXELIN, class VOXELOUT> void AOSCLMCFilter<VOXELIN,VOXELOUT>::SetSpacing(Vector3d<float> vec)
{
    Spacing.x = vec.x; 
    Spacing.y = vec.y; 
    Spacing.z = vec.z;
    GradMag->SetSpacing(Spacing);
    GaussEstimator->SetSpacing(Spacing);
}


template <class VOXELIN, class VOXELOUT> void AOSCLMCFilter<VOXELIN,VOXELOUT>::ComputeDiffusivity()
{
    VOXELOUT *pom, *last, Cm, Conductance2;
    Conductance2 = SQR(Conductance);
    if (GaussEstimator->GetSigma() != 0) 
    {
	GaussEstimator->ZeroElapsedIteration();
	GaussEstimator->Execute();
    }
    GradMag->ComputeUpdateData();
    Cm = RegulaFalsi(0.5f, 6.0f, &pmfce, &Exponent);
    pom = &((*GradMagData)[0]);
    last = &((*GradMagData)[Output.GetImageSize()]);
    while (pom != last)
    {
	*pom =  (1 - exp(- Cm / ( powf((*pom) / Conductance2,Exponent) + MINDIVIDER) ));
	++pom;
    }
}


template <class VOXELIN, class VOXELOUT> AOSCLMCFilter<VOXELIN,VOXELOUT>::~AOSCLMCFilter()
{
    delete GaussEstimator; 
}

template <class VOXELIN, class VOXELOUT> void AOSCLMCFilter<VOXELIN,VOXELOUT>::SetConductance (VOXELOUT c) 
{
    Conductance = c;
}

template <class VOXELIN, class VOXELOUT> VOXELOUT AOSCLMCFilter<VOXELIN,VOXELOUT>::GetConductance () 
{
    return Conductance;
}

template <class VOXELIN, class VOXELOUT> void AOSCLMCFilter<VOXELIN,VOXELOUT>::SetSigma(VOXELOUT sig) 
{
    GaussEstimator->SetSigma(sig);
}

template <class VOXELIN, class VOXELOUT> VOXELOUT AOSCLMCFilter<VOXELIN,VOXELOUT>::GetSigma()  
{
    if (GaussEstimator != NULL) 
	return GaussEstimator->GetSigma(); 
    else 
	return 1.0; 
}

template <class VOXELIN, class VOXELOUT> void AOSCLMCFilter<VOXELIN,VOXELOUT>::SetExponent(VOXELOUT exp)
{
    Exponent = exp;
}

template <class VOXELIN, class VOXELOUT> VOXELOUT AOSCLMCFilter<VOXELIN,VOXELOUT>::GetExponent()
{
    return Exponent;
}

template <class VOXELIN, class VOXELOUT>  AOSCLMCFilter<VOXELIN,VOXELOUT>::AOSCLMCFilter(Image3d<VOXELIN>& in, Image3d<VOXELOUT> & out)
:ImplicitAOSScheme<VOXELIN,VOXELOUT>(in,out)
{ Function = NULL;
  this->SetGlobalTimeStep(5.0);
  GaussEstimator = new  LODGaussianBlur<VOXELOUT,VOXELOUT>(Output,Output);
  GaussEstimator->SetSigma(1.0);
  GaussEstimator->SetMaximumNumberOfIterations(1);
  GaussEstimator->SetSpacing(1.0,1.0,1.0); 
  Conductance = 5.0;
  Exponent = 4.0;
  this->SetSymetric(true);
  SetSpacing(ComputeNormedSpacing(in));
}



/****************************************************
 *
 * AOSTVFilter class implementation
 *
*****************************************************/

template <class VOXELIN, class VOXELOUT > void AOSTVFilter <VOXELIN, VOXELOUT >::ComputeDiffusivity()
{
  VOXELOUT *pom, *last;
  GradMag->ComputeUpdateData();
  pom = &((*GradMagData)[0]);
  last = &((*GradMagData)[Output.GetImageSize ()]);
  if (Exponent == 1.0)
    {
      while (pom != last)
	{
	  *pom = 1.0 / (sqrt ((*pom) + Beta));
	  ++pom;
	}
    }
  else if (Exponent == 2.0)
    {
//	VOXELOUT B2 = SQR(Beta);
      while (pom != last)
	{
	  *pom = 1.0 / ((*pom) + Beta);
	  ++pom;
	}
    }
  else
    {
//	VOXELOUT B2 = SQR(Beta);
      while (pom != last)
	{
	  *pom = 1.0 / (powf ((*pom) + Beta, Exponent / 2.0) + MINDIVIDER);
	  ++pom;
	}

    }
}


template <class VOXELIN, class VOXELOUT>  AOSTVFilter<VOXELIN,VOXELOUT>::AOSTVFilter(Image3d<VOXELIN>& in, Image3d<VOXELOUT> & out)
:ImplicitAOSScheme<VOXELIN,VOXELOUT>(in,out)
{ 
    Function = NULL;
    this->SetGlobalTimeStep(5.0);
    SetSpacing(ComputeNormedSpacing(in));
    SetExponent(1.0);
}

template <class VOXELIN, class VOXELOUT> void AOSTVFilter<VOXELIN,VOXELOUT>::SetExponent(VOXELOUT exp)
{
    Exponent = exp;
    Beta = powf((float)MINDIVIDER,1/Exponent);
}

template <class VOXELIN, class VOXELOUT> AOSTVFilter<VOXELIN,VOXELOUT>::~AOSTVFilter()
{}

template <class VOXELIN, class VOXELOUT> VOXELOUT AOSTVFilter<VOXELIN,VOXELOUT>::GetExponent()
{
    return Exponent;
}

template <class VOXELIN, class VOXELOUT> void AOSTVFilter<VOXELIN,VOXELOUT>::SetBeta(VOXELOUT B)
{
    Beta = B;
}

template <class VOXELIN, class VOXELOUT> VOXELOUT AOSTVFilter<VOXELIN,VOXELOUT>::GetBeta()
{
    return Beta;
}

/****************************************************
 *
 * AOSPMFilter class implementation
 *
*****************************************************/

template <class VOXELIN, class VOXELOUT>  void AOSPMFilter<VOXELIN,VOXELOUT>::ComputeDiffusivity()
{
//    GradMag->ComputeUpdateData();
}

template <class VOXELIN, class VOXELOUT> void AOSPMFilter<VOXELIN,VOXELOUT>::PrepareDiagonals (size_t start, size_t incr, Vector3d<size_t> & size, size_t axe)
{   
    VOXELOUT m = (VOXELOUT) Dimensions;
    size_t neighbors[2];
        float spacing = Spacing[axe];
	size_t Max = size[axe], index;
        TwoNeighbors (neighbors, start, size, axe);
	Diag[0] =  ptToNormFunction((OutputArray[neighbors[1]] - OutputArray[start])/spacing,Conductance);
	UDiag[0] = -Diag[0];
	TwoNeighbors (neighbors, start + incr, size, axe);
	for (index = 1; index < Max -1; ++index)
	{
	    Diag[index] = ptToNormFunction(((OutputArray[neighbors[1]] - OutputArray[start+index * incr]) / spacing),Conductance);
	    UDiag[index] = - Diag[index];
	    Diag[index] += ptToNormFunction(((OutputArray[neighbors[0]] - OutputArray[start+index * incr]) / spacing),Conductance);
	    neighbors[0] += incr;
	    neighbors[1] += incr;
	}
	Diag[index] = ptToNormFunction(((OutputArray[neighbors[0]] - OutputArray[start+index * incr]) / spacing),Conductance) ;

	Diag *= SQR(m) * GlobalTimeStep;
	UDiag *=  SQR(m) * GlobalTimeStep;
	Diag += m;
}


template <class VOXELIN, class VOXELOUT>  AOSPMFilter<VOXELIN,VOXELOUT>::AOSPMFilter(Image3d<VOXELIN>& in, Image3d<VOXELOUT> & out)
:ImplicitAOSScheme<VOXELIN,VOXELOUT>(in,out)
{ 
  Function = NULL;
  this->SetGlobalTimeStep(5.0);
  SetSpacing(ComputeNormedSpacing(in));
  SetNorm(ORIGINALPM);
};

template <class VOXELIN, class VOXELOUT> AOSPMFilter<VOXELIN,VOXELOUT>::~AOSPMFilter()
{}

template <class VOXELIN, class VOXELOUT> void AOSPMFilter<VOXELIN,VOXELOUT>::SetConductance (VOXELOUT c) 
{
    Conductance = c;
}

template <class VOXELIN, class VOXELOUT> VOXELOUT AOSPMFilter<VOXELIN,VOXELOUT>::GetConductance () 
{
    return Conductance;
}

template <class VOXELIN, class VOXELOUT> int  AOSPMFilter<VOXELIN,VOXELOUT>::SetNorm (int type)
{
    switch (type)
    {
	case ORIGINALPM : 
	    ptToNormFunction = OriginalPMNorm;
	    break;
	case ALTERNATIVEPM : 
	    ptToNormFunction = AlternativePMNorm;
	    break;
        case TUKEY:  
	    ptToNormFunction = TukeyNorm;
	    break;
	default:
	    ptToNormFunction = OriginalPMNorm;
	    break;
	}
    return 0;
}


/****************************************************
 *
 * ImplicitLODScheme class implementation
 *
*****************************************************/

template <class VOXELIN, class VOXELOUT> ImplicitLODScheme<VOXELIN, VOXELOUT>::ImplicitLODScheme(Image3d<VOXELIN>& in, Image3d<VOXELOUT> & out):PDESolver<VOXELIN,VOXELOUT>(in,out),OutputArray(out.GetVoxelData())
{ 
    Function = NULL;
    this->SetMaximumNumberOfIterations (1);
}


template <class VOXELIN,class VOXELOUT>void ImplicitLODScheme<VOXELIN,VOXELOUT>::PrepareDiagonals(int dim){}
	

template <class VOXELIN, class VOXELOUT> ImplicitLODScheme<VOXELIN, VOXELOUT>::~ImplicitLODScheme(){
}

template <class VOXELIN, class VOXELOUT> void ImplicitLODScheme<VOXELIN, VOXELOUT>::ComputeChange()
{
    size_t x, z, i,j, start, imageSize;
    Vector3d < size_t > size;
    valarray < VOXELOUT > row, work, out;
    int rad, vec = 1, info;
    size.x = Output.GetSizeX();
    size.y = Output.GetSizeY();
    size.z = Output.GetSizeZ();
    imageSize = size.x * size.y * size.z;
    for (j = 0; j < Dimensions; ++j)
    {
	PrepareDiagonals(j);
	rad = size[j];
	row.resize(size[j]);
        if (j == 0)
	{
            for (i = 0; i < imageSize / size.x; ++i)
            {
                start = i * size.x;
                row = OutputArray[slice(start, size.x, 1)];
                sptsv_(rad, vec, &Diag[0], &SDiag[0], &row[0], rad, info);
#ifdef I3D_DEBUG		
                if (info != 0)
                    cerr << "LOD X axe: Failed to find the solution of system of linear equations\n";
#endif		
                MultiplyRow(OutputArray, row, slice(start, size.x, 1));
	Diag = PDiag;
	SDiag = PSDiag;
	
	    }
        }  else if (j == 1)
        {
            for (z = 0; z < size.z; ++z)
            {
                for (x = 0; x < size.x; ++x)
                {
                    start = (z * size.x * size.y) + x;

                    row = OutputArray[slice(start, size.y, size.x)];
                    sptsv_(rad, vec, &Diag[0], &SDiag[0], &row[0], rad, info);
#ifdef I3D_DEBUG
                    if (info != 0)
                        cerr << "LOD Y axe: Failed to find the solution of system of linear equations\n";
#endif
                    MultiplyRow(OutputArray, row, slice(start, size.y, size.x));
	Diag = PDiag;
	SDiag = PSDiag;
	
                }
            }
        } else
        {
            int sizexy = size.x * size.y;
            for (i = 0; i < imageSize / size.z; ++i)
            {
                row = OutputArray[slice(i, size.z, sizexy)];
                sptsv_(rad, vec, &Diag[0], &SDiag[0], &row[0], rad, info);
                MultiplyRow(OutputArray, row, slice(i, size.z, sizexy));
#ifdef I3D_DEBUG
                if (info != 0)
                    cout << "LOD Z axe: Failed to find the solution of system of linear equations\n";
#endif
	Diag = PDiag;
	SDiag = PSDiag;
            }
        }
    }

}

/****************************************************
 *
 * LODGaussianBlur class implementation
 *
*****************************************************/

template <class VOXELIN,class VOXELOUT> void LODGaussianBlur<VOXELIN,VOXELOUT>::PrepareDiagonals(int dim) 
{
    	VOXELOUT r = GlobalTimeStep / SQR(Spacing[dim]);
        Vector3d < size_t > size;
	size.x = Output.GetSizeX();
        size.y = Output.GetSizeY();
        size.z = Output.GetSizeZ();
        SDiag.resize(size[dim] - 1);
        Diag.resize(size[dim]);
        PSDiag.resize(size[dim] - 1);
        PDiag.resize(size[dim]);

	PSDiag = -r;
        PDiag = 1 + 2 * r;
        PDiag[0] -= r;
        PDiag[size[dim] - 1] -= r;
	Diag = PDiag;
	SDiag = PSDiag;
}

template <class VOXELIN, class VOXELOUT> void  LODGaussianBlur<VOXELIN,VOXELOUT>::SetSigma(VOXELOUT s)
{ 
    sigma = s;
    SetGlobalTimeStep((sigma*sigma)/2.0); 
}

template <class VOXELIN, class VOXELOUT> VOXELOUT  LODGaussianBlur<VOXELIN,VOXELOUT>::GetSigma() 
{
    return sigma;
}

template <class VOXELIN, class VOXELOUT> LODGaussianBlur<VOXELIN,VOXELOUT>::~LODGaussianBlur()
{}

template <class VOXELIN, class VOXELOUT> LODGaussianBlur<VOXELIN,VOXELOUT>::LODGaussianBlur(Image3d<VOXELIN>& in, Image3d<VOXELOUT> & out):ImplicitLODScheme<VOXELIN,VOXELOUT>(in,out)
{ Function = NULL;
    sigma = 1.2;
    SetGlobalTimeStep((sigma*sigma)/2.0);
    SetSpacing(ComputeNormedSpacing(in));
}




#endif

/****************************************************
 *
 * KMeansLevel Set algorithm  classes implementation
 *
*****************************************************/

template <class VOXELIN, class VOXELOUT> void KMeansLS<VOXELIN,VOXELOUT>::InitializeIteration()
{}

template <class VOXELIN,class VOXELOUT> KMeansLS<VOXELIN,VOXELOUT>::KMeansLS(Image3d<VOXELIN>& in, Image3d<VOXELOUT> & out):
PDESolver<VOXELIN,VOXELOUT>(in,out)
{
    Function = NULL;
    InitMask = NULL;
    Thres = 0;
    lambda1 = lambda2 = 1.0; 
    lastc1sum = lastc2sum = 0; 
    c1sum = c2sum = 1;
    c1 = c2 = 1;
    InitLow = -1;
    InitHigh = +1;
    initradius = 10;
    initgap = 20;
}
template <class VOXELIN,class VOXELOUT> KMeansLS<VOXELIN,VOXELOUT>::~KMeansLS()
{
    InitMask = NULL;
}

template <class VOXELIN,class VOXELOUT> void KMeansLS<VOXELIN,VOXELOUT>::SetLambda(VOXELOUT l1, VOXELOUT l2)
{
    lambda1 = l1;
    lambda2 = l2;
}
template <class VOXELIN,class VOXELOUT> void KMeansLS<VOXELIN,VOXELOUT>::SetInitBoundaryParams(size_t r, size_t g)
{
    initgap = g;

    if (r < initgap)
    {
	initradius = r;
    }
}

template <class VOXELIN,class VOXELOUT> void  KMeansLS<VOXELIN,VOXELOUT>::GetMeans (VOXELOUT & m1, VOXELOUT & m2)
{
    m2 = static_cast<VOXELOUT>(c2);
    m1 = static_cast<VOXELOUT>(c1);
}

template <class VOXELIN,class VOXELOUT> void KMeansLS<VOXELIN,VOXELOUT>::ComputeMeans()
{
    VOXELOUT * pom, * last;
    VOXELIN * puimage;
    pom = Output.GetVoxelAddr(0);
    last = Output.GetVoxelAddr(Output.GetImageSize());
    puimage = Input.GetVoxelAddr(0);
    lastc1sum = c1sum;
    lastc2sum = c2sum;
    c1 = 0;
    c2 = 0;
    c1sum = 0;
    c2sum = 0;
    while (pom != last)
    {
	c1sum +=  (*pom + 1);
	c2sum +=  (*pom - 1);
	c1 += (*pom + 1) * (*puimage);
	c2 += (*pom - 1) * (*puimage);
	++pom;
	++puimage;
    }
    
    if (c1sum != 0)
	c1 /= c1sum;
    if (c2sum != 0)
	c2 /= c2sum;
    
    if (c1 < c2)
    {
	double pom1;
	pom1 = c1;
	c1 = c2;
	c2 = pom1;
    pom = Output.GetVoxelAddr(0);
    while (pom != last)
    {
	*pom *= -1;
	++pom;
    }
    
    }

#ifdef I3D_DEBUG
    cout << "c1: "<<c1 << " c2 " << c2 << endl; 
       cout << "c1sum: "<<c1sum << " c2sum " << c2sum << endl; 
    cout << "Mean c1: "<<c1 << " Mean c2 " << c2 << endl; 
#endif
}

template <class VOXELIN,class VOXELOUT> void KMeansLS<VOXELIN,VOXELOUT>::ComputeChange()
{
    VOXELOUT RightHandSide;
    VOXELOUT * pom, * last;
    VOXELIN * puimage;
    pom = Output.GetVoxelAddr(0);
    last = Output.GetVoxelAddr(Output.GetImageSize());
    puimage =  Input.GetVoxelAddr(0);
//    char s[80];
 //   Image3d<GRAY8> img;
/*
    if (ElapsedIteration == 0)
    {
    if (Output.GetSizeZ() != 1) 
       sprintf(s,"data/out_%03d.i3d",ElapsedIteration);
   else
       sprintf(s,"out_%03d.tif",ElapsedIteration);
       
    FloatToGray(Output,img);
    img.SaveImage(s);
    }
*/
    ComputeMeans();       
    while (pom != last)
    {
	RightHandSide = -lambda1 * SQR((*puimage) - c1) 
	               + lambda2 * SQR((*puimage) - c2);
//	cout << "RHS: " << RightHandSide << endl;
/*	if (RightHandSide >= 0)
	    *pom = 1;
	else
	    *pom = -1;*/
	*pom = (RightHandSide >= 0 ? 1: -1);
	pom++;
	puimage++;
    }
/*
   if (Output.GetSizeZ() != 1) 
       sprintf(s,"data/out_%03d.i3d",ElapsedIteration+1);
   else
       sprintf(s,"out_%03d.tif",ElapsedIteration+1);
       
    FloatToGray(Output,img);
    img.SaveImage(s);
*/

}


template <class VOXELIN,class VOXELOUT> void KMeansLS<VOXELIN,VOXELOUT>::SetMask (Image3d<BINARY> * m)
{
    InitMask = m;
}


template < class VOXELIN, class VOXELOUT > void KMeansLS < VOXELIN,
    VOXELOUT >::SetThreshold(VOXELIN t)
{
    Thres = t;
}


template < class VOXELIN, class VOXELOUT > void KMeansLS < VOXELIN,
    VOXELOUT >::Initialize()
{
    VOXELOUT *pom, *last;
    pom = Output.GetVoxelAddr(0);
    last = Output.GetVoxelAddr(Output.GetImageSize());
    if (Thres > 0)
    {
#ifdef I3D_DEBUG
	cout << "Using Threshold\n";
#endif

        VOXELIN *pin;
        pin = Input.GetVoxelAddr(0);
        while (pom != last)
        {
            if (*pin < Thres)
                *pom = InitLow;
            else
                *pom = InitHigh;
            ++pom;
            ++pin;
        }
	return;
    }
    if (InitMask == NULL)
    {
#ifdef I3D_DEBUG
	cout << "Using circles\n";
#endif
        Image3d < VOXELOUT > onecircle;
        VOXELOUT *p, *last, *srcp;
        size_t sx, sy, sz, ind[3], dim, radius, gap, halfgap, sum, i, x, y, z;
        sx = Output.GetSizeX();
        sy = Output.GetSizeY();
        sz = Output.GetSizeZ();
/*sum = (std::min(sx,sy));
	sum = (sz > 1?std::min(sum,sz):sum);
	if (sum < 30)
	{
	    gap = sum;
	}
	else
	{
            gap = 30;
	}
	radius = (size_t)(((float)gap) / 2.8f) ;*/
	radius = static_cast<size_t>(initradius);
	gap = static_cast<size_t>(initgap);
	radius *= radius;

	
	halfgap = gap/2;

        dim = (sz==1?2:3);

        if (dim == 2)
            onecircle.MakeRoom(gap, gap, 1);
        else
            onecircle.MakeRoom(gap, gap, gap);

        p = onecircle.GetVoxelAddr(0);
        last = onecircle.GetVoxelAddr(onecircle.GetImageSize());
        ind[0] = ind[1] = ind[2] = 0;
        while (p != last)
        {
            sum = 0;
            for (i = 0; i < dim; ++i)
                sum += (halfgap - ind[i]) * (halfgap - ind[i]);
            if (sum <= radius)
                *p = InitHigh;
            else
                *p = InitLow;
            ++p;
            ++ind[0];
            ind[0] %= gap;
            if (ind[0] == 0)
            {
                ++ind[1];
                ind[1] %= gap;
            }
            if (dim == 3)
                if ((ind[0] == 0) && (ind[1] == 0))
                    ++ind[2];
        }
        srcp = onecircle.GetVoxelAddr(0);
        p = Output.GetVoxelAddr(0);
        size_t pomy, pomz;
        for (z = 0; z < sz; ++z)
        {
            pomz = (z % gap) * (gap * gap);
            for (y = 0; y < sy; ++y)
            {
                pomy = (y % gap) * gap + pomz;
                last = p;
                for (x = 0; x < sx; x += gap, p += gap)
                {
                    // *p =  onecircle.GetVoxel(x%gap,y%gap,z%gap);
                    memcpy((void *) p, (void *) (srcp + pomy),
                           sizeof(VOXELOUT) * std::min(gap, sx - x));
//            *p =  *(srcp + (x%gap + ((y%gap) * gap) +((z%gap)*(gap*gap))));
                    //    ++p;
                }
                p = last + sx;
            }

        }
    } else
    {
#ifdef I3D_DEBUG
	cout << "Using mask\n";
#endif
        BINARY *bp;
        bp = InitMask->GetVoxelAddr(0);
        while (pom != last)
        {
            if (*bp == numeric_limits < BINARY >::max())
                *pom = InitHigh;
            else
                *pom = InitLow;
            ++pom;
            ++bp;
        }
    }
}

template <class VOXELIN,class VOXELOUT> bool KMeansLS<VOXELIN,VOXELOUT>::Convergence()
{
#ifdef I3D_DEBUG
    cout << c1sum << " " << lastc1sum << endl;
#endif
    if (ElapsedIteration < MaximumIteration)
	return ((c1sum == lastc1sum)&&(c2sum == lastc2sum));
    else
	return true;
}

template <class VOXELIN,class VOXELOUT> void KMeansLS<VOXELIN,VOXELOUT>::SetInitValues(VOXELOUT l, VOXELOUT h)
{
    InitLow = l;
    InitHigh = h;
}



/****************************************************
 *
 * Threshold Dynamics for Mumford shah functional algorithm  classes implementation
 *
*****************************************************/
#ifdef WITH_LAPACK

template <class VOXELIN,class VOXELOUT> ThresholdActiveContours<VOXELIN,VOXELOUT>::ThresholdActiveContours(Image3d<VOXELIN>& in, Image3d<VOXELOUT> & out):
KMeansLS<VOXELIN,VOXELOUT>(in,out), InnerSteps(3)
{
    SetSpacing(ComputeNormedSpacing(in));
    this->SetInitValues(0,1);
    SetGlobalTimeStep(5.0);
    ComputeTauLambda();
    Buffer.resize(Output.GetImageSize());
    WithThreshold = true;
    lastVolume = 0;
    Volume = 0;

}

template <class VOXELIN,class VOXELOUT> ThresholdActiveContours<VOXELIN,VOXELOUT>::~ThresholdActiveContours()
{
}

template <class VOXELIN,class VOXELOUT> void ThresholdActiveContours<VOXELIN,VOXELOUT>::SetWithThreshold(bool t)
{
    WithThreshold = t;
}

template <class VOXELIN,class VOXELOUT> void ThresholdActiveContours<VOXELIN,VOXELOUT>::ComputeTauLambda()
{
    TauLambda = (GlobalTimeStep * lambda1) / (sqrt(M_PI * GlobalTimeStep * InnerSteps));
    ComputeCConst(); 
}

template <class VOXELIN,class VOXELOUT> void ThresholdActiveContours<VOXELIN,VOXELOUT>::ComputeCConst()
{

    VOXELOUT * pom, * last;
    VOXELIN * puimage;
    double max = std::numeric_limits<double>::min();
    
    last = Output.GetVoxelAddr(Output.GetImageSize());
    puimage = Input.GetVoxelAddr(0);
    pom = Output.GetVoxelAddr(0);

    if (c1 != c2)
    {
	while (pom != last)
	{
	    max = std::max(max,TauLambda * ( 
			(*pom) * SQR(c1 - (*puimage))  
			+ ((*pom)-1.0f) * SQR(c2 - (*puimage))
			));
	    ++pom;
	    ++puimage;
	}
	CConst =1.0*static_cast<double>(max);
    }
}



template <class VOXELIN,class VOXELOUT> void ThresholdActiveContours<VOXELIN,VOXELOUT>::SetInnerSteps(size_t s)
{
    InnerSteps = s;
    ComputeTauLambda();
}

template <class VOXELIN,class VOXELOUT> void ThresholdActiveContours<VOXELIN,VOXELOUT>::SetGlobalTimeStep(VOXELOUT t) 
{
    GlobalTimeStep = t;
    ComputeTauLambda();
    
}

template <class VOXELIN,class VOXELOUT> void ThresholdActiveContours<VOXELIN,VOXELOUT>::SetLambda(VOXELOUT l)
{
    lambda1 = l;
//    TauLambda = (GlobalTimeStep * lambda1);
    ComputeTauLambda();
}


template <class VOXELIN,class VOXELOUT> void ThresholdActiveContours<VOXELIN,VOXELOUT>::Threshold(VOXELOUT t)
{
    VOXELOUT * op, * last;
    op = Output.GetVoxelAddr(0);
    last = Output.GetVoxelAddr(Output.GetImageSize());
//    cout << InitLow << " " << InitHigh << endl;
    while (op != last)
    {
	//cout << "*op: " << *op << " threshold: " << t << "InitLow: " << InitLow << " InitHigh" << InitHigh << endl;
	*op = (*op < t ? InitLow : InitHigh);
	++op;
    }
}

template <class VOXELIN,class VOXELOUT> void ThresholdActiveContours<VOXELIN,VOXELOUT>::ComputeMeans()
{
    VOXELOUT * pom, * last;
    VOXELIN * puimage;
    pom = Output.GetVoxelAddr(0);
    last = Output.GetVoxelAddr(Output.GetImageSize());
    puimage = Input.GetVoxelAddr(0);
    lastc1sum = c1sum;
    lastc2sum = c2sum;
    c1 = 0;
    c2 = 0;
    c1sum = 0;
    c2sum = 0;
    while (pom != last)
    {
	c1sum +=  (*pom);
	c2sum +=  (*pom - InitHigh);
	c1 += (*pom ) * (*puimage);
	c2 += (*pom - InitHigh) * (*puimage);
	++pom;
	++puimage;
    }

#ifdef I3D_DEBUG    
    cout << "c1sum: "<<c1sum << " c2sum " << c2sum << endl; 
#endif

    if (c1sum != 0)
	c1 /= c1sum;
    if (c2sum != 0)
	c2 /= c2sum;
    
    if (c1 < c2)
    {
	double pom1;
	pom1 = c1;
	c1 = c2;
	c2 = pom1;

	pom1  = c1sum;
	c1sum = c2sum;
	c2sum = pom1;
	c1sum *= -1;
	c2sum *= -1;
    pom = Output.GetVoxelAddr(0);
    
    while (pom != last)
    {
	*pom -= InitHigh;
	*pom *= -1;
	++pom;
    }
    
    }
    
    
/*
    pom = Output.GetVoxelAddr(0);
    puimage = Input.GetVoxelAddr(0);
    sigma1 = 0;
    sigma2 = 0;
    while (pom != last)
    {
	sigma1 += (*pom ) * SQR(*puimage - c1);
	sigma2 += (*pom - InitHigh) * SQR(*puimage - c2);
	++pom;
	++puimage;
    }
    if (c1sum != 0)
	sigma1 /= c1sum;
     if (c2sum != 0)
	sigma2 /= c2sum;
  */  
    
#ifdef I3D_DEBUG
     
       cout << "c1sum: "<<c1sum << " c2sum " << c2sum << endl; 
    cout << "Mean c1: "<<c1 << " Mean c2 " << c2 << endl; 
//    cout << "sigma1: "<<sigma1 << " sigma2 " << sigma2 << endl; 
#endif
}

//#define C_CONST (lambda1 * GlobalTimeStep / 20.0)

//#define TD_AOS
#define TD_LOD
//#define TD_AFI
//#define TD_MCM

template <class VOXELIN, class VOXELOUT> void ThresholdActiveContours<VOXELIN,VOXELOUT>::RightHandSide(valarray<VOXELIN> & rf, valarray<VOXELOUT> & ru, size_t maxi)
{
#ifndef TD_MCM
   
    VOXELOUT WorkA, WorkB;
    VOXELOUT * Pru =&(ru[0]);
    VOXELOUT * last = &(ru[maxi-1]) + 1;
    VOXELIN * Prf = &(rf[0]);
    while (Pru != last)
    {
//	cout << "Pru before: " << *Pru << endl; Tmp = *Pru;
	WorkB = SQR(c2 - static_cast<VOXELOUT>(* Prf));
//	WorkB = fabs(c2 - static_cast<VOXELOUT>(* Prf));
//	WorkB = -log(1.0/(sqrt(M_PI * 2.0) * sigma2))*exp( - SQR(c2 - static_cast<VOXELOUT>(* Prf)));
//	cout << "SQR(c_2 - f): " << WorkB << endl;
	WorkB *= TauLambda;
//	cout << "TauLambda*SQR(c_2 - f): " << WorkB << endl;
	WorkA = SQR(c1 - static_cast<VOXELOUT>(* Prf)); 
//	WorkA = fabs(c1 - static_cast<VOXELOUT>(* Prf)); 
//	WorkA = -log(1.0/(sqrt(M_PI * 2.0) * sigma1)) * exp (-SQR(c1 - static_cast<VOXELOUT>(* Prf))); 
//	cout << "SQR(c_1 - f): " << WorkA << endl;
	WorkA *= TauLambda;
//	cout << "-TauLambda*SQR(c_1 - f): " << WorkA << endl;
	WorkA += WorkB;
//	cout << "-TauLambda*SQR(c_1 - f) -TauLambda*SQR(c_2 - f): " << WorkA << endl;
//	cout << "A(x)*u+B(x): " <<  WorkA * (*Pru) + WorkB << endl;
	*Pru = *Pru * (1.0  +CConst - WorkA) + WorkB;
//	cout << "Pru before and after: "<< Tmp << " " << *Pru << endl << endl;
	++Pru;
	++Prf;
    }        
#endif
}


template <class VOXELIN, class VOXELOUT> void ThresholdActiveContours<VOXELIN,VOXELOUT>::RightHandSideY(valarray<VOXELIN> & rf, valarray<VOXELOUT> & ru, size_t maxi, size_t offsetY)
{
   
    VOXELOUT WorkA, WorkB;
    VOXELOUT * Pru =&(ru[0]);
    VOXELOUT * last = &(ru[maxi-1]) + 1;
    VOXELIN * Prf = &(rf[0]);

    VOXELOUT r = (GlobalTimeStep / SQR(Spacing[0]));
    valarray<VOXELOUT> ruBuffer(maxi);
    
    ruBuffer = 0;
    for (size_t i =  0; i < offsetY; ++i)
    {

	ruBuffer[i] = r*(-ru[i]  + ru[i + offsetY]);

    }
    for (size_t i = offsetY; i < maxi-offsetY; ++i)
    {
	ruBuffer[i] = r*(-2.0*ru[i]  + ru[i + offsetY] + ru[i - offsetY]);
    }
    for (size_t i =  maxi - offsetY; i < maxi; ++i)
    {
	ruBuffer[i] = r*(-ru[i]  + ru[i - offsetY]);
    }

#ifndef TD_MCM
    while (Pru != last)
    {
	WorkB = SQR(c2 - static_cast<VOXELOUT>(* Prf));
	WorkB *= TauLambda;
	WorkA = SQR(c1 - static_cast<VOXELOUT>(* Prf)); 
	WorkA *= TauLambda;
	WorkA += WorkB;
	*Pru = *Pru * (1.0  +CConst - WorkA) + WorkB;
	++Pru;
	++Prf;
    }       
#endif
    ru += ruBuffer;
}
 
template <class VOXELIN, class VOXELOUT> void ThresholdActiveContours<VOXELIN,VOXELOUT>::RightHandSideX(valarray<VOXELIN> & rf, valarray<VOXELOUT> & ru,  size_t maxi, size_t sizeX, size_t sizeY)
{

    VOXELOUT r = (GlobalTimeStep / SQR(Spacing[1]));
    float d = (1.0f+CConst);
    valarray<VOXELOUT> ruBuffer(maxi);
    r /= d;
    ruBuffer = 0;
    for (size_t j =  0; j < sizeY; ++j)
    {
	ruBuffer[j*sizeX] =  r*(-ru[j*sizeX ] + ru[j*sizeX  + 1]);
	for (size_t i = 1 ; i < sizeX-1; ++i)
	{
	    ruBuffer[j*sizeX+i] =  r*(-2.0*ru[j*sizeX + i]  + ru[j*sizeX + i - 1] + ru[j*sizeX + i + 1]);
	}
	ruBuffer[(j+1)*sizeX-1] =  r*(-ru[(j+1)*sizeX -1]  + ru[(j+1)*sizeX -2]);
    }
    ru += ruBuffer;
    
}
 

template <class VOXELIN, class VOXELOUT> void ThresholdActiveContours<VOXELIN,VOXELOUT>::PrepareDiagonals(int dim) 
{
    	VOXELOUT r = (GlobalTimeStep / SQR(Spacing[dim]));
        Vector3d < size_t > size;
	size.x = Output.GetSizeX();
        size.y = Output.GetSizeY();
        size.z = Output.GetSizeZ();
	
        SDiag.resize(size[dim] - 1);
        Diag.resize(size[dim]);
	
        PSDiag.resize(size[dim] - 1);
        PDiag.resize(size[dim]);
	
/* AOS scheme */
#ifdef TD_AOS
//	cout << "r: " << r << " -> " ;
	r *= SQR(Dimensions); 
//	cout << r << endl;
	PSDiag = -r;
        PDiag = 2 * r;
        PDiag[0] -= r;
        PDiag[size[dim] - 1] -= r;
	PDiag += Dimensions*(1+CConst);
#endif
	
/* LOD scheme */
#ifdef TD_LOD

	if (dim != 0)
	{
	    float d = (1.0f+CConst);
	    PSDiag = -r/d;
	    PDiag = 1 + (2 * r)/d;
	    PDiag[0] -= (r/d);
	    PDiag[size[dim] - 1] -= (r/d);
	}else
	{
	    PSDiag = -r;
	    PDiag = 1 + 2 * r + CConst;
	    PDiag[0] -= r;
	    PDiag[size[dim] - 1] -= r;
	}
#endif
	
#ifdef TD_AFI
/*	cout << "-r: " << -r << endl;
	cout << "1+2r: " << 1+2*r << endl;
*/
        PSDiag = -r;
        PDiag = 1 + 2 * r +  CConst;
        PDiag[0] -= r;
        PDiag[size[dim] - 1] -= r;
/*	cout << "DiagTest: " << PDiag[3] << endl;
	PSDiag *= 2;
	PDiag *=2;
	cout << PDiag[3] << endl;
*/
#endif	

	Diag = PDiag;
	SDiag = PSDiag;
}


#ifdef TD_AOS 
template <class VOXELIN,class VOXELOUT> void ThresholdActiveContours<VOXELIN,VOXELOUT>::ComputeChange()
{
    size_t x, z, i, j, start, imageSize;
    Vector3d < size_t > size;
    valarray < VOXELOUT > rowU;
    valarray < VOXELIN > rowF;
    valarray < VOXELOUT > &OutputDataArray = Output.GetVoxelData();
    valarray < VOXELIN > &InputDataArray = Input.GetVoxelData();
    int rad, vec = 1, info;
    size.x = Output.GetSizeX();
    size.y = Output.GetSizeY();
    size.z = Output.GetSizeZ();
    imageSize = size.x * size.y * size.z;
    char s[80];
    Image3d<GRAY8> img;

//#ifdef I3D_DEBUG
    cout << "Using AOS scheme!\n";
    cout << "TauLambda: " << TauLambda << endl;
    cout << "Spacing: " << Spacing << endl;
    cout << "Size: " << size<< endl;
    cout << "Dimensions: " << Dimensions<< endl;
    cout << "CConst: " << CConst << endl;
//#endif

#ifdef TD_MCM
    lambda1 = 0;
    ComputeTauLambda();
#endif
   
    if (ElapsedIteration == 0)
    {

	if (size.z != 1) 
	    sprintf(s,"data/out_%03d.i3d",ElapsedIteration);
	else
	    sprintf(s,"out_%03d.tif",ElapsedIteration);
	FloatToGray(Output,img);
	img.SaveImage(s);
//	sprintf(s,"1out_%03d.mhd",ElapsedIteration);
//	Output.SaveImage(s);
    }
    ComputeMeans();
    
    for (size_t steps = 0; steps < InnerSteps; ++steps)
    {

	Buffer = 0.0;
	RightHandSide(InputDataArray,OutputDataArray,OutputDataArray.size());
        for (j = 0; j < Dimensions; ++j)
        {
            PrepareDiagonals(j);
            rad = size[j];
            rowU.resize(size[j]);
            rowF.resize(size[j]);

           if (j == 0)
            {
//		cout << "Updating X AXE:\n";
                for (i = 0; i < imageSize / size.x; ++i)
                {
                    start = i * size.x;
                    rowU = OutputDataArray[slice(start, size.x, 1)];
//                    rowF = InputDataArray[slice(start, size.x, 1)];
//                    RightHandSide(rowF, rowU, size.x);

                    sptsv_(rad, vec, &Diag[0], &SDiag[0], &rowU[0], rad, info);
#ifdef I3D_DEBUG              
                    if (info != 0)
                        cerr <<
                            "TD X axe: Failed to find the solution of system of linear equations\n";
#endif  
                    UpdateRow(Buffer, rowU, slice(start, size.x, 1));
                    Diag = PDiag;
                    SDiag = PSDiag;

                }
            }
	    else 
            if (j == 1)
            {
//		cout << "Updating Y AXE:\n";
                for (z = 0; z < size.z; ++z)
                {
                    for (x = 0; x < size.x; ++x)
                    {
                        start = (z * size.x * size.y) + x;
                        rowU = OutputDataArray[slice(start, size.y, size.x)];
  //                      rowF = InputDataArray[slice(start, size.y, size.x)];
                      //  RightHandSide(rowF, rowU, size.y);

                        sptsv_(rad, vec, &Diag[0], &SDiag[0], &rowU[0], rad,info);
#ifdef I3D_DEBUG
                        if (info != 0)
                            cerr <<
                                "TD Y axe: Failed to find the solution of system of linear equations\n";
#endif
                        UpdateRow(Buffer, rowU,slice(start, size.y, size.x));
                        Diag = PDiag;
                        SDiag = PSDiag;

                    }
                }
            } 

	

	    else
		
            {
//		cout << "Updating Z AXE:\n";
                int sizexy = size.x * size.y;
                for (i = 0; i < imageSize / size.z; ++i)
                {

                    rowU = OutputDataArray[slice(i, size.z, sizexy)];
//		    rowF = InputDataArray[slice(i, size.z, sizexy)];
  //                  RightHandSide(rowF, rowU, size.z);


                    sptsv_(rad, vec, &Diag[0], &SDiag[0], &rowU[0], rad, info);

#ifdef I3D_DEBUG
                    if (info != 0)
                        cerr <<
                            "LOD Z axe: Failed to find the solution of system of linear equations\n";
#endif
		    UpdateRow(Buffer, rowU,slice(i, size.z, sizexy));
                    Diag = PDiag;
                    SDiag = PSDiag;
                }
            }
	    
	    
        }
	OutputDataArray = Buffer;
    }
   if (WithThreshold )
   {
   Threshold(InitHigh/2.0);
   }
   if (size.z != 1) 
       sprintf(s,"data/out_%03d.i3d",ElapsedIteration+1);
   else
       sprintf(s,"out_%03d.tif",ElapsedIteration+1);
     
    FloatToGray(Output,img);
    img.SaveImage(s);
    
 /*   
	sprintf(s,"1out_%03d.mhd",ElapsedIteration+1);
	Output.SaveImage(s);
*/
}
#endif
template <class VOXELIN,class VOXELOUT> void ThresholdActiveContours<VOXELIN,VOXELOUT>::CallLinearSystemSolver(int rad, int vec, VOXELOUT * Diag, VOXELOUT * SDiag, VOXELOUT * rowU,  int info)
{
}
template <> void ThresholdActiveContours<float,float>::CallLinearSystemSolver(int rad, int vec, float * Diag, float * SDiag, float * rowU,  int info)
{
        sptsv_(rad, vec, Diag, SDiag, rowU, rad, info);
}
template <> void ThresholdActiveContours<double,double>::CallLinearSystemSolver(int rad, int vec, double * Diag, double * SDiag, double * rowU,  int info)
{
        dptsv_(rad, vec, Diag, SDiag, rowU, rad, info);
}




#ifdef TD_LOD
template <class VOXELIN,class VOXELOUT> void ThresholdActiveContours<VOXELIN,VOXELOUT>::ComputeChange()
{
    size_t x, z, i, j, start, imageSize;
    Vector3d < size_t > size;
    valarray < VOXELOUT > rowU;
    valarray < VOXELIN > rowF;
    valarray < VOXELOUT > &OutputDataArray = Output.GetVoxelData();
    valarray < VOXELIN > &InputDataArray = Input.GetVoxelData();
    int rad, vec = 1, info = 0;
    size.x = Output.GetSizeX();
    size.y = Output.GetSizeY();
    size.z = Output.GetSizeZ();
    imageSize = size.x * size.y * size.z;
//    char s[80];
    Image3d<GRAY8> img;


#ifdef TD_MCM
    lambda1 = 0;
    ComputeTauLambda();
#endif

    if (ElapsedIteration == 0)
    {
/*	if (size.z != 1) 
	    sprintf(s,"1out_%03d.ics",(int)ElapsedIteration);
	else
	    sprintf(s,"1out_%03d.tif",(int)ElapsedIteration);
	FloatToGray(Output,img);
	img.SaveImage(s);*/
//	sprintf(s,"1out_%03d.mhd",ElapsedIteration);
//	Output.SaveImage(s);
    }
   
    ComputeMeans();

    if (ElapsedIteration % 10 == 0)
    {
	ComputeCConst();
    }

#ifdef I3D_DEBUG
    cout << "Using LOD scheme!\n";
    cout << "TauLambda: " << TauLambda << endl;
    cout << "Spacing: " << Spacing << endl;
    cout << "Size: " << size<< endl;
    cout << "Dimensions: " << Dimensions<< endl;
    cout << "CConst: " << CConst << endl;
#endif
    
    for (size_t steps = 0; steps < InnerSteps; ++steps)
    {
        for (j = 0; j < Dimensions; ++j)
        {
            PrepareDiagonals(j);
            rad = size[j];
            rowU.resize(size[j]);
            rowF.resize(size[j]);

           if (j == 0)
            {
		RightHandSide(InputDataArray,OutputDataArray,OutputDataArray.size());
//		RightHandSideY(InputDataArray,OutputDataArray,OutputDataArray.size(), Output.GetSizeX());
//		RightHandSideX(InputDataArray,OutputDataArray,OutputDataArray.size(), Output.GetSizeX(),Output.GetSizeY());
//		OutputDataArray *= (1.0f+CConst);
		Buffer = OutputDataArray;
//		cout << "Updating X AXE:\n";
                for (i = 0; i < imageSize / size.x; ++i)
                {
                    start = i * size.x;
                    rowU = OutputDataArray[slice(start, size.x, 1)];
//                    rowF = InputDataArray[slice(start, size.x, 1)];
//                    RightHandSide(rowF, rowU, size.x);

                    CallLinearSystemSolver(rad, vec, &(Diag[0]), &(SDiag[0]), &(rowU[0]),  info);
#ifdef I3D_DEBUG              
                    if (info != 0)
                        cerr <<
                            "TD X axe: Failed to find the solution of system of linear equations\n";
#endif  
                    MultiplyRow(OutputDataArray, rowU, slice(start, size.x, 1));
                    Diag = PDiag;
                    SDiag = PSDiag;

                }
            }
	    else 
            if (j == 1)
            {
//		cout << "Updating Y AXE:\n";
//		RightHandSideX(InputDataArray,OutputDataArray,OutputDataArray.size(), Output.GetSizeX(),Output.GetSizeY());
//		RightHandSideY(InputDataArray,OutputDataArray,OutputDataArray.size(), Output.GetSizeX());
                for (z = 0; z < size.z; ++z)
                {
                    for (x = 0; x < size.x; ++x)
                    {
                        start = (z * size.x * size.y) + x;
                        rowU = OutputDataArray[slice(start, size.y, size.x)];
  //                      rowF = InputDataArray[slice(start, size.y, size.x)];
                      //  RightHandSide(rowF, rowU, size.y);

                     CallLinearSystemSolver(rad, vec, &(Diag[0]), &(SDiag[0]), &(rowU[0]), info);
#ifdef I3D_DEBUG
                        if (info != 0)
                            cerr <<
                                "TD Y axe: Failed to find the solution of system of linear equations\n";
#endif
                        MultiplyRow(OutputDataArray, rowU,slice(start, size.y, size.x));
                        Diag = PDiag;
                        SDiag = PSDiag;
                    }
                }
            } 

	

	    else
		
            {
//		cout << "Updating Z AXE:\n";
                int sizexy = size.x * size.y;
                for (i = 0; i < imageSize / size.z; ++i)
                {

                    rowU = OutputDataArray[slice(i, size.z, sizexy)];
//		    rowF = InputDataArray[slice(i, size.z, sizexy)];
  //                  RightHandSide(rowF, rowU, size.z);


                    CallLinearSystemSolver(rad, vec, &(Diag[0]), &(SDiag[0]), &(rowU[0]), info);

#ifdef I3D_DEBUG
                    if (info != 0)
                        cerr <<
                            "LOD Z axe: Failed to find the solution of system of linear equations\n";
#endif
                    MultiplyRow(OutputDataArray, rowU,slice(i, size.z, sizexy));
                    Diag = PDiag;
                    SDiag = PSDiag;
                }
            }
	    
/*		    for (size_t ind = 0 ; ind < Buffer.size(); ++ind)
	    {
		OutputDataArray[ind] = 0.5*(OutputDataArray[ind] +(1.0f/CConst) *Buffer[ind]);
	    }
*/
    
        }
    }
/* if (size.z != 1) 
       sprintf(s,"1out_%03dbt.ics",ElapsedIteration+1);
   else
       sprintf(s,"1out_%03dbt.tif",ElapsedIteration+1);
     FloatToGray(Output,img);
    img.SaveImage(s);
  */
   if (WithThreshold )
   {
   Threshold(InitHigh/2.0);
   }

/*   if (size.z != 1) 
       sprintf(s,"1out_%03d.ics",ElapsedIteration+1);
   else
       sprintf(s,"1out_%03d.tif",ElapsedIteration+1);
     
    FloatToGray(Output,img);
    img.SaveImage(s);*/
 /*   
	sprintf(s,"1out_%03d.mhd",ElapsedIteration+1);
	Output.SaveImage(s);
*/
}
#endif

#ifdef TD_AFI
template <class VOXELIN,class VOXELOUT> void ThresholdActiveContours<VOXELIN,VOXELOUT>::ComputeChange()
{
    size_t x, z, i, j, start, imageSize;
    Vector3d < size_t > size;
    valarray < VOXELOUT > rowU;
    valarray < VOXELIN > rowF;
    valarray < VOXELOUT > &OutputDataArray = Output.GetVoxelData();
    valarray < VOXELIN > &InputDataArray = Input.GetVoxelData();
    int rad, vec = 1, info;
    size.x = Output.GetSizeX();
    size.y = Output.GetSizeY();
    size.z = Output.GetSizeZ();
    imageSize = size.x * size.y * size.z;
    char s[80];
    Image3d<GRAY8> img;

//#ifdef I3D_DEBUG
    cout << "TauLambda: " << TauLambda << endl;
    cout << "Spacing: " << Spacing << endl;
    cout << "Size: " << size<< endl;
    cout << "Dimensions: " << Dimensions<< endl;
    cout << "CConst: " << CConst << endl;
//#endif
 #ifdef TD_MCM
    lambda1 = 0;
    ComputeTauLambda();
#endif
    
    if (ElapsedIteration == 0)
    {

	if (size.z != 1) 
	    sprintf(s,"data/out_%03d.i3d",ElapsedIteration);
	else
	    sprintf(s,"out_%03d.tif",ElapsedIteration);
	FloatToGray(Output,img);
	img.SaveImage(s);
/*	sprintf(s,"1out_%03d.mhd",ElapsedIteration);
	Output.SaveImage(s);*/
    }
    
    ComputeMeans();

    for (size_t steps = 0; steps < InnerSteps; ++steps)
    {

//	OutputDataArray *= 0.5;
	Buffer = OutputDataArray;
	j = 0;
            PrepareDiagonals(j);
            rad = size[j];
            rowU.resize(size[j]);
            rowF.resize(size[j]);

            if (j == 0)
            {
		cout << "Updating X AXE:\n";
                for (i = 0; i < imageSize / size.x; ++i)
                {
                    start = i * size.x;
                    rowU = OutputDataArray[slice(start, size.x, 1)];
                    rowF = InputDataArray[slice(start, size.x, 1)];
		    RightHandSide(rowF, rowU, size.x);
                    sptsv_(rad, vec, &Diag[0], &SDiag[0], &rowU[0], rad, info);
#ifdef I3D_DEBUG              
                    if (info != 0)
                        cerr <<
                            "LOD X axe: Failed to find the solution of system of linear equations\n";
#endif              
//                    UpdateRow(Buffer, rowU, slice(start, size.x, 1));
                    MultiplyRow(OutputDataArray, rowU, slice(start, size.x, 1));
                    Diag = PDiag;
                    SDiag = PSDiag;

                }
		j = 1;
            PrepareDiagonals(j);
            rad = size[j];
            rowU.resize(size[j]);
            rowF.resize(size[j]);

		for (z = 0; z < size.z; ++z)
                {
                    for (x = 0; x < size.x; ++x)
                    {
                        start = (z * size.x * size.y) + x;
                        rowU = OutputDataArray[slice(start, size.y, size.x)];
                        rowF = InputDataArray[slice(start, size.y, size.x)];
                        RightHandSide(rowF, rowU, size.y);

                        sptsv_(rad, vec, &Diag[0], &SDiag[0], &rowU[0], rad,info);
#ifdef I3D_DEBUG
                        if (info != 0)
                            cerr <<
                                "LOD Y axe: Failed to find the solution of system of linear equations\n";
#endif
//                        UpdateRow(Buffer, rowU,slice(start, size.y, size.x));
                        MultiplyRow(OutputDataArray, rowU,slice(start, size.y, size.x));
                        Diag = PDiag;
                        SDiag = PSDiag;

                    }
                }
	    }
            if (j == 1)
            {
		cout << "Updating Y AXE:\n";
                for (z = 0; z < size.z; ++z)
                {
                    for (x = 0; x < size.x; ++x)
                    {
                        start = (z * size.x * size.y) + x;
                        rowU = Buffer[slice(start, size.y, size.x)];
                        rowF = InputDataArray[slice(start, size.y, size.x)];
                        RightHandSide(rowF, rowU, size.y);

                        sptsv_(rad, vec, &Diag[0], &SDiag[0], &rowU[0], rad,info);
#ifdef I3D_DEBUG
                        if (info != 0)
                            cerr <<
                                "LOD Y axe: Failed to find the solution of system of linear equations\n";
#endif
//                        UpdateRow(Buffer, rowU,slice(start, size.y, size.x));
                        MultiplyRow(Buffer, rowU,slice(start, size.y, size.x));
                        Diag = PDiag;
                        SDiag = PSDiag;

                    }
                }

		j = 0;
            PrepareDiagonals(j);
            rad = size[j];
            rowU.resize(size[j]);
            rowF.resize(size[j]);


                for (i = 0; i < imageSize / size.x; ++i)
                {
                    start = i * size.x;
                    rowU = Buffer[slice(start, size.x, 1)];
                    rowF = InputDataArray[slice(start, size.x, 1)];
		    RightHandSide(rowF, rowU, size.x);
                    sptsv_(rad, vec, &Diag[0], &SDiag[0], &rowU[0], rad, info);
#ifdef I3D_DEBUG              
                    if (info != 0)
                        cerr <<
                            "LOD X axe: Failed to find the solution of system of linear equations\n";
#endif              
//                    UpdateRow(Buffer, rowU, slice(start, size.x, 1));
                    MultiplyRow(Buffer, rowU, slice(start, size.x, 1));
                    Diag = PDiag;
                    SDiag = PSDiag;
		}

            } 
	    for (size_t ind = 0 ; ind < Buffer.size(); ++ind)
	    {
		OutputDataArray[ind] = 0.5*(OutputDataArray[ind] + Buffer[ind]);
	    }
    }
   if (WithThreshold )
   {
   Threshold(InitHigh/2.0);
   }
   if (size.z != 1) 
       sprintf(s,"data/out_%03d.i3d",ElapsedIteration+1);
   else
       sprintf(s,"out_%03d.tif",ElapsedIteration+1);
     
    FloatToGray(Output,img);
    img.SaveImage(s);
    /*
	sprintf(s,"1out_%03d.mhd",ElapsedIteration+1);
	Output.SaveImage(s);
*/
}
#endif

template <class VOXELIN,class VOXELOUT> bool ThresholdActiveContours<VOXELIN,VOXELOUT>::Enough (Image3d<VOXELOUT> & input, bool WithThreshold)
{
    size_t minimum = std::numeric_limits<size_t>::max();
    if (WithThreshold)
    {
    Image3d<GRAY8> i;
    LabeledImage3d<size_t, GRAY8> li;
    ComponentInfo<GRAY8>::Container::const_iterator it;

    FloatToGray(input,i);
    li.CreateRegionsFF(i);
#ifdef I3D_DEBUG
    cout << "Number of FOREGROUND COMPONENTS: "<< li.NumberOfComponents() - 1 << endl;
#endif
    Volume = 0;
    it = li.components.begin();
    ++it;
    while (it  != li.components.end())
    {
	minimum = it->second.volume;
	Volume = Volume + minimum;
	++it;
    }
    bool rv;
    //cout << "lastVolume: " << lastVolume << "\nVolume: " << Volume <<  "\n input.GetImageSize()/1000: " <<  ((float)(std::max(lastVolume, Volume) - std::min(lastVolume,Volume))) / 100.0f << endl;
    //cout << endl<< std::max(lastVolume, Volume) - std::min(lastVolume,Volume) << endl << endl;
    if ( ((float)(std::max(lastVolume, Volume) - std::min(lastVolume,Volume))) / 100.0f < 1.0f)
	rv =  true;
    else
	rv = false;
    lastVolume = Volume;
    return rv;
    }
    else return false;
return false;
}

template <class VOXELIN,class VOXELOUT> bool ThresholdActiveContours<VOXELIN,VOXELOUT>::Convergence()
{
    if (ElapsedIteration < MaximumIteration)
//	return false;
	//return ((((c1sum == lastc1sum)&&(c2sum == lastc2sum))));// 
	return Enough(Output,WithThreshold);
    else
	return true;
}





/****************************************************
 *
 * AOSChanVese class implementation
 *
*****************************************************/

/*
template < class VOXELIN, class VOXELOUT > void AOSChanVese < VOXELIN,VOXELOUT >::ComputeDiffusivity ()
{
  VOXELOUT *pom, *last;
  GradMag->ComputeUpdateData();
  pom = &((*GradMagData)[0]);
  last = &((*GradMagData)[Output.GetImageSize ()]);
  if (Exponent == 1.0)
    {
      while (pom != last)
	{
	  *pom = 1.0 / (sqrt ((*pom) + Beta));
	  pom++;
	}
    }
  else if (Exponent == 2.0)
    {
//	VOXEL B2 = SQR(Beta);
      while (pom != last)
	{
	  *pom = 1.0 / ((*pom) + Beta);
	  pom++;
	}
    }
  else
    {
//	VOXEL B2 = SQR(Beta);
      while (pom != last)
	{
	  *pom = 1.0 / (powf ((*pom) + Beta, Exponent / 2.0) + MINDIVIDER);
	  pom++;
	}

    }
}

template < class VOXELIN, class VOXELOUT > void AOSChanVese < VOXELIN,VOXELOUT >::Initialize()
{
    size_t x, y, z, index,i;
    Vector3d<size_t> size = Output.GetSize();
    
    const size_t xMax = size.x,
                 yMax = size.y,
	         zMax = size.z;
    FourLSNeighbourhood n4(size);
    SixLSNeighbourhood n6(size);
    LSNeighbourhood * neighbourhood = NULL;
    valarray<VOXELOUT> & Data = Output.GetVoxelData();
    std::vector<size_t> indexes;
    std::vector<VOXELOUT> values;
    
    KMeansLS<VOXELIN,VOXELOUT>::Initialize();
  
   
    if (zMax > 1 )
    {
	neighbourhood = (LSNeighbourhood *) &n6;
    }
    else
    {
	neighbourhood = (LSNeighbourhood *) &n4;
    }
    const size_t nMax = neighbourhood->GetSize();
          size_t neighbors[nMax];
	  VOXELOUT offset[3], distance;
	  
    

    size_t increment[3];
    size_t proportion[3];
    neighbourhood->GetIncrements(increment);
    neighbourhood->GetProportions(proportion);
//#define FUNKCE if (GlobalTimeStep > (tempTimeStep = 10)) GlobalTimeStep = tempTimeStep;
   
    
  
    if (zMax > 1) //we must compute the level set function in 3 dimensions
    {
	index = xMax * yMax + xMax+1;
	NEIGH
	for (z = proportion[2]; z < zMax - proportion[2]; ++z)
	{
	    for (y = proportion[1];  y < yMax - proportion[1]; ++y )
	    {
		for (x = proportion[0]; x < xMax - proportion[0]; ++x)
		{
		    FUNKCE1
		    index += increment[0];
		    for (i=0; i < nMax; ++i)
			neighbors[i] += increment[0];
		}
		index += increment[1]; // should be +=2 (3-1)
		for (i=0; i < nMax; ++i)
		    neighbors[i]+= increment[1]; // should be += 2 (3-1)
	    }
	    index += increment[2]; // should be += 2 * xMax + 2
	    for (i=0; i < nMax; ++i)
		neighbors[i]+= increment[2]; // should be += 2 * xMax + 2
	}

	// dolni roviny
	for ( index = 0; 
	      index < proportion[2] * yMax * xMax; 
	      index += increment[0])
	{
	    NEIGH
	    FUNKCE1
	}
	
	//horni rovina
	for (index = xMax * yMax * zMax - proportion[2] * xMax * yMax;
	     index < xMax * yMax * zMax; 
	     index += increment[0])
	{
	    NEIGH
	    FUNKCE1
	}

	// predni roviny

	index = xMax * yMax * proportion[2];
	for (z = proportion[2]; z < zMax-proportion[2]; ++z)
	{
	    for (x = 0; x < proportion[1] * xMax; ++x)
	    {
		NEIGH
		FUNKCE1
		index += increment[0];
	    }
	    index += xMax * (yMax - proportion[1]);
	}
	
	// zadni roviny
	index = xMax * yMax * (proportion[2] + 1) -xMax * proportion[1];
	for (z = proportion[2]; z < zMax - proportion[2]; ++z)
	{
	    for (x = 0; x < proportion[1] * xMax; ++x)
	    {
		NEIGH
		FUNKCE1
		index += increment[0];
	    }
	    index += xMax * (yMax - proportion[1]);
	}
	
	// leve roviny
	index = xMax * yMax * proportion[2] + xMax * proportion[1];
	for (z = proportion[2]; z < zMax - proportion[2]; ++z)
	{
	    for (y = proportion[1]; y < yMax - proportion[1]; ++y)
	    {
		for (x = 0; x < proportion[0]; ++x)
		{
		    NEIGH
		    FUNKCE1
		    index += increment[0];
		}
		index += xMax - proportion[0];
	    }
	    index += xMax *(proportion[1] * 2);
	}
	// prave roviny
	index = xMax * yMax * proportion[2] + xMax * (proportion[1] + 1) - proportion[0];
	for (z = proportion[2]; z < zMax - proportion[2]; ++z)
	{
	    for (y = proportion[1]; y < yMax - proportion[1]; ++y)
	    {
		for (x = 0; x < proportion[0]; ++x)
		{
		    NEIGH
		    FUNKCE1
		    index += increment[0];
		}
		index += xMax - proportion[0];
	    }
	    index += xMax *(proportion[1] * 2) ;
	}
	


    }
    else
    { // two dimensions

//	cout << "Proportions: " << proportion[0] << ", " << proportion[1] << endl;
//	cout << "Increment: " << increment[0] << ", " << increment[1] << endl;
	index = xMax + 1;
	NEIGH
	for (y = proportion[1]; y < (yMax - proportion[1]); ++y)
	{
	    for (x = proportion[0]; x < (xMax - proportion[0]); ++x)
	    {
//		cout << index << " ";
//		cout << neighbors[0] << ", " << neighbors[1] << ", " << neighbors[2] << ", " << neighbors[3] << endl;
		FUNKCE1
		index += increment[0];
		for (i = 0; i < nMax; ++i)
		    neighbors[i] += increment[0];
		    
	    }
//	    cout << endl;
	    index += increment[1];
	    for (i = 0; i < nMax; ++i)
		neighbors[i] += increment[1]; // should be += 2;
	}

	// dolni radky obrazu
	for (index = 0; index < (proportion[1] * xMax); ++index)
	{

	    NEIGH
	    FUNKCE1
//		cout << index << " ";
//		cout << neighbors[0] << ", " << neighbors[1] << ", " << neighbors[2] << ", " << neighbors[3] << endl;
	
	}
//	cout << endl;
	// horni radky obrazu
	for (index = (xMax * yMax) - (proportion[1] * xMax); index < (xMax * yMax); ++index)
	{

	    NEIGH
	    FUNKCE1
//	    		cout << index << " ";
//		cout << neighbors[0] << ", " << neighbors[1] << ", " << neighbors[2] << ", " << neighbors[3] << endl;
	
	}

//	cout << endl;
	//leve sloupce obrazu
	for (x = 0; x < proportion[0]; ++x)
	{
	    for (index = (proportion[1] * xMax) + x; 
		 index < (xMax * yMax) - (xMax * proportion[1] + x); 
		 index += xMax )
	    {

		NEIGH
		FUNKCE1
//		cout << index << " ";
//		cout << neighbors[0] << ", " << neighbors[1] << ", " << neighbors[2] << ", " << neighbors[3] << endl;
	
	    }
	}

//	cout << endl;
	//leve sloupce obrazu
	for (x = 0; x < proportion[0]; ++x)
	{
	    for (index = (proportion[1]+1) * xMax - 1 - x;
		 index < xMax * yMax  - (proportion[1] - 1)*xMax - 1 - x;
		 index += xMax )
	    {
		NEIGH
		FUNKCE1
//		cout << index << " ";
//		cout << neighbors[0] << ", " << neighbors[1] << ", " << neighbors[2] << ", " << neighbors[3] << endl;
	
	    }
	}

    }
//	cout << endl;

    distance = 1.0;
    FastMarchingSignedDistance (distance, Output, indexes, values);


}

template < class VOXELIN, class VOXELOUT > AOSChanVese < VOXELIN, VOXELOUT >::AOSChanVese(Image3d<VOXELIN>& in, Image3d<VOXELOUT> & out):
    KMeansLS<VOXELIN, VOXELOUT>(in,out)
{
    SetGlobalTimeStep(0.25);
    SetSpacing(ComputeNormedSpacing(in));
    SetExponent(1.0);
    Epsilon = 1.0;
    Beta = powf((float)MINDIVIDER,1/Exponent);
    HeavisideData = new valarray<VOXELOUT>(0);
    DeltaData = new valarray<VOXELOUT>(0);
    HeavisideData->resize(Output.GetImageSize());
    DeltaData->resize(Output.GetImageSize());
    GradMag = new GradientMagnitudeEstimator<VOXELOUT>(Output);
    GradMag->SetSpacing(ComputeNormedSpacing(in));
    GradMagData = &(GradMag->BufferArray);
    Buffer.resize(Output.GetImageSize());
    Mu = 1.0;
}

template < class VOXELIN,class VOXELOUT > AOSChanVese < VOXELIN,VOXELOUT >::~AOSChanVese()
{
    delete HeavisideData;
    delete DeltaData;
    delete GradMag;
}

template <class VOXEL > inline VOXEL heaviside(VOXEL x)
{
    return (0.5+(1.0/M_PI)*atan(x));    
}

template <class VOXELIN, class VOXELOUT> void AOSChanVese <VOXELIN, VOXELOUT>::SetMu(VOXELOUT m)
{
    Mu = m;
}

template < class VOXELIN, class VOXELOUT > void AOSChanVese < VOXELIN, VOXELOUT >::ComputeHeavisideAndDelta()
{
    valarray<VOXELOUT> & outputData = Output.GetVoxelData();
  for (size_t i = 0; i < Output.GetImageSize(); ++i)
       (*HeavisideData)[i] = heaviside (outputData[i]/Epsilon);

   for (size_t i = 0; i < Output.GetImageSize(); ++i)
       (*DeltaData)[i] = Epsilon/(M_PI*(SQR(Epsilon)+SQR(outputData[i])));
   Image3d<float> tmp;
   tmp.MakeRoom(Output.GetSizeX(), Output.GetSizeY(), Output.GetSizeZ());
}
template < class VOXELIN, class VOXELOUT > void AOSChanVese < VOXELIN, VOXELOUT >::ComputeMeans()
{
  float sum, sum1;
  float nom, nom1;
  VOXELIN * puimage = Input.GetVoxelAddr(0);

  VOXELOUT * hp, * last;

  hp = &((*HeavisideData)[0]);
  last = &((*HeavisideData)[Output.GetImageSize()]);

  lastc1 = c1;
  lastc2 = c2;
  sum = 0;
  sum1 = 0;
  nom = 0;
  nom1 = 0;
  while (hp != last)
  {
      sum += (*hp);
      sum1 += 1-(*hp);
      nom += (*hp) * static_cast<VOXELOUT>(*puimage);
      nom1 += (1-(*hp)) * static_cast<VOXELOUT>(*puimage);
      ++hp;
      ++puimage;
  }
  if (sum != 0)
      c1 = nom/sum;
  if (sum1 != 0)
      c2 = nom1/sum1;
  cout << endl<< "Means c1: " << c1 << " c2: " << c2 << endl<<endl;
}

template <class VOXELIN, class VOXELOUT> void AOSChanVese<VOXELIN,VOXELOUT>::RightHandSide(valarray<VOXELIN> & rf, valarray<VOXELOUT> & ru, valarray<VOXELOUT> & rd, size_t maxi)
{
    VOXELOUT WorkA, WorkB;
    VOXELOUT * Pru =&(ru[0]);
    VOXELOUT * last = &(ru[maxi-1]) + 1;
    VOXELIN * Prf = &(rf[0]);
    VOXELOUT * Prd = &(rd[0]);
    
    while (Pru != last)
    {
	WorkB = lambda2*SQR(c2 - static_cast<VOXELOUT>(* Prf));
	WorkA = -lambda1*SQR(c1 - static_cast<VOXELOUT>(* Prf)); 
	WorkA += WorkB;
	WorkA *= (*Prd);
	WorkA *= GlobalTimeStep;
	*Pru +=  WorkA;
	++Pru;
	++Prf;
	++Prd;
    }        
}
 


template <class VOXELIN, class VOXELOUT> void AOSChanVese<VOXELIN,VOXELOUT>::PrepareDiagonals (size_t start, size_t incr, Vector3d<size_t> & size, size_t axe)
{   
    VOXELOUT m = (VOXELOUT) Dimensions;
    size_t neighbors[2];
        float spacing = 2.0 * SQR(Spacing[axe]);
	size_t Max = size[axe], index;
     //VOXEL nasob;
	
     TwoNeighbors (neighbors, start, size, axe);
     
	Diag[0] = ( //(*GradMagData)[neighbors[0]] + 
		      (*GradMagData)[neighbors[1]] + 
		      ((*GradMagData)[start])) / (spacing);
	SDiag[0]  = - ((*GradMagData)[neighbors[1]] + (*GradMagData)[start]) / (spacing);

	Diag[0] *= (*DeltaData)[start];
	SDiag[0] *= (*DeltaData)[neighbors[1]];


	TwoNeighbors (neighbors, start + incr, size, axe);
	for (index = 1; index < Max -1; ++index)
	{
	    Diag[index] = ((*GradMagData)[neighbors[0]]  + 
		    (*GradMagData)[neighbors[1]] + 
		    2.0*((*GradMagData)[start+index*incr])) / (spacing);
	    SDiag[index]  =  - ((*GradMagData)[neighbors[1]] + (*GradMagData)[start+index*incr]) / (spacing);

	    Diag[index] *=  (*DeltaData)[start+index*incr];
	    SDiag[index] *= (*DeltaData)[neighbors[1]];

	    neighbors[0] += incr;
	    neighbors[1] += incr;
	}
	neighbors[1] -= incr;
	Diag[index] = ((*GradMagData)[neighbors[0]] + 
		       ((*GradMagData)[start + index*incr])) / (spacing);
	Diag[index] *= (*DeltaData)[start + index*incr];

	Diag *= Mu;
	SDiag *= Mu;
	Diag *= SQR(m) * GlobalTimeStep;
	SDiag *=  SQR(m) * GlobalTimeStep;
	Diag += m;
	
}

template <class VOXEL> void Th (Image3d<VOXEL>& i, VOXEL t)
{
    VOXEL * op, * last;
    op = i.GetVoxelAddr(0);
    last = i.GetVoxelAddr(i.GetImageSize());
//    cout << InitLow << " " << InitHigh << endl;
    while (op != last)
    {
	//cout << "*op: " << *op << " threshold: " << t << "InitLow: " << InitLow << " InitHigh" << InitHigh << endl;
	*op = (*op < t ? 0 : 1);
	++op;
    }
}




template < class VOXELIN, class VOXELOUT > void AOSChanVese < VOXELIN, VOXELOUT >::ComputeChange()
{
    size_t x, z, i, j, start, imageSize;
    Vector3d < size_t > size;
    valarray < VOXELOUT > rowU;
    valarray<VOXELOUT> rowD;
    valarray < VOXELIN > rowF;
    valarray < VOXELOUT > &OutputDataArray = Output.GetVoxelData();
    valarray < VOXELIN > &InputDataArray = Input.GetVoxelData();
    int rad, vec = 1, info;
    size.x = Output.GetSizeX();
    size.y = Output.GetSizeY();
    size.z = Output.GetSizeZ();
    imageSize = size.x * size.y * size.z;
    char s[80];
    Image3d<GRAY8> img;
    Image3d<float> fimg;

#ifdef I3D_DEBUG
    cout << "Spacing: " << Spacing << endl;
    cout << "Size: " << size<< endl;
    cout << "Dimensions: " << Dimensions<< endl;
    cout << "TimeStep: " << GlobalTimeStep << endl;
    cout << "Epsilon: " << Epsilon << endl;
#endif
    
    if (ElapsedIteration == 0)
    {

	if (size.z != 1) 
	    sprintf(s,"data/out_%03d.i3d",ElapsedIteration);
	else
	    sprintf(s,"out_%03d.tif",ElapsedIteration);
	fimg = Output;
	Th(fimg,0.0f);
	FloatToGray(fimg,img);
	img.SaveImage(s);
    }
    
    ComputeDiffusivity();
    ComputeHeavisideAndDelta();
    ComputeMeans();

	Buffer = 0.0;

        for (j = 0; j < Dimensions; ++j)
        {
	    
            rad = size[j];
	    Diag.resize(size[j]);
	    SDiag.resize(size[j]-1);
            rowU.resize(size[j]);
	    rowD.resize(size[j]);
            rowF.resize(size[j]);
    
            if (j == 0)
            {
//		cout << "Updating X AXE:\n";
                for (i = 0; i < imageSize / size.x; ++i)
                {
                    start = i * size.x;
		    PrepareDiagonals(start, 1, size, X_AXE);
                    rowU = OutputDataArray[slice(start, size.x, 1)];
                    rowF = InputDataArray[slice(start, size.x, 1)];
		    rowD = (*DeltaData)[slice(start, size.x, 1)];
                    RightHandSide(rowF, rowU, rowD, size.x);

                    sptsv_(rad, vec, &Diag[0], &SDiag[0], &rowU[0], rad, info);
#ifdef I3D_DEBUG              
                    if (info != 0)
		    {
                        cerr << "LOD X axe: Failed to find the solution of system of linear equations\n";
			cerr << "Index of row: " << start / size.x << endl;

			for (size_t index = 0; index < size[j]; ++index)
			    cout << (*DeltaData)[index] << " ";
			cout << endl;

		
		    }
#endif              
                           UpdateRow(Buffer, rowU, slice(start, size.x, 1));
//                    MultiplyRow(OutputDataArray, rowU, slice(start, size.x, 1));
                }
            }
	    
	    else 
	    if (j == 1)
            {
//		cout << "Updating Y AXE:\n";
                for (z = 0; z < size.z; ++z)
                {
                    for (x = 0; x < size.x; ++x)
                    {
                        start = (z * size.x * size.y) + x;
			PrepareDiagonals(start, size.x, size, Y_AXE);
                        rowU = OutputDataArray[slice(start, size.y, size.x)];
                        rowF = InputDataArray[slice(start, size.y, size.x)];
			rowD = (*DeltaData)[slice(start, size.y, size.x)];
                        RightHandSide(rowF, rowU, rowD, size.y);

                        sptsv_(rad, vec, &Diag[0], &SDiag[0], &rowU[0], rad,info);
#ifdef I3D_DEBUG
                        if (info != 0)
			{
                            cerr << "LOD Y axe: Failed to find the solution of system of linear equations\n";
			    cerr << "Index of row: " << x << endl;
			}
#endif

                        UpdateRow(Buffer, rowU,slice(start, size.y, size.x));
//                        MultiplyRow(OutputDataArray, rowU,slice(start, size.y, size.x));
                    }
                }
            } 
	    
		

	    else
		
            {
//		cout << "Updating Z AXE:\n";
                int sizexy = size.x * size.y;
                for (i = 0; i < imageSize / size.z; ++i)
                {
		    PrepareDiagonals(i, sizexy, size, Z_AXE);
                    rowU = OutputDataArray[slice(i, size.z, sizexy)];
		    rowF = InputDataArray[slice(i, size.z, sizexy)];
		    rowD = (*DeltaData)[slice(i, size.z, sizexy)];
                    RightHandSide(rowF, rowU, rowD, size.z);


                    sptsv_(rad, vec, &Diag[0], &SDiag[0], &rowU[0], rad, info);

#ifdef I3D_DEBUG
                    if (info != 0)
                        cerr <<
                            "LOD Z axe: Failed to find the solution of system of linear equations\n";
#endif

		    UpdateRow(Buffer, rowU,slice(i, size.z, sizexy));
//                    MultiplyRow(OutputDataArray, rowU,slice(i, size.z, sizexy));
                }
            }
	    
	    
        }
	OutputDataArray = Buffer;
   {
   if (size.z != 1) 
       sprintf(s,"data/out_%03d.i3d",ElapsedIteration+1);
   else
       sprintf(s,"out_%03d.tif",ElapsedIteration+1);
     	fimg = Output;
	Th(fimg,0.0f);
	FloatToGray(fimg,img);

    img.SaveImage(s);
   }

}



template < class VOXELIN, class VOXELOUT > void AOSChanVese< VOXELIN,VOXELOUT >::SetExponent(VOXELOUT exp)
{
    Exponent = exp;
     Beta = powf((float)MINDIVIDER,1/Exponent);
}

template < class VOXELIN, class VOXELOUT > VOXELOUT AOSChanVese< VOXELIN, VOXELOUT >::GetExponent()
{return Exponent;}

template < class VOXELIN, class VOXELOUT > void AOSChanVese < VOXELIN, VOXELOUT >::SetBeta(VOXELOUT B)
{Beta = B;}
template <class VOXELIN, class VOXELOUT> void AOSChanVese<VOXELIN,VOXELOUT>::SetEpsilon(VOXELOUT e)
{Epsilon = e;}

template < class VOXELIN, class VOXELOUT> VOXELOUT AOSChanVese < VOXELIN, VOXELOUT >::GetBeta()
{return Beta;}
*/

#endif






/****************************************************
 *
 * FastActiveContour algorithm  classes implementation
 *
*****************************************************/
/*
template < class VOXELIN, class VOXELOUT > FastActiveContours < VOXELIN, VOXELOUT >::FastActiveContours(Image3d < VOXELIN > &in, Image3d < VOXELOUT > &out):ExplicitScheme < VOXELIN, VOXELOUT > (in, out)
{
        Function = new FastActiveContoursFunction < VOXELOUT > (Output);
	Thres = 0;
	InitMask = 0;
}

template < class VOXELIN, class VOXELOUT > FastActiveContours < VOXELIN, VOXELOUT >::~FastActiveContours()
{
}

template < class VOXELIN, class VOXELOUT > void FastActiveContours < VOXELIN, VOXELOUT >::SetLambda(VOXELOUT lambda1, VOXELOUT lambda2)
{
  (static_cast < FastActiveContoursFunction < VOXELOUT > *>(Function))->SetLambda(lambda1, lambda2);
}

template < class VOXELIN, class VOXELOUT > void FastActiveContours < VOXELIN, VOXELOUT >::SetMu(VOXELOUT mu) 
{
    (static_cast < FastActiveContoursFunction < VOXELOUT > *>(Function))->SetMu(m);
}

template < class VOXELIN, class VOXELOUT > void FastActiveContours < VOXELIN, VOXELOUT >::ApplyChange() 
{
}


template < class VOXELIN, class VOXELOUT > void FastActiveContours <VOXELIN, VOXELOUT>::Initialize()
{
    VOXELOUT *pom, *last;
    pom = Output.GetVoxelAddr(0);
    last = Output.GetVoxelAddr(Output.GetImageSize());
    if (Thres > 0)
    {
        VOXELIN *pin;
        pin = Input.GetVoxelAddr(0);
        while (pom != last)
        {
            if (*pin < Thres)
                *pom = -1;
            else
                *pom = 1;
            ++pom;
            ++pin;
        }
	return;
    }
    if (InitMask == NULL)
    {
        Image3d < VOXELOUT > onecircle;
        VOXELOUT *p, *last, *srcp;
        size_t sx, sy, sz, ind[3], dim, radius, gap, halfgap, sum, i, x, y, z;
        sx = Output.GetSizeX();
        sy = Output.GetSizeY();
        sz = Output.GetSizeZ();
	sum = (std::min(sx,sy));
	sum = (sz > 1?std::min(sum,sz):sum);
	if (sum < 30)
	{
	    gap = sum;
	}
	else
	{
            gap = 20;
	}
	radius = gap / 3;
	radius *= radius;
	halfgap = gap/2;

        dim = (sz==1?2:3);

        if (dim == 2)
            onecircle.MakeRoom(gap, gap, 1);
        else
            onecircle.MakeRoom(gap, gap, gap);

        p = onecircle.GetVoxelAddr(0);
        last = onecircle.GetVoxelAddr(onecircle.GetImageSize());
        ind[0] = ind[1] = ind[2] = 0;
        while (p != last)
        {
            sum = 0;
            for (i = 0; i < dim; ++i)
                sum += (halfgap - ind[i]) * (halfgap - ind[i]);
            if (sum <= radius)
                *p = 1;
            else
                *p = -1.0;
            ++p;
            ++ind[0];
            ind[0] %= gap;
            if (ind[0] == 0)
            {
                ++ind[1];
                ind[1] %= gap;
            }
            if (dim == 3)
                if ((ind[0] == 0) && (ind[1] == 0))
                    ++ind[2];
        }
        srcp = onecircle.GetVoxelAddr(0);
        p = Output.GetVoxelAddr(0);
        size_t pomy, pomz;
        for (z = 0; z < sz; ++z)
        {
            pomz = (z % gap) * (gap * gap);
            for (y = 0; y < sy; ++y)
            {
                pomy = (y % gap) * gap + pomz;
                last = p;
                for (x = 0; x < sx; x += gap, p += gap)
                {
                    // *p =  onecircle.GetVoxel(x%gap,y%gap,z%gap);
                    memcpy((void *) p, (void *) (srcp + pomy),
                           sizeof(VOXELOUT) * std::min(gap, sx - x));
//            *p =  *(srcp + (x%gap + ((y%gap) * gap) +((z%gap)*(gap*gap))));
                    //    ++p;
                }
                p = last + sx;
            }

        }
    } else
    {
        BINARY *bp;
        bp = InitMask->GetVoxelAddr(0);
        while (pom != last)
        {
            if (*bp == numeric_limits < BINARY >::max())
                *pom = 1;
            else
                *pom = -1;
            ++pom;
            ++bp;
        }
    }
}

template <class VOXELIN,class VOXELOUT> bool FastActiveContours<VOXELIN,VOXELOUT>::Convergence()
{
    if (ElapsedIteration < MaximumIteration)
	return (static_cast< FastActiveContoursFunction<VOXELOUT> * >(Function))->SameMeans();
    else
	return true;
}

template <class VOXELIN,class VOXELOUT> void FastActiveContours<VOXELIN,VOXELOUT>::SetMask (Image3d<BINARY> * m)
{
    InitMask = m;
}

template < class VOXELIN, class VOXELOUT > void FastActiveContours < VOXELIN, VOXELOUT >::SetThreshold(VOXELIN t)
{
    Thres = t;
}




template < class VOXEL > void FastActiveContoursFunction <VOXEL >::SetMu(VOXEL m)
{
    mu = m;
}
template < class VOXEL > void FastActiveContoursFunction < VOXEL >::SetLambda(VOXEL l1, VOXEL l2) 
{
    lambda1 = l1;
    lambda2 = l2;
}

    /// Udates the Globaltime step  from the value returned by \c ComputeUpdate() method.
template <class VOXEL> void FastActiveContoursFunction<VOXEL>::UpdateTimeStep(size_t * neighbors, size_t index, VOXEL tempTimeStep)
{
}
template <class VOXEL> FastActiveContoursFunction<VOXEL>::FastActiveContoursFunction(Image3d < VOXEL > &data):
ExplicitSchemeFunction <VOXEL> (data)
{
}

template <class VOXEL> FastActiveContoursFunction<VOXEL>::~ FastActiveContoursFunction()
{
}
*/



/****************************************************
 *
 * ExplicitSchemeFunctionSegmentation class implementation
 *
*****************************************************/

/* Doposud nehotove

#define FUNKCE1 if (EstimateDistanceInOnePoint(index,neighbors,offset)) { distance = 0; for (i=0; i< Dimensions; ++i) distance += SQR(offset[i]); distance = sqrt(distance); if (Data[index] < 0) distance *= -1; indexes.push_back(index); values.push_back(distance);  } else {if (Data[index] < 0) Data[index] = INFTIMEM(VOXELOUT); else Data[index] = INFTIME(VOXELOUT); }

template <class VOXEL>	ExplicitSchemeFunctionSegmentation<VOXEL>::ExplicitSchemeFunctionSegmentation(Image3d<VOXEL> & data):ExplicitSchemeFunction<VOXEL>(data){}

template <class VOXEL> ExplicitSchemeFunctionSegmentation<VOXEL>::~ExplicitSchemeFunctionSegmentation(){}


//template <class VOXELIN, class VOXELOUT> inline void LSFullSegmentation<VOXELIN,VOXELOUT>::

template <class VOXELIN, class VOXELOUT> void LSFullSegmentation<VOXELIN, VOXELOUT>::Initialize()
{
    size_t x, y, z, index,i;
    Vector3d<size_t> size = Output.GetSize();
    
    const size_t xMax = size.x,
                 yMax = size.y,
	         zMax = size.z;
    FourLSNeighbourhood n4(size);
    SixLSNeighbourhood n6(size);
    LSNeighbourhood * neighbourhood = NULL;
    valarray<VOXELOUT> & Data = Output.GetVoxelData();
    std::vector<size_t> indexes;
    std::vector<VOXELOUT> values;
  
    Function = static_cast<ExplicitSchemeFunction<VOXELOUT> * >(new ExplicitSchemeFunctionSegmentation<VOXELOUT>(Output));
   
    if (zMax > 1 )
    {
	neighbourhood = (LSNeighbourhood *) &n6;
    }
    else
    {
	neighbourhood = (LSNeighbourhood *) &n4;
    }
    const size_t nMax = neighbourhood->GetSize();
          size_t neighbors[nMax];
	  VOXELOUT offset[3], distance;
	  
    

    size_t increment[3];
    size_t proportion[3];
    neighbourhood->GetIncrements(increment);
    neighbourhood->GetProportions(proportion);
//#define FUNKCE if (GlobalTimeStep > (tempTimeStep = 10)) GlobalTimeStep = tempTimeStep;
   
    
  
    if (zMax > 1) //we must compute the level set function in 3 dimensions
    {
	index = xMax * yMax + xMax+1;
	NEIGH
	for (z = proportion[2]; z < zMax - proportion[2]; ++z)
	{
	    for (y = proportion[1];  y < yMax - proportion[1]; ++y )
	    {
		for (x = proportion[0]; x < xMax - proportion[0]; ++x)
		{
		    FUNKCE1
		    index += increment[0];
		    for (i=0; i < nMax; ++i)
			neighbors[i] += increment[0];
		}
		index += increment[1]; // should be +=2 (3-1)
		for (i=0; i < nMax; ++i)
		    neighbors[i]+= increment[1]; // should be += 2 (3-1)
	    }
	    index += increment[2]; // should be += 2 * xMax + 2
	    for (i=0; i < nMax; ++i)
		neighbors[i]+= increment[2]; // should be += 2 * xMax + 2
	}

	// dolni roviny
	for ( index = 0; 
	      index < proportion[2] * yMax * xMax; 
	      index += increment[0])
	{
	    NEIGH
	    FUNKCE1
	}
	
	//horni rovina
	for (index = xMax * yMax * zMax - proportion[2] * xMax * yMax;
	     index < xMax * yMax * zMax; 
	     index += increment[0])
	{
	    NEIGH
	    FUNKCE1
	}

	// predni roviny

	index = xMax * yMax * proportion[2];
	for (z = proportion[2]; z < zMax-proportion[2]; ++z)
	{
	    for (x = 0; x < proportion[1] * xMax; ++x)
	    {
		NEIGH
		FUNKCE1
		index += increment[0];
	    }
	    index += xMax * (yMax - proportion[1]);
	}
	
	// zadni roviny
	index = xMax * yMax * (proportion[2] + 1) -xMax * proportion[1];
	for (z = proportion[2]; z < zMax - proportion[2]; ++z)
	{
	    for (x = 0; x < proportion[1] * xMax; ++x)
	    {
		NEIGH
		FUNKCE1
		index += increment[0];
	    }
	    index += xMax * (yMax - proportion[1]);
	}
	
	// leve roviny
	index = xMax * yMax * proportion[2] + xMax * proportion[1];
	for (z = proportion[2]; z < zMax - proportion[2]; ++z)
	{
	    for (y = proportion[1]; y < yMax - proportion[1]; ++y)
	    {
		for (x = 0; x < proportion[0]; ++x)
		{
		    NEIGH
		    FUNKCE1
		    index += increment[0];
		}
		index += xMax - proportion[0];
	    }
	    index += xMax *(proportion[1] * 2);
	}
	// prave roviny
	index = xMax * yMax * proportion[2] + xMax * (proportion[1] + 1) - proportion[0];
	for (z = proportion[2]; z < zMax - proportion[2]; ++z)
	{
	    for (y = proportion[1]; y < yMax - proportion[1]; ++y)
	    {
		for (x = 0; x < proportion[0]; ++x)
		{
		    NEIGH
		    FUNKCE1
		    index += increment[0];
		}
		index += xMax - proportion[0];
	    }
	    index += xMax *(proportion[1] * 2) ;
	}
	


    }
    else
    { // two dimensions

//	cout << "Proportions: " << proportion[0] << ", " << proportion[1] << endl;
//	cout << "Increment: " << increment[0] << ", " << increment[1] << endl;
	index = xMax + 1;
	NEIGH
	for (y = proportion[1]; y < (yMax - proportion[1]); ++y)
	{
	    for (x = proportion[0]; x < (xMax - proportion[0]); ++x)
	    {
//		cout << index << " ";
//		cout << neighbors[0] << ", " << neighbors[1] << ", " << neighbors[2] << ", " << neighbors[3] << endl;
		FUNKCE1
		index += increment[0];
		for (i = 0; i < nMax; ++i)
		    neighbors[i] += increment[0];
		    
	    }
//	    cout << endl;
	    index += increment[1];
	    for (i = 0; i < nMax; ++i)
		neighbors[i] += increment[1]; // should be += 2;
	}

	// dolni radky obrazu
	for (index = 0; index < (proportion[1] * xMax); ++index)
	{

	    NEIGH
	    FUNKCE1
//		cout << index << " ";
//		cout << neighbors[0] << ", " << neighbors[1] << ", " << neighbors[2] << ", " << neighbors[3] << endl;
	
	}
//	cout << endl;
	// horni radky obrazu
	for (index = (xMax * yMax) - (proportion[1] * xMax); index < (xMax * yMax); ++index)
	{

	    NEIGH
	    FUNKCE1
//	    		cout << index << " ";
//		cout << neighbors[0] << ", " << neighbors[1] << ", " << neighbors[2] << ", " << neighbors[3] << endl;
	
	}

//	cout << endl;
	//leve sloupce obrazu
	for (x = 0; x < proportion[0]; ++x)
	{
	    for (index = (proportion[1] * xMax) + x; 
		 index < (xMax * yMax) - (xMax * proportion[1] + x); 
		 index += xMax )
	    {

		NEIGH
		FUNKCE1
//		cout << index << " ";
//		cout << neighbors[0] << ", " << neighbors[1] << ", " << neighbors[2] << ", " << neighbors[3] << endl;
	
	    }
	}

//	cout << endl;
	//leve sloupce obrazu
	for (x = 0; x < proportion[0]; ++x)
	{
	    for (index = (proportion[1]+1) * xMax - 1 - x;
		 index < xMax * yMax  - (proportion[1] - 1)*xMax - 1 - x;
		 index += xMax )
	    {
		NEIGH
		FUNKCE1
//		cout << index << " ";
//		cout << neighbors[0] << ", " << neighbors[1] << ", " << neighbors[2] << ", " << neighbors[3] << endl;
	
	    }
	}

    }
//	cout << endl;

    distance = 1.0;
    FastMarchingSignedDistance (distance, Output, indexes, values);

}

*/

/****************************************************
 *
 * ExplicitSchemeFunctionSegmentation class implementation
 *
*****************************************************/
/*

template <class VOXEL> VOXEL ExplicitSchemeFunctionSegmentation<VOXEL>::ComputeUpdate(size_t * neighbors, size_t center)
{
    VOXEL DxForward[3];
    VOXEL DxBackward[3];
    VOXEL Godunov;
    VOXEL Speed = -1.0;
    size_t i;


//    cout << " ExplicitSchemeFunctionSegmentation<VOXEL>::ComputeUpdate\n";
//    cout << "Center: " <<center << " Value: " << DataArray[center] << endl;

    
    for (i = 0; i < Dimensions; ++i ) 
    {
	DxBackward[i] = DataArray[center] - DataArray[neighbors[2*i]];
	DxForward[i] = DataArray[neighbors[2*i+1]] - DataArray[center];
	DxForward[i] /= Spacing[i];
	DxBackward[i] /= Spacing[i];
//	cout << "DxForward[" << i << "]: " << DxForward[i] << " DxBackward[" << i <<"]: " << DxBackward[i] << endl; 
    }

    Godunov = 0.0;
    if (Speed  > 0.0)
    {
        for (i = 0; i < Dimensions; ++i)
	    Godunov += SQR(MAX(DxBackward[i],0.0)) + SQR(MIN(DxForward[i],0.0));
    }
    else
    if (Speed < 0.0)
    {
	for (i = 0; i < Dimensions; ++i)
	    Godunov += SQR(MAX(DxForward[i],0.0)) + SQR(MIN(DxBackward[i],0.0));
    }
    Godunov = sqrt(Godunov);
//    cout << "Godunov: " << Godunov << endl;
//    cout << "Godunov * deltaT: " << Godunov * Spacing.x * 0.5<< endl;


    BufferArray[center] = Speed * Godunov;   
//    cout << endl;
    return 0.5 * Spacing.x; // pouze velmi provizorni 
    
}

*/




/****************************************************
 *
 * LevelSetNarrowBand class implementation
 *
*****************************************************/
/* Constructor of narrowband levelset algo */
/*
template <class VOXELIN, class VOXELOUT> LevelSetNarrowBand<VOXELIN,VOXELOUT>::
LevelSetNarrowBand(Image3d<VOXELIN> & in, Image3d<VOXELOUT> & out):
PDESolver<VOXELIN,VOXELOUT>(in,out)
{
    // Reserve room for narrowband
    nodes.reserve( (size_t)(round (0.4 * (float) (Output.GetImageSize()))));

    //Set default radiuses
    InnerRadius = 3.0;
    Radius = 2 * InnerRadius;
    
}
*/

/*
template <class VOXELIN, class VOXELOUT> void HonzovaFce (Image3d<VOXELIN> & ImgIn, Image3d<VOXELOUT> & ImgOut, Image3d<VOXELOUT> & Speed)
{
    FMRoutines2D<VOXELOUT,sgngreater> routines2D;
    FMRoutines3D<VOXELOUT,sgngreater> routines3D;
   
    Image3d<VOXELIN> * ImgTmp;
    Image3d<VOXELOUT> FImg; 
    HASH_SET<size_t> boundary;
    VOXELIN * ImgInP, * ImgTmpP, * LastP, * FirstP;
    SeedPointsVector SPV;
    size_t index;
    Vector3d<size_t> pos;
    
    ImgTmp = new i3d::Image3d<VOXELIN>();
    ImgTmp->SetOffset(ImgIn.GetOffset());
    ImgTmp->SetResolution(ImgIn.GetResolution());
    ImgTmp->MakeRoom(ImgIn.GetSize());
 #ifdef I3D_DEBUG
    cout << "Performing Dilation\n";
#endif
    if (ImgIn.GetSizeZ() > 1)
	Dilation(ImgIn, *ImgTmp, nb3D_6);
    else
	Dilation(ImgIn,*ImgTmp, nb2D_4);
    
    ImgInP = ImgIn.GetVoxelAddr(0);
    ImgTmpP = ImgTmp->GetVoxelAddr(0);
    LastP = ImgTmp->GetVoxelAddr(ImgTmp->GetImageSize());
    FirstP = ImgInP;
    while (ImgTmpP != LastP)
    {
	if ((*ImgTmpP) != (*ImgInP))
	{
	    boundary.insert((size_t)(ImgInP - FirstP));
	}
	++ImgInP;
	++ImgTmpP;
    }
#ifdef I3D_DEBUG
    cout << "Performing Erosion\n";
#endif
    if (ImgIn.GetSizeZ() > 1)
	Erosion(ImgIn, *ImgTmp, nb3D_6);
    else
	Erosion(ImgIn,*ImgTmp, nb2D_4);
   

   ImgTmpP = ImgTmp->GetVoxelAddr(0);
   FirstP = ImgTmpP;
   ImgInP = ImgIn.GetVoxelAddr(0);
   

   while (ImgTmpP != LastP)
   {
   if (*ImgTmpP != *ImgInP)
       {
	   index =  ImgTmpP-FirstP;
           SPV.push_back(ImgTmp->GetPos(index));
       }
       ++ImgTmpP;
       ++ImgInP;
   }

   delete ImgTmp;
 #ifdef I3D_DEBUG
    cout << "Making Room for Output Image\n";
#endif
    ImgOut.SetOffset(ImgIn.GetOffset());
    ImgOut.SetResolution(ImgIn.GetResolution());
    ImgOut.MakeRoom(ImgIn.GetSize());
   
   if (ImgIn.GetSizeZ() > 1)
   {
       WeightedGeodesicDistanceComp( ImgOut, Speed, SPV, boundary, routines3D);
   }
   else
   {
       WeightedGeodesicDistanceComp( ImgOut, Speed, SPV, boundary, routines2D);
   }
}
*/





/* Explicit instantiations */




template I3D_DLLEXPORT void FindContourLevel(Image3d<float> & Img, float& value,size_t Start, float Threshold, size_t Bins, size_t Step, double Sigma, double Radius);
template I3D_DLLEXPORT void FastMarchingMethod (Image3d<float> & SpeedImg, Image3d<float> & ImgOut, SeedPointsVector & SeedPoints);
template I3D_DLLEXPORT void GeodesicDistance (Image3d<BINARY> & ImgIn, Image3d<float> & ImgOut, SeedPointsVector & SeedPoints);
template I3D_DLLEXPORT void GeodesicDistance (Image3d<GRAY8> & ImgIn, Image3d<float> & ImgOut, SeedPointsVector & SeedPoints);
template I3D_DLLEXPORT void LocalRadius (const Image3d<BINARY> & ImgIn, Image3d<float> & ImgOut, SeedPointsVector & SeedPoints);
template I3D_DLLEXPORT void LocalRadius (const Image3d<GRAY8> & ImgIn, Image3d<float> & ImgOut, SeedPointsVector & SeedPoints);
template I3D_DLLEXPORT void LocalRadius (const Image3d<GRAY16> & ImgIn, Image3d<float> & ImgOut, SeedPointsVector & SeedPoints);

template I3D_DLLEXPORT void SpeedFunctionStandard (Image3d<GRAY8> & ImgIn,Image3d<float>&SpeedImg,double sigmax, double sigmay, double sigmaz, double width);
template I3D_DLLEXPORT void SpeedFunctionExponential (Image3d<GRAY8> & ImgIn,Image3d<float>&SpeedImg,double sigmax, double sigmay, double sigmaz, double width, double alpha);
template I3D_DLLEXPORT void SpeedFunctionStandard (Image3d<GRAY16> & ImgIn,Image3d<float>&SpeedImg,double sigmax, double sigmay, double sigmaz, double width);
template I3D_DLLEXPORT void SpeedFunctionExponential (Image3d<GRAY16> & ImgIn,Image3d<float>&SpeedImg,double sigmax, double sigmay, double sigmaz, double width, double alpha);


template I3D_DLLEXPORT i3d::Vector3d<float> ComputeNormedSpacing (i3d::Image3d<float> & img);
template I3D_DLLEXPORT i3d::Vector3d<float> ComputeNormedSpacing (i3d::Image3d<GRAY8> & img);
template class I3D_DLLEXPORT SpatialDerivatives<float>;


template class I3D_DLLEXPORT ExplicitScheme<GRAY8,float>;
template class I3D_DLLEXPORT PDESolver<GRAY8,float>;
template class I3D_DLLEXPORT PDESolver<GRAY16,float>;
template class I3D_DLLEXPORT MCFFilter<GRAY8,float>;
template class I3D_DLLEXPORT PMFilter<GRAY8,float>;
template class I3D_DLLEXPORT MCDEFilter<GRAY8,float>;

#ifdef WITH_LAPACK
template class I3D_DLLEXPORT PDESolver<float,float>;
template class I3D_DLLEXPORT AOSCLMCFilter<GRAY8,float>;
template class I3D_DLLEXPORT AOSTVFilter<float,float>;
template class I3D_DLLEXPORT AOSTVFilter<GRAY8,float>;
template class I3D_DLLEXPORT AOSTVFilter<GRAY16,float>;
template class I3D_DLLEXPORT AOSPMFilter<GRAY8,float>;
template class I3D_DLLEXPORT LODGaussianBlur<GRAY8,float>;
template class I3D_DLLEXPORT LODGaussianBlur<float,float>;
#endif

template class I3D_DLLEXPORT KMeansLS<GRAY8,float>;
template class I3D_DLLEXPORT KMeansLS<float,float>;
template class I3D_DLLEXPORT KMeansLS<GRAY16,float>;

#ifdef WITH_LAPACK
template class I3D_DLLEXPORT ThresholdActiveContours<GRAY8,float>;
template class I3D_DLLEXPORT ThresholdActiveContours<float,float>;
template class I3D_DLLEXPORT ThresholdActiveContours<double,double>;
#endif

}


