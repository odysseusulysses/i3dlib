/*
 * i3dlib - image manipulation library
 *
 * Copyright (C) 2000-2006   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

//------------------------------------------------------------------------
/*
 * FILE: convolution.cc
 *
 * Image convolution (1D, 2D, 3D) over the image of ANY size. For small
 * image the time complexity is:
 * - 1D: O(N log N)
 * - 2D: O(N^2 log N)
 * - 3D: O(N^3 log N)
 * For large images, that do not fit into physical memory, the complexity 
 * raises up to:
 * - 1D: O(N^2)
 * - 2D: O(N^4)
 * - 3D: O(N^6)
 *
 *
 * Author: David Svoboda <svoboda@fi.muni.cz> October 2009
 */
//------------------------------------------------------------------------

#ifdef __GNUG__
#pragma implementation
#endif

#include <iostream>
#include <vector>
#include <algorithm>
#include <queue>

#ifdef I3D_DEBUG
	#include <typeinfo>
#endif

#ifdef WITH_FFTW
	#include <complex>
	#include <fftw3.h>
#endif

#include "toolbox.h"
#include "fourier.h"
#include "convolution.h"

#define is_odd(X) (((X) % 2) == 1)
#define is_even(X) (((X) % 2) == 0)

using namespace std;


namespace i3d {

//--------------------------------------------------------------------------
/** A function this an auxiliary structure. It has only one input
 * parameter of type Vector3d<size_t>. It generate the sequence of such
 * 3D vectors that first the largest component is sequentially lowered to 1,
 * then the second largest component is lowered in the same way, and finally
 * the last component is lowered. The output sequence os vector is stored
 * in a queue.
 * */
//--------------------------------------------------------------------------
struct couple 
	{
		  size_t value;
		  size_t position; 

		  couple(size_t v,size_t p):value(v),position(p) {};
		  couple() {};

		  bool operator<(const couple &x) const
		  {
				return ((this->value) < (x.value));
		  }
	 };

void generate_descending_sequence(const Vector3d<size_t> &dim,
										  std::queue<Vector3d<size_t>/**/> &seq)
{
	 // The input vector should be positive. It cannot contain zeros.
	 if (dim.x * dim.y * dim.z == 0)
		  throw InternalException("Wrong dimensions");

	 // Pour the input vector into an auxiliary array
	 std::vector<couple> array(3);
	 array[0] = couple(dim.x, 0);
	 array[1] = couple(dim.y, 1);
	 array[2] = couple(dim.z, 2);

	 // Sort this array + remember the original positions
	 stable_sort(array.begin(), array.end());

	 // Store the initial value
	 seq.push(dim);

	 // Track the individual components (in descending order respecting
	 // their size) and decrease their value in successive steps.
	 for (int i=2; i>=0; i--)
	 {
		  do {
				array[i].value--;

				Vector3d<size_t> v;
				v[array[0].position] = array[0].value;
				v[array[1].position] = array[1].value;
				v[array[2].position] = array[2].value;

				// Store the new (decreased) triple
				seq.push(v);
		  } while (array[i].value > 1); // Decrease down to the value '1'
	 }
}

//--------------------------------------------------------------------------
/** Function checks if the given number can be factored 
 * into product of small primes
 *
 * fftw documentation: FFTW is best at handling sizes of the form:
 *   2^a * 3^b * 5^c * 7^d * 11^e * 13^f, where e+f is either 0 or 1, 
 *   and the other exponents are arbitrary.
 *
 * */
//--------------------------------------------------------------------------
bool is_factorable(size_t number)
{
#ifdef I3D_DEBUG
	 size_t number_backup = number;
#endif
	 // possible primes: 2,3,5 and 7 -- for more, see documentation of FFTW
	 const size_t primes[] = {2,3,5,7,11,13};
	 const size_t sz = sizeof(primes)/sizeof(size_t);
	 size_t remainder;
	 size_t *powers = new size_t[sz];

	 for (size_t i=0; i<sz; i++)
	 {
		  powers[i] = 0;

		  do {
				remainder = number % primes[i];
				if (remainder == 0)
				{
					 number /= primes[i];
					 powers[i]++;
				}
		  } while (remainder == 0);
	 }

	 // there is no remainder and 
	 // the sum of powers of 11 and 13 gives '0' or '1'
	 if ((number == 1) && (powers[4] + powers[5] <= 1))
	 {
#ifdef I3D_DEBUG
		  cout << "Convolution: " << number_backup << " = ";
		  for (size_t j=0; j<sz; j++)
		  {
				if (j > 0)
					 cout << " * ";

				cout << primes[j] << "^" << powers[j];
		  }
		  cout << endl;
#endif
		  delete [] powers;
		  return true;
	 }

	 delete [] powers;
	 return false;
}

//--------------------------------------------------------------------------
/** This function gives you the estimate, how much memory you will need
 *  if performing fft-based convolution over two images of specified size.
 *  The precision of the evaluation is also taken into account.
 *
 *  This is the result that 'estimate_needed_space()' should give you:
 *
 *  X = F_voi_sz.x + G_voi_sz.x
 *  Y = F_voi_sz.y + G_voi_sz.y
 *  Z = F_voi_sz.z + G_voi_sz.z
 *
 *  our_estimate = X*Y*Z*[sizeof(T) + 5*sizeof(PRECISION)]
 */
//--------------------------------------------------------------------------
template <class T, class PRECISION> double
estimate_needed_space(const Vector3d<size_t> &F_voi_sz,
							 const Vector3d<size_t> &G_voi_sz)
{
	 // If you want to perform convolution via FFT, you must align both
	 // tiles (their sizes are defined via VOIs) to the same size.
	 // This variable defines this new size that both tiles will 
	 // stretched to.
	 Vector3d<size_t> VOI_padded_dims = F_voi_sz + G_voi_sz - size_t(1);

	 // number of voxels in the padded image
	 double num_of_voxels =
				double(VOI_padded_dims.x) * // width
				double(VOI_padded_dims.y) * // height
				double(VOI_padded_dims.z); // depth

	 // Now, we will try to find out, how much memory is occupied by ONE
	 // stretched tile (size is defined in bytes).
	 double img_padded_sz = num_of_voxels *
				sizeof(T);  // size of data type

	 // When performing Fourier transform (REAL->COMPLEX) we will also
	 // need a certain memory that will be occupied by the output, i.e.
	 // by the complex image.
	 double ft_img_padded_sz = num_of_voxels *
				sizeof(complex<PRECISION>);

	 // There is some memory requirement by FFTW algorithm (an algorithm
	 // that performs Fast Fourier Transform - http://www.fftw.org/).
    double fftw_buffer_size = GetFFTWBufferSize<PRECISION>(VOI_padded_dims);

	 // The total size (excluding the input/output buffers) of the memory
	 // that will be needed for the evaluation of the convolution is the sum
	 // of all the previous variables. One can say that we must multiply
	 // all values by '2' as we convolve two images which means that we
	 // need twice more memory. In practice, some operation can be serialized,
	 // i.e. memory can be released before we start computing another
	 // part of the algorithm.
	 double total_size =
				img_padded_sz + 
				2 * ft_img_padded_sz +
				fftw_buffer_size;

	 return total_size;
}

//--------------------------------------------------------------------------
/** Function that checks the available memory and the possibilities
 * of saving the memory by splitting the huge images. It we have too few
 * memory, then splitting is required. 
 * Let us split both the image and the kernel
 * into the blocks of the same size of ANY shape. Why?
 * (a) using the same division for image and kernel is an optimal solution
 * (b) the complexity of convolution over chopped image is independent
 *     of the shape of the tiles. It depends only on the number of pixels
 *     processed.
 *
 * Return values:
 * - true ... Succeeded :)
 *   then vector 'tile_dim' stores the proposed tile dimensions
 * - false ... Failed to allocate memory :(
 */
template < class T, class PRECISION > bool
find_optimal_tile_size(const Vector3d<size_t> &img_dim,
							  const Vector3d<size_t> &ker_dim,
							  Vector3d<size_t> &tile_dim)
{
	 tile_dim = max(img_dim, ker_dim);

	 // How large portion of the total available memory can be dedicated
	 // to this process - we won't use the whole available memory, as we leave
	 // something for other processes etc...
	 const double memory_fraction = 0.9;
	 double free_ram = GetFreeMemory();
	 double mem_available = memory_fraction * free_ram;
	 double conv_size;

#ifdef I3D_DEBUG
	 cout << "Convolution: we can use: " << 
				human_readable(mem_available,0) << " bytes" << endl;
#endif

	 // NOTE:
	 // The memory for the input/output image and the convolution kernel has
	 // already been occupied, i.e. concerning I/O buffers we have
	 // nothing to do with. We will focus only on the memory requirements
	 // of FFTW and some local buffers.

 	 // Check if the convolution can be performed with the selected
	 // tiling of the input image and convolution kernel (the initial
	 // size tiles is the same as the whole image and the whole kernel
	 // i.e. without splitting). 
	 conv_size = estimate_needed_space<T, PRECISION>(img_dim, ker_dim);

#ifdef I3D_DEBUG
			  cout << "Convolution: we need: " << 
						 human_readable(conv_size,0) << " bytes" << endl;
#endif

	 // If tile fits the memory, leave the function and return 'true'.  
	 if (conv_size < mem_available)
	 {
#ifdef I3D_DEBUG
			  cout << "Convolution: enough memory for full convolution." << endl;
#endif
		  return true;
	 }

 	 /* If this code is reached, the available memory was found to be 
	  * too small. Prior to the convolution the filtered image has to 
	  * be split into tiles.
	  * 
	  * Some notes for better understanding:
	  * 
	  * - tile_dim ... The dimensions of the tile we're looking for
	  * 
	  * We will apply several limitations applied to `tile_dim`
	  *
	  * - all the tiles will be of the same size and shape 
	  *   (except for the boundary tiles)
	  * - both the image and the kernel will be split into the tiles
	  *   of the same size
	  * - padded `tile_dim` should be simply factored into small primes
	  * - `tile_dim` should be as large as possible (up to mem_available)
	  *
	  * Given the above conditions, it is true that:
	  *
	  * X = 2 * tile_dim.x - 1 (twice - due to padding)
	  * Y = 2 * tile_dim.y - 1 
	  * Z = 2 * tile_dim.z - 1
	  * memory_required = X*Y*Z*[sizeof(T) + 5*sizeof(PRECISION)]
	  *                 = (2*D)^3 * [sizeof(T) + 5*sizeof(PRECISION)]
	  * 
	  * The amount of the free memory is known. 
	  */
	 
	 size_t datatype_sz = sizeof(T) + 5*sizeof(PRECISION);
	 bool searching;
	 Vector3d<size_t> new_tile_dim;
	 
	 std::queue<Vector3d<size_t>/**/> seq;
	 generate_descending_sequence(tile_dim, seq);

	 VOI<PIXELS> img_voi(0, img_dim), ker_voi(0, ker_dim);

	 do {
		  // Initially, we believe that the chosen size of the tile is OK,
		  // hence we do not need search for the next one.
		  searching = false; 
		 
		  // Try the largest possible tile.
		  new_tile_dim = seq.front();
		  seq.pop();

		  // Make an intersection of this tile this image and kernel to get
		  // the real size of tiles in image and kernel.
		  VOI<PIXELS> 
					 tile_voi(0, new_tile_dim),
					 img_tile_voi = tile_voi * img_voi,
					 ker_tile_voi = tile_voi * ker_voi;

		  // Compute the size of padded tile
		  Vector3d<size_t> new_tile_padded = 
					 img_tile_voi.size + ker_tile_voi.size - size_t(1);

		  // Check if the padded image can be factored into small primes
		  for (size_t idx=0; (idx<3) && (!searching); idx++)
		  {
				// Verify only the site (x,y or z), that is split
				if (new_tile_dim[idx] < tile_dim[idx])
				{
					 // If the padded tile cannot be factored, 
					 // try another (lower) tile
					 if (!is_factorable(new_tile_padded[idx]))
						  searching = true;
				}
		  } 

		  // Check if the tile size fits the available memory
		  conv_size = estimate_needed_space<T, PRECISION>(new_tile_dim,
																		  new_tile_dim);

#if I3D_DEBUG
		  	cout << "Convolution: trying size: " << new_tile_dim << endl;
			cout << "Convolution: need x available: " << conv_size << " x " << 
					  mem_available << endl;
#endif

		  // If the memory requirements for the selected size of a tile 
		  // are more demanding than the available memory is, lower the size
		  // of tile, i.e. make the next loop
		  if (conv_size > mem_available)
				searching = true;
	 
	 } while (!seq.empty() && searching);


#ifdef I3D_DEBUG
	 cout << "Convolution: size of data types: " <<  datatype_sz << endl;
	 cout << "Convolution: size of tile: " << new_tile_dim << endl;
#endif

	if (seq.empty())
	{
#ifdef I3D_DEBUG
		 cout << "Convolution: failed to find optimal tile size." << endl;
		 cout << "Convolution: ADVICE: extend your physical memory." << endl;
#endif

	 	 return false; // failed to allocate the memory 
	}

	tile_dim = new_tile_dim;
	return true;
}

//--------------------------------------------------------------------------
/** This function performs FFTW based convolution over given VOIs (tiles).
 * It also solves the problem with both image and kernel tile padding. 
 * If the VOI goes beyond or touches the image boundary, the
 * padding algorithm pads the area with zeros. If the VOI (as the small
 * part of some larger image) is completely inside an image, 
 * the padding algorithm copies the values from the 
 * corresponding positions.
 *
 * This function DO NOT allocate the input/output memory buffers!
 *
 * H = F (x) G
 *
 * image 'F':                    image 'G':
 * |-----------------------|  
 * |                       |     |----------------|
 * |          |-------|    |     | |-------|      |
 * |          |       |    |     | | G_voi |      |
 * |          | F_voi |    | (x) | |-------|      |
 * |          |-------|    |     |                |
 * |                       |     |----------------|
 * |                       |
 * |-----------------------|
 *
 * image 'H':
 * |-----------------------|
 * |                       |
 * |          |-------|    |
 * |          |       |<-------- output area for F_voi (x) G_voi
 * |          |       |    |
 * |          |-------|    |
 * |                       |
 * |                       |
 * |-----------------------|
 *
 * F_voi is a small particle of some large scale image F. The convolution
 * between F_voi and the whole G can be performed if you guarantee some
 * padding of F_voi. Particularly, F_voi must be padded with the dimension
 * of G minus one. After that a convolution with this padded F_voi and G
 * can be performed without the lost of generality.
 *
 * G_voi is a small particle of some large scale image G (convolution kernel).
 * However splitting G into small G_voi-s is not as simple as in the case 
 * of image F. It is important to know that convolution F_voi with G_voi 
 * gives only a small portion of the final solution. All the possible 
 * (nonoverlapping) G_voi's must be convolved with the selected F_voi. All the
 * results will be ADDED together to get the final values in 'X'.
 *
 *
 * AN IDEA OF SPLITTING 'G' INTO SMALL PARTS (TILES)
 *
 * Given the original kernel 'G' we can split it into two parts
 * G = G_voi + G_rest
 * 
 * 'G':                    'G_voi':               'G_rest':
 * |----------------|      |----------------|     |----------------|
 * |xxxxxxxxxxxxxxxx|      |                |     |xxxxxxxxxxxxxxxx|
 * |xxxxxxxxxxxxxxxx|      | xxxxxxxx       |     |x       xxxxxxxx|
 * |xxxxxxxxxxxxxxxx|      | xxxxxxxx       |     |x       xxxxxxxx|
 * |xxxxxxxxxxxxxxxx|      | xxxxxxxx       |     |x       xxxxxxxx|
 * |----------------|      |----------------|     |----------------|
 *
 * As convolution keeps the distribution rules, we can write:
 * F (x) G = F (x) G_voi + F (x) G_rest
 *
 * If we generalize our idea, we can split G into many tiles:
 * G = G_1 + G_2 + G_3 + G_4 + ... G_n
 * which leads to the following:
 * F (x) G = F (x) G_1 + F (x) G_2 + ... + F(x) G_n
 *
 * One can simply see, that splitting G into small part won't save the memory,
 * as all the particles (tiles) are of the same size. There is another
 * good convolution property - shift invariance:
 *
 * If we remove all the zeros from G_voi we get smaller tile G_voi_cropped.
 * Aside from this, we must remember the vector S, which defines the position
 * of G_voi_cropped inside the original G_voi. Doing the two steps above
 * (cropping and remembering the shift vector) we can state that
 * F (x) G_voi = shift_by_S{F} (x) G_voi_cropped
 * which saves the memory.
 * 
 */
//--------------------------------------------------------------------------
template < class T_in, class T_out, class PRECISION > void
convolution_in_voi(const Image3d<T_in> &F,
						 const Image3d<T_in> &G,
						 Image3d<T_out> &H,
						 const VOI<PIXELS> &F_voi,
						 const VOI<PIXELS> &G_voi,
						 PRECISION factor)
{

#ifdef WITH_FFTW
	 /* F_voi_padding ... How much the 'F_voi' must be padded in order we
	  *                   could guarantee that the tiled-convolution is evaluated
	  *                   correctly, i.e. we won't lose any information.
	  * G_voi_padding ... Also G_voi must be padded. If 'G=G_voi' then padding
	  *                   of G is complementary to F, i.e. F padded and G padded
	  *                   have the same size.
	  * G_voi_halfsize ... One half of the area 'G_voi'
	  */
	 Vector3d<int> F_voi_padding = Vector3d<int>(G.GetSize() / size_t(2)),
						G_voi_padding = Vector3d<int>(F_voi.size / size_t(2)),
						G_voi_halfsize = Vector3d<int>(G_voi.size / size_t(2));

	 /* F_voi_padded is the base F_voi padded in all the directions
	  *
 	  *                                |-----------| 
	  * |-------|                      | |-------| |
	  * | F_voi | + F_voi_padding  =>  | | F_voi | | = F_voi_padded
	  * |-------|                      | |-------| |
	  *                                |-----------|
	  */
	 VOI<PIXELS> F_voi_padded(F_voi.offset - F_voi_padding,
									  F_voi.size + G.GetSize() - size_t(1));

	 /* G_voi_padded is the base G_voi padded ONLY in bottom-right direction.
	  * Theoretically, the padding should be done in the same way as in the
	  * case of F_voi. Fortunately, G_voi is cropped without any overlaps,
	  * i.e. everything beyond its boundaries is set to zero. For the further
	  * evaluation of FFT we need to shift G_voi to the origin. That is why
	  * we pad the G_voi in the following way. At least, we save some time.
	  *
	  *
 	  *                                |-------|---| 
	  * |-------|                      | G_voi |   |
	  * | G_voi | + G_voi_padding  =>  |-------|   |  = G_voi_padded
	  * |-------|                      |           |
	  *                                |-----------|
	  */
	 VOI<PIXELS> G_voi_padded(G_voi.offset,// - G_voi_padding, 
									  G_voi.size + F_voi.size - size_t(1));

	 /*
	  * If G=G_voi (is no G splitting) then F_active_area = F_voi_padded. 
	  * If G>G_voi, then we must select that part of F_voi_padded, that
	  * corresponds to the mirrored (across centre of F_voi_padded) and padded
	  * G_voi area. It is because convolution flips the kernel. As we handle
	  * with subparts of kernel G we must also manage this flipping.
	  *
	  * |---------------|                          |---------------|  
	  * |               |    G:|----|-------|      |               |
	  * |  F_voi_padded |      |    | G_voi |  =>  | F_voi_padded  |
	  * |               |      |    |-------|      |-------|       |
	  * |               |      |            |      |   *   |       |
	  * |---------------|      |------------|      |-------|-------|
	  *                                               ^ 
	  *                                               |
	  *                                            F_active_tile
	  */																 
	 VOI<PIXELS> F_active_tile(F_voi_padded.offset 
										+ Vector3d<int>(F_voi_padded.size)
										- Vector3d<int>(G_voi_padded.size)
										- G_voi.offset,
										G_voi_padded.size);

	 // ------------- prepare 'G'-part -------------
	 // pad the image
	 Image3d<T_in> *G_tile_padded = new Image3d<T_in>();

	 /*
	  * Initially, fill the image G_tile_padded with zeros (black background) 
	  * and then fill the top-left corner with values from the area in G defined 
	  * by G_voi.
	  **/
	 G_tile_padded->MakeRoom(G_voi_padded.size);
	 G_tile_padded->Clear(); 
	 
	 for (size_t z=0; z<G_voi.size.z; z++)
		  for (size_t y=0; y<G_voi.size.y; y++)
		  {
				const T_in* src = G.GetVoxelAddr(G_voi.offset.x,
															G_voi.offset.y + y,
															G_voi.offset.z + z);
				T_in* dst = G_tile_padded->GetVoxelAddr(0,y,z);
				memcpy(dst, src, G_voi.size.x * sizeof(T_in));
		  }

	 // perform FFT
	 Image3d< complex<PRECISION> > *ft_G_padded;
	 ft_G_padded = new Image3d< complex<PRECISION> >();
	 FFT(*G_tile_padded, *ft_G_padded); 

	 // G_tile_padded won't be used - release the memory.
	 delete G_tile_padded; 
	 // ------------- 'G'-part ready -------------

	 // ------------- prepare 'F'-part --------------
	 // pad the image
	 Image3d<T_in> *F_tile_padded = new Image3d<T_in>();
	 F_tile_padded->CopyFromVOI(F, F_active_tile, true);

	 // G_voi is a part of larger G. G originates in the top left corner
	 // while placing of G_voi starts from the opposite corner. As the
	 // dimensions of G_voi may be odd/even, we must shift the data by
	 // one pixel in some cases.
	 Vector3d<int> rotation = G_voi_halfsize;

	 if (is_even(G_voi.size.x))
		  rotation.x--;

	 if (is_even(G_voi.size.y))
		  rotation.y--;

	 if (is_even(G_voi.size.z))
		  rotation.z--;

	 FFTshift(*F_tile_padded, rotation);

	 // perform FFT
	 Image3d<complex<PRECISION> > *ft_F_padded = 
				new Image3d< complex<PRECISION> >();
	 FFT(*F_tile_padded, *ft_F_padded); 
	 delete F_tile_padded;

	 // The previous line is critical for memory usage:
	 // - 1x image in time domain (F)
	 // - 2x image in Fourier domain (F and G)
	 // - 1x fftw buffer 
	 // ------------- 'F'-part ready --------------

#ifdef I3D_DEBUG
	 {
	 double free_ram = GetFreeMemory();

	 cout << "Convolution: free in critical section: " <<
		human_readable(free_ram - 
				GetFFTWBufferSize<PRECISION>(F_voi_padded.size),0) << endl;
	 }
#endif

	 // Perform point-wise multiplication (including normalization)
	 complex<PRECISION> 
				*F_it = ft_F_padded->GetFirstVoxelAddr(), 
				*G_it = ft_G_padded->GetFirstVoxelAddr();

	 size_t sz = ft_F_padded->GetImageSize();
	 factor /= sz;

	 for (size_t i=0; i<sz; i++)
	 {
		  *F_it *= (*G_it) * (factor);
		  F_it++;
		  G_it++;
	 }

	 // ft_G_padded won't be used - release the memory.
	 delete ft_G_padded; 

	 // Perform inverse FFT - put the solution into the variable 'H_tile_padded'
	 Image3d<T_out> *H_tile_padded = new Image3d<T_out>();
	 IFFT(*ft_F_padded, *H_tile_padded);

	 // ft_F_padded won't be used - release the memory.
	 delete ft_F_padded;

	 // Put the data to the output. Strictly speaking, fill the image H
	 // with the output data. Take care only of the area defined by 'F_voi'!
	 
	 for (size_t z=0; z<F_voi.size.z; z++)
		  for (size_t y=0; y<F_voi.size.y; y++)
		  {
				T_out *H_padded_it = 
						  H_tile_padded->GetVoxelAddr(G_voi_halfsize.x,
																G_voi_halfsize.y + y,
																G_voi_halfsize.z + z),
						*H_it = H.GetVoxelAddr(F_voi.offset.x, 
													  F_voi.offset.y + y,
													  F_voi.offset.z + z);

				for (size_t i=0; i<F_voi.size.x; i++)
				{
					 *H_it += *H_padded_it;
					 H_it++; 
					 H_padded_it++;
				}
		  }

	 // release the memory dedicated for this image
	 delete H_tile_padded;

#else // WITH_FFTW
  throw InternalException("i3dlib: FFT support is missing. "
								  "Enable WITH_FFTW flag and recompile the i3dlib.");
#endif // WITH_FFTW
}

//--------------------------------------------------------------------------
/**
 * Convolution ... This function performs FFT based convolution. If needed
 * the input image is split before.
 */
//--------------------------------------------------------------------------
template < class PRECISION, class T_in, class T_out> void
Convolution (const Image3d<T_in> &img_in,
             const Image3d<T_in> &kernel,
             Image3d<T_out> &img_out,
				 bool normalize_kernel)
{
#ifdef I3D_DEBUG
	 cout << "Convolution FFT based convolution started" << endl;
	 cout << "\t - input image size: " << img_in.GetSize() << endl;
	 cout << "\t - convolution kernel size: " << kernel.GetSize() << endl;
	 cout << "\t - required precision: " << typeid(PRECISION).name() << endl;
#endif

	 if ((img_in.GetImageSize() == 0) || (kernel.GetImageSize() == 0))
	 {
		  throw LibException("I3D::Convolution: images have "
									"strange size."); 
	 }

	 // both the input image and kernel should have the same resolution.
	 const Vector3d<float> acceptable_diff(0.001, 0.001, 0.001);

	 Vector3d<float> 
				img_res = img_in.GetResolution().GetRes(),
				ker_res = kernel.GetResolution().GetRes();

	 Vector3d<float> real_diff(fabs(img_res.x - ker_res.x),
										fabs(img_res.y - ker_res.y),
										fabs(img_res.z - ker_res.z));

	 if (real_diff > acceptable_diff)
	 {
		  throw LibException("I3D::Convolution: images have "
									"different resolution."); 
	 }

	 // allocate the memory for the output (including resolution)
	 // and clear the memory buffer
	 img_out.CopyMetaData(img_in);
	 img_out.Clear();

	 // fetch the size of input image and the kernel
	 Vector3d<size_t>
				img_dim = img_in.GetSize(),
	 			ker_dim = kernel.GetSize(),
				tile_dim;

	 // find out the best memory allocation. There exist only three conclusions:
	 // 1) memory is ok, i.e. the convolution can be performed directly
	 // 2) not enough memory which causes the convolved image 
	 //    splitting into tiles. In this case the tiles won't be of the same
	 //    size as the whole image or the whole kernel
	 // 3) not enough memory at all -> failure
	 if (!find_optimal_tile_size<T_in,PRECISION>(img_in.GetSize(), 
																kernel.GetSize(), 
																tile_dim))
	 {
		  throw LibException("I3D::Convolution: Not enough memory for"
									" performing convolution.");
	 }

	 // How many pieces/tiles is split the image into?
	 Vector3d<size_t> num_of_img_tiles;
	 num_of_img_tiles.x = img_dim.x / tile_dim.x;
	 num_of_img_tiles.x += ((img_dim.x % tile_dim.x) > 0);

	 num_of_img_tiles.y = img_dim.y / tile_dim.y;
	 num_of_img_tiles.y +=((img_dim.y % tile_dim.y) > 0);

	 num_of_img_tiles.z = img_dim.z / tile_dim.z ;
	 num_of_img_tiles.z +=((img_dim.z % tile_dim.z) > 0);

	 // How many pieces/tiles is split the kernel into?
	 Vector3d<size_t> num_of_ker_tiles;
	 num_of_ker_tiles.x = ker_dim.x / tile_dim.x;
	 num_of_ker_tiles.x += ((ker_dim.x % tile_dim.x) > 0);

	 num_of_ker_tiles.y = ker_dim.y / tile_dim.y;
	 num_of_ker_tiles.y +=((ker_dim.y % tile_dim.y) > 0);

	 num_of_ker_tiles.z = ker_dim.z / tile_dim.z ;
	 num_of_ker_tiles.z +=((ker_dim.z % tile_dim.z) > 0);

#ifdef I3D_DEBUG
	 cout << "Convolution: image tiling: " << num_of_img_tiles << endl;
	 cout << "Convolution: kernel tiling: " << num_of_ker_tiles << endl;
	 cout << "Convolution: padded tile size: " << 
				size_t(2) * tile_dim - size_t(1) << endl;
#endif 

	 // create the VOI that covers the whole input image
	 VOI<PIXELS> voi_of_whole_img(0, img_dim);

	 // create the VOI that covers the whole kernel
	 VOI<PIXELS> voi_of_whole_ker(0, ker_dim);

	 // The kernel normalization (sum == 1) might be required
	 PRECISION factor = 1;

	 if (normalize_kernel)
	 {
		  PRECISION sum = 0;
		  for (size_t i=0; i<kernel.GetImageSize(); i++)
				sum += kernel.GetVoxel(i);

		  factor = 1.0 / sum; 

#ifdef I3D_DEBUG
		  cout << "Convolution: kernel normalization factor: " << factor << endl;
#endif

	 }

	 // Create the lists of all the tiles into which the input image and kernel
	 // will be split.
	 std::vector<VOI<PIXELS>/**/> tiles_from_image(0), tiles_from_kernel(0);
	 Vector3d<size_t> skip;

	 // Do it for the input image.
	 for (skip.x = 0; skip.x < num_of_img_tiles.x; skip.x++)
		  for (skip.y = 0; skip.y < num_of_img_tiles.y; skip.y++)
				for (skip.z = 0; skip.z < num_of_img_tiles.z; skip.z++)
				{
					 VOI<PIXELS> tile_voi(tile_dim * skip, tile_dim);
					 tile_voi *= voi_of_whole_img;

					 tiles_from_image.push_back(tile_voi);
				}

	 // Do it for the kernel. 
	 for (skip.x = 0; skip.x < num_of_ker_tiles.x; skip.x++)
		  for (skip.y = 0; skip.y < num_of_ker_tiles.y; skip.y++)
				for (skip.z = 0; skip.z < num_of_ker_tiles.z; skip.z++)
				{
					 VOI<PIXELS> kernel_voi(tile_dim * skip, tile_dim);
					 kernel_voi *= voi_of_whole_ker;

					 tiles_from_kernel.push_back(kernel_voi);
				}

#ifdef I3D_DEBUG
	 cout << "Convolution: tiles of image:  " << 
				tiles_from_image.size() << endl;
	 cout << "Convolution: tiles of kernel: " << 
				tiles_from_kernel.size() << endl;
#endif


	 // Perform the convolution.
	 for (size_t i=0; i<tiles_from_image.size(); i++)
	 {
		  for (size_t j=0; j<tiles_from_kernel.size(); j++)
		  {
				convolution_in_voi(img_in,
										 kernel,
										 img_out,
										 tiles_from_image[i],
										 tiles_from_kernel[j],
										 factor);
		  }
	 }


#ifdef I3D_DEBUG
	 cout << "Convolution: FFT based convolution successfully finished." << endl;
#endif
}


/************** explicit instantiation ************************************/ 

template I3D_DLLEXPORT 
void Convolution<float, BINARY, float> (const Image3d <BINARY> &, 
										  const Image3d <BINARY> &, 
										  Image3d <float> &, bool);

template I3D_DLLEXPORT 
void Convolution<double, BINARY, float> (const Image3d <BINARY> &, 
										  const Image3d <BINARY> &, 
										  Image3d <float> &, bool);

template I3D_DLLEXPORT 
void Convolution<float, GRAY8, GRAY8> (const Image3d <GRAY8> &, 
										  const Image3d <GRAY8> &, 
										  Image3d <GRAY8> &, bool);

template I3D_DLLEXPORT 
void Convolution<float, GRAY16, GRAY16>(const Image3d <GRAY16> &, 
										  const Image3d <GRAY16> &, 
										  Image3d <GRAY16> &, bool);

template I3D_DLLEXPORT 
void Convolution<float, float, float> (const Image3d <float> &, 
										  const Image3d <float> &, 
										  Image3d <float> &, bool);

template I3D_DLLEXPORT 
void Convolution<double, GRAY8, GRAY8> (const Image3d <GRAY8> &, 
											const Image3d <GRAY8> &, 
											Image3d <GRAY8> &, bool);

template I3D_DLLEXPORT 
void Convolution<double, GRAY16, GRAY16>(const Image3d <GRAY16> &, 
											const Image3d <GRAY16> &, 
											Image3d <GRAY16> &, bool);

template I3D_DLLEXPORT 
void Convolution<double, float, float> (const Image3d <float> &, 
											const Image3d <float> &, 
											Image3d <float> &, bool);

template I3D_DLLEXPORT 
void Convolution<double, double, double> (const Image3d <double> &, 
											 const Image3d <double> &, 
											 Image3d <double> &, bool);

/***************************************************************************/ 

}
