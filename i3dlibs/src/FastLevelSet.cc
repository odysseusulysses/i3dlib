
/*
 * FastLevelSet.cc
 *
 * Implementation of fast level set methods
 *
 * Martin Maska (xmaska@fi.muni.cz) 2005, 2006, 2007
 */

/*Suppression of Deprecated warning for Visual studio 8*/
#ifdef _MSC_VER
#if _MSC_VER == 1400
#define _CRT_SECURE_NO_DEPRECATE 1
#endif
#endif

#include "FastLevelSet.h"
#include "edge.h"
#include "points.h"
#include "filters.h"
#include "regions.h"
#include "DistanceTransform.h"

#define SQR(x) ((x) * (x))
#define MIN(x, y) ((x) < (y) ? (x) : (y))
#define MAX(x, y) ((x) > (y) ? (x) : (y))
#define UNDEFINED std::numeric_limits<float>::max()
#define EPSILON float(0.00000001)
#define CENTRAL(a, b, h) (((b) - (a)) / (2 * (h)))
#define SIDE(a, b, h) (((b) - (a)) / (h))
#define GETPOSITION_2D(x, y)  x = i % ImgSize.x; y = i / ImgSize.x
#define GETPOSITION_2D_COMP(x, y)  x = i % (ImgSize.x + 2); y = i / (ImgSize.x + 2)
#define GETPOSITION_3D(x, y, z) x = i % ImgSize.x; y = (i / ImgSize.x) % ImgSize.y; z = (i / ImgSize.x) / ImgSize.y
#define GETPOSITION_3D_COMP(x, y, z) x = i % (ImgSize.x + 2); y = (i / (ImgSize.x + 2)) % (ImgSize.y + 2); z = (i / (ImgSize.x + 2)) / (ImgSize.y + 2)

/** Look-up tables for fast simple point detection. */
static const unsigned char row_index[16] = { 0, 1, 0, 1, 2, 3, 4, 5, 0, 1, 0, 1, 2, 1, 4, 6 };
static const bool is_simple[112] = { false, false,  true,  true, false, false,  true,  true,  true,  true, false,  true,  true,  true, false,  true, // 0
                                      true,  true, false, false,  true,  true,  true,  true, false, false, false, false, false, false, false,  true, // 1
									  true,  true, false, false,  true,  true, false, false, false, false, false, false, false, false, false, false, // 2
									 false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, // 3
									  true,  true, false, false,  true,  true, false, false,  true,  true, false,  true,  true,  true, false,  true, // 4 
							         false, false, false, false, false, false, false, false, false, false, false, false, false, false, false,  true, // 5 
							          true,  true, false, false,  true,  true,  true,  true,  true,  true, false,  true,  true,  true,  true, false  // 6 
                                   };

/*static const unsigned char row_index[16] = { 0, 1, 2, 3, 2, 4, 2, 4, 5, 1, 6, 3, 2, 4, 2, 4 };
static const bool is_simple[112] = { false,  true,  true,  true,  true, false,  true,  true,  true,  true,  true,  true, false, false,  true,  true,  // 6
									  true, false,  true,  true,  true, false,  true,  true, false, false,  true,  true, false, false,  true,  true,  // 4
									  true, false, false, false, false, false, false, false,  true,  true,  true,  true, false, false,  true,  true,  // 1
									 false, false, false, false, false, false, false, false, false, false,  true,  true, false, false,  true,  true,  // 2
									  true, false,  true,  true,  true, false,  true,  true,  true,  true, false, false,  true,  true, false, false,  // 0
									  true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false,  // 5
									 false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false,  // 3
                            };*/

using namespace std;

/////////////////////////////////////////////////////////////////
//                                                             //
//                          Class HeapBase                     //
//                                                             //
/////////////////////////////////////////////////////////////////

template <class T> T * HeapBase<T>::Pop()
{
  int num = Nodes.size() - 1;

  // the heap is empty
  if (num == -1)
	throw i3d::InternalException("Pop from the empty heap.");
	
  T *min = Nodes[0];

  if (num)
  {
    // swap the root and the last node on the heap
	BackPointers[Nodes[0] -> position] = -1;
	BackPointers[Nodes[num] -> position] = 0;
	Nodes[0] = Nodes[num];
    Nodes.pop_back();
	BalanceDown(0);
  }
  else
  {
    // the heap contains the only one node
    BackPointers[Nodes[0] -> position] = -1;
    Nodes.pop_back();
  }

  return min;
}

template <class T> void HeapBase<T>::BalanceDown(size_t index)
{
  size_t num = Nodes.size();   
  size_t sec = 2 * index + 2; // an index of the second child of the node

  while (sec < num)
  {
    if (Compare(sec - 1, sec)) // select the smaller child
      --sec;

	if (Compare(sec, index))
	{
      // swap the node and the child on the heap
      T *pom = Nodes[sec];
	  Nodes[sec] = Nodes[index];
	  Nodes[index] = pom;

	  // swap back pointers of the node and the child
	  BackPointers[Nodes[index] -> position ] = index; 
	  BackPointers[Nodes[sec] -> position] = sec; 
	  
	  index = sec;
	  sec = 2 * index + 2;
	  continue;
	}

	break;
  }

  if (sec == num)  // the node has the only left child
  {
    --sec;

	if (Compare(sec, index))
    {
      // swap the node and the left child on the heap
      T *pom = Nodes[sec];
	  Nodes[sec] = Nodes[index];
	  Nodes[index] = pom;

	  // swap back pointers of the node and the left child
	  BackPointers[Nodes[index] -> position ] = index; 
	  BackPointers[Nodes[sec] -> position] = sec; 
	}
  }
} 

template <class T> void HeapBase<T>::BalanceUp(size_t index)
{
  size_t parent = (index - 1) / 2;

  while (index > 0 && Compare(index, parent))
  {
    // swap the parent and the node on the heap
    T *pom = Nodes[parent];
	Nodes[parent] = Nodes[index];
	Nodes[index] = pom;

	// swap back pointers of the parent and the node
	BackPointers[Nodes[parent] -> position ] = parent; 
	BackPointers[Nodes[index] -> position] = index; 

	index = parent;
	parent = (index - 1) / 2;
  }
}

/////////////////////////////////////////////////////////////////
//                                                             //
//                       Class SwedenHeap                      //
//                                                             //
/////////////////////////////////////////////////////////////////

void SwedenHeap::Insert(size_t pos, float atime, float speed)
{
  size_t index = Nodes.size();
  Nodes.push_back(new SwedenHeapNode(pos, atime, atime + 1 / MAX(std::abs(speed), EPSILON), speed));
  BackPointers[Nodes[index] -> position] = index;
  BalanceUp(index);
}

void SwedenHeap::Update(size_t index, float new_speed)
{
  // the update is needless
  if (Nodes[index] -> speed == new_speed)
    return;
  
  Nodes[index] -> speed = new_speed;
  Nodes[index] -> departure_time = Nodes[index] -> arrival_time + 1 / MAX(std::abs(new_speed), EPSILON);

  if (index == 0 || Compare((index - 1) / 2, index))
    BalanceDown(index);
  else
    BalanceUp(index);
}

/////////////////////////////////////////////////////////////////
//                                                             //
//                      Class ChineseHeap                      //
//                                                             //
/////////////////////////////////////////////////////////////////

void ChineseHeap::Insert(size_t pos, float atime)
{
  size_t index = Nodes.size();
  Nodes.push_back(new ChineseHeapNode(pos, atime));
  BackPointers[Nodes[index] -> position] = index;
  BalanceUp(index);
}

void ChineseHeap::Update(size_t index, float new_atime)
{
  // the update is needless
  if (Nodes[index] -> arrival_time == new_atime)
    return;
  
  Nodes[index] -> arrival_time = new_atime;

  if (index == 0 || Compare((index - 1) / 2, index))
    BalanceDown(index);
  else
    BalanceUp(index);
}

void ChineseHeap::Delete(size_t index)
{ 
  if (index > Nodes.size() - 1)
	throw i3d::InternalException("Invalid delete from the heap.");   

  size_t num = Nodes.size() - 1;

  if (index != num)
  {
    BackPointers[Nodes[index] -> position] = -1;
	Nodes[index] = Nodes[num];
    BackPointers[Nodes[index] -> position] = index;
    Nodes.pop_back();

	if (index == 0 || Nodes[index] -> arrival_time >= Nodes[(index - 1) / 2] -> arrival_time)
      BalanceDown(index);
    else
      BalanceUp(index);
  }
  else
  {
    BackPointers[Nodes[index] -> position] = -1;
    Nodes.pop_back();
  }
}

	/////////////////////////////////////////////////////////////////
	//                                                             //
	//                   Struct RoutineBase                        //
	//                                                             //
	/////////////////////////////////////////////////////////////////
	
	RoutineBase::RoutineBase(int width, int height, int depth)
		: m_heap(NULL), m_width(width), m_height(height), m_depth(depth)
	{
	}

	RoutineBase::~RoutineBase()
	{
		if (m_heap)
		{
			delete m_heap;
		}
	}

	void RoutineBase::Initialize()
	{
		int size = m_width * m_height * m_depth;

		m_implicit_function.resize(size, 1);
		m_curvature.resize(size, UNDEFINED);  
		m_back_pointers.resize(size, -1);

		if (m_heap)
		{
			delete m_heap;
		}

		m_heap = new SwedenHeap(m_back_pointers);
	}

	inline void RoutineBase::Get2DCoordinates(int i, int &x, int &y) const
	{
		x = i % m_width;
		y = i / m_width;
	}

	inline void RoutineBase::Get3DCoordinates(int i, int &x, int &y, int &z) const
	{
		x = i % m_width; 
		y = (i / m_width) % m_height; 
		z = (i / m_width) / m_height;
	}

	/////////////////////////////////////////////////////////////////
	//                                                             //
	//           Struct TopologyPreserving_RoutineBase             //
	//                                                             //
	/////////////////////////////////////////////////////////////////

	TopologyPreserving_RoutineBase::TopologyPreserving_RoutineBase(int width, int height, int depth)
		: RoutineBase(width, height, depth)
	{
	}

	void TopologyPreserving_RoutineBase::Initialize()
	{
		RoutineBase::Initialize();

		int size = (m_width + 2) * (m_height + 2) * (m_depth > 1 ? (m_depth + 2) : m_depth);
		m_component.resize(size, 0);
	}

	inline void TopologyPreserving_RoutineBase::Get2DComponentIndex(int i, int &index) const
	{
		int x, y;
		Get2DCoordinates(i, x, y);
		
		index = (m_width + 2) * (y + 1) + x + 1;
	}

	inline void TopologyPreserving_RoutineBase::Get3DComponentIndex(int i, int &index) const
	{
		int x, y, z;
		Get3DCoordinates(i, x, y, z);

		index = (m_width + 2) * (m_height + 2) * (z + 1) + (m_width + 2) * (y + 1) + x + 1;
	}

	/////////////////////////////////////////////////////////////////
	//                                                             //
	//                Class ComputationalDomain                    //
	//                                                             //
	/////////////////////////////////////////////////////////////////

	ComputationalDomain::ComputationalDomain(const i3d::BINARY *domain)
		: m_domain(domain)
	{
	}

	bool ComputationalDomain::IsInDomain(int i) const
	{
		return (*(m_domain + i) == std::numeric_limits<i3d::BINARY>::max());
	}
  
	/////////////////////////////////////////////////////////////////
	//                                                             //
	//                     Class Neighbourhood                     //
	//                                                             //
	/////////////////////////////////////////////////////////////////
	
	Neighbourhood::Neighbourhood(const RoutineBase *base, int size)
		: m_base(base)
	{
		m_neighbors = new int[size];
	}

	Neighbourhood::~Neighbourhood()
	{
		if (m_neighbors)
		{
			delete [] m_neighbors;
		}
	}

	/////////////////////////////////////////////////////////////////
	//                                                             //
	//             Class Standard_Neighbourhood2D                  //
	//                                                             //
	/////////////////////////////////////////////////////////////////

	Standard_Neighbourhood2D::Standard_Neighbourhood2D(const RoutineBase *base)
		: Neighbourhood(base, 4)
	{
	}

	const int *Standard_Neighbourhood2D::GetNeighbors(int i, int value, int &num)
	{
		int x, y, index;
		m_base -> Get2DCoordinates(i, x, y);

		num = 0;

		index = i - 1;
		if (x > 0 && m_base -> m_implicit_function[index] == value)
		{
			m_neighbors[num++] = index;
		}
		
		index = i + 1;
		if ((x + 1) < m_base -> m_width && m_base -> m_implicit_function[index] == value)
		{
			m_neighbors[num++] = index;
		}
		
		index = i - m_base -> m_width;
		if (y > 0 && m_base -> m_implicit_function[index] == value)
		{
			m_neighbors[num++] = index;
		}

		index = i + m_base -> m_width;
		if ((y + 1) < m_base -> m_height && m_base -> m_implicit_function[index] == value)
		{
			m_neighbors[num++] = index;
		}

		return m_neighbors;
	}

	/////////////////////////////////////////////////////////////////
	//                                                             //
	//             Class Standard_Neighbourhood3D                  //
	//                                                             //
	/////////////////////////////////////////////////////////////////

	Standard_Neighbourhood3D::Standard_Neighbourhood3D(const RoutineBase *base)
		: Neighbourhood(base, 6)
	{
	}

	const int *Standard_Neighbourhood3D::GetNeighbors(int i, int value, int &num)
	{
		int x, y, z, index;
		m_base -> Get3DCoordinates(i, x, y, z);

		num = 0;

		index = i - 1;
		if (x > 0 && m_base -> m_implicit_function[index] == value)
		{
			m_neighbors[num++] = index;
		}
		
		index = i + 1;
		if ((x + 1) < m_base -> m_width && m_base -> m_implicit_function[index] == value)
		{
			m_neighbors[num++] = index;
		}
		
		index = i - m_base -> m_width;
		if (y > 0 && m_base -> m_implicit_function[index] == value)
		{
			m_neighbors[num++] = index;
		}

		index = i + m_base -> m_width;
		if ((y + 1) < m_base -> m_height && m_base -> m_implicit_function[index] == value)
		{
			m_neighbors[num++] = index;
		}

		index = i - m_base -> m_width * m_base -> m_height;
		if (z > 0 && m_base -> m_implicit_function[index] == value)
		{
			m_neighbors[num++] = index;
		}

		index = i + m_base -> m_width * m_base -> m_height;
		if ((z + 1) < m_base -> m_depth && m_base -> m_implicit_function[index] == value)
		{
			m_neighbors[num++] = index;
		}

		return m_neighbors;
	}

	/////////////////////////////////////////////////////////////////
	//                                                             //
	//         Class Standard_Neighbourhood2D_WithDomain           //
	//                                                             //
	/////////////////////////////////////////////////////////////////

	Standard_Neighbourhood2D_WithDomain::Standard_Neighbourhood2D_WithDomain(const RoutineBase *base, const i3d::BINARY *domain)
		: Neighbourhood(base, 4), ComputationalDomain(domain)
	{
	}

	const int *Standard_Neighbourhood2D_WithDomain::GetNeighbors(int i, int value, int &num)
	{
		int x, y, index;
		m_base -> Get2DCoordinates(i, x, y);

		num = 0;

		index = i - 1;
		if (x > 0 && IsInDomain(index) && m_base -> m_implicit_function[index] == value)
		{
			m_neighbors[num++] = index;
		}
		
		index = i + 1;
		if ((x + 1) < m_base -> m_width && IsInDomain(index) && m_base -> m_implicit_function[index] == value)
		{
			m_neighbors[num++] = index;
		}
		
		index = i - m_base -> m_width;
		if (y > 0 && IsInDomain(index) && m_base -> m_implicit_function[index] == value)
		{
			m_neighbors[num++] = index;
		}

		index = i + m_base -> m_width;
		if ((y + 1) < m_base -> m_height && IsInDomain(index) && m_base -> m_implicit_function[index] == value)
		{
			m_neighbors[num++] = index;
		}

		return m_neighbors;
	}

	/////////////////////////////////////////////////////////////////
	//                                                             //
	//         Class Standard_Neighbourhood3D_WithDomain           //
	//                                                             //
	/////////////////////////////////////////////////////////////////

	Standard_Neighbourhood3D_WithDomain::Standard_Neighbourhood3D_WithDomain(const RoutineBase *base, const i3d::BINARY *domain)
		: Neighbourhood(base, 6), ComputationalDomain(domain)
	{
	}

	const int *Standard_Neighbourhood3D_WithDomain::GetNeighbors(int i, int value, int &num)
	{
		int x, y, z, index;
		m_base -> Get3DCoordinates(i, x, y, z);

		num = 0;

		index = i - 1;
		if (x > 0 && IsInDomain(index) && m_base -> m_implicit_function[index] == value)
		{
			m_neighbors[num++] = index;
		}
		
		index = i + 1;
		if ((x + 1) < m_base -> m_width && IsInDomain(index) && m_base -> m_implicit_function[index] == value)
		{
			m_neighbors[num++] = index;
		}
		
		index = i - m_base -> m_width;
		if (y > 0 && IsInDomain(index) && m_base -> m_implicit_function[index] == value)
		{
			m_neighbors[num++] = index;
		}

		index = i + m_base -> m_width;
		if ((y + 1) < m_base -> m_height && IsInDomain(index) && m_base -> m_implicit_function[index] == value)
		{
			m_neighbors[num++] = index;
		}

		index = i - m_base -> m_width * m_base -> m_height;
		if (z > 0 && IsInDomain(index) && m_base -> m_implicit_function[index] == value)
		{
			m_neighbors[num++] = index;
		}

		index = i + m_base -> m_width * m_base -> m_height;
		if ((z + 1) < m_base -> m_depth && IsInDomain(index) && m_base -> m_implicit_function[index] == value)
		{
			m_neighbors[num++] = index;
		}

		return m_neighbors;
	}

	/////////////////////////////////////////////////////////////////
	//                                                             //
	//         Class TopologyPreserving_Neighbourhood2D            //
	//                                                             //
	/////////////////////////////////////////////////////////////////

	TopologyPreserving_Neighbourhood2D::TopologyPreserving_Neighbourhood2D(const RoutineBase *base)
		: Neighbourhood(base, 4)
	{
	}

	const int *TopologyPreserving_Neighbourhood2D::GetNeighbors(int i, int value, int &num)
	{
		int x, y, index;
		m_base -> Get2DCoordinates(i, x, y);

		num = 0;

		if (x == 0)
		{
			m_neighbors[num++] = -1;
		}
		else
		{
			index = i - 1;
			if (m_base -> m_implicit_function[index] == value)
			{
				m_neighbors[num++] = index;
			}
		}

		if ((x + 1) == m_base -> m_width)
		{
			m_neighbors[num++] = -1;
		}
		else
		{
			index = i + 1;
			if (m_base -> m_implicit_function[index] == value)
			{
				m_neighbors[num++] = index;
			}
		}

		if (y == 0)
		{
			m_neighbors[num++] = -1;
		}
		else
		{
			index = i - m_base -> m_width;
			if (m_base -> m_implicit_function[index] == value)
			{
				m_neighbors[num++] = index;
			}
		}

		if ((y + 1) == m_base -> m_height)
		{
			m_neighbors[num++] = -1;
		}
		else
		{
			index = i + m_base -> m_width;
			if (m_base -> m_implicit_function[index] == value)
			{
				m_neighbors[num++] = index;
			}
		}

		return m_neighbors;
	}

	/////////////////////////////////////////////////////////////////
	//                                                             //
	//         Class TopologyPreserving_Neighbourhood3D            //
	//                                                             //
	/////////////////////////////////////////////////////////////////

	TopologyPreserving_Neighbourhood3D::TopologyPreserving_Neighbourhood3D(const RoutineBase *base)
		: Neighbourhood(base, 6)
	{
	}

	const int *TopologyPreserving_Neighbourhood3D::GetNeighbors(int i, int value, int &num)
	{
		int x, y, z, index;
		m_base -> Get3DCoordinates(i, x, y, z);

		num = 0;

		if (x == 0)
		{
			m_neighbors[num++] = -1;
		}
		else
		{
			index = i - 1;
			if (m_base -> m_implicit_function[index] == value)
			{
				m_neighbors[num++] = index;
			}
		}

		if ((x + 1) == m_base -> m_width )
		{
			m_neighbors[num++] = -1;
		}
		else
		{
			index = i + 1;
			if (m_base -> m_implicit_function[index] == value)
			{
				m_neighbors[num++] = index;
			}
		}

		if (y == 0)
		{
			m_neighbors[num++] = -1;
		}
		else
		{
			index = i - m_base -> m_width;
			if (m_base -> m_implicit_function[index] == value)
			{
				m_neighbors[num++] = index;
			}
		}

		if ((y + 1) == m_base -> m_height)
		{
			m_neighbors[num++] = -1;
		}
		else
		{
			index = i + m_base -> m_width;
			if (m_base -> m_implicit_function[index] == value)
			{
				m_neighbors[num++] = index;
			}
		}

		if (z == 0)
		{
			m_neighbors[num++] = -1;
		}
		else
		{
			index = i - m_base -> m_width * m_base -> m_height;
			if (m_base -> m_implicit_function[index] == value)
			{
				m_neighbors[num++] = index;
			}
		}

		if ((z + 1) == m_base -> m_depth)
		{
			m_neighbors[num++] = -1;
		}
		else
		{
			index = i + m_base -> m_width * m_base -> m_height;
			if (m_base -> m_implicit_function[index] == value)
			{
				m_neighbors[num++] = index;
			}
		}

		return m_neighbors;
	}

	/////////////////////////////////////////////////////////////////
	//                                                             //
	//     Class TopologyPreserving_Neighbourhood2D_WithDomain     //
	//                                                             //
	/////////////////////////////////////////////////////////////////

	TopologyPreserving_Neighbourhood2D_WithDomain::TopologyPreserving_Neighbourhood2D_WithDomain(const RoutineBase *base, const i3d::BINARY *domain)
		: Neighbourhood(base, 4), ComputationalDomain(domain)
	{
	}

	const int *TopologyPreserving_Neighbourhood2D_WithDomain::GetNeighbors(int i, int value, int &num)
	{
		int x, y, index;
		m_base -> Get2DCoordinates(i, x, y);

		num = 0;

		index = i - 1;
		if (x == 0 || !IsInDomain(index))
		{
			m_neighbors[num++] = -1;
		}
		else
		{
			if (m_base -> m_implicit_function[index] == value)
			{
				m_neighbors[num++] = index;
			}
		}

		index = i + 1;
		if ((x + 1) == m_base -> m_width || !IsInDomain(index))
		{
			m_neighbors[num++] = -1;
		}
		else
		{
			if (m_base -> m_implicit_function[index] == value)
			{
				m_neighbors[num++] = index;
			}
		}

		index = i - m_base -> m_width;
		if (y == 0 || !IsInDomain(index))
		{
			m_neighbors[num++] = -1;
		}
		else
		{
			if (m_base -> m_implicit_function[index] == value)
			{
				m_neighbors[num++] = index;
			}
		}

		index = i + m_base -> m_width;
		if ((y + 1) == m_base -> m_height || !IsInDomain(index))
		{
			m_neighbors[num++] = -1;
		}
		else
		{
			if (m_base -> m_implicit_function[index] == value)
			{
				m_neighbors[num++] = index;
			}
		}

		return m_neighbors;
	}

	/////////////////////////////////////////////////////////////////
	//                                                             //
	//    Class TopologyPreserving_Neighbourhood3D_WithDomain      //
	//                                                             //
	/////////////////////////////////////////////////////////////////

	TopologyPreserving_Neighbourhood3D_WithDomain::TopologyPreserving_Neighbourhood3D_WithDomain(const RoutineBase *base, const i3d::BINARY *domain)
		: Neighbourhood(base, 6), ComputationalDomain(domain)
	{
	}

	const int *TopologyPreserving_Neighbourhood3D_WithDomain::GetNeighbors(int i, int value, int &num)
	{
		int x, y, z, index;
		m_base -> Get3DCoordinates(i, x, y, z);

		num = 0;

		index = i - 1;
		if (x == 0 || !IsInDomain(index))
		{
			m_neighbors[num++] = -1;
		}
		else
		{
			if (m_base -> m_implicit_function[index] == value)
			{
				m_neighbors[num++] = index;
			}
		}

		index = i + 1;
		if ((x + 1) == m_base -> m_width || !IsInDomain(index))
		{
			m_neighbors[num++] = -1;
		}
		else
		{
			if (m_base -> m_implicit_function[index] == value)
			{
				m_neighbors[num++] = index;
			}
		}

		index = i - m_base -> m_width;
		if (y == 0 || !IsInDomain(index))
		{
			m_neighbors[num++] = -1;
		}
		else
		{
			if (m_base -> m_implicit_function[index] == value)
			{
				m_neighbors[num++] = index;
			}
		}

		index = i + m_base -> m_width;
		if ((y + 1) == m_base -> m_height || !IsInDomain(index))
		{
			m_neighbors[num++] = -1;
		}
		else
		{
			if (m_base -> m_implicit_function[index] == value)
			{
				m_neighbors[num++] = index;
			}
		}

		index = i - m_base -> m_width * m_base -> m_height;
		if (z == 0 || !IsInDomain(index))
		{
			m_neighbors[num++] = -1;
		}
		else
		{
			if (m_base -> m_implicit_function[index] == value)
			{
				m_neighbors[num++] = index;
			}
		}

		index = i + m_base -> m_width * m_base -> m_height;
		if ((z + 1) == m_base -> m_depth || !IsInDomain(index))
		{
			m_neighbors[num++] = -1;
		}
		else
		{
			if (m_base -> m_implicit_function[index] == value)
			{
				m_neighbors[num++] = index;
			}
		}

		return m_neighbors;
	}

	/////////////////////////////////////////////////////////////////
	//                                                             //
	//                    Class SpeedFunction                      //
	//                                                             //
	/////////////////////////////////////////////////////////////////

	SpeedFunction::SpeedFunction(const RoutineBase *base)
		: m_base(base)
	{
	}

	SpeedFunction::~SpeedFunction()
	{
	}

	/////////////////////////////////////////////////////////////////
	//                                                             //
	//               Class Segmentation_SpeedFunction              //
	//                                                             //
	/////////////////////////////////////////////////////////////////
	
	Segmentation_SpeedFunction::Segmentation_SpeedFunction(const RoutineBase *base, const std::valarray<float> &inflation_term, const std::valarray<float> &data_term, 
		                                                   float spacing_x, float spacing_y, float spacing_z, const SpeedFunctionParams &speed_params)
		: SpeedFunction(base), m_inflation_term(inflation_term), m_data_term(data_term), m_spacing_x(spacing_x), m_spacing_y(spacing_y), 
		  m_spacing_z(spacing_z), m_speed_params(speed_params)
	{
	}

	/////////////////////////////////////////////////////////////////
	//                                                             //
	//               Class Segmentation_SpeedFunction2D            //
	//                                                             //
	/////////////////////////////////////////////////////////////////
	
	Segmentation_SpeedFunction2D::Segmentation_SpeedFunction2D(const RoutineBase *base, const std::valarray<float> &inflation_term, const std::valarray<float> &data_term, 
		                                                       float spacing_x, float spacing_y, const SpeedFunctionParams &speed_params)
		: Segmentation_SpeedFunction(base, inflation_term, data_term, spacing_x, spacing_y, 1.0f, speed_params)
	{
	}

	float Segmentation_SpeedFunction2D::GetSpeed(int i) const
	{
		int x, y;
		m_base -> Get2DCoordinates(i, x, y);

		float Dx = 0, Dy = 0, IGx = 0, IGy = 0;
		
		if (x > 0 && (x + 1) < m_base -> m_width)
		{
			Dx = CENTRAL(m_base -> m_implicit_function[i - 1], m_base -> m_implicit_function[i + 1], m_spacing_x);
			IGx = CENTRAL(m_data_term[i - 1], m_data_term[i + 1], m_spacing_x);
		}

		if (y > 0 && (y + 1) < m_base -> m_height)
		{
			Dy = CENTRAL(m_base -> m_implicit_function[i - m_base -> m_width], m_base -> m_implicit_function[i + m_base -> m_width], m_spacing_y);
			IGy = CENTRAL(m_data_term [i - m_base -> m_width], m_data_term[i + m_base -> m_width], m_spacing_y);
		}
		
		float val = MAX(std::sqrt(SQR(Dx) + SQR(Dy)), EPSILON);
		float Nx = Dx / val;
		float Ny = Dy / val;

		float grad;
		if (m_speed_params.edge_sensitivity_coef == 2)
		{
			grad = 1 / SQR(1 + m_data_term[i]);
		}
		else 
		{
			grad = 1 / (1 + m_data_term[i]);
		}
		
		return (grad * (m_inflation_term[i] + m_speed_params.curvature_term_coef * m_base -> m_curvature[i]) + m_speed_params.data_term_coef * (Nx * IGx + Ny * IGy));
	}

	/////////////////////////////////////////////////////////////////
	//                                                             //
	//               Class Segmentation_SpeedFunction3D            //
	//                                                             //
	/////////////////////////////////////////////////////////////////
	
	Segmentation_SpeedFunction3D::Segmentation_SpeedFunction3D(const RoutineBase *base, const std::valarray<float> &inflation_term, const std::valarray<float> &data_term, 
		                                                       float spacing_x, float spacing_y, float spacing_z, const SpeedFunctionParams &speed_params)
		: Segmentation_SpeedFunction(base, inflation_term, data_term, spacing_x, spacing_y, spacing_z, speed_params)
	{
	}

	float Segmentation_SpeedFunction3D::GetSpeed(int i) const
	{
		int x, y, z;
		m_base -> Get3DCoordinates(i, x, y, z);

		float Dx = 0, Dy = 0, Dz = 0, IGx = 0, IGy = 0, IGz = 0;
		
		if (x > 0 && (x + 1) < m_base -> m_width)
		{
			Dx = CENTRAL(m_base -> m_implicit_function[i - 1], m_base -> m_implicit_function[i + 1], m_spacing_x);
			IGx = CENTRAL(m_data_term[i - 1], m_data_term[i + 1], m_spacing_x);
		}

		if (y > 0 && (y + 1) < m_base -> m_height)
		{
			Dy = CENTRAL(m_base -> m_implicit_function[i - m_base -> m_width], m_base -> m_implicit_function[i + m_base -> m_width], m_spacing_y);
			IGy = CENTRAL(m_data_term [i - m_base -> m_width], m_data_term[i + m_base -> m_width], m_spacing_y);
		}

		if (z > 0 && (z + 1) < m_base -> m_depth)
		{
			Dz = CENTRAL(m_base -> m_implicit_function[i - m_base -> m_width * m_base -> m_height], 
				         m_base -> m_implicit_function[i + m_base -> m_width * m_base -> m_height], m_spacing_z);
			IGz = CENTRAL(m_data_term [i - m_base -> m_width * m_base -> m_height], m_data_term[i + m_base -> m_width * m_base -> m_height], m_spacing_z);
		}

		
		float val = MAX(std::sqrt(SQR(Dx) + SQR(Dy) + SQR(Dz)), EPSILON);
		float Nx = Dx / val;
		float Ny = Dy / val;
		float Nz = Dz / val;

		float grad;
		if (m_speed_params.edge_sensitivity_coef == 2)
		{
			grad = 1 / SQR(1 + m_data_term[i]);
		}
		else 
		{
			grad = 1 / (1 + m_data_term[i]);
		}
		
		return (grad * (m_inflation_term[i] + m_speed_params.curvature_term_coef * m_base -> m_curvature[i]) + m_speed_params.data_term_coef * (Nx * IGx + Ny * IGy + Nz * IGz));
	}

	/////////////////////////////////////////////////////////////////
	//                                                             //
	//                 Class Binary_SpeedFunction                  //
	//                                                             //
	/////////////////////////////////////////////////////////////////

	Binary_SpeedFunction::Binary_SpeedFunction(const RoutineBase *base, const std::valarray<i3d::BINARY> &binary_speed)
		: SpeedFunction(base), m_binary_speed(binary_speed)
	{
	}

	float Binary_SpeedFunction::GetSpeed(int i) const
	{
		float value = static_cast<float>(m_binary_speed[i]);

		return (2 * value - 1);
	}

	/////////////////////////////////////////////////////////////////
	//                                                             //
	//         Class Segmentation_SpeedFunction2D_WithDomain       //
	//                                                             //
	/////////////////////////////////////////////////////////////////

	Segmentation_SpeedFunction2D_WithDomain::Segmentation_SpeedFunction2D_WithDomain(const RoutineBase *base, const i3d::BINARY *domain, 
		const std::valarray<float> &inflation_term, const std::valarray<float> &data_term, float spacing_x, float spacing_y, const SpeedFunctionParams &speed_params)
		: Segmentation_SpeedFunction(base, inflation_term, data_term, spacing_x, spacing_y, 1.0f, speed_params), ComputationalDomain(domain)
	{
	}

	float Segmentation_SpeedFunction2D_WithDomain::GetSpeed(int i) const
	{
		int x, y;
		m_base -> Get2DCoordinates(i, x, y);

		float Dx = 0, Dy = 0, IGx = 0, IGy = 0;
		
		if (x > 0 && (x + 1) < m_base -> m_width && IsInDomain(i - 1) && IsInDomain(i + 1))
		{
			Dx = CENTRAL(m_base -> m_implicit_function[i - 1], m_base -> m_implicit_function[i + 1], m_spacing_x);
			IGx = CENTRAL(m_data_term[i - 1], m_data_term[i + 1], m_spacing_x);
		}

		if (y > 0 && (y + 1) < m_base -> m_height && IsInDomain(i - m_base -> m_width) && IsInDomain(i + m_base -> m_width))
		{
			Dy = CENTRAL(m_base -> m_implicit_function[i - m_base -> m_width], m_base -> m_implicit_function[i + m_base -> m_width], m_spacing_y);
			IGy = CENTRAL(m_data_term [i - m_base -> m_width], m_data_term[i + m_base -> m_width], m_spacing_y);
		}
		
		float val = MAX(std::sqrt(SQR(Dx) + SQR(Dy)), EPSILON);
		float Nx = Dx / val;
		float Ny = Dy / val;

		float grad;
		if (m_speed_params.edge_sensitivity_coef == 2)
		{
			grad = 1 / SQR(1 + m_data_term[i]);
		}
		else 
		{
			grad = 1 / (1 + m_data_term[i]);
		}
		
		return (grad * (m_inflation_term[i] + m_speed_params.curvature_term_coef * m_base -> m_curvature[i]) + m_speed_params.data_term_coef * (Nx * IGx + Ny * IGy));
	}

	/////////////////////////////////////////////////////////////////
	//                                                             //
	//         Class Segmentation_SpeedFunction3D_WithDomain       //
	//                                                             //
	/////////////////////////////////////////////////////////////////

	Segmentation_SpeedFunction3D_WithDomain::Segmentation_SpeedFunction3D_WithDomain(const RoutineBase *base, const i3d::BINARY *domain, 
		const std::valarray<float> &inflation_term, const std::valarray<float> &data_term, float spacing_x, float spacing_y, float spacing_z, 
		const SpeedFunctionParams &speed_params)
		: Segmentation_SpeedFunction(base, inflation_term, data_term, spacing_x, spacing_y, spacing_z, speed_params), ComputationalDomain(domain)
	{
	}

	float Segmentation_SpeedFunction3D_WithDomain::GetSpeed(int i) const
	{
		int x, y, z;
		m_base -> Get3DCoordinates(i, x, y, z);

		float Dx = 0, Dy = 0, Dz = 0, IGx = 0, IGy = 0, IGz = 0;
		
		if (x > 0 && (x + 1) < m_base -> m_width && IsInDomain(i - 1) && IsInDomain(i + 1))
		{
			Dx = CENTRAL(m_base -> m_implicit_function[i - 1], m_base -> m_implicit_function[i + 1], m_spacing_x);
			IGx = CENTRAL(m_data_term[i - 1], m_data_term[i + 1], m_spacing_x);
		}

		if (y > 0 && (y + 1) < m_base -> m_height && IsInDomain(i - m_base -> m_width) && IsInDomain(i + m_base -> m_width))
		{
			Dy = CENTRAL(m_base -> m_implicit_function[i - m_base -> m_width], m_base -> m_implicit_function[i + m_base -> m_width], m_spacing_y);
			IGy = CENTRAL(m_data_term [i - m_base -> m_width], m_data_term[i + m_base -> m_width], m_spacing_y);
		}

		if (z > 0 && (z + 1) < m_base -> m_depth && IsInDomain(i - m_base -> m_width * m_base -> m_height) && IsInDomain(i + m_base -> m_width * m_base -> m_height))
		{
			Dz = CENTRAL(m_base -> m_implicit_function[i - m_base -> m_width * m_base -> m_height], 
				         m_base -> m_implicit_function[i + m_base -> m_width * m_base -> m_height], m_spacing_z);
			IGz = CENTRAL(m_data_term [i - m_base -> m_width * m_base -> m_height], m_data_term[i + m_base -> m_width * m_base -> m_height], m_spacing_z);
		}
		
		float val = MAX(std::sqrt(SQR(Dx) + SQR(Dy) + SQR(Dz)), EPSILON);
		float Nx = Dx / val;
		float Ny = Dy / val;
		float Nz = Dz / val;

		float grad;
		if (m_speed_params.edge_sensitivity_coef == 2)
		{
			grad = 1 / SQR(1 + m_data_term[i]);
		}
		else 
		{
			grad = 1 / (1 + m_data_term[i]);
		}
		
		return (grad * (m_inflation_term[i] + m_speed_params.curvature_term_coef * m_base -> m_curvature[i]) + m_speed_params.data_term_coef * (Nx * IGx + Ny * IGy + Nz * IGz));
	}

	/////////////////////////////////////////////////////////////////
	//                                                             //
	//                        Class Curvature                      //
	//                                                             //
	/////////////////////////////////////////////////////////////////

	Curvature::Curvature(RoutineBase *base, const SpeedFunction *speed_function, int radius)
		: m_base(base), m_speed_function(speed_function), m_radius(radius), m_size(0)
	{
	}
	
	Curvature::~Curvature()
	{
	}
	
	void Curvature::Undefine(int i)
	{
		m_base -> m_curvature[i] = UNDEFINED;
	}

	/////////////////////////////////////////////////////////////////
	//                                                             //
	//                      Class Curvature2D                      //
	//                                                             //
	/////////////////////////////////////////////////////////////////

	Curvature2D::Curvature2D(RoutineBase *base, const SpeedFunction *speed_function, int radius)
		: Curvature(base, speed_function, radius)
	{
		m_size = SQR(2 * radius + 1);
	}

	void Curvature2D::Update(int i, float change)
	{
		int x, y, lx, ux, ly, uy, index;

		m_base -> Get2DCoordinates(i, x, y);

		GetBounds(x, y, lx, ux, ly, uy);

		for (int y = ly; y <= uy; y++)
		{
			for (int x = lx; x <= ux; x++)
			{
				index = y * m_base -> m_width + x;
				if (m_base -> m_back_pointers[index] != -1)
				{
					m_base -> m_curvature[index] += (change / m_size);
					m_base -> m_heap -> Update(m_base -> m_back_pointers[index], m_speed_function -> GetSpeed(index));
				}
			}
		}
	}

	void Curvature2D::Estimate(int i)
	{
		int x, y;
		m_base -> Get2DCoordinates(i, x, y);

		int lx, ux, ly, uy;
		GetBounds(x, y, lx, ux, ly, uy);

		int poc = 0;
		for (int y = ly; y <= uy; y++)
		{
			for (int x = lx; x <= ux; x++)
			{
				if (m_base -> m_implicit_function[y * m_base -> m_width + x] == -1)
				{				
					poc++;
				}
			}
		}

		m_base -> m_curvature[i] = float(poc) / m_size - 0.5f;
	}

	inline void Curvature2D::GetBounds(int x, int y, int &lx, int &ux, int &ly, int &uy)
	{
		lx = std::max(x - m_radius, 0); 
		ux = std::min(x + m_radius, m_base -> m_width - 1);
		ly = std::max(y - m_radius, 0);
		uy = std::min(y + m_radius, m_base -> m_height - 1);
	}

	/////////////////////////////////////////////////////////////////
	//                                                             //
	//                      Class Curvature3D                      //
	//                                                             //
	/////////////////////////////////////////////////////////////////

	Curvature3D::Curvature3D(RoutineBase *base, const SpeedFunction *speed_function, int radius)
		: Curvature(base, speed_function, radius)
	{
		m_size = SQR(2 * radius + 1) * (2 * radius + 1);
	}

	void Curvature3D::Update(int i, float change)
	{
		int x, y, z, lx, ux, ly, uy, lz, uz, index;

		m_base -> Get3DCoordinates(i, x, y, z);

		GetBounds(x, y, z, lx, ux, ly, uy, lz, uz);

		for (int z = lz; z <= uz; z++)
		{
			for (int y = ly; y <= uy; y++)
			{
				for (int x = lx; x <= ux; x++)
				{
					index = (z * m_base -> m_height + y) * m_base -> m_width + x;
					if (m_base -> m_back_pointers[index] != -1)
					{
						m_base -> m_curvature[index] += (change / m_size);
						m_base -> m_heap -> Update(m_base -> m_back_pointers[index], m_speed_function -> GetSpeed(index));
					}
				}
			}
		}
	}

	void Curvature3D::Estimate(int i)
	{
		int x, y, z;
		m_base -> Get3DCoordinates(i, x, y, z);

		int lx, ux, ly, uy, lz, uz;
		GetBounds(x, y, z, lx, ux, ly, uy, lz, uz);

		int poc = 0;
		for (int z = lz; z <= uz; z++)
		{
			for (int y = ly; y <= uy; y++)
			{
				for (int x = lx; x <= ux; x++)
				{
					if (m_base -> m_implicit_function[(z * m_base -> m_height + y) * m_base -> m_width + x] == -1)
					{
						poc++;
					}
				}
			}
		}

		m_base -> m_curvature[i] = float(poc) / m_size - 0.5f;
	}

	inline void Curvature3D::GetBounds(int x, int y, int z, int &lx, int &ux, int &ly, int &uy, int &lz, int &uz)
	{
		lx = std::max(x - m_radius, 0); 
		ux = std::min(x + m_radius, m_base -> m_width - 1);
		ly = std::max(y - m_radius, 0);
		uy = std::min(y + m_radius, m_base -> m_height - 1);
		lz = std::max(z - m_radius, 0);
		uz = std::min(z + m_radius, m_base -> m_depth - 1);
	}

	/////////////////////////////////////////////////////////////////
	//                                                             //
	//                    Class BorderPointDecider                 //
	//                                                             //
	/////////////////////////////////////////////////////////////////

	BorderPointDecider::BorderPointDecider(const RoutineBase *base, const i3d::BINARY *mask)
		: m_base(base), m_mask(mask)
	{
	}

	BorderPointDecider::~BorderPointDecider()
	{
	}

	/////////////////////////////////////////////////////////////////
	//                                                             //
	//                   Class BorderPointDecider2D                //
	//                                                             //
	/////////////////////////////////////////////////////////////////

	BorderPointDecider2D::BorderPointDecider2D(const RoutineBase *base, const i3d::BINARY *mask)
		: BorderPointDecider(base, mask)
	{
	}

	bool BorderPointDecider2D::IsBorder(int i) const
	{
		int x, y;
		m_base -> Get2DCoordinates(i, x, y);

		/*if (x == 0 || (x + 1) == m_base -> m_width || y == 0 || (y + 1) == m_base -> m_height)
		{
			return true;
		}*/

		const i3d::BINARY *ptr = m_mask + i;

		return ((x > 0 && *(ptr - 1) != *ptr) || 
			    ((x + 1) < m_base -> m_width && *(ptr + 1) != *ptr) || 
				(y > 0 && *(ptr - m_base -> m_width) != *ptr) || 
				((y + 1) < m_base -> m_height && *(ptr + m_base -> m_width) != *ptr));
	}

	/////////////////////////////////////////////////////////////////
	//                                                             //
	//                   Class BorderPointDecider3D                //
	//                                                             //
	/////////////////////////////////////////////////////////////////

	BorderPointDecider3D::BorderPointDecider3D(const RoutineBase *base, const i3d::BINARY *mask)
		: BorderPointDecider(base, mask)
	{
	}

	
	bool BorderPointDecider3D::IsBorder(int i) const
	{
		int x, y, z;
		m_base -> Get3DCoordinates(i, x, y, z);

		/*if (x == 0 || (x + 1) == m_base -> m_width || y == 0 || (y + 1) == m_base -> m_height || z == 0 || (z + 1) == m_base -> m_depth)
		{
			return true;
		}*/

		const i3d::BINARY *ptr = m_mask + i;

		return ((x > 0 && *(ptr - 1) != *ptr) || 
			    ((x + 1) < m_base -> m_width && *(ptr + 1) != *ptr) || 
				(y > 0 && *(ptr - m_base -> m_width) != *ptr) || 
				((y + 1) < m_base -> m_height && *(ptr + m_base -> m_width) != *ptr) ||
				(z > 0 && *(ptr - m_base -> m_width * m_base -> m_height) != *ptr) ||
				((z + 1) < m_base -> m_depth && *(ptr + m_base -> m_width * m_base -> m_height) != *ptr));
	}

	/////////////////////////////////////////////////////////////////
	//                                                             //
	//           Class BorderPointDecider2D_WithDomain             //
	//                                                             //
	/////////////////////////////////////////////////////////////////

	BorderPointDecider2D_WithDomain::BorderPointDecider2D_WithDomain(const RoutineBase *base, const i3d::BINARY *mask, const i3d::BINARY *domain)
		: BorderPointDecider(base, mask), ComputationalDomain(domain)
	{
	}
	
	bool BorderPointDecider2D_WithDomain::IsBorder(int i) const
	{
		int x, y;
		m_base -> Get2DCoordinates(i, x, y);

		/*if (x == 0 || (x + 1) == m_base -> m_width || y == 0 || (y + 1) == m_base -> m_height)
		{
			return true;

		}*/

		const i3d::BINARY *ptr = m_mask + i;

		return ((x > 0 && (!IsInDomain(i - 1) || *(ptr - 1) != *ptr)) || 
			    ((x + 1) < m_base -> m_width && (!IsInDomain(i + 1) || *(ptr + 1) != *ptr)) ||
				(y > 0 && (!IsInDomain(i - m_base -> m_width) || *(ptr - m_base -> m_width) != *ptr)) || 
				((y + 1) < m_base -> m_height && (!IsInDomain(i + m_base -> m_width) || *(ptr + m_base -> m_width) != *ptr)));
	}

	/////////////////////////////////////////////////////////////////
	//                                                             //
	//           Class BorderPointDecider3D_WithDomain             //
	//                                                             //
	/////////////////////////////////////////////////////////////////

	BorderPointDecider3D_WithDomain::BorderPointDecider3D_WithDomain(const RoutineBase *base, const i3d::BINARY *mask, const i3d::BINARY *domain)
		: BorderPointDecider(base, mask), ComputationalDomain(domain)
	{
	}
	
	bool BorderPointDecider3D_WithDomain::IsBorder(int i) const
	{
		int x, y, z;
		m_base -> Get3DCoordinates(i, x, y, z);

		/*if (x == 0 || (x + 1) == m_base -> m_width || y == 0 || (y + 1) == m_base -> m_height)
		{
			return true;

		}*/

		const i3d::BINARY *ptr = m_mask + i;

		return ((x > 0 && (!IsInDomain(i - 1) || *(ptr - 1) != *ptr)) || 
			    ((x + 1) < m_base -> m_width && (!IsInDomain(i + 1) || *(ptr + 1) != *ptr)) ||
				(y > 0 && (!IsInDomain(i - m_base -> m_width) || *(ptr - m_base -> m_width) != *ptr)) || 
				((y + 1) < m_base -> m_height && (!IsInDomain(i + m_base -> m_width) || *(ptr + m_base -> m_width) != *ptr)) ||
				(z > 0 && (!IsInDomain(i - m_base -> m_width * m_base -> m_height) || *(ptr - m_base -> m_width * m_base -> m_height) != *ptr)) || 
				((z + 1) < m_base -> m_depth && (!IsInDomain(i + m_base -> m_width * m_base -> m_height) || *(ptr + m_base -> m_width * m_base -> m_height) != *ptr)));
	}

	/////////////////////////////////////////////////////////////////
	//                                                             //
	//                    Class SimplePointDecider                 //
	//                                                             //
	/////////////////////////////////////////////////////////////////

	SimplePointDecider::SimplePointDecider(TopologyPreserving_RoutineBase *base)
		: m_base(base)
	{
	}

	SimplePointDecider::~SimplePointDecider()
	{
	}

	/////////////////////////////////////////////////////////////////
	//                                                             //
	//                  Class SimplePointDecider2D                 //
	//                                                             //
	/////////////////////////////////////////////////////////////////

	SimplePointDecider2D::SimplePointDecider2D(TopologyPreserving_RoutineBase *base)
		: SimplePointDecider(base)
	{
	}

	void SimplePointDecider2D::SetComponent(int i, int value)
	{
		int index;
		m_base -> Get2DComponentIndex(i, index);

		m_base -> m_component[index] = value;
	}

	int SimplePointDecider2D::GetComponent(int i)
	{
		int index;
		m_base -> Get2DComponentIndex(i, index);

		return m_base -> m_component[index];
	}

	bool SimplePointDecider2D::IsSimple(int i)
	{
		if (i == -1)
		{
			return false;
		}

		int index;
		m_base -> Get2DComponentIndex(i, index);

		int val = ((m_base -> m_component[index - m_base -> m_width - 2 - 1] != 0) << 8) +
			      ((m_base -> m_component[index - m_base -> m_width - 2]     != 0) << 7) +
				  ((m_base -> m_component[index - m_base -> m_width - 2 + 1] != 0) << 6) +
				  ((m_base -> m_component[index - 1]                         != 0) << 5) +
				  ((m_base -> m_component[index]                             != 0) << 4) +
				  ((m_base -> m_component[index + 1]                         != 0) << 3) +
				  ((m_base -> m_component[index + m_base -> m_width + 2 - 1] != 0) << 2) +
				  ((m_base -> m_component[index + m_base -> m_width + 2]     != 0) << 1) +
				   (m_base -> m_component[index + m_base -> m_width + 2 + 1] != 0);
		
		return is_simple[(row_index[val >> 5] << 4) + val % 16];
	}

	/////////////////////////////////////////////////////////////////
	//                                                             //
	//                       Local Definitions                     //
	//                                                             //
	/////////////////////////////////////////////////////////////////

	#define ADD_2(a,b, val) graph[a].Add(val); graph[b].Add(val);

	#define ADD_4(a,b,c,d, val) graph[a].Add(val); graph[b].Add(val); graph[c].Add(val); graph[d].Add(val);

	#define ADD_6(a,b,c,d,e,f, val) graph[a].Add(val); graph[b].Add(val); graph[c].Add(val); \
	                                graph[d].Add(val); graph[e].Add(val); graph[f].Add(val);

	#define ADD_10(a,b,c,d,e,f,g,h,i,j, val) graph[a].Add(val); graph[b].Add(val); graph[c].Add(val); graph[d].Add(val); \
	                                         graph[e].Add(val); graph[f].Add(val); graph[g].Add(val); graph[h].Add(val); \
											 graph[i].Add(val); graph[j].Add(val);
	#define ADD_16(a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p, val) graph[a].Add(val); graph[b].Add(val); graph[c].Add(val); \
	                                                     graph[d].Add(val); graph[e].Add(val); graph[f].Add(val); \
														 graph[g].Add(val); graph[h].Add(val); graph[i].Add(val); \
														 graph[j].Add(val); graph[k].Add(val); graph[l].Add(val); \
														 graph[m].Add(val); graph[n].Add(val); graph[o].Add(val); \
														 graph[p].Add(val);

	/** A structure for representing a graph node in 3x3x3 voxels cube for 6-neighbourhood. */
	struct GraphNode6
	{
		int num;
		int neigh[4];

		/** Constructor. */
		GraphNode6() : num(0) {};

		void Add(int value) { neigh[num++] = value; };
	};
	
	/** A structure for representing a graph node in 3x3x3 voxels cube for 26-neighbourhood. */
	struct GraphNode26
	{
		int num;
		int neigh[16];

		/** Constructor. */
		GraphNode26() : num(0) {};

		void Add(int value) { neigh[num++] = value; };
	};

	/////////////////////////////////////////////////////////////////
	//                                                             //
	//                        Local Functions                      //
	//                                                             //
	/////////////////////////////////////////////////////////////////

	template <class T> static bool IsConnected(std::map<int, T> &graph, int start, int num)
	{
		int pom = 0, index = 0;
		bool visited[26];
		int queue[26];

		for (size_t i = 0; i < 26; i++)
		{
			visited[i] = false;
		}

		visited[start] = true;
		queue[pom++] = start;

		while (index < pom)
		{
			const T &node = graph[queue[index]];

			for (int i = 0; i < node.num; i++)
			{
				if (visited[node.neigh[i]] == false)
				{
					visited[node.neigh[i]] = true;
					queue[pom++] = node.neigh[i];
				}
			}

			index++;
		}

		return (pom == num);
	}

	static bool IsCorrect26(int neigh[26])
	{
		int start = -1;
		int num = 0;

		std::map<int, GraphNode26> graph;
		
		for (size_t i = 0; i < 26; i++)
		{
			graph[i] = GraphNode26();
		}

		if (neigh[0]  == 1) { num++; start =  0; ADD_6(1,3,4,9,10,12, 0); }
		if (neigh[1]  == 1) { num++; start =  1; ADD_10(0,2,3,4,5,9,10,11,12,13, 1); }
		if (neigh[2]  == 1) { num++; start =  2; ADD_6(1,4,5,10,11,13, 2); }
		if (neigh[3]  == 1) { num++; start =  3; ADD_10(0,1,4,6,7,9,10,12,14,15, 3); }
		if (neigh[4]  == 1) { num++; start =  4; ADD_16(0,1,2,3,5,6,7,8,9,10,11,12,13,14,15,16, 4); }
		if (neigh[5]  == 1) { num++; start =  5; ADD_10(1,2,4,7,8,10,11,13,15,16, 5); }
		if (neigh[6]  == 1) { num++; start =  6; ADD_6(3,4,7,12,14,15, 6); }
		if (neigh[7]  == 1) { num++; start =  7; ADD_10(3,4,5,6,8,12,13,14,15,16, 7); }
		if (neigh[8]  == 1) { num++; start =  8; ADD_6(4,5,7,13,15,16, 8); }

		if (neigh[9]  == 1) { num++; start =  9; ADD_10(0,1,3,4,10,12,17,18,20,21, 9); }
		if (neigh[10] == 1) { num++; start = 10; ADD_16(0,1,2,3,4,5,9,11,12,13,17,18,19,20,21,22, 10); }
		if (neigh[11] == 1) { num++; start = 11; ADD_10(1,2,4,5,10,13,18,19,21,22, 11); }
		if (neigh[12] == 1) { num++; start = 12; ADD_16(0,1,3,4,6,7,9,10,14,15,17,18,20,21,23,24, 12); }
		if (neigh[13] == 1) { num++; start = 13; ADD_16(1,2,4,5,7,8,10,11,15,16,18,19,21,22,24,25, 13); }
		if (neigh[14] == 1) { num++; start = 14; ADD_10(3,4,6,7,12,15,20,21,23,24, 14); }
		if (neigh[15] == 1) { num++; start = 15; ADD_16(3,4,5,6,7,8,12,13,14,16,20,21,22,23,24,25, 15); }
		if (neigh[16] == 1) { num++; start = 16; ADD_10(4,5,7,8,13,15,21,22,24,25, 16); }

		if (neigh[17] == 1) { num++; start = 17; ADD_6(9,10,12,18,20,21, 17); }
		if (neigh[18] == 1) { num++; start = 18; ADD_10(9,10,11,12,13,17,19,20,21,22, 18); }
		if (neigh[19] == 1) { num++; start = 19; ADD_6(10,11,13,18,21,22, 19); }
		if (neigh[20] == 1) { num++; start = 20; ADD_10(9,10,12,14,15,17,18,21,23,24, 20); }
		if (neigh[21] == 1) { num++; start = 21; ADD_16(9,10,11,12,13,14,15,16,17,18,19,20,22,23,24,25, 21); }
		if (neigh[22] == 1) { num++; start = 22; ADD_10(10,11,13,15,16,18,19,21,24,25, 22); }
		if (neigh[23] == 1) { num++; start = 23; ADD_6(12,14,15,20,21,24, 23); }
		if (neigh[24] == 1) { num++; start = 24; ADD_10(12,13,14,15,16,20,21,22,23,25, 24); }
		if (neigh[25] == 1) { num++; start = 25; ADD_6(13,15,16,21,22,24, 25); }

		if (start == -1)
		{
			return false;
		}

		return IsConnected(graph, start, num);
	}

	static bool IsCorrect6(int neigh[26])
	{
		int start = -1;
		int num6 = 0;
		int sum = 0;
		int num = 0;

		std::map<int, GraphNode6> graph;

		for (size_t i = 0; i < 18; i++)
		{
			graph[i] = GraphNode6();
		}

		if (neigh[1]  == 0) { num++; ADD_2(2,6, 0); }
		if (neigh[3]  == 0) { num++; ADD_2(2,8, 1); }
		if (neigh[4]  == 0) { num++; sum += 2; num6++; start = 2; ADD_4(0,1,3,4, 2); }
		if (neigh[5]  == 0) { num++; ADD_2(2,9, 3); }
		if (neigh[7]  == 0) { num++; ADD_2(2,11, 4); }
		if (neigh[9]  == 0) { num++; ADD_2(6,8, 5); }
		if (neigh[10] == 0) { num++; sum += 6; num6++; start = 6; ADD_4(0,5,7,13, 6); }
		if (neigh[11] == 0) { num++; ADD_2(6,9, 7); }
		if (neigh[12] == 0) { num++; sum += 8; num6++; start = 8; ADD_4(1,5,10,14, 8); }
		if (neigh[13] == 0) { num++; sum += 9; num6++; start = 9; ADD_4(3,7,12,16, 9); }
		if (neigh[14] == 0) { num++; ADD_2(8,11, 10); }
		if (neigh[15] == 0) { num++; sum += 11; num6++; start = 11; ADD_4(4,10,12,17, 11); }
		if (neigh[16] == 0) { num++; ADD_2(9,11, 12); }
		if (neigh[18] == 0) { num++; ADD_2(6,15, 13); }
		if (neigh[20] == 0) { num++; ADD_2(8,15, 14); }
		if (neigh[21] == 0) { num++; sum += 15; num6++; start = 15; ADD_4(13,14,16,17, 15); }
		if (neigh[22] == 0) { num++; ADD_2(9,15, 16); }
		if (neigh[24] == 0) { num++; ADD_2(11,15, 17); }

		if (start == -1) 
		{
			// situation number one
			return false;
		}

		if (num6 == 1)
		{
			// situation number two 
			return true;
		}

		if (num6 == 2)
		{
			if (sum == 17)
			{
				// situation number three - the first case
				return false;
			}
			else
			{
				// situation number three - the second case
				switch (sum)
				{
  				  case  8: return neigh[1] == 0;
				  case 10: return neigh[3] == 0;
				  case 11: return neigh[5] == 0;
				  case 13: return neigh[7] == 0;
				  case 14: return neigh[9] == 0;
				  case 15: return neigh[11] == 0;
				  case 19: return neigh[14] == 0;
				  case 20: return neigh[16] == 0;
				  case 21: return neigh[18] == 0;
				  case 23: return neigh[20] == 0;
				  case 24: return neigh[22] == 0;
				  case 26: return neigh[24] == 0;
				}
			}
		}

		if (IsConnected(graph, start, num))
		{
			return IsCorrect26(neigh);	
		}
		else
		{
			return false;
		}
	}

	/////////////////////////////////////////////////////////////////
	//                                                             //
	//                  Class SimplePointDecider3D                 //
	//                                                             //
	/////////////////////////////////////////////////////////////////

	SimplePointDecider3D::SimplePointDecider3D(TopologyPreserving_RoutineBase *base)
		: SimplePointDecider(base)
	{
	}

	void SimplePointDecider3D::SetComponent(int i, int value)
	{
		int index;
		m_base -> Get3DComponentIndex(i, index);

		m_base -> m_component[index] = value;
	}

	int SimplePointDecider3D::GetComponent(int i)
	{
		int index;
		m_base -> Get3DComponentIndex(i, index);

		return m_base -> m_component[index];
	}

	bool SimplePointDecider3D::IsSimple(int i)
	{
		if (i == -1)
		{
			return false;
		}

		int index;
		m_base -> Get3DComponentIndex(i, index);

		int step_x = 1;
		int step_y = m_base -> m_width + 2;
		int step_z = (m_base -> m_width + 2) * (m_base -> m_height + 2);
		int component_value = m_base -> m_component[index];

		// get 26-neighbourhood of given point
		int neigh[26];
		neigh[0]  = m_base -> m_component[index - step_z - step_y - step_x] == component_value;
		neigh[1]  = m_base -> m_component[index - step_z - step_y         ] == component_value;
		neigh[2]  = m_base -> m_component[index - step_z - step_y + step_x] == component_value;
		neigh[3]  = m_base -> m_component[index - step_z          - step_x] == component_value;
		neigh[4]  = m_base -> m_component[index - step_z                  ] == component_value;
		neigh[5]  = m_base -> m_component[index - step_z          + step_x] == component_value;
		neigh[6]  = m_base -> m_component[index - step_z + step_y - step_x] == component_value;
		neigh[7]  = m_base -> m_component[index - step_z + step_y         ] == component_value;
		neigh[8]  = m_base -> m_component[index - step_z + step_y + step_x] == component_value;

		neigh[9]  = m_base -> m_component[index          - step_y - step_x] == component_value;
		neigh[10] = m_base -> m_component[index          - step_y         ] == component_value;
		neigh[11] = m_base -> m_component[index          - step_y + step_x] == component_value;
		neigh[12] = m_base -> m_component[index                   - step_x] == component_value;
		neigh[13] = m_base -> m_component[index                   + step_x] == component_value;
		neigh[14] = m_base -> m_component[index          + step_y - step_x] == component_value;
		neigh[15] = m_base -> m_component[index          + step_y         ] == component_value;
		neigh[16] = m_base -> m_component[index          + step_y + step_x] == component_value;

		neigh[17] = m_base -> m_component[index + step_z - step_y - step_x] == component_value;
		neigh[18] = m_base -> m_component[index + step_z - step_y         ] == component_value;
		neigh[19] = m_base -> m_component[index + step_z - step_y + step_x] == component_value;
		neigh[20] = m_base -> m_component[index + step_z          - step_x] == component_value;
		neigh[21] = m_base -> m_component[index + step_z                  ] == component_value;
		neigh[22] = m_base -> m_component[index + step_z          + step_x] == component_value;
		neigh[23] = m_base -> m_component[index + step_z + step_y - step_x] == component_value;
		neigh[24] = m_base -> m_component[index + step_z + step_y         ] == component_value;
		neigh[25] = m_base -> m_component[index + step_z + step_y + step_x] == component_value;
		
		return IsCorrect6(neigh);
	}

	/////////////////////////////////////////////////////////////////
	//                                                             //
	//                        Class Initiator                      //
	//                                                             //
	/////////////////////////////////////////////////////////////////

	Initiator::Initiator(RoutineBase *base, const SpeedFunction *speed_function, Curvature *curvature, const BorderPointDecider *border_point_decider)
		: m_base(base), m_speed_function(speed_function), m_curvature(curvature), m_border_point_decider(border_point_decider)
	{
	}

	Initiator::~Initiator()
	{
		if (m_border_point_decider)
		{
			delete m_border_point_decider;
		}
	}

	/////////////////////////////////////////////////////////////////
	//                                                             //
	//                  Class Standard_Initiator                   //
	//                                                             //
	/////////////////////////////////////////////////////////////////

	Standard_Initiator::Standard_Initiator(RoutineBase *base, const SpeedFunction *speed_function, Curvature *curvature, 
										   const BorderPointDecider *border_point_decider, const i3d::BINARY *mask)
		: Initiator(base, speed_function, curvature, border_point_decider), m_mask(mask)
	{
	}

	void Standard_Initiator::Initialize()
	{
		std::vector<int> init_interface;
		int num = m_base -> m_width * m_base -> m_height * m_base -> m_depth;

		const i3d::BINARY *ptr = m_mask;

		for (int i = 0; i < num; i++)
		{
			if (*ptr == std::numeric_limits<i3d::BINARY>::max())
			{
				if (m_border_point_decider -> IsBorder(i))
				{
					m_base -> m_implicit_function[i] = 0;
					init_interface.push_back(i);
				}
				else
				{
					m_base -> m_implicit_function[i] = -1;
				}
			}

			ptr++;
		}

		for (size_t i = 0; i < init_interface.size(); i++)
		{
			m_curvature -> Estimate(init_interface[i]);
			m_base -> m_heap -> Insert(init_interface[i], 0, m_speed_function -> GetSpeed(init_interface[i]));
		}
	}

	/////////////////////////////////////////////////////////////////
	//                                                             //
	//             Class Standard_Initiator_WithDomain             //
	//                                                             //
	/////////////////////////////////////////////////////////////////

	Standard_Initiator_WithDomain::Standard_Initiator_WithDomain(RoutineBase *base, const SpeedFunction *speed_function, Curvature *curvature, 
		const BorderPointDecider *border_point_decider, const i3d::BINARY *mask, const i3d::BINARY *domain)
		: Initiator(base, speed_function, curvature, border_point_decider), ComputationalDomain(domain), m_mask(mask)
	{
	}

	void Standard_Initiator_WithDomain::Initialize()
	{
		std::vector<int> init_interface;
		int num = m_base -> m_width * m_base -> m_height * m_base -> m_depth;

		const i3d::BINARY *ptr = m_mask;

		for (int i = 0; i < num; i++)
		{
			if (*ptr == std::numeric_limits<i3d::BINARY>::max() && IsInDomain(i))
			{
				if (m_border_point_decider -> IsBorder(i))
				{
					m_base -> m_implicit_function[i] = 0;
					init_interface.push_back(i);
				}
				else
				{
					m_base -> m_implicit_function[i] = -1;
				}
			}

			ptr++;
		}

		for (size_t i = 0; i < init_interface.size(); i++)
		{
			m_curvature -> Estimate(init_interface[i]);
			m_base -> m_heap -> Insert(init_interface[i], 0, m_speed_function -> GetSpeed(init_interface[i]));
		}
	}

	/////////////////////////////////////////////////////////////////
	//                                                             //
	//             Class TopologyPreserving_Initiator              //
	//                                                             //
	/////////////////////////////////////////////////////////////////

	TopologyPreserving_Initiator::TopologyPreserving_Initiator(RoutineBase *base, const SpeedFunction *speed_function, Curvature *curvature, 
															   const BorderPointDecider *border_point_decider, SimplePointDecider *simple_point_decider,
															   const i3d::Image3d<i3d::BINARY> *mask)
		: Initiator(base, speed_function, curvature, border_point_decider), m_mask(mask), m_simple_point_decider(simple_point_decider)
	{
	}

	void TopologyPreserving_Initiator::Initialize()
	{
		std::vector<int> init_interface;
		int num = m_base -> m_width * m_base -> m_height * m_base -> m_depth;

		i3d::LabeledImage3d<i3d::GRAY16, i3d::BINARY> limg;
		limg.CreateForegroundRegionsFF(*m_mask);

		const i3d::BINARY *mask_ptr = m_mask -> GetFirstVoxelAddr();
		const i3d::GRAY16 *lab_ptr = limg.GetFirstVoxelAddr();

		for (int i = 0; i < num; i++)
		{
			if (*mask_ptr == std::numeric_limits<i3d::BINARY>::max())
			{
				m_simple_point_decider -> SetComponent(i, *lab_ptr);

				if (m_border_point_decider -> IsBorder(i))
				{
					m_base -> m_implicit_function[i] = 0;
					init_interface.push_back(i);
				}
				else
				{
					m_base -> m_implicit_function[i] = -1;
				}
			}

			mask_ptr++;
			lab_ptr++;
		}

		for (size_t i = 0; i < init_interface.size(); i++)
		{
			m_curvature -> Estimate(init_interface[i]);
			m_base -> m_heap -> Insert(init_interface[i], 0, m_speed_function -> GetSpeed(init_interface[i]));
		}
	}

	/////////////////////////////////////////////////////////////////
	//                                                             //
	//       Class TopologyPreserving_Initiator_WithDomain         //
	//                                                             //
	/////////////////////////////////////////////////////////////////

	TopologyPreserving_Initiator_WithDomain::TopologyPreserving_Initiator_WithDomain(RoutineBase *base, const SpeedFunction *speed_function, Curvature *curvature, 
		const BorderPointDecider *border_point_decider, SimplePointDecider *simple_point_decider, const i3d::Image3d<i3d::BINARY> *mask, const i3d::BINARY *domain)
		: Initiator(base, speed_function, curvature, border_point_decider), ComputationalDomain(domain), m_mask(mask), m_simple_point_decider(simple_point_decider)
	{
	}

	void TopologyPreserving_Initiator_WithDomain::Initialize()
	{
		std::vector<int> init_interface;
		int num = m_base -> m_width * m_base -> m_height * m_base -> m_depth;

		i3d::LabeledImage3d<i3d::GRAY16, i3d::BINARY> limg;
		limg.CreateForegroundRegionsFF(*m_mask);

		const i3d::BINARY *mask_ptr = m_mask -> GetFirstVoxelAddr();
		const i3d::GRAY16 *lab_ptr = limg.GetFirstVoxelAddr();

		for (int i = 0; i < num; i++)
		{
			if (*mask_ptr == std::numeric_limits<i3d::BINARY>::max() && IsInDomain(i))
			{
				m_simple_point_decider -> SetComponent(i, *lab_ptr);

				if (m_border_point_decider -> IsBorder(i))
				{
					m_base -> m_implicit_function[i] = 0;
					init_interface.push_back(i);
				}
				else
				{
					m_base -> m_implicit_function[i] = -1;
				}
			}

			mask_ptr++;
			lab_ptr++;
		}

		for (size_t i = 0; i < init_interface.size(); i++)
		{
			m_curvature -> Estimate(init_interface[i]);
			m_base -> m_heap -> Insert(init_interface[i], 0, m_speed_function -> GetSpeed(init_interface[i]));
		}
	}

	/////////////////////////////////////////////////////////////////
	//                                                             //
	//                    Class ResultCreator                      //
	//                                                             //
	/////////////////////////////////////////////////////////////////
	
	ResultCreator::ResultCreator(const RoutineBase *base, const i3d::Offset &offset, const i3d::Resolution &resolution)
		: m_base(base), m_offset(offset), m_resolution(resolution)
	{
	}

	ResultCreator::~ResultCreator()
	{
	}

	/////////////////////////////////////////////////////////////////
	//                                                             //
	//              Class Segmentation_ResultCreator               //
	//                                                             //
	/////////////////////////////////////////////////////////////////

	Segmentation_ResultCreator::Segmentation_ResultCreator(const RoutineBase *base, const i3d::Offset &offset, const i3d::Resolution &resolution)
		: ResultCreator(base, offset, resolution)
	{
	}

	void Segmentation_ResultCreator::GetResult(i3d::Image3d<i3d::BINARY> &output, ResultType type)
	{
		output.SetOffset(m_offset);
		output.SetResolution(m_resolution);
		output.MakeRoom(m_base -> m_width, m_base -> m_height, m_base -> m_depth);

		int num = m_base -> m_width * m_base -> m_height * m_base -> m_depth;
		i3d::BINARY *ptr = output.GetFirstVoxelAddr();

		switch (type)
		{
			case ResultType_Interior:

				for (int i = 0; i < num; i++)
				{
					*ptr = m_base -> m_implicit_function[i] == -1 ? std::numeric_limits<i3d::BINARY>::max() : 
						                                            std::numeric_limits<i3d::BINARY>::min();
					ptr++;
				}

				break;

			case ResultType_Interface:

				for (int i = 0; i < num; i++)
				{
					*ptr = m_base -> m_implicit_function[i] == 0 ? std::numeric_limits<i3d::BINARY>::max() : 
																   std::numeric_limits<i3d::BINARY>::min();
					ptr++;
				}

				break;

			case ResultType_Exterior:

				for (int i = 0; i < num; i++)
				{
					*ptr = m_base -> m_implicit_function[i] == 1 ? std::numeric_limits<i3d::BINARY>::max() : 
						                                           std::numeric_limits<i3d::BINARY>::min();
					ptr++;
				}

				break;

			case ResultType_InteriorAndInterface:

				for (int i = 0; i < num; i++)
				{
					*ptr = m_base -> m_implicit_function[i] < 1 ? std::numeric_limits<i3d::BINARY>::max() : 
						                                          std::numeric_limits<i3d::BINARY>::min();
					ptr++;
				}

				break;

			case ResultType_ExteriorAndInterface:

				for (int i = 0; i < num; i++)
				{
					*ptr = m_base -> m_implicit_function[i] > -1 ? std::numeric_limits<i3d::BINARY>::max() : 
						                                           std::numeric_limits<i3d::BINARY>::min();
					ptr++;
				}

				break;
		}
	}

	/////////////////////////////////////////////////////////////////
	//                                                             //
	//         Class Segmentation_ResultCreator_WithDomain         //
	//                                                             //
	/////////////////////////////////////////////////////////////////

	Segmentation_ResultCreator_WithDomain::Segmentation_ResultCreator_WithDomain(const RoutineBase *base, const i3d::Offset &offset, const i3d::Resolution &resolution, 
																				 const i3d::BINARY *domain)
		: ResultCreator(base, offset, resolution), ComputationalDomain(domain)
	{
	}

	void Segmentation_ResultCreator_WithDomain::GetResult(i3d::Image3d<i3d::BINARY> &output, ResultType type)
	{
		output.SetOffset(m_offset);
		output.SetResolution(m_resolution);
		output.MakeRoom(m_base -> m_width, m_base -> m_height, m_base -> m_depth);

		int num = m_base -> m_width * m_base -> m_height * m_base -> m_depth;
		i3d::BINARY *ptr = output.GetFirstVoxelAddr();

		switch (type)
		{
			case ResultType_Interior:

				for (int i = 0; i < num; i++)
				{
					*ptr = (m_base -> m_implicit_function[i] == -1 && IsInDomain(i)) ? 
										std::numeric_limits<i3d::BINARY>::max() : 
										std::numeric_limits<i3d::BINARY>::min();
					ptr++;
				}

				break;

			case ResultType_Interface:

				for (int i = 0; i < num; i++)
				{
					*ptr = (m_base -> m_implicit_function[i] == 0 && IsInDomain(i)) ? 
										std::numeric_limits<i3d::BINARY>::max() : 
										std::numeric_limits<i3d::BINARY>::min();
					ptr++;
				}

				break;

			case ResultType_Exterior:

				for (int i = 0; i < num; i++)
				{
					*ptr = (m_base -> m_implicit_function[i] == 1 && IsInDomain(i)) ? 
										std::numeric_limits<i3d::BINARY>::max() : 
										std::numeric_limits<i3d::BINARY>::min();
					ptr++;
				}

				break;

			case ResultType_InteriorAndInterface:

				for (int i = 0; i < num; i++)
				{
					*ptr = (m_base -> m_implicit_function[i] < 1 && IsInDomain(i)) ? 
										std::numeric_limits<i3d::BINARY>::max() : 
										std::numeric_limits<i3d::BINARY>::min();
					ptr++;
				}

				break;

			case ResultType_ExteriorAndInterface:

				for (int i = 0; i < num; i++)
				{
					*ptr = (m_base -> m_implicit_function[i] > -1 && IsInDomain(i)) ? 
										std::numeric_limits<i3d::BINARY>::max() : 
										std::numeric_limits<i3d::BINARY>::min();
					ptr++;
				}

				break;
		}
	}

	/////////////////////////////////////////////////////////////////
	//                                                             //
	//                    Class SwedenLevelSetRoutine              //
	//                                                             //
	/////////////////////////////////////////////////////////////////

	SwedenLevelSetRoutine::SwedenLevelSetRoutine(RoutineBase *base, Neighbourhood *neighbourhood, SpeedFunction *speed_function, 
		                                         Curvature *curvature, Initiator *initiator, ResultCreator *result_creator)
		: m_base(base), m_neighbourhood(neighbourhood), m_speed_function(speed_function), m_curvature(curvature), m_initiator(initiator), m_result_creator(result_creator)
	{
	}

	SwedenLevelSetRoutine::~SwedenLevelSetRoutine()
	{
		if (m_base)
		{
			delete m_base;
		}

		if (m_neighbourhood)
		{
			delete m_neighbourhood;
		}

		if (m_speed_function)
		{
			delete m_speed_function;
		}

		if (m_curvature)
		{
			delete m_curvature;
		}

		if (m_initiator)
		{
			delete m_initiator;
		}

		if (m_result_creator)
		{
			delete m_result_creator;
		}
	}

	void SwedenLevelSetRoutine::Initialize()
	{
		m_base -> Initialize();
		m_initiator -> Initialize();
	}

	size_t SwedenLevelSetRoutine::GetInterfaceSize()
	{
		return m_base -> m_heap -> HeapSize();
	}

	bool SwedenLevelSetRoutine::IsOk()
	{
		return (m_base -> m_heap -> HeapSize() != 0);
	}

	void SwedenLevelSetRoutine::Iterate(int num_iterations)
	{
		int i = 0;

		while (IsOk() && i < num_iterations)
		{
			ComputeIteration();
			i++;
		}
	}

	const int *SwedenLevelSetRoutine::GetImplicitFunction()
	{
		return &(m_base -> m_implicit_function[0]);
	}

	void SwedenLevelSetRoutine::GetResult(i3d::Image3d<i3d::BINARY> &output, ResultType type)
	{
		m_result_creator -> GetResult(output, type);
	}

	/////////////////////////////////////////////////////////////////
	//                                                             //
	//             Class Standard_SwedenLevelSetRoutine            //
	//                                                             //
	/////////////////////////////////////////////////////////////////
	
	Standard_SwedenLevelSetRoutine::Standard_SwedenLevelSetRoutine(RoutineBase *base, Neighbourhood *neighbourhood, SpeedFunction *speed_function, 
																   Curvature *curvature, Initiator *initiator, ResultCreator *result_creator)
		: SwedenLevelSetRoutine(base, neighbourhood, speed_function, curvature, initiator, result_creator)
	{
	}

	void Standard_SwedenLevelSetRoutine::ComputeIteration()
	{
		SwedenHeapNode *min = m_base -> m_heap -> Pop();

		if (!min)
		{
			throw i3d::InternalException("Cannot pop from the empty heap!");
		}

		int neigh_type = min -> speed > 0 ? 1 : -1; // use exterior or interior neighbors?

		int num_neigh = 0;
		const int *neigh = m_neighbourhood -> GetNeighbors(min -> position, neigh_type, num_neigh);

		m_base -> m_implicit_function[min -> position] = -neigh_type;
		m_curvature -> Undefine(min -> position);

		if (neigh_type == 1)
		{
			m_curvature -> Update(min -> position, 1.0f);
		}

		for (int i = 0; i < num_neigh; i++)
		{
			m_base -> m_implicit_function[neigh[i]] = 0;
			m_curvature -> Estimate(neigh[i]);

			if (neigh_type == -1)
			{
				m_curvature -> Update(neigh[i], -1.0f);
			}

			m_base -> m_heap -> Insert(neigh[i], min -> departure_time, m_speed_function -> GetSpeed(neigh[i]));
		}

		delete min;
	}

	/////////////////////////////////////////////////////////////////
	//                                                             //
	//        Class TopologyPreserving_SwedenLevelSetRoutine       //
	//                                                             //
	/////////////////////////////////////////////////////////////////
	
	TopologyPreserving_SwedenLevelSetRoutine::TopologyPreserving_SwedenLevelSetRoutine(TopologyPreserving_RoutineBase *base, Neighbourhood *neighbourhood, SpeedFunction *speed_function, 
																					   Curvature *curvature, Initiator *initiator, ResultCreator *result_creator, 
																					   SimplePointDecider *simple_point_decider)
		: SwedenLevelSetRoutine(base, neighbourhood, speed_function, curvature, initiator, result_creator), m_simple_point_decider(simple_point_decider)
	{
	}

	TopologyPreserving_SwedenLevelSetRoutine::~TopologyPreserving_SwedenLevelSetRoutine()
	{
		if (m_simple_point_decider)
		{
			delete m_simple_point_decider;
		}
	}

	void TopologyPreserving_SwedenLevelSetRoutine::ComputeIteration()
	{
		SwedenHeapNode *min = m_base -> m_heap -> Pop();

		if (!min)
		{
			throw i3d::InternalException("Cannot pop from the empty heap!");
		}

		int neigh_type = min -> speed > 0 ? 1 : -1; // use exterior or interior neighbors?

		int num_neigh = 0;
		const int *neigh = m_neighbourhood -> GetNeighbors(min -> position, neigh_type, num_neigh);

		int num = 0;
		int *simple_neigh = NULL;

		if (neigh_type == -1 && !m_simple_point_decider -> IsSimple(min -> position))
		{
			m_base -> m_heap -> Insert(min -> position, min -> departure_time, m_speed_function -> GetSpeed(min -> position));
			delete min;
			return;
		}

		// topology test
		if (num_neigh > 0)
		{
			/*if (!m_simple_point_decider -> IsSimple(min -> position))
			{
				m_base -> m_heap -> Insert(min -> position, min -> departure_time, m_speed_function -> GetSpeed(min -> position));
				delete min;
				return;
			}*/

			simple_neigh = new int[num_neigh];
			for (int i = 0; i < num_neigh; i++)
			{
				if ((neigh_type == -1 && neigh[i] != -1) || (neigh_type == 1 && m_simple_point_decider -> IsSimple(neigh[i])))
				{
					simple_neigh[num++] = neigh[i];
				}
			}
		}

		int comp_num = m_simple_point_decider -> GetComponent(min -> position); // the value of component 

		if (num_neigh == num)
		{
			m_base -> m_implicit_function[min -> position] = -neigh_type;
			m_curvature -> Undefine(min -> position);

			if (neigh_type == 1)
			{
				m_curvature -> Update(min -> position, 1.0f);
			}
			else
			{
				m_simple_point_decider -> SetComponent(min -> position, 0);
			}
		}
		else
		{
			m_base -> m_heap -> Insert(min -> position, min -> departure_time, m_speed_function -> GetSpeed(min -> position));
		}
		
		for (int i = 0; i < num; i++)
		{
			m_base -> m_implicit_function[simple_neigh[i]] = 0;
			m_curvature -> Estimate(simple_neigh[i]);

			m_simple_point_decider -> SetComponent(simple_neigh[i], comp_num);

			if (neigh_type == -1)
			{
				m_curvature -> Update(simple_neigh[i], -1.0f);
			}

			m_base -> m_heap -> Insert(simple_neigh[i], min -> departure_time, m_speed_function -> GetSpeed(simple_neigh[i]));
		}

		if (simple_neigh)
		{
			delete [] simple_neigh;
		}
			
		delete min;
	}

	template <class T> SpeedFunction *CreateSegmentationSpeedFunction(const RoutineBase *base, const i3d::Image3d<T> &input, const i3d::Image3d<i3d::BINARY> *domain, 
		                                                              float sigma, const SpeedFunctionParams &params, bool use_distance)
	{
		i3d::Image3d<float> ImgPom, ImgG;
		i3d::GrayToFloat(input, ImgPom);
		i3d::GaussIIR(ImgPom, sigma);

		i3d::Vector3d<float> res = input.GetResolution().GetRes();
		float spacing_x = 1.0f / res.x;
		float spacing_y = 1.0f / res.y;
		float spacing_z = 1.0f / res.z;

		//std::valarray<float> inflation;
		//inflation.resize(input.GetImageSize(), params.inflation_term_coef);

		i3d::Image3d<float> inflation;
		inflation.MakeRoom(input.GetSize());
		float *inflation_ptr = inflation.GetFirstVoxelAddr();
		
		if (domain && use_distance)
		{
			const i3d::BINARY *domain_ptr = domain -> GetFirstVoxelAddr();

			for (size_t i = 0; i < inflation.GetImageSize(); i++)
			{
				*inflation_ptr = (*domain_ptr == std::numeric_limits<i3d::BINARY>::max() ? 1.0f : 0.0f);

				inflation_ptr++;
				domain_ptr++;
			}

			//inflation.SetResolution(i3d::Resolution(10.0, 10.0, 10.0));
			//inflation.SetResolution(res);
			i3d::HybridSaito(inflation, 0, 1.0f);

			/*for (size_t i = 0; i < inflation.GetImageSize(); i++)
			{
				float val = inflation.GetVoxel(i);
				inflation.SetVoxel(i, val * val);
			}*/
		}
		else
		{
			for (size_t i = 0; i < inflation.GetImageSize(); i++)
			{
				//float value = input.GetVoxel(i) > 0 ? 1.0 : 0.0;
				//*inflation_ptr = 2 * value - 1.0;
				*inflation_ptr = params.inflation_term_coef;
				inflation_ptr++;
			}
		}

		if (input.GetSizeZ() == 1)
		{
			i3d::EstimateGradient2_2D(ImgPom, ImgG, true);

			if (domain)
			{
				return new Segmentation_SpeedFunction2D_WithDomain(base, domain -> GetFirstVoxelAddr(), inflation.GetVoxelData(), ImgG.GetVoxelData(), 
					                                               spacing_x, spacing_y, params);
			}
			else
			{
				return new Segmentation_SpeedFunction2D(base, inflation.GetVoxelData(), ImgG.GetVoxelData(), spacing_x, spacing_y, params);
			}
		}
		else
		{
			i3d::EstimateGradient2_3D(ImgPom, ImgG, true);

			if (domain)
			{
				return new Segmentation_SpeedFunction3D_WithDomain(base, domain -> GetFirstVoxelAddr(), inflation.GetVoxelData(), ImgG.GetVoxelData(), 
																   spacing_x, spacing_y, spacing_z, params);
			}
			else
			{
				return new Segmentation_SpeedFunction3D(base, inflation.GetVoxelData(), ImgG.GetVoxelData(), spacing_x, spacing_y, spacing_z, params);
			}
		}
	}

	template <> SpeedFunction *CreateSegmentationSpeedFunction(const RoutineBase *base, const i3d::Image3d<i3d::BINARY> &input, 
		                                                       const i3d::Image3d<i3d::BINARY> *domain, float sigma, 
															   const SpeedFunctionParams &params, bool use_distance)
	{
		return new Binary_SpeedFunction(base, input.GetVoxelData());
	}
	
	template <class T> SwedenLevelSetRoutine *CreateSegmentationRoutine(const i3d::Image3d<T> &input, const i3d::Image3d<i3d::BINARY> *mask, const i3d::Image3d<i3d::BINARY> *domain,
			bool preserve_topology, float sigma, int radius, const SpeedFunctionParams &params, bool use_distance)
	{
		if (!mask || mask -> GetSize() != input.GetSize() || (domain && domain -> GetSize() != input.GetSize()))
		{
			return NULL;
		}

		int num = 0;
		if (input.GetSizeZ() > 1)
		{
			num += 1;
		}

		if (domain)
		{
			num += 2;
		}

		if (preserve_topology)
		{
			num += 4;
		}

		RoutineBase *base = NULL;
		TopologyPreserving_RoutineBase *tp_base = NULL;
		Neighbourhood *neighbourhood = NULL;
		SpeedFunction *speed_function = NULL;
		Curvature *curvature = NULL;
		BorderPointDecider *border_point_decider = NULL;
		SimplePointDecider *simple_point_decider = NULL;
		Initiator *initiator = NULL;
		ResultCreator *result_creator = NULL;
				
		switch (num)
		{
			case 0:

				base = new RoutineBase(input.GetSizeX(), input.GetSizeY(), input.GetSizeZ());
				neighbourhood = new Standard_Neighbourhood2D(base);
				speed_function = CreateSegmentationSpeedFunction(base, input, domain, sigma, params, use_distance);
				curvature = new Curvature2D(base, speed_function, radius);
				border_point_decider = new BorderPointDecider2D(base, mask -> GetFirstVoxelAddr());
				initiator = new Standard_Initiator(base, speed_function, curvature, border_point_decider, mask -> GetFirstVoxelAddr());
				result_creator = new Segmentation_ResultCreator(base, input.GetOffset(), input.GetResolution());
				
				return new Standard_SwedenLevelSetRoutine(base, neighbourhood, speed_function, curvature, initiator, result_creator);

			case 1:

				base = new RoutineBase(input.GetSizeX(), input.GetSizeY(), input.GetSizeZ());
				neighbourhood = new Standard_Neighbourhood3D(base);
				speed_function = CreateSegmentationSpeedFunction(base, input, domain, sigma, params, use_distance);
				curvature = new Curvature3D(base, speed_function, radius);
				border_point_decider = new BorderPointDecider3D(base, mask -> GetFirstVoxelAddr());
				initiator = new Standard_Initiator(base, speed_function, curvature, border_point_decider, mask -> GetFirstVoxelAddr());
				result_creator = new Segmentation_ResultCreator(base, input.GetOffset(), input.GetResolution());

				return new Standard_SwedenLevelSetRoutine(base, neighbourhood, speed_function, curvature, initiator, result_creator);

			case 2:

				base = new RoutineBase(input.GetSizeX(), input.GetSizeY(), input.GetSizeZ());
				neighbourhood = new Standard_Neighbourhood2D_WithDomain(base, domain -> GetFirstVoxelAddr());
				speed_function = CreateSegmentationSpeedFunction(base, input, domain, sigma, params, use_distance);
				curvature = new Curvature2D(base, speed_function, radius);
				border_point_decider = new BorderPointDecider2D_WithDomain(base, mask -> GetFirstVoxelAddr(), domain -> GetFirstVoxelAddr());
				initiator = new Standard_Initiator_WithDomain(base, speed_function, curvature, border_point_decider, mask -> GetFirstVoxelAddr(), domain -> GetFirstVoxelAddr());
				result_creator = new Segmentation_ResultCreator_WithDomain(base, input.GetOffset(), input.GetResolution(), domain -> GetFirstVoxelAddr());

				return new Standard_SwedenLevelSetRoutine(base, neighbourhood, speed_function, curvature, initiator, result_creator);

			case 3:

				base = new RoutineBase(input.GetSizeX(), input.GetSizeY(), input.GetSizeZ());
				neighbourhood = new Standard_Neighbourhood3D_WithDomain(base, domain -> GetFirstVoxelAddr());
				speed_function = CreateSegmentationSpeedFunction(base, input, domain, sigma, params, use_distance);
				curvature = new Curvature3D(base, speed_function, radius);
				border_point_decider = new BorderPointDecider3D_WithDomain(base, mask -> GetFirstVoxelAddr(), domain -> GetFirstVoxelAddr());
				initiator = new Standard_Initiator_WithDomain(base, speed_function, curvature, border_point_decider, mask -> GetFirstVoxelAddr(), domain -> GetFirstVoxelAddr());
				result_creator = new Segmentation_ResultCreator_WithDomain(base, input.GetOffset(), input.GetResolution(), domain -> GetFirstVoxelAddr());

				return new Standard_SwedenLevelSetRoutine(base, neighbourhood, speed_function, curvature, initiator, result_creator);

			case 4:

				tp_base = new TopologyPreserving_RoutineBase(input.GetSizeX(), input.GetSizeY(), input.GetSizeZ());
				neighbourhood = new TopologyPreserving_Neighbourhood2D(tp_base);
				speed_function = CreateSegmentationSpeedFunction(tp_base, input, domain, sigma, params, use_distance);
				curvature = new Curvature2D(tp_base, speed_function, radius);
				border_point_decider = new BorderPointDecider2D(tp_base, mask -> GetFirstVoxelAddr());
				simple_point_decider = new SimplePointDecider2D(tp_base);
				initiator = new TopologyPreserving_Initiator(tp_base, speed_function, curvature, border_point_decider, simple_point_decider, mask);
				result_creator = new Segmentation_ResultCreator(tp_base, input.GetOffset(), input.GetResolution());
				
				return new TopologyPreserving_SwedenLevelSetRoutine(tp_base, neighbourhood, speed_function, curvature, initiator, result_creator, simple_point_decider);

			case 5:

				tp_base = new TopologyPreserving_RoutineBase(input.GetSizeX(), input.GetSizeY(), input.GetSizeZ());
				neighbourhood = new TopologyPreserving_Neighbourhood3D(tp_base);
				speed_function = CreateSegmentationSpeedFunction(tp_base, input, domain, sigma, params, use_distance);
				curvature = new Curvature3D(tp_base, speed_function, radius);
				border_point_decider = new BorderPointDecider3D(tp_base, mask -> GetFirstVoxelAddr());
				simple_point_decider = new SimplePointDecider3D(tp_base);
				initiator = new TopologyPreserving_Initiator(tp_base, speed_function, curvature, border_point_decider, simple_point_decider, mask); 
				result_creator = new Segmentation_ResultCreator(tp_base, input.GetOffset(), input.GetResolution());

				return new TopologyPreserving_SwedenLevelSetRoutine(tp_base, neighbourhood, speed_function, curvature, initiator, result_creator, simple_point_decider);

			case 6:

				tp_base = new TopologyPreserving_RoutineBase(input.GetSizeX(), input.GetSizeY(), input.GetSizeZ());
				neighbourhood = new TopologyPreserving_Neighbourhood2D_WithDomain(tp_base, domain -> GetFirstVoxelAddr());
				speed_function = CreateSegmentationSpeedFunction(tp_base, input, domain, sigma, params, use_distance);
				curvature = new Curvature2D(tp_base, speed_function, radius);
				border_point_decider = new BorderPointDecider2D_WithDomain(tp_base, mask -> GetFirstVoxelAddr(), domain -> GetFirstVoxelAddr());
				simple_point_decider = new SimplePointDecider2D(tp_base);
				initiator = new TopologyPreserving_Initiator_WithDomain(tp_base, speed_function, curvature, border_point_decider, simple_point_decider, mask, 
					                                                    domain -> GetFirstVoxelAddr());
				result_creator = new Segmentation_ResultCreator_WithDomain(tp_base, input.GetOffset(), input.GetResolution(), domain -> GetFirstVoxelAddr());

				return new TopologyPreserving_SwedenLevelSetRoutine(tp_base, neighbourhood, speed_function, curvature, initiator, result_creator, simple_point_decider);

			case 7:

				tp_base = new TopologyPreserving_RoutineBase(input.GetSizeX(), input.GetSizeY(), input.GetSizeZ());
				neighbourhood = new TopologyPreserving_Neighbourhood3D_WithDomain(tp_base, domain -> GetFirstVoxelAddr());
				speed_function = CreateSegmentationSpeedFunction(tp_base, input, domain, sigma, params, use_distance);
				curvature = new Curvature3D(tp_base, speed_function, radius);
				border_point_decider = new BorderPointDecider3D_WithDomain(tp_base, mask -> GetFirstVoxelAddr(), domain -> GetFirstVoxelAddr());
				simple_point_decider = new SimplePointDecider3D(tp_base);
				initiator = new TopologyPreserving_Initiator_WithDomain(tp_base, speed_function, curvature, border_point_decider, simple_point_decider, mask, 
					                                                    domain -> GetFirstVoxelAddr());
				result_creator = new Segmentation_ResultCreator_WithDomain(tp_base, input.GetOffset(), input.GetResolution(), domain -> GetFirstVoxelAddr());

				return new TopologyPreserving_SwedenLevelSetRoutine(tp_base, neighbourhood, speed_function, curvature, initiator, result_creator, simple_point_decider);
		}

		return NULL;
	}

	template I3D_DLLEXPORT SwedenLevelSetRoutine *CreateSegmentationRoutine(const i3d::Image3d<i3d::GRAY16> &input, const i3d::Image3d<i3d::BINARY> *mask, 
		const i3d::Image3d<i3d::BINARY> *domain, bool preserve_topology, float sigma, int radius, const SpeedFunctionParams &params, bool use_distance);
	template I3D_DLLEXPORT SwedenLevelSetRoutine *CreateSegmentationRoutine(const i3d::Image3d<i3d::GRAY8> &input, const i3d::Image3d<i3d::BINARY> *mask, 
		const i3d::Image3d<i3d::BINARY> *domain, bool preserve_topology, float sigma, int radius, const SpeedFunctionParams &params, bool use_distance);
	template I3D_DLLEXPORT SwedenLevelSetRoutine *CreateSegmentationRoutine(const i3d::Image3d<i3d::BINARY> &input, const i3d::Image3d<i3d::BINARY> *mask, 
		const i3d::Image3d<i3d::BINARY> *domain, bool preserve_topology, float sigma, int radius, const SpeedFunctionParams &params, bool use_distance);

	template SpeedFunction *CreateSegmentationSpeedFunction(const RoutineBase *base, const i3d::Image3d<i3d::GRAY16> &input, const i3d::Image3d<i3d::BINARY> *domain, 
		                                                    float sigma, const SpeedFunctionParams &params, bool use_distance);	





/////////////////////////////////////////////////////////////////
//                                                             //
//                   Class CompSwedenLevelSet                  //
//                                                             //
/////////////////////////////////////////////////////////////////

template <class T> CompSwedenLevelSet<T>::CompSwedenLevelSet(const i3d::Image3d<i3d::BINARY> *_Domain, std::valarray<float> *ImageGrad, 
	i3d::Vector3d<size_t> _ImgSize, i3d::Vector3d<float> _Spacing, float _alpha, float _beta, float _c, int _p, 
	size_t _N, size_t _R): Domain(_Domain), ImageGradient(*ImageGrad), ImgSize(_ImgSize), Spacing(_Spacing), alpha(_alpha), 
	beta(_beta), c(_c), p(_p), N(_N), R(_R)
{ 
	const size_t size = ImgSize.x * ImgSize.y * ImgSize.z;
	ImplicitFunction.resize(size, 1);
	BackPointers.resize(size, -1);
	Curvature.resize(size, UNDEFINED);  
	heap = new SwedenHeap(BackPointers);

	Inflation.MakeRoom(ImgSize);
	Inflation.SetOffset(i3d::Offset(0,0,0));
	Inflation.SetResolution(i3d::Resolution(1.0f, 1.0f, 1.0f));
	for (size_t i = 0; i < size; i++)
	{
		Inflation.SetVoxel(i, c);
	}
}

template <class T> void CompSwedenLevelSet<T>::Init(const std::vector<size_t> *Seeds, const i3d::Image3d<T> *mask)
{
	if (!Seeds && !mask)
	{
		throw i3d::InternalException("Initialization failed!");
	}

	std::vector<size_t> init_interface;

	if (Seeds)
	{
		size_t size = ImgSize.x * ImgSize.y * ImgSize.z;

		for(size_t i = 0; i < Seeds -> size(); i++)
		{
			if ((*Seeds)[i] < size)
			{
				if (IsInDomain((*Seeds)[i]))
				{
					ImplicitFunction[(*Seeds)[i]] = 0;
					init_interface.push_back((*Seeds)[i]);
				}
			}
			else
			{
				#ifdef I3D_DEBUG
					cout << "Seed " << (*Seeds)[i] << " was ignored. It isn't a point of processed image!" << endl;
				#endif
			}
		}
	}
	else
	{
		for (size_t i = 0; i < mask -> GetImageSize(); i++)
		{
			if (mask -> GetVoxel(i) == std::numeric_limits<T>::min())
			{
				ImplicitFunction[i] = 1;
			}

			if (mask -> GetVoxel(i) == std::numeric_limits<T>::max() && IsInDomain(i))
			{
				if (IsBorderPoint(i, mask -> GetVoxelData()))
				{
					ImplicitFunction[i] = 0;
					init_interface.push_back(i);
				}
				else
				{
					ImplicitFunction[i] = -1;
				}
			}
		}
	}

	for (size_t i = 0; i < init_interface.size(); i++)
	{
		Curvature[init_interface[i]] = EstimateCurvature(init_interface[i]);
		heap -> Insert(init_interface[i], 0, ComputeSpeedFunction(init_interface[i]));
	}
}

template <class T> void CompSwedenLevelSet<T>::ComputeIteration()
{
	SwedenHeapNode *min = heap -> Pop();

	if (!min)
	{
		throw i3d::InternalException("Cannot pop from the empty heap!");
	}

	int neigh_type = min -> speed > 0 ? 1 : -1; // use exterior or interior neighbors?
    
	std::vector<size_t> neigh;
	GetNeighbors(min -> position, neigh, neigh_type);

	ImplicitFunction[min -> position] = -neigh_type;
	Curvature[min -> position] = UNDEFINED;

	if (neigh_type == 1)
	{
		UpdateCurvature(min -> position, 1 / float(N));
	}
    
	for (size_t i = 0; i < neigh.size(); i++)
	{
		ImplicitFunction[neigh[i]] = 0;
		Curvature[neigh[i]] = EstimateCurvature(neigh[i]);

		if (neigh_type == -1)
		{
			UpdateCurvature(neigh[i], -1 / float(N));
		}

		heap -> Insert(neigh[i], min -> departure_time, ComputeSpeedFunction(neigh[i]));
	}

	delete min;
}

template <class T> void CompSwedenLevelSet<T>::ComputeSegmentatedImage(i3d::Image3d<T> &output, 
																	   SegmentationType type)
{ 
	output.MakeRoom(ImgSize);

	if (type)
	{
		for (size_t i = 0; i < ImplicitFunction.size(); i++)
		{
			output.SetVoxel(i,  ImplicitFunction[i] == 1 ? 
				std::numeric_limits<T>::min() : std::numeric_limits<T>::max());
		}
	}
	else
	{
		for (size_t i = 0; i < ImplicitFunction.size(); i++)
		{
			output.SetVoxel(i,  ImplicitFunction[i] == 1 ? 
				std::numeric_limits<T>::max() : std::numeric_limits<T>::min());
		}
	}

	/*for (size_t i = 0; i < ImplicitFunction.size(); i++)
	{
		output.SetVoxel(i, static_cast<T>(ImplicitFunction[i] + 10));
	}*/
}

template <class T> bool CompSwedenLevelSet<T>::IsInDomain(size_t i)
{
	return (Domain == NULL || Domain -> GetVoxel(i) == std::numeric_limits<i3d::BINARY>::max());
}

/////////////////////////////////////////////////////////////////
//                                                             //
//         Class CompSwedenLevelSet_PreserveTopology           //
//                                                             //
/////////////////////////////////////////////////////////////////

template <class T> CompSwedenLevelSet_PreserveTopology<T>::CompSwedenLevelSet_PreserveTopology(
	const i3d::Image3d<i3d::BINARY> *Domain, std::valarray<float> *ImageGrad, i3d::Vector3d<size_t> ImgSize, i3d::Vector3d<float> Spacing, 
	float alpha, float beta, float c, int p, size_t N, size_t R)
	: CompSwedenLevelSet<T>(Domain, ImageGrad, ImgSize, Spacing, alpha, beta, c, p, N, R)
{
	const size_t size = (ImgSize.x + 2) * (ImgSize.y + 2) * (ImgSize.z + 2);
	Component.resize(size, 0);
}

template <class T> void CompSwedenLevelSet_PreserveTopology<T>::Init(const std::vector<size_t> *Seeds, 
																	 const i3d::Image3d<T> *mask)
{
	if (!Seeds && !mask)
	{
		throw i3d::InternalException("Initialization failed!");
	}

	std::vector<size_t> init_interface;

	if (Seeds)
	{
		size_t size = ImgSize.x * ImgSize.y * ImgSize.z;

		for(size_t i = 0; i < Seeds -> size(); i++)
		{
			if ((*Seeds)[i] < size)
			{
				if (IsInDomain((*Seeds)[i]))
				{
					ImplicitFunction[(*Seeds)[i]] = 0;
					init_interface.push_back((*Seeds)[i]);

					SetComponent((*Seeds)[i], i + 1);
				}
			}
			else
			{
				#ifdef I3D_DEBUG
					cout << "Seed " << (*Seeds)[i] << " was ignored. It isn't a point of processed image!" << endl;
				#endif
			}
		}
	}
	else
	{
		i3d::LabeledImage3d <size_t, T> limg;
		limg.CreateForegroundRegionsFF(*mask);

		for (size_t i = 0; i < mask -> GetImageSize(); i++)
		{
			if (mask -> GetVoxel(i) == std::numeric_limits<T>::min())
			{
				ImplicitFunction[i] = 1;
			}

			if (mask -> GetVoxel(i) == std::numeric_limits<T>::max() && IsInDomain(i))
			{
				SetComponent(i, static_cast<int>(limg.GetVoxel(i)));

				if (this -> IsBorderPoint(i, mask -> GetVoxelData()))
				{
					ImplicitFunction[i] = 0;
					init_interface.push_back(i);
				}
				else
				{
					ImplicitFunction[i] = -1;
				}
			}
		}
	}

	if (Domain && c != 1.0f)
	{
	for (size_t i = 0; i < Inflation.GetImageSize(); i++)
	{
		Inflation.SetVoxel(i, Domain -> GetVoxel(i) == i3d::BINARY(1) ? 1.0f : 0.0f);
	}

//	Inflation.SetResolution(i3d::Resolution(1.0, 1.0, 1.0));
	i3d::HybridSaito(Inflation, 0, 1.0f);
	//i3d::CityBlock(Inflation);
	}
	
	for (size_t i = 0; i < init_interface.size(); i++)
	{
		Curvature[init_interface[i]] = EstimateCurvature(init_interface[i]);
		heap -> Insert(init_interface[i], 0, ComputeSpeedFunction(init_interface[i]));
	}
}

template <class T> void CompSwedenLevelSet_PreserveTopology<T>::ComputeIteration()
{ 
	SwedenHeapNode *min = heap -> Pop();

	if (!min)
	{
		throw i3d::InternalException("Cannot pop from the empty heap!");
	}
  
	int neigh_type = min -> speed > 0 ? 1 : -1; // use exterior or interior neighbors?
	
	std::vector<size_t> neigh;
	GetNeighbors(min -> position, neigh, neigh_type);
    
	int num_changes = neigh.size(); // number of possible neighbors to change
	int num = 0;

	// topology test
	if (num_changes > 0)
	{
		if (!IsSimplePoint(GetComponentIndex(min -> position, 0, 0, 0)))
		{
			heap -> Insert(min -> position, min -> departure_time, ComputeSpeedFunction(min -> position));
			delete min;
			return;
		}

		size_t i = 0;
		while (i < neigh.size())
		{
			if (!IsSimplePoint(neigh[i]))
			{
				neigh.erase(neigh.begin() + i);
			}
			else
			{
				i++;
				num++;
			}
		}
	}

	int comp_num = GetComponent(min -> position); // the value of component 

	if (num_changes == num)
	{
		ImplicitFunction[min -> position] = -neigh_type;
		Curvature[min -> position] = UNDEFINED;

		if (neigh_type == 1)
		{
			UpdateCurvature(min -> position, 1 / float(N));
		}
		else
		{
			SetComponent(min -> position, 0);
		}
	}
	else
	{
		heap -> Insert(min -> position, min -> departure_time, ComputeSpeedFunction(min -> position));
	}

	size_t index;
	for (size_t i = 0; i < neigh.size(); i++)
	{
		index = GetIFIndex(neigh[i]); 

		ImplicitFunction[index] = 0;
		Curvature[index] = EstimateCurvature(index);
		
		SetComponent(index, comp_num);

		if (neigh_type == -1)
		{
			UpdateCurvature(index, -1 / float(N));
		}

		heap -> Insert(index, min -> departure_time, ComputeSpeedFunction(index));
	}

	delete min;
}

template <class T> void CompSwedenLevelSet_PreserveTopology<T>::ComputeSegmentatedImage(i3d::Image3d<T> &output, 
																						SegmentationType type)
{ 
	if (type)
	{
		for (size_t i = 0; i < ImplicitFunction.size(); i++)
		{
			output.SetVoxel(i, GetComponent(i) == 0 ? std::numeric_limits<T>::min() : std::numeric_limits<T>::max());
		}
	}
	else  
	{
		for (size_t i = 0; i < ImplicitFunction.size(); i++)
		{
			output.SetVoxel(i, GetComponent(i) == 0 ? std::numeric_limits<T>::max() : std::numeric_limits<T>::min());
		}
	}

	/*for (size_t i = 0; i < ImplicitFunction.size(); i++)
	{
		output.SetVoxel(i, static_cast<T>(ImplicitFunction[i] + 10));
	}*/
}


/////////////////////////////////////////////////////////////////
//                                                             //
//                      Class CompSweden2D                     //
//                                                             //
/////////////////////////////////////////////////////////////////

template <class T> float CompSweden2D<T>::ComputeSpeedFunction(size_t i)
{ 
	int x, y;
	GETPOSITION_2D(x, y);

	float Dx = 0, Dy = 0, IGx = 0, IGy = 0;

	if (x > 0 && x < (int)ImgSize.x - 1 && IsInDomain(i - 1) && IsInDomain(i + 1))
	{
		Dx = CENTRAL(ImplicitFunction[i - 1], ImplicitFunction[i + 1], Spacing.x);
		IGx = CENTRAL(ImageGradient[i - 1], ImageGradient[i + 1], Spacing.x);
	}

	if (y > 0 && y < (int)ImgSize.y - 1 && IsInDomain(i - ImgSize.x) && IsInDomain(i + ImgSize.x))
	{
		Dy = CENTRAL(ImplicitFunction[i - ImgSize.x], ImplicitFunction[i + ImgSize.x], Spacing.y);
		IGy = CENTRAL(ImageGradient[i - ImgSize.x], ImageGradient[i + ImgSize.x], Spacing.y);
	}

	float val = MAX(sqrt(SQR(Dx) + SQR(Dy)), EPSILON);
	float Nx = Dx / val;
	float Ny = Dy / val;

	float grad;
	if (p == 2)
	{
		grad = 1 / SQR(1 + ImageGradient[i]);
	}
	else 
	{
		grad = 1 / (1 + ImageGradient[i]);
	}

	//grad = 1;
	//return 1;
	return (grad * (Inflation.GetVoxel(i) + alpha * Curvature[i]) + beta * (Nx * IGx + Ny * IGy));
	//return std::abs(Inflation.GetVoxel(i));
}

template <class T> void CompSweden2D<T>::GetNeighbors(size_t i, std::vector<size_t> &neigh, int value)
{
	int x, y;
	GETPOSITION_2D(x, y);

	if (x > 0 && ImplicitFunction[i - 1] == value && IsInDomain(i - 1))
		neigh.push_back(i - 1);

	if (x < (int)ImgSize.x - 1 && ImplicitFunction[i + 1] == value && IsInDomain(i + 1))
		neigh.push_back(i + 1);

	if (y > 0 && ImplicitFunction[i - ImgSize.x] == value && IsInDomain(i - ImgSize.x))
		neigh.push_back(i - ImgSize.x);

	if (y < (int)ImgSize.y - 1 && ImplicitFunction[i + ImgSize.x] == value && IsInDomain(i + ImgSize.x))
		neigh.push_back(i + ImgSize.x);
}

template <class T> float CompSweden2D<T>::EstimateCurvature(size_t i)
{
	int x, y;
	GETPOSITION_2D(x, y);

	int lx, ux, ly, uy;
	GetBounds(x, y, lx, ux, ly, uy);

	size_t poc = 0;
	for (int y = ly; y <= uy; y++)
	{
		for (int x = lx; x <= ux; x++)
		{
			if (ImplicitFunction[y * ImgSize.x + x] == -1)
			{				
				poc++;
			}
		}
	}

	return float(poc / float(N) - 0.5); 
}

template <class T> void CompSweden2D<T>::UpdateCurvature(size_t i, float change)
{
	int x, y;
	GETPOSITION_2D(x, y);

	int lx, ux, ly, uy;
	GetBounds(x, y, lx, ux, ly, uy);

	size_t index;
	for (int y = ly; y <= uy; y++)
	{
		for (int x = lx; x <= ux; x++)
		{
			index = y * ImgSize.x + x;
			if (BackPointers[index] != -1)
			{
				Curvature[index] += change;
				heap -> Update(BackPointers[index], ComputeSpeedFunction(index));
			}
		}
	}
}

template <class T> inline void CompSweden2D<T>::GetBounds(int x, int y, int &lx, int &ux, int &ly, int &uy)
{ 
	lx = x - R; 
	ux = x + R; 
	ly = y - R; 
	uy = y + R; 

	lx = lx > 0 ? lx : 0; 
	ux = ux < (int)ImgSize.x - 1 ? ux : ImgSize.x - 1; 
	ly = ly > 0 ? ly : 0; 
	uy = uy < (int)ImgSize.y - 1 ? uy : ImgSize.y - 1;
}

template <class T> bool CompSweden2D<T>::IsBorderPoint(size_t i, const std::valarray<T> &data)
{
	size_t x, y;
	GETPOSITION_2D(x, y);

	/*if ((x > 0 && data[i - 1] == std::numeric_limits<T>::min()) ||
		(x < (int)ImgSize.x - 1  && data[i + 1] == std::numeric_limits<T>::min()) ||
		(y > 0 && data[i - ImgSize.x] == std::numeric_limits<T>::min()) ||
		(y < (int)ImgSize.y - 1 && data[i + ImgSize.x] == std::numeric_limits<T>::min()) ||
		(x > 0 && y > 0 && data[i - ImgSize.x - 1] == std::numeric_limits<T>::min()) ||
		(x < (int)ImgSize.x - 1 && y > 0 && data[i - ImgSize.x + 1] == std::numeric_limits<T>::min()) ||
		(x > 0 && y < (int)ImgSize.y - 1 && data[i + ImgSize.x - 1] == std::numeric_limits<T>::min()) ||
		(x < (int)ImgSize.x - 1 && y < (int)ImgSize.y - 1 && data[i + ImgSize.x + 1] == std::numeric_limits<T>::min()))
	{
		return true;
	}

	return false;*/

	/*if (x == 0 || y == 0 || x == (int) ImgSize.x - 1 || y == (int) ImgSize.y - 1)
	{
		return true;
	}*/

	if ((x > 0 && (!IsInDomain(i - 1) || data[i - 1] != data[i])) ||
		(x + 1 < ImgSize.x && (!IsInDomain(i + 1) || data[i + 1] != data[i])) ||
		(y > 0 && (!IsInDomain(i - ImgSize.x) || data[i - ImgSize.x] != data[i])) ||
		(y + 1 < ImgSize.y && (!IsInDomain(i + ImgSize.x) || data[i + ImgSize.x] != data[i]))/* ||
		(!IsInDomain(i - ImgSize.x - 1) || data[i - ImgSize.x - 1] != data[i]) ||
		(!IsInDomain(i - ImgSize.x + 1) || data[i - ImgSize.x + 1] != data[i]) ||
		(!IsInDomain(i + ImgSize.x - 1) || data[i + ImgSize.x - 1] != data[i]) ||
		(!IsInDomain(i + ImgSize.x + 1) || data[i + ImgSize.x + 1] != data[i]))*/)
	{
		return true;
	}

	return false;
}

/////////////////////////////////////////////////////////////////
//                                                             //
//           Class CompSweden2D_PreserveTopology               //
//                                                             //
/////////////////////////////////////////////////////////////////

template <class T> void CompSweden2D_PreserveTopology<T>::GetNeighbors(size_t i, std::vector<size_t> &neigh, 
																	   int value)
{
	int x, y;
	GETPOSITION_2D(x, y);

	if (x == 0 || (x > 0 && ImplicitFunction[i - 1] == value && IsInDomain(i - 1)))
		neigh.push_back(GetComponentIndex(i, -1, 0, 0)); 

	if (x == (int)ImgSize.x - 1 || (x < (int)ImgSize.x - 1  && ImplicitFunction[i + 1] == value && IsInDomain(i + 1)))
		neigh.push_back(GetComponentIndex(i, 1, 0, 0));

	if (y == 0 || (y > 0 && ImplicitFunction[i - ImgSize.x] == value && IsInDomain(i - ImgSize.x)))
		neigh.push_back(GetComponentIndex(i, 0, -1, 0));

	if (y == (int)ImgSize.y - 1 || (y < (int)ImgSize.y - 1 && ImplicitFunction[i + ImgSize.x] == value && IsInDomain(i + ImgSize.x)))
		neigh.push_back(GetComponentIndex(i, 0, 1, 0));
}

template <class T> bool CompSweden2D_PreserveTopology<T>::IsSimplePoint(size_t i)
{
	int x, y;
	GETPOSITION_2D_COMP(x, y);

	if (x == 0 || x == (int)ImgSize.x + 1 || y == 0 || y == (int)ImgSize.y + 1)
		return false;

	int x1 = GetIFIndex(i) % ImgSize.x;
	int y1 = GetIFIndex(i) / ImgSize.x;
	if (x1 > 0 && y1 > 0 && x1 < (int) ImgSize.x - 1 && y1 < (int) ImgSize.y - 1 &&
		(!IsInDomain(GetIFIndex(i)) || !IsInDomain(GetIFIndex(i) - 1) || !IsInDomain(GetIFIndex(i) + 1) || 
		 !IsInDomain(GetIFIndex(i) + ImgSize.x) || !IsInDomain(GetIFIndex(i) - ImgSize.x)))
		 return false;

	int val = ((Component[i - ImgSize.x - 2 - 1] != 0) << 8) +
		      ((Component[i - ImgSize.x - 2]     != 0) << 7) +
			  ((Component[i - ImgSize.x - 2 + 1] != 0) << 6) +
			  ((Component[i - 1]                 != 0) << 5) +
			  ((Component[i]                     != 0) << 4) +
			  ((Component[i + 1]                 != 0) << 3) +
			  ((Component[i + ImgSize.x + 2 - 1] != 0) << 2) +
			  ((Component[i + ImgSize.x + 2]     != 0) << 1) +
			   (Component[i + ImgSize.x + 2 + 1] != 0);

	return is_simple[(row_index[val >> 5] << 4) + val % 16];


}

template <class T> int CompSweden2D_PreserveTopology<T>::GetComponent(size_t i)
{
	int x, y;
	GETPOSITION_2D(x, y);

	return Component[(ImgSize.x + 2) * (y + 1) + x + 1];
}

template <class T> void CompSweden2D_PreserveTopology<T>::SetComponent(size_t i, int value)
{
	int x, y;
	GETPOSITION_2D(x, y);

	Component[(ImgSize.x + 2) * (y + 1) + x + 1] = value;
}

template <class T> size_t CompSweden2D_PreserveTopology<T>::GetComponentIndex(size_t i, int move_x, 
																			  int move_y, int move_z)
{
	int x, y;
	GETPOSITION_2D(x, y);

	return (ImgSize.x + 2) * (y + 1 + move_y) + x + 1 + move_x;
}

template <class T> size_t CompSweden2D_PreserveTopology<T>::GetIFIndex(size_t i)
{
	int x, y;
	GETPOSITION_2D_COMP(x, y);

	return ((y - 1) * ImgSize.x + (x - 1));
}

/////////////////////////////////////////////////////////////////
//                                                             //
//                      Class CompSweden3D                     //
//                                                             //
/////////////////////////////////////////////////////////////////

template <class T> float CompSweden3D<T>::ComputeSpeedFunction(size_t i)
{
	int x, y, z;
	GETPOSITION_3D(x, y, z);

	float Dx = 0, Dy = 0, Dz = 0, IGx = 0, IGy = 0, IGz = 0;

	if (x > 0 && x < (int)ImgSize.x - 1 && IsInDomain(i - 1) && IsInDomain(i + 1))
	{
		Dx = CENTRAL(ImplicitFunction[i - 1], ImplicitFunction[i + 1], Spacing.x);
		IGx = CENTRAL(ImageGradient[i - 1], ImageGradient[i + 1], Spacing.x);
	}

	if (y > 0 && y < (int)ImgSize.y - 1 && IsInDomain(i - ImgSize.x) && IsInDomain(i + ImgSize.x))
	{
		Dy = CENTRAL(ImplicitFunction[i - ImgSize.x], ImplicitFunction[i + ImgSize.x], Spacing.y);
		IGy = CENTRAL(ImageGradient[i - ImgSize.x], ImageGradient[i + ImgSize.x], Spacing.y);
	}

	if (z > 0 && z < (int)ImgSize.z - 1 && IsInDomain(i - ImgSize.x * ImgSize.y) && IsInDomain(i + ImgSize.x * ImgSize.y))
	{
		Dz = CENTRAL(ImplicitFunction[i - ImgSize.x * ImgSize.y], ImplicitFunction[i + ImgSize.x * ImgSize.y], Spacing.z);
		IGz = CENTRAL(ImageGradient[i - ImgSize.x * ImgSize.y], ImageGradient[i + ImgSize.x * ImgSize.y], Spacing.z);
	}
 
	float val = MAX(sqrt(SQR(Dx) + SQR(Dy) + SQR(Dz)), EPSILON);
	float Nx = Dx / val;
	float Ny = Dy / val;
	float Nz = Dz / val;

	float grad;
	if (p == 2)
	{
		grad = 1 / SQR(1 + ImageGradient[i]);
	}
	else 
	{
		grad = 1 / (1 + ImageGradient[i]);
	}

	return (grad * (Inflation.GetVoxel(i) + alpha * Curvature[i]) + beta * (Nx * IGx + Ny * IGy + Nz * IGz));
}

template <class T> void CompSweden3D<T>::GetNeighbors(size_t i, std::vector<size_t> &neigh, int value)
{
	int x, y, z;
	GETPOSITION_3D(x, y, z);

	if (x > 0 && ImplicitFunction[i - 1] == value && IsInDomain(i - 1))
		neigh.push_back(i - 1);

	if (x < (int)ImgSize.x - 1  && ImplicitFunction[i + 1] == value && IsInDomain(i + 1))
		neigh.push_back(i + 1);

	if (y > 0 && ImplicitFunction[i - ImgSize.x] == value && IsInDomain(i - ImgSize.x))
		neigh.push_back(i - ImgSize.x);

	if (y < (int)ImgSize.y - 1 && ImplicitFunction[i + ImgSize.x] == value && IsInDomain(i + ImgSize.x))
		neigh.push_back(i + ImgSize.x);

	if (z > 0 && ImplicitFunction[i - ImgSize.x * ImgSize.y] == value && IsInDomain(i - ImgSize.x * ImgSize.y))
		neigh.push_back(i - ImgSize.x * ImgSize.y);

	if (z < (int)ImgSize.z - 1 && ImplicitFunction[i + ImgSize.x * ImgSize.y] == value && IsInDomain(i + ImgSize.x * ImgSize.y))
		neigh.push_back(i + ImgSize.x * ImgSize.y);
}

template <class T> float CompSweden3D<T>::EstimateCurvature(size_t i)
{
	int x, y, z;
	GETPOSITION_3D(x, y, z);

	int lx, ux, ly, uy, lz, uz;
	GetBounds(x, y, z, lx, ux, ly, uy, lz, uz);

	int poc = 0;
	for (int z = lz; z <= uz; z++)
	{
		for (int y = ly; y <= uy; y++)
		{
			for (int x = lx; x <= ux; x++)
			{
				if (ImplicitFunction[(z * ImgSize.y + y) * ImgSize.x + x] == -1)
				{
					poc++;
				}
			}
		}
	}
	
	return float(poc / float(N) - 0.5); 
}

template <class T> void CompSweden3D<T>::UpdateCurvature(size_t i, float change)
{
	int x, y, z;
	GETPOSITION_3D(x, y, z);

	int lx, ux, ly, uy, lz, uz;
	GetBounds(x, y, z, lx, ux, ly, uy, lz, uz);

	size_t index;
	for (int z = lz; z <= uz; z++)
	{
		for (int y = ly; y <= uy; y++)
		{
			for (int x = lx; x <= ux; x++)
			{
				index = (z * ImgSize.y + y) * ImgSize.x + x;
				if (BackPointers[index] != -1)
				{
					Curvature[index] += change;
					heap -> Update(BackPointers[index], ComputeSpeedFunction(index));
				}
			}
		}
	}
}

template <class T> 
  inline void CompSweden3D<T>::GetBounds(int x, int y, int z, int &lx, int &ux, int &ly, int &uy, int &lz, int &uz)
{
	lx = x - R; 
	ux = x + R; 
	ly = y - R; 
	uy = y + R; 
	lz = z - R; 
	uz = z + R;

	lx = lx > 0 ? lx : 0; 
	ux = ux < (int)ImgSize.x - 1 ? ux : ImgSize.x - 1; 
	ly = ly > 0 ? ly : 0; 
	uy = uy < (int)ImgSize.y - 1 ? uy : ImgSize.y - 1;
	lz = lz > 0 ? lz : 0; 
	uz = uz < (int)ImgSize.z - 1 ? uz : ImgSize.z - 1;
}

template <class T> bool CompSweden3D<T>::IsBorderPoint(size_t i, const std::valarray<T> &data)
{
	size_t x, y, z;
	GETPOSITION_3D(x, y, z);

	size_t slice = ImgSize.x * ImgSize.y;

	/*if (((x > 0 && data[i - 1] == std::numeric_limits<T>::min()) ||
		 (x < (int)ImgSize.x - 1  && data[i + 1] == std::numeric_limits<T>::min()) ||
		 (y > 0 && data[i - ImgSize.x] == std::numeric_limits<T>::min()) ||
		 (y < (int)ImgSize.y - 1 && data[i + ImgSize.x] == std::numeric_limits<T>::min()) ||
		 (x > 0 && y > 0 && data[i - ImgSize.x - 1] == std::numeric_limits<T>::min()) ||
		 (x < (int)ImgSize.x - 1 && y > 0 && data[i - ImgSize.x + 1] == std::numeric_limits<T>::min()) ||
		 (x > 0 && y < (int)ImgSize.y - 1 && data[i + ImgSize.x - 1] == std::numeric_limits<T>::min()) ||
		 (x < (int)ImgSize.x - 1 && y < (int)ImgSize.y - 1 && data[i + ImgSize.x + 1] == std::numeric_limits<T>::min())
		) ||
		(z > 0 && 
		 ((x > 0 && data[i - slice - 1] == std::numeric_limits<T>::min()) ||
		  (x < (int)ImgSize.x - 1  && data[i - slice + 1] == std::numeric_limits<T>::min()) ||
		  (y > 0 && data[i - slice - ImgSize.x] == std::numeric_limits<T>::min()) ||
		  (y < (int)ImgSize.y - 1 && data[i - slice + ImgSize.x] == std::numeric_limits<T>::min()) ||
		  (x > 0 && y > 0 && data[i - slice - ImgSize.x - 1] == std::numeric_limits<T>::min()) ||
		  (x < (int)ImgSize.x - 1 && y > 0 && data[i - slice - ImgSize.x + 1] == std::numeric_limits<T>::min()) ||
		  (x > 0 && y < (int)ImgSize.y - 1 && data[i - slice + ImgSize.x - 1] == std::numeric_limits<T>::min()) ||
		  (x < (int)ImgSize.x - 1 && y < (int)ImgSize.y - 1 && data[i - slice + ImgSize.x + 1] == std::numeric_limits<T>::min())
		 )
		) ||
		(z < (int)ImgSize.z - 1 && 
		 ((x > 0 && data[i + slice - 1] == std::numeric_limits<T>::min()) ||
		  (x < (int)ImgSize.x - 1  && data[i + slice + 1] == std::numeric_limits<T>::min()) ||
		  (y > 0 && data[i + slice - ImgSize.x] == std::numeric_limits<T>::min()) ||
		  (y < (int)ImgSize.y - 1 && data[i + slice + ImgSize.x] == std::numeric_limits<T>::min()) ||
		  (x > 0 && y > 0 && data[i + slice - ImgSize.x - 1] == std::numeric_limits<T>::min()) ||
		  (x < (int)ImgSize.x - 1 && y > 0 && data[i + slice - ImgSize.x + 1] == std::numeric_limits<T>::min()) ||
		  (x > 0 && y < (int)ImgSize.y - 1 && data[i + slice + ImgSize.x - 1] == std::numeric_limits<T>::min()) ||
		  (x < (int)ImgSize.x - 1 && y < (int)ImgSize.y - 1 && data[i + slice + ImgSize.x + 1] == std::numeric_limits<T>::min())
 	     )
		)
	  )*/

	if ( ( (x > 0 && (!IsInDomain(i - 1) || data[i - 1] != data[i])) ||
		   (x + 1 < ImgSize.x && (!IsInDomain(i + 1) || data[i + 1] != data[i])) ||
		   (y > 0 && (!IsInDomain(i - ImgSize.x) || data[i - ImgSize.x] != data[i])) ||
		   (y + 1 < ImgSize.y && (!IsInDomain(i + ImgSize.x) || data[i + ImgSize.x] != data[i])) ||
		   (z > 0 && (!IsInDomain(i - slice) || data[i - slice] != data[i])) ||
		   (z + 1 < ImgSize.z && (!IsInDomain(i + slice) || data[i + slice] != data[i]))
		   ))
		   /* ||
		   (!IsInDomain(i - ImgSize.x - 1) || data[i - ImgSize.x - 1] != data[i]) ||
		   (!IsInDomain(i - ImgSize.x + 1) || data[i - ImgSize.x + 1] != data[i]) ||
		   (!IsInDomain(i + ImgSize.x - 1) || data[i + ImgSize.x - 1] != data[i]) ||
		   (!IsInDomain(i + ImgSize.x + 1) || data[i + ImgSize.x + 1] != data[i])
	     ) ||
		 ( (!IsInDomain(i - slice - 1) || data[i - slice - 1] != data[i]) ||
		   (!IsInDomain(i - slice + 1) || data[i - slice + 1] != data[i]) ||
		   (!IsInDomain(i - slice - ImgSize.x) || data[i - slice - ImgSize.x] != data[i]) ||
		   (!IsInDomain(i - slice + ImgSize.x) || data[i - slice + ImgSize.x] != data[i]) ||
		   (!IsInDomain(i - slice - ImgSize.x - 1) || data[i - slice - ImgSize.x - 1] != data[i]) ||
		   (!IsInDomain(i - slice - ImgSize.x + 1) || data[i - slice - ImgSize.x + 1] != data[i]) ||
		   (!IsInDomain(i - slice + ImgSize.x - 1) || data[i - slice + ImgSize.x - 1] != data[i]) ||
		   (!IsInDomain(i - slice + ImgSize.x + 1) || data[i - slice + ImgSize.x + 1] != data[i])
	     ) ||
		 ( (!IsInDomain(i + slice - 1) || data[i + slice - 1] != data[i]) ||
		   (!IsInDomain(i + slice + 1) || data[i + slice + 1] != data[i]) ||
		   (!IsInDomain(i + slice - ImgSize.x) || data[i + slice - ImgSize.x] != data[i]) ||
		   (!IsInDomain(i + slice + ImgSize.x) || data[i + slice + ImgSize.x] != data[i]) ||
		   (!IsInDomain(i + slice - ImgSize.x - 1) || data[i + slice - ImgSize.x - 1] != data[i]) ||
		   (!IsInDomain(i + slice - ImgSize.x + 1) || data[i + slice - ImgSize.x + 1] != data[i]) ||
		   (!IsInDomain(i + slice + ImgSize.x - 1) || data[i + slice + ImgSize.x - 1] != data[i]) ||
		   (!IsInDomain(i + slice + ImgSize.x + 1) || data[i + slice + ImgSize.x + 1] != data[i])
	     )
	  )*/
	{	  
	  return true;
	}
	
	return false;
}

/////////////////////////////////////////////////////////////////
//                                                             //
//            Class CompSweden3D_PreserveTopology              //
//                                                             //
/////////////////////////////////////////////////////////////////

template <class T> void CompSweden3D_PreserveTopology<T>::GetNeighbors(size_t i, std::vector<size_t> &neigh, 
																	   int value)
{
	int x, y, z;
	GETPOSITION_3D(x, y, z);

	if (x == 0 || (x > 0 && ImplicitFunction[i - 1] == value && IsInDomain(i - 1)))
		neigh.push_back(GetComponentIndex(i, -1, 0, 0)); 

	if (x == (int)ImgSize.x - 1 || (x < (int)ImgSize.x - 1  && ImplicitFunction[i + 1] == value && IsInDomain(i + 1)))
		neigh.push_back(GetComponentIndex(i, 1, 0, 0));

	if (y == 0 || (y > 0 && ImplicitFunction[i - ImgSize.x] == value && IsInDomain(i - ImgSize.x)))
		neigh.push_back(GetComponentIndex(i, 0, -1, 0));

	if (y == (int)ImgSize.y - 1 || (y < (int)ImgSize.y - 1 && ImplicitFunction[i + ImgSize.x] == value && IsInDomain(i + ImgSize.x)))
		neigh.push_back(GetComponentIndex(i, 0, 1, 0));

	if (z == 0 || (z > 0 && ImplicitFunction[i - ImgSize.x * ImgSize.y] == value && IsInDomain(i - ImgSize.x * ImgSize.y)))
		neigh.push_back(GetComponentIndex(i, 0, 0, -1));

	if (z == (int) ImgSize.z - 1 || (z < (int)ImgSize.z - 1 && ImplicitFunction[i + ImgSize.x * ImgSize.y] == value && IsInDomain(i + ImgSize.x * ImgSize.y)))
		neigh.push_back(GetComponentIndex(i, 0, 0, 1));
}


template <class T> bool CompSweden3D_PreserveTopology<T>::IsSimplePoint(size_t i)
{
	int x, y, z;
	GETPOSITION_3D_COMP(x, y, z);

	if (x == 0 || x == (int)ImgSize.x + 1 || y == 0 || y == (int)ImgSize.y + 1 || z == 0 || z == (int)ImgSize.z + 1)
		return false;

	/*int pom = (ImgSize.x + 2) * (ImgSize.y + 2);
	int value = Component[i]; 
	i3d::Image3d<i3d::GRAY8> img;
	img.MakeRoom(3, 3, 3);

	img.SetVoxel(0, i3d::GRAY8(Component[i - pom - ImgSize.x - 2 - 1] == value));
	img.SetVoxel(1, i3d::GRAY8(Component[i - pom - ImgSize.x - 2] == value));
	img.SetVoxel(2, i3d::GRAY8(Component[i - pom - ImgSize.x - 2 + 1] == value));
	img.SetVoxel(3, i3d::GRAY8(Component[i - pom - 1] == value));
	img.SetVoxel(4, i3d::GRAY8(Component[i - pom] == value));
	img.SetVoxel(5, i3d::GRAY8(Component[i - pom + 1] == value));
	img.SetVoxel(6, i3d::GRAY8(Component[i - pom + ImgSize.x + 2 - 1] == value));
	img.SetVoxel(7, i3d::GRAY8(Component[i - pom + ImgSize.x + 2] == value));
	img.SetVoxel(8, i3d::GRAY8(Component[i - pom + ImgSize.x + 2 + 1] == value));

	img.SetVoxel(9, i3d::GRAY8(Component[i - ImgSize.x - 2 - 1] == value));
	img.SetVoxel(10, i3d::GRAY8(Component[i - ImgSize.x - 2] == value));
	img.SetVoxel(11, i3d::GRAY8(Component[i - ImgSize.x - 2 + 1] == value));
	img.SetVoxel(12, i3d::GRAY8(Component[i - 1] == value));
	img.SetVoxel(13, i3d::GRAY8(2));
	img.SetVoxel(14, i3d::GRAY8(Component[i + 1] == value));
	img.SetVoxel(15, i3d::GRAY8(Component[i + ImgSize.x + 2 - 1] == value));
	img.SetVoxel(16, i3d::GRAY8(Component[i + ImgSize.x + 2] == value));
	img.SetVoxel(17, i3d::GRAY8(Component[i + ImgSize.x + 2 + 1] == value));

	img.SetVoxel(18, i3d::GRAY8(Component[i + pom - ImgSize.x - 2 - 1] == value));
	img.SetVoxel(19, i3d::GRAY8(Component[i + pom - ImgSize.x - 2] == value));
	img.SetVoxel(20, i3d::GRAY8(Component[i + pom - ImgSize.x - 2 + 1] == value));
	img.SetVoxel(21, i3d::GRAY8(Component[i + pom - 1] == value));
	img.SetVoxel(22, i3d::GRAY8(Component[i + pom] == value));
	img.SetVoxel(23, i3d::GRAY8(Component[i + pom + 1] == value));
	img.SetVoxel(24, i3d::GRAY8(Component[i + pom + ImgSize.x + 2 - 1] == value));
	img.SetVoxel(25, i3d::GRAY8(Component[i + pom + ImgSize.x + 2] == value));
	img.SetVoxel(26, i3d::GRAY8(Component[i + pom + ImgSize.x + 2 + 1] == value));

	i3d::LabeledImage3d<size_t, i3d::GRAY8> limg;
	limg.CreateRegions(img);

	size_t num = limg.NumberOfComponents();

	if (num <= 3)
	{
		if (num == 2)
		{
			//return img.GetVoxel(0) == i3d::GRAY8(0);
			return (value != 0 && img.GetVoxel(0) == i3d::GRAY8(0));
		}
		else
		{
			return true;
		}
	}
	else
	{
		return false;
	}*/

	int step_x = 1;
	int step_y = ImgSize.x + 2;
	int step_z = (ImgSize.x + 2) * (ImgSize.y + 2);
	int component_value = Component[i];

	// get 26-neighbourhood of given point
	int neigh[26];
	neigh[0]  = Component[i - step_z - step_y - step_x] == component_value;
	neigh[1]  = Component[i - step_z - step_y         ] == component_value;
	neigh[2]  = Component[i - step_z - step_y + step_x] == component_value;
	neigh[3]  = Component[i - step_z          - step_x] == component_value;
	neigh[4]  = Component[i - step_z                  ] == component_value;
	neigh[5]  = Component[i - step_z          + step_x] == component_value;
	neigh[6]  = Component[i - step_z + step_y - step_x] == component_value;
	neigh[7]  = Component[i - step_z + step_y         ] == component_value;
	neigh[8]  = Component[i - step_z + step_y + step_x] == component_value;

	neigh[9]  = Component[i          - step_y - step_x] == component_value;
	neigh[10] = Component[i          - step_y         ] == component_value;
	neigh[11] = Component[i          - step_y + step_x] == component_value;
	neigh[12] = Component[i                   - step_x] == component_value;
	neigh[13] = Component[i                   + step_x] == component_value;
	neigh[14] = Component[i          + step_y - step_x] == component_value;
	neigh[15] = Component[i          + step_y         ] == component_value;
	neigh[16] = Component[i          + step_y + step_x] == component_value;

	neigh[17] = Component[i + step_z - step_y - step_x] == component_value;
	neigh[18] = Component[i + step_z - step_y         ] == component_value;
	neigh[19] = Component[i + step_z - step_y + step_x] == component_value;
	neigh[20] = Component[i + step_z          - step_x] == component_value;
	neigh[21] = Component[i + step_z                  ] == component_value;
	neigh[22] = Component[i + step_z          + step_x] == component_value;
	neigh[23] = Component[i + step_z + step_y - step_x] == component_value;
	neigh[24] = Component[i + step_z + step_y         ] == component_value;
	neigh[25] = Component[i + step_z + step_y + step_x] == component_value;

	return IsCorrect6(neigh);
}

template <class T> int CompSweden3D_PreserveTopology<T>::GetComponent(size_t i)
{
	int x, y, z;
	GETPOSITION_3D(x, y, z);

	return Component[((ImgSize.x + 2) * (ImgSize.y + 2)) * (z + 1) + (ImgSize.x + 2) * (y + 1) + x + 1];
}

template <class T> void CompSweden3D_PreserveTopology<T>::SetComponent(size_t i, int value)
{
	int x, y, z;
	GETPOSITION_3D(x, y, z);

	Component[((ImgSize.x + 2) * (ImgSize.y + 2)) * (z + 1) + (ImgSize.x + 2) * (y + 1) + x + 1] = value;
}


template <class T> size_t CompSweden3D_PreserveTopology<T>::GetComponentIndex(size_t i, int move_x, 
																			  int move_y, int move_z)
{
	int x, y, z;
	GETPOSITION_3D(x, y, z);

	return ((ImgSize.x + 2) * (ImgSize.y + 2)) * (z + 1 + move_z) + (ImgSize.x + 2) * (y + 1 + move_y) + 
		   x + 1 + move_x;
}

template <class T> size_t CompSweden3D_PreserveTopology<T>::GetIFIndex(size_t i)
{
	int x, y, z;
	GETPOSITION_3D_COMP(x, y, z);

	return ((z - 1) * (ImgSize.x * ImgSize.y) + (y - 1) * ImgSize.x + (x - 1));
}

/////////////////////////////////////////////////////////////////
//                                                             //
//                  Class CompChineseLevelSet                  //
//                                                             //
/////////////////////////////////////////////////////////////////

template <class T> CompChineseLevelSet<T>::CompChineseLevelSet(std::valarray<float> *ImageGrad, std::vector<size_t> &Seeds, 
									                           i3d::Vector3d<size_t> _ImgSize, i3d::Vector3d<float> _Spacing, 
									                           float _alpha, float _beta, int _p):
  ImageGradient(*ImageGrad), ImgSize(_ImgSize), Spacing(_Spacing), alpha(_alpha), beta(_beta), p(_p)
{ 
  const size_t size = ImgSize.x * ImgSize.y * ImgSize.z;
  ImplicitFunction.resize(size, UNDEFINED);
  BackPointers.resize(size, -1);
  KindOfPoint.resize(size, 1);
  heap = new ChineseHeap(BackPointers);
  Global_time = 0;
}

template <class T> void CompChineseLevelSet<T>::ComputeIteration()
{ 
  ChineseHeapNode *min = heap -> Pop();
  float time = min -> arrival_time - Global_time;

  std::vector<size_t> move;
  move.push_back(min -> position);

  size_t min_index = min -> position;
  size_t max_index = min -> position;

  while (heap -> HeapSize() != 0 && heap -> TimeOfRoot() == min -> arrival_time)
  {
    min = heap -> Pop();
    move.push_back(min -> position);
	if (min_index > min ->position)
      min_index = min -> position;
    if (max_index < min ->position)
      max_index = min -> position;
  }

  int mmin = min_index - ImgSize.x * 3 - 3;
  if (ImgSize.z > 1)
    mmin -= 3 * ImgSize.x * ImgSize.y;
  mmin = mmin < 0 ? 0 : mmin;
  int mmax = max_index + ImgSize.x * 3 + 3;
  if (ImgSize.z > 1)
    mmax += 3 * ImgSize.x * ImgSize.y;
  mmax = mmax > (int)ImplicitFunction.size() ? ImplicitFunction.size()-1 : mmax;

  int num = mmax - mmin + 1;
  Global_time += time;

  std::valarray<float> IF;
  std::valarray<float> Kind;
  std::valarray<bool> WasChanged;
  IF.resize(num,float(0));
  Kind.resize(num, float(0));
  WasChanged.resize(num, false);

  for (int i = mmin; i < mmax; i++)
  {
    IF[i - mmin] = ImplicitFunction[i];
	Kind[i - mmin] = KindOfPoint[i];
  }

  std::vector<size_t> ToInterface;

  for (size_t i = 0; i < move.size(); i++)
  {
	std::vector<size_t> moje;
	GetNeighbors(7, move[i], moje, true);
	for (size_t j = 0; j < moje.size(); j++)
      if (ImplicitFunction[moje[j]] == 0)
	  {
        if (!WasChanged[moje[j] - mmin])
	    {
          IF[moje[j] - mmin] = abs(time * ComputeSpeedFunction(moje[j]) * ComputeGradient(moje[j]));
	      Kind[moje[j] - mmin] = ComputeSpeedFunction(moje[j]) > 0 ? -1 : 1;
		  WasChanged[moje[j] - mmin] = true;
	    }
	  } 
    
	IF[move[i] - mmin] = 0;
	Kind[move[i] - mmin] = 0;
	WasChanged[move[i] - mmin] = true;

    std::vector<size_t> neigh;
    GetNeighbors(7, move[i], neigh, false);
    for (size_t j = 0; j < neigh.size(); j++)
    {
      if (!WasChanged[neigh[j] - mmin])
	  {
        IF[neigh[j] - mmin] -= time * ComputeSpeedFunction(neigh[j]) * ComputeGradient(neigh[j]);
	    if (IF[neigh[j] - mmin] == 0)
	    {
          Kind[neigh[j] - mmin] = 0;
	      ToInterface.push_back(neigh[j]);
		  heap -> Delete(BackPointers[neigh[j]]);
        }
		WasChanged[neigh[j] - mmin] = true;
	  }
    }

	ToInterface.push_back(move[i]);
  }

  for (int i = mmin; i < mmax; i++)
  {
    ImplicitFunction[i] = IF[i - mmin];
	KindOfPoint[i] = Kind[i - mmin];
  }

  std::vector<size_t> new_active;
  for (size_t j = 0; j < ToInterface.size(); j++)
  {
	std::vector<size_t> neigh2;
	GetNeighbors(3, ToInterface[j], neigh2, true);
	for (size_t a = 0; a < neigh2.size(); a++)
	{ 
      if (IsActivePoint(neigh2[a]) && BackPointers[neigh2[a]] == -1)
        new_active.push_back(neigh2[a]);
	}
  }

  for (size_t j = 0; j < new_active.size(); j++)
  {
    if (BackPointers[new_active[j]] == -1)
    {
      ComputeSignedDistance(new_active[j]);
	  float val = KindOfPoint[new_active[j]] * ImplicitFunction[new_active[j]] / (ComputeSpeedFunction(new_active[j]) * ComputeGradient(new_active[j]));
	  if (val > 0)
        heap -> Insert(new_active[j], val + Global_time);
	}   		
  }

  for (size_t i = 0; i < move.size(); i++)
  {
    std::vector<size_t> neigh3;
    GetNeighbors(3, move[i], neigh3, false);
    for (size_t j = 0; j < neigh3.size(); j++)
    {	 
      float val = KindOfPoint[neigh3[j]] * ImplicitFunction[neigh3[j]] / (ComputeSpeedFunction(neigh3[j]) * ComputeGradient(neigh3[j]));
      if (val > 0)
        heap -> Update(BackPointers[neigh3[j]], val + Global_time);
	  else
 	  {
	    heap -> Delete(BackPointers[neigh3[j]]);
	  }
    }
  }

  delete min;
}

template <class T> void CompChineseLevelSet<T>::ComputeSegmentatedImage(i3d::Image3d<T> &output, SegmentationType type)
{ 
  if (type)
  {
    for (size_t i = 0; i < ImplicitFunction.size(); i++)
      output.SetVoxel(i,  KindOfPoint[i] > 0 ? std::numeric_limits<T>::min() : std::numeric_limits<T>::max());
  }
  else
  {
     for (size_t i = 0; i < ImplicitFunction.size(); i++)
       output.SetVoxel(i,  KindOfPoint[i] > 0 ? std::numeric_limits<T>::max() : std::numeric_limits<T>::min());
  }
}

template <class T> void CompChineseLevelSet<T>::Init(std::vector<size_t> &Seeds)
{
  size_t size = ImgSize.x * ImgSize.y * ImgSize.z;
  for(size_t i = 0; i < Seeds.size(); i++)
  {
    if (Seeds[i] < size)
	{
      ImplicitFunction[Seeds[i]] = 0;
	  KindOfPoint[Seeds[i]] = 0;
	}
	else
	{
      #ifdef I3D_DEBUG
        cout << "Seed " << Seeds[i] << " was ignored. It isn't a point of processed image!" << endl;
      #endif
	}
  }

  std::vector<size_t> candidate;
  for (size_t i = 0; i < ImplicitFunction.size(); i++)
  {
    if (IsActivePoint(i))
	{
      ComputeSignedDistance(i);
	  candidate.push_back(i);
	}
  }

  for (size_t i = 0; i < candidate.size(); i++)
  {
    float val = KindOfPoint[candidate[i]] * ImplicitFunction[candidate[i]] / (ComputeSpeedFunction(candidate[i]) * ComputeGradient(candidate[i]));
    if (val > 0)
      heap -> Insert(candidate[i], val);
  }
}

/////////////////////////////////////////////////////////////////
//                                                             //
//                      Class CompChinese2D                    //
//                                                             //
/////////////////////////////////////////////////////////////////

template <class T> CompChinese2D<T>::CompChinese2D(std::valarray<float> *ImageGrad, std::vector<size_t> &Seeds, 
											  	   i3d::Vector3d<size_t> ImgSize, i3d::Vector3d<float> Spacing, 
												   float alpha, float beta, int p):
  CompChineseLevelSet<T>(ImageGrad, Seeds, ImgSize, Spacing, alpha, beta, p)
{   
  Init(Seeds);
}

template <class T> void CompChinese2D<T>::ComputeSignedDistance(size_t i)
{
  std::vector<size_t> neigh;
  GetNeighbors(5, i, neigh, true);
  int lx = neigh[0] % ImgSize.x;
  int ly = neigh[0] / ImgSize.x;
  int ux = neigh[neigh.size() - 1] % ImgSize.x;
  int uy = neigh[neigh.size() - 1] / ImgSize.x;

  // from top to the bottom
  for (size_t j = 0; j < neigh.size(); j++)
  {
    int x = neigh[j] % ImgSize.x;
	int y = neigh[j] / ImgSize.x;

	bool b1 = y - 1 >= ly;
	bool b2 = x - 1 >= lx;
	bool b3 = x + 1 <= ux;

	if (b1 && b2)
      ImplicitFunction[neigh[j]] = MIN(ImplicitFunction[neigh[j]], ImplicitFunction[neigh[j] - ImgSize.x - 1] + sqrt(/*2.0*/Spacing.x+Spacing.y));
	if (b1)
      ImplicitFunction[neigh[j]] = MIN(ImplicitFunction[neigh[j]], ImplicitFunction[neigh[j] - ImgSize.x] + /*1*/Spacing.y);
	if (b1 && b3)
      ImplicitFunction[neigh[j]] = MIN(ImplicitFunction[neigh[j]], ImplicitFunction[neigh[j] - ImgSize.x + 1] + sqrt(/*2.0*/Spacing.x+Spacing.y));
	if (b2)
      ImplicitFunction[neigh[j]] = MIN(ImplicitFunction[neigh[j]], ImplicitFunction[neigh[j] - 1] + /*1*/Spacing.x);
  }

  // from bottom to the top
  int num = neigh.size() - 1;
  for (int j = num; j >= 0; j--)
  {
    int x = neigh[j] % ImgSize.x;
	int y = neigh[j] / ImgSize.x;

	bool b1 = y + 1 <= uy;
	bool b2 = x + 1 <= ux;
	bool b3 = x - 1 >= lx;

	if (b1 && b2)
      ImplicitFunction[neigh[j]] = MIN(ImplicitFunction[neigh[j]], ImplicitFunction[neigh[j] + ImgSize.x + 1] + sqrt(/*2.0*/Spacing.x+Spacing.y));
	if (b1)
      ImplicitFunction[neigh[j]] = MIN(ImplicitFunction[neigh[j]], ImplicitFunction[neigh[j] + ImgSize.x] + /*1*/Spacing.y);
	if (b1 && b3)
      ImplicitFunction[neigh[j]] = MIN(ImplicitFunction[neigh[j]], ImplicitFunction[neigh[j] + ImgSize.x - 1] + sqrt(/*2.0*/Spacing.x+Spacing.y));
	if (b2)
      ImplicitFunction[neigh[j]] = MIN(ImplicitFunction[neigh[j]], ImplicitFunction[neigh[j] + 1] + /*1*/Spacing.x);
  }
}

template <class T> float CompChinese2D<T>::ComputeGradient(size_t i)
{
  int x = i % ImgSize.x;
  int y = i / ImgSize.x;

  float bDx = 0;
  float bDy = 0;
  float val = KindOfPoint[i] * ImplicitFunction[i];
  
  // Backward differentation
  if (x > 0)
    bDx = SIDE(KindOfPoint[i - 1] * ImplicitFunction[i - 1], val, Spacing.x);
  
  if (y > 0)
    bDy = SIDE(KindOfPoint[i - ImgSize.x] * ImplicitFunction[i - ImgSize.x], val, Spacing.y);
  
  float fDx = 0;
  float fDy = 0;
  
  // Forward differentation
  if (x < (int)ImgSize.x - 1)
    fDx = SIDE(val, KindOfPoint[i + 1] * ImplicitFunction[i + 1], Spacing.x);
  
  if (y < (int)ImgSize.y - 1)
    fDy = SIDE(val, KindOfPoint[i + ImgSize.x] * ImplicitFunction[i + ImgSize.x], Spacing.y);

  float value = sqrt(SQR(MAX(bDx,0)) + SQR(MIN(fDx,0)) + SQR(MAX(bDy,0)) + SQR(MIN(fDy,0)));
   
  //return 1;
  return value;
}

template <class T> float CompChinese2D<T>::ComputeSpeedFunction(size_t i)
{ 
  int x = i % ImgSize.x;
  int y = i / ImgSize.x;

  float Dx = -2 * KindOfPoint[i] * ImplicitFunction[i];
  float Dy = -2 * KindOfPoint[i] * ImplicitFunction[i];
  float IGx = 0;
  float IGy = 0;
    
  if (x > 0 && x < (int)ImgSize.x - 1)
  {
    Dx = CENTRAL(KindOfPoint[i - 1] * ImplicitFunction[i - 1], KindOfPoint[i + 1] * ImplicitFunction[i + 1], Spacing.x);
	IGx = CENTRAL(ImageGradient[i - 1], ImageGradient[i + 1], Spacing.x);
  }
 
  if (y > 0 && y < (int)ImgSize.y - 1)
  {
    Dy = CENTRAL(KindOfPoint[i - ImgSize.x] * ImplicitFunction[i - ImgSize.x], KindOfPoint[i + ImgSize.x] * ImplicitFunction[i + ImgSize.x], Spacing.y);
    IGy = CENTRAL(ImageGradient[i - ImgSize.x], ImageGradient[i + ImgSize.x], Spacing.x);
  }
 
  float val = MAX(sqrt(SQR(Dx) + SQR(Dy)), EPSILON);
  //float Nx = Dx / val;
  //float Ny = Dy / val;

  float Dxx = 0, Dyy = 0, Dxy = 0;

  if (x > 0 && x < (int)ImgSize.x - 1)
    Dxx += KindOfPoint[i + 1] * ImplicitFunction[i + 1] + KindOfPoint[i - 1] * ImplicitFunction[i - 1];

  if (y > 0 && y < (int)ImgSize.y - 1)
    Dyy += KindOfPoint[i + ImgSize.x] * ImplicitFunction[i + ImgSize.x] + KindOfPoint[i - ImgSize.x] * ImplicitFunction[i - ImgSize.x];

  if (x > 0 && x < (int)ImgSize.x - 1 && y > 0 && y < (int)ImgSize.y - 1)
    Dxy = (KindOfPoint[i + ImgSize.x + 1] * ImplicitFunction[i + ImgSize.x + 1] - KindOfPoint[i - ImgSize.x + 1] * ImplicitFunction[i - ImgSize.x + 1] - KindOfPoint[i + ImgSize.x - 1] * ImplicitFunction[i + ImgSize.x - 1] + KindOfPoint[i - ImgSize.x - 1] * ImplicitFunction[i - ImgSize.x - 1]) / (4 * SQR(Spacing.x));

  float cur = (SQR(Dx) * Dyy - 0.5 * Dx * Dy * Dxy + SQR(Dy) * Dxx) / (val * SQR(Spacing.x)); 
  cur = cur > 1 / Spacing.x ? 1 / Spacing.x : cur;
  cur = cur < -1 / Spacing.x ? -1 / Spacing.x : cur;

  float fDx = 0, bDx = 0, fDy = 0, bDy = 0;
  if (x > 0)
    bDx = SIDE(KindOfPoint[i-1] * ImplicitFunction[i-1], KindOfPoint[i] * ImplicitFunction[i], Spacing.x);
  if (x < (int)ImgSize.x - 1)
    fDx = SIDE(KindOfPoint[i] * ImplicitFunction[i], KindOfPoint[i+1] * ImplicitFunction[i+1], Spacing.x);
  if (y > 0)
    bDy = SIDE(KindOfPoint[i-ImgSize.x] * ImplicitFunction[i-ImgSize.x], KindOfPoint[i] * ImplicitFunction[i], Spacing.y);
  if (y < (int)ImgSize.y - 1)
    fDy = SIDE(KindOfPoint[i] * ImplicitFunction[i], KindOfPoint[i+ImgSize.x] * ImplicitFunction[i+ImgSize.x], Spacing.y);

  float adv = MAX(IGx, 0) * bDx + MIN(IGx, 0) * fDx + MAX(IGy, 0) * bDy + MIN(IGy, 0) * fDy;

  //float Godunov = sqrt(MAX(SQR(MAX(bDx, 0)), SQR(MIN(fDx, 0))) + MAX(SQR(MAX(bDy, 0)), SQR(MIN(fDy, 0))));

  float grad;
  if (p == 2)
    grad = 1 / SQR(1 + ImageGradient[i]);
  else 
    grad = 1 / (1 + ImageGradient[i]);

  return (grad * (1 - alpha * cur) + beta * adv);
}

template <class T> void CompChinese2D<T>::GetNeighbors(size_t size, size_t i, std::vector<size_t> &neigh, bool all)
{
  int x = i % ImgSize.x;
  int y = i / ImgSize.x;

  int lx = x - size / 2;
  int ux = x + size / 2;
  int ly = y - size / 2;
  int uy = y + size / 2;

  lx = lx > 0 ? lx : 0;
  ux = ux < (int)ImgSize.x - 1 ? ux : ImgSize.x - 1;
  ly = ly > 0 ? ly : 0;
  uy = uy < (int)ImgSize.y - 1 ? uy : ImgSize.y - 1;

  size_t index;
  for (int y = ly; y <= uy; y++)
    for (int x = lx; x <= ux; x++)
	{
      index = y * ImgSize.x + x;
	  if (all || BackPointers[index] != -1)
	    neigh.push_back(index);
	}
}

template <class T> bool CompChinese2D<T>::IsActivePoint(size_t i)
{
  if (ImplicitFunction[i] == 0)
    return false;
  
  int x = i % ImgSize.x;
  int y = i / ImgSize.x;

  if ((x > 0 && KindOfPoint[i - 1] * KindOfPoint[i] <= 0) || 
	  (x < (int)ImgSize.x - 1 && KindOfPoint[i + 1] * KindOfPoint[i] <= 0) ||
      (y > 0 && KindOfPoint[i - ImgSize.x] * KindOfPoint[i] <= 0) || 
	  (y < (int)ImgSize.y - 1 && KindOfPoint[i + ImgSize.x] * KindOfPoint[i] <= 0))
    return true;
      
  return false;    
}

/////////////////////////////////////////////////////////////////
//                                                             //
//                      Class CompChinese3D                    //
//                                                             //
/////////////////////////////////////////////////////////////////

template <class T> CompChinese3D<T>::CompChinese3D(std::valarray<float> *ImageGrad, std::vector<size_t> &Seeds, 
											  	   i3d::Vector3d<size_t> ImgSize, i3d::Vector3d<float> Spacing, 
												   float alpha, float beta, int p):
  CompChineseLevelSet<T>(ImageGrad, Seeds, ImgSize, Spacing, alpha, beta, p)
{   
  Init(Seeds);
}

template <class T> void CompChinese3D<T>::ComputeSignedDistance(size_t i)
{

  std::vector<size_t> neigh;
  GetNeighbors(5, i, neigh, true);
  int lx = neigh[0] % ImgSize.x;
  int ly = (neigh[0] / ImgSize.x) % ImgSize.y;
  int lz = (neigh[0] / ImgSize.x) / ImgSize.y;
  int ux = neigh[neigh.size() - 1] % ImgSize.x;
  int uy = (neigh[neigh.size() - 1] / ImgSize.x) % ImgSize.y;
  int uz = (neigh[neigh.size() - 1] / ImgSize.x) / ImgSize.y;

  // from top to the bottom
  for (size_t j = 0; j < neigh.size(); j++)
  {
    int x = neigh[j] % ImgSize.x;
    int y = (neigh[j] / ImgSize.x) % ImgSize.y;
    int z = (neigh[j] / ImgSize.x) / ImgSize.y;

	bool b0 = z - 1 >= lz;
	bool b4 = y + 1 <= uy;
	bool b1 = y - 1 >= ly;
	bool b2 = x - 1 >= lx;
	bool b3 = x + 1 <= ux;

	if (b1 && b2)
      ImplicitFunction[neigh[j]] = MIN(ImplicitFunction[neigh[j]], ImplicitFunction[neigh[j] - ImgSize.x - 1] + sqrt(/*2.0*/Spacing.x+Spacing.y));
	if (b1)
      ImplicitFunction[neigh[j]] = MIN(ImplicitFunction[neigh[j]], ImplicitFunction[neigh[j] - ImgSize.x] + /*1*/Spacing.y);
	if (b1 && b3)
      ImplicitFunction[neigh[j]] = MIN(ImplicitFunction[neigh[j]], ImplicitFunction[neigh[j] - ImgSize.x + 1] + sqrt(/*2.0*/Spacing.x+Spacing.y));
	if (b2)
      ImplicitFunction[neigh[j]] = MIN(ImplicitFunction[neigh[j]], ImplicitFunction[neigh[j] - 1] + /*1*/Spacing.x);

	if (b0)
      ImplicitFunction[neigh[j]] = MIN(ImplicitFunction[neigh[j]], ImplicitFunction[neigh[j] - ImgSize.x * ImgSize.y] + /*1*/Spacing.z);
	if (b0 && b1 && b2)
      ImplicitFunction[neigh[j]] = MIN(ImplicitFunction[neigh[j]], ImplicitFunction[neigh[j] - ImgSize.x * ImgSize.y - ImgSize.x - 1] + sqrt(/*3.0*/Spacing.x+Spacing.y+Spacing.z));
    if (b0 && b1 && b3)
      ImplicitFunction[neigh[j]] = MIN(ImplicitFunction[neigh[j]], ImplicitFunction[neigh[j] - ImgSize.x * ImgSize.y - ImgSize.x + 1] + sqrt(/*3.0*/Spacing.x+Spacing.y+Spacing.z));
	if (b0 && b4 && b2)
      ImplicitFunction[neigh[j]] = MIN(ImplicitFunction[neigh[j]], ImplicitFunction[neigh[j] - ImgSize.x * ImgSize.y + ImgSize.x - 1] + sqrt(/*3.0*/Spacing.x+Spacing.y+Spacing.z));
	if (b0 && b1 && b3)
      ImplicitFunction[neigh[j]] = MIN(ImplicitFunction[neigh[j]], ImplicitFunction[neigh[j] - ImgSize.x * ImgSize.y + ImgSize.x + 1] + sqrt(/*3.0*/Spacing.x+Spacing.y+Spacing.z));
	if (b0 && b1)
      ImplicitFunction[neigh[j]] = MIN(ImplicitFunction[neigh[j]], ImplicitFunction[neigh[j] - ImgSize.x * ImgSize.y - ImgSize.x] + sqrt(/*2.0*/Spacing.z + Spacing.y));
    if (b0 && b4)
      ImplicitFunction[neigh[j]] = MIN(ImplicitFunction[neigh[j]], ImplicitFunction[neigh[j] - ImgSize.x * ImgSize.y + ImgSize.x] + sqrt(/*2.0*/Spacing.z+Spacing.y));
	if (b0 && b2)
      ImplicitFunction[neigh[j]] = MIN(ImplicitFunction[neigh[j]], ImplicitFunction[neigh[j] - ImgSize.x * ImgSize.y - 1] + sqrt(/*2.0*/Spacing.z+Spacing.x));
	if (b0 && b3)
      ImplicitFunction[neigh[j]] = MIN(ImplicitFunction[neigh[j]], ImplicitFunction[neigh[j] - ImgSize.x * ImgSize.y + 1] + sqrt(/*2.0*/Spacing.z+Spacing.x));
  }

  // from bottom to the top
  int num = neigh.size() - 1;
  for (int j = num; j >= 0; j--)
  {
    int x = neigh[j] % ImgSize.x;
    int y = (neigh[j] / ImgSize.x) % ImgSize.y;
    int z = (neigh[j] / ImgSize.x) / ImgSize.y;

	bool b0 = z + 1 <= uz;
	bool b4 = y - 1 >= ly;
	bool b1 = y + 1 <= uy;
	bool b2 = x + 1 <= ux;
	bool b3 = x - 1 >= lx;

	if (b1 && b2)
      ImplicitFunction[neigh[j]] = MIN(ImplicitFunction[neigh[j]], ImplicitFunction[neigh[j] + ImgSize.x + 1] + sqrt(/*2.0*/Spacing.y+Spacing.x));
	if (b1)
      ImplicitFunction[neigh[j]] = MIN(ImplicitFunction[neigh[j]], ImplicitFunction[neigh[j] + ImgSize.x] + /*1*/Spacing.y);
	if (b1 && b3)
      ImplicitFunction[neigh[j]] = MIN(ImplicitFunction[neigh[j]], ImplicitFunction[neigh[j] + ImgSize.x - 1] + sqrt(/*2.0*/Spacing.y+Spacing.x));
	if (b2)
      ImplicitFunction[neigh[j]] = MIN(ImplicitFunction[neigh[j]], ImplicitFunction[neigh[j] + 1] + /*1*/Spacing.x);

	if (b0)
      ImplicitFunction[neigh[j]] = MIN(ImplicitFunction[neigh[j]], ImplicitFunction[neigh[j] + ImgSize.x * ImgSize.y] + /*1*/Spacing.z);
	if (b0 && b1 && b2)
      ImplicitFunction[neigh[j]] = MIN(ImplicitFunction[neigh[j]], ImplicitFunction[neigh[j] + ImgSize.x * ImgSize.y + ImgSize.x + 1] + sqrt(/*3.0*/Spacing.x+Spacing.y+Spacing.z));
    if (b0 && b1 && b3)
      ImplicitFunction[neigh[j]] = MIN(ImplicitFunction[neigh[j]], ImplicitFunction[neigh[j] + ImgSize.x * ImgSize.y + ImgSize.x - 1] + sqrt(/*3.0*/Spacing.x+Spacing.y+Spacing.z));
	if (b0 && b4 && b2)
      ImplicitFunction[neigh[j]] = MIN(ImplicitFunction[neigh[j]], ImplicitFunction[neigh[j] + ImgSize.x * ImgSize.y - ImgSize.x + 1] + sqrt(/*3.0*/Spacing.x+Spacing.y+Spacing.z));
	if (b0 && b1 && b3)
      ImplicitFunction[neigh[j]] = MIN(ImplicitFunction[neigh[j]], ImplicitFunction[neigh[j] + ImgSize.x * ImgSize.y - ImgSize.x - 1] + sqrt(/*3.0*/Spacing.x+Spacing.y+Spacing.z));
	if (b0 && b1)
      ImplicitFunction[neigh[j]] = MIN(ImplicitFunction[neigh[j]], ImplicitFunction[neigh[j] + ImgSize.x * ImgSize.y + ImgSize.x] + sqrt(/*2.0*/Spacing.z+Spacing.y));
    if (b0 && b4)
      ImplicitFunction[neigh[j]] = MIN(ImplicitFunction[neigh[j]], ImplicitFunction[neigh[j] + ImgSize.x * ImgSize.y - ImgSize.x] + sqrt(/*2.0*/Spacing.z+Spacing.y));
	if (b0 && b2)
      ImplicitFunction[neigh[j]] = MIN(ImplicitFunction[neigh[j]], ImplicitFunction[neigh[j] + ImgSize.x * ImgSize.y + 1] + sqrt(/*2.0*/Spacing.z+Spacing.x));
	if (b0 && b3)
      ImplicitFunction[neigh[j]] = MIN(ImplicitFunction[neigh[j]], ImplicitFunction[neigh[j] + ImgSize.x * ImgSize.y - 1] + sqrt(/*2.0*/Spacing.z+Spacing.x));
  }

}

template <class T> float CompChinese3D<T>::ComputeGradient(size_t i)
{
  
  int x = i % ImgSize.x;
  int y = (i / ImgSize.x) % ImgSize.y;
  int z = (i / ImgSize.x) / ImgSize.y;

  float bDx = 0;
  float bDy = 0;
  float bDz = 0;
  float val = KindOfPoint[i] * ImplicitFunction[i];
  
  // Backward differentation
  if (x > 0)
    bDx = SIDE(KindOfPoint[i - 1] * ImplicitFunction[i - 1], val, Spacing.x);
  
  if (y > 0)
    bDy = SIDE(KindOfPoint[i - ImgSize.x] * ImplicitFunction[i - ImgSize.x], val, Spacing.y);

  if (z > 0)
    bDz = SIDE(KindOfPoint[i - ImgSize.x * ImgSize.y] * ImplicitFunction[i - ImgSize.x * ImgSize.y], val, Spacing.z);
  
  float fDx = 0;
  float fDy = 0;
  float fDz = 0;
  
  // Forward differentation
  if (x < (int)ImgSize.x - 1)
    fDx = SIDE(val, KindOfPoint[i + 1] * ImplicitFunction[i + 1], Spacing.x);
  
  if (y <  (int)ImgSize.y - 1)
    fDy = SIDE(val, KindOfPoint[i + ImgSize.x] * ImplicitFunction[i + ImgSize.x], Spacing.y);

  if (z <  (int)ImgSize.z - 1)
    fDz = SIDE(val, KindOfPoint[i + ImgSize.x * ImgSize.y] * ImplicitFunction[i + ImgSize.x * ImgSize.y], Spacing.z);

  float value = sqrt(SQR(MAX(bDx,0)) + SQR(MIN(fDx,0)) + SQR(MAX(bDy,0)) + SQR(MIN(fDy,0)) + SQR(MAX(bDz,0)) + SQR(MIN(fDz,0)));

  return MAX(value, EPSILON);
}

template <class T> float CompChinese3D<T>::ComputeSpeedFunction(size_t i)
{ 
  int x = i % ImgSize.x;
  int y = (i / ImgSize.x) % ImgSize.y;
  int z = (i / ImgSize.x) / ImgSize.y;

  float Dx = 0;
  float Dy = 0;
  float Dz = 0;
  float IGx = 0;
  float IGy = 0;
  float IGz = 0;
    
  if (x > 0 && x < (int)ImgSize.x - 1)
  {
    Dx = CENTRAL(KindOfPoint[i - 1] * ImplicitFunction[i - 1], KindOfPoint[i + 1] * ImplicitFunction[i + 1], Spacing.x);
	IGx = CENTRAL(ImageGradient[i - 1], ImageGradient[i + 1], Spacing.x);
  }
  
  if (y > 0 && y <  (int)ImgSize.y - 1)
  {
    Dy = CENTRAL(KindOfPoint[i - ImgSize.x] * ImplicitFunction[i - ImgSize.x], KindOfPoint[i + ImgSize.x] * ImplicitFunction[i + ImgSize.x], Spacing.y);
    IGy = CENTRAL(ImageGradient[i - ImgSize.x], ImageGradient[i + ImgSize.x], Spacing.x);
  }

  if (z > 0 && z <  (int)ImgSize.z - 1)
  {
    Dz = CENTRAL(KindOfPoint[i - ImgSize.x * ImgSize.y] * ImplicitFunction[i - ImgSize.x * ImgSize.y], KindOfPoint[i + ImgSize.x * ImgSize.y] * ImplicitFunction[i + ImgSize.x * ImgSize.y], Spacing.z);
    IGz = CENTRAL(ImageGradient[i - ImgSize.x * ImgSize.y], ImageGradient[i + ImgSize.x * ImgSize.y], Spacing.x);
  }

  float val = MAX(sqrt(SQR(Dx) + SQR(Dy) + SQR(Dz)), EPSILON);
  float Nx = Dx / val;
  float Ny = Dy / val;
  float Nz = Dz / val;

  float Dxx = 0, Dyy = 0, Dzz = 0, Dxy = 0, Dxz = 0, Dyz = 0;

  if (x > 0 && x < (int)ImgSize.x - 1)
    Dxx = (KindOfPoint[i + 1] * ImplicitFunction[i + 1] - 2 * KindOfPoint[i] * ImplicitFunction[i] + KindOfPoint[i - 1] * ImplicitFunction[i - 1]) / SQR(Spacing.x);

  if (y > 0 && y < (int)ImgSize.y - 1)
    Dyy = (KindOfPoint[i + ImgSize.x] * ImplicitFunction[i + ImgSize.x] - 2 * KindOfPoint[i] * ImplicitFunction[i] + KindOfPoint[i - ImgSize.x] * ImplicitFunction[i - ImgSize.x]) / SQR(Spacing.y);

  if (z > 0 && z < (int)ImgSize.z - 1)
    Dzz = (KindOfPoint[i + ImgSize.x * ImgSize.y] * ImplicitFunction[i + ImgSize.x * ImgSize.y] - 2 * KindOfPoint[i] * ImplicitFunction[i] + KindOfPoint[i - ImgSize.x * ImgSize.y] * ImplicitFunction[i - ImgSize.x * ImgSize.y]) / SQR(Spacing.z);

  if (x > 0 && x < (int)ImgSize.x - 1 && y > 0 && y < (int)ImgSize.y - 1)
    Dxy = (KindOfPoint[i + ImgSize.x + 1] * ImplicitFunction[i + ImgSize.x + 1] - KindOfPoint[i - ImgSize.x + 1] * ImplicitFunction[i - ImgSize.x + 1] - KindOfPoint[i + ImgSize.x - 1] * ImplicitFunction[i + ImgSize.x - 1] + KindOfPoint[i - ImgSize.x - 1] * ImplicitFunction[i - ImgSize.x - 1]) / (4 * SQR(Spacing.x));

  if (x > 0 && x < (int)ImgSize.x - 1 && z > 0 && z < (int)ImgSize.z - 1)
    Dxz = (KindOfPoint[i + ImgSize.x * ImgSize.y + 1] * ImplicitFunction[i + ImgSize.x * ImgSize.y + 1] - KindOfPoint[i - ImgSize.x * ImgSize.y + 1] * ImplicitFunction[i - ImgSize.x * ImgSize.y + 1] - KindOfPoint[i + ImgSize.x * ImgSize.y - 1] * ImplicitFunction[i + ImgSize.x * ImgSize.y - 1] + KindOfPoint[i - ImgSize.x * ImgSize.y - 1] * ImplicitFunction[i - ImgSize.x * ImgSize.y - 1]) / (4 * SQR(Spacing.x));

  if (y > 0 && y < (int)ImgSize.y - 1 && z > 0 && z < (int)ImgSize.z - 1)
    Dyz = (KindOfPoint[i + ImgSize.x * ImgSize.y + ImgSize.x] * ImplicitFunction[i + ImgSize.x * ImgSize.y + ImgSize.x] - KindOfPoint[i - ImgSize.x * ImgSize.y + ImgSize.x] * ImplicitFunction[i - ImgSize.x * ImgSize.y + ImgSize.x] - KindOfPoint[i + ImgSize.x * ImgSize.y - ImgSize.x] * ImplicitFunction[i + ImgSize.x * ImgSize.y - ImgSize.x] + KindOfPoint[i - ImgSize.x * ImgSize.y - ImgSize.x] * ImplicitFunction[i - ImgSize.x * ImgSize.y - ImgSize.x]) / (4 * SQR(Spacing.y));  

  float cur = (SQR(Dx) * Dyy - 2 * Dx * Dy * Dxy + SQR(Dy) * Dxx + SQR(Dx) * Dzz - 2 * Dx * Dz * Dxz + SQR(Dz) * Dxx + SQR(Dy) * Dzz - 2 * Dy * Dz * Dyz + SQR(Dz) * Dyy) / (val * val * val); 
  cur = cur > 1 / Spacing.x ? 1 / Spacing.x : cur;
  cur = cur < -1 / Spacing.x ? -1 / Spacing.x : cur;

  float grad;
  if (p == 2)
    grad = 1 / SQR(1 + ImageGradient[i]);
  else 
    grad = 1 / (1 + ImageGradient[i]);

  
  return (grad * (1 - alpha * cur) - beta * (Nx * IGx + Ny * IGy + Nz * IGz));
}

template <class T> void CompChinese3D<T>::GetNeighbors(size_t size, size_t i, std::vector<size_t> &neigh, bool all)
{

  int x = i % ImgSize.x;
  int y = (i / ImgSize.x) % ImgSize.y;
  int z = (i / ImgSize.x) / ImgSize.y;

  int lx = x - size / 2;
  int ux = x + size / 2;
  int ly = y - size / 2;
  int uy = y + size / 2;
  int lz = z - size / 2;
  int uz = z + size / 2;


  lx = lx > 0 ? lx : 0;
  ux = ux < (int) ImgSize.x - 1 ? ux : ImgSize.x - 1;
  ly = ly > 0 ? ly : 0;
  uy = uy < (int) ImgSize.y - 1 ? uy : ImgSize.y - 1;
  lz = lz > 0 ? lz : 0;
  uz = uz < (int) ImgSize.z - 1 ? uz : ImgSize.z - 1;

  size_t index;
  for (int z = lz; z <= uz; z++)
    for (int y = ly; y <= uy; y++)
      for (int x = lx; x <= ux; x++)
	  {
		index = (z * ImgSize.y + y) * ImgSize.x + x;
	    if (all || BackPointers[index] != -1)
	      neigh.push_back(index);
	  }

}

template <class T> bool CompChinese3D<T>::IsActivePoint(size_t i)
{
  if (ImplicitFunction[i] == 0)
    return false;

  int x = i % ImgSize.x;
  int y = (i / ImgSize.x) % ImgSize.y;
  int z = (i / ImgSize.x) / ImgSize.y;

  if ((x > 0 && KindOfPoint[i - 1] * KindOfPoint[i] <= 0) || 
	  (x < (int) ImgSize.x - 1 && KindOfPoint[i + 1] * KindOfPoint[i] <= 0) ||
      (y > 0 && KindOfPoint[i - ImgSize.x] * KindOfPoint[i] <= 0) ||
	  (y < (int) ImgSize.y - 1 && KindOfPoint[i + ImgSize.x] * KindOfPoint[i] <= 0) ||
	  (z > 0 && KindOfPoint[i - ImgSize.x * ImgSize.y] * KindOfPoint[i] <= 0) || 
	  (z < (int) ImgSize.z - 1 && KindOfPoint[i + ImgSize.x * ImgSize.y] * KindOfPoint[i] <= 0))
    return true;
  
  return false;    
}

/////////////////////////////////////////////////////////////////
//                                                             //
//                    Class SparseFieldRoutine                 //
//                                                             //
/////////////////////////////////////////////////////////////////

template <class T> SparseFieldRoutine<T>::SparseFieldRoutine(std::valarray<float> *ImageGrad, std::vector<size_t> &Seeds, 
                                                             i3d::Vector3d<size_t> _ImgSize, float _spacing, float _alpha,
															 float _beta, int _p):
  ImageGradient(*ImageGrad),  Layers(5, std::list<size_t>()), ImgSize(_ImgSize), spacing(_spacing), CurvatureLimit(1.0 / spacing), ActiveLayerBound(0.5 * spacing), alpha(_alpha), beta(_beta), p(_p)
{ 
  size_t size = ImgSize.x * ImgSize.y * ImgSize.z;
  ImplicitFunction.resize(size, UNDEFINED);
  Status.resize(size, StatusNull);
}

template <class T> void SparseFieldRoutine<T>::ComputeIteration()
{ 
  if (Layers[0].size() == 0)
	throw i3d::InternalException("Nothing to process");

  std::vector<std::list<size_t> > ChangeUp = std::vector<std::list<size_t> >(2, std::list<size_t>());
  std::vector<std::list<size_t> > ChangeDown = std::vector<std::list<size_t> >(2, std::list<size_t>());

  UpdateInterface(ChangeUp[0], ChangeDown[0]);

  UpdateStatus(ChangeUp[0], ChangeUp[1], 2, 1);
  UpdateStatus(ChangeDown[0], ChangeDown[1], 1, 2);
  UpdateStatus(ChangeUp[1], ChangeUp[0], 0, 3);
  UpdateStatus(ChangeDown[1], ChangeDown[0], 0, 4);
  UpdateStatus(ChangeUp[0], ChangeUp[1], 1, StatusNull);
  UpdateStatus(ChangeDown[0], ChangeDown[1], 2, StatusNull);

  UpdateBorderLayer(ChangeUp[1], 3);
  UpdateBorderLayer(ChangeDown[1], 4);
    
  UpdateImplicitFunction(0, 1, 3);
  UpdateImplicitFunction(0, 2, 4);
  UpdateImplicitFunction(1, 3, Layers.size());
  UpdateImplicitFunction(2, 4, Layers.size());
}

template <class T> void SparseFieldRoutine<T>::UpdateInterface(std::list<size_t> &up, std::list<size_t> &down)
{
  std::list<size_t>::iterator iter;
  std::vector<float> buffer;
  std::vector<size_t> neigh;
  size_t i = 0;
  float new_value, tmp;

  MaxAdv = 0.0;
  MaxProp = 0.0;

  for (iter = Layers[0].begin(); iter != Layers[0].end(); iter++)
  {
    buffer.push_back(ComputeSpeedFunction(*iter));
  }

  TimeStep = 0.9 / ((MaxProp + MaxAdv * beta) / spacing + 6 * alpha / SQR(spacing));

  for (iter = Layers[0].begin(); iter != Layers[0].end(); iter++, i++)
  {
    new_value = ImplicitFunction[*iter] - TimeStep * buffer[i];

    if (new_value > ActiveLayerBound)
    { 
	  // the image point will be moved into the outer layer
      up.push_back(*iter);
      tmp = new_value - spacing;

	  // update ImplicitFunction of its inner neighbors
	  GetNeighbors(*iter, neigh, 1);
      for (size_t j = 0; j < neigh.size(); j++)
      {
        // keep the new active point the closest to the zero level set 
        if (ImplicitFunction[neigh[j]] < -ActiveLayerBound || tmp > ImplicitFunction[neigh[j]])
          ImplicitFunction[neigh[j]] = tmp;
      }
    }
    else 
      if (new_value < -ActiveLayerBound)
      { 
        // the image point will be moved into the inner layer
        down.push_back(*iter);
		tmp = new_value + spacing;
       
        // update ImplicitFunction of its outer neighbors
	    GetNeighbors(*iter, neigh, 2);
        for (size_t j = 0; j < neigh.size(); j++)
        {
          // keep the new active point the closest to the zero level set 
		  if (ImplicitFunction[neigh[j]] > ActiveLayerBound || tmp < ImplicitFunction[neigh[j]])
            ImplicitFunction[neigh[j]] = tmp;
		}
	  }
      else
      {
        // the image point remains in the active range
        ImplicitFunction[*iter] = new_value;
      }
  }

}

template <class T> void SparseFieldRoutine<T>::UpdateStatus(std::list<size_t> &input, std::list<size_t> &output,
															size_t ChangeTo, int SearchFor)
{ 
  std::list<size_t>::iterator iter;
  std::vector<size_t> neigh;
  
  for (iter = input.begin(); iter != input.end(); iter++)
  {
	// find the current layer of the image point
	int cur = 0;
    if (SearchFor == StatusNull)
      cur = ChangeTo + 2;
	else
      cur = SearchFor - 2;
	cur = cur < 0 ? 0 : cur;

	// remove the image point from the current layer
    Layers[cur].remove(*iter);

    // find the neighbors of the image point with the status SearchFor 
    GetNeighbors(*iter, neigh, SearchFor);

    // change the status of the image point and add it to the new layer   
    Status[*iter] = ChangeTo;
	Layers[ChangeTo].push_back(*iter);

    // add the neighbors of the image point to the output list
    for (size_t i = 0; i < neigh.size(); i++)
    {
      // mark the image point so we don't add it twice.
      Status[neigh[i]] = StatusChange;
      output.push_back(neigh[i]); 
    }
  }

  input.clear();
}

template <class T> void SparseFieldRoutine<T>::UpdateBorderLayer(std::list<size_t> &input, size_t ChangeTo)
{
  std::list<size_t>::iterator iter;

  for (iter = input.begin(); iter != input.end(); iter++)
  {
    Status[*iter] = ChangeTo;
	Layers[ChangeTo].push_back(*iter);
  }

  input.clear();
}

template <class T> void SparseFieldRoutine<T>::UpdateImplicitFunction(size_t from, size_t to, size_t promote)
{
  std::vector<size_t> neigh;
  size_t del;
  float tmp;
  
  // propagate the values inward or outward?
  int sign = to % 2 ? -1 : 1;
  
  std::list<size_t>::iterator iter = Layers[to].begin();
  while (iter != Layers[to].end())
  {
    GetNeighbors(*iter, neigh, from);

	tmp = sign * UNDEFINED;
	for (size_t i = 0; i < neigh.size(); i++)
    {
      // find the closest neighbor of the image point and save its value of the ImplicitFunction
	  tmp = sign > 0 ? MIN(tmp, ImplicitFunction[neigh[i]]) : MAX(tmp, ImplicitFunction[neigh[i]]);
	}
		
    if (tmp != sign * UNDEFINED)
    {
      // we found some neighbor of the image point
      ImplicitFunction[*iter] = tmp + sign * spacing;
      ++iter;
    }
    else
    {
      // we didn't find any neighbor so the image point is promoted
      if (promote == Layers.size())
      {
        Status[*iter] = StatusNull;
		ImplicitFunction[*iter]  = tmp;
      }
      else
      {
        Layers[promote].push_back(*iter);
        Status[*iter] = promote;
		// its value of the ImplicitFunction will be updated later
      }

	  // remove the image point from the old layer
	  del = *iter;
	  iter++;
	  Layers[to].remove(del);
    }
  }
}

template <class T> void SparseFieldRoutine<T>::ComputeSegmentatedImage(i3d::Image3d<T> &output, SegmentationType type)
{ 
  if (type)
  {
    for (size_t i = 0; i < ImplicitFunction.size(); i++)
      output.SetVoxel(i,  ImplicitFunction[i] > 0 ? std::numeric_limits<T>::min() : std::numeric_limits<T>::max());
  }
  else
  {
     for (size_t i = 0; i < ImplicitFunction.size(); i++)
       output.SetVoxel(i,  ImplicitFunction[i] > 0 ? std::numeric_limits<T>::max() : std::numeric_limits<T>::min());
  }
}

template <class T> void SparseFieldRoutine<T>::Init(std::vector<size_t> &Seeds)
{
  std::vector<size_t> neigh;
  std::list<size_t>::iterator iter;

  size_t size = ImgSize.x * ImgSize.y * ImgSize.z;
  for(size_t i = 0; i < Seeds.size(); i++)
  {
    if (Seeds[i] < size)
	{
      if (Status[Seeds[i]] == StatusNull)
	  {
        // this starting point is used for the first time
        ImplicitFunction[Seeds[i]] = -spacing;
	    Status[Seeds[i]] = 1;
	    Layers[1].push_back(Seeds[i]);
	  }
	}
	else
	{
      #ifdef I3D_DEBUG
        cout << "A seed point " << Seeds[i] << " was ignored. It isn't a point of processed image!" << endl;
      #endif
	}
  }

  for (iter = Layers[1].begin(); iter != Layers[1].end(); iter++)
  {
    GetNeighbors(*iter, neigh, StatusNull);
	for (size_t i = 0; i < neigh.size(); i++)
	{
      ImplicitFunction[neigh[i]] = 0;
	  Status[neigh[i]] = 0;
	  Layers[0].push_back(neigh[i]);
	}
  }

  for (size_t i = 0; i < 2; i++)
    for (iter = Layers[2 * i].begin(); iter != Layers[2 * i].end(); iter++)
    {
	  GetNeighbors(*iter, neigh, StatusNull);
	  for (size_t j = 0; j < neigh.size(); j++)
	  {
        ImplicitFunction[neigh[j]] = (i + 1) * spacing;
	    Status[neigh[j]] = 2 * i + 2;
	    Layers[2 * i + 2].push_back(neigh[j]);
	  }
    }
}

/////////////////////////////////////////////////////////////////
//                                                             //
//                 Class SparseFieldRoutine2D                  //
//                                                             //
/////////////////////////////////////////////////////////////////

template <class T> 
  SparseFieldRoutine2D<T>::SparseFieldRoutine2D(std::valarray<float> *ImageGrad, std::vector<size_t> &Seeds, 
												i3d::Vector3d<size_t> ImgSize, float spacing, float alpha, float beta, int p): 
  SparseFieldRoutine<T>(ImageGrad, Seeds, ImgSize, spacing, alpha, beta, p)
{ 
  Init(Seeds);
}

template <class T> float SparseFieldRoutine2D<T>::ComputeSpeedFunction(size_t i)
{ 
  int x, y;
  GETPOSITION_2D(x, y);
 
  float Dx = 0, Dy = 0, IGx = 0, IGy = 0;
  float bDx = 0.0f, fDx = 0.0f, bDy = 0.0f, fDy = 0.0f;
  float Dxx = -2 * ImplicitFunction[i], Dyy = -2 * ImplicitFunction[i], Dxy = 0;

  size_t point = (x > 0) * 1 + (x < ((int)ImgSize.x - 1)) * 10 + (y > 0) * 100 + (y < ((int)ImgSize.y - 1))* 1000;
  switch(point)
  {
    case 1111:
      Dx = CENTRAL(ImplicitFunction[i - 1], ImplicitFunction[i + 1], spacing);
	  Dy = CENTRAL(ImplicitFunction[i - ImgSize.x], ImplicitFunction[i + ImgSize.x], spacing);
	  IGx = CENTRAL(ImageGradient[i - 1], ImageGradient[i + 1], spacing);
	  IGy = CENTRAL(ImageGradient[i - ImgSize.x], ImageGradient[i + ImgSize.x], spacing);
	  bDx = SIDE(ImplicitFunction[i - 1], ImplicitFunction[i], spacing);
	  fDx = SIDE(ImplicitFunction[i], ImplicitFunction[i + 1], spacing);
	  bDy = SIDE(ImplicitFunction[i - ImgSize.x], ImplicitFunction[i], spacing);
	  fDy = SIDE(ImplicitFunction[i], ImplicitFunction[i + ImgSize.x], spacing);
	  Dxx += ImplicitFunction[i + 1] + ImplicitFunction[i - 1];
	  Dyy += ImplicitFunction[i + ImgSize.x] + ImplicitFunction[i - ImgSize.x];
	  Dxy = (ImplicitFunction[i + ImgSize.x + 1] - ImplicitFunction[i - ImgSize.x + 1] - 
	         ImplicitFunction[i + ImgSize.x - 1] + ImplicitFunction[i - ImgSize.x - 1]);
      break;

 	case 1011:
      Dx =  CENTRAL(ImplicitFunction[i - 1], ImplicitFunction[i + 1], spacing);
	  IGx = CENTRAL(ImageGradient[i - 1], ImageGradient[i + 1], spacing);
	  bDx = SIDE(ImplicitFunction[i - 1], ImplicitFunction[i], spacing);
	  fDx = SIDE(ImplicitFunction[i], ImplicitFunction[i + 1], spacing);
	  fDy = SIDE(ImplicitFunction[i], ImplicitFunction[i + ImgSize.x], spacing);
	  bDy = -fDy;
	  Dxx += ImplicitFunction[i + 1] + ImplicitFunction[i - 1];
	  Dyy += 2 * ImplicitFunction[i + ImgSize.x];
      break;

	case 111:
      Dx = CENTRAL(ImplicitFunction[i - 1], ImplicitFunction[i + 1], spacing);
	  IGx = CENTRAL(ImageGradient[i - 1], ImageGradient[i + 1], spacing);
	  bDx = SIDE(ImplicitFunction[i - 1], ImplicitFunction[i], spacing);
	  fDx = SIDE(ImplicitFunction[i], ImplicitFunction[i + 1], spacing);
	  bDy = SIDE(ImplicitFunction[i - ImgSize.x], ImplicitFunction[i], spacing);
	  fDy = -bDy;
	  Dxx += ImplicitFunction[i + 1] + ImplicitFunction[i - 1];
	  Dyy += 2 * ImplicitFunction[i - ImgSize.x];
      break;

	case 1110:
	  Dy = CENTRAL(ImplicitFunction[i - ImgSize.x], ImplicitFunction[i + ImgSize.x], spacing);
	  IGy = CENTRAL(ImageGradient[i - ImgSize.x], ImageGradient[i + ImgSize.x], spacing);
	  fDx = SIDE(ImplicitFunction[i], ImplicitFunction[i + 1], spacing);
	  bDx = -fDx;
	  fDx = SIDE(ImplicitFunction[i], ImplicitFunction[i + 1], spacing);
	  bDy = SIDE(ImplicitFunction[i - ImgSize.x], ImplicitFunction[i], spacing);
	  fDy = SIDE(ImplicitFunction[i], ImplicitFunction[i + ImgSize.x], spacing);
	  Dxx += 2 * ImplicitFunction[i + 1];
	  Dyy += ImplicitFunction[i + ImgSize.x] + ImplicitFunction[i - ImgSize.x];
      break;

	case 1101:
	  Dy = CENTRAL(ImplicitFunction[i - ImgSize.x], ImplicitFunction[i + ImgSize.x], spacing);
	  IGy = CENTRAL(ImageGradient[i - ImgSize.x], ImageGradient[i + ImgSize.x], spacing);
	  bDx = SIDE(ImplicitFunction[i - 1], ImplicitFunction[i], spacing);
	  fDx = -bDx;
	  bDy = SIDE(ImplicitFunction[i - ImgSize.x], ImplicitFunction[i], spacing);
	  fDy = SIDE(ImplicitFunction[i], ImplicitFunction[i + ImgSize.x], spacing);
	  Dxx += 2 * ImplicitFunction[i - 1];
	  Dyy += ImplicitFunction[i + ImgSize.x] + ImplicitFunction[i - ImgSize.x];
      break;

	case 1010:
	  fDx = SIDE(ImplicitFunction[i], ImplicitFunction[i + 1], spacing);
	  bDx = -fDx;
	  fDy = SIDE(ImplicitFunction[i], ImplicitFunction[i + ImgSize.x], spacing);
	  bDy = -fDy;
	  Dxx += 2 * ImplicitFunction[i + 1];
	  Dyy += 2 * ImplicitFunction[i + ImgSize.x];
	  break;

	case 1001:
	  bDx = SIDE(ImplicitFunction[i - 1], ImplicitFunction[i], spacing);
	  fDx = -bDx;
	  fDy = SIDE(ImplicitFunction[i], ImplicitFunction[i + ImgSize.x], spacing);
	  bDy = -fDy;
	  Dxx += 2 * ImplicitFunction[i - 1];
	  Dyy += 2 * ImplicitFunction[i + ImgSize.x];
      break;

	case 110:
      fDx = SIDE(ImplicitFunction[i], ImplicitFunction[i + 1], spacing);
	  bDx = -fDx;
	  bDy = SIDE(ImplicitFunction[i - ImgSize.x], ImplicitFunction[i], spacing);
	  fDy = -bDy;
	  Dxx += 2 * ImplicitFunction[i + 1];
	  Dyy += 2 * ImplicitFunction[i - ImgSize.x];
      break;

	case 101:
	  bDx = SIDE(ImplicitFunction[i - 1], ImplicitFunction[i], spacing);
	  fDx = -bDx;
	  bDy = SIDE(ImplicitFunction[i - ImgSize.x], ImplicitFunction[i], spacing);
	  fDy = -bDy;
	  Dxx += 2 * ImplicitFunction[i - 1];
	  Dyy += 2 * ImplicitFunction[i - ImgSize.x];
      break; 
  }
 
  float val = MAX(SQR(Dx) + SQR(Dy), EPSILON);
  float cur = (SQR(Dx) * Dyy - 0.5 * Dx * Dy * Dxy + SQR(Dy) * Dxx) / (val * SQR(spacing));

  if (cur > CurvatureLimit)
	cur = CurvatureLimit;
  else
    if (cur < -CurvatureLimit)
      cur = -CurvatureLimit;

  float adv = MAX(IGx, 0) * bDx + MIN(IGx, 0) * fDx + MAX(IGy, 0) * bDy + MIN(IGy, 0) * fDy;

  float Godunov = sqrt(MAX(SQR(MAX(bDx, 0)), SQR(MIN(fDx, 0))) + MAX(SQR(MAX(bDy, 0)), SQR(MIN(fDy, 0))));

  float grad;
  if (p == 2)
    grad = 1 / SQR(1 + ImageGradient[i]);
  else 
    grad = 1 / (1 + ImageGradient[i]);


  MaxProp = MAX(MaxProp, 2 * Godunov * grad);
  MaxAdv = MAX(MaxAdv, std::abs(IGx) + std::abs(IGy));

  return grad * (Godunov - alpha * cur) + beta * adv;
}

template <class T> void SparseFieldRoutine2D<T>::GetNeighbors(size_t i, std::vector<size_t> &neigh, int layer)
{
  int x, y;
  GETPOSITION_2D(x, y);

  neigh.clear();

  if (x > 0 && Status[i - 1] == layer)
    neigh.push_back(i - 1);

  if (x < (int)ImgSize.x - 1  && Status[i + 1] == layer)
    neigh.push_back(i + 1);

  if (y > 0 && Status[i - ImgSize.x] == layer)
    neigh.push_back(i - ImgSize.x);

  if (y < (int)ImgSize.y - 1 && Status[i + ImgSize.x] == layer)
    neigh.push_back(i + ImgSize.x);
}

/////////////////////////////////////////////////////////////////
//                                                             //
//                 Class SparseFieldRoutine3D                  //
//                                                             //
/////////////////////////////////////////////////////////////////

template <class T> 
  SparseFieldRoutine3D<T>::SparseFieldRoutine3D(std::valarray<float> *ImageGrad, std::vector<size_t> &Seeds, 
   									            i3d::Vector3d<size_t> ImgSize, float spacing, float alpha, float beta, int p):
  SparseFieldRoutine<T>(ImageGrad, Seeds, ImgSize, spacing, alpha, beta, p)
{
  Init(Seeds);
}

template <class T> float SparseFieldRoutine3D<T>::ComputeSpeedFunction(size_t i)
{ 
  int x, y, z;
  GETPOSITION_3D(x, y, z);
 
  float Dx = 0, Dy = 0, Dz = 0, IGx = 0, IGy = 0, IGz = 0;
  float bDx = 0.0f, fDx = 0.0f, bDy = 0.0f, fDy = 0.0f, bDz = 0.0f, fDz = 0.0f;
  float Dxx = -2 * ImplicitFunction[i], Dyy = -2 * ImplicitFunction[i], Dzz = -2 * ImplicitFunction[i];
  float Dxy = 0, Dxz = 0, Dyz = 0;

  size_t point = (x > 0) * 1 + (x < ((int)ImgSize.x - 1)) * 10 + 
	  				  (y > 0) * 100 + (y < ((int)ImgSize.y - 1))* 1000 +
	              (z > 0) * 10000 + (z < ((int)ImgSize.z - 1)) * 100000;

  switch(point)
  {
    case 111111:
      Dx = CENTRAL(ImplicitFunction[i - 1], ImplicitFunction[i + 1], spacing);
	  Dy = CENTRAL(ImplicitFunction[i - ImgSize.x], ImplicitFunction[i + ImgSize.x], spacing);
	  Dz = CENTRAL(ImplicitFunction[i - ImgSize.x * ImgSize.y], ImplicitFunction[i + ImgSize.x * ImgSize.y], spacing);
	  IGx = CENTRAL(ImageGradient[i - 1], ImageGradient[i + 1], spacing);
	  IGy = CENTRAL(ImageGradient[i - ImgSize.x], ImageGradient[i + ImgSize.x], spacing);
	  IGz = CENTRAL(ImageGradient[i - ImgSize.x * ImgSize.y], ImageGradient[i + ImgSize.x * ImgSize.y], spacing);
	  bDx = SIDE(ImplicitFunction[i - 1], ImplicitFunction[i], spacing);
	  fDx = SIDE(ImplicitFunction[i], ImplicitFunction[i + 1], spacing);
	  bDy = SIDE(ImplicitFunction[i - ImgSize.x], ImplicitFunction[i], spacing);
	  fDy = SIDE(ImplicitFunction[i], ImplicitFunction[i + ImgSize.x], spacing);
	  bDz = SIDE(ImplicitFunction[i - ImgSize.x * ImgSize.y], ImplicitFunction[i], spacing);
      fDz = SIDE(ImplicitFunction[i], ImplicitFunction[i + ImgSize.x * ImgSize.y], spacing);
	  Dxx += ImplicitFunction[i + 1] + ImplicitFunction[i - 1];
	  Dyy += ImplicitFunction[i + ImgSize.x] + ImplicitFunction[i - ImgSize.x];
	  Dzz += ImplicitFunction[i + ImgSize.x * ImgSize.y] + ImplicitFunction[i - ImgSize.x * ImgSize.y];	
	  Dxy = (ImplicitFunction[i + ImgSize.x + 1] - ImplicitFunction[i - ImgSize.x + 1] - 
	         ImplicitFunction[i + ImgSize.x - 1] + ImplicitFunction[i - ImgSize.x - 1]);
	  Dxz = (ImplicitFunction[i + ImgSize.x * ImgSize.y + 1] - ImplicitFunction[i - ImgSize.x * ImgSize.y + 1] - 
	         ImplicitFunction[i + ImgSize.x * ImgSize.y - 1] + ImplicitFunction[i - ImgSize.x * ImgSize.y - 1]);
	  Dyz = (ImplicitFunction[i + ImgSize.x * ImgSize.y + ImgSize.x] - 
		     ImplicitFunction[i - ImgSize.x * ImgSize.y + ImgSize.x] - 
			 ImplicitFunction[i + ImgSize.x * ImgSize.y - ImgSize.x] + 
			 ImplicitFunction[i - ImgSize.x * ImgSize.y - ImgSize.x]);
      break;
   
	case 111110:
	  Dy = CENTRAL(ImplicitFunction[i - ImgSize.x], ImplicitFunction[i + ImgSize.x], spacing);
	  Dz = CENTRAL(ImplicitFunction[i - ImgSize.x * ImgSize.y], ImplicitFunction[i + ImgSize.x * ImgSize.y], spacing);
	  IGy = CENTRAL(ImageGradient[i - ImgSize.x], ImageGradient[i + ImgSize.x], spacing);
	  IGz = CENTRAL(ImageGradient[i - ImgSize.x * ImgSize.y], ImageGradient[i + ImgSize.x * ImgSize.y], spacing);
	  fDx = SIDE(ImplicitFunction[i], ImplicitFunction[i + 1], spacing);
	  bDx = -fDx;
	  bDy = SIDE(ImplicitFunction[i - ImgSize.x], ImplicitFunction[i], spacing);
	  fDy = SIDE(ImplicitFunction[i], ImplicitFunction[i + ImgSize.x], spacing);
	  bDz = SIDE(ImplicitFunction[i - ImgSize.x * ImgSize.y], ImplicitFunction[i], spacing);
      fDz = SIDE(ImplicitFunction[i], ImplicitFunction[i + ImgSize.x * ImgSize.y], spacing);
	  Dxx += 2 * ImplicitFunction[i + 1];
	  Dyy += ImplicitFunction[i + ImgSize.x] + ImplicitFunction[i - ImgSize.x];
	  Dzz += ImplicitFunction[i + ImgSize.x * ImgSize.y] + ImplicitFunction[i - ImgSize.x * ImgSize.y];	
	  Dyz = (ImplicitFunction[i + ImgSize.x * ImgSize.y + ImgSize.x] - 
		     ImplicitFunction[i - ImgSize.x * ImgSize.y + ImgSize.x] - 
			 ImplicitFunction[i + ImgSize.x * ImgSize.y - ImgSize.x] + 
			 ImplicitFunction[i - ImgSize.x * ImgSize.y - ImgSize.x]);
      break;

	case 111101:
	  Dy = CENTRAL(ImplicitFunction[i - ImgSize.x], ImplicitFunction[i + ImgSize.x], spacing);
	  Dz = CENTRAL(ImplicitFunction[i - ImgSize.x * ImgSize.y], ImplicitFunction[i + ImgSize.x * ImgSize.y], spacing);
	  IGy = CENTRAL(ImageGradient[i - ImgSize.x], ImageGradient[i + ImgSize.x], spacing);
	  IGz = CENTRAL(ImageGradient[i - ImgSize.x * ImgSize.y], ImageGradient[i + ImgSize.x * ImgSize.y], spacing);
	  bDx = SIDE(ImplicitFunction[i - 1], ImplicitFunction[i], spacing);
	  fDx = -bDx;
	  bDy = SIDE(ImplicitFunction[i - ImgSize.x], ImplicitFunction[i], spacing);
	  fDy = SIDE(ImplicitFunction[i], ImplicitFunction[i + ImgSize.x], spacing);
	  bDz = SIDE(ImplicitFunction[i - ImgSize.x * ImgSize.y], ImplicitFunction[i], spacing);
      fDz = SIDE(ImplicitFunction[i], ImplicitFunction[i + ImgSize.x * ImgSize.y], spacing);
	  Dxx += 2 * ImplicitFunction[i - 1];
	  Dyy += ImplicitFunction[i + ImgSize.x] + ImplicitFunction[i - ImgSize.x];
	  Dzz += ImplicitFunction[i + ImgSize.x * ImgSize.y] + ImplicitFunction[i - ImgSize.x * ImgSize.y];	
	  Dyz = (ImplicitFunction[i + ImgSize.x * ImgSize.y + ImgSize.x] - 
		     ImplicitFunction[i - ImgSize.x * ImgSize.y + ImgSize.x] - 
			 ImplicitFunction[i + ImgSize.x * ImgSize.y - ImgSize.x] + 
			 ImplicitFunction[i - ImgSize.x * ImgSize.y - ImgSize.x]);
      break;


	case 111011:
      Dx = CENTRAL(ImplicitFunction[i - 1], ImplicitFunction[i + 1], spacing);
	  Dz = CENTRAL(ImplicitFunction[i - ImgSize.x * ImgSize.y], ImplicitFunction[i + ImgSize.x * ImgSize.y], spacing);
	  IGx = CENTRAL(ImageGradient[i - 1], ImageGradient[i + 1], spacing);
	  IGz = CENTRAL(ImageGradient[i - ImgSize.x * ImgSize.y], ImageGradient[i + ImgSize.x * ImgSize.y], spacing);
	  bDx = SIDE(ImplicitFunction[i - 1], ImplicitFunction[i], spacing);
	  fDx = SIDE(ImplicitFunction[i], ImplicitFunction[i + 1], spacing);
	  fDy = SIDE(ImplicitFunction[i], ImplicitFunction[i + ImgSize.x], spacing);
	  bDy = -fDy;
	  bDz = SIDE(ImplicitFunction[i - ImgSize.x * ImgSize.y], ImplicitFunction[i], spacing);
      fDz = SIDE(ImplicitFunction[i], ImplicitFunction[i + ImgSize.x * ImgSize.y], spacing);
	  Dxx += ImplicitFunction[i + 1] + ImplicitFunction[i - 1];
	  Dyy += 2 * ImplicitFunction[i + ImgSize.x];
	  Dzz += ImplicitFunction[i + ImgSize.x * ImgSize.y] + ImplicitFunction[i - ImgSize.x * ImgSize.y];	
	  Dxz = (ImplicitFunction[i + ImgSize.x * ImgSize.y + 1] - ImplicitFunction[i - ImgSize.x * ImgSize.y + 1] - 
	         ImplicitFunction[i + ImgSize.x * ImgSize.y - 1] + ImplicitFunction[i - ImgSize.x * ImgSize.y - 1]);
      break;

	case 110111:
      Dx = CENTRAL(ImplicitFunction[i - 1], ImplicitFunction[i + 1], spacing);
	  Dz = CENTRAL(ImplicitFunction[i - ImgSize.x * ImgSize.y], ImplicitFunction[i + ImgSize.x * ImgSize.y], spacing);
	  IGx = CENTRAL(ImageGradient[i - 1], ImageGradient[i + 1], spacing);
	  IGz = CENTRAL(ImageGradient[i - ImgSize.x * ImgSize.y], ImageGradient[i + ImgSize.x * ImgSize.y], spacing);
	  bDx = SIDE(ImplicitFunction[i - 1], ImplicitFunction[i], spacing);
	  fDx = SIDE(ImplicitFunction[i], ImplicitFunction[i + 1], spacing);
	  bDy = SIDE(ImplicitFunction[i - ImgSize.x], ImplicitFunction[i], spacing);
	  fDy = -bDy;
	  bDz = SIDE(ImplicitFunction[i - ImgSize.x * ImgSize.y], ImplicitFunction[i], spacing);
      fDz = SIDE(ImplicitFunction[i], ImplicitFunction[i + ImgSize.x * ImgSize.y], spacing);
	  Dxx += ImplicitFunction[i + 1] + ImplicitFunction[i - 1];
	  Dyy += 2 * ImplicitFunction[i - ImgSize.x];
	  Dzz += ImplicitFunction[i + ImgSize.x * ImgSize.y] + ImplicitFunction[i - ImgSize.x * ImgSize.y];	
	  Dxz = (ImplicitFunction[i + ImgSize.x * ImgSize.y + 1] - ImplicitFunction[i - ImgSize.x * ImgSize.y + 1] - 
	         ImplicitFunction[i + ImgSize.x * ImgSize.y - 1] + ImplicitFunction[i - ImgSize.x * ImgSize.y - 1]);
      break;

	case 101111:
      Dx = CENTRAL(ImplicitFunction[i - 1], ImplicitFunction[i + 1], spacing);
	  Dy = CENTRAL(ImplicitFunction[i - ImgSize.x], ImplicitFunction[i + ImgSize.x], spacing);
	  IGx = CENTRAL(ImageGradient[i - 1], ImageGradient[i + 1], spacing);
	  IGy = CENTRAL(ImageGradient[i - ImgSize.x], ImageGradient[i + ImgSize.x], spacing);
	  bDx = SIDE(ImplicitFunction[i - 1], ImplicitFunction[i], spacing);
	  fDx = SIDE(ImplicitFunction[i], ImplicitFunction[i + 1], spacing);
	  bDy = SIDE(ImplicitFunction[i - ImgSize.x], ImplicitFunction[i], spacing);
	  fDy = SIDE(ImplicitFunction[i], ImplicitFunction[i + ImgSize.x], spacing);
	  fDz = SIDE(ImplicitFunction[i], ImplicitFunction[i + ImgSize.x * ImgSize.y], spacing);
	  bDz = -fDz;
	  Dxx += ImplicitFunction[i + 1] + ImplicitFunction[i - 1];
	  Dyy += ImplicitFunction[i + ImgSize.x] + ImplicitFunction[i - ImgSize.x];
	  Dzz += 2 * ImplicitFunction[i + ImgSize.x * ImgSize.y];	
	  Dxy = (ImplicitFunction[i + ImgSize.x + 1] - ImplicitFunction[i - ImgSize.x + 1] - 
	         ImplicitFunction[i + ImgSize.x - 1] + ImplicitFunction[i - ImgSize.x - 1]);
      break;

	case 11111:
      Dx = CENTRAL(ImplicitFunction[i - 1], ImplicitFunction[i + 1], spacing);
	  Dy = CENTRAL(ImplicitFunction[i - ImgSize.x], ImplicitFunction[i + ImgSize.x], spacing);
	  IGx = CENTRAL(ImageGradient[i - 1], ImageGradient[i + 1], spacing);
	  IGy = CENTRAL(ImageGradient[i - ImgSize.x], ImageGradient[i + ImgSize.x], spacing);
	  bDx = SIDE(ImplicitFunction[i - 1], ImplicitFunction[i], spacing);
	  fDx = SIDE(ImplicitFunction[i], ImplicitFunction[i + 1], spacing);
	  bDy = SIDE(ImplicitFunction[i - ImgSize.x], ImplicitFunction[i], spacing);
	  fDy = SIDE(ImplicitFunction[i], ImplicitFunction[i + ImgSize.x], spacing);
	  bDz = SIDE(ImplicitFunction[i - ImgSize.x * ImgSize.y], ImplicitFunction[i], spacing);
      fDz = -bDz;
	  Dxx += ImplicitFunction[i + 1] + ImplicitFunction[i - 1];
	  Dyy += ImplicitFunction[i + ImgSize.x] + ImplicitFunction[i - ImgSize.x];
	  Dzz += 2 * ImplicitFunction[i - ImgSize.x * ImgSize.y];	
	  Dxy = (ImplicitFunction[i + ImgSize.x + 1] - ImplicitFunction[i - ImgSize.x + 1] - 
	         ImplicitFunction[i + ImgSize.x - 1] + ImplicitFunction[i - ImgSize.x - 1]);
      break;

	case 111010:
	  Dz = CENTRAL(ImplicitFunction[i - ImgSize.x * ImgSize.y], ImplicitFunction[i + ImgSize.x * ImgSize.y], spacing);
	  IGz = CENTRAL(ImageGradient[i - ImgSize.x * ImgSize.y], ImageGradient[i + ImgSize.x * ImgSize.y], spacing);
	  fDx = SIDE(ImplicitFunction[i], ImplicitFunction[i + 1], spacing);
	  bDx = -fDx;
	  fDy = SIDE(ImplicitFunction[i], ImplicitFunction[i + ImgSize.x], spacing);
	  bDy = -fDy;
	  bDz = SIDE(ImplicitFunction[i - ImgSize.x * ImgSize.y], ImplicitFunction[i], spacing);
      fDz = SIDE(ImplicitFunction[i], ImplicitFunction[i + ImgSize.x * ImgSize.y], spacing);
	  Dxx += 2 * ImplicitFunction[i + 1];
	  Dyy += 2 * ImplicitFunction[i + ImgSize.x];
	  Dzz += ImplicitFunction[i + ImgSize.x * ImgSize.y] + ImplicitFunction[i - ImgSize.x * ImgSize.y];	
      break;

    case 111001:
	  Dz = CENTRAL(ImplicitFunction[i - ImgSize.x * ImgSize.y], ImplicitFunction[i + ImgSize.x * ImgSize.y], spacing);
	  IGz = CENTRAL(ImageGradient[i - ImgSize.x * ImgSize.y], ImageGradient[i + ImgSize.x * ImgSize.y], spacing);
	  bDx = SIDE(ImplicitFunction[i - 1], ImplicitFunction[i], spacing);
	  fDx = -bDx;
	  fDy = SIDE(ImplicitFunction[i], ImplicitFunction[i + ImgSize.x], spacing);
	  bDy = -fDy;
	  bDz = SIDE(ImplicitFunction[i - ImgSize.x * ImgSize.y], ImplicitFunction[i], spacing);
      fDz = SIDE(ImplicitFunction[i], ImplicitFunction[i + ImgSize.x * ImgSize.y], spacing);
	  Dxx += 2 * ImplicitFunction[i - 1];
	  Dyy += 2 * ImplicitFunction[i + ImgSize.x];
	  Dzz += ImplicitFunction[i + ImgSize.x * ImgSize.y] + ImplicitFunction[i - ImgSize.x * ImgSize.y];	
      break;

	case 110110:
	  Dz = CENTRAL(ImplicitFunction[i - ImgSize.x * ImgSize.y], ImplicitFunction[i + ImgSize.x * ImgSize.y], spacing);
	  IGz = CENTRAL(ImageGradient[i - ImgSize.x * ImgSize.y], ImageGradient[i + ImgSize.x * ImgSize.y], spacing);
	  fDx = SIDE(ImplicitFunction[i], ImplicitFunction[i + 1], spacing);
	  bDx = -fDx;
	  bDy = SIDE(ImplicitFunction[i - ImgSize.x], ImplicitFunction[i], spacing);
	  fDy = -bDy;
	  bDz = SIDE(ImplicitFunction[i - ImgSize.x * ImgSize.y], ImplicitFunction[i], spacing);
      fDz = SIDE(ImplicitFunction[i], ImplicitFunction[i + ImgSize.x * ImgSize.y], spacing);
	  Dxx += 2 * ImplicitFunction[i + 1];
	  Dyy += 2 * ImplicitFunction[i - ImgSize.x];
	  Dzz += ImplicitFunction[i + ImgSize.x * ImgSize.y] + ImplicitFunction[i - ImgSize.x * ImgSize.y];	
      break;

	case 110101:
	  Dz = CENTRAL(ImplicitFunction[i - ImgSize.x * ImgSize.y], ImplicitFunction[i + ImgSize.x * ImgSize.y], spacing);
	  IGz = CENTRAL(ImageGradient[i - ImgSize.x * ImgSize.y], ImageGradient[i + ImgSize.x * ImgSize.y], spacing);
	  bDx = SIDE(ImplicitFunction[i - 1], ImplicitFunction[i], spacing);
	  fDx = -bDx;
	  bDy = SIDE(ImplicitFunction[i - ImgSize.x], ImplicitFunction[i], spacing);
	  fDy = -bDy;
	  bDz = SIDE(ImplicitFunction[i - ImgSize.x * ImgSize.y], ImplicitFunction[i], spacing);
      fDz = SIDE(ImplicitFunction[i], ImplicitFunction[i + ImgSize.x * ImgSize.y], spacing);
	  Dxx += 2 * ImplicitFunction[i - 1];
	  Dyy += 2 * ImplicitFunction[i - ImgSize.x];
	  Dzz += ImplicitFunction[i + ImgSize.x * ImgSize.y] + ImplicitFunction[i - ImgSize.x * ImgSize.y];	
      break;

	case 101110:
	  Dy = CENTRAL(ImplicitFunction[i - ImgSize.x], ImplicitFunction[i + ImgSize.x], spacing);
	  IGy = CENTRAL(ImageGradient[i - ImgSize.x], ImageGradient[i + ImgSize.x], spacing);
	  fDx = SIDE(ImplicitFunction[i], ImplicitFunction[i + 1], spacing);
	  bDx = -fDx;
	  bDy = SIDE(ImplicitFunction[i - ImgSize.x], ImplicitFunction[i], spacing);
	  fDy = SIDE(ImplicitFunction[i], ImplicitFunction[i + ImgSize.x], spacing);
	  fDz = SIDE(ImplicitFunction[i], ImplicitFunction[i + ImgSize.x * ImgSize.y], spacing);
	  bDz = -fDz;
      Dxx += 2 * ImplicitFunction[i + 1];
	  Dyy += ImplicitFunction[i + ImgSize.x] + ImplicitFunction[i - ImgSize.x];
	  Dzz += 2 * ImplicitFunction[i + ImgSize.x * ImgSize.y];	
      break;

	case 101101:
	  Dy = CENTRAL(ImplicitFunction[i - ImgSize.x], ImplicitFunction[i + ImgSize.x], spacing);
	  IGy = CENTRAL(ImageGradient[i - ImgSize.x], ImageGradient[i + ImgSize.x], spacing);
	  bDx = SIDE(ImplicitFunction[i - 1], ImplicitFunction[i], spacing);
	  fDx = -bDx;
	  bDy = SIDE(ImplicitFunction[i - ImgSize.x], ImplicitFunction[i], spacing);
	  fDy = SIDE(ImplicitFunction[i], ImplicitFunction[i + ImgSize.x], spacing);
	  fDz = SIDE(ImplicitFunction[i], ImplicitFunction[i + ImgSize.x * ImgSize.y], spacing);
	  bDz = -fDz;
	  Dxx += 2 * ImplicitFunction[i - 1];
	  Dyy += ImplicitFunction[i + ImgSize.x] + ImplicitFunction[i - ImgSize.x];
	  Dzz += 2 * ImplicitFunction[i + ImgSize.x * ImgSize.y];	
      break; 

	case  11110:
	  Dy = CENTRAL(ImplicitFunction[i - ImgSize.x], ImplicitFunction[i + ImgSize.x], spacing);
	  IGy = CENTRAL(ImageGradient[i - ImgSize.x], ImageGradient[i + ImgSize.x], spacing);
	  fDx = SIDE(ImplicitFunction[i], ImplicitFunction[i + 1], spacing);
	  bDx = -fDx;
	  bDy = SIDE(ImplicitFunction[i - ImgSize.x], ImplicitFunction[i], spacing);
	  fDy = SIDE(ImplicitFunction[i], ImplicitFunction[i + ImgSize.x], spacing);
	  bDz = SIDE(ImplicitFunction[i - ImgSize.x * ImgSize.y], ImplicitFunction[i], spacing);
      fDz = -bDz;
	  Dxx += 2 * ImplicitFunction[i + 1];
	  Dyy += ImplicitFunction[i + ImgSize.x] + ImplicitFunction[i - ImgSize.x];
	  Dzz += 2 * ImplicitFunction[i - ImgSize.x * ImgSize.y];	
      break;

	case  11101:
	  Dy = CENTRAL(ImplicitFunction[i - ImgSize.x], ImplicitFunction[i + ImgSize.x], spacing);
	  IGy = CENTRAL(ImageGradient[i - ImgSize.x], ImageGradient[i + ImgSize.x], spacing);
	  bDx = SIDE(ImplicitFunction[i - 1], ImplicitFunction[i], spacing);
	  fDx = -bDx;
	  bDy = SIDE(ImplicitFunction[i - ImgSize.x], ImplicitFunction[i], spacing);
	  fDy = SIDE(ImplicitFunction[i], ImplicitFunction[i + ImgSize.x], spacing);
	  bDz = SIDE(ImplicitFunction[i - ImgSize.x * ImgSize.y], ImplicitFunction[i], spacing);
      fDz = -bDz;
	  Dxx += 2 * ImplicitFunction[i - 1];
	  Dyy += ImplicitFunction[i + ImgSize.x] + ImplicitFunction[i - ImgSize.x];
	  Dzz += 2 * ImplicitFunction[i - ImgSize.x * ImgSize.y];	
      break;

	case 101011:
      Dx = CENTRAL(ImplicitFunction[i - 1], ImplicitFunction[i + 1], spacing);
	  IGx = CENTRAL(ImageGradient[i - 1], ImageGradient[i + 1], spacing);
	  bDx = SIDE(ImplicitFunction[i - 1], ImplicitFunction[i], spacing);
	  fDx = SIDE(ImplicitFunction[i], ImplicitFunction[i + 1], spacing);
	  fDy = SIDE(ImplicitFunction[i], ImplicitFunction[i + ImgSize.x], spacing);
	  bDy = -fDy;
	  fDz = SIDE(ImplicitFunction[i], ImplicitFunction[i + ImgSize.x * ImgSize.y], spacing);
	  bDz = -fDz;
      Dxx += ImplicitFunction[i + 1] + ImplicitFunction[i - 1];
	  Dyy += 2 * ImplicitFunction[i + ImgSize.x];
	  Dzz += 2 * ImplicitFunction[i + ImgSize.x * ImgSize.y];	
      break;

	case 100111:
      Dx = CENTRAL(ImplicitFunction[i - 1], ImplicitFunction[i + 1], spacing);
	  IGx = CENTRAL(ImageGradient[i - 1], ImageGradient[i + 1], spacing);
	  bDx = SIDE(ImplicitFunction[i - 1], ImplicitFunction[i], spacing);
	  fDx = SIDE(ImplicitFunction[i], ImplicitFunction[i + 1], spacing);
	  bDy = SIDE(ImplicitFunction[i - ImgSize.x], ImplicitFunction[i], spacing);
	  fDy = -bDy;
	  fDz = SIDE(ImplicitFunction[i], ImplicitFunction[i + ImgSize.x * ImgSize.y], spacing);
	  bDz = -fDz;
	  Dxx += ImplicitFunction[i + 1] + ImplicitFunction[i - 1];
	  Dyy += 2 * ImplicitFunction[i - ImgSize.x];
	  Dzz += 2 * ImplicitFunction[i + ImgSize.x * ImgSize.y];	
      break;

	case  11011:
      Dx = CENTRAL(ImplicitFunction[i - 1], ImplicitFunction[i + 1], spacing);
	  IGx = CENTRAL(ImageGradient[i - 1], ImageGradient[i + 1], spacing);
	  bDx = SIDE(ImplicitFunction[i - 1], ImplicitFunction[i], spacing);
	  fDx = SIDE(ImplicitFunction[i], ImplicitFunction[i + 1], spacing);
	  fDy = SIDE(ImplicitFunction[i], ImplicitFunction[i + ImgSize.x], spacing);
	  bDy = -fDy;
	  bDz = SIDE(ImplicitFunction[i - ImgSize.x * ImgSize.y], ImplicitFunction[i], spacing);
      fDz = -bDz;
	  Dxx += ImplicitFunction[i + 1] + ImplicitFunction[i - 1];
	  Dyy += 2 * ImplicitFunction[i + ImgSize.x];
	  Dzz += 2 * ImplicitFunction[i + ImgSize.x - ImgSize.y];	
      break;

	case  10111:
      Dx = CENTRAL(ImplicitFunction[i - 1], ImplicitFunction[i + 1], spacing);
	  IGx = CENTRAL(ImageGradient[i - 1], ImageGradient[i + 1], spacing);
	  bDx = SIDE(ImplicitFunction[i - 1], ImplicitFunction[i], spacing);
	  fDx = SIDE(ImplicitFunction[i], ImplicitFunction[i + 1], spacing);
	  bDy = SIDE(ImplicitFunction[i - ImgSize.x], ImplicitFunction[i], spacing);
	  fDy = -bDy;
	  bDz = SIDE(ImplicitFunction[i - ImgSize.x * ImgSize.y], ImplicitFunction[i], spacing);
      fDz = -bDz;
	  Dxx += ImplicitFunction[i + 1] + ImplicitFunction[i - 1];
	  Dyy += ImplicitFunction[i + ImgSize.x] + ImplicitFunction[i - ImgSize.x];
	  Dzz += 2 * ImplicitFunction[i - ImgSize.x * ImgSize.y];	
      break;	

	case 101010:
	  fDx = SIDE(ImplicitFunction[i], ImplicitFunction[i + 1], spacing);
	  bDx = -fDx;
	  fDy = SIDE(ImplicitFunction[i], ImplicitFunction[i + ImgSize.x], spacing);
	  bDy = -fDy;
      fDz = SIDE(ImplicitFunction[i], ImplicitFunction[i + ImgSize.x * ImgSize.y], spacing);
	  bDz = -fDz;
	  Dxx += 2 * ImplicitFunction[i + 1];
	  Dyy += 2 * ImplicitFunction[i + ImgSize.x];
	  Dzz += 2 * ImplicitFunction[i + ImgSize.x * ImgSize.y];	
      break;

	case 101001:
      bDx = SIDE(ImplicitFunction[i - 1], ImplicitFunction[i], spacing);
	  fDx = -bDx;
	  fDy = SIDE(ImplicitFunction[i], ImplicitFunction[i + ImgSize.x], spacing);
	  bDy = -fDy;
      fDz = SIDE(ImplicitFunction[i], ImplicitFunction[i + ImgSize.x * ImgSize.y], spacing);
	  bDz = -fDz;
	  Dxx += 2 * ImplicitFunction[i - 1];
	  Dyy += 2 * ImplicitFunction[i + ImgSize.x];
	  Dzz += 2 * ImplicitFunction[i + ImgSize.x * ImgSize.y];	
      break;

	case 100110:
	  fDx = SIDE(ImplicitFunction[i], ImplicitFunction[i + 1], spacing);
	  bDx = -fDx;
	  bDy = SIDE(ImplicitFunction[i - ImgSize.x], ImplicitFunction[i], spacing);
	  fDy = -bDy;
	  fDz = SIDE(ImplicitFunction[i], ImplicitFunction[i + ImgSize.x * ImgSize.y], spacing);
	  bDz = -fDz;
	  Dxx += 2 * ImplicitFunction[i + 1];
	  Dyy += 2 * ImplicitFunction[i - ImgSize.x];
	  Dzz += 2 * ImplicitFunction[i + ImgSize.x * ImgSize.y];	
      break;

	case 100101:
      bDx = SIDE(ImplicitFunction[i - 1], ImplicitFunction[i], spacing);
	  fDx = -bDx;
	  bDy = SIDE(ImplicitFunction[i - ImgSize.x], ImplicitFunction[i], spacing);
	  fDy = -bDy;
	  fDz = SIDE(ImplicitFunction[i], ImplicitFunction[i + ImgSize.x * ImgSize.y], spacing);
	  bDz = -fDz;
	  Dxx += 2 * ImplicitFunction[i - 1];
	  Dyy += 2 * ImplicitFunction[i - ImgSize.x];
	  Dzz += 2 * ImplicitFunction[i + ImgSize.x * ImgSize.y];	
      break;

	case  11010:
      fDx = SIDE(ImplicitFunction[i], ImplicitFunction[i + 1], spacing);
	  bDx = -fDx;
	  fDy = SIDE(ImplicitFunction[i], ImplicitFunction[i + ImgSize.x], spacing);
	  bDy = -fDy;
      bDz = SIDE(ImplicitFunction[i - ImgSize.x * ImgSize.y], ImplicitFunction[i], spacing);
      fDz = -bDz;
	  Dxx += 2 * ImplicitFunction[i + 1];
	  Dyy += 2 * ImplicitFunction[i + ImgSize.x];
	  Dzz += 2 * ImplicitFunction[i - ImgSize.x * ImgSize.y];	
      break;
		 
	case  11001:
      bDx = SIDE(ImplicitFunction[i - 1], ImplicitFunction[i], spacing);
	  fDx = -bDx;
	  fDy = SIDE(ImplicitFunction[i], ImplicitFunction[i + ImgSize.x], spacing);
	  bDy = -fDy;
      bDz = SIDE(ImplicitFunction[i - ImgSize.x * ImgSize.y], ImplicitFunction[i], spacing);
      fDz = -bDz;
	  Dxx += 2 * ImplicitFunction[i - 1];
	  Dyy += 2 * ImplicitFunction[i + ImgSize.x];
	  Dzz += 2 * ImplicitFunction[i - ImgSize.x * ImgSize.y];	
      break;

	case  10110: 
      fDx = SIDE(ImplicitFunction[i], ImplicitFunction[i + 1], spacing);
	  bDx = -fDx;
	  bDy = SIDE(ImplicitFunction[i - ImgSize.x], ImplicitFunction[i], spacing);
	  fDy = -bDy;
	  bDz = SIDE(ImplicitFunction[i - ImgSize.x * ImgSize.y], ImplicitFunction[i], spacing);
      fDz = -bDz;
	  Dxx += 2 * ImplicitFunction[i + 1];
	  Dyy += 2 * ImplicitFunction[i - ImgSize.x];
	  Dzz += 2 * ImplicitFunction[i - ImgSize.x * ImgSize.y];	
      break;

	case  10101:
      bDx = SIDE(ImplicitFunction[i - 1], ImplicitFunction[i], spacing);
	  fDx = -bDx;
	  bDy = SIDE(ImplicitFunction[i - ImgSize.x], ImplicitFunction[i], spacing);
	  fDy = -bDy;
	  bDz = SIDE(ImplicitFunction[i - ImgSize.x * ImgSize.y], ImplicitFunction[i], spacing);
      fDz = -bDz;
	  Dxx += 2 * ImplicitFunction[i - 1];
	  Dyy += 2 * ImplicitFunction[i - ImgSize.x];
	  Dzz += 2 * ImplicitFunction[i - ImgSize.x * ImgSize.y];	
      break;
  }
  
  float val = MAX(SQR(Dx) + SQR(Dy) + SQR(Dz) , EPSILON);
  float cur = ((SQR(Dy) + SQR(Dz)) * Dxx + (SQR(Dx) + SQR(Dz)) * Dyy + (SQR(Dx) + SQR(Dy)) * Dzz - 
	           0.5 * (Dx * Dy * Dxy + Dx * Dz * Dxz + Dy * Dz * Dyz)) / (val * SQR(spacing));

  if (cur > CurvatureLimit)
	cur = CurvatureLimit;
  else
    if (cur < -CurvatureLimit)
      cur = -CurvatureLimit;

  float adv = MAX(IGx, 0) * bDx + MIN(IGx, 0) * fDx + MAX(IGy, 0) * bDy + MIN(IGy, 0) * fDy + 
	          MAX(IGz, 0) * bDz + MIN(IGz, 0) * fDz;

  float Godunov = sqrt(MAX(SQR(MAX(bDx, 0)), SQR(MIN(fDx, 0))) + MAX(SQR(MAX(bDy, 0)), SQR(MIN(fDy, 0))) + 
 	                   MAX(SQR(MAX(bDz, 0)), SQR(MIN(fDz, 0))));

  float grad;
  if (p == 2)
    grad = 1 / SQR(1 + ImageGradient[i]);
  else 
    grad = 1 / (1 + ImageGradient[i]);

  MaxProp = MAX(MaxProp, 3 * Godunov * grad);
  MaxAdv = MAX(MaxAdv, std::abs(IGx) + std::abs(IGy) + std::abs(IGz));

  return grad * (Godunov - alpha * cur) + beta * adv;
}

template <class T> void SparseFieldRoutine3D<T>::GetNeighbors(size_t i, std::vector<size_t> &neigh, int layer)
{
  int x, y, z;
  GETPOSITION_3D(x, y, z);

  neigh.clear();

  if (x > 0 && Status[i - 1] == layer)
    neigh.push_back(i - 1);

  if (x < (int)ImgSize.x - 1  && Status[i + 1] == layer)
    neigh.push_back(i + 1);

  if (y > 0 && Status[i - ImgSize.x] == layer)
    neigh.push_back(i - ImgSize.x);

  if (y < (int)ImgSize.y - 1 && Status[i + ImgSize.x] == layer)
    neigh.push_back(i + ImgSize.x);

  if (z > 0 && Status[i - ImgSize.x * ImgSize.y] == layer)
    neigh.push_back(i - ImgSize.x * ImgSize.y);

  if (z < (int)ImgSize.z - 1 && Status[i + ImgSize.x * ImgSize.y] == layer)
    neigh.push_back(i + ImgSize.x * ImgSize.y);
}

/** Helper function for SwedenFastLevelSet function, which transforms its input image to float image. */
template <class VOXEL> static void InputToFloat(const i3d::Image3d<VOXEL> &input, i3d::Image3d<float> &output);

template <> void InputToFloat<i3d::GRAY8>(const i3d::Image3d<i3d::GRAY8> &input, i3d::Image3d<float> &output)
{
  i3d::GrayToFloat(input, output);
}

template <> void InputToFloat<i3d::GRAY16>(const i3d::Image3d<i3d::GRAY16> &input, i3d::Image3d<float> &output)
{
  i3d::GrayToFloat(input, output);
}

template <> void InputToFloat<float>(const i3d::Image3d<float> &input, i3d::Image3d<float> &output)
{
  output = input;
}

/////////////////////////////////////////////////////////////////
//                                                             //
//                    Class SwedenFastLevelSet                 //
//                                                             //
/////////////////////////////////////////////////////////////////

template <class VOXELIN, class VOXELOUT> 
SwedenFastLevelSet<VOXELIN, VOXELOUT>::SwedenFastLevelSet(const i3d::Image3d<VOXELIN> &input, 
	i3d::Image3d<VOXELOUT> &out, const std::vector<size_t> *Seeds, const i3d::Image3d<VOXELIN> *mask, const i3d::Image3d<i3d::BINARY> *Domain,
	i3d::Vector3d<float> Spacing, float alpha, float beta, float c, int p, size_t R, double sigma_x, double sigma_y, 
	double sigma_z, double width, SegmentationType _type, bool preserve_topology): output(out), type(_type)
{
	#ifdef I3D_DEBUG
		cout << endl << "Parameters of sweden fast level set method: " << endl << "SpacingXY = " << Spacing.x 
			 << endl << "SpacingZ = " << Spacing.z << endl << "alpha = " << alpha << endl << "beta = " << beta 
			 << endl << "p= " << p << endl << "R= " << R << endl << "SigmaX = " << sigma_x << endl << "SigmaY = " 
			 << sigma_y << endl  << "SigmaZ = " << sigma_z << endl << "Width = " << width << endl
			 << "PreserveTopology = " << preserve_topology << endl;
	#endif

	output.SetOffset(input.GetOffset());
	output.SetResolution(input.GetResolution());
	//output.MakeRoom(input.GetSize());

	i3d::Image3d<float> ImgPom, ImgG;
	InputToFloat(input, ImgPom);

	i3d::Resolution res(1.0 / Spacing.x, 1.0 / Spacing.y, 1.0 / Spacing.z);
	ImgPom.SetResolution(res);

	if (input.GetSizeZ() == 1)
	{
		Gauss(ImgPom, sigma_x, sigma_y, 0, width);
		EstimateGradient2_2D(ImgPom, ImgG, true);

		size_t tmp = 2 * R + 1;
		size_t N = SQR(tmp);

		if (preserve_topology)
		{
			Comp = new CompSweden2D_PreserveTopology<VOXELOUT>(Domain, &ImgG.GetVoxelData(), input.GetSize(), Spacing, 
															   alpha, beta, c, p, N, R);
		}
		else
		{
			Comp = new CompSweden2D<VOXELOUT>(Domain, &ImgG.GetVoxelData(), input.GetSize(), Spacing, alpha, beta, c, p, N, R);
		}
	}
	else
	{ 
		i3d::GaussIIR(ImgPom, float(sigma_x), float(sigma_y), float(sigma_z));
		EstimateGradient2_3D(ImgPom, ImgG, true);
		ImgPom.DisposeData();
		size_t tmp = 2 * R + 1;
		size_t N = SQR(tmp) * tmp; 

		if (preserve_topology)
		{
			Comp = new CompSweden3D_PreserveTopology<VOXELOUT>(Domain, &ImgG.GetVoxelData(), input.GetSize(), Spacing, 
															   alpha, beta, c, p, N, R);
		}
		else
		{
			Comp = new CompSweden3D<VOXELOUT>(Domain, &ImgG.GetVoxelData(), input.GetSize(), Spacing, alpha, beta, c, p, N, R);
		}
	}

	ImgG.DisposeData();

	Comp -> Init(Seeds, mask);
}

/////////////////////////////////////////////////////////////////
//                                                             //
//                    Class ChineseFastLevelSet                //
//                                                             //
/////////////////////////////////////////////////////////////////

template <class VOXELIN, class VOXELOUT> 
ChineseFastLevelSet<VOXELIN, VOXELOUT>::ChineseFastLevelSet(const i3d::Image3d<VOXELIN> &input, i3d::Image3d<VOXELOUT> &out, 
											 			    std::vector<size_t> &Seeds, i3d::Vector3d<float> Spacing, 
														    float alpha, float beta, int p, double sigma_x, double sigma_y, 
															double sigma_z, double width, SegmentationType _type): 
    output(out), type(_type)
{
#ifdef I3D_DEBUG
  cout << endl << "Parameters of chinese fast level set method: " << endl << "SpacingXY = " << Spacing.x 
       << endl << "SpacingZ = " << Spacing.z << endl << "alpha = " << alpha << endl << "beta = " << beta 
	   << endl << "p= " << p << endl << "SigmaX = " << sigma_x << endl << "SigmaY = " << sigma_y << endl 
	   << "SigmaZ = " << sigma_z << endl << "Width = " << width << endl;
 #endif

  output.SetOffset(input.GetOffset());
  output.SetResolution(input.GetResolution());
  output.MakeRoom(input.GetSize());

  i3d::Image3d<float> ImgPom, ImgG;
  InputToFloat(input, ImgPom);

  i3d::Resolution res(1.0 / Spacing.x, 1.0 / Spacing.y, 1.0 / Spacing.z);
  ImgPom.SetResolution(res);

  if (input.GetSizeZ() == 1)
  {
	  i3d::Gauss(ImgPom, sigma_x, sigma_y, 0, width);
	  i3d::EstimateGradient2_2D(ImgPom, ImgG, true);
	Comp = new CompChinese2D<VOXELOUT>(&ImgG.GetVoxelData(), Seeds, input.GetSize(), Spacing, alpha, beta, p);
  }
  else
  { 
	  i3d::Gauss(ImgPom, sigma_x, sigma_y, sigma_z, width);
	  i3d::EstimateGradient2_3D(ImgPom, ImgG, true);
    Comp = new CompChinese3D<VOXELOUT>(&ImgG.GetVoxelData(), Seeds, input.GetSize(), Spacing, alpha, beta, p);
  }
}

/////////////////////////////////////////////////////////////////
//                                                             //
//                    Class SparseFieldMethod                  //
//                                                             //
/////////////////////////////////////////////////////////////////

template <class VOXELIN, class VOXELOUT> 
SparseFieldMethod<VOXELIN, VOXELOUT>::SparseFieldMethod(const i3d::Image3d<VOXELIN> &input, i3d::Image3d<VOXELOUT> &out, 
														std::vector<size_t> &Seeds, i3d::Vector3d<float> Spacing, 
														float alpha, float beta, int p, double sigma_x, double sigma_y, 
														double sigma_z, double width, SegmentationType _type): 
    output(out), type(_type)
{
#ifdef I3D_DEBUG
  cout << endl << "Parameters of chinese fast level set method: " << endl << "SpacingXY = " << Spacing.x 
       << endl << "SpacingZ = " << Spacing.z << endl << "alpha = " << alpha << endl << "beta = " << beta 
	   << endl << "p= " << p << endl << "SigmaX = " << sigma_x << endl << "SigmaY = " << sigma_y << endl 
	   << "SigmaZ = " << sigma_z << endl << "Width = " << width << endl;
#endif

  output.SetOffset(input.GetOffset());
  output.SetResolution(input.GetResolution());
  output.MakeRoom(input.GetSize());

  i3d::Image3d<float> ImgPom, ImgG;
  InputToFloat(input, ImgPom);

  i3d::Resolution res(1.0 / Spacing.x, 1.0 / Spacing.y, 1.0 / Spacing.z);
  ImgPom.SetResolution(res);
  
  if (input.GetSizeZ() == 1)
  {
    Gauss(ImgPom, sigma_x, sigma_y, 0, width);
    EstimateGradient2_2D(ImgPom, ImgG, true);
	Routine = new SparseFieldRoutine2D<VOXELOUT>(&ImgG.GetVoxelData(), Seeds, input.GetSize(), Spacing.x, alpha, beta, p);
  }
  else
  { 
    Gauss(ImgPom, sigma_x, sigma_y, sigma_z, width);
    EstimateGradient2_3D(ImgPom, ImgG, true);
    Routine = new SparseFieldRoutine3D<VOXELOUT>(&ImgG.GetVoxelData(), Seeds, input.GetSize(), Spacing.x, alpha, beta, p);
  }
}

/* Explicit instantiations */
template class I3D_DLLEXPORT SwedenFastLevelSet<i3d::GRAY8, i3d::GRAY8>;
template class I3D_DLLEXPORT SwedenFastLevelSet<i3d::GRAY16, i3d::GRAY16>;

template class I3D_DLLEXPORT ChineseFastLevelSet<i3d::GRAY8, i3d::GRAY8>;
template class I3D_DLLEXPORT SparseFieldMethod<i3d::GRAY8, i3d::GRAY8>;

template class I3D_DLLEXPORT CompSwedenLevelSet<i3d::GRAY8>;
template class I3D_DLLEXPORT CompSwedenLevelSet<i3d::GRAY16>;

template class I3D_DLLEXPORT CompSwedenLevelSet_PreserveTopology<i3d::GRAY8>;
template class I3D_DLLEXPORT CompSwedenLevelSet_PreserveTopology<i3d::GRAY16>;

template class I3D_DLLEXPORT CompSweden2D<i3d::GRAY8>;
template class I3D_DLLEXPORT CompSweden2D<i3d::GRAY16>;

template class I3D_DLLEXPORT CompSweden2D_PreserveTopology<i3d::GRAY8>;
template class I3D_DLLEXPORT CompSweden2D_PreserveTopology<i3d::GRAY16>;

template class I3D_DLLEXPORT CompSweden3D<i3d::GRAY8>;
template class I3D_DLLEXPORT CompSweden3D<i3d::GRAY16>;

template class I3D_DLLEXPORT CompSweden3D_PreserveTopology<i3d::GRAY8>;
template class I3D_DLLEXPORT CompSweden3D_PreserveTopology<i3d::GRAY16>;

