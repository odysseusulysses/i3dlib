/*
* i3dlib - image manipulation library
*
* Copyright (C) 2000-2006   Centre for Biomedical Image Analysis (CBIA)
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Library General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Library General Public License for more details.
*
* You should have received a copy of the GNU Library General Public
* License along with this library; if not, write to the Free
* Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

/** \file watersheds.h
\brief Part of mathematical morphology routines - header.

This file provides various watershed transformation functions.
*/

#ifndef __WATERSHEDS_H__
#define __WATERSHEDS_H__

#ifdef __GNUG__
#	pragma interface
#endif

#include "image3d.h"
#include "neighbours.h"

namespace i3d
{
	/** \addtogroup morph */
	///@{

	/** Morphological Watershed. It uses Toboggan based approach implemented primarily by Lin et.al 
	and published in 
	"Comparison between immersion-based and toboggan-based watershed image segmentation"
	Yung-Chieh Lin; Yu-Pao Tsai; Yi-Ping Hung; Zen-Chung Shih, 
	IEEE Transactions on Image Processing, 15, 3, March 2006

	and altered for 3D by Martin Maska and Pavel Matula

	This algorithm sometimes produces thick watershed lines. The result of the algorithm itself is very close to the Vincent-Soille algorithm.

	\param in the input image
	\param out the output labelled image, -1 is marking the watershed line, regions are marked beginning at 1 (0 is not used for anything)
	\param connectivity is 4, 8, 6, 18, 26
	\param drawLines whether to draw watershed lines everywhere or just in the ambiguous cases
	*/
	template <class T> I3D_DLLEXPORT void WatershedToboggan(
		const Image3d<T> &in, 
		Image3d<int> &out,
		int connectivity,
		bool drawLines = false);

	/** Morphological Watershed. It uses Toboggan based approach implemented by Lin et.al 
	and published in 
	"Comparison between immersion-based and toboggan-based watershed image segmentation"
	Yung-Chieh Lin; Yu-Pao Tsai; Yi-Ping Hung; Zen-Chung Shih, 
	IEEE Transactions on Image Processing, 15, 3, March 2006

	It can be called for 2D images (NumSlices() is equal to 1). InternalException is raised otherwise.

	This algorithm sometimes produces thick watershed lines. The result of the algorithm itself is very close to the Vincent-Soille algorithm.

	\param in the input image
	\param out the output labelled image, -1 is marking the watershed line, regions are marked beginning at 1 (0 is not used for anything)
	\param connectivity is 4 or 8
	*/
	template <class T> I3D_DLLEXPORT void WatershedToboggan2D(
		const Image3d<T> &in, 
		Image3d<int> &out,
		int connectivity);

	/** Morphological Watershed. It uses Watershed function implemented primarily by Lin et.al 
	and published in 
	"Comparison between immersion-based and toboggan-based watershed image segmentation"
	Yung-Chieh Lin; Yu-Pao Tsai; Yi-Ping Hung; Zen-Chung Shih, 
	IEEE Transactions on Image Processing, 15, 3, March 2006

	and altered for 3D by Martin Maska.

	This algorithm sometimes produces thick watershed lines. The result of the algorithm itself is very close to the Vincent-Soille algorithm. Use WatershedToboggan to get the same results but in shorter time.
	\warning Parameter drawLines has different default value than in WatershedToboggan.

	\param in the input image
	\param out the output labelled image, -1 is marking the watershed line, regions are marked beginning at 1 (0 is not used for anything)
	\param connectivity is 4, 8, 6, 18, 26
	\param drawLines whether to draw watershed lines everywhere or just in the ambiguous cases
	*/
	template <class T> I3D_DLLEXPORT void WatershedLin(
		const Image3d<T> &in, 
		Image3d<int> &out,
		int connectivity,
		bool drawLines = false);

	/** Morphological Watershed. It uses Watershed function implemented by Lin et.al 
	and published in 
	"Comparison between immersion-based and toboggan-based watershed image segmentation"
	Yung-Chieh Lin; Yu-Pao Tsai; Yi-Ping Hung; Zen-Chung Shih, 
	IEEE Transactions on Image Processing, 15, 3, March 2006

	It can be called for 2D images (NumSlices() is equal to 1). InternalException is raised otherwise.

	Watershed lines are drawn only in the ambiguous cases. The result of the algorithm itself is very close to the Vincent-Soille algorithm. Use WatershedToboggan2D to get the same results but in shorter time.

	\param in the input image
	\param out the output labelled image, -1 is marking the watershed line, regions are marked beginning at 1 (0 is not used for anything)
	\param connectivity is 4 or 8
	*/
	template <class T> I3D_DLLEXPORT void WatershedLin2D(
		const Image3d<T> &in, 
		Image3d<int> &out,
		int connectivity);

	/** Morphological watershed algorithm according to Meyer. This algorithm produces thin watershed lines.
	\param input the input image
	\param marker the marker image used for the start of the region growing (non-zero values should be used for the markers, zero otherwise)
	\param output the output labelled image, 0 is marking the watershed line
	\param neighbourhood the neighbourhood used for the computation
	*/
	template <typename T> 
	I3D_DLLEXPORT void WatershedMeyer(const Image3d<T> &input, const Image3d<size_t> &marker, Image3d<size_t> &output, const Neighbourhood &neighbourhood);

	/** Morphological watershed algorithm according to Meyer. This algorithm produces thin watershed lines. This version uses all the regional minima as markers.
	\param input the input image
	\param output the output labelled image, 0 is marking the watershed line
	\param neighbourhood the neighbourhood used for the computation
	*/
	template <typename T> 
	I3D_DLLEXPORT void WatershedMeyer(const Image3d<T> &input, Image3d<size_t> &output, const Neighbourhood &neighbourhood);

	/** Watershed constours geodesic saliency. Morphological watershed that 
	uses the concept of dynamics of contours presented by Najman and Schmitt in
	Laurent Najman, Michel Schmitt, "Geodesic Saliency of Watershed Contours and Hierarchical Segmentation,"
	IEEE Transactions on Pattern Analysis and Machine Intelligence, 
	vol. 18, no. 12, pp. 1163-1173, December, 1996.

	\param in the input image
	\param out the output labelled image
	\param connectivity is 4, 8, 6, 18, 26
	*/
	template <class T> I3D_DLLEXPORT void WatershedCGS(
		const Image3d<T> &in, 
		Image3d<T> &out,
		int connectivity);

	/** Morphological Watershed.
	\warning This function converts image voxels to int internally. 
	\warning This implementation has been outperformed by other implementations available, stop using it! */
	template <class T> I3D_DLLEXPORT void Watershed(
		const Image3d<T> &in, 
		Image3d<T> &out,
		const Neighbourhood &neib);

	///@} end of morph group part

} // namespace i3d

#endif // #ifndef __WATERSHEDS_H__
