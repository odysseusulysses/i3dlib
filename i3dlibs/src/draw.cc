/*
 * i3dlib - image manipulation library
 *
 * Copyright (C) 2000-2006   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** \file draw.cc

  \brief Basic drawing, filling and rasterization algorithms - implementation

  \author Pavel Matula (pam@fi.muni.cz) 2002
  \author David Svoboda (xsvobod2@fi.muni.cz) 2002 - FloodFill functions
  \author Petr Matula (pem@fi.muni.cz) 2004 - DrawCube
  
*/

#ifdef __GNUG__
#	pragma implementation
#endif

#include <iostream>
#include <algorithm>
#include <math.h>
#include <stack>
#include "neighbours.h"
#include "draw.h"

#ifdef _WIN32
#	define rint(x) (floor((x)+0.5))
#endif

using namespace std;
using std::abs;

namespace i3d {
	/*****************************************************************************\
	*
	*                            Local functions
	*
	\******************************************************************************/
	//--------------------------------------------------------------------------
	// PrepareLine
	//--------------------------------------------------------------------------
	size_t PrepareLine(Vector3d<size_t> p1, Vector3d <size_t> p2, Point3d<double> &d)
	{
		int p1x = p1.x, p1y = p1.y, p1z = p1.z, p2x = p2.x, p2y = p2.y, p2z = p2.z;

		Vector3d<int> v = Vector3d<int>(p2x - p1x, p2y - p1y, p2z - p1z);
		size_t lng = std::max(std::max(::abs(v.x), ::abs(v.y)), ::abs(v.z));
		d = Point3d<double>(v.x / (double) lng, v.y / (double) lng, v.z / (double) lng);
		return lng;
	} // PrepareLine

	//--------------------------------------------------------------------------
	// ShiftPenAlongLine
	//--------------------------------------------------------------------------
	inline void ShiftPenAlongLine(Point3d<double> &D, int &dx, int &dy, int &dz)
	{
		if (D.x () < 0 && (D.x () < (dx - 1)))
			--dx;
		if (D.x () > 0 && (D.x () > (dx + 1)))
			++dx;
		if (D.y () < 0 && (D.y () < (dy - 1)))
			--dy;
		if (D.y () > 0 && (D.y () > (dy + 1)))
			++dy;
		if (D.z () < 0 && (D.z () < (dz - 1)))
			--dz;
		if (D.z () > 0 && (D.z () > (dz + 1)))
			++dz;
	} // ShiftPenAlongLine

	//--------------------------------------------------------------------------
	// RasterizeLineBres
	//--------------------------------------------------------------------------
	/** Drawing line using Bressenham algorithm. `Dest` is an appropriate functor 
	storing generated points to required place in memory/display. */
	template <class T> void RasterizeLineBres(const T &Dest, Vector3d<size_t> p1, Vector3d<size_t> p2)
	{
		int i, dx, dy, dz, l, m, n, x_inc, y_inc, z_inc, err_1, err_2, dx2, dy2, dz2;
		Vector3d<size_t> point = p1;

		dx = p2.x - p1.x;
		dy = p2.y - p1.y;
		dz = p2.z - p1.z;

		x_inc = (dx < 0) ? -1 : 1;
		l = ::abs (dx);

		y_inc = (dy < 0) ? -1 : 1;
		m = ::abs (dy);

		z_inc = (dz < 0) ? -1 : 1;
		n = ::abs (dz);

		dx2 = l << 1;
		dy2 = m << 1;
		dz2 = n << 1;

		if ((l >= m) && (l >= n))
		{
			err_1 = dy2 - l;
			err_2 = dz2 - l;
			for (i = 0; i < l; i++)
			{
				Dest.StorePoint (point);
				if (err_1 > 0)
				{
					point.y += y_inc;
					err_1 -= dx2;
				}
				if (err_2 > 0)
				{
					point.z += z_inc;
					err_2 -= dx2;
				}
				err_1 += dy2;
				err_2 += dz2;
				point.x += x_inc;
			}
		}
		else if ((m >= l) && (m >= n))
		{
			err_1 = dx2 - m;
			err_2 = dz2 - m;

			for (i = 0; i < m; i++)
			{
				Dest.StorePoint (point);
				if (err_1 > 0)
				{
					point.x += x_inc;                
					err_1 -= dy2;             
				}
				if (err_2 > 0)
				{
					point.z += z_inc;
					err_2 -= dy2;
				}
				err_1 += dx2;
				err_2 += dz2;
				point.y += y_inc;
			}
		}
		else
		{
			err_1 = dy2 - n;
			err_2 = dx2 - n;
			for (i = 0; i < n; i++)
			{
				Dest.StorePoint (point);
				if (err_1 > 0)
				{
					point.y += y_inc;
					err_1 -= dz2;
				}
				if (err_2 > 0)
				{
					point.x += x_inc;
					err_2 -= dz2;
				}
				err_1 += dy2;
				err_2 += dx2;
				point.z += z_inc;
			}
		}
		Dest.StorePoint (point);
	} // RasterizeLineBres

	//--------------------------------------------------------------------------
	// FillRow
	//--------------------------------------------------------------------------
	template <class VOXEL1, class VOXEL2> size_t FillRow(
		std::stack<size_t> &buf,
		const Image3d<VOXEL1> &input,
		Image3d<VOXEL2> &output, 
		VOXEL2 NewColor,
		const Neighbourhood &neib)
	{
		std::vector<const VOXEL1 *> adjacent;
		size_t max_x = input.GetSizeX() - 1;
		size_t Current, Addr, RowSeed;
		size_t i, j, Overlap, ScanLineLength = 0;
		RowSeed = buf.top();
		buf.pop();
		Addr = RowSeed;
		// Test whether the voxel has been already filled with the NewColor
		if (output.GetVoxel(Addr) == NewColor)
			return 0;

		// In case the flood has not already reached the current voxel we pick up
		// its color = color of FloodFilled region
		VOXEL1 OldColor = input.GetVoxel(Addr);
		// Mark the scanline voxels (bounded by object voxels) with NewColor
		do
		{
			output.SetVoxel(Addr, NewColor);
			ScanLineLength++;
			if ((input.GetX (Addr) == 0) || (input.GetVoxel (Addr - 1) != OldColor))
				break;
			Addr--;
		}
		while (true);
		// Find the apropriate neighbouring voxels
		GetWindow(input, input.GetX(RowSeed), input.GetY(RowSeed), input.GetZ(RowSeed), neib, adjacent);
		for (i = 0; i < adjacent.size(); i++)
		{
			Current = adjacent[i] - input.GetFirstVoxelAddr();

			// Eliminate all the voxels in the same line as the seed is.
			if ((input.GetY(Current) == input.GetY(RowSeed)) && (input.GetZ(Current) == input.GetZ(RowSeed)))
				continue;

			// Before implating new seed we have to find an apropriate place
			// for new seed. It should be the rightmost voxel in the currently
			// passed scanline. The variable "Overlap" measures the distance
			// from voxel "Current" to the rightmost voxel.
			Overlap = 0;
			// The first voxel in the scanline may be added to the
			// queue as a new seed if its color is not equal to new color .
			if ((input.GetVoxel(Current) == OldColor) && (output.GetVoxel(Current) != NewColor))
			{
				// Go to the rightmost component voxel in the line
				// parallel to the currently filled scanline.
				while ((input.GetX(Current) < max_x) && (input.GetVoxel(Current + 1) == OldColor))
				{
					Current++;
					Overlap++;
				}

				buf.push(Current);
			}

			// Verify the scanline - the same length as currently filled
			// scanline.
			for (j = 0; j < (ScanLineLength + Overlap - 1); j++)
			{
				if (input.GetX(Current) == 0)
					break;

				if ((input.GetVoxel (Current) != OldColor) && 
					(input.GetVoxel (Current - 1) == OldColor) && 
					(output.GetVoxel (Current - 1) != NewColor))
					buf.push (Current - 1);

				Current--;
			}
		}

		return ScanLineLength;
	} // FillRow

	/**************************************************************************\

	*

	*					Drawing and rasterization algorithms

	*

	\**************************************************************************/
	//--------------------------------------------------------------------------
	// DrawLine
	//--------------------------------------------------------------------------
	template <class T> void DrawLine(Image3d<T> &img, const Offset &p1, const Offset &p2, T color)
	{
		//cout << "DrawLine microns" << endl; 
		//cout << "p1 = " << p1 << endl; 
		//cout << "p2 = " << p2 << endl; 
		Vector3d<float> pp1 = MicronsToPixels(p1 - img.GetOffset(), img.GetResolution());
		Vector3d<float> pp2 = MicronsToPixels(p2 - img.GetOffset(), img.GetResolution());
		//cout << "pp1 = " << pp1 << endl; 
		//cout << "pp2 = " << pp2 << endl; 
		//FIXME: clipping!!!

		Vector3d<size_t> pix1 = Vector3d<size_t>(
			(size_t) rint (pp1.x),
			(size_t) rint (pp1.y),
			(size_t) rint (pp1.z));

		Vector3d<size_t> pix2 = Vector3d<size_t>(
			(size_t) rint (pp2.x),
			(size_t) rint (pp2.y),
			(size_t) rint (pp2.z));

		DrawLine(img, pix1, pix2, color);
		//cout << "DrawLine microns OK" << endl; 
	} // DrawLine

	template <class T> void DrawLine(Image3d<T> &img, Vector3d<size_t> p1, Vector3d<size_t> p2, T color)
	{
		int dx = 0, dy = 0, dz = 0;
		Point3d<double> D = Point3d<double>(0, 0, 0), d;
		size_t lng = PrepareLine(p1, p2, d);

		for (size_t i = 0; i <= lng; ++i)
		{
			img.SetVoxel(p1.x + dx, p1.y + dy, p1.z + dz, color);
			D += d;
			ShiftPenAlongLine(D, dx, dy, dz);
		}
		img.SetVoxel(p2.x, p2.y, p2.z, color);
	} // DrawLine

	//--------------------------------------------------------------------------
	// Explicit instantiations
	//--------------------------------------------------------------------------
	template I3D_DLLEXPORT void DrawLine(
		Image3d<BINARY> &img,
		Vector3d<size_t> p1,
		Vector3d<size_t> p2, 
		BINARY color);

	template I3D_DLLEXPORT void DrawLine(
		Image3d<GRAY8> &img,
		Vector3d<size_t> p1,
		Vector3d<size_t> p2, 
		GRAY8 color);

	template I3D_DLLEXPORT void DrawLine(
		Image3d<GRAY16> &img,
		Vector3d<size_t> p1,
		Vector3d<size_t> p2, 
		GRAY16 color);

	template I3D_DLLEXPORT void DrawLine(
		Image3d<RGB> &img,
		Vector3d<size_t> p1,
		Vector3d<size_t> p2, 
		RGB color);

	template I3D_DLLEXPORT void DrawLine(
		Image3d<RGB16> &img,
		Vector3d<size_t> p1,
		Vector3d<size_t> p2, 
		RGB16 color);

	//--------------------------------------------------------------------------
	// DrawLines
	//--------------------------------------------------------------------------
	template <class T> void DrawLines(Image3d<T> &img, const std::vector<Offset> &pnts, T color)
	{
		//cout << "DrawLines" << endl; 
		for (size_t i = 0; i < pnts.size() / 2; ++i)
		{
			DrawLine (img, pnts[2 * i], pnts[2 * i + 1], color);
		}
		//cout << "DrawLines OK" << endl; 
	} // DrawLines

	//--------------------------------------------------------------------------
	// Explicit instantiations
	//--------------------------------------------------------------------------
	template I3D_DLLEXPORT void DrawLines(
		Image3d<GRAY8> &img, 
		const std::vector<Offset> &pnts, 
		GRAY8 color);

	template I3D_DLLEXPORT void DrawLines(
		Image3d<GRAY16> &img,
		const std::vector<Offset> &pnts,
		GRAY16 color);

	template I3D_DLLEXPORT void DrawLines(
		Image3d<RGB> &img,
		const std::vector<Offset> &pnts, 
		RGB color);

	template I3D_DLLEXPORT void DrawLines(
		Image3d<RGB16> &img,
		const std::vector<Offset> &pnts,
		RGB16 color);

	//--------------------------------------------------------------------------
	// DrawLineBres
	//--------------------------------------------------------------------------
	template <class T> void DrawLineBres(Image3d <T> &img, Vector3d <size_t> p1, Vector3d <size_t> p2, T color)
	{
		RasterizeLineBres(StoreToImage<T>(&img, color), p1, p2);
	} // DrawLineBres

	void DrawLineBres(std::list< Vector3d<int> > &ll, Vector3d<size_t> p1, Vector3d<size_t> p2)
	{
		RasterizeLineBres(StoreToMem(&ll), p1, p2);
	}  // DrawLineBres

	//--------------------------------------------------------------------------
	// Explicit instantiations
	//--------------------------------------------------------------------------
	template I3D_DLLEXPORT void DrawLineBres(
		Image3d<BINARY> &img,
		Vector3d<size_t> p1,
		Vector3d<size_t> p2,
		BINARY color);

	template I3D_DLLEXPORT void DrawLineBres(
		Image3d<GRAY8> &img,
		Vector3d<size_t> p1,
		Vector3d<size_t> p2, 
		GRAY8 color);

	//--------------------------------------------------------------------------
	// DrawCube
	//--------------------------------------------------------------------------
	template <class T> void DrawCube(Image3d<T> &img, const VOI<PIXELS> &voi, T color)
	{
		// make sure that voi do not address elements out of image
		VOI<PIXELS> img_voi(0,img.GetSize());
		VOI<PIXELS> v = voi * img_voi; // make VOI intersection

		// addr of first pixel to draw
		size_t i = 
				  v.offset.x +   
				  v.offset.y * img.GetSizeX() + 
				  v.offset.z * img.GetSliceSize();

		size_t dy = img.GetSizeX() - v.size.x;
		size_t dz = img.GetSliceSize() - img.GetSizeX() * v.size.y;

		T* data = img.GetVoxelAddr(i);

		for (size_t z = 0; z < v.size.z; z++)
		{
			for (size_t y = 0; y < v.size.y; y++)
			{
				for (size_t x = 0; x < v.size.x; x++)
				{
					*data = color;
					++data;
				}
				data += dy;
			}
			data += dz;
		}
	} // DrawCube

	//--------------------------------------------------------------------------
	// Explicit instantiations
	//--------------------------------------------------------------------------
	template I3D_DLLEXPORT void DrawCube (Image3d<BINARY> &img, const VOI<PIXELS> &voi, BINARY color);
	template I3D_DLLEXPORT void DrawCube (Image3d<GRAY8> &img, const VOI<PIXELS> &voi, GRAY8 color);

	/**************************************************************************\

	*

	*						Filling algorithms

	*

	\**************************************************************************/
	//--------------------------------------------------------------------------
	// FloodFill
	//--------------------------------------------------------------------------
	template <class VOXEL1, class VOXEL2> ComponentInfo<VOXEL1> FloodFill(
		const Image3d<VOXEL1> &input,
		Image3d<VOXEL2> &output, 
		size_t x,
		size_t y, 
		size_t z, 
		VOXEL2 color,
		const Neighbourhood &neib)
	{
		size_t FirstSeed, LengthOfRow, RightEnd, LeftEnd;
		size_t OffX, OffY, OffZ;
		std::stack<size_t> buf;
		ComponentInfo<VOXEL1> component;
		// Show the first seed (voxel) and insert it into the stack.
		FirstSeed = input.GetVoxelAddr(x, y, z) - input.GetFirstVoxelAddr();
		VOXEL1 OldColor = input.GetVoxel(FirstSeed);
		size_t max_x = input.GetSizeX() - 1;
		while ((input.GetX(FirstSeed) < max_x) && (input.GetVoxel(FirstSeed + 1) == OldColor))
		{
			FirstSeed++;
		}
		buf.push(FirstSeed);
		while (!buf.empty())
		{
			RightEnd = buf.top();
			LengthOfRow = FillRow(buf, input, output, color, neib);
			if (LengthOfRow > 0)
			{
				LeftEnd = RightEnd - LengthOfRow + 1;
				OffX = input.GetX(LeftEnd);
				OffY = input.GetY(LeftEnd);
				OffZ = input.GetZ(LeftEnd);
				// Enlarge the VOI of component
				if (component.volume == 0)
				{
					component.voi = VOI<PIXELS>(OffX, OffY, OffZ, LengthOfRow, 1, 1);
				}
				else
				{
					component.voi += VOI<PIXELS>(OffX, OffY, OffZ, LengthOfRow, 1, 1);
				}

				// Enlarge the volume of component
				component.volume += LengthOfRow;
			}
		}

		Vector3d<size_t> ph = (Vector3d<size_t>) component.voi.offset + 
				  component.voi.size - 
				  Vector3d<size_t>(1,1,1);

		component.border = input.OnBorder(component.voi.offset.x, 
													 component.voi.offset.y, 
													 component.voi.offset.z) || 
				  input.OnBorder (ph.x, ph.y, ph.z);

		component.voxel_value = input.GetVoxel (x, y, z);

		return component;

	} // FloodFill

	template <class VOXEL> void FloodFill(
		Image3d<VOXEL> &img,
		VOXEL color, 
		size_t x, 
		size_t y, 
		size_t z,
		const Neighbourhood &neib)
	{
		std::stack<size_t> buf;
		size_t FirstSeed = img.GetVoxelAddr(x, y, z) - img.GetFirstVoxelAddr();
		VOXEL OldColor = img.GetVoxel(FirstSeed);
		size_t max_x = img.GetSizeX() - 1;
		// move the first seed voxel to the rightmost voxel
		while ((img.GetX(FirstSeed) < max_x) && (img.GetVoxel(FirstSeed + 1) == OldColor))
		{
			FirstSeed++;
		}

		buf.push (FirstSeed);
		while (!buf.empty())
		{
			FillRow(buf, img, img, color, neib);
		}
	} // FloodFill

	//--------------------------------------------------------------------------
	// Explicit instantiations
	//--------------------------------------------------------------------------
	template I3D_DLLEXPORT ComponentInfo<BINARY> FloodFill(
		const Image3d<BINARY> &input,
		Image3d<BINARY> &output, 
		size_t x,
		size_t y, 
		size_t z, 
		BINARY color, 
		const Neighbourhood &neib);

	template I3D_DLLEXPORT ComponentInfo<BINARY> FloodFill(
		const Image3d<BINARY> &input,
		Image3d<size_t> &output,
		size_t x,
		size_t y,
		size_t z, 
		size_t color, 
		const Neighbourhood &neib);

	template I3D_DLLEXPORT ComponentInfo<BINARY> FloodFill(
		const Image3d<BINARY> &input,
		Image3d<GRAY8> &output, 
		size_t x,
		size_t y, 
		size_t z, 
		GRAY8 color, 
		const Neighbourhood &neib);

	template I3D_DLLEXPORT ComponentInfo<BINARY> FloodFill(
		const Image3d<BINARY> &input,
		Image3d<GRAY16> &output,
		size_t x,
		size_t y,
		size_t z, 
		GRAY16 color, 
		const Neighbourhood &neib);

	template I3D_DLLEXPORT ComponentInfo<GRAY8> FloodFill(
		const Image3d<GRAY8> &input,
		Image3d<GRAY8> &output, 
		size_t x,
		size_t y,
		size_t z, 
		GRAY8 color, 
		const Neighbourhood &neib);

	template I3D_DLLEXPORT ComponentInfo<GRAY8> FloodFill(
		const Image3d<GRAY8> &input,
		Image3d<size_t> &output, 
		size_t x,
		size_t y, 
		size_t z, 
		size_t color, 
		const Neighbourhood &neib);

	template I3D_DLLEXPORT ComponentInfo<GRAY16> FloodFill(
		const Image3d<GRAY16> &input,
		Image3d<size_t> &output, 
		size_t x,
		size_t y, 
		size_t z, 
		size_t color, 
		const Neighbourhood &neib);

	template I3D_DLLEXPORT ComponentInfo<GRAY16> FloodFill(
		const Image3d<GRAY16> &input,
		Image3d<byte> &output, 
		size_t x,
		size_t y, 
		size_t z, 
		byte color, 
		const Neighbourhood &neib);

	template I3D_DLLEXPORT ComponentInfo<float> FloodFill(
		const Image3d<float> &input,
		Image3d<size_t> &output, 
		size_t x,
		size_t y, 
		size_t z, 
		size_t color, 
		const Neighbourhood &neib);

	template I3D_DLLEXPORT void FloodFill(
		Image3d<BINARY> &img,
		BINARY color, 
		size_t x, 
		size_t y, 
		size_t z,
		const Neighbourhood &neib);

	template I3D_DLLEXPORT void FloodFill(
		Image3d<GRAY8> &img,
		GRAY8 color, 
		size_t x, 
		size_t y, 
		size_t z,
		const Neighbourhood &neib);

	template I3D_DLLEXPORT void FloodFill(
		Image3d<GRAY16> &img,
		GRAY16 color, 
		size_t x, 
		size_t y, 
		size_t z,
		const Neighbourhood &neib);

	template I3D_DLLEXPORT void FloodFill(
		Image3d<RGB> &img,
		RGB color,
		size_t x,
		size_t y,
		size_t z,
		const Neighbourhood &neib);

	template I3D_DLLEXPORT void FloodFill(
		Image3d<RGB16> &img,
		RGB16 color,
		size_t x,
		size_t y,
		size_t z,
		const Neighbourhood &neib);

	template I3D_DLLEXPORT void FloodFill(
		Image3d<size_t> &img,
		size_t color,
		size_t x,
		size_t y,
		size_t z,
		const Neighbourhood &neib);

	template I3D_DLLEXPORT void FloodFill(
		Image3d<float> &img,
		float color, 
		size_t x, 
		size_t y, 
		size_t z,
		const Neighbourhood &neib);

	/**************************************************************************\

	*

	*						Other functions

	*

	\**************************************************************************/
	//--------------------------------------------------------------------------
	// Negative
	//--------------------------------------------------------------------------
	template <class VOXEL> void Negative(Image3d<VOXEL> &img)
	{
		size_t i = 0;
		while (i < img.GetImageSize())
		{
			img.SetVoxel(i, ColorInversion(img.GetVoxel(i)));
			i++;
		}
	} // Negative

	//--------------------------------------------------------------------------
	// Explicit instantiations
	//--------------------------------------------------------------------------
	template I3D_DLLEXPORT void Negative(Image3d<BINARY> &img);
	template I3D_DLLEXPORT void Negative(Image3d<GRAY8> &img);
	template I3D_DLLEXPORT void Negative(Image3d<GRAY16> &img);

} // i3d namespace
