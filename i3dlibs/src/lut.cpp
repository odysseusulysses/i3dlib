/*
 * lut.cpp
 *
 * Pavel Matula (pam@fi.muni.cz) 2007
 */

#include "lut.h"
#ifndef _MSC_VER
#include <cmath>
#endif
#include "image3d.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

namespace i3d {

/*************************************************************************************/

template <class ITYPE, class OTYPE> void LUT<ITYPE, OTYPE>::CreateAffine(float a, float b, OTYPE min, OTYPE max) 
{ 
	if (!(_lut))
	  throw LibException("CreateAffine(): ASSERT FAILED: NOT(_lut)");

	float r;
	for (long i = 0; i < _lut_size; ++i) 
    {
		r = a * GetCenter(i) + b;
		if (r < min) {
			_lut[i] = min;
		} else if (r > max) {
			_lut[i] = static_cast<OTYPE>(max);
		} else {
			_lut[i] = static_cast<OTYPE>(round(r));
		}
	} 
};

/*************************************************************************************/

template <class ITYPE, class OTYPE> 
void LUT<ITYPE, OTYPE>::CreateLinearMapping(ITYPE imin, ITYPE imax, OTYPE omin, OTYPE omax)
{
	if (!(_lut))
	  throw LibException("CreateLinearMapping(): ASSERT FAILED: NOT(_lut)");
	if (!(imin <= imax))
	  throw LibException("CreateLinearMapping(): ASSERT FAILED: NOT(imin <= imax)");

	float a, b;

	if (imin == imax) {
		CreateThresholding(imax, _max_value, omin, omax);
	} else {
		//if (typeid(OTYPE) == typeid(i3d::RGB) && omin == 0 && omax == 0) { // std::numeric_limits< >:: does not work for RGB correctly
		//	omin = std::numeric_limits<i3d::GRAY8>::min();
		//	omax = std::numeric_limits<i3d::GRAY8>::max();
		//} else if (typeid(OTYPE) == typeid(i3d::RGB16) && omin == 0 && omax == 0) {
		//	omin = std::numeric_limits<i3d::GRAY16>::min();
		//	omax = std::numeric_limits<i3d::GRAY16>::max();
		//}

		a = float(omax - omin)/float(imax - imin);
		b = omin - a * imin;
		// compute linear mapping correction
		for (int i = 0; i < GetPos(imin); i++)
		_lut[i] = omin;

		for (int i = GetPos(imin); i <= GetPos(imax); i++)
			_lut[i] = static_cast<OTYPE>(round(a * GetCenter(i) + b));

        for (int i = GetPos(imax) + 1; i < _lut_size; i++)
			_lut[i] = omax;
	}
}

/*************************************************************************************/

template <class ITYPE, class OTYPE> void LUT<ITYPE, OTYPE>::CreateThresholding(ITYPE min_threshold, ITYPE max_threshold, 
																					OTYPE min_value, OTYPE max_value) 
{ 
	if (!(_lut))
	  throw LibException("CreateThresholding(): ASSERT FAILED: NOT(_lut)");

	long i = 0;
	if (max_threshold > _max_value) max_threshold = _max_value;

	for (; i < GetPos(min_threshold); i++) 
		_lut[i] = min_value;

	for (; i <= GetPos(max_threshold); i++)
		_lut[i] = max_value;

	for (; i < _lut_size; i++)
		_lut[i] = min_value;
};

/*************************************************************************************/

template <class ITYPE, class OTYPE> void LUT<ITYPE, OTYPE>::CreateGamma(ITYPE imin, ITYPE imax, float gamma, OTYPE omin, OTYPE omax)
{
	if (!(_lut))
	  throw LibException("CreateGamma(): ASSERT FAILED: NOT(_lut)");

	int iminpos = GetPos(imin);
	int imaxpos = GetPos(imax);

	if (gamma != 1.0)
	{
		// compute gamma correction
		for (int i = 0; i < iminpos; i++)
		_lut[i] = omin;

		for (int i = iminpos; i <= imaxpos; i++)
			_lut[i] = static_cast<OTYPE>(round(powf((float) (GetCenter(i) - imin) / (float) (imax - imin), gamma) * (float) (omax - omin) + omin));

        for (int i = imaxpos + 1; i < _lut_size; i++)
			_lut[i] = omax;
	}
	else
	{
		CreateLinearMapping(imin, imax, omin, omax);
	}
}

/*************************************************************************************/

template <class ITYPE, class OTYPE> void LUT<ITYPE, OTYPE>::Apply(const ITYPE *idata, OTYPE *odata, long size) const 
{
	if (!(_lut))
	  throw LibException("Apply(): ASSERT FAILED: NOT(_lut)");

	for (long i = 0; i < size; ++i) {
		*odata = _lut[GetPos(*idata)];
		++odata; ++idata;
	}
}

/*************************************************************************************/

template <class ITYPE, class OTYPE> void LUT<ITYPE, OTYPE>::Select8Bits(int last)
{
	throw LibException("i3d::LUT<ITYPE,OTYPE>::Select8Bits: This can be used only for LUT<i3d::GRAY16, i3d::GRAY8>!");
}

/*************************************************************************************/

template <> void LUT<i3d::GRAY16,i3d::GRAY8>::Select8Bits(int last)
{
	if (last < 0 || last > 16)
		throw LibException("i3d::LUT<i3d::GRAY16,i3d::GRAY8>::Select8Bits: Selection is out of range!");

	if (last == 0) {
		CreateAffine(0.0, 0.0);
	} else if (last <= 8)
	{
		CreateAffine(float(std::numeric_limits<i3d::GRAY8>::max() / float((0x01 << last) - 1)), 0.0);
	}
	else
	{
		CreateAffine(float(1.0 / float(0x01 << (last - 8))), -1.0); 
	}
}

/*************************************************************************************/

template class I3D_DLLEXPORT LUT<i3d::GRAY8, i3d::GRAY8>;
template class I3D_DLLEXPORT LUT<i3d::GRAY16, i3d::GRAY8>;
template class I3D_DLLEXPORT LUT<i3d::GRAY8, i3d::GRAY16>;
template class I3D_DLLEXPORT LUT<i3d::GRAY16, i3d::GRAY16>;
template class I3D_DLLEXPORT LUT<i3d::GRAY8, i3d::RGB>;
template class I3D_DLLEXPORT LUT<i3d::GRAY16, i3d::RGB>;
template class I3D_DLLEXPORT LUT<size_t, i3d::RGB>;
template class I3D_DLLEXPORT LUT<int, i3d::GRAY8>;
template class I3D_DLLEXPORT LUT<float, i3d::GRAY8>;

/*************************************************************************************/

} // namespace i3d
