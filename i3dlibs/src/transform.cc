/*
 * i3dlib - image manipulation library
 *
 * Copyright (C) 2000-2006   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 * FILE: transform.cc
 *
 * Image transformations
 *
 * Petr Matula (pem@fi.muni.cz)
 */

#ifdef __GNUG__
#pragma implementation
#endif

#include "basic.h"
#include "transform.h"
#include "toolbox.h"

std::string affine_message;
bool
  was_affine_error;

extern
  "C"
{
#include "affine.h"
  void
  errorReport (char *errorMessage)
  {
    affine_message = errorMessage;
    was_affine_error = true;
  }
}


namespace i3d
{
using std::max;

	/// function for memory allocation including memory check
	template <class NUMBER> inline
		void allocate(
		NUMBER *&address,
		unsigned long int count,
		const char *var_name,
		const char *function_name)
	{
		try 
		{
			address = new NUMBER[count];
		}
		catch (std::bad_alloc) 
		{
#ifdef I3D_DEBUG
			std::cerr << function_name << 
				": Cannot allocate continuous memory block for '" << 
				var_name << "'." << std::endl;
#endif
			throw InternalException("I3DLIB: Lack of memory.");
		}
	}

	/// function for memory allocation including memory check
	template <class WHATEVER> inline
		void single_allocate(
		WHATEVER *&address, 
		const char *var_name, 
		const char *function_name)
	{
		try
		{
			address = new WHATEVER();
		}
		catch (std::bad_alloc)
		{
#ifdef I3D_DEBUG
			std::cerr << function_name << ": unable to allocate memory for '" << 
				var_name << "'." << std::endl;
#endif
			throw InternalException("I3DLIB: Lack of memory.");
		}
	}
/***************************************************************************/

template < class VOXEL > void
RemapIntensities (Image3d < VOXEL > &img, std::vector < VOXEL > &map_fun)
  {
    for (size_t i = 0; i < img.GetImageSize (); ++i)
      {
        img.SetVoxel (i, map_fun[img.GetVoxel (i)]);
      }
  }

/***************************************************************************/

  template < class T > inline
  void Resample (const Image3d < T > &src, Image3d < T > &dest,
            const Vector3d < size_t > &v, SamplingMode m)
  {
    Resize (src, dest, v, m);
    Resolution r = src.GetResolution ();
	 dest.SetOffset(src.GetOffset());
    if (r.IsDefined ())
      dest.SetResolution (Vector3d < float >(v) /
                          PixelsToMicrons (src.GetSize (), r));
  }

/***************************************************************************/
  
  template <class T> void Resample(const Image3d<T> &src,
                                          Image3d<T> &dest,
                                          size_t new_x,
                                          size_t new_y,
                                          size_t new_z,
                                          SamplingMode m)
  { 
	  Resample(src,dest,Vector3d<size_t>(new_x,new_y,new_z),m); 
  }

/***************************************************************************/
  
  template <class T> void Resize(const Image3d<T> &src,
                                        Image3d<T> &dest,
                                        size_t new_x,
                                        size_t new_y,
                                        size_t new_z,
                                        SamplingMode m)
  { 
	  Resize(src,dest,Vector3d<size_t>(new_x,new_y,new_z),m); 
  }

/***************************************************************************/
  
template <class T>
void ResizeInMicrons(const Image3d<T> &src,
                              Image3d<T> &dest,
                              float new_x,
                              float new_y,
                              float new_z,
                              SamplingMode m)
  { 
	  ResizeInMicrons(src,dest,Vector3d<float>(new_x,new_y,new_z),m); 
  }

/***************************************************************************/

inline RGB max(const RGB &v1, const RGB &v2)
    {
      RGB v;
      v.red   = std::max(v1.red,v2.red);
      v.green = std::max(v1.green,v2.green);
      v.blue  = std::max(v1.blue,v2.blue);
      return v;
    }
  
template <class T> I3D_DLLEXPORT
void CopySlice(const Image3d<T> &slc, Image3d<T> &img, size_t z)
{
	if (slc.GetSizeX() != img.GetSizeX() || slc.GetSizeY() != img.GetSizeY())
	{

		throw InternalException("CopySlice: slice must have the same xy size as 3d image");
	}
	if (slc.GetSizeZ() + z > img.GetSizeZ())
	{
		std::ostringstream os;
		os << "CopySlice: trying to copy slice out-of-bounds: " <<  slc.GetSizeZ()
			<< " + " << z << " > " << img.GetSizeZ();
		throw InternalException(os.str());
	}

	const T *from = slc.GetFirstVoxelAddr();
	const T *last = from + slc.GetImageSize();
	T *to = img.GetFirstVoxelAddr() + z*img.GetSliceSize();
	for (; from < last; ++from, ++to)
		*to = *from;
}

/***************************************************************************/

  template < class T > inline 
  void Resize (const Image3d < T > &src, Image3d < T > &dest,
          const Vector3d < size_t > &v, SamplingMode m)
  {
    if (m != NEAREST_NEIGHBOUR)
      throw (InternalException ("Resize: Only nearest neighbour algorithm "
                                "implemented"));

    dest.MakeRoom (v.x, v.y, v.z);

    T *
      dest_data = dest.GetFirstVoxelAddr ();

    size_t
      sx = src.GetSizeX ();
    size_t
      sy = src.GetSizeY ();
    size_t
      sz = src.GetSizeZ ();

    for (size_t z = 0; z < v.z; ++z)
      for (size_t y = 0; y < v.y; ++y)
        for (size_t x = 0; x < v.x; ++x)
          {
            *dest_data = src.GetVoxel ((sx * x) / v.x,
                                       (sy * y) / v.y, (sz * z) / v.z);
            ++dest_data;
          }
  }

/***************************************************************************/

  template < class T > inline 
  void ResizeInMicrons (const Image3d < T > &src,
                   Image3d < T > &dest,
                   const Vector3d < float >&size, SamplingMode m)
  {
    Vector3d < size_t > s = MicronsToPixels (size, src.GetResolution ());
    Resize (src, dest, s, m);
  }

/***************************************************************************/

  template < class VOXEL > void
  Get_AF_XY (const Image3d < VOXEL > &src, Image3d < VOXEL > &dest)
  {
    dest.MakeRoom(src.GetSizeX (), src.GetSizeY (), 1);
    dest.SetOffset(Offset(src.GetOffset().x, src.GetOffset().y, 0));
    dest.SetResolution(src.GetResolution());

    const VOXEL *
      src_data = src.GetFirstVoxelAddr ();
    VOXEL *
      dest_data = dest.GetFirstVoxelAddr ();

    size_t max_z = src.GetSizeZ ();
    size_t slice_size = src.GetSliceSize ();

    size_t i;
    VOXEL  max_intensity;

    for (size_t j = 0; j < slice_size; j++)
      {
        i = j;
        max_intensity = src_data[i];
        for (size_t z = 1; z < max_z; z++)
          {
            i += slice_size;
            max_intensity = max (max_intensity, src_data[i]);
          }
        dest_data[j] = max_intensity;
      }
  }

/***************************************************************************/

  template < class VOXEL > void
  Get_AF_XZ (const Image3d < VOXEL > &src, Image3d < VOXEL > &dest)
  {
    dest.MakeRoom(src.GetSizeX(), 1, src.GetSizeZ());
    dest.SetOffset(Offset(src.GetOffset().x, 0, src.GetOffset().z));
    dest.SetResolution(src.GetResolution());

    const VOXEL *src_data = src.GetFirstVoxelAddr ();
    VOXEL *dest_data = dest.GetFirstVoxelAddr ();

    size_t max_y = src.GetSizeY ();
    size_t size_x = src.GetSizeX ();
    size_t slice_size = src.GetSliceSize ();
    size_t new_size = src.GetSizeX () * src.GetSizeZ ();

    size_t i;
    VOXEL max_intensity;

    for (size_t j = 0; j < new_size; j++)
      {
        i = (j / size_x) * slice_size + (j % size_x);
        max_intensity = src_data[i];
        for (size_t y = 1; y < max_y; y++)
          {
            i += size_x;
            max_intensity = max (max_intensity, src_data[i]);
          }
        dest_data[j] = max_intensity;
      }
  }

/***************************************************************************/

  template < class VOXEL > void
  Get_AF_YZ (const Image3d < VOXEL > &src, Image3d < VOXEL > &dest)
  {
    dest.MakeRoom (1, src.GetSizeY (), src.GetSizeZ ());
    dest.SetOffset (Offset(0,src.GetOffset().y,src.GetOffset().z));
    dest.SetResolution (src.GetResolution ());

    const VOXEL *src_data = src.GetFirstVoxelAddr ();
    VOXEL *dest_data = dest.GetFirstVoxelAddr ();

    size_t size_x = src.GetSizeX ();
    size_t slice_size = src.GetSliceSize ();
    size_t size_y = src.GetSizeY ();
    size_t new_size = src.GetSizeY () * src.GetSizeZ ();

    size_t i;
    VOXEL  max_intensity;

    for (size_t j = 0; j < new_size; j++)
      {
        i = (j % size_y) * size_x + (j / size_y) * slice_size;
        max_intensity = src_data[i];
        for (size_t x = 1; x < size_x; x++)
          {
            i++;
            max_intensity = max (max_intensity, src_data[i]);
          }
        dest_data[j] = max_intensity;
      }
  }

  /***************************************************************************/

  template <class LABEL, class VOXEL> void
	  Get_Labeled_AF_XY(const LabeledImage3d<LABEL, VOXEL> &src, Image3d<LABEL> &dest, 
	                    const bool vis[], bool only_boundary)
  {
	  dest.MakeRoom(src.GetSizeX(), src.GetSizeY(), 1);
	  dest.SetOffset(Offset(src.GetOffset().x, src.GetOffset().y, 0));
	  dest.SetResolution(src.GetResolution());

	  if (only_boundary)
	  {
		  #if defined(__GNUG__) && __GNUG__ < 3
				typename map<size_t, ComponentInfo<VOXEL> >::const_iterator it;
		  #else
				typename ComponentInfo<VOXEL>::Container::const_iterator it;
		  #endif

          // Clear image first
          for (size_t i = dest.GetImageSize(); i > 0; i--)
              dest.SetVoxel (i - 1, 0);

		  i3d::Image3d<LABEL> tmp;
		  VOI<PIXELS> voi;
          LABEL label, val, bnd_mask = std::numeric_limits<LABEL>::max() / 2 + 1;

		  it = src.components.begin();
		  for (; it != src.components.end(); it++)
		  {
			  if (it->first != LABEL(0) && vis[it -> first])
			  {
                  voi = it -> second.voi;
				  val = it -> first | bnd_mask;

				  tmp.MakeRoom(voi.size.x, voi.size.y, 1);
				  LABEL *tmp_data = tmp.GetFirstVoxelAddr();

				  for (size_t y = 0; y < voi.size.y; y++)
				  {
					  for (size_t x = 0; x < voi.size.x; x++)
					  {
						  label = 0;
						  
						  for (size_t z = 0; z < voi.size.z; z++)
						  {
							  if (src.GetVoxel(voi.offset.x + x, voi.offset.y + y, voi.offset.z + z) == it -> first)
							  {
								  label = it -> first;
								  break;
							  }
						  }

						  tmp_data[y * voi.size.x + x] = label;

						  if (x == 0 || y == 0)
						  {
							  if (label != LABEL(0))
							  {
								  dest.SetVoxel(voi.offset.x + x, voi.offset.y + y, 0, val);
							  }
						  }
						  else
						  {
							  if (tmp_data[y * voi.size.x + x - 1] != label)
							  {
								  if (label == LABEL(0))
								  {
									  dest.SetVoxel(voi.offset.x + x - 1, voi.offset.y + y, 0, val);
								  }
								  else
								  {
									  dest.SetVoxel(voi.offset.x + x, voi.offset.y + y, 0, val);
								  }
							  }

							  if (tmp_data[(y - 1) * voi.size.x + x] != label)
							  {
								  if (label == LABEL(0))
								  {
									  dest.SetVoxel(voi.offset.x + x, voi.offset.y + y - 1, 0, val);
								  }
								  else
								  {
									  dest.SetVoxel(voi.offset.x + x, voi.offset.y + y, 0, val);
								  }
							  }

							  if (x == (voi.size.x - 1) || y == (voi.size.y - 1))
							  {
								  if (label != LABEL(0))
								  {
									  dest.SetVoxel(voi.offset.x + x, voi.offset.y + y, 0, val);
								  }
							  }
						  }
					  }
				  } 
			  }
		  }
	  }
	  else
	  {
		  const LABEL *src_data = src.GetFirstVoxelAddr();
		  LABEL *dest_data = dest.GetFirstVoxelAddr();

		  size_t max_z = src.GetSizeZ();
		  size_t slice_size = src.GetSliceSize();

		  size_t i;
		  LABEL label;

		  for (size_t j = 0; j < slice_size; j++)
		  {
			  i = j;
			  label = 0;

			  for (size_t z = 0; z < max_z; z++)
			  {
				  if (vis[src_data[i]])
				  {
					  label = src_data[i];
					  break;
				  }

				  i += slice_size;
			  }

			  dest_data[j] = label;
		  }
	  }
  }

/***************************************************************************/

  template <class LABEL, class VOXEL> void
  Get_Labeled_AF_XZ(const LabeledImage3d<LABEL, VOXEL> &src, Image3d<LABEL> &dest, 
	                    const bool vis[], bool only_boundary)
  {
	  dest.MakeRoom(src.GetSizeX(), 1, src.GetSizeZ());
	  dest.SetOffset(Offset(src.GetOffset().x, 0, src.GetOffset().z));
	  dest.SetResolution(src.GetResolution());

	  if (only_boundary)
	  {
		  #if defined(__GNUG__) && __GNUG__ < 3
				typename map<size_t, ComponentInfo<VOXEL> >::const_iterator it;
		  #else
				typename ComponentInfo<VOXEL>::Container::const_iterator it;
		  #endif

          // Clear image first
          for (size_t i = dest.GetImageSize(); i > 0; i--)
              dest.SetVoxel (i - 1, 0);

          i3d::Image3d<LABEL> tmp;
		  VOI<PIXELS> voi;
		  LABEL label, val, bnd_mask = std::numeric_limits<LABEL>::max() / 2 + 1;
		  
		  it = src.components.begin();
		  for (; it != src.components.end(); it++)
		  {
			  if (it->first != LABEL(0) && vis[it -> first])
			  {
				  voi = it -> second.voi;
				  val = it -> first | bnd_mask;

				  tmp.MakeRoom(voi.size.x, 1, voi.size.z);				  
				  LABEL *tmp_data = tmp.GetFirstVoxelAddr();

				  for (size_t z = 0; z < voi.size.z; z++)
				  {
					  for (size_t x = 0; x < voi.size.x; x++)
					  {
						  label = 0;
						  
						  for (size_t y = 0; y < voi.size.y; y++)
						  {
							  if (src.GetVoxel(voi.offset.x + x, voi.offset.y + y, voi.offset.z + z) == it -> first)
							  {
								  label = it -> first;
								  break;
							  }
						  }

						  tmp_data[z * voi.size.x + x] = label;

						  if (x == 0 || z == 0)
						  {
							  if (label != LABEL(0))
							  {
								  dest.SetVoxel(voi.offset.x + x, 0, voi.offset.z + z, val);
							  }
						  }
						  else
						  {
							  if (tmp_data[z * voi.size.x + x - 1] != label)
							  {
								  if (label == LABEL(0))
								  {
									  dest.SetVoxel(voi.offset.x + x - 1, 0, voi.offset.z + z, val);
								  }
								  else
								  {
									  dest.SetVoxel(voi.offset.x + x, 0, voi.offset.z + z, val);
								  }
							  }

							  if (tmp_data[(z - 1) * voi.size.x + x] != label)
							  {
								  if (label == LABEL(0))
								  {
									  dest.SetVoxel(voi.offset.x + x, 0, voi.offset.z + z - 1, val);
								  }
								  else
								  {
									  dest.SetVoxel(voi.offset.x + x, 0, voi.offset.z + z, val);
								  }
							  }

							  if (x == (voi.size.x - 1) || z == (voi.size.z - 1))
							  {
								  if (label != LABEL(0))
								  {
									  dest.SetVoxel(voi.offset.x + x, 0, voi.offset.z + z, val);
								  }
							  }
						  }
					  }
				  }
			  }
		  }
	  }
	  else
	  {
		  const LABEL *src_data = src.GetFirstVoxelAddr();
		  LABEL *dest_data = dest.GetFirstVoxelAddr();

		  size_t max_y = src.GetSizeY();
		  size_t size_x = src.GetSizeX();
		  size_t slice_size = src.GetSliceSize();
		  size_t new_size = src.GetSizeX() * src.GetSizeZ();

		  size_t i;
		  LABEL label;

		  for (size_t j = 0; j < new_size; j++)
		  {
			  i = (j / size_x) * slice_size + (j % size_x);
			  label = 0;

			  for (size_t y = 0; y < max_y; y++)
			  {
				  if (vis[src_data[i]])
				  {
					  label = src_data[i];
					  break;
				  }

				  i += size_x;
			  }

			  dest_data[j] = label;
		  }
	  }
  }

/***************************************************************************/

  template <class LABEL, class VOXEL> void
	  Get_Labeled_AF_YZ(const LabeledImage3d<LABEL, VOXEL> &src, Image3d<LABEL> &dest, 
	                    const bool vis[], bool only_boundary)
  {
	  dest.MakeRoom(1, src.GetSizeY(), src.GetSizeZ());
	  dest.SetOffset(Offset(0,src.GetOffset().y,src.GetOffset().z));
	  dest.SetResolution(src.GetResolution());

	  if (only_boundary)
	  {
		  #if defined(__GNUG__) && __GNUG__ < 3
				typename map<size_t, ComponentInfo<VOXEL> >::const_iterator it;
		  #else
				typename ComponentInfo<VOXEL>::Container::const_iterator it;
		  #endif

          // Clear image first
          for (size_t i = dest.GetImageSize(); i > 0; i--)
              dest.SetVoxel (i - 1, 0);

          i3d::Image3d<LABEL> tmp;
		  VOI<PIXELS> voi;
		  LABEL label, val, bnd_mask = std::numeric_limits<LABEL>::max() / 2 + 1;
		  
		  it = src.components.begin();
		  for (; it != src.components.end(); it++)
		  {
			  if (it->first != LABEL(0) && vis[it -> first])
			  {
				  voi = it -> second.voi;
				  val = it -> first | bnd_mask;

				  tmp.MakeRoom(1, voi.size.y, voi.size.z);
				  LABEL *tmp_data = tmp.GetFirstVoxelAddr();

				  for (size_t z = 0; z < voi.size.z; z++)
				  {
					  for (size_t y = 0; y < voi.size.y; y++)
					  {
						  label = 0;
						  
						  for (size_t x = 0; x < voi.size.x; x++)
						  {
							  if (src.GetVoxel(voi.offset.x + x, voi.offset.y + y, voi.offset.z + z) == it -> first)
							  {
								  label = it -> first;
								  break;
							  }
						  }

						  tmp_data[z * voi.size.y + y] = label;

						  if (y == 0 || z == 0)
						  {
							  if (label != LABEL(0))
							  {
								  dest.SetVoxel(0, voi.offset.y + y, voi.offset.z + z, val);
							  }
						  }
						  else
						  {
							  if (tmp_data[z * voi.size.y + y - 1] != label)
							  {
								  if (label == LABEL(0))
								  {
									  dest.SetVoxel(0, voi.offset.y + y - 1, voi.offset.z + z, val);
								  }
								  else
								  {
									  dest.SetVoxel(0, voi.offset.y + y, voi.offset.z + z, val);
								  }
							  }

							  if (tmp_data[(z - 1) * voi.size.y + y] != label)
							  {
								  if (label == LABEL(0))
								  {
									  dest.SetVoxel(0, voi.offset.y + y, voi.offset.z + z - 1, val);
								  }
								  else
								  {
									  dest.SetVoxel(0, voi.offset.y + y, voi.offset.z + z, val);
								  }
							  }

							  if (y == (voi.size.y - 1) || z == (voi.size.z - 1))
							  {
								  if (label != LABEL(0))
								  {
									  dest.SetVoxel(0, voi.offset.y + y, voi.offset.z + z, val);
								  }
							  }
						  }
					  }
				  }
			  }
		  }
	  }
	  else
	  {
		  const LABEL *src_data = src.GetFirstVoxelAddr();
		  LABEL *dest_data = dest.GetFirstVoxelAddr();

		  size_t size_x = src.GetSizeX();
		  size_t slice_size = src.GetSliceSize();
		  size_t size_y = src.GetSizeY();
		  size_t new_size = src.GetSizeY() * src.GetSizeZ();

		  size_t i;
		  LABEL label;

		  for (size_t j = 0; j < new_size; j++)
		  {
			  i = (j % size_y) * size_x + (j / size_y) * slice_size;
			  label = 0;

			  for (size_t x = 0; x < size_x; x++)
			  {
				  if (vis[src_data[i]])
				  {
					  label = src_data[i];
					  break;
				  }

				  i++;
			  }

			  dest_data[j] = label;
		  }
	  }
  }

/***************************************************************************/

  int ComputeFrom(float soff, float doff, float dres)
  {
	  int m = (int) round((soff - doff) * dres);
	  return std::max<int>(m, 0);
  }

  int ComputeTo(float soff, float sres, float ssz, int dsz, float doff, float dres)
  {
	  int m = (int) ceil((ssz / sres + soff - doff) * dres);
	  return std::min<int>(m, dsz);
  }

template <class VOXEL> 
void CopyToImage(const i3d::Image3d<VOXEL> &src, i3d::Image3d<VOXEL> &dst)
{
	int from_x = ComputeFrom(src.GetOffset().x, dst.GetOffset().x, dst.GetResolution().GetRes().x);
	int from_y = ComputeFrom(src.GetOffset().y, dst.GetOffset().y, dst.GetResolution().GetRes().y);
	int from_z = ComputeFrom(src.GetOffset().z, dst.GetOffset().z, dst.GetResolution().GetRes().z);
	
	int to_x   = ComputeTo(src.GetOffset().x, src.GetResolution().GetRes().x, 
		                   src.GetSizeX(), dst.GetSizeX(),
		                   dst.GetOffset().x, dst.GetResolution().GetRes().x);
	int to_y   = ComputeTo(src.GetOffset().y, src.GetResolution().GetRes().y, 
		                   src.GetSizeY(), dst.GetSizeY(),
		                   dst.GetOffset().y, dst.GetResolution().GetRes().y);
	int to_z   = ComputeTo(src.GetOffset().z, src.GetResolution().GetRes().z,
		                   src.GetSizeZ(), dst.GetSizeZ(),
		                   dst.GetOffset().z, dst.GetResolution().GetRes().z);

	float dx = src.GetResolution().GetRes().x / dst.GetResolution().GetRes().x;
	float dy = src.GetResolution().GetRes().y / dst.GetResolution().GetRes().y;
	float dz = src.GetResolution().GetRes().z / dst.GetResolution().GetRes().z;

	float ini_x = /*0.5*dx + */(dst.GetOffset().x - src.GetOffset().x)*src.GetResolution().GetRes().x;
	float ini_y = /*0.5*dy + */(dst.GetOffset().y - src.GetOffset().y)*src.GetResolution().GetRes().y;
	float ini_z = /*0.5*dz +*/ (dst.GetOffset().z - src.GetOffset().z)*src.GetResolution().GetRes().z;

	float fx, fy, fz;

	fz = ini_z;
	VOXEL *dstptr;
	for (int z = from_z; z < to_z; ++z)
	{
		fy = ini_y;
		for (int y = from_y; y < to_y; ++y)
		{
			fx = ini_x;
			dstptr = dst.GetVoxelAddr(from_x, y, z);

			for (int x = from_x; x < to_x; ++x)
			{
				*dstptr = src.GetVoxel(size_t(fx), size_t(fy), size_t(fz));
				fx += dx;
				++dstptr;
			}
			fy += dy;
		}
		fz += dz;
	}
}

/***************************************************************************/

template <class VOXEL> I3D_DLLEXPORT
void GetResampledSubimage(const Image3d<VOXEL> &src, Image3d<VOXEL> &dest,
						  const VOI<PIXELS> &ivoi, const Vector3d<size_t> &size)
{
	dest.MakeRoom(size);
	dest.SetOffset(PixelsToMicrons(ivoi.offset, src.GetResolution()));
	dest.SetResolution((Vector3d<float>(size)/Vector3d<float>(ivoi.size)) * src.GetResolution().GetRes());

	// TODO: optimization of the code for the flat vois
	// TODO: optimization of the code for 3D vois

	VOXEL* dst = dest.GetFirstVoxelAddr();
	float srcdx = float(ivoi.size.x) / float(size.x);
	float srcdy = float(ivoi.size.y) / float(size.y);
	float srcdz = float(ivoi.size.z) / float(size.z);

	float srcx, srcy, srcz;

	srcz = float(ivoi.offset.z);
	// general in 3d 
	for (size_t z = 0; z < size.z; ++z)
	{
		srcy = float(ivoi.offset.y);
		for (size_t y = 0; y < size.y; ++y)
		{
			srcx = float(ivoi.offset.x);
			for (size_t x = 0; x < size.x; ++x)
			{
				*dst = src.GetVoxel(size_t(srcx), size_t(srcy), size_t(srcz));
				++dst;
				srcx += srcdx;
			}
			srcy += srcdy;
		}
		srcz += srcdz;
	}
}

/***************************************************************************/

  template <class VOXEL> float *
  ImgToFloat (Image3d < VOXEL > &im)
  {
    size_t
      imsz = im.GetImageSize ();

    float *
      fi = new float[imsz];
    VOXEL *
      vd = im.GetFirstVoxelAddr ();
    for (size_t i = 0; i < imsz; i++)
      fi[i] = vd[i];
    return fi;
  }

/***************************************************************************/

  template <class VOXEL> void
  FloatToImg (Image3d < VOXEL > &im, float *fi)
  {
    size_t
      imsz = im.GetImageSize ();

    VOXEL *
      vd = im.GetFirstVoxelAddr ();
    for (size_t i = 0; i < imsz; i++)
      vd[i] = VOXEL (fi[i]);
  }

/***************************************************************************/

  template <class VOXEL> void
  AffineTransform (Image3d < VOXEL > &img, const double matrix[4][4],
                   const Vector3d < double >&origin, int degree, VOXEL bgcol,
                   bool use_resolution)
  {
    float *
      fin = ImgToFloat (img);
    float *
      fout = new float[img.GetImageSize ()];
    double
      M[4][4];

    if (use_resolution)
      {
        double
          rx = img.GetResolution ().GetX ();
        double
          ry = img.GetResolution ().GetY ();
        double
          rz = img.GetResolution ().GetZ ();
        M[0][0] = matrix[0][0];
        M[0][1] = matrix[0][1] * rx / ry;
        M[0][2] = matrix[0][2] * rx / rz;
        M[0][3] = matrix[0][3] * rx;

        M[1][0] = matrix[1][0] * ry / rx;
        M[1][1] = matrix[1][1];
        M[1][2] = matrix[1][2] * ry / rz;
        M[1][3] = matrix[1][3] * ry;

        M[2][0] = matrix[2][0] * rz / rx;
        M[2][1] = matrix[2][1] * rz / ry;
        M[2][2] = matrix[2][2];
        M[2][3] = matrix[2][3] * rz;

        M[3][0] = matrix[3][0]; //0
        M[3][1] = matrix[3][1]; //0
        M[3][2] = matrix[3][2]; //0
        M[3][3] = matrix[3][3]; //1
      }
    else
      {
        // just copy matrix
        M[0][0] = matrix[0][0];
        M[0][1] = matrix[0][1];
        M[0][2] = matrix[0][2];
        M[0][3] = matrix[0][3];

        M[1][0] = matrix[1][0];
        M[1][1] = matrix[1][1];
        M[1][2] = matrix[1][2];
        M[1][3] = matrix[1][3];

        M[2][0] = matrix[2][0];
        M[2][1] = matrix[2][1];
        M[2][2] = matrix[2][2];
        M[2][3] = matrix[2][3];

        M[3][0] = matrix[3][0];
        M[3][1] = matrix[3][1];
        M[3][2] = matrix[3][2];
        M[3][3] = matrix[3][3];
      }

    double
      org[3];
    org[0] = origin.x;
    org[1] = origin.y;
    org[2] = origin.z;

    affineTransform (M, org, fin, fout, img.GetSizeX (), img.GetSizeY (),
                     img.GetSizeZ (), degree, float (bgcol));
    delete[]fin;

    FloatToImg (img, fout);
    delete[]fout;
    if (was_affine_error)
      throw
      InternalException (affine_message);
  }

/***************************************************************************/

template <class T2,class PRED> 
  void MapCondColor(const Image3d<T2> &Iin,Image3d<T2> &Iout,PRED pred,T2 color,const int direction) {
	signed int x=0,y=0,z=0;
	signed int start=0,end=0,inc=0;
	signed int x_a=0,x_b=0,y_a=0,y_b=0;

	switch (direction) {
	//case direction:
		//x,y dimension of output image/plane
		//start,end,inc describing each scanning line
		//x_[ab],y_[ab] linear (a*x+b) transformations from scanning planes to output plane
		//break;

	case MAP_DIRECTION_LEFT_RIGHT:	//x-coordinate will increase
		x=Iin.GetSizeY(); y=Iin.GetSizeZ(); //y must be inversed
		start=0; end=Iin.GetSizeX() -1; inc=1;
		x_a=1; x_b=0; y_a=-1; y_b=y;
		break;

	case MAP_DIRECTION_RIGHT_LEFT:	//x-coordinate will decrease
		x=Iin.GetSizeY(); y=Iin.GetSizeZ(); //x,y must be inversed
		start=Iin.GetSizeX() -1; end=0; inc=-1;
		x_a=-1; x_b=x; y_a=-1; y_b=y;
		break;

	case MAP_DIRECTION_FRONT_REAR:	//y-coordinate will decrease
		x=Iin.GetSizeX(); y=Iin.GetSizeZ(); //y must be inversed
		start=Iin.GetSizeY() -1; end=0; inc=-1;
		x_a=1; x_b=0; y_a=-1; y_b=y;
		break;

	case MAP_DIRECTION_REAR_FRONT:	//y-coordinate will increase
		x=Iin.GetSizeX(); y=Iin.GetSizeZ(); //x,y must be inversed
		start=0; end=Iin.GetSizeY() -1; inc=1;
		x_a=-1; x_b=x; y_a=-1; y_b=y;
		break;

	case MAP_DIRECTION_BOTTOM_TOP:	//z-coordinate will increase
		x=Iin.GetSizeX(); y=Iin.GetSizeY(); //y must be inversed
		start=0; end=Iin.GetSizeZ() -1; inc=1;
		x_a=1; x_b=0; y_a=-1; y_b=y;
		break;

	case MAP_DIRECTION_TOP_BOTTOM:	//z-coordinate will decrease
		x=Iin.GetSizeX(); y=Iin.GetSizeY(); //none must be inversed
		start=Iin.GetSizeZ() -1; end=0; inc=-1;
		x_a=1; x_b=0; y_a=1; y_b=0;
		break;
	}

	Iout.MakeRoom(x,y,1);		//"only" 2D image

	switch (direction) {
	case MAP_DIRECTION_LEFT_RIGHT:
	case MAP_DIRECTION_RIGHT_LEFT:
		for (z=0; z < (signed int)Iin.GetSizeZ(); z++)
			for (y=0; y < (signed int)Iin.GetSizeY(); y++) {
				x=start;
				while (((inc*x) <= end) && (pred(Iin.GetVoxel(x,y,z)))) x+=inc;

				if ((inc*x) > end) Iout.SetVoxel(x_a*y +x_b,y_a*z +y_b,0,color);
				else               Iout.SetVoxel(x_a*y +x_b,y_a*z +y_b,0,Iin.GetVoxel(x,y,z));
			}
		break;

	case MAP_DIRECTION_FRONT_REAR:
	case MAP_DIRECTION_REAR_FRONT:
		for (z=0; z < (signed int)Iin.GetSizeZ(); z++)
			for (x=0; x < (signed int)Iin.GetSizeX(); x++) {
				y=start;
				while (((inc*y) <= end) && (pred(Iin.GetVoxel(x,y,z)))) y+=inc;

				if ((inc*y) > end) Iout.SetVoxel(x_a*x +x_b,y_a*z +y_b,0,color);
				else               Iout.SetVoxel(x_a*x +x_b,y_a*z +y_b,0,Iin.GetVoxel(x,y,z));
			}
		break;

	case MAP_DIRECTION_BOTTOM_TOP:
	case MAP_DIRECTION_TOP_BOTTOM:
		for (y=0; y < (signed int)Iin.GetSizeY(); y++)
			for (x=0; x < (signed int)Iin.GetSizeX(); x++) {
				z=start;
				while (((inc*z) <= end) && (pred(Iin.GetVoxel(x,y,z)))) z+=inc;

				if ((inc*z) > end) Iout.SetVoxel(x_a*x +x_b,y_a*y +y_b,0,color);
				else               Iout.SetVoxel(x_a*x +x_b,y_a*y +y_b,0,Iin.GetVoxel(x,y,z));
			}
		break;
	}
}

/***************************************************************************/

/** Added by Vladimir Ulman (xulman@fi.muni.cz), 3.12.2004 */
  template < class T1, class T2 > void
  MapColor (const Image3d < T1 > &Iselect, const Image3d < T2 > &Imask,
            Image3d < RGB > &Iout, const std::vector < RGB > &rgb_map)
  {
    if ((Iselect.GetWidth () != Imask.GetWidth ()) ||
        (Iselect.GetHeight () != Imask.GetHeight ()) ||
        (Iselect.GetNumSlices () != Imask.GetNumSlices ()))
      {
        throw
        InternalException
          ("MapColor: Input images are not of the same dimensions.");
      }

    Iout.MakeRoom (Iselect.GetSize ());

    const T1 *
      select = Iselect.GetFirstVoxelAddr ();
    const T2 *
      mask = Imask.GetFirstVoxelAddr ();
    RGB *
      out = Iout.GetFirstVoxelAddr ();

    int
      mask_interval =
      (int) std::numeric_limits < T2 >::max () - (int) std::numeric_limits < T2 >::min ();

    size_t
      i;
    for (i = 0; i < Iselect.GetImageSize (); i++)
      {
        int
          intensity = (int) (*mask) - (int) std::numeric_limits < T2 >::min ();
        intensity = (intensity << 8) / mask_interval;

        (*out).red =
          (intensity * (int) (rgb_map[*select % rgb_map.size ()].red)) >> 8;
        (*out).green =
          (intensity * (int) (rgb_map[*select % rgb_map.size ()].green)) >> 8;
        (*out).blue =
          (intensity * (int) (rgb_map[*select % rgb_map.size ()].blue)) >> 8;

        select++;
        mask++;
        out++;
      }
  }

/***************************************************************************/

inline double sinc(double x)
{
	if(x==0.0) return 1.0;
	return sin(M_PI*x)/(M_PI*x);
}

inline double lanczos_windowed_sinc(double x,int n)
{
	if(x==0.0) return 1.0;
	if(x<=-n || x>=n)	return 0.0;
	return sinc(x)*sinc(x/n);
}

/***************************************************************************/

template <class VOXEL> 
void lanczos_resample(
		const Image3d<VOXEL> *input,
		Image3d<VOXEL> *&output,
		const float factor[3],
		const int n){
	lanczos_resample(input,output,Vector3d<float>(factor[0],factor[1],factor[2]),n);
}

template <class VOXEL> 
void lanczos_resample(
		const Image3d<VOXEL> *input,
		Image3d<VOXEL> *&output,
		const Vector3d<float> factor,
		const int n){
	Image3d<float> *tmp1=0,*tmp2=0;
	Vector3d<int>  size,new_size;
	long double    sum=0.0;
	double         *weights=0,c_real,c;
	int            x,y,z,i,length,c_int,read_position;

	if(factor.x<=0.0 || factor.y<=0.0 || factor.z<=0.0 || n<1){
		std::cerr<<"lanczos_resample: Wrong parameters!"<<std::endl;
		exit(1);
	}

	size=input->GetSize();
	new_size.x=static_cast<int>(round(size.x*factor.x));
	new_size.y=static_cast<int>(round(size.y*factor.y));
	new_size.z=static_cast<int>(round(size.z*factor.z));

	single_allocate(tmp1,"tmp1","lanczos_resample");
	tmp1->MakeRoom(new_size.x,size.y,size.z);

	length=2*n+3;
	weights=new double[length];

	for(x=0;x<new_size.x;++x){
		c=(1.0/factor.x-1.0)/2.0+x/factor.x;
		c_int=static_cast<int>(floor(c));
		c_real=c-c_int;

		sum=0.0;
		for(i=0;i<length;++i){
			weights[i]=lanczos_windowed_sinc(i-(length/2)-c_real,n);
			sum+=weights[i];
		}

		for(i=0;i<length;++i){
			weights[i]/=sum;
		}

		for(y=0;y<size.y;++y)
			for(z=0;z<size.z;++z){
				sum=0.0;
				for(i=0;i<length;++i){
					read_position=c_int+i-length/2;

					if(read_position<0 || read_position>(size.x-1)){
						if(read_position<0)
							read_position=0;
						if(read_position>(size.x-1))
							read_position=size.x-1;
					}

					sum+=input->GetVoxel(read_position,y,z)*weights[i];
				}
				tmp1->SetVoxel(x,y,z,static_cast<float>(sum));
			}
	}

	single_allocate(tmp2,"tmp2","lanczos_resample");
	tmp2->MakeRoom(new_size.x,new_size.y,size.z);

	for(y=0;y<new_size.y;++y){
		c=(1.0/factor.y-1.0)/2.0+y/factor.y;
		c_int=static_cast<int>(floor(c));
		c_real=c-c_int;

		sum=0.0;
		for(i=0;i<length;++i){
			weights[i]=lanczos_windowed_sinc(i-(length/2)-c_real,n);
			sum+=weights[i];
		}

		for(i=0;i<length;++i){
			weights[i]/=sum;
		}

		for(x=0;x<new_size.x;++x)
			for(z=0;z<size.z;++z){
				sum=0.0;
				for(i=0;i<length;++i){
					read_position=c_int+i-length/2;

					if(read_position<0 || read_position>(size.y-1)){
						if(read_position<0)
							read_position=0;
						if(read_position>(size.y-1))
							read_position=size.y-1;
					}

					sum+=tmp1->GetVoxel(x,read_position,z)*weights[i];
				}
				tmp2->SetVoxel(x,y,z,static_cast<float>(sum));
			}
	}

	delete tmp1;

	if(output==0)
		single_allocate(output,"output","lanczos_resample");
	output->MakeRoom(new_size);
	output->SetOffset(input->GetOffset());
	output->SetResolution(Resolution(input->GetResolution().GetRes()*factor));

	for(z=0;z<new_size.z;++z){
		c=(1.0/factor.z-1.0)/2.0+z/factor.z;
		c_int=static_cast<int>(floor(c));
		c_real=c-c_int;

		sum=0.0;
		for(i=0;i<length;++i){
			weights[i]=lanczos_windowed_sinc(i-(length/2)-c_real,n);
			sum+=weights[i];
		}

		for(i=0;i<length;++i){
			weights[i]/=sum;
		}

		for(x=0;x<new_size.x;++x)
			for(y=0;y<new_size.y;++y){
				sum=0.0;
				for(i=0;i<length;++i){
					read_position=c_int+i-length/2;

					if(read_position<0 || read_position>(size.z-1)){
						if(read_position<0)
							read_position=0;
						if(read_position>(size.z-1))
							read_position=size.z-1;
					}

					sum+=tmp2->GetVoxel(x,y,read_position)*weights[i];
				}

				if (sum>std::numeric_limits<VOXEL>::max())
					 sum=std::numeric_limits<VOXEL>::max();

				if (sum<std::numeric_limits<VOXEL>::min())
					 sum=std::numeric_limits<VOXEL>::min();

				if (std::numeric_limits<VOXEL>::is_integer)
					output->SetVoxel(x,y,z,static_cast<VOXEL>(round(sum)));
				else
					output->SetVoxel(x,y,z,static_cast<VOXEL>(sum));
			}
	}


	delete tmp2;
}

//-------------------------------------------------------------------------

template <class VOXEL> 
void ResampleToDesiredResolution (Image3d<VOXEL> &img, 
											 const Resolution &res, 
										    SamplingMode m)
{
	 Vector3d<float> vec = res.GetRes();

	 ResampleToDesiredResolution(img, vec.x, vec.y, vec.z, m);
}

//-------------------------------------------------------------------------
	
template < class VOXEL >
void ResampleToDesiredResolution (Image3d < VOXEL > &image, 
											 float xRes, 
											 float yRes, 
											 float zRes, 
											 SamplingMode m)
{
  Image3d < VOXEL > *newImage;

  switch (m)
    {
    case NEAREST_NEIGHBOUR:
      {
        Resolution actualRes = image.GetResolution ();
        Resolution newRes = Resolution (xRes, yRes, zRes);

        Vector3d < size_t > actualSize = image.GetSize ();

        Vector3d < size_t > newSize =
          Vector3d < size_t > (static_cast < size_t >
                               (round
                                ((static_cast < float >(actualSize.x) /
                                  actualRes.GetRes().x)*newRes.GetRes().x)),
                               static_cast < size_t >
                               (round
                                ((static_cast < float >(actualSize.y) /
                                  actualRes.GetRes().y)*newRes.GetRes().y)),
                               static_cast < size_t >
                               (round
                                ((static_cast < float >(actualSize.z) /
                                  actualRes.GetRes().z)*newRes.GetRes().z)));

        newImage = new Image3d < VOXEL > ();
        newImage->MakeRoom (newSize);
        newImage->SetResolution (newRes);

        Resample (image, *newImage, newSize.x, newSize.y, newSize.z);
      }
      break;
    case LANCZOS:
      {
        Vector3d < float >res = image.GetResolution ().GetRes ();

        Vector3d < float >factor (xRes / res.x, yRes / res.y, zRes / res.z);
        newImage = NULL;

        lanczos_resample (&image, newImage, factor, 3);
      }
      break;
    default:
      break;
    }

  image = *newImage;

  delete newImage;
}

/***************************************************************************/

// explicit instantiations
  template I3D_DLLEXPORT void
  Resample (const Image3d < float >&, Image3d < float >&, size_t, size_t, size_t , SamplingMode);
  template I3D_DLLEXPORT void
  Resample (const Image3d < GRAY8 > &, Image3d < GRAY8 > &, size_t, size_t, size_t , SamplingMode);
template I3D_DLLEXPORT void
  Resample (const Image3d < GRAY16 > &, Image3d < GRAY16 > &, size_t, size_t, size_t , SamplingMode);
  template I3D_DLLEXPORT void
  Resample (const Image3d < RGB > &, Image3d < RGB > &, size_t, size_t, size_t , SamplingMode);
  template I3D_DLLEXPORT void
  Resample (const Image3d < RGB16 > &, Image3d < RGB16 > &, size_t, size_t, size_t , SamplingMode);

  template I3D_DLLEXPORT void
  Get_AF_XY (const Image3d < RGB > &src, Image3d < RGB > &dest);
  template I3D_DLLEXPORT void
  Get_AF_XZ (const Image3d < RGB > &src, Image3d < RGB > &dest);
  template I3D_DLLEXPORT void
  Get_AF_YZ (const Image3d < RGB > &src, Image3d < RGB > &dest);
  template I3D_DLLEXPORT void
  Get_AF_XY (const Image3d < RGB16 > &src, Image3d < RGB16 > &dest);
  template I3D_DLLEXPORT void
  Get_AF_XZ (const Image3d < RGB16 > &src, Image3d < RGB16 > &dest);
  template I3D_DLLEXPORT void
  Get_AF_YZ (const Image3d < RGB16 > &src, Image3d < RGB16 > &dest);
  template I3D_DLLEXPORT void
  Get_AF_XY (const Image3d < GRAY8 > &src, Image3d < GRAY8 > &dest);
  template I3D_DLLEXPORT void
  Get_AF_XZ (const Image3d < GRAY8 > &src, Image3d < GRAY8 > &dest);
  template I3D_DLLEXPORT void
  Get_AF_YZ (const Image3d < GRAY8 > &src, Image3d < GRAY8 > &dest);
  template I3D_DLLEXPORT void
  Get_AF_XY (const Image3d < GRAY16 > &src, Image3d < GRAY16 > &dest);
  template I3D_DLLEXPORT void
  Get_AF_XZ (const Image3d < GRAY16 > &src, Image3d < GRAY16 > &dest);
  template I3D_DLLEXPORT void
  Get_AF_YZ (const Image3d < GRAY16 > &src, Image3d < GRAY16 > &dest);
  template I3D_DLLEXPORT void
  Get_AF_XY (const Image3d < BINARY > &src, Image3d < BINARY > &dest);
  template I3D_DLLEXPORT void
  Get_AF_XZ (const Image3d < BINARY > &src, Image3d < BINARY > &dest);
  template I3D_DLLEXPORT void
  Get_AF_YZ (const Image3d < BINARY > &src, Image3d < BINARY > &dest);
  template I3D_DLLEXPORT void
  Get_AF_XY (const Image3d < float >&src, Image3d < float >&dest);
  template I3D_DLLEXPORT void
  Get_AF_XZ (const Image3d < float >&src, Image3d < float >&dest);
  template I3D_DLLEXPORT void
  Get_AF_YZ (const Image3d < float >&src, Image3d < float >&dest);
  template I3D_DLLEXPORT void
  Get_AF_XY (const Image3d < int >&src, Image3d < int >&dest);
  template I3D_DLLEXPORT void
  Get_AF_XZ (const Image3d < int >&src, Image3d < int >&dest);
  template I3D_DLLEXPORT void
  Get_AF_YZ (const Image3d < int >&src, Image3d < int >&dest);

  template I3D_DLLEXPORT void
  Get_Labeled_AF_XY(const LabeledImage3d<size_t, GRAY8> &src, Image3d<size_t> &dest, 
                    const bool vis[], bool only_boundary);
  template I3D_DLLEXPORT void
  Get_Labeled_AF_XZ(const LabeledImage3d<size_t, GRAY8> &src, Image3d<size_t> &dest, 
                    const bool vis[], bool only_boundary);
  template I3D_DLLEXPORT void
  Get_Labeled_AF_YZ(const LabeledImage3d<size_t, GRAY8> &src, Image3d<size_t> &dest, 
                    const bool vis[], bool only_boundary);
  template I3D_DLLEXPORT void
  Get_Labeled_AF_XY(const LabeledImage3d<byte, GRAY8> &src, Image3d<byte> &dest, 
                    const bool vis[], bool only_boundary);
  template I3D_DLLEXPORT void
  Get_Labeled_AF_XZ(const LabeledImage3d<byte, GRAY8> &src, Image3d<byte> &dest, 
                    const bool vis[], bool only_boundary);
  template I3D_DLLEXPORT void
  Get_Labeled_AF_YZ(const LabeledImage3d<byte, GRAY8> &src, Image3d<byte> &dest, 
                    const bool vis[], bool only_boundary);
  template I3D_DLLEXPORT void
  Get_Labeled_AF_XY(const LabeledImage3d<size_t, BINARY> &src, Image3d<size_t> &dest, 
                    const bool vis[], bool only_boundary);
  template I3D_DLLEXPORT void
  Get_Labeled_AF_XZ(const LabeledImage3d<size_t, BINARY> &src, Image3d<size_t> &dest, 
                    const bool vis[], bool only_boundary);
  template I3D_DLLEXPORT void
  Get_Labeled_AF_YZ(const LabeledImage3d<size_t, BINARY> &src, Image3d<size_t> &dest, 
                    const bool vis[], bool only_boundary);
  template I3D_DLLEXPORT void
  Get_Labeled_AF_XY(const LabeledImage3d<byte, BINARY> &src, Image3d<byte> &dest, 
                    const bool vis[], bool only_boundary);
  template I3D_DLLEXPORT void
  Get_Labeled_AF_XZ(const LabeledImage3d<byte, BINARY> &src, Image3d<byte> &dest, 
                    const bool vis[], bool only_boundary);
  template I3D_DLLEXPORT void
  Get_Labeled_AF_YZ(const LabeledImage3d<byte, BINARY> &src, Image3d<byte> &dest, 
                    const bool vis[], bool only_boundary);
  template I3D_DLLEXPORT void
  Get_Labeled_AF_XY(const LabeledImage3d<size_t, GRAY16> &src, Image3d<size_t> &dest, 
                    const bool vis[], bool only_boundary);
  template I3D_DLLEXPORT void
  Get_Labeled_AF_XZ(const LabeledImage3d<size_t, GRAY16> &src, Image3d<size_t> &dest, 
                    const bool vis[], bool only_boundary);
  template I3D_DLLEXPORT void
  Get_Labeled_AF_YZ(const LabeledImage3d<size_t, GRAY16> &src, Image3d<size_t> &dest, 
                    const bool vis[], bool only_boundary);
  template I3D_DLLEXPORT void
  Get_Labeled_AF_XY(const LabeledImage3d<GRAY16, BINARY> &src, Image3d<GRAY16> &dest, 
                    const bool vis[], bool only_boundary);
  template I3D_DLLEXPORT void
  Get_Labeled_AF_XZ(const LabeledImage3d<GRAY16, BINARY> &src, Image3d<GRAY16> &dest, 
                    const bool vis[], bool only_boundary);
  template I3D_DLLEXPORT void
  Get_Labeled_AF_YZ(const LabeledImage3d<GRAY16, BINARY> &src, Image3d<GRAY16> &dest, 
                    const bool vis[], bool only_boundary);


template I3D_DLLEXPORT void CopyToImage(const Image3d<float> &src, Image3d<float> &dst);
template I3D_DLLEXPORT void CopyToImage(const Image3d<GRAY16> &src, Image3d<GRAY16> &dst);
template I3D_DLLEXPORT void CopyToImage(const Image3d<GRAY8> &src, Image3d<GRAY8> &dst);
template I3D_DLLEXPORT void CopyToImage(const Image3d<RGB> &src, Image3d<RGB> &dst);
template I3D_DLLEXPORT void CopyToImage(const Image3d<RGB16> &src, Image3d<RGB16> &dst);
template I3D_DLLEXPORT void CopyToImage(const Image3d<size_t> &src, Image3d<size_t> &dst);
template I3D_DLLEXPORT void CopyToImage(const Image3d<int> &src, Image3d<int> &dst);

  template I3D_DLLEXPORT void GetResampledSubimage(const Image3d<GRAY8> &src, 
	  Image3d<GRAY8> &dest, const VOI<PIXELS> &ivoi, const Vector3d<size_t> &size);
  template I3D_DLLEXPORT void GetResampledSubimage(const Image3d<RGB> &src, 
	  Image3d<RGB> &dest, const VOI<PIXELS> &ivoi, const Vector3d<size_t> &size);


  template I3D_DLLEXPORT void CopySlice(const Image3d<GRAY8> &slc, Image3d<GRAY8> &img, size_t z);
template I3D_DLLEXPORT void CopySlice(const Image3d<GRAY16> &slc, Image3d<GRAY16> &img, size_t z);
template I3D_DLLEXPORT void CopySlice(const Image3d<float> &slc, Image3d<float> &img, size_t z);
template I3D_DLLEXPORT void CopySlice(const Image3d<RGB> &slc, Image3d<RGB> &img, size_t z);
template I3D_DLLEXPORT void CopySlice(const Image3d<RGB16> &slc, Image3d<RGB16> &img, size_t z);
template I3D_DLLEXPORT void CopySlice(const Image3d<BINARY> &slc, Image3d<BINARY> &img, size_t z);

  template I3D_DLLEXPORT void
  MapColor (const Image3d < GRAY8 > &, const Image3d < GRAY8 > &,
            Image3d < RGB > &, const std::vector < RGB > &);
  template I3D_DLLEXPORT void
  MapColor (const Image3d < size_t > &, const Image3d < GRAY8 > &,
            Image3d < RGB > &, const std::vector < RGB > &);
  template I3D_DLLEXPORT void
  MapCondColor (const Image3d < GRAY8 > &, Image3d < GRAY8 > &,
                is_background < GRAY8 > pred, GRAY8 color, const int);
  template I3D_DLLEXPORT void
  MapCondColor (const Image3d < size_t > &, Image3d < size_t > &,
                is_background < size_t > pred, size_t color, const int);

  template I3D_DLLEXPORT void
  RemapIntensities (Image3d < GRAY8 > &img, std::vector < GRAY8 > &map_fun);
  
  //explicit instantiations

  template I3D_DLLEXPORT void
  AffineTransform (Image3d < GRAY8 > &img, const double matrix[4][4],
                   const Vector3d < double >&origin, int degree, GRAY8 bgcol,
                   bool use_resolution);
  template I3D_DLLEXPORT void
  AffineTransform (Image3d < GRAY16 > &img, const double matrix[4][4],
                   const Vector3d < double >&origin, int degree, GRAY16 bgcol,
                   bool use_resolution);


  template I3D_DLLEXPORT void Resample(const Image3d<GRAY8> &, Image3d<GRAY8> &,
                         const Vector3d<size_t> &,SamplingMode);
  template I3D_DLLEXPORT void Resample(const Image3d<GRAY16> &, Image3d<GRAY16> &,
                         const Vector3d<size_t> &,SamplingMode);
  template I3D_DLLEXPORT void Resample(const Image3d<RGB> &, Image3d<RGB> &,
                         const Vector3d<size_t> &,SamplingMode);
  template I3D_DLLEXPORT void Resample(const Image3d<RGB16> &, Image3d<RGB16> &,
                         const Vector3d<size_t> &,SamplingMode);
  template I3D_DLLEXPORT void Resample(const Image3d<float> &, Image3d<float> &,
                         const Vector3d<size_t> &,SamplingMode);
  template I3D_DLLEXPORT void Resize(const Image3d<GRAY8> &, Image3d<GRAY8> &,
                       size_t, size_t, size_t, SamplingMode);
  template I3D_DLLEXPORT void Resize(const Image3d<GRAY16> &, Image3d<GRAY16> &,
                       size_t, size_t, size_t, SamplingMode);
  template I3D_DLLEXPORT void Resize(const Image3d<RGB> &, Image3d<RGB> &,
                       size_t, size_t, size_t, SamplingMode);
  template I3D_DLLEXPORT void Resize(const Image3d<RGB16> &, Image3d<RGB16> &,
                       size_t, size_t, size_t, SamplingMode);
  template I3D_DLLEXPORT void Resize(const Image3d<GRAY8> &, Image3d<GRAY8> &,
                       const Vector3d<size_t> &,SamplingMode);
  template I3D_DLLEXPORT void Resize(const Image3d<GRAY16> &, Image3d<GRAY16> &,
                       const Vector3d<size_t> &,SamplingMode);
  template I3D_DLLEXPORT void Resize(const Image3d<RGB> &, Image3d<RGB> &,
                       const Vector3d<size_t> &,SamplingMode);
  template I3D_DLLEXPORT void ResizeInMicrons(const Image3d<GRAY8> &,Image3d<GRAY8> &,
                                const Vector3d<float> &,SamplingMode);
  template I3D_DLLEXPORT void ResizeInMicrons(const Image3d<RGB> &,Image3d<RGB> &,
                                const Vector3d<float> &,SamplingMode);
  template I3D_DLLEXPORT void ResizeInMicrons(const Image3d<RGB16> &,Image3d<RGB16> &,
                                const Vector3d<float> &,SamplingMode);

  template I3D_DLLEXPORT void  RemapIntensities (Image3d < size_t > &img, std::vector < size_t > &map_fun);
  template I3D_DLLEXPORT void  RemapIntensities (Image3d < GRAY16 > &img, std::vector < GRAY16 > &map_fun);

  template I3D_DLLEXPORT void lanczos_resample(const Image3d<GRAY8> *input,Image3d<GRAY8> *&output,const float factor[3],const int n);
  template I3D_DLLEXPORT void lanczos_resample(const Image3d<GRAY16> *input,Image3d<GRAY16> *&output,const float factor[3],const int n);
  template I3D_DLLEXPORT void lanczos_resample(const Image3d<float> *input,Image3d<float> *&output,const float factor[3],const int n);
  template I3D_DLLEXPORT void lanczos_resample(const Image3d<GRAY8> *input,Image3d<GRAY8> *&output,const Vector3d<float> factor,const int n);
  template I3D_DLLEXPORT void lanczos_resample(const Image3d<GRAY16> *input,Image3d<GRAY16> *&output,const Vector3d<float> factor,const int n);
  template I3D_DLLEXPORT void lanczos_resample(const Image3d<float> *input,Image3d<float> *&output,const Vector3d<float> factor,const int n);

template I3D_DLLEXPORT void ResampleToDesiredResolution(Image3d<BINARY> &img, 
														 const Resolution &, 
														 SamplingMode m);

template I3D_DLLEXPORT void ResampleToDesiredResolution(Image3d<GRAY8> &img, 
														 const Resolution &, 
														 SamplingMode m);

template I3D_DLLEXPORT void ResampleToDesiredResolution(Image3d<GRAY16> &img, 
														 const Resolution &, 
														 SamplingMode m);

template I3D_DLLEXPORT void ResampleToDesiredResolution(Image3d<float> &img, 
														 const Resolution &, 
														 SamplingMode m);


}
