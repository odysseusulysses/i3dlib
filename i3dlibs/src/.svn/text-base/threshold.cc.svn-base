/*
 * i3dlib - image manipulation library
 *
 * Copyright (C) 2000-2006   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 * FILE: threshold.cc
 *
 * Thresholding of 3d image
 *
 * Petr Matula (pem@fi.muni.cz)
 */

#ifdef __GNUG__
#pragma implementation
#endif

#include "threshold.h"
#include "histogram.h"
#include <algorithm>


namespace i3d {

/* implementation */


////////////////////////////////////////////////////////////////////////////
template <class T> void Threshold(Image3d<T> &img, T t1, T t2, T lower_value, T upper_value)
{
    size_t last = img.GetImageSize();
    T *data = img.GetFirstVoxelAddr();
    
    for (size_t i=0; i < last; i++) 
      data[i] = ((data[i] >= t1 && data[i] <= t2) ?
                 upper_value :
                 lower_value);
}
  
////////////////////////////////////////////////////////////////////////////
template <class VOXELIN, class VOXELOUT>
void Threshold(const Image3d<VOXELIN> &imgin, Image3d<VOXELOUT> &imgout,
                VOXELIN t1, VOXELIN t2)
{
    imgout.SetOffset(imgin.GetOffset());
    imgout.SetResolution(imgin.GetResolution());
    imgout.MakeRoom(imgin.GetWidth(), imgin.GetHeight(), imgin.GetNumSlices());
	
    size_t last = imgin.GetImageSize();
    const VOXELIN *datain = imgin.GetFirstVoxelAddr();
    VOXELOUT *dataout = imgout.GetFirstVoxelAddr();
    
    for (size_t i=0; i < last; i++) 
      dataout[i] = ((datain[i] >= t1 && datain[i] <= t2) ?
                 std::numeric_limits<VOXELOUT>::max() :
                 std::numeric_limits<VOXELOUT>::min());
}

////////////////////////////////////////////////////////////////////////////
void InitDivisionPoints(std::vector<size_t> &div, size_t sz, size_t n)
  {
    div.resize(n + 2);

    for (size_t i = 0; i <= n; i++)
      div[i] = (sz  * i) / n;

    div[n+1] = div[n];
  }

////////////////////////////////////////////////////////////////////////////
void RecomputeDivisionPoints(std::vector<size_t> &div)
  {
    for (size_t i = div.size() - 1; i > 0; i--)
      div[i] = (div[i] + div[i-1])/2;
  }

/****************************************************************************
 * semi-threshold -- all the voxel above threshold are left untouched, while
 * the rest is set to background
 */
template < class VOXELIN, class VOXELOUT > void
SemiThreshold (const Image3d < VOXELIN > &imgin, Image3d < VOXELOUT > &imgout,
               VOXELIN t)
{
  imgout.SetOffset (imgin.GetOffset ());
  imgout.SetResolution (imgin.GetResolution ());
  imgout.MakeRoom (imgin.GetWidth (), imgin.GetHeight (),
                   imgin.GetNumSlices ());

  size_t last = imgin.GetImageSize ();
  const VOXELIN * datain = imgin.GetFirstVoxelAddr ();
  VOXELOUT * dataout = imgout.GetFirstVoxelAddr ();

  for (size_t i = 0; i < last; i++)
    {
      dataout[i] = (datain[i] >= t) ?
        datain[i] : std::numeric_limits < VOXELOUT >::min ();
    }
}

//////////////////////////////////////////////////////////////////////////
template <class T> 
	T HistFinder<T>::find(const Image3d<T> &img, const VOI<PIXELS> &voi) const 
       {
	 Histogram hist;
	 IntensityHist(img, hist, VOItoGslice(voi, img.GetSize()));
	 Histogram smooth;
	 
	 if (ignore_max_column)
	   hist[HistMax(hist)] = 0;
	 
	 if (smooth_diff == 1)
	   SmoothHist(hist,smooth);
	 else if (smooth_diff > 1)
	   SmoothHist(hist,smooth,smooth_diff);
	 else 
	   smooth = hist; 
	 
	 return AnalyzeHist(smooth);
       }

//////////////////////////////////////////////////////////////////////////

 template <class T> T MaxPlusKDeltaFinder<T>::AnalyzeHist
 		(const Histogram &hist) const
    {
      size_t m = HistMax(hist);
      size_t d;
      size_t delta = 0;
      size_t sum = hist[m];
	  size_t i;

      for (i = 0; i < m; i++)
        {
          d = m - i;
          delta += d*d*hist[i];
          sum += hist[i];
        }
      for (i = m+1; i < hist.size(); i++)
        {
          d = i - m;
          delta += d*d*hist[i];
          sum += hist[i];
        }
      double dd = delta;
	  dd = k*std::sqrt(dd/sum);
      delta = size_t(dd);

      return  m + delta;
    }

//////////////////////////////////////////////////////////////////////////
template <class T> 
T OtsuThresholdFinder<T>::AnalyzeHist( const Histogram &hist) const 
	{        
      return FindOtsuThreshold(hist);
   }

//////////////////////////////////////////////////////////////////////////

	struct ProbabilityRecord {
		int _value;
		float _probability;
		ProbabilityRecord(int value, float probability): _value(value), _probability(probability) {};
	};

	float compute_sigma2(const std::vector<float> &omega, const std::vector<float> &mu)
	{
		float res = 0.0;
		for (size_t i = 0; i < omega.size(); ++i) {
			res += omega[i] * mu[i] * mu[i];
		}

		return res;
	}

	bool ComputeMultilevelOtsuThresholds(const Histogram &hist, size_t k, std::vector<int> &res)
	{
		if (k < 2) {
			throw InternalException("The number of levels for Multilevel Otsu Thresholding "
				                    "must be at least 2!");
		}

		std::vector<ProbabilityRecord> p;	///< condensed histogram of the image

		std::vector<size_t> thresholds;		///< actually tested thresholds (contains indexes to p)
		thresholds.resize(k - 1);

		std::vector<size_t> best_thresholds;	///< thresholds with best value (contains indexes to p)
		best_thresholds.resize(k - 1);

		std::vector<float> omega;					///< omega (total probability) of each cluster
		omega.resize(k);

		std::vector<float> mu;						///< mean of each cluster
		mu.resize(k);

		float max_value;							///< value of the best thresholds
		float imgsize = 0.0;

		for (size_t i = 0; i < hist.size(); ++i) {
			imgsize += hist[i];
		}

		/* Create condensed histogram of the image ------------- */
		for (size_t i = 0; i < hist.size(); ++i) {
			if (hist[i] != 0) {
				p.push_back(ProbabilityRecord(i, float(hist[i])/imgsize));
			}
		}
        
		/* Initialization --------------------------------------- */
		for (size_t i = 0; i < k - 1; ++i) {
			best_thresholds[i] = thresholds[i] = i;
			omega[i] = p[i]._probability;
			mu[i] = p[i]._value;
		}

		if (k > p.size()) 
			return false;

		omega[k - 1] = 0.0; 
		mu[k - 1] = 0.0;

		for (size_t i = k - 1; i < p.size(); ++i) {
			omega[k - 1] += p[i]._probability;
			mu[k - 1] += p[i]._value * p[i]._probability;
		}
		mu[k - 1] /= omega[k - 1];

		max_value = compute_sigma2(omega, mu);

		/* Go through all the possibilities --------------------- */
		while (thresholds[0] != p.size() - k) {
			// move the last threshold... if it isn't possible move its predecessor and reset descendants and so on
			size_t last = k - 2;
			while (thresholds[last] == p.size() - k + last) { // find last movable
				last--;
			}
			int thresh = ++thresholds[last];	// move

			// udate mu and omega
			mu[last] = mu[last] * omega[last] + p[thresh]._value * p[thresh]._probability; 
			omega[last] += p[thresh]._probability;
			mu[last] /= omega[last];
			
			for (size_t i = last + 1; i < k - 1; ++i) { // reset descendants
				thresh = thresholds[i] = thresholds[last] + i - last;
				omega[i] = p[thresh]._probability;
				mu[i] = p[thresh]._value;
			}

			// recompute mu and omega of reset clusters
			omega[k - 1] = 0.0;
			mu[k - 1] = 0.0;

			for (size_t i = thresholds[k - 2] + 1; i < p.size(); ++i) {
				omega[k - 1] += p[i]._probability;
				mu[k - 1] += p[i]._value * p[i]._probability;
			}
			mu[k - 1] /= omega[k - 1];

			float value = compute_sigma2(omega, mu); // compute new value

			if (value > max_value) {
				max_value = value;
				best_thresholds = thresholds;
			}
		} 

		res.resize(k - 1);
		for (size_t i = 0; i < best_thresholds.size(); ++i) {
			res[i] = p[best_thresholds[i]]._value;
		}

		return true;
	}

//////////////////////////////////////////////////////////////////////////


	template <class VOXEL> void DoMultilevelThresholding(const Image3d<VOXEL> &img, Image3d<GRAY8> &imgout, const std::vector<int> &thresholds)
	{
		imgout.MakeRoom(img.GetSize());
		imgout.SetResolution(img.GetResolution());
		imgout.SetOffset(img.GetOffset());

		/* Do multilevel thresholding --------------------------- */
		const VOXEL *px    = img.GetFirstVoxelAddr();
		GRAY8 *pxout	   = imgout.GetFirstVoxelAddr();

		for (size_t j = 0; j < img.GetImageSize(); ++j) {
			GRAY8 i = 0;  
			while (i < thresholds.size() && thresholds[i] < *px) {
				++i;
			}
			*pxout = i;

			++px; ++pxout;
		}
		return;

	}

	template void DoMultilevelThresholding(const Image3d<GRAY8> &img, Image3d<GRAY8> &imgout, const std::vector<int> &thresholds);
	template void DoMultilevelThresholding(const Image3d<GRAY16> &img, Image3d<GRAY8> &imgout, const std::vector<int> &thresholds);

//////////////////////////////////////////////////////////////////////////

template <class T> 
T UnimodalThresholdFinder<T>::AnalyzeHist(const Histogram &hist) const
    { 
      return FindUnimodalThreshold(hist);
    }

//////////////////////////////////////////////////////////////////////////

template <class T, class OUTVOXEL, class TF> 
void LocalThreshold(const Image3d<T> &in, Image3d<OUTVOXEL> &out,
                      size_t nx, size_t ny, size_t nz,
                      const TF &ThresholdFinder)
  {

    out.SetOffset(in.GetOffset()); 
    out.SetResolution(in.GetResolution());
    out.MakeRoom(in.GetSize());
			 
    size_t i,j,k;
    size_t sx = in.GetSizeX();
    size_t sy = in.GetSizeY();
    size_t sz = in.GetSizeZ();
    
    // sub-region devision points
    std::vector<size_t> div_x, div_y, div_z;
    InitDivisionPoints(div_x, sx, nx);
    InitDivisionPoints(div_y, sy, ny);
    InitDivisionPoints(div_z, sz, nz);

    // local thresholds for sub-regions
    T *lt = new T[(nx+2)*(ny+2)*(nz+2)];
#define LT(x,y,z) lt[(x) + (nx+2) * ((y) + (z) * (ny+2)) ]
    {
      T* p = lt;
      for (i = 0; i < (nx+2)*(ny+2)*(nz+2); i++,p++)
        *p=0;
    }
           
    for (k = 1; k <= nz; k++)
      for (j = 1; j <= ny; j++)
        for (i = 1; i <= nx; i++)
	  LT(i, j, k) = ThresholdFinder.find(in,VOI<PIXELS>(div_x[i-1], 
																		 div_y[j-1], 
																		 div_z[k-1],
																		 div_x[i]-div_x[i-1],
																		 div_y[j]-div_y[j-1],
																		 div_z[k]-div_z[k-1]));	

    // recompute division points and copy border thresholds
    RecomputeDivisionPoints(div_x);
    RecomputeDivisionPoints(div_y);
    RecomputeDivisionPoints(div_z);

    // copy corners
    LT(     0,      0,      0) = LT( 1,  1,  1);
    LT(     0,      0, nz + 1) = LT( 1,  1, nz); 
    LT(     0, ny + 1,      0) = LT( 1, ny,  1); 
    LT(nx + 1,      0,      0) = LT(nx,  1,  1); 
    LT(nx + 1, ny + 1,      0) = LT(nx, ny,  1); 
    LT(nx + 1,      0, nz + 1) = LT(nx,  1, nz); 
    LT(     0, ny + 1, nz + 1) = LT( 1, ny, nz); 
    LT(nx + 1, ny + 1, nz + 1) = LT(nx, ny, nz); 

    // copy edges
    for (i = 1; i <= nx; i++) // x
      {
        LT(i,      0,      0) = LT(i,  1,  1);
        LT(i, ny + 1,      0) = LT(i, ny,  1); 
        LT(i,      0, nz + 1) = LT(i,  1, nz); 
        LT(i, ny + 1, nz + 1) = LT(i, ny, nz); 
      }
    for (j = 1; j <= ny; j++) // y
      {
        LT(     0, j,      0) = LT( 1, j,  1);
        LT(nx + 1, j,      0) = LT(nx, j,  1);
        LT(     0, j, nz + 1) = LT( 1, j, nz);
        LT(nx + 1, j, nz + 1) = LT(nx, j, nz);
      }
    for (k = 1; k <= nz; k++) // z
      {
        LT(     0,      0, k) = LT( 1,  1, k);
        LT(nx + 1,      0, k) = LT(nx,  1, k);
        LT(     0, ny + 1, k) = LT( 1, ny, k);
        LT(nx + 1, ny + 1, k) = LT(nx, ny, k);
      }

    // copy faces
    for (j = 1; j <= ny; j++) for (i = 1; i <= nx; i++) // xy
      {
        LT(i, j,      0) = LT(i, j,  1);
        LT(i, j, nz + 1) = LT(i, j, nz);
      }
    for (k = 1; k <= nz; k++) for (i = 1; i <= nx; i++) // xz
      {
        LT(i,      0, k) = LT(i,  1, k);
        LT(i, ny + 1, k) = LT(i, ny, k);
      }
    for (k = 1; k <= nz; k++) for (j = 1; j <= ny; j++) // yz
      {
        LT(     0, j, k) = LT( 1, j, k);
        LT(nx + 1, j, k) = LT(nx, j, k);
      }

#ifdef I3D_DEBUG 
    std::cout << std::endl << "Division Points" << std::endl;
    for (i = 0; i < div_x.size(); i++)
		std::cout << std::setw(4) << div_x[i] << " ";
    std::cout << std::endl;
    for (i = 0; i < div_y.size(); i++)
      std::cout << std::setw(4) << div_y[i] << " ";
    std::cout << std::endl;
    for (i = 0; i < div_z.size(); i++)
      std::cout << std::setw(4) << div_z[i] << " ";
    std::cout << std::endl;
    
    // show thresholds
    std::cout << std::endl << "Thresholds " << std::endl;
    // show thresholds
    for (k = 0; k < nz+2; k++)
      {
        std::cout << "-" << k << "-" << std::endl;
        for (j = 0; j < ny+2; j++)
          {
            for (i = 0; i < nx+2; i++)
              std::cout << std::setw(4) << int(LT(i, j, k)) << " ";
            std::cout << std::endl;
          }
      }
#endif
    /**********************************************************************
     *                                                                    *
     *    threshold input image by trilinear interpolated thresholds      *
     *                                                                    *
     **********************************************************************/
    static size_t x,y,z,idx;
    unsigned long thres;
    unsigned long X1,Y1,Z1,X2,Y2,Z2,D;

    for (k = 0; k <= nz; k++) for (j = 0; j <= ny; j++) for (i = 0; i <= nx; i++)
      // for each region
      {
        D = (div_x[i+1] - div_x[i] ) * 
				(div_y[j+1] - div_y[j] ) * 
				(div_z[k+1] - div_z[k] );
		  
        for (z = div_z[k]; z < div_z[k+1]; z++)
          for (y = div_y[j]; y < div_y[j+1]; y++)
            for (x = div_x[i]; x < div_x[i+1]; x++)
              // for each voxel in region
              {
                X1 = div_x[i+1] - x;  X2 = x - div_x[i];
                Y1 = div_y[j+1] - y;  Y2 = y - div_y[j];
                Z1 = div_z[k+1] - z;  Z2 = z - div_z[k];

                thres = (((LT(i, j, k) * (X1) + LT(i+1, j, k) * (X2)) * (Y1) +
                          (LT(i, j+1, k) * (X1) + LT(i+1, j+1, k) * (X2)) * (Y2)) * (Z1) +
                         ((LT(i, j, k+1) * (X1) + LT(i+1, j, k+1) * (X2)) * (Y1) +
                          (LT(i, j+1, k+1) * (X1) + LT(i+1, j+1, k+1) * (X2)) * (Y2)) * (Z2)) / D;
                idx = GetIndex(x,y,z,out.GetSize());
				OUTVOXEL vxl = OUTVOXEL((in.GetVoxel(idx) > thres) ? std::numeric_limits<OUTVOXEL>::max(): std::numeric_limits<OUTVOXEL>::min());
				out.SetVoxel(idx, vxl);
              }
      }
    delete [] lt;
  }
#undef LT

//////////////////////////////////////////////////////////////////////////

/* Explicit instantiations: */
template I3D_DLLEXPORT void Threshold(Image3d<GRAY8>&, GRAY8, GRAY8, GRAY8, GRAY8);
template I3D_DLLEXPORT void Threshold(Image3d<GRAY16>&, GRAY16, GRAY16, GRAY16, GRAY16);
template I3D_DLLEXPORT void Threshold(Image3d<size_t>&, size_t, size_t, size_t, size_t);
template I3D_DLLEXPORT void Threshold(Image3d<float>&, float, float, float, float);
template I3D_DLLEXPORT void Threshold(const Image3d<GRAY8>&, Image3d<BINARY>&, GRAY8, GRAY8);
template I3D_DLLEXPORT void Threshold(const Image3d<GRAY8>&, Image3d<GRAY8>&, GRAY8, GRAY8);
template I3D_DLLEXPORT void Threshold(const Image3d<GRAY16>&, Image3d<GRAY16>&, GRAY16, GRAY16);
template I3D_DLLEXPORT void Threshold(const Image3d<GRAY16>&, Image3d<BINARY>&, GRAY16, GRAY16);
template I3D_DLLEXPORT void Threshold(const Image3d<GRAY16>&, Image3d<size_t>&, GRAY16, GRAY16);
template I3D_DLLEXPORT void Threshold(const Image3d<float>&, Image3d<float>&, float, float);
template I3D_DLLEXPORT void Threshold(const Image3d<float>&, Image3d<BINARY>&, float, float);
template I3D_DLLEXPORT void Threshold(const Image3d<size_t>&, Image3d<BINARY>&, size_t, size_t);

//template class I3D_DLLEXPORT HistFinder<GRAY8>;
template class I3D_DLLEXPORT MaxPlusKDeltaFinder<GRAY8>;
template class I3D_DLLEXPORT MaxPlusKDeltaFinder<GRAY16>;
template class I3D_DLLEXPORT UnimodalThresholdFinder<GRAY8>;
template class I3D_DLLEXPORT UnimodalThresholdFinder<GRAY16>;
template class I3D_DLLEXPORT OtsuThresholdFinder<GRAY8>;
template class I3D_DLLEXPORT OtsuThresholdFinder<GRAY16>;

template I3D_DLLEXPORT 
void LocalThreshold(const Image3d<GRAY8>&, Image3d<GRAY8> &out, 
		size_t, size_t, size_t, const MaxPlusKDeltaFinder<GRAY8>&);

template  I3D_DLLEXPORT 
void LocalThreshold(const Image3d<GRAY8>&, Image3d<BINARY> &out, 
		size_t, size_t, size_t, const MaxPlusKDeltaFinder<GRAY8>&);

template  I3D_DLLEXPORT 
void LocalThreshold(const Image3d<GRAY16>&, Image3d<BINARY> &out, 
		size_t, size_t, size_t, const MaxPlusKDeltaFinder<GRAY16>&);

template  I3D_DLLEXPORT 
void LocalThreshold(const Image3d<GRAY8>&, Image3d<GRAY8> &out, 
		size_t, size_t, size_t, const OtsuThresholdFinder<GRAY8>&);

template  I3D_DLLEXPORT 
void LocalThreshold(const Image3d<GRAY8>&, Image3d<BINARY> &out, 
		size_t, size_t, size_t, const OtsuThresholdFinder<GRAY8>&);

template  I3D_DLLEXPORT 
void LocalThreshold(const Image3d<GRAY16>&, Image3d<BINARY> &out, 
		size_t, size_t, size_t, const OtsuThresholdFinder<GRAY16>&);

template  I3D_DLLEXPORT 
void LocalThreshold(const Image3d<GRAY8>&, Image3d<GRAY8> &out, 
		size_t, size_t, size_t, const UnimodalThresholdFinder<GRAY8>&);

template  I3D_DLLEXPORT 
void LocalThreshold(const Image3d<GRAY8>&, Image3d<BINARY> &out, 
		size_t, size_t, size_t, const UnimodalThresholdFinder<GRAY8>&);

template  I3D_DLLEXPORT 
void LocalThreshold(const Image3d<GRAY16>&, Image3d<BINARY> &out, 
		size_t, size_t, size_t, const UnimodalThresholdFinder<GRAY16>&);

//////////////////////////////////////////////////////////////////////////


}
