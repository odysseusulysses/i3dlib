/*
 * i3dlib - image manipulation library
 *
 * Copyright (C) 2000-2006   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 * FILE: bspline.cc, by Tomá limar
 *
 * 
 * TODO: needs to be amended and translated!!!
 * 
 */

#include "bspline.h"

inline double bSplineSurface::baseFce( const size_t degree, const double t, const size_t i, const double *knotVector )
{
	double p,a,b;

	if( degree<1 )
	{
		if (( knotVector[i] <= t ) && ( t < knotVector[i+1] )) return 1.0f;
		else return 0.0f;
	}

	a=(t-knotVector[i]);
	b=(knotVector[i+degree]-knotVector[i]);
	if (b==0)   p=0.0;
	else        p=baseFce(degree-1, t,i,knotVector)*a/b;

	a=(knotVector[i+degree+1]-t);
	b=(knotVector[i+degree+1]-knotVector[i+1]);
	if (b==0)   p+=0.0;
	else        p+=baseFce( degree-1, t,i+1,knotVector)*a/b;

	return  p;
}

/*
double bSplineSurface::deBoor( int degree, const int i, const double t, const double *knotVector )
{
    if( degree == 0 )
    {
        if( t>=knotVector[i] && t<=knotVector[i+1] ) return 1.0;
        else return 0.0;
    }
    else
    {
        double tmp1=knotVector[i+degree]-knotVector[i]==0?
        0:(  (t-knotVector[i])/(knotVector[i+degree]-knotVector[i])  *  deBoor( degree-1, i, t, knotVector )  );
        double tmp2=knotVector[i+degree+1]-knotVector[i+1]==0?
        0:(  (knotVector[i+degree+1]-t)/(knotVector[i+degree+1]-knotVector[i+1])  *  deBoor( degree-1, i+1, t, knotVector )  );
        return tmp1+tmp2;
    }
}
*/

bool bSplineSurface::setControlPoints( const double *points )
{
	if( !paramOK ) return false;
	if( cp ) delete[] cp;
	cp = new double[numCpX*numCpY];
	for(size_t i = 0; i < numCpX * numCpY; i++)
	{
		cp[i]=points[i];
	}
	return true;
}

double *bSplineSurface::computeKnotVector( const size_t degree )
{
	if( !paramOK ) return NULL;
	double *knot = new double[ numCpX + degree + 1 ];
	for(size_t i = 0; i < numCpX + degree + 1; i++)
	{
		if( i<degree+1 ) knot[i]=0;
		else if( i>numCpX-1 ) knot[i]=numCpX-degree;
		else knot[i]=i-degree;
	}
	return knot;
}

bool bSplineSurface::compute( void )
{
	if( cp == NULL || !paramOK || !allocateSurface() ) return false;
	
	const double incrementX = computeIncrementX();
	const double incrementY = computeIncrementY();
	const double *knotVector = computeKnotVector( degree );

	double tmp;

	for( size_t i=0; i<sizeX; i++ )
	{
	       	for( size_t j=0; j<sizeY; j++ )
        	{
			tmp=0.0;
			for ( size_t mm=0; mm<numCpX; mm++ )
			{
				for( size_t nn=0; nn<numCpY; nn++ )
				{
					tmp+=baseFce( degree, incrementY*j, nn, knotVector )*
					     baseFce( degree, incrementX*i, mm, knotVector )*
					     cp[nn*numCpX+mm];
				}
			}
			surface[j*sizeX+i]=tmp;
		}

	}

	delete[] knotVector;
	return true;
}
/*
bool bSplineSurface::computeComponents( double *a )
{
	if( !a ) return false;
//	if( cp == 0 ) return false;
//	if( numCpX - degree < 1 ) return false;
//	if( !allocateSurface() ) return false;

	double incrementX = (double)( numCpX - degree ) / (double)sizeX;
	double incrementY = (double)( numCpX - degree ) / (double)sizeY;

	double *knotVector = computeKnotVector( degree );

	for( int i=0; i<sizeX; i++ )
	{
	       	for( int j=0; j<sizeY; j++ )
        	{
			for( int mm=0; mm<numCpX; mm++ )
			{
				for( int nn=0; nn<numCpX; nn++ )
				{
					a[(i*sizeY+j)+(sizeX*sizeY)*(mm*numCpX+nn)]=
						baseFce( degree, incrementY*j, nn, knotVector )*
						baseFce( degree, incrementX*i, mm, knotVector );
				}
			}
		}

	}

	delete[] knotVector;
	return true;
}
*/
