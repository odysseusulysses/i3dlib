/*
 * i3dlib - image manipulation library
 *
 * Copyright (C) 2000-2006   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 * FILE: points.cc
 *
 * Points and point sets representations
 *
 * Petr Matula (pem@fi.muni.cz) 2002
 */

#include "points.h"

#ifdef WITH_BLAS
	// C++ header of dgemm_ function located in the BLAS library
	extern "C" void dgemm_(const char &transa, const char &transb, const int &m, const int &n, const int &k, 
		                   const double &alpha, const double *a, const int &lda, const double *b, const int &ldb, 
						   const double &beta, double *c, const int &ldc);
#endif


namespace i3d {
  
/* Explicit instantiations */

template class PointSet<double>;

#ifdef WITH_BLAS
// Points in point set p_in are multiplied by affine transformation matrix
// and are stored to p_out. The last row of mat must have the form [0 0 0 1]'
// otherwise an InternalException is thrown in I3D_DEBUG mode or its values
// are ignored in release mode
  void ApplyTransMat(const PointSet<double> &pin,
                     PointSet<double> &pout, double mat[4][4])
  {

	  // Apply 3x3 transformation matrix
  dgemm_('N', 'N', 3, pin.size(), 3,
         1, mat[0], 4, pin.data(), 3, // input matrices
         0, pout.data(), 3); // output matrix
  // Add translation
  Point3d<double> t(mat[0][3],mat[1][3],mat[2][3]);
  pout += t;
}
#endif


  void Pol2Cart(const PointSet<double> &p_in, PointSet<double> &p_out, Direction axis)
  {
    int a1 = (axis == 0) ? 1 : 0;
    int a2 = (axis < 2)  ? 2 : 1;

    if (p_in.size() != p_out.size())
      p_out.resize(p_in.size());
    
    for (size_t i = 0; i < p_in.size(); ++i)
      {
        p_out[i][axis] = p_in[i][axis];
        p_out[i][a1] = p_in[i][a2] * cos(p_in[i][a1]);
        p_out[i][a2] = p_in[i][a2] * sin(p_in[i][a1]);
      }
  }

  void Cart2Pol(const PointSet<double> &p_in, PointSet<double> &p_out, Direction axis)
  {
    int a1 = (axis == 0) ? 1 : 0;
    int a2 = (axis < 2)  ? 2 : 1;

    if (p_in.size() != p_out.size())
      p_out.resize(p_in.size());
    
    for (size_t i = 0; i < p_in.size(); ++i)
      {
        p_out[i][axis] = p_in[i][axis];
        p_out[i][a1] = atan2(p_in[i][a2], p_in[i][a1]);
        p_out[i][a2] = sqrt(p_in[i][a1]*p_in[i][a1] + p_in[i][a2]*p_in[i][a2]);
      }
  }


  void Cart2Spherical(const PointSet<double> &p_in, PointSet<double> &p_out)
  {
    if (p_in.size() != p_out.size())
      p_out.resize(p_in.size());
    
    for (size_t i = 0; i < p_in.size(); ++i)
      {
		  const double &x = p_in[i][0];
		  const double &y = p_in[i][1];
		  const double &z = p_in[i][2];
		  double &r = p_out[i][0];
		  double &rho = p_out[i][1];
		  double &phi = p_out[i][2];
		  rho = (x == 0 && y == 0) ? 0.0 : atan2(y, x);
		  r = sqrt(x*x + y*y + z*z);
		  phi = (r == 0) ? 0.0 : acos(z / r);
      }
  }

  void Spherical2Cart(const PointSet<double> &p_in, PointSet<double> &p_out)
  {
    if (p_in.size() != p_out.size())
      p_out.resize(p_in.size());
    
	for (size_t i = 0; i < p_in.size(); ++i)
	{
		const double &r   = p_in[i][0];
		const double &rho = p_in[i][1];
		const double &phi = p_in[i][2];
		double &x = p_out[i][0];
		double &y = p_out[i][1];
		double &z = p_out[i][2];

		x = r * cos(rho) * sin(phi);
		y = r * sin(rho) * sin(phi);
		z = r * cos(phi);
	}
  }


	template class I3D_DLLEXPORT Point3d < float>;
	template class I3D_DLLEXPORT Point3d < int >;

}
