/*
 * i3dlib - image manipulation library
 *
 * Copyright (C) 2000-2006   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** \file morphology.cc
  \brief Part of mathematical morphology routines - implementation.

  This file contains bodies of mathematical morphology routines dealing with 
  the watershed transformation.
*/ 

#include <vector>
#include <queue>
#include <set>
#include <map>
#include <limits>
using std::vector;
using std::queue;
using std::set;
using std::map;
using std::priority_queue;
using std::numeric_limits;

#include "watersheds.h"
#include "i3dassert.h"
#include "morphology.h"
#include "draw.h"
#include "ivLab/toboggan.h"
#include "ivLab/watershed.h"
#include "ivLab3D/toboggan.h"
#include "ivLab3D/watershed.h"
#include "ivLab3D/watershed_cgs.h"


#ifdef __GNUG__
#	pragma implementation
#endif

namespace i3d {

	//--------------------------------------------------------------------------
	// Watershed tobogan implementation - uses code from ivLab3D
	//--------------------------------------------------------------------------

	template <class T> void WatershedToboggan(const Image3d<T> &in_grad, Image3d<int> &out, int connectivity, bool drawLines)
	{
		I3DASSERT_ALWAYS_ET(connectivity == 4 || connectivity == 8 || connectivity == 6 || connectivity == 18 || connectivity == 26,
			"Unsupported connectivity required");

		out.MakeRoom(in_grad.GetSize());

		int *result = ivLab3D::Toboggan(in_grad.GetFirstVoxelAddr(), in_grad.GetWidth(), in_grad.GetHeight(), in_grad.GetNumSlices(), true, connectivity, drawLines);
		if (result) {
			memcpy(out.GetFirstVoxelAddr(), result, out.GetImageSize()*sizeof(int));
			delete [] result;
			result = NULL;
		}
	}

	template I3D_DLLEXPORT void WatershedToboggan(const Image3d<i3d::GRAY8> &in_grad, Image3d<int> &out, int connectivity, bool drawLines);
	template I3D_DLLEXPORT void WatershedToboggan(const Image3d<i3d::GRAY16> &in_grad, Image3d<int> &out, int connectivity, bool drawLines);
	template I3D_DLLEXPORT void WatershedToboggan(const Image3d<float> &in_grad, Image3d<int> &out, int connectivity, bool drawLines);

	//--------------------------------------------------------------------------
	// Watershed tobogan implementation - uses code from ivLab
	//--------------------------------------------------------------------------

	template <class T> void WatershedToboggan2D(const Image3d<T> &in_grad, Image3d<int> &out, int connectivity)
	{
		I3DASSERT_ALWAYS_ET(connectivity == 4 || connectivity == 8, "Unsupported connectivity required");
		I3DASSERT_ALWAYS_ET(in_grad.GetSizeZ() == 1, "Algorithm works for 2D images only");

		out.MakeRoom(in_grad.GetSize());

		int *result = Toboggan(in_grad.GetFirstVoxelAddr(), in_grad.GetWidth(), in_grad.GetHeight(), true, connectivity);
		if (result) {
			memcpy(out.GetFirstVoxelAddr(), result, out.GetImageSize()*sizeof(int));
			delete [] result;
			result = NULL;
		}
	}

	template I3D_DLLEXPORT void WatershedToboggan2D(const Image3d<i3d::GRAY8> &in_grad, Image3d<int> &out, int connectivity);
	template I3D_DLLEXPORT void WatershedToboggan2D(const Image3d<i3d::GRAY16> &in_grad, Image3d<int> &out, int connectivity);
	template I3D_DLLEXPORT void WatershedToboggan2D(const Image3d<float> &in_grad, Image3d<int> &out, int connectivity);

	//--------------------------------------------------------------------------
	// Watershed immersion implementation - uses code from ivLab3D
	//--------------------------------------------------------------------------

	template <class T> void WatershedLin(const Image3d<T> &in_grad, Image3d<int> &out, int connectivity, bool drawLines)
	{
		I3DASSERT_ALWAYS_ET(connectivity == 4 || connectivity == 8 || connectivity == 6 || connectivity == 18 || connectivity == 26,
			"Unsupported connectivity required");

		out.MakeRoom(in_grad.GetSize());

		int *result = ivLab3D::Watershed(in_grad.GetFirstVoxelAddr(), in_grad.GetWidth(), in_grad.GetHeight(), in_grad.GetNumSlices(), true, connectivity, drawLines);
		if (result) {
			memcpy(out.GetFirstVoxelAddr(), result, out.GetImageSize()*sizeof(int));
			delete [] result;
			result = NULL;
		}
	}

	template I3D_DLLEXPORT void WatershedLin(const Image3d<i3d::GRAY8> &in_grad, Image3d<int> &out, int connectivity, bool drawLines);
	template I3D_DLLEXPORT void WatershedLin(const Image3d<i3d::GRAY16> &in_grad, Image3d<int> &out, int connectivity, bool drawLines);
	template I3D_DLLEXPORT void WatershedLin(const Image3d<float> &in_grad, Image3d<int> &out, int connectivity, bool drawLines);

	//--------------------------------------------------------------------------
	// Watershed immersion implementation - uses code from ivLab
	//--------------------------------------------------------------------------

	template <class T> void WatershedLin2D(const Image3d<T> &in_grad, Image3d<int> &out, int connectivity)
	{
		I3DASSERT_ALWAYS_ET(connectivity == 4 || connectivity == 8, "Unsupported connectivity required");
		I3DASSERT_ALWAYS_ET(in_grad.GetSizeZ() == 1, "Algorithm works for 2D images only");

		out.MakeRoom(in_grad.GetSize());

		int *result = ::Watershed(in_grad.GetFirstVoxelAddr(), in_grad.GetWidth(), in_grad.GetHeight(), true, connectivity);
		if (result) {
			memcpy(out.GetFirstVoxelAddr(), result, out.GetImageSize()*sizeof(int));
			delete [] result;
			result = NULL;
		}
	}

	template I3D_DLLEXPORT void WatershedLin2D(const Image3d<i3d::GRAY8> &in_grad, Image3d<int> &out, int connectivity);
	template I3D_DLLEXPORT void WatershedLin2D(const Image3d<i3d::GRAY16> &in_grad, Image3d<int> &out, int connectivity);
	template I3D_DLLEXPORT void WatershedLin2D(const Image3d<float> &in_grad, Image3d<int> &out, int connectivity);

	//--------------------------------------------------------------------------
	// Watershed Meyer's algorithm implementation
	//--------------------------------------------------------------------------

	/** Priority queue used in the Meyer's watershed algorithm. Every value can
	be stored only once during the existence of the queue. Whenever a value is
	removed, it's priority defines minimum priority and no inserted element can
	have lower one (in that case, it is	treated as having minimum priority).
	*/
	template <typename T>
	class CWSMeyerQueue
	{
	public:
		/// creates new empty queue allocates some required space, and marks border voxels as already processed
		CWSMeyerQueue(const Image3d<size_t> &output): inQueue(output.GetImageSize(), false)
		{
			minimumPriority = std::numeric_limits<T>::min();
			for (size_t i = output.GetImageSize(); i > 0; --i)
			{
				if (output.OnBorder(i - 1))
					inQueue[i - 1] = true;
			}
		}

		/// pushes element of value value with priority priority into the queue without checking whether it has been in the queue before
		void PushNoCheck(T priority, const size_t &value)
		{
			I3DASSERT_BT(value < inQueue.size(), "Trying to push value that is higher than allowed.");
			if (minimumPriority > priority)
				priority = minimumPriority;
			data[priority].push(value);
			inQueue[value] = true;
		}
		/// pushes element of value value with priority priority into the queue while checking it has not been in the queue before
		void Push(const T &priority, const size_t &value)
		{
			I3DASSERT_BT(value < inQueue.size(), "Trying to push value that is higher than allowed.");
			if (inQueue[value])
				return;
			PushNoCheck(priority, value);
		}
		/// pops the first element with the lowest priority from the queue
		void Pop()
		{
			I3DASSERT_BT(!data.empty() && !data.begin()->second.empty(), "You are trying to pop an element from an empty queue.");
			typename map<T, queue<size_t> >::iterator lowestPriorityPair = data.begin();
			lowestPriorityPair->second.pop();
			if (lowestPriorityPair->second.empty())
				data.erase(lowestPriorityPair++);
			if (!IsEmpty())
				minimumPriority = lowestPriorityPair->first;
		}
		/// returns the first element with the lowest priority from the queue
		size_t Top()
		{
			I3DASSERT_BT(!data.empty() && !data.begin()->second.empty(), "You are trying to get an element from an empty queue.");
			return data.begin()->second.front();
		}
		/// returns whether the queue is empty
		bool IsEmpty() const
		{
			return data.empty();
		}
		/// returns whether has specified value been inside the queue before
		bool HasBeenInside(const size_t &value) const
		{
			I3DASSERT_BT(value < inQueue.size(), "Trying to question value that is higher than allowed.");
			return inQueue[value];
		}

	private:
		/// stores the pairs representing the priority and a queue of values
		map<T, queue<size_t> > data;
		/// stores the information whether has specified value been in the queue before
		vector<bool> inQueue;
		/// minimum priority defining the basket that we are allowed to insert into
		T minimumPriority;
	};

	template <typename T> 
	I3D_DLLEXPORT void WatershedMeyer(const Image3d<T> &input, const Image3d<size_t> &marker, Image3d<size_t> &output, const Neighbourhood &neighbourhood)
	{
		I3DASSERT_ALWAYS_ET(input.GetImageSize() == marker.GetImageSize(), "Input and marker image size differs.");

		// copy markers into the output
		output.CopyMetaData(input);
		memcpy(output.GetFirstVoxelAddr(), marker.GetFirstVoxelAddr(), marker.GetImageSize() * sizeof(size_t));
		output.PadImage(1, 0);
		Image3d<T> inputExt(input);
		inputExt.PadImage(1, std::numeric_limits<T>::max());

		// start growing the output
		// initialize priority queue - insert all the non-labeled voxels (their indices) neighbouring with the so far labeled regions into it
		CWSMeyerQueue<T> voxelsToProcess(output);
		NeighbourhoodWalkerU<size_t> nbhWalkwer(output, neighbourhood);
		for (size_t i = output.GetImageSize(); i > 0; --i)
		{
			if (output.GetVoxel(i - 1))
			{
				for (nbhWalkwer.Begin(i - 1); nbhWalkwer.Valid(); nbhWalkwer.Next())
				{
					size_t nbhIndex = nbhWalkwer.GetIndex();
					if (output.GetVoxel(nbhIndex) == 0)
						voxelsToProcess.Push(inputExt.GetVoxel(nbhIndex), nbhIndex);
				}
			}
		}
		// while we have some unprocessed voxels in the priority queue, pick one with the lowest voxel value and process it
		vector<std::pair<T, size_t> > voxelsToProcessTmp;
		voxelsToProcessTmp.reserve(neighbourhood.size());
		while (!voxelsToProcess.IsEmpty())
		{
			size_t voxelIndex = voxelsToProcess.Top();
			I3DASSERT_BT(!output.GetVoxel(voxelIndex), "Voxel in the queue should not have label assigned.");
			// check the neighbourhood of the chosen voxel, when it contains just one label, mark it by that label and put all the unmarked neighbours that are (were) not in the queue into it
			bool ambiguousLabel = false;
			for (nbhWalkwer.Begin(voxelIndex); nbhWalkwer.Valid(); nbhWalkwer.Next())
			{
				size_t nbhIndex = nbhWalkwer.GetIndex();
				size_t nbhLabel = output.GetVoxel(nbhIndex);
				if (nbhLabel == 0)
				{
					if (!voxelsToProcess.HasBeenInside(nbhIndex))
						voxelsToProcessTmp.push_back(std::pair<T, size_t>(inputExt.GetVoxel(nbhIndex), nbhIndex));
					continue;
				}
				else if (output.GetVoxel(voxelIndex) == 0)
				{
					output.SetVoxel(voxelIndex, nbhLabel);
					continue;
				}
				else if (output.GetVoxel(voxelIndex) != nbhLabel)
					ambiguousLabel = true;
			}
			if (ambiguousLabel)
				output.SetVoxel(voxelIndex, 0);
			else
			{
				// in case we were able to chose label for this voxel (there was a single (non-zero) label in the neighbourhood), put all it's non-labeled neighbours into the queue
				if (output.GetVoxel(voxelIndex))
				{
					for (typename vector<std::pair<T, size_t> >::size_type i = voxelsToProcessTmp.size(); i > 0; --i)
					{
						typename vector<std::pair<T, size_t> >::size_type iMin1 = i - 1;
						voxelsToProcess.PushNoCheck(voxelsToProcessTmp[iMin1].first, voxelsToProcessTmp[iMin1].second);
					}
				}
			}
			voxelsToProcessTmp.clear();
			voxelsToProcess.Pop();
		}
		output.RemovePadding(1);
	}

	template <typename T> 
	I3D_DLLEXPORT void WatershedMeyer(const Image3d<T> &input, Image3d<size_t> &output, const Neighbourhood &neighbourhood)
	{
		// detect regional minima and construct the marker image
		Image3d<T> marker;
		marker.CopyMetaData(input);
		r_Min(input, marker, neighbourhood);
		Image3d<size_t> markerUI;
		markerUI.CopyMetaData(input);
		markerUI.SetAllVoxels(0);
		size_t label = 1;
		for (size_t i = marker.GetImageSize(); i > 0; --i)
		{
			size_t iMinusOne = i - 1;
			if (markerUI.GetVoxel(iMinusOne) == 0 && marker.GetVoxel(iMinusOne) == std::numeric_limits<T>::max())
				FloodFill(marker, markerUI, marker.GetX(iMinusOne), marker.GetY(iMinusOne), marker.GetZ(iMinusOne), label++, neighbourhood);
		}

		// call the watershed
		WatershedMeyer(input, markerUI, output, neighbourhood);
	}

	template I3D_DLLEXPORT void WatershedMeyer(const Image3d<i3d::GRAY8> &input, const Image3d<size_t> &marker, Image3d<size_t> &output, const Neighbourhood &neighbourhood);
	template I3D_DLLEXPORT void WatershedMeyer(const Image3d<i3d::GRAY16> &input, const Image3d<size_t> &marker, Image3d<size_t> &output, const Neighbourhood &neighbourhood);
	template I3D_DLLEXPORT void WatershedMeyer(const Image3d<float> &input, const Image3d<size_t> &marker, Image3d<size_t> &output, const Neighbourhood &neighbourhood);
	template I3D_DLLEXPORT void WatershedMeyer(const Image3d<i3d::GRAY8> &input, Image3d<size_t> &output, const Neighbourhood &neighbourhood);
	template I3D_DLLEXPORT void WatershedMeyer(const Image3d<i3d::GRAY16> &input, Image3d<size_t> &output, const Neighbourhood &neighbourhood);
	template I3D_DLLEXPORT void WatershedMeyer(const Image3d<float> &input, Image3d<size_t> &output, const Neighbourhood &neighbourhood);
	
	//--------------------------------------------------------------------------
	// Watershed contours geodesic saliency implementation - uses modified code of immersion watershed from ivLab
	//--------------------------------------------------------------------------

	template <class T> void WatershedCGS(const Image3d<T> &in, Image3d<T> &out, int connectivity)
	{
		I3DASSERT_ALWAYS_ET(connectivity == 4 || connectivity == 8 || connectivity == 6 || connectivity == 18 || connectivity == 26,
			"Unsupported connectivity required");

		out.MakeRoom(in.GetSize());
		ivLab3D::WatershedCGS(in.GetFirstVoxelAddr(), in.GetWidth(), in.GetHeight(), in.GetNumSlices(), out.GetFirstVoxelAddr(), connectivity);
	}

	template I3D_DLLEXPORT void WatershedCGS(const Image3d<i3d::GRAY8> &in_grad, Image3d<i3d::GRAY8> &out, int connectivity);
	template I3D_DLLEXPORT void WatershedCGS(const Image3d<i3d::GRAY16> &in_grad, Image3d<i3d::GRAY16> &out, int connectivity);
	template I3D_DLLEXPORT void WatershedCGS(const Image3d<float> &in_grad, Image3d<float> &out, int connectivity);

	//--------------------------------------------------------------------------
	// Watershed
	//--------------------------------------------------------------------------

	/** Computes cumulative frequency distribution for vector u and put it into v, where u is a vector with frequency distibution.
	\todo Secteni dvou histogramu? */
	static void Cumulative_freqdist(const vector<int> &u, vector<int> &v)
	{ 
		v[0] = u [0];
		for (size_t j = 1; j < u.size(); ++j)  
			v[j] = u[j] + v[j-1]; 
	} // Cumulative_freqdist

	/** Frequency_distribution determines the frequency distibution of each
	image gray level.
	\todo Dalsi histogram? */
	template <class T> static void Frequency_distribution(const Image3d<T> &img, vector<int> &v)
	{ 
		const T *s = img.GetFirstVoxelAddr();

		for (size_t i = 0; i < img.GetImageSize(); ++i)
		{ 
			++v[(int)*s];    
			++s;
		}   
	} // Frequency_distribution

	/** Sorts the pixels of input image img and put pointers to them into v by
	cumulative frequency distribution in u */
	template <class T> static void Sort_to_vector(const Image3d<T> &img, vector<int> &u, vector<int> &v)
	{
		const T *s = img.GetFirstVoxelAddr();

		for (size_t i = 0; i < img.GetImageSize(); ++i)
		{ 
			--u[(int)*s];      

			v[ u[(int)*s] ] = i;

			++s;
		}
	}

	// constants definition for Watershed
#	define MASK    -2
#	define WSHED    0
#	define INIT    -1
#	define INQUEUE -3

	template <class T> void Watershed(const Image3d<T> &in, Image3d<T> &out, const Neighbourhood &neib)
	{
		/* Because we need to use negative values for image pixels in this algorithm,
		we define help image img of signed type S */
		typedef int S;     
		Image3d<S> img;
		img.MakeRoom(out.GetWidth(),out.GetHeight(),out.GetNumSlices());

		S *r, *p = img.GetFirstVoxelAddr();
		S current_label = 0;
		bool flag = false;
		vector<S*> win;
		queue<S*> fifo;  
		int j;
		size_t k, index;

		vector<int> v_fd(numeric_limits<T>::max()+1);  // vector of frequency dustribution
		vector<int> v_cfd(numeric_limits<T>::max()+1);  // vector of cumulative fr. distr.
		vector<int> vect(in.GetImageSize());  // vector of indexes of sorted pixels
		vector<int>::iterator i1 = vect.begin(), i2 = vect.begin();

		for (p=img.GetFirstVoxelAddr(); p<img.GetFirstVoxelAddr()+img.GetImageSize(); ++p)
		{
			*p = INIT;  // Set value off all output pixels to initial value
		}

		// Sort the input pixels in the increasing order of their gray values
		Frequency_distribution(in, v_fd);
		Cumulative_freqdist(v_fd, v_cfd);
		Sort_to_vector(in, v_cfd, vect);

		for (T h = numeric_limits<T>::min(); h < numeric_limits<T>::max(); ++h)
		{ 
			for (j = 0; j < v_fd[(int)h]; ++j)  // for all input pixels with value = h
			{
				p = img.GetVoxelAddr(*i1);
				*p = MASK;
				GetWindow(img, img.GetX(*i1), img.GetY(*i1), img.GetZ(*i1), neib, win);

				for (k = 0; k < win.size(); ++k)
				{
					if ((*win[k] > 0) || (*win[k] == WSHED))
					{           
						*p = INQUEUE;
						fifo.push(p);
						k = win.size();
					}                         
				}
				++i1;    
			}

			while(!fifo.empty())
			{ 
				p = fifo.front(); fifo.pop();
				index=p - img.GetFirstVoxelAddr();
				GetWindow(img, img.GetX(index), img.GetY(index), img.GetZ(index), neib, win);

				for (k = 0; k < win.size(); ++k) {
					if (*win[k] > 0) { 
						if ((*p == INQUEUE) || ((*p == WSHED) && (flag == true)) ) {
							*p = *win[k];
						} else if ((*p > 0) && (*p != *win[k])) { 
							*p = WSHED;
							flag = false;
						}        
					} else { 
						if (*win[k] == WSHED) {
							if (*p == INQUEUE) { 
								*p = WSHED;
								flag = true;
							}        
						}
						if (*win[k] == MASK) { 
							*win[k] = INQUEUE;
							fifo.push(win[k]);
						}
					} 
				}
			}

			for (j = 0; j < v_fd[(int)h]; ++j)  // for all input pixels with value = h
			{ 
				p = img.GetVoxelAddr(*i2);      

				if (*p == MASK)
				{ 
					++current_label;
					fifo.push(p);
					*p = current_label;

					while(!fifo.empty())
					{
						r = fifo.front(); fifo.pop();
						index=r - img.GetFirstVoxelAddr();
						GetWindow(img, img.GetX(index), img.GetY(index), img.GetZ(index), neib, win);          
						for (k = 0; k < win.size(); ++k) {
							if (*win[k] == MASK) { 
								fifo.push(win[k]);
								*win[k] = current_label;
							}
						}
					}
				}
				++i2;
			}
		}
		out.ConvertFrom(img);
	} // Watershed

	template I3D_DLLEXPORT void Watershed(const Image3d<GRAY8> &in, Image3d<GRAY8> &out, const Neighbourhood &neib);

} // namespace i3d
