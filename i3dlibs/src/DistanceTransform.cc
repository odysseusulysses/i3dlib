/*
 * i3dlib - image manipulation library
 *
 * Copyright (C) 2000-2006   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** \file DistanceTransform.h
  \brief Distance transform routines - header.
  
  \author Jan Havelka <60609@mail.muni.cz> 2005
  
*/

#ifndef _MSC_VER
#include <cmath>
#endif
#include <valarray>
#include "DistanceTransform.h"

namespace i3d {
    
	/***************************************************************************\
	*
	*                               Local structures
	*
	\***************************************************************************/
	//--------------------------------------------------------------------------
	// voxelxy
	//--------------------------------------------------------------------------
	struct voxelxy {
		unsigned short int x;
		unsigned short int y;
	};

	//--------------------------------------------------------------------------
	// voxelxyz
	//--------------------------------------------------------------------------
	struct voxelxyz {
		unsigned short int x;
		unsigned short int y;
		unsigned short int z;
	};

	/***************************************************************************\
	*
	*                               Local constants
	*
	\***************************************************************************/
	//--------------------------------------------------------------------------
	// max_saito
	//--------------------------------------------------------------------------
    const float max_saito = 100000000; //std::numeric_limits<float>::max();

	//--------------------------------------------------------------------------
	// max_DT
	//--------------------------------------------------------------------------
    const float max_DT = 10000; //std::numeric_limits<float>::max();

	//--------------------------------------------------------------------------
	// Infinity
	//--------------------------------------------------------------------------
	const float Infinity = 10000;

	/***************************************************************************\
	*
	*                               Local functions
	*
	\***************************************************************************/
	//--------------------------------------------------------------------------
	// scanCityBlock
	//--------------------------------------------------------------------------
    /** Scan for algorithm that computes City Blocks distance. */
	template <typename T> void scanCityBlock(
		int pocx, 
		int konx,
		int pocy,
		int kony,
		int pocz, 
		int konz,
		int xmax, 
		int ymax, 
		int zmax,
		T xp, 
		T yp, 
		T zp,
		std::valarray<T> &mapa)
	{
		size_t index, index2;
		int i,j,k;
		int posunx=(pocx<konx)?1:-1, posuny=(pocy<kony)?1:-1,posunz=(pocz<konz)?1:-1;

		int kslide=(zmax>1)?(pocz-posunz):0;         

		//for 2D image
		//case for neighbour for any voxel on first line
		index=(kslide)*(xmax*ymax)+(pocy-posuny)*xmax+pocx;
		for(i=pocx;i!=konx+2*posunx;i+=posunx)
		{  
			index2=index-posunx;
			if(mapa[index]>mapa[index2]+xp) mapa[index]=mapa[index2]+xp;
			index+=posunx;
		}

		for(j=pocy;j!=kony+2*posuny;j+=posuny)
		{  
			//case for neighbour for first voxel on any line(except first and last)
			index=(kslide)*(xmax*ymax)+j*xmax+pocx-posunx;
			index2=index-posuny*xmax;
			if(mapa[index]>mapa[index2]+yp) mapa[index]=mapa[index2]+yp;
			//case for neighbour for any voxel on any line
			index=(kslide)*(xmax*ymax)+j*xmax+pocx;
			for(i=pocx;i!=konx+2*posunx;i+=posunx)
			{ 
				index2=index-posunx;
				if(mapa[index]>mapa[index2]+xp) mapa[index]=mapa[index2]+xp;
				index2=index-posuny*(xmax);
				if(mapa[index]>mapa[index2]+yp) mapa[index]=mapa[index2]+yp;
				index+=posunx;
			}
		}

		//for 3D image
		if(zmax>1)
		{  
			for(k=pocz;k!=konz+posunz;k+=posunz)
			{  
				//case for neighbour for first voxel on first line
				index=k*xmax*ymax+(pocy-posuny)*xmax+pocx-posunx;
				index2=index-posunz*(xmax*ymax);
				if(mapa[index]>mapa[index2]+zp) mapa[index]=mapa[index2]+zp;
				//case for neighbour for voxel on first line
				index=k*xmax*ymax+(pocy-posuny)*xmax+pocx;
				for(i=pocx;i!=konx+2*posunx;i+=posunx)
				{  
					index2=index-posunx;
					if(mapa[index]>mapa[index2]+xp) mapa[index]=mapa[index2]+xp;
					index2=index-posunz*(xmax*ymax);
					if(mapa[index]>mapa[index2]+zp) mapa[index]=mapa[index2]+zp;
					index+=posunx;
				}

				for(j=pocy;j!=kony+2*posuny;j+=posuny)
				{  
					//case for neighbour for first voxel on line
					index=k*xmax*ymax+j*xmax+pocx-posunx;
					index2=index-posuny*xmax;
					if(mapa[index]>mapa[index2]+yp) mapa[index]=mapa[index2]+yp;
					index2=index-posunz*(xmax*ymax);
					if(mapa[index]>mapa[index2]+zp) mapa[index]=mapa[index2]+zp;
					//case for neighbour for any voxel(expect first) on line
					index=k*xmax*ymax+j*xmax+pocx;
					for(i=pocx;i!=konx+2*posunx;i+=posunx)
					{  
						index2=index-posunx;
						if(mapa[index]>mapa[index2]+xp) mapa[index]=mapa[index2]+xp;
						index2=index-posuny*xmax;
						if(mapa[index]>mapa[index2]+yp) mapa[index]=mapa[index2]+yp;
						index2=index-posunz*(xmax*ymax);
						if(mapa[index]>mapa[index2]+zp) mapa[index]=mapa[index2]+zp;
						index+=posunx;
					}
				}
			}
		}
	} // scanCityBlock

	//--------------------------------------------------------------------------
	// scanChamfer
	//--------------------------------------------------------------------------
	/** Scan for algorithm that computes Chamfer distance. */
	template <typename T> void scanChamfer(
		int pocx, 
		int konx,
		int pocy,
		int kony,
		int pocz,
		int konz,
		int xmax,
		int ymax, 
		int zmax,
		T xp, 
		T yp, 
		T zp,
		std::valarray<T> &mapa)
	{  
		size_t index, index2;
		int i,j,k;
		int posunx=(pocx<konx)?1:-1, posuny=(pocy<kony)?1:-1,posunz=(pocz<konz)?1:-1;

		T diaxy=sqrt(xp*xp+yp*yp), diaxz=sqrt(xp*xp+zp*zp), diayz=sqrt(yp*yp+zp*zp), diaxyz=sqrt(xp*xp+yp*yp+zp*zp);
     
		int kslide=(zmax>1)?(pocz-posunz):0;         

		//for 2D image
		//case for neighbour for any voxel on first line
		index=(kslide)*(xmax*ymax)+(pocy-posuny)*xmax+pocx;
		for(i=pocx;i!=konx+2*posunx;i+=posunx)
		{  
			index2=index-posunx;
			if(mapa[index]>mapa[index2]+xp) mapa[index]=mapa[index2]+xp;
			index+=posunx;
		}

		for(j=pocy;j!=kony+2*posuny;j+=posuny)
		{  
			//case for neighbour for first voxel on any line(except first and last)
			index=(kslide)*(xmax*ymax)+j*xmax+pocx-posunx;
			index2=index-posuny*xmax;
			if(mapa[index]>mapa[index2]+yp) mapa[index]=mapa[index2]+yp;
			index2=index2+posunx;
			if(mapa[index]>mapa[index2]+diaxy) mapa[index]=mapa[index2]+diaxy;
			//case for neighbour for any voxel on any line
			index=(kslide)*(xmax*ymax)+j*xmax+pocx;
			for(i=pocx;i!=konx+posunx;i+=posunx)
			{  
				index2=index-posunx;
				if(mapa[index]>mapa[index2]+xp) mapa[index]=mapa[index2]+xp;
				index2=index-posuny*(xmax);
				if(mapa[index]>mapa[index2]+yp) mapa[index]=mapa[index2]+yp;
				index2=index2-posunx;
				if(mapa[index]>mapa[index2]+diaxy) mapa[index]=mapa[index2]+diaxy;
				index2=index2+2*posunx;
				if(mapa[index]>mapa[index2]+diaxy) mapa[index]=mapa[index2]+diaxy;
				index+=posunx;
			}
			//case for neighbour for last voxel on any line
			index=(kslide)*(xmax*ymax)+j*xmax+konx+posunx;
			index2=index-posunx;
			if(mapa[index]>mapa[index2]+xp) mapa[index]=mapa[index2]+xp;
			index2=index-posuny*(xmax);
			if(mapa[index]>mapa[index2]+yp) mapa[index]=mapa[index2]+yp;
			index2=index2-posunx;
			if(mapa[index]>mapa[index2]+diaxy) mapa[index]=mapa[index2]+diaxy;
		}

		//for 3D image
		if(zmax>1)
		{  
			for(k=pocz;k!=konz+posunz;k+=posunz)
			{  
				//case for neighbour for first voxel on first line
				index=k*xmax*ymax+(pocy-posuny)*xmax+pocx-posunx;
				index2=index-posunz*(xmax*ymax);
				if(mapa[index]>mapa[index2]+zp) mapa[index]=mapa[index2]+zp;
				index2=index2+posunx;
				if(mapa[index]>mapa[index2]+diaxz) mapa[index]=mapa[index2]+diaxz;
				index2=index2+posuny*xmax;
				if(mapa[index]>mapa[index2]+diaxyz) mapa[index]=mapa[index2]+diaxyz;
				index2=index2-posunx;
				if(mapa[index]>mapa[index2]+diayz) mapa[index]=mapa[index2]+diayz;
				//case for neighbour for voxel on first line
				index=k*xmax*ymax+(pocy-posuny)*xmax+pocx;
				for(i=pocx;i!=konx+posunx;i+=posunx)
				{  
					index2=index-posunx;
					if(mapa[index]>mapa[index2]+xp) mapa[index]=mapa[index2]+xp;
					index2=index-posunz*(xmax*ymax);
					if(mapa[index]>mapa[index2]+zp) mapa[index]=mapa[index2]+zp;
					index2=index2+posunx;
					if(mapa[index]>mapa[index2]+diaxz) mapa[index]=mapa[index2]+diaxz;
					index2=index2+posuny*xmax;
					if(mapa[index]>mapa[index2]+diaxyz) mapa[index]=mapa[index2]+diaxyz;
					index2=index2-posunx;
					if(mapa[index]>mapa[index2]+diayz) mapa[index]=mapa[index2]+diayz;
					index2=index2-posunx;
					if(mapa[index]>mapa[index2]+diaxyz) mapa[index]=mapa[index2]+diaxyz;
					index2=index2-posuny*xmax;
					if(mapa[index]>mapa[index2]+diaxz) mapa[index]=mapa[index2]+diaxz;
					index+=posunx;
				}
				//case for neighbour for last voxel on first line
				index=k*xmax*ymax+(pocy-posuny)*xmax+konx+posunx;
				index2=index-posunx;
				if(mapa[index]>mapa[index2]+xp) mapa[index]=mapa[index2]+xp;
				index2=index-posunz*(xmax*ymax);
				if(mapa[index]>mapa[index2]+zp) mapa[index]=mapa[index2]+zp;
				index2=index2+posuny*xmax;
				if(mapa[index]>mapa[index2]+diayz) mapa[index]=mapa[index2]+diayz;
				index2=index2-posunx;
				if(mapa[index]>mapa[index2]+diaxyz) mapa[index]=mapa[index2]+diaxyz;
				index2=index2-posuny*xmax;
				if(mapa[index]>mapa[index2]+diaxz) mapa[index]=mapa[index2]+diaxz;
				//case for neighbour for any line (expect first and last)
				for(j=pocy;j!=kony+posuny;j+=posuny)
				{  
					//case for neighbour for first voxel on line
					index=k*xmax*ymax+j*xmax+pocx-posunx;
					index2=index-posuny*xmax;                  
					if(mapa[index]>mapa[index2]+yp) mapa[index]=mapa[index2]+yp;                  
					index2=index2+posunx;
					if(mapa[index]>mapa[index2]+diaxy) mapa[index]=mapa[index2]+diaxy;                  
					index2=index-posunz*(xmax*ymax);
					if(mapa[index]>mapa[index2]+zp) mapa[index]=mapa[index2]+zp;                  
					index2=index2-posuny*xmax;
					if(mapa[index]>mapa[index2]+diayz) mapa[index]=mapa[index2]+diayz;                  
					index2=index2+posunx;;
					if(mapa[index]>mapa[index2]+diaxyz) mapa[index]=mapa[index2]+diaxyz;                 
					index2=index2+posuny*xmax;
					if(mapa[index]>mapa[index2]+diaxz) mapa[index]=mapa[index2]+diaxz;                  
					index2=index2+posuny*xmax;;
					if(mapa[index]>mapa[index2]+diaxyz) mapa[index]=mapa[index2]+diaxyz;                  
					index2=index2-posunx;
					if(mapa[index]>mapa[index2]+diayz) mapa[index]=mapa[index2]+diayz;
					//case for neighbour for any voxel(expect first and last) on line
					index=k*xmax*ymax+j*xmax+pocx;
					for(i=pocx;i!=konx+posunx;i+=posunx)
					{  
						//first mask(1,1,0)
						index2=index-posunx;
						if(mapa[index]>mapa[index2]+xp) mapa[index]=mapa[index2]+xp;
						index2=index2-posuny*xmax;
						if(mapa[index]>mapa[index2]+diaxy) mapa[index]=mapa[index2]+diaxy;
						index2=index2+posunx;
						if(mapa[index]>mapa[index2]+yp) mapa[index]=mapa[index2]+yp;
						index2=index2+posunx;
						if(mapa[index]>mapa[index2]+diaxy) mapa[index]=mapa[index2]+diaxy;
						//second mask(1,1,1)
						index2=index-posunz*(xmax*ymax);
						if(mapa[index]>mapa[index2]+zp) mapa[index]=mapa[index2]+zp;
						index2=index2-posunx;
						if(mapa[index]>mapa[index2]+diaxz) mapa[index]=mapa[index2]+diaxz;
						index2=index2-posuny*xmax;
						if(mapa[index]>mapa[index2]+diaxyz) mapa[index]=mapa[index2]+diaxyz;
						index2=index2+posunx;
						if(mapa[index]>mapa[index2]+diayz) mapa[index]=mapa[index2]+diayz;
						index2=index2+posunx;
						if(mapa[index]>mapa[index2]+diaxyz) mapa[index]=mapa[index2]+diaxyz;
						index2=index2+posuny*xmax;
						if(mapa[index]>mapa[index2]+diaxz) mapa[index]=mapa[index2]+diaxz;
						index2=index2+posuny*xmax;
						if(mapa[index]>mapa[index2]+diaxyz) mapa[index]=mapa[index2]+diaxyz;
						index2=index2-posunx;
						if(mapa[index]>mapa[index2]+diayz) mapa[index]=mapa[index2]+diayz;
						index2=index2-posunx;
						if(mapa[index]>mapa[index2]+diaxyz) mapa[index]=mapa[index2]+diaxyz;
						index+=posunx;
					}
					//case for neighbour for last voxel on line
					index=k*xmax*ymax+j*xmax+konx+posunx;
					index2=index-posunx;
					if(mapa[index]>mapa[index2]+xp) mapa[index]=mapa[index2]+xp;
					index2=index-posuny*xmax;
					if(mapa[index]>mapa[index2]+yp) mapa[index]=mapa[index2]+yp;
					index2=index2-posunx;
					if(mapa[index]>mapa[index2]+diaxy) mapa[index]=mapa[index2]+diaxy;
					index2=index-posunz*(xmax*ymax);
					if(mapa[index]>mapa[index2]+zp) mapa[index]=mapa[index2]+zp;
					index2=index2-posuny*xmax;
					if(mapa[index]>mapa[index2]+diayz) mapa[index]=mapa[index2]+diayz;
					index2=index2-posunx;
					if(mapa[index]>mapa[index2]+diaxyz) mapa[index]=mapa[index2]+diaxyz;
					index2=index2+posuny*xmax;
					if(mapa[index]>mapa[index2]+diaxz) mapa[index]=mapa[index2]+diaxz;
					index2=index2+posuny*xmax;
					if(mapa[index]>mapa[index2]+diaxyz) mapa[index]=mapa[index2]+diaxyz;
					index2=index2+posunx;
					if(mapa[index]>mapa[index2]+diayz) mapa[index]=mapa[index2]+diayz;
				}
				//case for neighbour for first voxel on last line
				index=k*xmax*ymax+(kony+posuny)*xmax+pocx-posunx;
				index2=index-posuny*xmax;
				if(mapa[index]>mapa[index2]+yp) mapa[index]=mapa[index2]+yp;
				index2=index2+posunx;
				if(mapa[index]>mapa[index2]+diaxy) mapa[index]=mapa[index2]+diaxy;
				index2=index-posunz*(xmax*ymax);
				if(mapa[index]>mapa[index2]+zp) mapa[index]=mapa[index2]+zp;
				index2=index2-posuny*xmax;
				if(mapa[index]>mapa[index2]+diayz) mapa[index]=mapa[index2]+diayz;
				index2=index2+posunx;
				if(mapa[index]>mapa[index2]+diaxyz) mapa[index]=mapa[index2]+diaxyz;
				index2=index2+posuny*xmax;
				if(mapa[index]>mapa[index2]+diaxz) mapa[index]=mapa[index2]+diaxz;
				//case for neighbour for any voxel(except first and last) on last line
				index=k*xmax*ymax+(kony+posuny)*xmax+pocx;
				for(i=pocx;i!=konx+posunx;i+=posunx)
				{  
					index2=index-posunx;
					if(mapa[index]>mapa[index2]+xp) mapa[index]=mapa[index2]+xp;
					index2=index2-posuny*xmax;
					if(mapa[index]>mapa[index2]+diaxy) mapa[index]=mapa[index2]+diaxy;
					index2=index2+posunx;
					if(mapa[index]>mapa[index2]+yp) mapa[index]=mapa[index2]+yp;
					index2=index2+posunx;
					if(mapa[index]>mapa[index2]+diaxy) mapa[index]=mapa[index2]+diaxy;
					index2=index-posunz*(xmax*ymax);
					if(mapa[index]>mapa[index2]+zp) mapa[index]=mapa[index2]+zp;
					index2=index2-posunx;
					if(mapa[index]>mapa[index2]+diaxz) mapa[index]=mapa[index2]+diaxz;
					index2=index2-posuny*xmax;
					if(mapa[index]>mapa[index2]+diaxyz) mapa[index]=mapa[index2]+diaxyz;
					index2=index2+posunx;
					if(mapa[index]>mapa[index2]+diayz) mapa[index]=mapa[index2]+diayz;
					index2=index2+posunx;
					if(mapa[index]>mapa[index2]+diaxyz) mapa[index]=mapa[index2]+diaxyz;
					index2=index2+posuny*xmax;
					if(mapa[index]>mapa[index2]+diaxz) mapa[index]=mapa[index2]+diaxz;
					index+=posunx;
				}
				//case for neighbour for last voxel on last line
				index=k*xmax*ymax+(kony+posuny)*xmax+konx+posunx;
				index2=index-posunx;
				if(mapa[index]>mapa[index2]+xp) mapa[index]=mapa[index2]+xp;
				index2=index2-posuny*xmax;
				if(mapa[index]>mapa[index2]+diaxy) mapa[index]=mapa[index2]+diaxy;
				index2=index2-posunx;
				if(mapa[index]>mapa[index2]+yp) mapa[index]=mapa[index2]+yp;
				index2=index-posunz*(xmax*ymax);
				if(mapa[index]>mapa[index2]+zp) mapa[index]=mapa[index2]+zp;
				index2=index2-posunx;
				if(mapa[index]>mapa[index2]+diaxz) mapa[index]=mapa[index2]+diaxz;
				index2=index2-posuny*xmax;
				if(mapa[index]>mapa[index2]+diaxyz) mapa[index]=mapa[index2]+diaxyz;
				index2=index2+posunx;
				if(mapa[index]>mapa[index2]+diayz) mapa[index]=mapa[index2]+diayz;
			}
		}
	} // scanChamfer

	//--------------------------------------------------------------------------
	// scan4N
	//--------------------------------------------------------------------------
	/* Scan for Danielsson's 4SED algrithm. */
	template<typename T> void scan4N(
		int pocx, // begin of scan
		int konx, // end of scan
		int pocy,
		int kony,
		size_t xm,
		size_t ym, // size of array field
		std::valarray<T> &mocninax, 
		std::valarray<T> &mocninay,
		std::valarray<voxelxy> &pole)// distance map
	{  
		int i,j;
		size_t index,index2;
		int posuny=(pocy<kony)?1:-1; // determination direction of scan

		for(j=pocy;j!=kony+posuny;j+=posuny)
		{  
			index=j*xm;
			index2=index-posuny*xm;
			for(i=pocx;i<=konx;i++)  // minimum from [i,j],[i,j-posuny]
			{  
				if((mocninax[pole[index].x] + mocninay[pole[index].y])> 
				   (mocninax[pole[index2].x] + mocninay[pole[index2].y+1]))
				{  
					pole[index].x=pole[index2].x;
					pole[index].y=pole[index2].y+1;
				}
				index+=1;
				index2+=1;
			}

			index2=j*xm;
			index=index2+1;
			for(i=pocx+1;i<=konx;i++)  // minimum from [i,j],[i-1,j]
			{  
				if((mocninax[pole[index].x] + mocninay[pole[index].y])>
				   (mocninax[pole[index2].x+1] + mocninay[pole[index2].y]))
				{  
					pole[index].x=pole[index2].x+1;
					pole[index].y=pole[index2].y;
				}
				index+=1;
				index2+=1;
			}

			index2=(j+1)*xm-1;
			index=index2-1;
			for(i=konx-1;pocx<=i;i--) // minimum from [i,j],[i+1,j]
			{  
				if((mocninax[pole[index].x] + mocninay[pole[index].y])>
				   (mocninax[pole[index2].x+1] + mocninay[pole[index2].y]))
				{  
					pole[index].x=pole[index2].x+1;
					pole[index].y=pole[index2].y;
				}
				index-=1;
				index2-=1;
			}
		}
	} // scan4N

	//--------------------------------------------------------------------------
	// scan8N
	//--------------------------------------------------------------------------
    /* Scan for Danielsson's 8SED algrithm. */
	template<typename T> void scan8N(
		int pocx,
		int konx, // begin and end of scan
		int pocy,
		int kony,
		size_t xm, 
		size_t ym, // size of array field
		std::valarray<T> &mocninax, 
		std::valarray<T> &mocninay,
		std::valarray<voxelxy> &pole)// distance map
	{  
		int i,j;
		size_t index, index2;
		int posuny=(pocy<kony)?1:-1;  // determination direction of scan
		T v1,v2;

		for(j=pocy;j!=kony+posuny;j+=posuny)
		{  
			for(i=pocx+1;i<=konx-1;i++)// minimum from (-1,-posuny),(0,-posuny),(1,-posuny)
			{  
				index=j*xm+i;
				index2=index-posuny*xm-1;
				v1=mocninax[pole[index].x]+mocninay[pole[index].y];
				v2=mocninax[pole[index2].x+1]+mocninay[pole[index2].y+1];
				if(v1>v2)
				{  
					pole[index].x=pole[index2].x+1;
					pole[index].y=pole[index2].y+1;
					v1=v2;
				}

				index2+=1;
				v2=mocninax[pole[index2].x]+mocninay[pole[index2].y+1];
				if(v1>v2)
				{  
					pole[index].x=pole[index2].x;
					pole[index].y=pole[index2].y+1;
					v1=v2;
				}

				index2+=1;
				v2=mocninax[pole[index2].x+1]+mocninay[pole[index2].y+1];
				if(v1>v2)
				{  
					pole[index].x=pole[index2].x+1;
					pole[index].y=pole[index2].y+1;
				}
			}

			index2=j*xm;
			index=index2+1;
			for(i=pocx+1;i<=konx;i++) // minimum from (-1,0),(0,0)
			{  
				if((mocninax[pole[index].x]+mocninay[pole[index].y])>
				   (mocninax[pole[index2].x+1]+mocninay[pole[index2].y]))
				{  
					pole[index].x=pole[index2].x+1;
					pole[index].y=pole[index2].y;
				}
				index+=1;
				index2+=1;
			}

			index2=(j+1)*xm-1;
			index=index2-1; // minimum from (0,0),(1,0)
			for(i=konx-1;pocx<=i;i--)
			{  
				if((mocninax[pole[index].x]+mocninay[pole[index].y])>
				   (mocninax[pole[index2].x+1]+mocninay[pole[index2].y]))
				{ 
					pole[index].x=pole[index2].x+1;
					pole[index].y=pole[index2].y;
				}
				index-=1;
				index2-=1;
			}
		}
	} // scan8N

	//--------------------------------------------------------------------------
	// EDM2D
	//--------------------------------------------------------------------------
	/* Danielsson's 4SED and 8SED algorithm. This algorithm is determined for 2D pictures. */
	template<typename T> void EDM2D(Image3d<T> &Obraz, byte kvalita, T Image_Background, bool odmocnina)
	{  
		const int xmax=Obraz.GetWidth(), ymax=Obraz.GetHeight(); // size of picture

		Vector3d<float> res=Obraz.GetResolution().GetRes();
		const T xp=1.0/res.x, // value of one x step
			    yp=1.0/res.y; // value of one y step
		const T xp2=xp*xp, yp2=yp*yp;

		int i, konec;

		std::valarray<voxelxy> pole(xmax*ymax);
		std::valarray<T> &pObraz = Obraz.GetVoxelData();

		std::valarray<T> mocninax(xmax+2), mocninay(ymax+2);
		for(i=0;i<=xmax+1;i++) mocninax[i]=xp2*i*i;
		for(i=0;i<=ymax+1;i++) mocninay[i]=yp2*i*i;

		//  initialization of algorithm
		konec=(ymax-1)*xmax+xmax-1;
		for(i=0;i<=konec;i++)
		{  
			if(pObraz[i]!=Image_Background)
			{  
				// white pixels are setting on zero
				pole[i].x=0;
				pole[i].y=0;
			}
			else 
			{  
				// black pixels are setting on infinite
				pole[i].x=xmax;
				pole[i].y=ymax;
			}
		}
		// end of initialization

		if(kvalita==0)
		{  
			// first scan from (-1,0), (0,-1), (1,0)
			scan4N(0,xmax-1,1,ymax-1,xmax,ymax,mocninax,mocninay,pole);
			// second scan from (-1,0), (0,1), (1,0)
			scan4N(0,xmax-1,ymax-2,0,xmax,ymax,mocninax,mocninay,pole);
		}
		else 
		{  
			// first scan from (-1,0), (0,-1), (1,0)
			scan8N(0,xmax-1,1,ymax-1,xmax,ymax,mocninax,mocninay,pole);
			// second scan from (-1,0), (0,1), (1,0)
			scan8N(0,xmax-1,ymax-2,0,xmax,ymax,mocninax,mocninay,pole);
		}

		//save results
		if(pole[0].x==((unsigned int)xmax))
		{  
			konec=(ymax-1)*xmax+xmax-1;
			for(i=0;i<=konec;i++) pObraz[i]=max_DT;
		}
		else 
		{  
			if(odmocnina)
			{  
				konec=(ymax-1)*xmax+xmax-1;
				for(i=0;i<=konec;i++)
					pObraz[i]=sqrt(mocninax[pole[i].x]+mocninay[pole[i].y]);
			}
			else 
			{
				konec=(ymax-1)*xmax+xmax-1;
				for(i=0;i<=konec;i++)
					pObraz[i]=mocninax[pole[i].x]+mocninay[pole[i].y];
			}
		}
	} // EDM2D

	//--------------------------------------------------------------------------
	// scan6N
	//--------------------------------------------------------------------------
	/* Scan for 3D Images with using 6-neighbours. */
	template<typename T> void scan6N(
		int pocx,
		int konx, // begin and end of scan in x
		int pocy,
		int kony, // begin and end of scan in y
		int pocz,
		int konz, // begin and end of scan in z
		size_t xm,
		size_t ym,
		size_t zm, // size of array field
		std::valarray<T> &mocninax, 
		std::valarray<T> &mocninay, 
		std::valarray<T> &mocninaz,
		std::valarray<voxelxyz> &pole)// distance map
	{
		int i,j,k;
		T v1,v2;
		size_t index,index2;

		// determination direction of scan
		int posunx=(pocx<konx)?1:-1,posuny=(pocy<kony)?1:-1,posunz=(pocz<konz)?1:-1;

		// minimum from (0,0,0), (-posunx,0,0), (0,-posuny,0), (0,0,-posunz)
		for(k=pocz;k!=konz+posunz;k+=posunz)
			for(j=pocy;j!=kony+posuny;j+=posuny)
				for(i=pocx;i!=konx+posunx;i+=posunx)
				{
					index=k*(xm*ym)+j*xm+i;
					index2=index-posunx;
					v1=mocninax[pole[index].x]+mocninay[pole[index].y]+mocninaz[pole[index].z];
					v2=mocninax[pole[index2].x+1]+mocninay[pole[index2].y]+mocninaz[pole[index2].z];
					if(v1>v2)
					{  
						pole[index].x=pole[index2].x+1;
						pole[index].y=pole[index2].y;
						pole[index].z=pole[index2].z;
						v1=v2;
					}

					index2=index-posuny*xm;
					v2=mocninax[pole[index2].x]+mocninay[pole[index2].y+1]+mocninaz[pole[index2].z];
					if(v1>v2)
					{  
						pole[index].x=pole[index2].x;
						pole[index].y=pole[index2].y+1;
						pole[index].z=pole[index2].z;
						v1=v2;
					}

					index2=index-posunz*xm*ym;
					v2=mocninax[pole[index2].x]+mocninay[pole[index2].y]+mocninaz[pole[index2].z+1];
					if(v1>v2)
					{  
						pole[index].x=pole[index2].x;
						pole[index].y=pole[index2].y;
						pole[index].z=pole[index2].z+1;
					}
				}
	} // scan6N

	//--------------------------------------------------------------------------
	// scan26N
	//--------------------------------------------------------------------------
	/* Scan for 3D Images with using 26-neighbours. */
	template<typename T> void scan26N(
		int pocx,
		int konx, // begin and end of scan in x
		int pocy,
		int kony, // begin and end of scan in y
		int pocz,
		int konz, // begin and end of scan in z
        size_t xm,
		size_t ym,
		size_t zm, // size of array field
		std::valarray<T> &mocninax, 
		std::valarray<T> &mocninay, 
		std::valarray<T> &mocninaz,
		std::valarray<voxelxyz> &pole) // distance map
	{  
		int i,j,k;
		T v1,v2;
		size_t index,index2;

		// determination direction of scan
		int posunx=(pocx<konx)?1:-1,posuny=(pocy<kony)?1:-1,posunz=(pocz<konz)?1:-1;

		/* minimum from (0,0,0), (-posunx,0,0), (0,-posuny,0), (0,-posuny,-posunx),
		(0,0,-posunz), (-posunx,0,-posunz), (0,-posuny,-posunz),
		(-posunx,-posuny,-posunz)
		*/
		for(k=pocz;k!=konz+posunz;k+=posunz)
			for(j=pocy;j!=kony+posuny;j+=posuny)
				for(i=pocx;i!=konx+posunx;i+=posunx)
				{  
					index=k*(xm*ym)+j*xm+i;
					index2=index-posunx;
					v1=mocninax[pole[index].x]+mocninay[pole[index].y]+mocninaz[pole[index].z];
					v2=mocninax[pole[index2].x+1]+mocninay[pole[index2].y]+mocninaz[pole[index2].z];
					if(v1>v2)
					{
						pole[index].x=pole[index2].x+1;
						pole[index].y=pole[index2].y;
						pole[index].z=pole[index2].z;
						v1=v2;
					}

					index2=index-posuny*xm;
					v2=mocninax[pole[index2].x]+mocninay[pole[index2].y+1]+mocninaz[pole[index2].z];
					if(v1>v2)
					{
						pole[index].x=pole[index2].x;
						pole[index].y=pole[index2].y+1;
						pole[index].z=pole[index2].z;
						v1=v2;
					}

					index2-=posunx;
					v2=mocninax[pole[index2].x+1]+mocninay[pole[index2].y+1]+mocninaz[pole[index2].z];
					if(v1>v2)
					{
						pole[index].x=pole[index2].x+1;
						pole[index].y=pole[index2].y+1;
						pole[index].z=pole[index2].z;
						v1=v2;
					}

					index2=index-posunz*xm*ym;
					v2=mocninax[pole[index2].x]+mocninay[pole[index2].y]+mocninaz[pole[index2].z+1];
					if(v1>v2)
					{  
						pole[index].x=pole[index2].x;                 
						pole[index].y=pole[index2].y;
						pole[index].z=pole[index2].z+1;
						v1=v2;
					}

					index2-=posunx;
					v2=mocninax[pole[index2].x+1]+mocninay[pole[index2].y]+mocninaz[pole[index2].z+1];
					if(v1>v2)
					{  
						pole[index].x=pole[index2].x+1;
						pole[index].y=pole[index2].y;
						pole[index].z=pole[index2].z+1;
						v1=v2;
					}

					index2=index-posunz*xm*ym-posuny*xm;
					v2=mocninax[pole[index2].x]+mocninay[pole[index2].y+1]+mocninaz[pole[index2].z+1];
					if(v1>v2)
					{  
						pole[index].x=pole[index2].x;
						pole[index].y=pole[index2].y+1;
						pole[index].z=pole[index2].z+1;
						v1=v2;
					}

					index2-=posunx;
					v2=mocninax[pole[index2].x+1]+mocninay[pole[index2].y+1]+mocninaz[pole[index2].z+1];
					if(v1>v2)
					{  
						pole[index].x=pole[index2].x+1;
						pole[index].y=pole[index2].y+1;
						pole[index].z=pole[index2].z+1;
					}
				}
	} // scan26N

	//--------------------------------------------------------------------------
	// EDM3D
	//--------------------------------------------------------------------------
	/* Corner EDM Algoritm for 3D Images and Images with low dimension.
	This Algoritm uses 6-Neighbours or 26-Neighbours for computing distance map. */
	template <typename T> void EDM3D(Image3d<T> &Obraz, byte kvalita, T Image_Background, bool odmocnina)
	{  
		const int xmax=Obraz.GetWidth(), ymax=Obraz.GetHeight(), zmax=Obraz.GetNumSlices();// size of picture

		Vector3d<float> res=Obraz.GetResolution().GetRes();
		const T xp=1.0/res.x, // value of one x step
			    yp=1.0/res.y, // value of one y step
				zp=1.0/res.z; // value of one y step
		const T xp2=xp*xp, yp2=yp*yp, zp2=zp*zp;

		int i,j,k, konec;

		size_t index, index2;

		std::valarray<voxelxyz> pole((xmax+2)*(ymax+2)*(zmax+2));
		std::valarray<T> &pObraz = Obraz.GetVoxelData();

		std::valarray<T> mocninax(xmax+2), mocninay(ymax+2), mocninaz(zmax+2);
		for(i=0;i<=xmax+1;i++) mocninax[i]=xp2*i*i;
		for(i=0;i<=ymax+1;i++) mocninay[i]=yp2*i*i;
		for(i=0;i<=zmax+1;i++) mocninaz[i]=zp2*i*i;

		//  initialization of algorithm
		for(j=0;j<ymax+2;j++)
		{  
			index=j*(xmax+2);
			index2=(zmax+1)*(xmax+2)*(ymax+2)+j*(xmax+2);
			for(i=0;i<xmax+2;i++)
			{ 
				pole[index].x=xmax;
				pole[index].y=ymax;
				pole[index].z=zmax;
				pole[index2].x=xmax;
				pole[index2].y=ymax;

				pole[index2].z=zmax;
				index+=1;
				index2+=1;
			}
		}

		for(k=0;k<zmax+2;k++)
		{  
			index=k*(xmax+2)*(ymax+2);
			index2=k*(xmax+2)*(ymax+2)+(ymax+1)*(xmax+2);
			for(i=0;i<xmax+2;i++)
			{  
				pole[index].x=xmax;
				pole[index].y=ymax;
				pole[index].z=zmax;
				pole[index2].x=xmax;
				pole[index2].y=ymax;
				pole[index2].z=zmax;
				index+=1;
				index2+=1;
			}
		}

		for(k=0;k<zmax+1;k++)
		{  
			index=k*(xmax+2)*(ymax+2);
			index2=k*(xmax+2)*(ymax+2)+xmax+1;
			for(j=0;j<ymax+2;j++)
			{  
				pole[index].x=xmax;
				pole[index].y=ymax;
				pole[index].z=zmax;
				pole[index2].x=xmax;
				pole[index2].y=ymax;
				pole[index2].z=zmax;
				index+=(xmax+2);
				index2+=(xmax+2);
			}
		}

		index=0;
		for(k=0;k<zmax;k++)
			for(j=0;j<ymax;j++)
			{  
				index2=(k+1)*(xmax+2)*(ymax+2)+(j+1)*(xmax+2)+1;
				for(i=0;i<xmax;i++)
				{  
					if(pObraz[index]==Image_Background)
					{  
						pole[index2].x=xmax;
						pole[index2].y=ymax;
						pole[index2].z=zmax;
					}
					else 
					{  
						pole[index2].x=0;
						pole[index2].y=0;
						pole[index2].z=0;
					}
					index+=1;
					index2+=1;
				}
			}
			
		// end of initialization
		if(kvalita==0)
		{  
			// 1 scan from neighbours (1,0,0), (0,1,0), (0,0,1)
			scan6N(xmax,1,ymax,1,zmax,1,xmax+2,ymax+2,zmax+2,mocninax,mocninay,mocninaz,pole);
			// 2 scan from neighbours (-1,0,0), (0,-1,0), (0,0,-1)
			scan6N(1,xmax,1,ymax,1,zmax,xmax+2,ymax+2,zmax+2,mocninax,mocninay,mocninaz,pole);
			// 3 scan from neighbours (-1,0,0), (0,1,0), (0,0,1)
			scan6N(1,xmax,ymax,1,zmax,1,xmax+2,ymax+2,zmax+2,mocninax,mocninay,mocninaz,pole);
			// 4 scan from neighbours (1,0,0), (0,-1,0), (0,0,-1)
			scan6N(xmax,1,1,ymax,1,zmax,xmax+2,ymax+2,zmax+2,mocninax,mocninay,mocninaz,pole);
			// 5 scan from neighbours (-1,0,0), (0,-1,0), (0,0,1)
			scan6N(1,xmax,1,ymax,zmax,1,xmax+2,ymax+2,zmax+2,mocninax,mocninay,mocninaz,pole);
			// 6 scan from neighbours (1,0,0), (0,1,0), (0,0,-1)
			scan6N(xmax,1,ymax,1,1,zmax,xmax+2,ymax+2,zmax+2,mocninax,mocninay,mocninaz,pole);
			// 7 scan from neighbours (1,0,0), (0,-1,0), (0,0,1)
			scan6N(xmax,1,1,ymax,zmax,1,xmax+2,ymax+2,zmax+2,mocninax,mocninay,mocninaz,pole);
			// 8 scan from neighbours (-1,0,0), (0,1,0), (0,0,-1)
			scan6N(1,xmax,ymax,1,1,zmax,xmax+2,ymax+2,zmax+2,mocninax,mocninay,mocninaz,pole);
		}
		else 
		{  
			// 1 scan with direction (1,1,1)
			scan26N(xmax,1,ymax,1,zmax,1,xmax+2,ymax+2,zmax+2,mocninax,mocninay,mocninaz,pole);
			// 2 scan with direction (-1,-1,-1)
			scan26N(1,xmax,1,ymax,1,zmax,xmax+2,ymax+2,zmax+2,mocninax,mocninay,mocninaz,pole);
			// 3 scan with direction (-1,1,1)
			scan26N(1,xmax,ymax,1,zmax,1,xmax+2,ymax+2,zmax+2,mocninax,mocninay,mocninaz,pole);
			// 4 scan with direction (1,-1,-1)
			scan26N(xmax,1,1,ymax,1,zmax,xmax+2,ymax+2,zmax+2,mocninax,mocninay,mocninaz,pole);
			// 5 scan with direction (-1,-1,1)
			scan26N(1,xmax,1,ymax,zmax,1,xmax+2,ymax+2,zmax+2,mocninax,mocninay,mocninaz,pole);
			// 6 scan with direction (1,1,-1)
			scan26N(xmax,1,ymax,1,1,zmax,xmax+2,ymax+2,zmax+2,mocninax,mocninay,mocninaz,pole);
			// 7 scan with direction (1,-1,1)
			scan26N(xmax,1,1,ymax,zmax,1,xmax+2,ymax+2,zmax+2,mocninax,mocninay,mocninaz,pole);
			// 8 scan with direction (-1,1,-1)
			scan26N(1,xmax,ymax,1,1,zmax,xmax+2,ymax+2,zmax+2,mocninax,mocninay,mocninaz,pole);
		}

		//save results
		if(pole[(xmax+2)*(ymax+2)+(xmax+2)+1].x==((unsigned int)xmax))
		{  
			konec=(zmax-1)*xmax*ymax+(ymax-1)*xmax+xmax-1;
			for(i=0;i<=konec;i++) pObraz[i]=max_DT;
		}
		else 
		{  
			if(odmocnina)
			{  
				index=0;
				for(k=1;k<zmax+1;k++)
					for(j=1;j<ymax+1;j++)
					{  
						index2=k*(xmax+2)*(ymax+2)+j*(xmax+2)+1;
						for(i=1;i<xmax+1;i++)
						{  
							pObraz[index]=sqrt(mocninax[pole[index2].x]+
													 mocninay[pole[index2].y]+
													 mocninaz[pole[index2].z]);
							index+=1;
							index2+=1;
						}
					}
			}		      
			else 
			{  
				index=0;
				for(k=1;k<zmax+1;k++)
					for(j=1;j<ymax+1;j++)
					{  
						index2=k*(xmax+2)*(ymax+2)+j*(xmax+2)+1;
						for(i=1;i<xmax+1;i++)
						{  
							pObraz[index]=
									  mocninax[pole[index2].x]+
									  mocninay[pole[index2].y]+ 
									  mocninaz[pole[index2].z];
							index+=1;
							index2+=1;
						}
					}
			}
		}
	} // EDM3D

	//--------------------------------------------------------------------------
	// Saito4Sed
	//--------------------------------------------------------------------------
	/** This algorithm is combination of Danielsson's algoritm and Saito's algorithm. */
	template <typename T> void Saito4Sed(Image3d<T> &Obraz, T Image_Background, bool odmocnina)
	{  
		const int xmax=Obraz.GetWidth(), ymax=Obraz.GetHeight(), zmax=Obraz.GetNumSlices();// size of picture

		Vector3d<float> res=Obraz.GetResolution().GetRes();
		const T xp=1.0/res.x, // value of one x step
			    yp=1.0/res.y, // value of one y step
				zp=1.0/res.z; // value of one y step
		const T xp2=xp*xp, yp2=yp*yp, zp2=zp*zp;

		int i, j ,k;

		size_t n, a, index, index2, konec;

		T b, m;

		bool nkonec;

		std::valarray<voxelxy> pole(xmax*ymax);
		std::valarray<T> &pObraz = Obraz.GetVoxelData();
		std::valarray<T> mocninax(xmax+2), mocninay(ymax+2);
		for(i=0;i<=xmax+1;i++) mocninax[i]=xp2*i*i;
		for(i=0;i<=ymax+1;i++) mocninay[i]=yp2*i*i;

		std::valarray<T> buff(zmax), mocnina(zmax+1);

		for(k=0;k<zmax;k++)
		{  
			index=k*xmax*ymax;
			index2=0;
			for(j=0;j<ymax;j++)
			{  
				for(i=0;i<xmax;i++)
				{  
					if(pObraz[index]==Image_Background)
					{  
						pole[index2].x=xmax;
						pole[index2].y=ymax;
					}
					else 
					{ 
						pole[index2].x=0;
						pole[index2].y=0;
					}
					index+=1;
					index2+=1;
				}
			}

			// first scan from (-1,0), (0,-1), (1,0)
			scan4N(0,xmax-1,1,ymax-1,xmax,ymax,mocninax,mocninay,pole);
			// second scan from (-1,0), (0,1), (1,0)
			scan4N(0,xmax-1,ymax-2,0,xmax,ymax,mocninax,mocninay,pole);

			index=k*xmax*ymax;
			if(pole[0].x==xmax)
			{  
				for(j=0;j<ymax;j++)
					for(i=0;i<xmax;i++)
					{  
						pObraz[index]=max_saito;
						index+=1;
					}
			}
			else 
			{ 
				if((zmax==1)&&(odmocnina))
				{  
					index2=0;
					for(j=0;j<ymax;j++)
						for(i=0;i<xmax;i++)
						{  
							pObraz[index]=sqrt(mocninax[pole[index2].x]+mocninay[pole[index2].y]);
							index+=1;
							index2+=1;
						}
						odmocnina=false;
				}
				else 
				{  
					index2=0;
					for(j=0;j<ymax;j++)
						for(i=0;i<xmax;i++)
						{  
							pObraz[index]=mocninax[pole[index2].x]+mocninay[pole[index2].y];
							index+=1;
							index2+=1;
						}
				}
			}
		}

		//step 3
		if(zmax>1)
		{  
			for(k=0;k<=zmax;k++) mocnina[k]=zp2*k*k;
			for(j=0;j<ymax;j++)
				for(i=0;i<xmax;i++)
				{  
					index=j*xmax+i;
					for(k=0;k<zmax;k++)
					{ 
						buff[k]=pObraz[index];
						index+=(xmax*ymax);
					}

					//forward scan
					a=0;
					for(k=1;k<zmax;k++)
					{ 
						if (a>0) a-=1;
						if (buff[k]>buff[k-1]+zp2)
						{  
							b=((buff[k]-buff[k-1]-zp2)/(2*zp2));
							if ((k+b)>(zmax-1)) b=(zmax-1)-k;
							index=(k+a)*xmax*ymax+j*xmax+i;
							nkonec=true;
							for(n=a;((n<=b)&&(nkonec));n++)
							{ 
								m=buff[k-1]+ mocnina[n+1];
								if(buff[k+n]<=m) 
								{ 
									nkonec=false;
								}
								else 
								{  
									if(pObraz[index]>m)
										pObraz[index]=m;                                                    
								}
								index+=(xmax*ymax);
							}
							a=(size_t)b;
						}
						else a=0;
					}

					//backward scan
					a=0;
					for(k=zmax-2;k>=0;k--)
					{  
						if(a>0) a-=1;
						if(buff[k]>buff[k+1]+zp2)
						{  
							b=((buff[k]-buff[k+1]-zp2)/(2*zp2));
							if((k-b)<0) b=k;
							index=(k-a)*xmax*ymax+j*xmax+i;
							nkonec=true;
							for(n=a;((n<=b)&&(nkonec));n++)
							{  
								m=buff[k+1]+ mocnina[n+1];
								if (buff[k-n]<=m) 
								{  
									nkonec=false;
								}
								else 
								{  
									if(pObraz[index]>m)
										pObraz[index]=m;
								}
								index-=(xmax*ymax);
							}
							a=(size_t)b;
						}
						else a=0;
					}
				}
		}

		if(odmocnina)
		{  
			konec=(zmax-1)*xmax*ymax+(ymax-1)*xmax+xmax-1;
			for(index=0;index<=konec;index++)
				pObraz[index]=sqrt(pObraz[index]);
		}
	} // Saito4Sed

	//--------------------------------------------------------------------------
	// Saito8Sed
	//--------------------------------------------------------------------------
	/*This algorithm is combination of Danielsson's algoritm and Saito's algorithm. */
	template <typename T> void Saito8Sed(Image3d<T> &Obraz, T Image_Background, bool odmocnina)
	{  
		const int xmax=Obraz.GetWidth(), ymax=Obraz.GetHeight(), zmax=Obraz.GetNumSlices();// size of picture


		Vector3d<float> res=Obraz.GetResolution().GetRes();
		const T xp=1.0/res.x, // value of one x step
			    yp=1.0/res.y, // value of one y step
				zp=1.0/res.z; // value of one y step
		const T xp2=xp*xp, yp2=yp*yp, zp2=zp*zp;

		int i, j ,k;

		size_t n, a, index, index2, konec;

		T b, m;

		bool nkonec;

		std::valarray<voxelxy> pole(xmax*ymax);
		std::valarray<T> &pObraz = Obraz.GetVoxelData();
        
		std::valarray<T> mocninax(xmax+2), mocninay(ymax+2);
		for(i=0;i<=xmax+1;i++) mocninax[i]=xp2*i*i;
		for(i=0;i<=ymax+1;i++) mocninay[i]=yp2*i*i;

		std::valarray<T> buff(zmax), mocnina(zmax+1);

		for(k=0;k<zmax;k++)
		{  
			index=k*xmax*ymax;
			index2=0;
			for(j=0;j<ymax;j++)
			{  
				for(i=0;i<xmax;i++)
				{  
					if(pObraz[index]==Image_Background)
					{ 
						pole[index2].x=xmax;
						pole[index2].y=ymax;
					}
					else
					{
						pole[index2].x=0;
						pole[index2].y=0;
					}
					index+=1;
					index2+=1;
				}
			}

			// first scan from (-1,0), (0,-1), (1,0)
			scan8N(0,xmax-1,1,ymax-1,xmax,ymax,mocninax,mocninay,pole);
			// second scan from (-1,0), (0,1), (1,0)
			scan8N(0,xmax-1,ymax-2,0,xmax,ymax,mocninax,mocninay,pole);

			index=k*xmax*ymax;
			if(pole[0].x==xmax)
			{  
				for(j=0;j<ymax;j++)
					for(i=0;i<xmax;i++)
					{  
						pObraz[index]=max_saito;
						index+=1;
					}
			}
			else 
			{  
				if((zmax==1)&&(odmocnina))
				{  
					index2=0;
					for(j=0;j<ymax;j++)
						for(i=0;i<xmax;i++)
						{  
							pObraz[index]=sqrt(mocninax[pole[index2].x]+mocninay[pole[index2].y]);
							index+=1;
							index2+=1;
						}
						odmocnina=false;
				}
				else 
				{  
					index2=0;
					for(j=0;j<ymax;j++)
						for(i=0;i<xmax;i++)
						{  
							pObraz[index]=mocninax[pole[index2].x]+mocninay[pole[index2].y];
							index+=1;
							index2+=1;
						}
				}
			}
		}

		//step 3
		if(zmax>1)
		{  
			for(k=0;k<=zmax;k++) mocnina[k]=zp2*k*k;
			for(j=0;j<ymax;j++)
				for(i=0;i<xmax;i++)
				{  
					index=j*xmax+i;
					for(k=0;k<zmax;k++)
					{  
						buff[k]=pObraz[index];           
						index+=(xmax*ymax);
					}

					//forward scan
					a=0;
					for(k=1;k<zmax;k++)
					{  
						if (a>0) a-=1;
						if (buff[k]>buff[k-1]+zp2)
						{  
							b=((buff[k]-buff[k-1]-zp2)/(2*zp2));
							if ((k+b)>(zmax-1)) b=(zmax-1)-k;
							index=(k+a)*xmax*ymax+j*xmax+i;
							nkonec=true;
							for(n=a;((n<=b)&&(nkonec));n++)
							{  
								m=buff[k-1]+ mocnina[n+1];
								if(buff[k+n]<=m) 
								{  
									nkonec=false;
								}
								else 
								{  
									if(pObraz[index]>m)
										pObraz[index]=m;
								}
								index+=(xmax*ymax);
							}
							a=(size_t)b;
						}
						else a=0;
					}

					//backward scan
					a=0;
					for(k=zmax-2;k>=0;k--)
					{  
						if(a>0) a-=1;
						if(buff[k]>buff[k+1]+zp2)
						{  
							b=((buff[k]-buff[k+1]-zp2)/(2*zp2));
							if((k-b)<0) b=k;
							index=(k-a)*xmax*ymax+j*xmax+i;
							nkonec=true;
							for(n=a;((n<=b)&&(nkonec));n++)
							{ 
								m=buff[k+1]+ mocnina[n+1];
								if (buff[k-n]<=m) 
								{  
									nkonec=false;
								}
								else 
								{  
									if(pObraz[index]>m)
										pObraz[index]=m;
								}
								index-=(xmax*ymax);
							}
							a=(size_t)b;
						}
						else a=0;
					}
				}
		}

		if(odmocnina)
		{  
			konec=(zmax-1)*xmax*ymax+(ymax-1)*xmax+xmax-1;
			for(index=0;index<=konec;index++)
				pObraz[index]=sqrt(pObraz[index]);
		}
	} // Saito8Sed

	//--------------------------------------------------------------------------
	// scanDED4N
	//--------------------------------------------------------------------------
	/* Scan for D-Euclidean Distance that uses 4-neighbour. */
	template <typename T> void scanDED4N(
		int pocx,
		int konx, // begin and end of scan in x
		int pocy,
		int kony, // begin and end of scan in y
		size_t xmax, 
		size_t ymax, // size of array field
		std::valarray<T> &mocninax,
		std::valarray<T> &mocninay,
		std::valarray<voxelxy> &pole) // distance map
	{
		int posunx=(pocx<konx)?1:-1, posuny=(pocy<kony)?1:-1;

		T v1,v2;
		size_t index, index2;

		int i,j;

		for(j=pocy;j!=kony+posuny;j+=posuny)
		{  
			index=j*xmax+pocx;
			for(i=pocx;i!=konx+posunx;i+=posunx)
			{  
				v1=mocninax[pole[index].x]+mocninay[pole[index].y];
				index2=index-posunx;
				v2=mocninax[pole[index2].x+1]+mocninay[pole[index2].y];
				if(v1>v2)
				{  
					pole[index].x=pole[index2].x+1;
					pole[index].y=pole[index2].y;
					v1=v2;
				}
				index2=index-posuny*xmax;
				v2=mocninax[pole[index2].x]+mocninay[pole[index2].y+1];
				if(v1>v2)
				{  
					pole[index].x=pole[index2].x;
					pole[index].y=pole[index2].y+1;
				}
				index+=posunx;
			}

			index=j*xmax+konx;
			for(i=konx;i!=pocx-posunx;i-=posunx)
			{  
				v1=mocninax[pole[index].x]+mocninay[pole[index].y];
				index2=index+posunx;
				v2=mocninax[pole[index2].x+1]+mocninay[pole[index2].y];
				if(v1>v2)
				{  
					pole[index].x=pole[index2].x+1;
					pole[index].y=pole[index2].y;
				}
				index-=posunx;
			}
		}
	} // scanDED4N

	//--------------------------------------------------------------------------
	// scanDED8N
	//--------------------------------------------------------------------------
	/* Scan for D-Euclidean Distance that uses 8-neighbour. */
	template<typename T> void scanDED8N(
		int pocx,
		int konx, // begin and end of scan in x
		int pocy,
		int kony, // begin and end of scan in y
		size_t xmax,
		size_t ymax, // size of array field
		std::valarray<T> &mocninax, 
		std::valarray<T> &mocninay,
		std::valarray<voxelxy> &pole) // distance map
	{  
		int posunx=(pocx<konx)?1:-1, posuny=(pocy<kony)?1:-1;

		T v1,v2;
		size_t index, index2;

		int i,j;

		for(j=pocy;j!=kony+posuny;j+=posuny)
		{  
			index=j*xmax+pocx;
			for(i=pocx;i!=konx+posunx;i+=posunx)
			{  
				v1=mocninax[pole[index].x]+mocninay[pole[index].y];
				index2=index-posunx;
				v2=mocninax[pole[index2].x+1]+mocninay[pole[index2].y];
				if(v1>v2)
				{  
					pole[index].x=pole[index2].x+1;
					pole[index].y=pole[index2].y;
					v1=v2;
				}
				index2=index2-posuny*xmax;
				v2=mocninax[pole[index2].x+1]+mocninay[pole[index2].y+1];
				if(v1>v2)
				{  
					pole[index].x=pole[index2].x+1;
					pole[index].y=pole[index2].y+1;
					v1=v2;
				}
				index2=index2+posunx;
				v2=mocninax[pole[index2].x]+mocninay[pole[index2].y+1];
				if(v1>v2)
				{  
					pole[index].x=pole[index2].x;
					pole[index].y=pole[index2].y+1;
					v1=v2;
				}
				index2=index2+posunx;
				v2=mocninax[pole[index2].x+1]+mocninay[pole[index2].y+1];
				if(v1>v2)
				{  
					pole[index].x=pole[index2].x+1;
					pole[index].y=pole[index2].y+1;
				}
				index+=posunx;
			}

			index=j*xmax+konx;
			for(i=konx;i!=pocx-posunx;i-=posunx)
			{  
				v1=mocninax[pole[index].x]+mocninay[pole[index].y];
				index2=index+posunx;
				v2=mocninax[pole[index2].x+1]+mocninay[pole[index2].y];
				if(v1>v2)
				{  
					pole[index].x=pole[index2].x+1;
					pole[index].y=pole[index2].y;
				}
				index-=posunx;
			}
		}
	} // scanDED8N

	//--------------------------------------------------------------------------
	// DeuclideanDistance2D
	//--------------------------------------------------------------------------
	/* This function computes D-Euclidean Distance with using 4-neighbour or 8-neighbour for 2D images. */
	template <typename T> void DeuclideanDistance2D(
		Image3d<T> &Obraz, 
		byte kvalita,
		T Image_Background, 
		bool odmocnina)
	{  
		const int xmax=Obraz.GetWidth(), ymax=Obraz.GetHeight();

		Vector3d<float> res=Obraz.GetResolution().GetRes();
		const T xp=1.0/res.x, // value of one x step
			    yp=1.0/res.y; // value of one y step
		const T xp2=xp*xp, yp2=yp*yp;

		int i,j, konec;

		size_t index, index2;

		std::valarray<voxelxy> pole((xmax+2)*(ymax+2));
		std::valarray<T> &pObraz = Obraz.GetVoxelData();

		std::valarray<T> mocninax(xmax+2), mocninay(ymax+2);
		for(i=0;i<=xmax+1;i++) mocninax[i]=xp2*i*i;
		for(i=0;i<=ymax+1;i++) mocninay[i]=yp2*i*i;

		//  initialization of algorithm
		index=0;
		index2=(ymax+1)*(xmax+2);
		for(i=0;i<xmax+2;i++)
		{   
			pole[index].x=xmax;
			pole[index].y=ymax;
			pole[index2].x=xmax;
			pole[index2].y=ymax;
			index+=1;
			index2+=1;
		}

		index=0;
		index2=xmax+1;
		for(j=0;j<ymax+2;j++)
		{
			pole[index].x=xmax;
			pole[index].y=ymax;
			pole[index2].x=xmax;
			pole[index2].y=ymax;
			index+=(xmax+2);
			index2+=(xmax+2);
		}

		index=0;
		for(j=0;j<ymax;j++)
		{  
			index2=(j+1)*(xmax+2)+1;
			for(i=0;i<xmax;i++)
			{  
				if(pObraz[index]==Image_Background)
				{ 
					pole[index2].x=xmax;
					pole[index2].y=ymax;
				}
				else 
				{
					pole[index2].x=0;
					pole[index2].y=0;
				}
				index+=1;
				index2+=1;
			}
		}
		//end of initialization

		if(kvalita==0)
		{  
			scanDED4N(1,xmax,1,ymax,xmax+2,ymax+2,mocninax,mocninay,pole);
			scanDED4N(xmax,1,ymax,1,xmax+2,ymax+2,mocninax,mocninay,pole);
		}
		else 
		{
			scanDED8N(1,xmax,1,ymax,xmax+2,ymax+2,mocninax,mocninay,pole);
			scanDED8N(xmax,1,ymax,1,xmax+2,ymax+2,mocninax,mocninay,pole);
		}

		//save results
		if(pole[xmax+3].x==((unsigned int)xmax))
		{  
			konec=(ymax-1)*xmax+xmax-1;
			for(i=0;i<=konec;i++) pObraz[i]=max_DT;
		}
		else 
		{  
			if(odmocnina)
			{  
				index=0;
				for(j=0;j<ymax;j++)
				{ 
					index2=(j+1)*(xmax+2)+1;
					for(i=0;i<xmax;i++)
					{  
						pObraz[index]=sqrt(mocninax[pole[index2].x]+mocninay[pole[index2].y]);                 
						index+=1;
						index2+=1;
					}
				}
			}
			else
			{  
				index=0;
				for(j=0;j<ymax;j++)
				{  
					index2=(j+1)*(xmax+2)+1;
					for(i=0;i<xmax;i++)
					{  
						pObraz[index]=mocninax[pole[index2].x]+mocninay[pole[index2].y];
						index+=1;
						index2+=1;
					}
				}
			}
		}
	} // DeuclideanDistance2D 

	//--------------------------------------------------------------------------
	// scanDED26N
	//--------------------------------------------------------------------------
	/* Scan for D-Euclidean Distance that uses 26-neighbour. */
	template <typename T> void scanDED26N(
		int pocx,
		int konx, // begin and end of scan in x
		int pocy,
		int kony, // begin and end of scan in y
		int pocz,
		int konz, // begin and end of scan in z
		size_t xmax,
		size_t ymax,
		size_t zmax, // size of array field
		std::valarray<T> &mocninax, 
		std::valarray<T> &mocninay, 
		std::valarray<T> &mocninaz,
		std::valarray<voxelxyz> &pole) // distance map
	{  
		int posunx=(pocx<konx)?1:-1, posuny=(pocy<kony)?1:-1,posunz=(pocz<konz)?1:-1;

		T v1,v2;
		size_t index, index2=0; 

		int i,j,k;

		for(k=pocz;k!=konz+posunz;k+=posunz)
		{  
			for(j=pocy;j!=kony+posuny;j+=posuny)
			{  
				index=k*xmax*ymax+j*xmax+pocx;
				for(i=pocx;i!=konx+posunx;i+=posunx)
				{  
					v1=mocninax[pole[index].x]+mocninay[pole[index].y]+mocninaz[pole[index].z];
					index2=index-posunx;
					v2=mocninax[pole[index2].x+1]+mocninay[pole[index2].y]+mocninaz[pole[index2].z];
					if(v1>v2)
					{  
						pole[index].x=pole[index2].x+1;
						pole[index].y=pole[index2].y;
						pole[index].z=pole[index2].z;
						v1=v2;
					}
					index2=index2-posuny*xmax;
					v2=mocninax[pole[index2].x+1]+mocninay[pole[index2].y+1]+mocninaz[pole[index2].z];
					if(v1>v2)
					{
						pole[index].x=pole[index2].x+1;
						pole[index].y=pole[index2].y+1;
						pole[index].z=pole[index2].z;
						v1=v2;
					}
					index2=index2+posunx;
					v2=mocninax[pole[index2].x]+mocninay[pole[index2].y+1]+mocninaz[pole[index2].z];
					if(v1>v2)
					{  
						pole[index].x=pole[index2].x;
						pole[index].y=pole[index2].y+1;
						pole[index].z=pole[index2].z;
						v1=v2;
					}
					index2=index2+posunx;
					v2=mocninax[pole[index2].x+1]+mocninay[pole[index2].y+1]+mocninaz[pole[index2].z];
					if(v1>v2)
					{ 
						pole[index].x=pole[index2].x+1;
						pole[index].y=pole[index2].y+1;
						pole[index].z=pole[index2].z;
						v1=v2;
					}
					index2=index-posunz*xmax*ymax;
					v2=mocninax[pole[index2].x]+mocninay[pole[index2].y]+mocninaz[pole[index2].z+1];
					if(v1>v2)
					{  
						pole[index].x=pole[index2].x;
						pole[index].y=pole[index2].y;
						pole[index].z=pole[index2].z+1;
						v1=v2;
                    }
					index2=index2-posunx;
					v2=mocninax[pole[index2].x+1]+mocninay[pole[index2].y]+mocninaz[pole[index2].z+1];
					if(v1>v2)
					{  
						pole[index].x=pole[index2].x+1;
						pole[index].y=pole[index2].y;
						pole[index].z=pole[index2].z+1;
						v1=v2;
					}
					index2=index2-posuny*xmax;
					v2=mocninax[pole[index2].x+1]+mocninay[pole[index2].y+1]+mocninaz[pole[index2].z+1];
					if(v1>v2)
					{
						pole[index].x=pole[index2].x+1;
						pole[index].y=pole[index2].y+1;
						pole[index].z=pole[index2].z+1;
						v1=v2;
					}
					index2=index2+posunx;
					v2=mocninax[pole[index2].x]+mocninay[pole[index2].y+1]+mocninaz[pole[index2].z+1];
					if(v1>v2)
					{
						pole[index].x=pole[index2].x;
						pole[index].y=pole[index2].y+1;
						pole[index].z=pole[index2].z+1;
						v1=v2;
					}
					index2=index2+posunx;
					v2=mocninax[pole[index2].x+1]+mocninay[pole[index2].y+1]+mocninaz[pole[index2].z+1];
					if(v1>v2)
					{  
						pole[index].x=pole[index2].x+1;
						pole[index].y=pole[index2].y+1;
						pole[index].z=pole[index2].z+1;
						v1=v2;
					}
					index2=index2+posuny*xmax;
					v2=mocninax[pole[index2].x+1]+mocninay[pole[index2].y]+mocninaz[pole[index2].z+1];
					if(v1>v2)
					{  
						pole[index].x=pole[index2].x+1;
						pole[index].y=pole[index2].y;
						pole[index].z=pole[index2].z+1;
						v1=v2;
					}
					index2=index2+posuny*xmax;
					v2=mocninax[pole[index2].x+1]+mocninay[pole[index2].y+1]+mocninaz[pole[index2].z+1];
					if(v1>v2)
					{  
						pole[index].x=pole[index2].x+1;
						pole[index].y=pole[index2].y+1;
						pole[index].z=pole[index2].z+1;
						v1=v2;
					}
					index2=index2-posunx;
					v2=mocninax[pole[index2].x]+mocninay[pole[index2].y+1]+mocninaz[pole[index2].z+1];
					if(v1>v2)
					{  
						pole[index].x=pole[index2].x;
						pole[index].y=pole[index2].y+1;
						pole[index].z=pole[index2].z+1;
						v1=v2;
					}
					index2=index2-posunx;
					v2=mocninax[pole[index2].x+1]+mocninay[pole[index2].y+1]+mocninaz[pole[index2].z+1];
					if(v1>v2)
					{ 
						pole[index].x=pole[index2].x+1;
						pole[index].y=pole[index2].y+1;
						pole[index].z=pole[index2].z+1;
					}
					index+=posunx;
			   }
			   index=k*xmax*ymax+j*xmax+konx;
			   for(i=konx;i!=pocx-posunx;i-=posunx)
			   {  
				   v1=mocninax[pole[index].x]+mocninay[pole[index].y]+mocninaz[pole[index].z];
				   index2=index+posunx;
				   v2=mocninax[pole[index2].x+1]+mocninay[pole[index2].y]+mocninaz[pole[index2].z];
				   if(v1>v2)
				   {
					   pole[index].x=pole[index2].x+1;
					   pole[index].y=pole[index2].y;
					   pole[index].z=pole[index2].z;
				   }
				   index-=posunx;
			   }
			}
			for(j=kony;j!=pocy-posuny;j-=posuny)
			{  
				index=k*xmax*ymax+j*xmax+konx;
				for(i=konx;i!=pocx-posunx;i-=posunx)
				{  
					v1=mocninax[pole[index].x]+mocninay[pole[index].y]+mocninaz[pole[index].z];
					index2=index+posunx;
					v2=mocninax[pole[index2].x+1]+mocninay[pole[index2].y]+mocninaz[pole[index2].z];
					if(v1>v2)
					{
						pole[index].x=pole[index2].x+1;
						pole[index].y=pole[index2].y;
						pole[index].z=pole[index2].z;
						v1=v2;
					}
					index2=index2+posuny*xmax;
					v2=mocninax[pole[index2].x+1]+mocninay[pole[index2].y+1]+mocninaz[pole[index2].z];
					if(v1>v2)
					{  
						pole[index].x=pole[index2].x+1;
						pole[index].y=pole[index2].y+1;
						pole[index].z=pole[index2].z;
						v1=v2;
					}
					index2=index2-posunx;
					v2=mocninax[pole[index2].x]+mocninay[pole[index2].y+1]+mocninaz[pole[index2].z];
					if(v1>v2)
					{  
						pole[index].x=pole[index2].x;
						pole[index].y=pole[index2].y+1;
						pole[index].z=pole[index2].z;
						v1=v2;
					}
					index2=index2-posunx;
					v2=mocninax[pole[index2].x+1]+mocninay[pole[index2].y+1]+mocninaz[pole[index2].z];
					if(v1>v2)
					{  
						pole[index].x=pole[index2].x+1;
						pole[index].y=pole[index2].y+1;
						pole[index].z=pole[index2].z;
					}
					index-=posunx;
				}
				index=k*xmax*ymax+j*xmax+pocx;
				for(i=pocx;i!=konx+posunx;i+=posunx)
				{  
					v1=mocninax[pole[index].x]+mocninay[pole[index].y]+mocninaz[pole[index].z];
					index2=index-posunx;
					v2=mocninax[pole[index2].x+1]+mocninay[pole[index2].y]+mocninaz[pole[index2].z];
					if(v1>v2)
					{  
						pole[index].x=pole[index2].x+1;
						pole[index].y=pole[index2].y;
						pole[index].z=pole[index2].z;
					}
					index+=posunx;
				}
			}
		}
	} // scanDED26N

	//--------------------------------------------------------------------------
	// scanDED18N
	//--------------------------------------------------------------------------
	/* Scan for D-Euclidean Distance that uses 18-neighbour. */
	template <typename T> void scanDED18N(
		int pocx,
		int konx, // begin and end of scan in x
		int pocy,
		int kony, // begin and end of scan in y
		int pocz,
		int konz, // begin and end of scan in z
		size_t xmax,
		size_t ymax,
		size_t zmax, // size of array field
		std::valarray<T> &mocninax,
		std::valarray<T> &mocninay, 
		std::valarray<T> &mocninaz,
		std::valarray<voxelxyz> &pole) // distance map
	{  
		int posunx=(pocx<konx)?1:-1, posuny=(pocy<kony)?1:-1,posunz=(pocz<konz)?1:-1;

		T v1,v2;
		size_t index, index2=0; 

		int i,j,k;

		for(k=pocz;k!=konz+posunz;k+=posunz)
		{
			for(j=pocy;j!=kony+posuny;j+=posuny)
			{
				index=k*xmax*ymax+j*xmax+pocx;
				for(i=pocx;i!=konx+posunx;i+=posunx)
				{  
					v1=mocninax[pole[index].x]+mocninay[pole[index].y]+mocninaz[pole[index].z];
					index2=index-posunx;
					v2=mocninax[pole[index2].x+1]+mocninay[pole[index2].y]+mocninaz[pole[index2].z];
					if(v1>v2)
					{  
						pole[index].x=pole[index2].x+1;
						pole[index].y=pole[index2].y;
						pole[index].z=pole[index2].z;
						v1=v2;
					}
					index2=index2-posuny*xmax;
					v2=mocninax[pole[index2].x+1]+mocninay[pole[index2].y+1]+mocninaz[pole[index2].z];
					if(v1>v2)
					{  
						pole[index].x=pole[index2].x+1;
						pole[index].y=pole[index2].y+1;
						pole[index].z=pole[index2].z;
						v1=v2;
					}
					index2=index2+posunx;
					v2=mocninax[pole[index2].x]+mocninay[pole[index2].y+1]+mocninaz[pole[index2].z];
					if(v1>v2)
					{
						pole[index].x=pole[index2].x;
						pole[index].y=pole[index2].y+1;
						pole[index].z=pole[index2].z;
						v1=v2;
					}
					index2=index2+posunx;
					v2=mocninax[pole[index2].x+1]+mocninay[pole[index2].y+1]+mocninaz[pole[index2].z];
					if(v1>v2)
					{  
						pole[index].x=pole[index2].x+1;
						pole[index].y=pole[index2].y+1;
						pole[index].z=pole[index2].z;
						v1=v2;
					}
					index2=index-posunz*xmax*ymax;
					v2=mocninax[pole[index2].x]+mocninay[pole[index2].y]+mocninaz[pole[index2].z+1];
					if(v1>v2)
					{  
						pole[index].x=pole[index2].x;
						pole[index].y=pole[index2].y;
						pole[index].z=pole[index2].z+1;
						v1=v2;
					}
					index2=index2-posunx;
					v2=mocninax[pole[index2].x+1]+mocninay[pole[index2].y]+mocninaz[pole[index2].z+1];
					if(v1>v2)
					{  
						pole[index].x=pole[index2].x+1;
						pole[index].y=pole[index2].y;
						pole[index].z=pole[index2].z+1;
						v1=v2;
					}
					index2=index2-posuny*xmax+posunx;
					v2=mocninax[pole[index2].x]+mocninay[pole[index2].y+1]+mocninaz[pole[index2].z+1];
					if(v1>v2)
					{  
						pole[index].x=pole[index2].x;
						pole[index].y=pole[index2].y+1;
						pole[index].z=pole[index2].z+1;
						v1=v2;
					}
					index2=index2+posuny*xmax+posunx;
					v2=mocninax[pole[index2].x+1]+mocninay[pole[index2].y]+mocninaz[pole[index2].z+1];
					if(v1>v2)
					{
						pole[index].x=pole[index2].x+1;
						pole[index].y=pole[index2].y;
						pole[index].z=pole[index2].z+1;
						v1=v2;
					}
					index2=index2+posuny*xmax-posunx;
					v2=mocninax[pole[index2].x]+mocninay[pole[index2].y+1]+mocninaz[pole[index2].z+1];
					if(v1>v2)
					{  
						pole[index].x=pole[index2].x;
						pole[index].y=pole[index2].y+1;
						pole[index].z=pole[index2].z+1;
						v1=v2;
					}
					index+=posunx;
				}
				index=k*xmax*ymax+j*xmax+konx;
				for(i=konx;i!=pocx-posunx;i-=posunx)
				{  
					v1=mocninax[pole[index].x]+mocninay[pole[index].y]+mocninaz[pole[index].z];
					index2=index+posunx;
					v2=mocninax[pole[index2].x+1]+mocninay[pole[index2].y]+mocninaz[pole[index2].z];
					if(v1>v2)
					{  
						pole[index].x=pole[index2].x+1;
						pole[index].y=pole[index2].y;
						pole[index].z=pole[index2].z;
					}
					index-=posunx;
				}
			}
			for(j=kony;j!=pocy-posuny;j-=posuny)
			{  
				index=k*xmax*ymax+j*xmax+konx;
				for(i=konx;i!=pocx-posunx;i-=posunx)
				{  
					v1=mocninax[pole[index].x]+mocninay[pole[index].y]+mocninaz[pole[index].z];
					index2=index+posunx;
					v2=mocninax[pole[index2].x+1]+mocninay[pole[index2].y]+mocninaz[pole[index2].z];
					if(v1>v2)
					{  
						pole[index].x=pole[index2].x+1;
						pole[index].y=pole[index2].y;
						pole[index].z=pole[index2].z;
						v1=v2;
					}
					index2=index2+posuny*xmax;
					v2=mocninax[pole[index2].x+1]+mocninay[pole[index2].y+1]+mocninaz[pole[index2].z];
					if(v1>v2)
					{  
						pole[index].x=pole[index2].x+1;
						pole[index].y=pole[index2].y+1;
						pole[index].z=pole[index2].z;
						v1=v2;
					}
					index2=index2-posunx;
					v2=mocninax[pole[index2].x]+mocninay[pole[index2].y+1]+mocninaz[pole[index2].z];
					if(v1>v2)
					{  
						pole[index].x=pole[index2].x;
						pole[index].y=pole[index2].y+1;
						pole[index].z=pole[index2].z;
						v1=v2;
					}
					index2=index2-posunx;
					v2=mocninax[pole[index2].x+1]+mocninay[pole[index2].y+1]+mocninaz[pole[index2].z];
					if(v1>v2)
					{  
						pole[index].x=pole[index2].x+1;
						pole[index].y=pole[index2].y+1;
						pole[index].z=pole[index2].z;
					}
					index-=posunx;
				}
				index=k*xmax*ymax+j*xmax+pocx;
				for(i=pocx;i!=konx+posunx;i+=posunx)
				{  
					v1=mocninax[pole[index].x]+mocninay[pole[index].y]+mocninaz[pole[index].z];
					index2=index-posunx;
					v2=mocninax[pole[index2].x+1]+mocninay[pole[index2].y]+mocninaz[pole[index2].z];
					if(v1>v2)
					{
						pole[index].x=pole[index2].x+1;
						pole[index].y=pole[index2].y;
						pole[index].z=pole[index2].z;
					}
					index+=posunx;
				}
			}
		}
	} // scanDED18N

	//--------------------------------------------------------------------------
	// scanDED6N
	//--------------------------------------------------------------------------
	/* scan for D-Euclidean Distance that uses 6-neighbour*/
	template <typename T> void scanDED6N(
		int pocx,
		int konx, // begin and end of scan in x
		int pocy,
		int kony, // begin and end of scan in y
		int pocz, 
		int konz, // begin and end of scan in z
		size_t xmax,
		size_t ymax,
		size_t zmax, // size of array field
		std::valarray<T> &mocninax,
		std::valarray<T> &mocninay, 
		std::valarray<T> &mocninaz,
		std::valarray<voxelxyz> &pole) // distance map
	{  
		int posunx=(pocx<konx)?1:-1, posuny=(pocy<kony)?1:-1,posunz=(pocz<konz)?1:-1;

		T v1,v2;
		size_t index, index2=0; 

		int i,j,k;

		for(k=pocz;k!=konz+posunz;k+=posunz)
		{
			for(j=pocy;j!=kony+posuny;j+=posuny)
			{
				index=k*xmax*ymax+j*xmax+pocx;
				for(i=pocx;i!=konx+posunx;i+=posunx)
				{ 
					v1=mocninax[pole[index].x]+mocninay[pole[index].y]+mocninaz[pole[index].z];
					index2=index-posunx;
					v2=mocninax[pole[index2].x+1]+mocninay[pole[index2].y]+mocninaz[pole[index2].z];
					if(v1>v2)
					{
						pole[index].x=pole[index2].x+1;
						pole[index].y=pole[index2].y;
						pole[index].z=pole[index2].z;
						v1=v2;
					}
					index2=index-posuny*xmax;
					v2=mocninax[pole[index2].x]+mocninay[pole[index2].y+1]+mocninaz[pole[index2].z];
					if(v1>v2)
					{  
						pole[index].x=pole[index2].x;
						pole[index].y=pole[index2].y+1;
						pole[index].z=pole[index2].z;
						v1=v2;
					}
					index2=index-posunz*xmax*ymax;
					v2=mocninax[pole[index2].x]+mocninay[pole[index2].y]+mocninaz[pole[index2].z+1];
					if(v1>v2)
					{
						pole[index].x=pole[index2].x;
						pole[index].y=pole[index2].y;
						pole[index].z=pole[index2].z+1;
					}
					index+=posunx;
				}
				index=k*xmax*ymax+j*xmax+konx;
				for(i=konx;i!=pocx-posunx;i-=posunx)
				{  
					v1=mocninax[pole[index].x]+mocninay[pole[index].y]+mocninaz[pole[index].z];
					index2=index+posunx;
					v2=mocninax[pole[index2].x+1]+mocninay[pole[index2].y]+mocninaz[pole[index2].z];
					if(v1>v2)
					{  
						pole[index].x=pole[index2].x+1;
						pole[index].y=pole[index2].y;
						pole[index].z=pole[index2].z;
					}
					index-=posunx;
				}
			}
			for(j=kony;j!=pocy-posuny;j-=posuny)
			{ 
				index=k*xmax*ymax+j*xmax+konx;
				for(i=konx;i!=pocx-posunx;i-=posunx)
				{ 
					v1=mocninax[pole[index].x]+mocninay[pole[index].y]+mocninaz[pole[index].z];
					index2=index+posunx;
					v2=mocninax[pole[index2].x+1]+mocninay[pole[index2].y]+mocninaz[pole[index2].z];
					if(v1>v2)
					{  
						pole[index].x=pole[index2].x+1;
						pole[index].y=pole[index2].y;
						pole[index].z=pole[index2].z;
						v1=v2;
					}
					index2=index+posuny*xmax;
					v2=mocninax[pole[index2].x]+mocninay[pole[index2].y+1]+mocninaz[pole[index2].z];
					if(v1>v2)
					{
						pole[index].x=pole[index2].x;
						pole[index].y=pole[index2].y+1;
						pole[index].z=pole[index2].z;
						v1=v2;
					}
					index-=posunx;
				}
				index=k*xmax*ymax+j*xmax+pocx;
				for(i=pocx;i!=konx+posunx;i+=posunx)
				{ 
					v1=mocninax[pole[index].x]+mocninay[pole[index].y]+mocninaz[pole[index].z];
					index2=index-posunx;
					v2=mocninax[pole[index2].x+1]+mocninay[pole[index2].y]+mocninaz[pole[index2].z];
					if(v1>v2)
					{
						pole[index].x=pole[index2].x+1;
						pole[index].y=pole[index2].y;
						pole[index].z=pole[index2].z;
					}
					index+=posunx;
				}
			}
		}
	} // scanDED6N

	//--------------------------------------------------------------------------
	// DeuclideanDistance3D
	//--------------------------------------------------------------------------
	/* This function computes D-Euclidean Distance with using 6-neighbour, 
	18-neighbour or 26-neighbour for 3D images. */
	template <typename T> void DeuclideanDistance3D(
		Image3d<T> &Obraz, 
		byte kvalita, 
		T Image_Background, 
		bool odmocnina)
	{  
		const int xmax=Obraz.GetWidth(), ymax=Obraz.GetHeight(), zmax=Obraz.GetNumSlices();// size of picture
		
		Vector3d<float> res=Obraz.GetResolution().GetRes();
		const T xp=1.0/res.x, // value of one x step
			    yp=1.0/res.y, // value of one y step
				zp=1.0/res.z; // value of one y step
		const T xp2=xp*xp, yp2=yp*yp, zp2=zp*zp;

		int i,j,k, konec;

		size_t index, index2;

		std::valarray<voxelxyz> pole((xmax+2)*(ymax+2)*(zmax+2));
		std::valarray<T> &pObraz = Obraz.GetVoxelData();

		std::valarray<T> mocninax(xmax+2), mocninay(ymax+2), mocninaz(zmax+2);
		for(i=0;i<=xmax+1;i++) mocninax[i]=xp2*i*i;
		for(i=0;i<=ymax+1;i++) mocninay[i]=yp2*i*i;
		for(i=0;i<=zmax+1;i++) mocninaz[i]=zp2*i*i;

		//  initialization of algorithm
		for(j=0;j<ymax+2;j++)
		{  
			index=j*(xmax+2);
			index2=(zmax+1)*(xmax+2)*(ymax+2)+j*(xmax+2);
			for(i=0;i<xmax+2;i++)
			{
				pole[index].x=xmax;
				pole[index].y=ymax;
				pole[index].z=zmax;
				pole[index2].x=xmax;
				pole[index2].y=ymax;
				pole[index2].z=zmax;
				index+=1;
				index2+=1;
			}
		}

		for(k=0;k<zmax+2;k++)
		{  
			index=k*(xmax+2)*(ymax+2);
			index2=k*(xmax+2)*(ymax+2)+(ymax+1)*(xmax+2);
			for(i=0;i<xmax+2;i++)
			{ 
				pole[index].x=xmax;
				pole[index].y=ymax;
				pole[index].z=zmax;
				pole[index2].x=xmax;
				pole[index2].y=ymax;
				pole[index2].z=zmax;
				index+=1;
				index2+=1;
			}
		}

		for(k=0;k<zmax+1;k++)
		{  
			index=k*(xmax+2)*(ymax+2);
			index2=k*(xmax+2)*(ymax+2)+xmax+1;
			for(j=0;j<ymax+2;j++)
			{
				pole[index].x=xmax;
				pole[index].y=ymax;
				pole[index].z=zmax;
				pole[index2].x=xmax;
				pole[index2].y=ymax;
				pole[index2].z=zmax;
				index+=(xmax+2);
				index2+=(xmax+2);
			}
		}

		index=0;
		for(k=0;k<zmax;k++)
			for(j=0;j<ymax;j++)
			{  
				index2=(k+1)*(xmax+2)*(ymax+2)+(j+1)*(xmax+2)+1;
				for(i=0;i<xmax;i++)
				{  
					if(pObraz[index]==Image_Background)
					{
						pole[index2].x=xmax;
						pole[index2].y=ymax;
						pole[index2].z=zmax;
					}
					else
					{
						pole[index2].x=0;
						pole[index2].y=0;
						pole[index2].z=0;
					}
					index+=1;
					index2+=1;
				}
			}
			
		// end of initialization
		if(kvalita>1)
		{  
			scanDED26N(1,xmax,1,ymax,1,zmax,xmax+2,ymax+2,zmax+2,mocninax,mocninay,mocninaz,pole);
			scanDED26N(xmax,1,ymax,1,zmax,1,xmax+2,ymax+2,zmax+2,mocninax,mocninay,mocninaz,pole);
		}
		if(kvalita==1)
		{  
			scanDED18N(1,xmax,1,ymax,1,zmax,xmax+2,ymax+2,zmax+2,mocninax,mocninay,mocninaz,pole);
            scanDED18N(xmax,1,ymax,1,zmax,1,xmax+2,ymax+2,zmax+2,mocninax,mocninay,mocninaz,pole);
		}
		if(kvalita==0)
		{
			scanDED6N(1,xmax,1,ymax,1,zmax,xmax+2,ymax+2,zmax+2,mocninax,mocninay,mocninaz,pole);
			scanDED6N(xmax,1,ymax,1,zmax,1,xmax+2,ymax+2,zmax+2,mocninax,mocninay,mocninaz,pole);
		}          	

		//save results
		if(pole[(xmax+2)*(ymax+2)+(xmax+2)+1].x==((unsigned int)xmax))
		{  
			konec=(zmax-1)*xmax*ymax+(ymax-1)*xmax+xmax-1;
			for(i=0;i<=konec;i++) pObraz[i]=Infinity;
		}
		else 
		{  
			if(odmocnina)
			{ 
				index=0;
				for(k=1;k<zmax+1;k++)
					for(j=1;j<ymax+1;j++)
					{  
						index2=k*(xmax+2)*(ymax+2)+j*(xmax+2)+1;
						for(i=1;i<xmax+1;i++)
						{  
							pObraz[index]=sqrt(mocninax[pole[index2].x]+mocninay[pole[index2].y]+
								               mocninaz[pole[index2].z]);
							index+=1;
							index2+=1;
						}
					}
			}		      
			else 
			{  
				index=0;
				for(k=1;k<zmax+1;k++)
					for(j=1;j<ymax+1;j++)
					{  
						index2=k*(xmax+2)*(ymax+2)+j*(xmax+2)+1;
						for(i=1;i<xmax+1;i++)
						{  
							pObraz[index]=mocninax[pole[index2].x]+mocninay[pole[index2].y]+
								          mocninaz[pole[index2].z];
							index+=1;
							index2+=1;
						}
					}
			}
		}
	} // DeuclideanDistance3D

	/***************************************************************************\
	*
	*                     City-block distance transforms          
	*
	\***************************************************************************/
	//--------------------------------------------------------------------------
	// CityBlock
	//--------------------------------------------------------------------------
	template <typename T> I3D_DLLEXPORT void CityBlock(Image3d<T>& Obraz, T Image_Background)
	{  
		const int xmax=Obraz.GetWidth(), ymax=Obraz.GetHeight(), zmax=Obraz.GetNumSlices();

		Vector3d<float> res=Obraz.GetResolution().GetRes();
		const T xp=1.0/res.x, // value of one x step
                yp=1.0/res.y, // value of one y step
				zp=1.0/res.z; // value of one z step

		std::valarray<T> &pObraz = Obraz.GetVoxelData();
        
		size_t index, konec;

		//inicialization of algoritm
		konec=(zmax-1)*xmax*ymax+(ymax-1)*xmax+xmax-1;
		for(index=0;index<=konec;index++)
		{
			if(pObraz[index]==Image_Background)
			{  
				pObraz[index]=Infinity;
			}
			else 
			{
				pObraz[index]=0;
			}
		}

		//scan for direction (-1,-1,-1)
		scanCityBlock(1,xmax-2,1,ymax-2,1,zmax-1,xmax,ymax,zmax,xp,yp,zp,pObraz);
		//scan for direction (1,1,1)
		scanCityBlock(xmax-2,1,ymax-2,1,zmax-2,0,xmax,ymax,zmax,xp,yp,zp,pObraz);

	} // CityBlock

	//--------------------------------------------------------------------------
	// Explicit instantiations
	//--------------------------------------------------------------------------
	template I3D_DLLEXPORT void CityBlock(Image3d<float> &Obraz, float Image_background);

	/***************************************************************************\
	*
	*                     Chamfer distance transforms          
	*
	\***************************************************************************/
	//--------------------------------------------------------------------------
	// Chamfer
	//--------------------------------------------------------------------------
	template <typename T> I3D_DLLEXPORT void Chamfer(Image3d<T> &Obraz, T Image_Background)
	{  
		const int xmax=Obraz.GetWidth(), ymax=Obraz.GetHeight(), zmax=Obraz.GetNumSlices();

		Vector3d<float> res=Obraz.GetResolution().GetRes();
		const T xp=1.0/res.x, // value of one x step
				yp=1.0/res.y, // value of one y step
				zp=1.0/res.z; // value of one z step

		std::valarray<T> &pObraz = Obraz.GetVoxelData();

		size_t index, konec;

		//inicialization of algoritm
		konec=(zmax-1)*xmax*ymax+(ymax-1)*xmax+xmax-1;
		for(index=0;index<=konec;index++)
		{
			if(pObraz[index]==Image_Background)
			{
				pObraz[index]=Infinity;
			}
			else 
			{
				pObraz[index]=0;
			}

			//scan for direction (-1,-1,-1)
			scanChamfer(1,xmax-2,1,ymax-2,1,zmax-1,xmax,ymax,zmax,xp,yp,zp,pObraz);
			//scan for direction (1,1,1)
			scanChamfer(xmax-2,1,ymax-2,1,zmax-2,0,xmax,ymax,zmax,xp,yp,zp,pObraz);
		}
	} // Chamfer

	//--------------------------------------------------------------------------
	// Explicit instantiations
	//--------------------------------------------------------------------------
	template I3D_DLLEXPORT void Chamfer(Image3d<float> &Obraz, float Image_background);

	/***************************************************************************\
	*
	*                     Euclidean distance transforms          
	*
	\***************************************************************************/
	//--------------------------------------------------------------------------
	// EDM
	//--------------------------------------------------------------------------
	template <typename T> I3D_DLLEXPORT void EDM(Image3d<T> &Obraz, byte kvalita, T Image_Background, bool odmocnina)
	{
		const size_t zmax = Obraz.GetNumSlices();
		if (zmax == 1)
		{
			EDM2D(Obraz, kvalita, Image_Background, odmocnina);
		}
		if (zmax > 1)
		{
			EDM3D(Obraz, kvalita, Image_Background, odmocnina);
		}
	} // EDM

	//--------------------------------------------------------------------------
	// Explicit instantiations
	//--------------------------------------------------------------------------
	template I3D_DLLEXPORT void EDM(Image3d<float> &Obraz, byte kvalita, float Image_background, bool odmocnina);

	//--------------------------------------------------------------------------
	// HybridSaito
	//--------------------------------------------------------------------------	
	template<typename T> I3D_DLLEXPORT void HybridSaito(Image3d<T> &Obraz, byte kvalita, T Image_Background, bool odmocnina)
	{ 
		const size_t zmax = Obraz.GetNumSlices();
		if((zmax > 0) && (kvalita == 0))
		{
			Saito4Sed(Obraz, Image_Background, odmocnina);
		}
		if((zmax > 0) && (kvalita != 0))
		{
			Saito8Sed(Obraz, Image_Background, odmocnina);
		}
	} // HybridSaito

	//--------------------------------------------------------------------------
	// Explicit instantiations
	//--------------------------------------------------------------------------
	template I3D_DLLEXPORT void HybridSaito(Image3d<float> &Obraz, byte kvalita, float Image_background, bool odmocnina);

	//--------------------------------------------------------------------------
	// SlowSaito
	//--------------------------------------------------------------------------
	template<typename T> I3D_DLLEXPORT void SlowSaito(Image3d<T> &Obraz, T Image_Background, bool odmocnina)
	{  
		const int xmax = Obraz.GetWidth(), ymax = Obraz.GetHeight(), zmax = Obraz.GetNumSlices(); // size of picture
		
		Vector3d<float> res = Obraz.GetResolution().GetRes();
		const T xp = 1.0 / res.x, //value of one x step
			    yp = 1.0 / res.y, //value of one y step
				zp = 1.0 / res.z; //value of one z step
		const T xp2 = xp * xp, yp2 = yp * yp, zp2 = zp * zp;

		int max = (xmax < ymax) ? ymax : xmax;
		max =(zmax < max) ? max : zmax;
		std::valarray<T> buff(max), mocnina(max);

		std::valarray<T> &pObraz = Obraz.GetVoxelData();

		size_t index, konec;
		int i, j, k, n, vzdalenost;
		bool nasel;
        
		T w, d, rmax, rstart, rend, voxel, skok;
		
		//step 1
		for(i=0;i<xmax;i++) mocnina[i]=xp2*i*i;
		index=0;
		for(k=0;k<zmax;k++)
			for(j=0;j<ymax;j++)
			{  //forward scan
				nasel=false;
				i=0;
				index=k*xmax*ymax+j*xmax;
				while((!nasel)&&(i<xmax))
				{  
					if(pObraz[index]==Image_Background)
					{  
						pObraz[index]=max_saito;
						buff[i]=max_saito;
					}
					else 
					{  
						pObraz[index]=0;
						buff[i]=0;
						nasel=true;
					}
					i+=1;
					index+=1;
				}
				vzdalenost=0;
				for(n=i;n<xmax;n++)
				{  
					if(pObraz[index]==Image_Background)
					{  
						vzdalenost+=1;
						pObraz[index]=mocnina[vzdalenost];
						buff[n]=vzdalenost;
					}
					else 
					{  
						vzdalenost=0;
						pObraz[index]=0;
						buff[n]=0;
					}
					index+=1;
				}

				//backward scan
				index=k*xmax*ymax+j*xmax+xmax-1;
				i=xmax-1;
				voxel=buff[i];
				if(voxel!=max_saito)
				{  
					if(voxel!=0)
					{  
						i-=(((int)voxel)+1);
						index-=(((int)voxel)+1);
					}
					else 
					{  
						i-=1;
						index-=1;
					}
					while(i>=0)
					{  
						voxel=buff[i];
						if(voxel==max_saito) skok=i+1;
						else skok=(voxel)/2;
						vzdalenost=1;
						for(n=0;n<skok;n++)
						{  
							pObraz[index]=mocnina[vzdalenost];
							vzdalenost+=1;
							index-=1;
							i-=1;
						}
						i-=(((int)skok)+1);
						index-=(((int)skok)+1);
					}
				}
			}

		//Step 2
		for(j=0;j<ymax;j++) mocnina[j]=yp2*j*j;
		for(k=0;k<zmax;k++)
			for(i=0;i<xmax;i++)
			{  
				index=k*xmax*ymax+i;
				for(j=0;j<ymax;j++)
				{  
					buff[j]=pObraz[index];
					index+=xmax;
				}

				index=k*xmax*ymax+i;
				for(j=0;j<ymax;j++)
				{  
					d=buff[j];
					if(d!=0)
					{  
						rmax=(sqrt(d)/yp)+1;
						rstart=(rmax>(j))?(j):rmax;
						rend=(rmax>(ymax-1-j))?(ymax-1-j):rmax;
						for(n=((int)(-rstart));n<=((int)rend);n++)
						{  
							w=buff[j+n]+ mocnina[((n<0)?(-1*n):(n))];
							if(w<d)d=w;
						}
						pObraz[index]=d;
					}
					index+=xmax;
				}
			}

		//Step 3
		if(zmax>1)
		{  
			for(k=0;k<zmax;k++) mocnina[k]=zp2*k*k;
			for(j=0;j<ymax;j++)
				for(i=0;i<xmax;i++)
				{  
					index=j*xmax+i;
					for(k=0;k<zmax;k++)
					{  
						buff[k]=pObraz[index];
						index+=(xmax*ymax);
					}

					index=j*xmax+i;
					for(k=0;k<zmax;k++)
					{  
						d=buff[k];
						if(d!=0)
						{  
							rmax=(sqrt(d)/zp)+1;
							rstart=(rmax>k)?k:rmax;
							rend=(rmax>(zmax-1-k))?(zmax-1-k):rmax;
							for(n=((int)(-rstart));n<=((int)rend);n++)
							{  
								w=buff[k+n]+ mocnina[((n<0)?(-1*n):(n))];
								if(w<d)d=w;
							}
							pObraz[index]=d;
						}
						index+=(xmax*ymax);
					}
				}
		}

		if(odmocnina)
		{  
			konec=(zmax-1)*xmax*ymax+(ymax-1)*xmax+xmax-1;
			for(index=0;index<=konec;index++)
				pObraz[index]=sqrt(pObraz[index]);
		}
	} // SlowSaito

	//--------------------------------------------------------------------------
	// Explicit instantiations
	//--------------------------------------------------------------------------
	template I3D_DLLEXPORT void SlowSaito(Image3d<float> &Obraz, float Image_background, bool odmocnina);

	//--------------------------------------------------------------------------
	// FastSaito
	//--------------------------------------------------------------------------
	template <typename T> I3D_DLLEXPORT void FastSaito(Image3d<T> &Obraz, T Image_Background, bool odmocnina)
	{  
		const int xmax = Obraz.GetWidth(), ymax = Obraz.GetHeight(), zmax = Obraz.GetNumSlices(); // size of picture

		Vector3d<float> res = Obraz.GetResolution().GetRes();
		const T xp=1.0/res.x, // value of one x step
			    yp=1.0/res.y, // value of one y step
				zp=1.0/res.z; // value of one z step
		const T xp2=xp*xp, yp2=yp*yp, zp2= zp*zp;

		T voxel, m, skok, b;
		size_t index, konec;
		bool nasel, nkonec;
		int i,j,k,n,vzdalenost;

		size_t a;

		int max=(xmax<ymax)?ymax:xmax;
		max=(zmax<max)?max:zmax;
		std::valarray<T> buff(max), mocnina(max+1);
		std::valarray<T> &pObraz = Obraz.GetVoxelData();

		//step 1
		for(i=0;i<xmax;i++) mocnina[i]=xp2*i*i;
		index=0;
		for(k=0;k<zmax;k++)
			for(j=0;j<ymax;j++)
			{  //forward scan
				nasel=false;
				i=0;
				index=k*xmax*ymax+j*xmax;
				while((!nasel)&&(i<xmax))
				{  
					if(pObraz[index]==Image_Background)
					{  
						pObraz[index]=max_saito;
						buff[i]=max_saito;
					}
					else 
					{
						pObraz[index]=0;
						buff[i]=0;
						nasel=true;
					}
					i+=1;
					index+=1;
				}
				vzdalenost=0;
				for(n=i;n<xmax;n++)
				{  
					if(pObraz[index]==Image_Background)
					{  
						vzdalenost+=1;
						pObraz[index]=mocnina[vzdalenost];
						buff[n]=vzdalenost;
					}
					else 
					{
						vzdalenost=0;
						pObraz[index]=0;
						buff[n]=0;
					}
					index+=1;
				}

				//backward scan
				index=k*xmax*ymax+j*xmax+xmax-1;
				i=xmax-1;
				voxel=buff[i];
				if(voxel!=max_saito)
				{  
					if(voxel!=0)
					{ 
						i-=(((int)voxel)+1);
						index-=(((int)voxel)+1);
					}
					else 
					{
						i-=1;
						index-=1;
					}
					while(i>=0)
					{  
						voxel=buff[i];
						if(voxel==max_saito) skok=i+1;
						else skok=(voxel)/2;
						vzdalenost=1;
						for(n=0;n<skok;n++)
						{ 
							pObraz[index]=mocnina[vzdalenost];
							vzdalenost+=1;
							index-=1;
							i-=1;
						}
						i-=(((int)skok)+1);
						index-=(((int)skok)+1);
					}
				}
			}

		//step 2
		for(j=0;j<=ymax;j++) mocnina[j]=yp2*j*j;
		for(k=0;k<zmax;k++)
			for(i=0;i<xmax;i++)
			{
				index=k*(xmax*ymax)+i;
				for(j=0;j<ymax;j++)
				{
					buff[j]=pObraz[index];
					index+=xmax;
				}

				//forward scan
				a=0;
				for(j=1;j<ymax;j++)
				{ 
					if (a>0) a-=1;
					if (buff[j]>buff[j-1]+yp2)
					{ 
						b=((buff[j]-buff[j-1]-yp2)/(2*yp2));
						if ((j+b)>(ymax-1)) b=(ymax-1)-j;
						index=k*xmax*ymax+(j+a)*xmax+i;
						nkonec=true;
						for(n=a;((n<=b)&&(nkonec));n++)
						{ 
							m=buff[j-1]+mocnina[n+1];
							if(buff[j+n]<=m) 
							{ 
								nkonec=false;                                         
							}
							else
							{
								if(pObraz[index]>m)
									pObraz[index]=m;
							}
							index+=xmax;
						}
						a=(size_t)b;
					}
					else a=0;
				}
                
				//backward scan
				a=0;
				for(j=ymax-2;j>=0;j--)
				{  
					if(a>0) a-=1;
					if(buff[j]>buff[j+1]+yp2)
					{
						b=((buff[j]-buff[j+1]-yp2)/(2*yp2));
						if((j-b)<0) b=j;
						index=k*xmax*ymax+(j-a)*xmax+i;
						nkonec=true;
						for(n=a;((n<=b)&&(nkonec));n++)
						{ 
							m=buff[j+1]+ mocnina[n+1];
							if (buff[j-n]<=m) 
							{  
								nkonec=false;
							}
							else 
							{
								if(pObraz[index]>m)
									pObraz[index]=m;
							}
							index-=xmax;
						}
						a=(size_t)b;
					}
					else a=0;
				}
			}

		//step 3
		if(zmax>1)
		{  
			for(k=0;k<=zmax;k++) mocnina[k]=zp2*k*k;
			for(j=0;j<ymax;j++)
				for(i=0;i<xmax;i++)
				{
					index=j*xmax+i;
					for(k=0;k<zmax;k++)
					{ 
						buff[k]=pObraz[index];
						index+=(xmax*ymax);
					}

					//forward scan
					a=0;
					for(k=1;k<zmax;k++)
					{
						if (a>0) a-=1;
						if (buff[k]>buff[k-1]+zp2)
						{
							b=((buff[k]-buff[k-1]-zp2)/(2*zp2));
							if ((k+b)>(zmax-1)) b=(zmax-1)-k;
							index=(k+a)*xmax*ymax+j*xmax+i;
							nkonec=true;
							for(n=a;((n<=b)&&(nkonec));n++)
							{ 
								m=buff[k-1]+ mocnina[n+1];
								if(buff[k+n]<=m) 
								{
									nkonec=false;
								}
								else 
								{
									if(pObraz[index]>m)
										pObraz[index]=m;                                            
								}
								index+=(xmax*ymax);
							}
							a=(size_t)b;
						}
						else a=0;
					}

					//backward scan
					a=0;
					for(k=zmax-2;k>=0;k--)
					{ 
						if(a>0) a-=1;
						if(buff[k]>buff[k+1]+zp2)
						{
							b=((buff[k]-buff[k+1]-zp2)/(2*zp2));
							if((k-b)<0) b=k;
							index=(k-a)*xmax*ymax+j*xmax+i;
							nkonec=true;
							for(n=a;((n<=b)&&(nkonec));n++)
							{
								m=buff[k+1]+ mocnina[n+1];
								if (buff[k-n]<=m) 
								{
									nkonec=false;
								}
								else 
								{
									if(pObraz[index]>m)
										pObraz[index]=m;
								}
								index-=(xmax*ymax);
							}
							a=(size_t)b;
						}
						else a=0;
					}
            }
		}

		if(odmocnina)
		{  
			konec=(zmax-1)*xmax*ymax+(ymax-1)*xmax+xmax-1;
			for(index=0;index<=konec;index++)
				pObraz[index]=sqrt(pObraz[index]);
		}
	} // FastSaito

	//--------------------------------------------------------------------------
	// Explicit instantiations
	//--------------------------------------------------------------------------
	template I3D_DLLEXPORT void FastSaito(Image3d<float>& Obraz,float Image_background, bool odmocnina);

	/***************************************************************************\
	*
	*                     D-Euclidean distance transforms          
	*
	\***************************************************************************/
	//--------------------------------------------------------------------------
	// DeuclideanDistance
	//--------------------------------------------------------------------------
	template <typename T> void DeuclideanDistance(
		Image3d<T> &Obraz,
		byte kvalita,
		T Image_Background, 
		bool odmocnina)
	{  
		const int zmax = Obraz.GetNumSlices();
		if (zmax == 1)
		{
			DeuclideanDistance2D(Obraz, kvalita, Image_Background, odmocnina);
		}
		if (zmax > 1)
		{
			DeuclideanDistance3D(Obraz, kvalita, Image_Background, odmocnina);
		}
	} // DeuclideanDistance

	//--------------------------------------------------------------------------
	// Explicit instantiations
	//--------------------------------------------------------------------------
	template void DeuclideanDistance(Image3d<float> &Obraz, byte kvalita, float Image_background, bool odmocnina);

} // i3d namespace

