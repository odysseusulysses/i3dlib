/*
 * i3dlib - image manipulation library
 *
 * Copyright (C) 2000-2006   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 * FILE: fourier.cc
 *
 * Image convolution
 *
 * David Svoboda <svoboda@fi.muni.cz> 2009
 */

/** \file 
 * Forward and backward Fourier transform
 *
 * \author David Svoboda <svoboda@fi.muni.cz> 
 * \date 2009
 */

#include <stdlib.h>
#include <typeinfo>
#include <iostream>
#include <sstream>
#include <complex>
#include <fftw3.h>


#include "toolbox.h"
#include "fourier.h"

#define ABS(x) (((x) > 0) ? x : -(x))
#define SQR(x) ((x)*(x))

#ifdef _WIN32
	#include <windows.h>
	volatile HANDLE hMutex = CreateMutex(NULL,FALSE,NULL);
#else
	#include <pthread.h>
	pthread_mutex_t job_queue_mutex = PTHREAD_MUTEX_INITIALIZER;
#endif

using namespace std;

namespace i3d {


//------------------------------------------------------------------------
void MUTEX_LOCK()
{
#ifdef _WIN32 
	WaitForSingleObject(hMutex,INFINITE);
#else
	pthread_mutex_lock(&job_queue_mutex);
#endif
}
//------------------------------------------------------------------------


//------------------------------------------------------------------------
void MUTEX_UNLOCK()
{
#ifdef _WIN32
	ReleaseMutex(hMutex);
#else
	pthread_mutex_unlock(&job_queue_mutex);
#endif
}
//------------------------------------------------------------------------

//--------------------------------------------------------------------------
// Begin of FFTW template wrappers
//--------------------------------------------------------------------------
#ifdef ALGO_WITH_MULTITHREADING
template <class T> void _fftw_plan_with_nthreads(int nthreads)
{
	 ostringstream os;
	 os << "i3d::fftw_plan_with_nthreads<" << 
				typeid(T).name() << ">(...) not implemented.";

	 throw LibException(os.str().c_str());
}


template <> void _fftw_plan_with_nthreads<float>(int nthreads)
{
	 fftwf_plan_with_nthreads(nthreads);
}

template <> void _fftw_plan_with_nthreads<double>(int nthreads)
{
	 fftw_plan_with_nthreads(nthreads);
}

template <> void _fftw_plan_with_nthreads<long double>(int nthreads)
{
	 fftwl_plan_with_nthreads(nthreads);
}

//--------------------------------------------------------------------------
template <class T> int _fftw_init_threads(void)
{
	 ostringstream os;
	 os << "i3d::fftw_init_threads<" << 
				typeid(T).name() << ">(void) not implemented.";

	 throw LibException(os.str().c_str());
}

template <> int _fftw_init_threads<float>(void)
{
	 return fftwf_init_threads();
}

template <> int _fftw_init_threads<double>(void)
{
	 return fftw_init_threads();
}

template <> int _fftw_init_threads<long double>(void)
{
	 return fftwl_init_threads();
}
#endif

//--------------------------------------------------------------------------
template <class T> void* _fftw_malloc(size_t n) 
{
	 ostringstream os;
	 os << "i3d::fftw_malloc<" << 
				typeid(T).name() << ">(...) not implemented.";

	 throw LibException(os.str().c_str());
}

template <> void* _fftw_malloc<float>(size_t n)
{
	 return fftwf_malloc(n);
}

template <> void* _fftw_malloc<double>(size_t n)
{
	 return fftw_malloc(n);
}

template <> void * _fftw_malloc<long double>(size_t n)
{
	 return fftwl_malloc(n);
}

//--------------------------------------------------------------------------
template <class T> struct _fftw_complex 
{ 
	 _fftw_complex()
	 {
		  ostringstream os;
		  os << "i3d::fftw_complex<" << typeid(T).name() << "> not implemented."; 
		  
		  throw LibException(os.str().c_str());
	 };
};

template <> struct _fftw_complex<float> 
{ 
	 typedef fftwf_complex type; 

	 type value;
};

template <> struct _fftw_complex<double> 
{ 
	 typedef fftw_complex type; 

	 type value;
};

template <> struct _fftw_complex<long double> 
{ 
	 typedef  fftwl_complex type; 

	 type value;
};

//--------------------------------------------------------------------------
template <class T> struct _fftw_plan 
{
	 _fftw_plan()
	 {
		  ostringstream os;
		  os << "i3d::fftw_plan<" << typeid(T).name() << "> not implemented."; 
		  
		  throw LibException(os.str().c_str());
	 };
};

template <> struct _fftw_plan<float> 
{ 
	 typedef fftwf_plan type; 
	 type plan;

	 _fftw_plan(fftwf_plan input):plan(input) {};
};

template <> struct _fftw_plan<double> 
{ 
	 typedef fftw_plan type; 
	 type plan;

	 _fftw_plan(fftw_plan input):plan(input) {};
};

template <> struct _fftw_plan<long double> 
{ 
	 typedef fftwl_plan type; 
	 type plan;

	 _fftw_plan(fftwl_plan input):plan(input) {};
};

//--------------------------------------------------------------------------

template <class T> _fftw_plan<T> _fftw_plan_dft_c2r_3d(int n0, int n1, int n2, _fftw_complex<T> *in, T *out, unsigned flags)
{
	 ostringstream os;
	 os << "i3d::fftw_plan_dft_c2r_3d<" << typeid(T).name() << 
				">(...) not implemented.";

	 throw LibException(os.str().c_str());
}

template <> _fftw_plan<float> _fftw_plan_dft_c2r_3d(int n0, int n1, int n2, _fftw_complex<float> *in, float *out, unsigned flags)
{
	 fftwf_plan plan = fftwf_plan_dft_c2r_3d(n0, n1, n2, 
											(fftwf_complex *) in, out,
											flags);

	 return _fftw_plan<float>(plan);
}

template <> _fftw_plan<double> _fftw_plan_dft_c2r_3d(int n0, int n1, int n2, _fftw_complex<double> *in, double *out, unsigned flags)
{
	 fftw_plan plan = fftw_plan_dft_c2r_3d(n0, n1, n2, 
											(fftw_complex *) in, out,
											flags);

	 return _fftw_plan<double>(plan);
}

template <> _fftw_plan<long double> _fftw_plan_dft_c2r_3d(int n0, int n1, int n2, _fftw_complex<long double> *in, long double *out, unsigned flags)
{
	 fftwl_plan plan = fftwl_plan_dft_c2r_3d(n0, n1, n2, 
											(fftwl_complex *) in, out,
											flags);

	 return _fftw_plan<long double>(plan);
}

//--------------------------------------------------------------------------

template <class T> _fftw_plan<T> _fftw_plan_dft_r2c_3d(int n0, int n1, int n2, T *in, _fftw_complex<T> *out, unsigned flags)
{
	 ostringstream os;
	 os << "i3d::fftw_plan_dft_r2c_3d<" << typeid(T).name() << 
				">(...) not implemented.";

	 throw LibException(os.str().c_str());
}

template <> _fftw_plan<float> _fftw_plan_dft_r2c_3d(int n0, int n1, int n2, float *in, _fftw_complex<float> *out, unsigned flags)
{
	 fftwf_plan plan = fftwf_plan_dft_r2c_3d(n0, n1, n2, in, 
											(fftwf_complex*) out,
											flags);

	 return _fftw_plan<float>(plan);
}

template <> _fftw_plan<double> _fftw_plan_dft_r2c_3d(int n0, int n1, int n2, double *in, _fftw_complex<double> *out, unsigned flags)
{
	 fftw_plan plan = fftw_plan_dft_r2c_3d(n0, n1, n2, in, 
										(fftw_complex *) out, 
										flags);

	 return _fftw_plan<double>(plan);
}

template <> _fftw_plan<long double> _fftw_plan_dft_r2c_3d(int n0, int n1, int n2, long double *in, _fftw_complex<long double> *out, unsigned flags)
{
	 fftwl_plan plan = fftwl_plan_dft_r2c_3d(n0, n1, n2, in, 
										(fftwl_complex *) out, 
										flags);

	 return _fftw_plan<long double>(plan);
}

//--------------------------------------------------------------------------

template <class T> void _fftw_execute(const _fftw_plan<T> plan)
{
	 ostringstream os;
	 os << "i3d::fftw_execute<" << 
				typeid(T).name() << ">(...) not implemented.";

	 throw LibException(os.str().c_str());

}

template <> void _fftw_execute(const _fftw_plan<float> plan)
{
	 fftwf_execute(plan.plan);
}

template <> void _fftw_execute(const _fftw_plan<double> plan)
{
	 fftw_execute(plan.plan);
}

template <> void _fftw_execute(const _fftw_plan<long double> plan)
{
	 fftwl_execute(plan.plan);
}

//--------------------------------------------------------------------------

template <class T> void _fftw_destroy_plan(_fftw_plan<T> plan)
{
	 ostringstream os;
	 os << "i3d::fftw_destroy_plan<" << 
				typeid(T).name() << ">(...) not implemented.";

	 throw LibException(os.str().c_str());
}

template <> void _fftw_destroy_plan(_fftw_plan<float> plan)
{
	 fftwf_destroy_plan(plan.plan);
}

template <> void _fftw_destroy_plan(_fftw_plan<double> plan)
{
	 fftw_destroy_plan(plan.plan);
}

template <> void _fftw_destroy_plan(_fftw_plan<long double> plan)
{
	 fftwl_destroy_plan(plan.plan);
}

//--------------------------------------------------------------------------

template <class T> void _fftw_free(void *memory)
{
	 ostringstream os;
	 os << "i3d::fftw_free<" << 
				typeid(T).name() << ">(...) not implemented.";

	 throw LibException(os.str().c_str());
}

template <> void _fftw_free<float>(void *memory)
{
	 fftwf_free(memory);
}

template <> void _fftw_free<double>(void *memory)
{
	 fftw_free(memory);
}

template <> void _fftw_free<long double>(void *memory)
{
	 fftwl_free(memory);
}

//--------------------------------------------------------------------------
// End of FFTW template wrappers 
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
/**
 * Function return how much memory is occupied when performing FFT.
 * Approximately, it is: X*Y*Z*sizeof(PRECISION)
 */
//--------------------------------------------------------------------------
template <class PRECISION> I3D_DLLEXPORT
unsigned long long GetFFTWBufferSize(Vector3d<size_t> img_sz)
{
	 size_t buffer_line_length = 2 * (img_sz.x/2 + 1);

	 return (buffer_line_length * img_sz.y * img_sz.z * sizeof(PRECISION));
}

//--------------------------------------------------------------------------
/**
 * When FFTW works 'in-place' the memory must be padded along x-axis 
 * following this rule: xsize -> 2*(xsize/2 + 1). This function preforms 
 * the padding only.
 */
//--------------------------------------------------------------------------
template <class S, class T> 
void RealImage2FFTWBuffer(const Image3d<S>& img, T* fftw_buffer, T factor)
{
	 Vector3d<size_t> img_sz = img.GetSize();
    // real -> multiplied by "2"!
	 size_t buffer_line_length = 2 * (img_sz.x/2 + 1);
	 size_t buffer_slice_sz = img_sz.y * buffer_line_length;

	 const S *it_img = img.GetFirstVoxelAddr();

	 for (size_t z=0; z<img_sz.z; z++)
		  for (size_t y=0; y<img_sz.y; y++)
		  {
				T *it_buffer = fftw_buffer 
						  + buffer_slice_sz*z 
						  + buffer_line_length*y;

				// track the line and copy the data
				for (size_t x=0; x<img_sz.x; x++)
				{
					 *it_buffer = static_cast<T>(*it_img) * factor;

					 it_buffer++;
					 it_img++;
				}
		  }
}

//--------------------------------------------------------------------------
/**
 * Function rotates the image with respect to the given vector.
 */
//--------------------------------------------------------------------------
template <class T> I3D_DLLEXPORT
void FFTshift(Image3d<T> &img, const Vector3d<int> &shift)
{
	 T *buffer_line;
	 size_t x,y,z;
	 size_t sz = img.GetSizeZ(),
			  sy = img.GetSizeY(),
			  sx = img.GetSizeX();

	 Vector3d<size_t> positive_shift;
	 positive_shift.x = size_t(shift.x % int(sx));
	 positive_shift.y = size_t(shift.y % int(sy));
	 positive_shift.z = size_t(shift.z % int(sz));

	 // x - axis
	 buffer_line = new T[sx];

	 for (z=0; z<sz; z++)
		  for (y=0; y<sy; y++)
		  {
				for (x=0; x<sx; x++)
					 buffer_line[x] = 
								*img.GetVoxelAddr((positive_shift.x + x) % sx, y, z);

				memcpy(img.GetVoxelAddr(0,y,z), buffer_line, sx*sizeof(T));
		  }

	 delete [] buffer_line;

	 // y-axis
	 buffer_line = new T[sy];

	 for (z=0; z<sz; z++)
		  for (x=0; x<sx; x++)
		  {
				for (y=0; y<sy; y++)
					 buffer_line[y] = 
								*img.GetVoxelAddr(x, (positive_shift.y + y) % sy, z);

				for (y=0; y<sy; y++)
					 *img.GetVoxelAddr(x,y,z) = buffer_line[y];
		  }
	 delete [] buffer_line;

	 // z-axis
	 buffer_line = new T[sz];

	 for (y=0; y<sy; y++)
		  for (x=0; x<sx; x++)
		  {
				for (z=0; z<sz; z++)
					 buffer_line[z] = 
								*img.GetVoxelAddr(x, y, (positive_shift.z + z) % sz);

				for (z=0; z<sz; z++)
					 *img.GetVoxelAddr(x,y,z) = buffer_line[z];
		  }
	 delete [] buffer_line;
}

//--------------------------------------------------------------------------
/**
 * Copy the results of FFT (stored in the buffer) into the ordinary image
 * This image contain complex data.
 */
//--------------------------------------------------------------------------
template <class T> 
void FFTWBuffer2ComplexImage(const _fftw_complex<T>* cbuffer, 
									  Image3d<complex<T>/**/> &img)
{
  Vector3d<size_t> img_sz = img.GetSize();
  // complex -> not multiplied by "2"!
  size_t buffer_line_length = img_sz.x / 2 + 1; 
  size_t buffer_slice_sz = img_sz.y * buffer_line_length;

  // process only the first half of the processed image:
  for (size_t z=0; z<img_sz.z; z++)
		for (size_t y=0; y<img_sz.y; y++)
		{
			 const _fftw_complex<T> *it_buffer = cbuffer 
						+ buffer_slice_sz * z 
						+ buffer_line_length * y; 

			 complex<T> *it_img = img.GetFirstVoxelAddr() 
						+ img.GetSliceSize() * z 
						+ img.GetSizeX() * y;

			 size_t length = buffer_line_length * sizeof(complex<T>);

			 memcpy(it_img, it_buffer, length);
		}

  // fill the second half of the matrix
  // symmetry:
  // x -> [(sx-1) - x + 1] % sx
  // y -> [(sy-1) - y + 1] % sy
  // z -> [(sz-1) - z + 1] % sz
  for (size_t z=0; z<img_sz.z; z++)
		for (size_t y=0; y<img_sz.y; y++)
			 for (size_t x=buffer_line_length; x<img_sz.x; x++)
			 {
				  // find mirrored position
				  size_t mx = (img_sz.x - x) % img_sz.x,
							my = (img_sz.y - y) % img_sz.y,
							mz = (img_sz.z - z) % img_sz.z;

				  // get the value
				  complex<T> value = img.GetVoxel(mx, my, mz);

				  // store conjugated value
				  img.SetVoxel(x,y,z, conj(value));
			 }
}

//--------------------------------------------------------------------------
/**
 * Copy the results of inverse FT into the ordinary (real) image 
 */
//--------------------------------------------------------------------------
template <class S, class T> 
void FFTWBuffer2RealImage(const S* fftw_buffer, Image3d<T>& img, S factor)
{
	 Vector3d<size_t> img_sz = img.GetSize();
    // real - multiplied by "2"!
	 size_t buffer_line_length = 2 * (img_sz.x/2 + 1);
	 size_t slice_sz = img_sz.y * buffer_line_length;

	 T *it_img = img.GetFirstVoxelAddr();

	 if (std::numeric_limits<T>::is_integer)
	 {
		 for (size_t z=0; z<img_sz.z; z++)
			  for (size_t y=0; y<img_sz.y; y++)
			  {
					const S *it_buffer = fftw_buffer 
							  + slice_sz * z 
							  + buffer_line_length * y;
	
					// track the line and copy the data
					for (size_t x=0; x<img_sz.x; x++)
					{
						 *it_img = static_cast<T>(round(*it_buffer * factor));
	
						 it_buffer++;
						 it_img++;
					}
			  }
	 }
	 else
	 {
		 for (size_t z=0; z<img_sz.z; z++)
			  for (size_t y=0; y<img_sz.y; y++)
			  {
					const S *it_buffer = fftw_buffer 
							  + slice_sz * z 
							  + buffer_line_length * y;
	
					// track the line and copy the data
					for (size_t x=0; x<img_sz.x; x++)
					{
						 *it_img = static_cast<T>(*it_buffer * factor);
	
						 it_buffer++;
						 it_img++;
					}
			  }
	 }
}

//--------------------------------------------------------------------------
/**
 * Copy the image content into the FFTW-ready buffer
 */
//--------------------------------------------------------------------------
template <class T> 
void ComplexImage2FFTWBuffer(const Image3d<complex<T>/**/> &img,
									  _fftw_complex<T>* cbuffer)
{
  Vector3d<size_t> img_sz = img.GetSize();
  // complex - not multiplied by "2"!
  size_t buffer_line_length = img_sz.x / 2 + 1; 
  size_t buffer_slice_sz = img_sz.y * buffer_line_length;

  for (size_t z=0; z<img_sz.z; z++)
		for (size_t y=0; y<img_sz.y; y++)
		{
			 const complex<T> *it_img = img.GetFirstVoxelAddr() 
						+ img.GetSliceSize() * z 
						+ img.GetSizeX() * y;

			 _fftw_complex<T> *it_buffer = cbuffer 
						+ buffer_slice_sz * z 
						+ buffer_line_length * y; 

			 size_t length = buffer_line_length * sizeof(complex<T>);

			 // copy only the half of the image due to the symmetry
			 memcpy(it_buffer, it_img, length);
		}
}

//--------------------------------------------------------------------------
/** 
 * This function performs forward fast Fourier transform. 
 * Default configuration does not perform normalization
 */
//--------------------------------------------------------------------------
template < class T, class PRECISION> I3D_DLLEXPORT
void FFT(const Image3d<T> &real_img, 
			Image3d<complex<PRECISION>/**/> &complex_img,
			bool normalize)
{
#ifdef WITH_FFTW

#ifdef I3D_DEBUG
	cout << "FFT: forward Fourier transform over image: " << 
			  real_img.GetSize() << endl;
#endif

	
#ifdef ALGO_WITH_MULTITHREADING
	// Note:
	// If the computer contains dual (or more) core CPU, we can utilize
	// multi-threading computing. The acceleration of computation is not 
	// enormous but not negligible. It was experimentally found out that
	// running two or more independent multi-threaded processes does not 
	// reduce the computation speed. Hence, each process can ask for 
	// running its task over all the available CPUs. On the other hand the
	// process should NOT ask for running N threads, where N > CPUs. It has
	// considerable (negative) impact on the performance.
	size_t CPUs = GetNumberOfProcessors();


	MUTEX_LOCK();

 	_fftw_init_threads<PRECISION>();
   _fftw_plan_with_nthreads<PRECISION>(CPUs);

	MUTEX_UNLOCK();

#endif

	 // the VOI covering the whole image area
	 Vector3d<size_t> sz = real_img.GetSize();

	 // allocate the required memory, if failed raise an exception
	 unsigned long long buffer_size = GetFFTWBufferSize<PRECISION>(sz);
	 PRECISION *buffer = (PRECISION*) _fftw_malloc<PRECISION>(buffer_size);
	 
	 if (buffer == NULL)
	 {
		  ostringstream os;

		  os << "i3dlib::FourierTranform<" << typeid(PRECISION).name() <<
					 "> ... memory allocation failed.";
		  throw LibException(os.str().c_str());
	 }

	 // clear the memory buffer (all the positions are set to zero)
	 memset (buffer, 0, buffer_size);

	 // By default, the FT is not normalized. If required, each item is
	 // divided by factor (number of elements in the processed image).
	 PRECISION factor = 1;
	 if (normalize)
	 {
		  factor = 1.0 / sqrt(static_cast<PRECISION>(real_img.GetImageSize()));
	 }
	 
	 // copy the input image data to the allocated (real) buffer and prepare
	 // the output (complex) buffer. Notice that these buffers point to the 
	 // same address. It means that the FFT will be computed 'in-place'.
	 RealImage2FFTWBuffer(real_img, buffer, factor); 
	 _fftw_complex<PRECISION> *cbuffer = (_fftw_complex<PRECISION> *) buffer; 
	
	 // prepare the FFTW plan for given data
	 
#ifdef ALGO_WITH_MULTITHREADING
	 MUTEX_LOCK();
#endif


	 _fftw_plan<PRECISION> plan = 
		 _fftw_plan_dft_r2c_3d<PRECISION> (sz.z, sz.y, sz.x, 
													  buffer, cbuffer,
													  FFTW_ESTIMATE); 

	 
#ifdef ALGO_WITH_MULTITHREADING
	 MUTEX_UNLOCK();
#endif

	 // perform the forward FFT
	 _fftw_execute<PRECISION>(plan);

	 // release the memory allocated by FFTW plan
	 _fftw_destroy_plan<PRECISION> (plan); 
	 
	 // allocate the new (complex) image
	 complex_img.CopyMetaData(real_img);
	 complex_img.Clear(); 

	 // copy the results from FFTW-ready buffer into the ordinary image
	 FFTWBuffer2ComplexImage(cbuffer, complex_img); 
	 
	 // release the memory occupied by the FFTW buffer.
	 _fftw_free<PRECISION>(buffer); 

#ifdef I3D_DEBUG
	cout << "FFT: forward Fourier transform finished" << endl;
#endif

#else // WITH_FFTW
  throw InternalException("i3dlib: FFT support is missing. "
								  "Enable WITH_FFTW flag and recompile the i3dlib.");
#endif // WITH_FFTW
}

//--------------------------------------------------------------------------
/** This function performs inverse fast Fourier transform. 
 * Default configuration does not perform normalization
 */
//--------------------------------------------------------------------------
template < class T, class PRECISION> I3D_DLLEXPORT
void IFFT(const Image3d<std::complex<PRECISION>/**/> &complex_img, 
			 Image3d<T> &real_img,
			 bool normalize)
{
#ifdef WITH_FFTW

#ifdef I3D_DEBUG
	cout << "IFFT: inverse Fourier transform over image " << complex_img.GetSize() << endl;
#endif

	
#ifdef ALGO_WITH_MULTITHREADING
	 // if required, use multi-threaded computing
	 size_t CPUs = GetNumberOfProcessors();

	 MUTEX_LOCK();

	 _fftw_init_threads<PRECISION>();
	 _fftw_plan_with_nthreads<PRECISION>(CPUs);

	 MUTEX_UNLOCK();
#endif


	 // the VOI covering the whole image area
	 Vector3d<size_t> sz = complex_img.GetSize();

	 // allocate the required memory, if failed raise an exception
	 unsigned long long buffer_size = GetFFTWBufferSize<PRECISION>(sz);
	 _fftw_complex<PRECISION> *cbuffer = 
				(_fftw_complex<PRECISION>*) _fftw_malloc<PRECISION>(buffer_size); 
	 
	 if (cbuffer == NULL)
	 {
		  ostringstream os;

		  os << "i3dlib::FourierTranform<" << typeid(PRECISION).name() <<
					 "> ... memory allocation failed.";
		  throw LibException(os.str().c_str());
	 }
	 
	 // clear the memory buffer (all the positions are set to zero)
	 memset (cbuffer, 0, buffer_size);
	 
	 // copy the input image data to the allocated (complex) buffer and prepare
	 // the output (real) buffer. Notice that these buffers point to the 
	 // same address. It means that the FFT will be computed 'in-place'.
	 ComplexImage2FFTWBuffer(complex_img, cbuffer); 
	 PRECISION *buffer = (PRECISION *) cbuffer; 
	
	 // prepare the FFT plan for given data
	 
#ifdef ALGO_WITH_MULTITHREADING
	 MUTEX_LOCK();
#endif

	 _fftw_plan<PRECISION> plan = 
		 _fftw_plan_dft_c2r_3d<PRECISION> (sz.z, sz.y, sz.x, 
													  cbuffer, buffer,
													  FFTW_ESTIMATE);

	 
#ifdef ALGO_WITH_MULTITHREADING
	 MUTEX_UNLOCK();
#endif

	 // perform the inverse FT
	 _fftw_execute<PRECISION>(plan);

	 // release the memory occupied by the FFT plan
	 _fftw_destroy_plan<PRECISION> (plan);
	 
	 // allocate the new image
	 real_img.CopyMetaData(complex_img);
	 real_img.Clear(); 

	 // By default, the FT is not normalized. If required, each item is
	 // divided by factor (number of elements in the processed image).
	 PRECISION factor = 1;
	 if (normalize)
	 {
		  factor = 1.0/sqrt(static_cast<PRECISION>(complex_img.GetImageSize()));
	 }
	
	 // copy the results from FFTW-like buffer to ordinary image
	 FFTWBuffer2RealImage(buffer, real_img, factor); 
	
	 // release the memory occupied by the FFTW buffer
	 _fftw_free<PRECISION>(cbuffer); 

#ifdef I3D_DEBUG
	cout << "IFFT: inverse Fourier transform finished" << endl;
#endif

#else // WITH_FFTW
  throw InternalException("i3dlib: FFT support is missing. "
								  "Enable WITH_FFTW flag and recompile the i3dlib.");
#endif // WITH_FFTW

}

//--------------------------------------------------------------------------
// explicit instantiation
//--------------------------------------------------------------------------

// single precision
template I3D_DLLEXPORT 
void FFT(const Image3d<BINARY> &,
			Image3d<complex<float>/**/> &,
			bool);

template I3D_DLLEXPORT 
void FFT(const Image3d<GRAY8> &,
			Image3d<complex<float>/**/> &,
			bool);

template I3D_DLLEXPORT 
void FFT(const Image3d<GRAY16> &,
			Image3d<complex<float>/**/> &,
			bool);

template I3D_DLLEXPORT 
void FFT(const Image3d<float> &,
			Image3d<complex<float>/**/> &,
			bool);

template I3D_DLLEXPORT 
void FFT(const Image3d<double> &,
			Image3d<complex<float>/**/> &,
			bool);

template I3D_DLLEXPORT 
void IFFT(const Image3d<complex<float>/**/> &,
			 Image3d<GRAY8> &,
			 bool);

template I3D_DLLEXPORT 
void IFFT(const Image3d<complex<float>/**/> &,
			 Image3d<GRAY16> &,
			 bool);

template I3D_DLLEXPORT 
void IFFT(const Image3d<complex<float>/**/> &,
			 Image3d<float> &,
			 bool);

template I3D_DLLEXPORT 
void IFFT(const Image3d<complex<float>/**/> &,
			 Image3d<double> &,
			 bool);

// double precision
template I3D_DLLEXPORT 
void FFT(const Image3d<BINARY> &,
			Image3d<complex<double>/**/> &,
			bool);

template I3D_DLLEXPORT 
void FFT(const Image3d<GRAY8> &,
			Image3d<complex<double>/**/> &,
			bool);

template I3D_DLLEXPORT 
void FFT(const Image3d<GRAY16> &,
			Image3d<complex<double>/**/> &,
			bool);

template I3D_DLLEXPORT 
void FFT(const Image3d<float> &,
			Image3d<complex<double>/**/> &,
			bool);

template I3D_DLLEXPORT 
void FFT(const Image3d<double> &,
			Image3d<complex<double>/**/> &,
			bool);

template I3D_DLLEXPORT 
void IFFT(const Image3d<complex<double>/**/> &,
			 Image3d<GRAY8> &,
			 bool);

template I3D_DLLEXPORT 
void IFFT(const Image3d<complex<double>/**/> &,
			 Image3d<GRAY16> &,
			 bool);

template I3D_DLLEXPORT 
void IFFT(const Image3d<complex<double>/**/> &,
			 Image3d<float> &,
			 bool);

template I3D_DLLEXPORT 
void IFFT(const Image3d<complex<double>/**/> &,
			 Image3d<double> &,
			 bool);

// fft shift
template I3D_DLLEXPORT 
void FFTshift(Image3d<BINARY> &img, const Vector3d<int> &shift);

template I3D_DLLEXPORT 
void FFTshift(Image3d<GRAY8> &img, const Vector3d<int> &shift);

template I3D_DLLEXPORT 
void FFTshift(Image3d<GRAY16> &img, const Vector3d<int> &shift);

template I3D_DLLEXPORT 
void FFTshift(Image3d<float> &img, const Vector3d<int> &shift);

template I3D_DLLEXPORT 
void FFTshift(Image3d<double> &img, const Vector3d<int> &shift);

// evaluate the amount of needed space
template I3D_DLLEXPORT 
unsigned long long GetFFTWBufferSize<float>(Vector3d<size_t> img_sz);

template I3D_DLLEXPORT 
unsigned long long GetFFTWBufferSize<double>(Vector3d<size_t> img_sz);

}
