/*
 * i3dlib - image manipulation library
 *
 * Copyright (C) 2000-2006   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** \file neigbours.h
  \brief Definition of neighbourhood - implementation.

  \author Pavel Matula (pam@fi.muni.cz) 2001

*/

#ifdef __GNUG__
#	pragma implementation
#endif

#include <algorithm>
#include <iostream>
#ifndef _MSC_VER
#include <math.h>
#endif
#include "neighbours.h"
#include "vector3d.h"
#include "image3d.h"
#include "i3dassert.h"

namespace i3d {
	/***************************************************************************\
	*
	*                               Local definitions
	*
	\***************************************************************************/
	//--------------------------------------------------------------------------
	// SQR(X)
	//--------------------------------------------------------------------------
#	define SQR(x) ((x)*(x))

	//--------------------------------------------------------------------------
	// get_max(x,y)
	//--------------------------------------------------------------------------
#	define get_max(x,y) (x=((x)>(y)?(x):(y)))

	//--------------------------------------------------------------------------
	// get_min(x,y)
	//--------------------------------------------------------------------------
#	define get_min(x,y) (x=((x)<(y)?(x):(y)))

	//--------------------------------------------------------------------------
	// MAKE_WINDOW_BODY
	//--------------------------------------------------------------------------
#	define MAKE_WINDOW_BODY  \
		valid.resize(nb.size()); \
		VectContainer::const_iterator off; \
		int i = 0; \
		for (off = nb.offset.begin(); off != nb.offset.end(); ++off) \
		{ \
			valid[i++] = img.GetVoxelAddr(x + off->x, y + off->y, z + off->z); \
		}

	//--------------------------------------------------------------------------
	// GET_WINDOW_BODY
	//--------------------------------------------------------------------------
#	define GET_WINDOW_BODY \
		Neighbourhood rnb; \
		size_t tmp = GetNbh(img, x, y, z, nb, rnb); \
		MakeWindow(img, x, y, z, rnb, valid); \
		return tmp;

	//--------------------------------------------------------------------------
	// GET_FULL_WINDOW_BODY
	//--------------------------------------------------------------------------
#	define GET_FULL_WINDOW_BODY \
		std::vector<bool> is_valid; \
		size_t tmp = GetNbh(img, x, y, z, nb, is_valid); \
		/* and now make full window */ \
		valid.resize(nb.size()); \
		VectContainer::const_iterator off; \
		size_t i; \
		for (off = nb.offset.begin(), i = 0; off != nb.offset.end(); ++off, ++i) \
		{ \
			valid[i] = is_valid[i] ? img.GetVoxelAddr(x + off->x, y + off->y, z + off->z) : NULL; \
		} \
		return tmp; 

	/***************************************************************************\
	*
	*                               Local constants
	*
	\***************************************************************************/
	//--------------------------------------------------------------------------
	// cube
	//--------------------------------------------------------------------------
	const int cube[] = 
	{  0, 0, 0,   1, 0, 0,   0, 1, 0,   0, 0, 1,  -1, 0, 0,   0,-1, 0,   0, 0,-1,
	   1, 1, 0,   0, 1, 1,   1, 0, 1,  -1, 1, 0,   0,-1, 1,   1, 0,-1,   1,-1, 0,
	   0, 1,-1,  -1, 0, 1,  -1,-1, 0,   0,-1,-1,  -1, 0,-1,   1, 1, 1,  -1, 1, 1,
	   1,-1, 1,   1, 1,-1,  -1,-1,-1,   1,-1,-1,  -1, 1,-1,  -1,-1, 1
	}; // cube

    //--------------------------------------------------------------------------
	// cube2d
	//--------------------------------------------------------------------------
	const int cube2d[] = 
	{  0, 0, 0,   1, 0, 0,   0, 1, 0,  -1, 0, 0,  0,-1, 0,
	   1, 1, 0,  -1, 1, 0,   1,-1, 0,  -1,-1, 0
	}; // cube2d

	/***************************************************************************\
	*
	*                               Local functions
	*
	\***************************************************************************/
	//--------------------------------------------------------------------------
	// in_range
	//--------------------------------------------------------------------------
	/** Function in_range test if 0 <= coord + offset < max.
    coord can be how_long times incremented to stay in range
    if offset == 0 then how_long is unchanged
    if coord + offset is not in range, how_long tells after how many incremets
    it will be in range

    precondition 0 <= coord < max. */
	bool in_range(size_t coord, size_t max, int offset, size_t &how_long)
	{
		bool pom = false;

		if (offset == 0) 
			return true;
		
		if (offset < 0 ) 
		{
			size_t off = -offset;
			if (coord >= off) 
			{      
				how_long = max - coord - 1;
				pom = true;
			} 
			else 
			{
				how_long = off - coord - 1;
			}
		} 
		else 
		{
			if (coord + offset < max) 
			{
				how_long = max - offset - coord - 1;
				pom = true;
			} 
			else 
			{
				how_long = max - coord - 1;
			}
		}
		return pom;
	} // in_range

	/***************************************************************************\
	*
	*                               Neighbourhood
	*
	\***************************************************************************/
	//--------------------------------------------------------------------------
	// Constructors and destructors
	//--------------------------------------------------------------------------
    Neighbourhood::Neighbourhood(const int offset_[], int n_) 
		: offset()
    {
		for (int i = 0; i < n_; i++)
		{
			offset.push_back(Vector3d<int>(offset_[3 * i], offset_[3 * i + 1], offset_[3 * i + 2]));
		}
    }

	Neighbourhood::Neighbourhood(Vector3d<int> v, size_t p)
	{ 
		int vx=0, vy=0, vz=0;
		for (size_t i=0; i<p; ++i)
		{ 
			offset.push_back(Vector3d<int>(vx,vy,vz));
			vx += v.x;
			vy += v.y;
			vz += v.z;
		}
	}
    
	Neighbourhood::Neighbourhood(double r1, double r2, double r3)
	{
		r1 = fabs(r1); r2 = fabs(r2); r3 = fabs(r3);
		for (int x = -int(floor(r1)); x <= int(floor(r1)); ++x) 
		{
			for (int y = -int(floor(r2)); y <= int(floor(r2)); ++y) 
			{
				for (int z = -int(floor(r3)); z <= int(floor(r3)); ++z) 
				{
					if (SQR(x/r1) + SQR(y/r2) + SQR(z/r3) - 1 <= 0.0) 
					{
						offset.push_back(Vector3d<int>(x, y, z));
					}
				}
			}
		}
	} // Neighbourhood constructors and destructors

	//--------------------------------------------------------------------------
	// Comparison operator
	//--------------------------------------------------------------------------
	bool Neighbourhood::operator==(const Neighbourhood &N) const
	{
		VectContainer v1(offset), v2(N.offset);
		std::sort(v1.begin(), v1.end());
		std::sort(v2.begin(), v2.end());
		return std::equal(v1.begin(), v1.end(), v2.begin());
	} // Comparison operator

	//--------------------------------------------------------------------------
	// Union operator
	//--------------------------------------------------------------------------
	const Neighbourhood Neighbourhood::operator+(const Neighbourhood &N) const
	{ 
		Neighbourhood Pom;
		VectContainer v1(offset), v2(N.offset);
		std::sort(v1.begin(), v1.end());
		std::sort(v2.begin(), v2.end());
        std::set_union(v1.begin(), v1.end(), v2.begin(), v2.end(), std::back_inserter(Pom.offset));
		return Pom;
	} // Union operator

	//--------------------------------------------------------------------------
	// Difference operator
	//--------------------------------------------------------------------------
	const Neighbourhood Neighbourhood::operator-(const Neighbourhood &N) const
	{
		Neighbourhood Pom;
		VectContainer v1(offset), v2(N.offset);
		std::sort(v1.begin(), v1.end());
		std::sort(v2.begin(), v2.end());
		std::set_difference(v1.begin(), v1.end(), v2.begin(), v2.end(), std::back_inserter(Pom.offset));
		return Pom;
	} // Difference operator

	//--------------------------------------------------------------------------
	// Inversion operator
	//--------------------------------------------------------------------------
	Neighbourhood Neighbourhood::operator-() const
	{
		Neighbourhood Pom;
		for (VectContainer::const_iterator i = offset.begin(); i != offset.end(); ++i)
		{
			Pom.offset.push_back(-(*i));
		}
		
		return Pom; 
	} // Inversion operator  

	/***************************************************************************\
	*
	*                         External neighbourhood constants
	*
	\***************************************************************************/
	//--------------------------------------------------------------------------
	// nb2D_4
	//--------------------------------------------------------------------------
	extern const Neighbourhood nb2D_4(&(cube2d[3]), 4);

    //--------------------------------------------------------------------------
	// nb2D_o4
	//--------------------------------------------------------------------------
	extern const Neighbourhood nb2D_o4(cube2d, 5);
    
	//--------------------------------------------------------------------------
	// nb2D_8
	//--------------------------------------------------------------------------
	extern const Neighbourhood nb2D_8(&(cube2d[3]), 8);

	//--------------------------------------------------------------------------
	// nb2D_o8
	//--------------------------------------------------------------------------
	extern const Neighbourhood nb2D_o8(cube2d, 9);

	//--------------------------------------------------------------------------
	// nb3D_6
	//--------------------------------------------------------------------------
	extern const Neighbourhood nb3D_6(&(cube[3]), 6);

	//--------------------------------------------------------------------------
	// nb3D_o6
	//--------------------------------------------------------------------------
	extern const Neighbourhood nb3D_o6(cube, 7);

	//--------------------------------------------------------------------------
	// nb3D_18
	//--------------------------------------------------------------------------
	extern const Neighbourhood nb3D_18(&(cube[3]), 18);

	//--------------------------------------------------------------------------
	// nb3D_o18
	//--------------------------------------------------------------------------
	extern const Neighbourhood nb3D_o18(cube, 19);

	//--------------------------------------------------------------------------
	// nb3D_26
	//--------------------------------------------------------------------------
	extern const Neighbourhood nb3D_26(&(cube[3]), 26);

	//--------------------------------------------------------------------------
	// nb3D_o26
	//--------------------------------------------------------------------------
	extern const Neighbourhood nb3D_o26(cube, 27);

	/***************************************************************************\
	*
	*                         Neigbourhood related operators
	*
	\***************************************************************************/
	//--------------------------------------------------------------------------
	// Output operator
	//--------------------------------------------------------------------------
	std::ostream &operator<<(std::ostream &out, const Neighbourhood &n)
	{
		if (n.offset.empty()) 
		{
			out << "Empty" << std::endl;
			return out;
		}
		
		out << "size: " << n.size() << std::endl;
		for (size_t k = 0; k < n.size(); ++k)
		{
			out << n.offset[k] << " ";
		}
		out << std::endl;

		int max_x = n.offset.begin() -> x;
		int max_y = n.offset.begin() -> y;
		int max_z = n.offset.begin() -> z;
		int min_x = n.offset.begin() -> x;
		int min_y = n.offset.begin() -> y;
		int min_z = n.offset.begin() -> z;

		typedef VectContainer::const_iterator IT;
		for (IT i = n.offset.begin(); i != n.offset.end(); ++i) 
		{
			get_max(max_x, i->x);
			get_max(max_y, i->y);
			get_max(max_z, i->z);
			get_min(min_x, i->x);
			get_min(min_y, i->y);
			get_min(min_z, i->z);
		}

		for (int z = min_z; z <= max_z; ++z) 
		{
			for (int y = min_y; y <= max_y; ++y) 
			{
				for (int x = min_x; x <= max_x; ++x) 
				{
					/// if (n.offset.find(Vector3d<int>(x, y, z)) == n.offset.end()) {
					if (find(n.offset.begin(), n.offset.end(), Vector3d<int>(x, y, z)) == n.offset.end()) 
					{
						if (x == 0 && y == 0 && z == 0) 
						{
							out << "0";
						}
						else
						{
							out << ".";
						}
					} 
					else
					{
						if (x == 0 && y == 0 && z == 0) 
						{
							out << "#";
						}
						else
						{
							out << "X";
						}
					}
				}
				out << std::endl;
			}
			out << std::endl;
		}
		out << std::endl;
		return out;
	} // Output operator

	/***************************************************************************\
	*
	*                         Neigbourhood related functions
	*
	\***************************************************************************/
	//--------------------------------------------------------------------------
	// ConstructBoxNeighbourhood
	//--------------------------------------------------------------------------
	void ConstructBoxNeighbourhood(Neighbourhood &nb, double r1, double r2, double r3)
	{
		nb.offset.clear();
		r1 = fabs(r1); r2 = fabs(r2); r3 = fabs(r3);
		for (int x = -int(floor(r1)); x <= int(floor(r1)); ++x) 
		{
			for (int y = -int(floor(r2)); y <= int(floor(r2)); ++y) 
			{
				for (int z = -int(floor(r3)); z <= int(floor(r3)); ++z) 
				{
					if (((x != 0) || (y!=0) || (z != 0))) 
					{
						nb.offset.push_back(Vector3d<int>(x, y, z));
					}
				}
			}
		}
	} //ConstructBoxNeighbourhood

	/***************************************************************************\
	*
	*							  Other functions
	*
	\***************************************************************************/
	//--------------------------------------------------------------------------
	// GetNbh
	//--------------------------------------------------------------------------
	template <class T> size_t GetNbh(
		const Image3d<T> &img, 
		size_t x, 
		size_t y, 
		size_t z,
        const Neighbourhood &nb,
		Neighbourhood &rnb)
	{
		std::vector<bool> valid;
		size_t ret;

		rnb.offset.clear();
		ret = GetNbh(img, x, y, z, nb, valid);

		size_t i;
		VectContainer::const_iterator off;
		for (off = nb.offset.begin(), i = 0; off != nb.offset.end(); ++off, ++i) 
		{
			if (valid[i]) 
			{
				rnb.offset.push_back(Vector3d<int>(off->x,off->y,off->z));
			}
		}
		return ret;
	}

	template <class T> size_t GetNbh(
		const Image3d<T> &img,
		size_t x, 
		size_t y, 
		size_t z, 
		const Neighbourhood &nb,
		std::vector<bool> &valid)
	{
		valid.resize(nb.size());

		size_t tmp1, tmp2, tmp3;
		const size_t pos = (z * img.GetSizeY() + y) * img.GetSizeX() + x;
		const size_t remain = img.GetImageSize() - pos - 1;
		const size_t remain_x = img.GetSizeX() - x - 1;
		const size_t remain_y = img.GetSizeY() - y - 1;
		const size_t remain_z = img.GetSizeZ() - z - 1;
		size_t how_long = remain;
		bool all_valid;

		//rnb.offset.clear();
		size_t i;
		VectContainer::const_iterator off;
		for (off = nb.offset.begin(), i = 0; off != nb.offset.end(); ++off, ++i) 
		{
			/* set temporaries for case offset.? == 0, in_range() don't change it 
			if all members of offset are in_range() we want tmp? == remain at the 
			end in case offset.? == 0 */ 
			tmp1 = remain;
			tmp2 = remain_z * img.GetSizeY() + remain_y;
			tmp3 = remain_z;

			all_valid = in_range(x, img.GetSizeX(), off->x, tmp1);
			all_valid = in_range(y, img.GetSizeY(), off->y, tmp2) && all_valid;
			all_valid = in_range(z, img.GetSizeZ(), off->z, tmp3) && all_valid;

			tmp2 = tmp2 * img.GetSizeX() + remain_x;
			tmp3 = (tmp3 * img.GetSizeY() + remain_y) * img.GetSizeX() + remain_x;

			valid[i] = all_valid;
			//if (all_valid)
			//{
				//rnb.offset.push_back(Vector3d<int>(off->x,off->y,off->z));
			//}

			how_long = std::min(std::min(how_long, tmp1), std::min(tmp2, tmp3));
		}
		return how_long + 1;
	} // GetNbh

    //--------------------------------------------------------------------------
	// Explicit instantiations
	//--------------------------------------------------------------------------
	template I3D_DLLEXPORT size_t GetNbh(
		const Image3d<BINARY> &img, 
		size_t x, 
		size_t y, 
		size_t z, 
		const Neighbourhood &nb,
		Neighbourhood &rnb);

	template I3D_DLLEXPORT size_t GetNbh(
		const Image3d<GRAY8> &img, 
		size_t x, 
		size_t y, 
		size_t z, 
		const Neighbourhood &nb,
		Neighbourhood &rnb);

	template I3D_DLLEXPORT size_t GetNbh(
		const Image3d<size_t> &img, 
		size_t x, 
		size_t y, 
		size_t z, 
		const Neighbourhood &nb,
		Neighbourhood &rnb);

	//--------------------------------------------------------------------------
	// MakeWindow
	//--------------------------------------------------------------------------
	template <class T> void MakeWindow(
		Image3d<T> &img,
		size_t x, 
		size_t y, 
		size_t z, 
		const Neighbourhood &nb,
		std::vector<T *> &valid)
	{
		MAKE_WINDOW_BODY
	}
	
	template <class T> void MakeWindow(
		const Image3d<T> &img,
		size_t x, 
		size_t y, 
		size_t z, 
		const Neighbourhood &nb,
		std::vector<const T *> &valid)
	{
		MAKE_WINDOW_BODY
	}

	//--------------------------------------------------------------------------
	// Explicit instantiations
	//--------------------------------------------------------------------------
	template I3D_DLLEXPORT void MakeWindow(
		Image3d<BINARY> &img,
		size_t x, 
		size_t y, 
		size_t z,
		const Neighbourhood &nb,
		std::vector<BINARY *> &valid);

	template I3D_DLLEXPORT void MakeWindow(
		Image3d<GRAY8> &img,
		size_t x, 
		size_t y, 
		size_t z, 
		const Neighbourhood &nb,
		std::vector<GRAY8 *> &valid);

	template I3D_DLLEXPORT void MakeWindow(
		Image3d<GRAY16> &img,
		size_t x, 
		size_t y, 
		size_t z, 
		const Neighbourhood &nb,
		std::vector<GRAY16 *> &valid);

	template I3D_DLLEXPORT void MakeWindow(
		Image3d<size_t> &img,
		size_t x, 
		size_t y, 
		size_t z, 
		const Neighbourhood &nb,
		std::vector<size_t *> &valid);

	//--------------------------------------------------------------------------
	// GetWindow
	//--------------------------------------------------------------------------
	template <class T> size_t GetWindow(
		Image3d<T> &img, 
		size_t x, 
		size_t y, 
		size_t z, 
		const Neighbourhood &nb, 
		std::vector<T *> &valid)
	{
        
		GET_WINDOW_BODY
	}

	template <class T> size_t GetWindow(
		const Image3d<T> &img, 
		size_t x, 
		size_t y, 
		size_t z, 
		const Neighbourhood &nb, 
		std::vector<const T *> &valid)
	{
		GET_WINDOW_BODY
	}
    
	//--------------------------------------------------------------------------
	// Explicit instantiations
	//--------------------------------------------------------------------------
	template I3D_DLLEXPORT size_t GetWindow(
		Image3d<BINARY> &img, 
		size_t x, 
		size_t y, 
		size_t z, 
		const Neighbourhood &nb, 
		std::vector<BINARY *> &valid);

	template I3D_DLLEXPORT size_t GetWindow(
		Image3d<GRAY8> &img,
        size_t x, 
		size_t y, 
		size_t z, 
		const Neighbourhood &nb, 
		std::vector<GRAY8 *> &valid);

	template I3D_DLLEXPORT size_t GetWindow(
		Image3d<RGB> &img, 
		size_t x, 
		size_t y, 
		size_t z, 
		const Neighbourhood &nb, 
		std::vector<RGB *> &valid);

	template I3D_DLLEXPORT size_t GetWindow(
		Image3d<RGB16> &img, 
		size_t x, 
		size_t y, 
		size_t z, 
		const Neighbourhood &nb, 
		std::vector<RGB16 *> &valid);

	template I3D_DLLEXPORT size_t GetWindow(
		Image3d<GRAY16> &img, 
		size_t x, 
		size_t y, 
		size_t z, 
		const Neighbourhood &nb, 
		std::vector<GRAY16 *> &valid);
 
	template I3D_DLLEXPORT size_t GetWindow(
		Image3d<size_t> &img, 
		size_t x, 
		size_t y, 
		size_t z, 
		const Neighbourhood &nb, 
		std::vector<size_t *> &valid);

	template I3D_DLLEXPORT size_t GetWindow(
		Image3d<int> &img, 
		size_t x, 
		size_t y, 
		size_t z, 
		const Neighbourhood &nb, 
		std::vector<int *> &valid);
    
	template I3D_DLLEXPORT size_t GetWindow(
		Image3d<float> &img, 
		size_t x, 
		size_t y, 
		size_t z, 
		const Neighbourhood &nb, 
		std::vector<float *> &valid);

	/*template I3D_DLLEXPORT size_t GetWindow(
		Image3d< Vector3d<int> > &img,
		size_t x, 
		size_t y, 
		size_t z, 
		const Neighbourhood &nb, 
		std::vector< Vector3d<int> *> &valid);*/

	template I3D_DLLEXPORT size_t GetWindow(
		const Image3d<BINARY> &img,
        size_t x, 
		size_t y, 
		size_t z, 
		const Neighbourhood &nb, 
		std::vector<const BINARY *> &valid);

	template I3D_DLLEXPORT size_t GetWindow(
		const Image3d<GRAY8> &img,
        size_t x, 
		size_t y, 
		size_t z, 
		const Neighbourhood &nb, 
		std::vector<const GRAY8 *> &valid);

	template I3D_DLLEXPORT size_t GetWindow(
		const Image3d<RGB> &img,
		size_t x, 
		size_t y, 
		size_t z, 
		const Neighbourhood &nb, 
		std::vector<const RGB *> &valid);

	template I3D_DLLEXPORT size_t GetWindow(
		const Image3d<RGB16> &img, 
		size_t x, 
		size_t y, 
		size_t z, 
		const Neighbourhood &nb, 
		std::vector<const RGB16 *> &valid);

	template I3D_DLLEXPORT size_t GetWindow(
		const Image3d<GRAY16> &img, 
		size_t x, 
		size_t y, 
		size_t z,
        const Neighbourhood &nb, 
		std::vector<const GRAY16 *> &valid);

	template I3D_DLLEXPORT size_t GetWindow(
		const Image3d<size_t> &img,
		size_t x, 
		size_t y, 
		size_t z, 
		const Neighbourhood &nb, 
		std::vector<const size_t *> &valid);

	template I3D_DLLEXPORT size_t GetWindow(
		const Image3d<int> &img, 
		size_t x, 
		size_t y, 
		size_t z, 
		const Neighbourhood &nb, 
		std::vector<const int *> &valid);

	template I3D_DLLEXPORT size_t GetWindow(
		const Image3d<float> &img,
        size_t x, 
		size_t y, 
		size_t z, 
		const Neighbourhood &nb, 
		std::vector<const float *> &valid);

	/*template I3D_DLLEXPORT size_t GetWindow(
		const Image3d< Vector3d<int> > &img, 
		size_t x, 
		size_t y, 
		size_t z, 
		const Neighbourhood &nb, 
		std::vector<const Vector3d<int> *> &valid);*/

	//--------------------------------------------------------------------------
	// GetFullWindow
	//--------------------------------------------------------------------------
	template <class T> size_t GetFullWindow(
		Image3d<T> &img,
		size_t x, 
		size_t y, 
		size_t z, 
		const Neighbourhood &nb,
		std::vector<T *> &valid)
	{
		GET_FULL_WINDOW_BODY
	}
    
	template <class T> size_t GetFullWindow(
		const Image3d<T> &img,
		size_t x, 
		size_t y, 
		size_t z, 
		const Neighbourhood &nb,
		std::vector<const T *> &valid)
	{
		GET_FULL_WINDOW_BODY
	}

	//--------------------------------------------------------------------------
	// Explicit instantiations
	//--------------------------------------------------------------------------
	template I3D_DLLEXPORT size_t GetFullWindow(
		Image3d<BINARY> &img, 
		size_t x, 
		size_t y, 
		size_t z, 
		const Neighbourhood &nb, 
		std::vector<BINARY *> &valid);

	template I3D_DLLEXPORT size_t GetFullWindow(
		Image3d<GRAY8> &img, 
		size_t x, 
		size_t y, 
		size_t z, 
		const Neighbourhood &nb, 
		std::vector<GRAY8 *> &valid);

	template I3D_DLLEXPORT size_t GetFullWindow(
		Image3d<size_t> &img, 
		size_t x, 
		size_t y, 
		size_t z, 
		const Neighbourhood &nb, 
		std::vector<size_t *> &valid);

	template I3D_DLLEXPORT size_t GetFullWindow(
		const Image3d<BINARY> &img, 
		size_t x, 
		size_t y, 
		size_t z, 
		const Neighbourhood &nb, 
		std::vector<const BINARY *> &valid);

    
	template I3D_DLLEXPORT size_t GetFullWindow(
		const Image3d<GRAY8> &img, 
		size_t x, 
		size_t y, 
		size_t z,
        const Neighbourhood &nb, 
		std::vector<const GRAY8 *> &valid);
	
	template I3D_DLLEXPORT size_t GetFullWindow(
		const Image3d<size_t> &img, 
		size_t x, 
		size_t y, 
		size_t z, 
		const Neighbourhood &nb, 
		std::vector<const size_t *> &valid);

	template I3D_DLLEXPORT size_t GetFullWindow(
		const Image3d<unsigned short> &img, 
		size_t x, 
		size_t y, 
		size_t z, 
		const Neighbourhood &nb, 
		std::vector<const unsigned short *> &valid);

	//--------------------------------------------------------------------------
	// GetMedian
	//--------------------------------------------------------------------------
	template <class T> T GetMedian(const std::vector<const T *> &win)
	{
		std::vector<T> in;
		in.resize(win.size());
		for (size_t i = 0; i < win.size(); i++)
		{
			in[i] = *win[i];
		}
		std::sort(in.begin(),in.end());
		return in[in.size()/2];
	}

	//--------------------------------------------------------------------------
	// Explicit instantiations
	//--------------------------------------------------------------------------
	template I3D_DLLEXPORT GRAY8 GetMedian(const std::vector<const GRAY8 *> &win);

	template I3D_DLLEXPORT GRAY16 GetMedian(const std::vector<const GRAY16 *> &win);

	template I3D_DLLEXPORT float GetMedian(const std::vector<const float *> &win);

	/***************************************************************************\
	*
	*                         Neigbourhood related classes
	*
	\***************************************************************************/
	//--------------------------------------------------------------------------
	// Constructors
	//--------------------------------------------------------------------------
	template <typename VOXEL> 
	NeighbourhoodWalkerU<VOXEL>::NeighbourhoodWalkerU(const Image3d<VOXEL> &image, const Neighbourhood &neigbourhood)
	{
		I3DASSERT_ALWAYS_ET(neigbourhood.offset.size(), "Neighbourhood to be used for walking should have at least one element.");
		for (size_t i = 0; i < neigbourhood.offset.size(); ++i)
			offsets.push_back(neigbourhood.offset[i].x + neigbourhood.offset[i].y * image.GetWidth() + neigbourhood.offset[i].z * image.GetWidth() * image.GetHeight());
		offsetsIndex = offsets.size();
		offsetsSize = offsetsIndex;
	}

	//--------------------------------------------------------------------------
	// Begin
	//--------------------------------------------------------------------------
	template <typename VOXEL> 
	void NeighbourhoodWalkerU<VOXEL>::Begin(size_t index)
	{
		this->index = index;
		offsetsIndex = std::numeric_limits<size_t>::max();
		Next();
	}

	//--------------------------------------------------------------------------
	// Explicit instantiations
	//--------------------------------------------------------------------------
	template class I3D_DLLEXPORT NeighbourhoodWalkerU<int>;
	template class I3D_DLLEXPORT NeighbourhoodWalkerU<size_t>;
	template class I3D_DLLEXPORT NeighbourhoodWalkerU<GRAY8>;
	template class I3D_DLLEXPORT NeighbourhoodWalkerU<GRAY16>;
	template class I3D_DLLEXPORT NeighbourhoodWalkerU<BINARY>;

} // i3d namespace
