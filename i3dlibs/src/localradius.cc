/*
 * i3dlib - image manipulation library
 *
 * Copyright (C) 2000-2006   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 * FILE : localradius.cc
 *
 * Pavel Matula <pam@fi.muni.cz> 2009
 *
 */

#ifdef __GNUG__
#pragma implementation "localradius.h"
#endif

#include "image3d.h"
#include "vector3d.h"
#include "localradius.h"
			       
using namespace std;

namespace i3d 
{

	template <class LABEL, class VOXELOUT> I3D_DLLEXPORT void 
		LocalRadiusFlat(const i3d::LabeledImage3d<LABEL, i3d::BINARY> &ImgIn, Image3d<VOXELOUT> & ImgOut, const i3d::Vector3d<size_t> &centroid)
{
	ImgOut.CopyMetaData(ImgIn);
	ImgOut.SetAllVoxels(std::numeric_limits<VOXELOUT>::max());

	if (!ImgIn.Include(centroid) || ImgIn.GetVoxel(centroid) == 0) {
		// radius will be equal std::numeric_limits<VOXELOUT>::max() for all foreground voxels
		return;
	}

	LABEL label = ImgIn.GetVoxel(centroid);

	size_t zlim1, zlim2; // z limits 

	zlim1 = centroid.z;
	while (zlim1 >= 0 && ImgIn.GetVoxel(centroid.x, centroid.y, zlim1) == label) {
		zlim1--;
	}

	zlim2 = centroid.z;
	while (zlim2 < ImgIn.GetSizeZ() && ImgIn.GetVoxel(centroid.x, centroid.y, zlim2) == label) {
		zlim2++;
	}

	for (size_t z = 0; z < zlim1 + 1; ++z) {
		for (size_t y = 0; y < ImgIn.GetSizeY(); y++) {
			for (size_t x = 0; x < ImgIn.GetSizeX(); x++) {
				if (ImgIn.GetVoxel(x, y, z) == label)
					ImgOut.SetVoxel(x, y, z, 100.0);
			}
		}	
	}


	i3d::Image3d <i3d::BINARY> slice;
	slice.SetResolution(ImgIn.GetResolution());
	slice.MakeRoom(ImgIn.GetSizeX(), ImgIn.GetSizeY(), 1);

	i3d::SeedPointsVector spv;
	spv.push_back(i3d::Vector3d<size_t>(centroid.x, centroid.y, 0));

	i3d::Image3d <VOXELOUT> lr;
	lr.CopyMetaData(slice);

	VOXELOUT zrad = VOXELOUT(zlim2 - zlim1)/VOXELOUT(2);

	for (size_t z = zlim1 + 1; z < zlim2; ++z) {
		// copy one slice in binary image
		for (size_t y = 0; y < ImgIn.GetSizeY(); y++) {
			for (size_t x = 0; x < ImgIn.GetSizeX(); x++) {
				slice.SetVoxel(x, y, 0, (ImgIn.GetVoxel(x, y, z) == label) ? i3d::BINARY(1) : i3d::BINARY(0));
			}
		}	
		i3d::LocalRadius(slice, lr, spv);

		// copy back (appropriately weigthed) the local radius computed at this slice 
		for (size_t y = 0; y < ImgIn.GetSizeY(); y++) {
			for (size_t x = 0; x < ImgIn.GetSizeX(); x++) {
				if (lr.GetVoxel(x, y, 0) < std::numeric_limits<VOXELOUT>::max()) {
					VOXELOUT lradnorm2 = lr.GetVoxel(x, y, 0)/100.0;
					lradnorm2 *= lradnorm2; // square of 2D normalized (0..1) local radius

					VOXELOUT zdist2 = (VOXELOUT(centroid.z) - z)/zrad;
					zdist2 *= zdist2; // square of normalized distance from central slice (0..1)

					ImgOut.SetVoxel(x, y, z, sqrt(lradnorm2 - lradnorm2 * zdist2 + zdist2) * 100.0);
				} else {
					ImgOut.SetVoxel(x, y, z, std::numeric_limits<VOXELOUT>::max());
				}
			}
		}	
						
	}

	for (size_t z = zlim2; z < ImgIn.GetSizeZ(); ++z) {
		for (size_t y = 0; y < ImgIn.GetSizeY(); y++) {
			for (size_t x = 0; x < ImgIn.GetSizeX(); x++) {
				if (ImgIn.GetVoxel(x, y, z) == label)
					ImgOut.SetVoxel(x, y, z, 100.0);
			}
		}	
	}

}

/****************************************************************************************************/

	template <class LABEL, class VOXELOUT> I3D_DLLEXPORT void LocalRadiusFlat(
		const i3d::LabeledImage3d<LABEL, i3d::BINARY> &ImgIn, 
		Image3d<VOXELOUT> & ImgOut, 
		const i3d::SeedPointsVector &seeds)
	{
		ImgOut.CopyMetaData(ImgIn);
		ImgOut.SetAllVoxels(std::numeric_limits<VOXELOUT>::max());

		Image3d<VOXELOUT> ImgTmp;

		for (size_t i = 0; i < seeds.size(); i++) {
			LocalRadiusFlat(ImgIn, ImgTmp, seeds[i]);
			for (size_t j = 0; j < ImgIn.GetImageSize(); ++j) {
				ImgOut.SetVoxel(j, std::min(ImgOut.GetVoxel(j), ImgTmp.GetVoxel(j)));
			}
		}
	}

/****************************************************************************************************/


/* Explicit instantiations */
template I3D_DLLEXPORT void LocalRadiusFlat(const i3d::LabeledImage3d<unsigned short, i3d::BINARY> &ImgIn, Image3d<float> & ImgOut, const i3d::Vector3d<size_t> &centroid);
template I3D_DLLEXPORT void LocalRadiusFlat(const i3d::LabeledImage3d<unsigned short, i3d::BINARY> &ImgIn, Image3d<float> & ImgOut, const i3d::SeedPointsVector &seeds);

}


