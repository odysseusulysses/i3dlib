/*
 * segment_nuclei.cpp
 *
 * Function for segmentation of nuclei
 *
 * Marek Vinkler (xvinkl@mail.muni.cz) 2009
 * 
 */

#include "segment_nuclei.h"
#include "regions.h"
#include "threshold.h"
#include "LevelSet.h"
#include "morphology.h"
#include "bin.h"
#include "rank_filters.h"

namespace i3d
{
	bool SegmentNuclei2d(const Image3d<i3d::GRAY16> &in, Image3d<i3d::GRAY16> &out, const SNR_Params &params)
	{
		Image3d<i3d::GRAY16> mask_img, label_gray, c1, c2;
		Image3d<i3d::BINARY> lap_bin, gm_bin, mask_bin, bin, bin_small, merged_mask;
		Image3d<float> gauss, lap_img, gm_img;
		LabeledImage3d<i3d::GRAY16, i3d::BINARY> label;
		Histogram hist;
	
		GrayToFloat(in, gauss);
		GaussIIR(gauss, (float)params.laplace_sigma);
		Laplace(gauss, lap_img); // Other i3d Laplacians are unsigned
		Threshold<float, i3d::BINARY>(lap_img, lap_bin, -std::numeric_limits<float>::infinity(), 0);

		GrayToFloat(in, gauss);
		GaussIIR(gauss, (float)params.laplace_sigma);
		EstimateGradient2_2D(gauss, gm_img, false);

		switch(params.grad_thresh_type)
		{
		case Background:
			IntensityHist(gm_img, hist);
			Threshold<float, i3d::BINARY>(gm_img, gm_bin, FindUnimodalThreshold(hist));
			break;

		case Median:
			gm_bin.CopyMetaData(gm_img);
			float median = gm_img.GetKPercentileValue(50.0f);
			Threshold<float, i3d::BINARY>(gm_img, gm_bin, median);
			break;

		// TODO later
		//case Border:
		//	/*border = dilation(lap_img,2)-erosion(lap_img, 2);
		//	border_lbl = label(border,2);
		//	border_msr = measure(border_lbl, gradmag(img, params.gradmag_sigma), 'Mean');
		//	gm_img = msr2obj(border_lbl, border_msr);
		//	gm_img = gm_img > params.min_avg_gradmag;*/
		//	Image3d<i3d::BINARY> dilation, erosion, border;

		//	Dilation(lap_bin, dilation, 2);
		//	Erosion(lap_bin, erosion, 2);
		//	for(size_t i = 0; i < dilation.GetSizeX()*dilation.GetSizeY(); i++)
		//	{
		//		border.SetVoxel(i, (i3d::BINARY)(dilation.GetVoxel(i) > erosion.GetVoxel(i)));
		//	}
		//	label.CreateRegions(border, nb2D_o8);
		//	break;
		}

		if(params.remove_faint_objects)
		{
			IntensityHist(in, hist);
			Threshold<i3d::GRAY16, i3d::BINARY>(in, mask_bin, FindUnimodalThreshold(hist));
			i3d::BINARY *lapB = lap_bin.GetFirstVoxelAddr();
			i3d::BINARY *gmB = gm_bin.GetFirstVoxelAddr();
			i3d::BINARY *maskB = mask_bin.GetFirstVoxelAddr();
			for(size_t i = 0; i < lap_bin.GetSizeX()*lap_bin.GetSizeY(); i++)
			{
				*maskB = (i3d::BINARY)(*lapB & *gmB & *maskB);
				lapB++;
				gmB++;
				maskB++;
			}
		}
		else
		{
			mask_bin.CopyMetaData(lap_bin);
			i3d::BINARY *lapB = lap_bin.GetFirstVoxelAddr();
			i3d::BINARY *gmB = gm_bin.GetFirstVoxelAddr();
			i3d::BINARY *maskB = mask_bin.GetFirstVoxelAddr();
			for(size_t i = 0; i < lap_bin.GetSizeX()*lap_bin.GetSizeY(); i++)
			{
				*maskB = (i3d::BINARY)(*lapB & *gmB);
				lapB++;
				gmB++;
				maskB++;
			}
		}

		label.CreateForegroundRegionsFF(mask_bin, nb2D_o8);

		// Delete components with volume < params.min_fragment_size
		Remove(label.components, std::binder2nd<component_volume_less<i3d::BINARY> >
			(component_volume_less<i3d::BINARY>(), params.min_fragment_size));

		label.Convert(label_gray);
		Threshold<i3d::GRAY16, i3d::BINARY>(label_gray, mask_bin, 1);

		// The neighbourhood variant doesn't work
		Reconstruction_by_dilation(mask_bin, lap_bin, bin);
		
		label.CreateForegroundRegionsFF(bin, nb2D_o8);

		// Delete components with volume < params.min_fragment_size
		Remove(label.components, std::binder2nd<component_volume_less<i3d::BINARY> >
			(component_volume_less<i3d::BINARY>(), params.min_fragment_size));

		// COMMENT: Ignore params.closing_size for now
		label.Convert(label_gray);
		Closing(label_gray, c1, nb2D_o8);
		label.components.begin()->second.voxel_value = label.get_max_label()+1;
		label.Convert(label_gray);
		Opening(label_gray, c2, nb2D_o8);

		i3d::GRAY16 *labelG = label_gray.GetFirstVoxelAddr();
		i3d::GRAY16 *c1G = c1.GetFirstVoxelAddr();
		i3d::GRAY16 *c2G = c2.GetFirstVoxelAddr();
		for(size_t i = 0; i < label_gray.GetSizeX()*label_gray.GetSizeY(); i++)
		{
			if(*c1G == *c2G)
				*labelG = *c1G;
			else
				*labelG = 0;

			labelG++;
			c1G++;
			c2G++;
		}

		Threshold<i3d::GRAY16, i3d::BINARY>(label_gray, bin, 1);
		label.CreateForegroundRegionsFF(bin, nb2D_o8);
		Remove(label.components, component_touch_border<i3d::BINARY>());
		label.Convert(label_gray);
		Threshold<i3d::GRAY16, i3d::BINARY>(label_gray, bin, 1);
		BinFillHoles(bin, 0, 0, 0, nb2D_o8); // TODO: Find image background pixel

		if(params.merge_small_objects)
		{
			label.CreateForegroundRegionsFF(bin, nb2D_o8);
			// Delete components with volume > params.max_fragment_size
			Remove(label.components, std::binder2nd<component_volume_greater<i3d::BINARY> >
				(component_volume_greater<i3d::BINARY>(), params.max_fragment_size));
			label.Convert(label_gray);
			Threshold<i3d::GRAY16, i3d::BINARY>(label_gray, bin_small, 1);
			Closing(bin_small, merged_mask, params.closing_size);

			i3d::BINARY *binB = bin.GetFirstVoxelAddr();
			i3d::BINARY *mergedB = merged_mask.GetFirstVoxelAddr();
			for(size_t i = 0; i < bin.GetSizeX()*bin.GetSizeY(); i++)
			{
				*binB = (i3d::BINARY)(*binB | *mergedB);
				binB++;
				mergedB++;
			}
		}

		label.CreateForegroundRegionsFF(bin, nb2D_o8);
		// Delete components with volume < params.min_cell_size
		Remove(label.components, std::binder2nd<component_volume_less<i3d::BINARY> >
			(component_volume_less<i3d::BINARY>(), params.min_cell_size));

		label.Convert(out);

		return true;
	}
}