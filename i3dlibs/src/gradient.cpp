/*
 * gradient.cpp
 *
 * Pavel Matula (pam@fi.muni.cz) 2007
 */

#include "gradient.h"
#include "image3d.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

namespace i3d {

/*************************************************************************************/

template <class VOXEL_TYPE> void GradientMagnitude(const Image3d<VOXEL_TYPE> &img, Image3d<float> &gimg)
{
	GradientMagnitude2(img, gimg);

	float *v = gimg.GetFirstVoxelAddr();
	float *v_end = gimg.GetVoxelAddr(gimg.GetImageSize());

	while (v != v_end) {
		*v = sqrt(*v);
		v++;
	};
}

/*************************************************************************************/

template <class VOXEL_TYPE> void GradientMagnitude2(const Image3d<VOXEL_TYPE> &img, Image3d<float> &gimg)
{
	Image3d<float> pom;

	gimg.MakeRoom(img.GetSize());
	gimg.SetResolution(img.GetResolution());
	gimg.SetOffset(img.GetOffset());

	memset(gimg.GetFirstVoxelAddr(), 0, gimg.GetImageSize() * sizeof(VOXEL_TYPE));

	GradientX(img, pom);
	SquareImage(pom);
	AddImage(gimg, pom);
		
	GradientY(img, pom);
	SquareImage(pom);
	AddImage(gimg, pom);

	GradientZ(img, pom);
	SquareImage(pom);
	AddImage(gimg, pom);
}

/*************************************************************************************/

template <class VOXEL_TYPE> void Gradient(const Image3d<VOXEL_TYPE> &img, Image3d<float> &dx, Image3d<float> &dy, Image3d<float> &dz) 
{
	GradientX(img, dx);
	GradientY(img, dy);
	GradientZ(img, dz);
}

/*************************************************************************************/

template <class VOXEL_TYPE> void GradientX(const Image3d<VOXEL_TYPE> &img, Image3d<float> &dx) 
{
	dx.MakeRoom(img.GetSize());
	dx.SetResolution(img.GetResolution());
	dx.SetOffset(img.GetOffset());

	if (img.GetSizeX() > 2) {
		for (size_t z = 0; z < img.GetSizeZ(); ++z) {
			for (size_t y = 0; y < img.GetSizeY(); ++y) {
				dx.SetVoxel(0, y, z, 0.0);					//	Neumann boundary contition - the first voxel in the row

				const VOXEL_TYPE *left = img.GetVoxelAddr(0, y, z);
				float *v = dx.GetVoxelAddr(1, y, z);
				const VOXEL_TYPE *rght = img.GetVoxelAddr(2, y, z);

				for (size_t x = 1; x < img.GetSizeX() - 1; ++x) {
					*v = float(*rght) - float(*left);
					v++; left++; rght++;
				}
				dx.SetVoxel(img.GetSizeX() - 1, y, z, 0.0);	//	Neumann boundary contition  - the last voxel in the row
			}
		}
	} else {
		memset(dx.GetFirstVoxelAddr(), 0, img.GetImageSize() * sizeof(float));		//	Neumann boundary contition holds for all voxels
	}
}


/*************************************************************************************/

template <class VOXEL_TYPE> void GradientY(const Image3d<VOXEL_TYPE> &img, Image3d<float> &dy)
{
	dy.MakeRoom(img.GetSize());
	dy.SetResolution(img.GetResolution());
	dy.SetOffset(img.GetOffset());

	if (img.GetSizeY() > 2) {
		for (size_t z = 0; z < img.GetSizeZ(); ++z) {
			memset(dy.GetVoxelAddr(0, 0, z), 0, img.GetSizeX() * sizeof(float));	//	Neumann boundary contition

			for (size_t y = 1; y < img.GetSizeY() - 1; ++y) {

				const VOXEL_TYPE *left = img.GetVoxelAddr(0, y - 1, z);
				float *v = dy.GetVoxelAddr(0, y, z);
				const VOXEL_TYPE *rght = img.GetVoxelAddr(0, y + 1, z);

				for (size_t x = 0; x < img.GetSizeX(); ++x) {
					*v = float(*rght) - float(*left);
					v++; left++; rght++;
				}
			}

			memset(dy.GetVoxelAddr(0, img.GetSizeY() - 1, z), 0, img.GetSizeX() * sizeof(float));	//	Neumann boundary contition
		}
	} else {
		memset(dy.GetFirstVoxelAddr(), 0, dy.GetImageSize() * sizeof(float));		//	Neumann boundary contition holds for all voxels
	}
}

/*************************************************************************************/

template <class VOXEL_TYPE> void GradientZ(const Image3d<VOXEL_TYPE> &img, Image3d<float> &dz)
{
	dz.MakeRoom(img.GetSize());
	dz.SetResolution(img.GetResolution());
	dz.SetOffset(img.GetOffset());

	if (img.GetSizeZ() > 2) {
		memset(dz.GetVoxelAddr(0, 0, 0), 0, img.GetSizeX() * img.GetSizeY() * sizeof(float));	//	Neumann boundary contition
		for (size_t y = 0; y < img.GetSizeY(); ++y) {
			for (size_t z = 1; z < img.GetSizeZ() - 1; ++z) {
				const VOXEL_TYPE *left = img.GetVoxelAddr(0, y, z - 1);
				float *v = dz.GetVoxelAddr(0, y, z);
				const VOXEL_TYPE *rght = img.GetVoxelAddr(0, y, z + 1);

				for (size_t x = 0; x < img.GetSizeX(); ++x) {
					*v = float(*rght) - float(*left);
					v++; left++; rght++;
				}
			}
		}
		memset(dz.GetVoxelAddr(0, 0, img.GetSizeZ() - 1), 0, img.GetSizeX() * img.GetSizeY() * sizeof(float));	//	Neumann boundary contition
	} else {
		memset(dz.GetFirstVoxelAddr(), 0, dz.GetImageSize() * sizeof(float));		//	Neumann boundary contition holds for all voxels
	}
}

/*************************************************************************************/

	template <class VOXEL_TYPE> void MultiplyImage(Image3d<VOXEL_TYPE> &img, VOXEL_TYPE k)
	{
		VOXEL_TYPE *v = img.GetFirstVoxelAddr();
		VOXEL_TYPE *v_end = img.GetVoxelAddr(img.GetImageSize());

		while (v != v_end) {
			*v = *v * k; 
			v++;
		}
	}

/*************************************************************************************/

	template <class VOXEL_TYPE> void SquareImage(Image3d<VOXEL_TYPE> &img)
	{
		VOXEL_TYPE *v = img.GetFirstVoxelAddr();
		VOXEL_TYPE *v_end = img.GetVoxelAddr(img.GetImageSize());

		while (v != v_end) {
			*v = *v * *v; 
			v++;
		}
	}


/*************************************************************************************/

	template <class VOXEL_TYPE> void AddImage(Image3d<VOXEL_TYPE> &img1, const Image3d<VOXEL_TYPE> &img2)
	{
		if (img1.GetImageSize() != img2.GetImageSize()) {
			throw i3d::InternalException("AddImage: incompatible sizes.");
		}

		VOXEL_TYPE *v1 = img1.GetFirstVoxelAddr();
		const VOXEL_TYPE *v2 = img2.GetFirstVoxelAddr();
		VOXEL_TYPE *v_end = img1.GetVoxelAddr(img1.GetImageSize());

		while (v1 != v_end) {
			*v1 += *v2; 
			v1++; v2++;
		}
	}


/*************************************************************************************/

template I3D_DLLEXPORT void Gradient(const Image3d<i3d::GRAY8> &img, Image3d<float> &dx, Image3d<float> &dy, Image3d<float> &dz);
template I3D_DLLEXPORT void Gradient(const Image3d<i3d::GRAY16> &img, Image3d<float> &dx, Image3d<float> &dy, Image3d<float> &dz);
template I3D_DLLEXPORT void Gradient(const Image3d<float> &img, Image3d<float> &dx, Image3d<float> &dy, Image3d<float> &dz);

template I3D_DLLEXPORT void GradientMagnitude(const Image3d<i3d::GRAY8> &img, Image3d<float> &gimg);
template I3D_DLLEXPORT void GradientMagnitude(const Image3d<i3d::GRAY16> &img, Image3d<float> &gimg);
template I3D_DLLEXPORT void GradientMagnitude(const Image3d<float> &img, Image3d<float> &gimg);

template I3D_DLLEXPORT void GradientMagnitude2(const Image3d<i3d::GRAY8> &img, Image3d<float> &gimg);
template I3D_DLLEXPORT void GradientMagnitude2(const Image3d<i3d::GRAY16> &img, Image3d<float> &gimg);
template I3D_DLLEXPORT void GradientMagnitude2(const Image3d<float> &img, Image3d<float> &gimg);

template I3D_DLLEXPORT void GradientX(const Image3d<i3d::GRAY8> &img, Image3d<float> &dx);
template I3D_DLLEXPORT void GradientX(const Image3d<i3d::GRAY16> &img, Image3d<float> &dx);
template I3D_DLLEXPORT void GradientX(const Image3d<float> &img, Image3d<float> &dx);

template I3D_DLLEXPORT void GradientY(const Image3d<i3d::GRAY8> &img, Image3d<float> &dy);
template I3D_DLLEXPORT void GradientY(const Image3d<i3d::GRAY16> &img, Image3d<float> &dy);
template I3D_DLLEXPORT void GradientY(const Image3d<float> &img, Image3d<float> &dy);

template I3D_DLLEXPORT void GradientZ(const Image3d<i3d::GRAY8> &img, Image3d<float> &dz);
template I3D_DLLEXPORT void GradientZ(const Image3d<i3d::GRAY16> &img, Image3d<float> &dz);
template I3D_DLLEXPORT void GradientZ(const Image3d<float> &img, Image3d<float> &dz);

template I3D_DLLEXPORT void MultiplyImage(Image3d<float> &img, float k);
template I3D_DLLEXPORT void SquareImage(Image3d<float> &img);
template I3D_DLLEXPORT void AddImage(Image3d<float> &img1, const Image3d<float> &img2);


/*************************************************************************************/

} // namespace i3d
