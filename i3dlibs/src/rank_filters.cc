/*
* i3dlib - image manipulation library
*
* Copyright (C) 2000-2006   Centre for Biomedical Image Analysis (CBIA)
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Library General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Library General Public License for more details.
*
* You should have received a copy of the GNU Library General Public
* License along with this library; if not, write to the Free
* Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

/*
* FILE: rank_filters.cc
*
* Image filtering using rank filters
*/

#ifdef __GNUG__
#pragma implementation
#endif

#include "rank_filters.h"
namespace i3d 
{
	/////////////////////////////////////////////////////////////////
	//                                                             //
	//                       Local Declarations                    //
	//                                                             //
	/////////////////////////////////////////////////////////////////

	/** A moving histogram cell. */
	template <class T> struct MovingHistCell
	{
		T value;
		int position;

		/** Constructor. */
		MovingHistCell() : value(std::numeric_limits<T>::max()), position(-1) {};
	};

	/** A basic moving histogram class. */
	template <class T> class MovingHist
	{
		protected:
			/** Histogram size. */
			int m_size;
			/** Lateral radius. */
			int m_lat_radius;
			/** Axial radius. */
			int m_axi_radius;
			/** Histogram cells. */
			MovingHistCell<T> *m_cells;
			/** Positions of histogram cells. */
			int *m_positions;

		public:
			/** Constuctor. */
			MovingHist(int size, int lr, int ar) : m_size(size), m_lat_radius(lr), m_axi_radius(ar), m_cells(NULL), m_positions(NULL) { Allocate(); };
			/** Destructor. */
			virtual ~MovingHist() { Deallocate(); };
			/** Get median. */
			T GetMedian() const { return m_cells[m_size / 2].value; }
			/** Add value. */
			void Add(int position, T value);
			/** Move histogram in the horizontal direction. */
			virtual void MoveHorizontal(int direction, int position, T value) = 0;
			/** Move histogram in the vertical direction. */
			virtual void MoveVertical(int direction, int position, T value) = 0;
			/** Switch positions in the horizontal direction. */
			virtual void SwitchHorizontal(int direction) = 0;
			/** Switch positions in the vertical direction. */
			virtual void SwitchVertical(int direction) = 0;

		private:
			/** Memory allocation. */
			void Allocate();
			/** Memory deallocation. */
			void Deallocate();

		protected:
			/** Balance i-th cell. */
			void Balance(int i);
			/** Move i-th cell to the left. */
			void MoveLeft(int i);
			/** Move i-th cell to the right. */
			void MoveRight(int i);
			/** Switch two cells. */
			void Switch(int i, int j);
	};

	/** 2D moving histogram class. */
	template <class T> class MovingHist2D : public MovingHist<T>
	{
		private:
			/** Precomputed look-up tables. */
			int *m_move_hor_right;
			int *m_move_hor_left;
			int *m_switch_hor_right;
			int *m_switch_hor_left;
			int *m_switch_ver;

		public:
			/** Constuctor. */
			MovingHist2D(int lr) : MovingHist<T>((2 * lr + 1) * (2 * lr + 1), lr, 1) { InitTables(); };
			/** Destructor. */
			virtual ~MovingHist2D() { ClearTables(); };
			/** Move histogram in the horizontal direction. */
			virtual void MoveHorizontal(int direction, int position, T value);
			/** Move histogram in the vertical direction. */
			virtual void MoveVertical(int direction, int position, T value);
			/** Switch positions in the horizontal direction. */
			virtual void SwitchHorizontal(int direction);
			/** Switch positions in the vertical direction. */
			virtual void SwitchVertical(int direction);

		private:
			/** Initialize look-up tables. */
			void InitTables();
			/** Deallocate look-up tables. */
			void ClearTables();

		protected:
			using MovingHist<T>::m_size;
			using MovingHist<T>::m_lat_radius;
			using MovingHist<T>::m_cells;
			using MovingHist<T>::m_positions;
			using MovingHist<T>::Balance;
	};

	/** 3D moving histogram class. */
	template <class T> class MovingHist3D : public MovingHist<T>
	{
		private:
			/** Precomputed look-up tables. */
			int *m_move_hor_right;
			int *m_move_hor_left;
			int *m_move_ver_down;
			int *m_move_ver_up;
			int *m_switch_hor_right;
			int *m_switch_hor_left;
			int *m_switch_ver_down;
			int *m_switch_ver_up;
			int *m_switch_axi;

		public:
			/** Constuctor. */
			MovingHist3D(int lr, int ar) : MovingHist<T>((2 * lr + 1) * (2 * lr + 1) * (2 * ar + 1), lr, ar) { InitTables(); };
			/** Destructor. */
			virtual ~MovingHist3D() { ClearTables(); };
			/** Move histogram in the horizontal direction. */
			virtual void MoveHorizontal(int direction, int position, T value);
			/** Move histogram in the vertical direction. */
			virtual void MoveVertical(int direction, int position, T value);
			/** Move histogram in the axial direction. */
			void MoveAxial(int position, T value);
			/** Switch positions in the horizontal direction. */
			virtual void SwitchHorizontal(int direction);
			/** Switch positions in the vertical direction. */
			virtual void SwitchVertical(int direction);
			/** Switch positions in the axial direction. */
			void SwitchAxial();

		private:
			/** Initialize look-up tables. */
			void InitTables();
			/** Deallocate look-up tables. */
			void ClearTables();

		protected:
			using MovingHist<T>::m_size;
			using MovingHist<T>::m_lat_radius;
			using MovingHist<T>::m_axi_radius;
			using MovingHist<T>::m_cells;
			using MovingHist<T>::m_positions;
			using MovingHist<T>::Balance;
	};

	/** 1D moving histogram class. */
	template <class T> class MovingHist1D
	{
		private:
			/** Histogram size. */
			int m_size;
			/** Histogram radius. */
			int m_radius;
			/** Position of the moving cell. */
			int m_move_pos;
			/** Histogram cells. */
			MovingHistCell<T> *m_cells;
			/** Positions of histogram cells. */
			int *m_positions;
			
		public:
			/** Constuctor. */
			MovingHist1D(int radius) : m_size(2 * radius + 1), m_radius(radius), m_move_pos(0), m_cells(NULL), m_positions(NULL) { Allocate(); };
			/** Destructor. */
			~MovingHist1D() { Deallocate(); };
			/** Get median. */
			T GetMedian() const { return m_cells[m_radius].value; }
			/** Initialize histogram. */
			void Init();
			/** Add value. */
			void Add(int position, T value);
			/** Move histogram. */
			void Move(T value);
			/** Switch histogram positions. */
			void SwitchPos();

		private:
			/** Memory allocation. */
			void Allocate();
			/** Memory deallocation. */
			void Deallocate();
			/** Balance i-th cell. */
			void Balance(int i);
			/** Balance i-th cell to the left. */
			void BalanceLeft(int i);
			/** Balance i-th cell to the right. */
			void BalanceRight(int i);
			/** Switch two cells. */
			void Switch(int i, int j);
	};

	/////////////////////////////////////////////////////////////////
	//                                                             //
	//                       Local Definitions                     //
	//                                                             //
	/////////////////////////////////////////////////////////////////

	template <class T> void MovingHist<T>::Add(int position, T value)
	{
		m_cells[position].value = value;
		m_cells[position].position = position;
		m_positions[position] = position;
		Balance(position);
	}
	
	template <class T> void MovingHist<T>::Allocate()
	{
		m_cells = new MovingHistCell<T>[m_size];
		m_positions = new int[m_size];
	}

	template <class T> void MovingHist<T>::Deallocate()
	{
		delete [] m_cells;
		delete [] m_positions;
	}

	template <class T> void MovingHist<T>::Balance(int i)
	{
		if (i > 0 && m_cells[i].value < m_cells[i - 1].value)
		{
			MoveLeft(i);
			return;
		}

		if (i < m_size - 1 && m_cells[i].value > m_cells[i + 1].value)
		{
			MoveRight(i);
			return;
		}
	}

	template <class T> void MovingHist<T>::MoveLeft(int i)
	{
		do 
		{
			Switch(i - 1, i);
			i--;
		} while (i > 0 && m_cells[i].value < m_cells[i - 1].value);
	}

	template <class T> void MovingHist<T>::MoveRight(int i)
	{
		do 
		{
			Switch(i, i + 1);
			i++;
		} while (i < m_size - 1 && m_cells[i].value > m_cells[i + 1].value);
	}

	template <class T> void MovingHist<T>::Switch(int i, int j)
	{
		MovingHistCell<T> tmp = m_cells[j];
		
		m_cells[j] = m_cells[i];
		m_cells[i] = tmp;

		m_positions[m_cells[j].position] = j;
		m_positions[tmp.position] = i;
	}

	template <class T> void MovingHist2D<T>::MoveHorizontal(int direction, int position, T value)
	{
		int pos = direction == 1 ? m_positions[m_move_hor_right[position]] : m_positions[m_move_hor_left[position]];

		m_cells[pos].value = value;
		Balance(pos);
	}

	template <class T> void MovingHist2D<T>::MoveVertical(int direction, int position, T value)
	{
		int pos = m_positions[position];

		m_cells[pos].value = value;
		Balance(pos);
	}

	template <class T> void MovingHist2D<T>::SwitchHorizontal(int direction)
	{
		int *hor_switch = direction == 1 ? m_switch_hor_right : m_switch_hor_left;

		for (int i = 0; i < m_size; i++)
		{
			m_cells[i].position = hor_switch[m_cells[i].position];
			m_positions[m_cells[i].position] = i;
		}
	}

	template <class T> void MovingHist2D<T>::SwitchVertical(int direction)
	{
		for (int i = 0; i < m_size; i++)
		{
			m_cells[i].position = m_switch_ver[m_cells[i].position];
			m_positions[m_cells[i].position] = i;
		}
	}

	template <class T> void MovingHist2D<T>::InitTables()
	{
		int tmp1, tmp2;
		int diam = 2 * m_lat_radius + 1;

		m_move_hor_right = new int[diam];
		m_move_hor_left = new int[diam];
		for (int i = 0; i < diam; i++)
		{
			m_move_hor_right[i] = i * diam;
			m_move_hor_left[i] = (i + 1) * diam - 1;
		}

		m_switch_hor_right = new int[m_size];
		m_switch_hor_left = new int[m_size];
		m_switch_ver = new int[m_size];
		for (int i = 0; i < m_size; i++)
		{
			tmp1 = i / diam;
			tmp2 = tmp1 * diam;
			m_switch_hor_right[i] = tmp2 + (i + diam - 1) % diam;
			m_switch_hor_left[i] = tmp2 + (i + 1) % diam;
			m_switch_ver[i] = ((tmp1 + diam - 1) % diam) * diam + i % diam;
		}
	}

	template <class T> void MovingHist2D<T>::ClearTables()
	{
		delete [] m_move_hor_right;
		delete [] m_move_hor_left;
		delete [] m_switch_hor_right;
		delete [] m_switch_hor_left;
		delete [] m_switch_ver;
	}

	template <class T> void MovingHist3D<T>::MoveHorizontal(int direction, int position, T value)
	{
		int pos = direction == 1 ? m_positions[m_move_hor_right[position]] : m_positions[m_move_hor_left[position]];

		m_cells[pos].value = value;
		Balance(pos);
	}

	template <class T> void MovingHist3D<T>::MoveVertical(int direction, int position, T value)
	{
		int pos = direction == 1 ? m_positions[m_move_ver_down[position]] : m_positions[m_move_ver_up[position]];

		m_cells[pos].value = value;
		Balance(pos);
	}

	template <class T> void MovingHist3D<T>::MoveAxial(int position, T value)
	{
		int pos = m_positions[position];

		m_cells[pos].value = value;
		Balance(pos);
	}

	template <class T> void MovingHist3D<T>::SwitchHorizontal(int direction)
	{
		int *hor_switch = direction == 1 ? m_switch_hor_right : m_switch_hor_left;

		for (int i = 0; i < m_size; i++)
		{
			m_cells[i].position = hor_switch[m_cells[i].position];
			m_positions[m_cells[i].position] = i;
		}
	}

	template <class T> void MovingHist3D<T>::SwitchVertical(int direction)
	{
		int *ver_switch = direction == 1 ? m_switch_ver_down : m_switch_ver_up;

		for (int i = 0; i < m_size; i++)
		{
			m_cells[i].position = ver_switch[m_cells[i].position];
			m_positions[m_cells[i].position] = i;
		}
	}

	template <class T> void MovingHist3D<T>::SwitchAxial()
	{
		for (int i = 0; i < m_size; i++)
		{
			m_cells[i].position = m_switch_axi[m_cells[i].position];
			m_positions[m_cells[i].position] = i;
		}
	}

	template <class T> void MovingHist3D<T>::InitTables()
	{
		int tmp1, tmp2, tmp3, tmp4;
		int lat_diam = 2 * m_lat_radius + 1;
		int lat_size = lat_diam * lat_diam;
		int axi_diam = 2 * m_axi_radius + 1;
		int axi_size = lat_diam * axi_diam;
		
		m_move_hor_right = new int[axi_size];
		m_move_hor_left = new int[axi_size];
		m_move_ver_down = new int[axi_size];
		m_move_ver_up = new int[axi_size];
		for (int i = 0; i < axi_size; i++)
		{
			tmp1 = (i / lat_diam) * lat_size;
			tmp2 = i % lat_diam;
			m_move_hor_right[i] = i * lat_diam;
			m_move_hor_left[i] = (i + 1) * lat_diam - 1;
			m_move_ver_down[i] = tmp1 + tmp2; 
			m_move_ver_up[i] = tmp1 + lat_size - lat_diam + tmp2;
		}

		m_switch_hor_right = new int[m_size];
		m_switch_hor_left = new int[m_size];
		m_switch_ver_down = new int[m_size];
		m_switch_ver_up = new int[m_size];
		m_switch_axi = new int[m_size];
		for (int i = 0; i < m_size; i++)
		{
			tmp1 = i / lat_diam;
			tmp2 = tmp1 * lat_diam;
			m_switch_hor_right[i] = tmp2 + (i + lat_diam - 1) % lat_diam;
			m_switch_hor_left[i] = tmp2 + (i + 1) % lat_diam;

			tmp2 = i % lat_diam;
			tmp3 = i / lat_size;
			tmp4 = tmp3 * lat_size;
			m_switch_ver_down[i] = tmp4 + ((tmp1 + lat_diam - 1) % lat_diam) * lat_diam + tmp2;
			m_switch_ver_up[i] = tmp4 + ((tmp1 + 1) % lat_diam) * lat_diam + tmp2;

			m_switch_axi[i] = ((tmp3 + axi_diam - 1) % axi_diam) * lat_size + i % lat_size;
		}
	}

	template <class T> void MovingHist3D<T>::ClearTables()
	{
		delete [] m_move_hor_right;
		delete [] m_move_hor_left;
		delete [] m_move_ver_down;
		delete [] m_move_ver_up;
		delete [] m_switch_hor_right;
		delete [] m_switch_hor_left;
		delete [] m_switch_ver_down;
		delete [] m_switch_ver_up;
		delete [] m_switch_axi;
	}

	template <class T> void MovingHist1D<T>::Init()
	{
		m_move_pos = 0;

		for (int i = 0; i < m_radius; i++)
		{
			m_cells[i].value = T(0);
			m_cells[i].position = i;
			m_positions[i] = i;
		}

		for (int i = m_radius; i < m_size; i++)
		{
			m_cells[i].value = std::numeric_limits<T>::max();
			m_cells[i].position = -1;
			m_positions[i] = -1;
		}
	}

	template <class T> void MovingHist1D<T>::Add(int position, T value)
	{
		int index = position + m_radius;

		m_cells[index].value = value;
		m_cells[index].position = index;
		m_positions[index] = index;
		Balance(index);
	}

	template <class T> void MovingHist1D<T>::Move(T value)
	{
		int pos = m_positions[m_move_pos];

		m_cells[pos].value = value;
		Balance(pos);
	}

	template <class T> void MovingHist1D<T>::SwitchPos()
	{
		m_move_pos++;
		m_move_pos %= m_size;
	}
	
	template <class T> void MovingHist1D<T>::Allocate()
	{
		m_cells = new MovingHistCell<T>[m_size];
		m_positions = new int[m_size];
	}

	template <class T> void MovingHist1D<T>::Deallocate()
	{
		delete [] m_cells;
		delete [] m_positions;
	}

	template <class T> void MovingHist1D<T>::Balance(int i)
	{
		if (i > 0 && m_cells[i].value < m_cells[i - 1].value)
		{
			BalanceLeft(i);
			return;
		}

		if (i < m_size - 1 && m_cells[i].value > m_cells[i + 1].value)
		{
			BalanceRight(i);
			return;
		}
	}

	template <class T> void MovingHist1D<T>::BalanceLeft(int i)
	{
		do 
		{
			Switch(i - 1, i);
			i--;
		} while (i > 0 && m_cells[i].value < m_cells[i - 1].value);
	}

	template <class T> void MovingHist1D<T>::BalanceRight(int i)
	{
		do 
		{
			Switch(i, i + 1);
			i++;
		} while (i < m_size - 1 && m_cells[i].value > m_cells[i + 1].value);
	}

	template <class T> void MovingHist1D<T>::Switch(int i, int j)
	{
		MovingHistCell<T> tmp = m_cells[j];
		
		m_cells[j] = m_cells[i];
		m_cells[i] = tmp;

		m_positions[m_cells[j].position] = j;
		m_positions[tmp.position] = i;
	}

	template <class T> void MedianFilter2D(const i3d::Image3d<T> &input, i3d::Image3d<T> &output, int radius)
	{
		i3d::Image3d<T> tmp;
		i3d::GenerateDummyVoxels(input, tmp, T(0), i3d::Vector3d<size_t>(radius, radius, 0));

		output.CopyMetaData(input);
		MovingHist2D<T> hist(radius);

		size_t tmp_size_x = tmp.GetSizeX();
		size_t input_size_x = input.GetSizeX();
		size_t input_size_y = input.GetSizeY();

		T *p = tmp.GetFirstVoxelAddr();
		int diam = 2 * radius + 1;
		int size = diam * diam;
		for (int i = 0; i < size; i++)
		{
			hist.Add(i, *(p + (i / diam) * tmp_size_x + i % diam));	
		}

		p += radius * (tmp_size_x + 1);
		T *q = output.GetFirstVoxelAddr();
		int direction = 1;
		int tmp1;

		for (size_t y = 0; y < input_size_y; y++)
		{
			for (size_t x = 0; x < input_size_x - 1; x++)
			{
				*q = hist.GetMedian();

				// move histogram in the horizontal direction
				tmp1 = direction * (radius + 1);
				for (int i = -radius; i <= radius; i++)
				{
					hist.MoveHorizontal(direction, i + radius, *(p + i * tmp_size_x + tmp1));
				}
				hist.SwitchHorizontal(direction);

				p += direction;
				q += direction;
			}

			*q = hist.GetMedian();

			if (y != input_size_y - 1)
			{
				// move histogram in the vertical direction
				tmp1 = tmp_size_x * (radius + 1);
				for (int i = -radius; i <= radius; i++)
				{
					hist.MoveVertical(direction, i + radius, *(p + tmp1 + i));
				}
				hist.SwitchVertical(direction);
			
				p += tmp_size_x;
				q += input_size_x;
				direction *= -1;
			}
		}
	}

	template <class T> void MedianFilter3D(const i3d::Image3d<T> &input, i3d::Image3d<T> &output, 
			                               int lat_radius, int axi_radius)
	{
		i3d::Image3d<T> tmp;
		i3d::GenerateDummyVoxels(input, tmp, T(0), i3d::Vector3d<size_t>(lat_radius, lat_radius, axi_radius));

		output.CopyMetaData(input);
		MovingHist3D<T> hist(lat_radius, axi_radius);

		size_t tmp_size_x = tmp.GetSizeX();
		size_t tmp_size_y = tmp.GetSizeY();
		size_t tmp_slice = tmp_size_x * tmp_size_y;
		size_t input_size_x = input.GetSizeX();
		size_t input_size_y = input.GetSizeY();
		size_t input_size_z = input.GetSizeZ();
		size_t input_slice = input_size_x * input_size_y;

		T *p = tmp.GetFirstVoxelAddr();
		int axi_diam = 2 * axi_radius + 1;
		int lat_diam = 2 * lat_radius + 1;
		int lat_size = lat_diam * lat_diam;
		int axi_size = axi_diam * lat_diam;
		int size = lat_size * axi_diam;
		for (int i = 0; i < size; i++)
		{
			hist.Add(i, *(p + (i / lat_size) * tmp_slice + ((i / lat_diam) % lat_diam) * tmp_size_x + i % lat_diam));
		}

		p += axi_radius * tmp_slice + lat_radius * (tmp_size_x + 1);
		T *q = output.GetFirstVoxelAddr();
		int direction_x = 1;
		int direction_y = 1;
		int tmp1;

		for (size_t z = 0; z < input_size_z; z++)
		{
			for (size_t y = 0; y < input_size_y; y++)
			{
				for (size_t x = 0; x < input_size_x - 1; x++)
				{
					*q = hist.GetMedian();

					// move histogram in the horizontal direction
					tmp1 = direction_x * (lat_radius + 1);
					for (int j = -axi_radius; j <= axi_radius; j++)
					{
						for (int i = -lat_radius; i <= lat_radius; i++)
						{
							hist.MoveHorizontal(direction_x, j * lat_diam + i + axi_size / 2, 
										        *(p + tmp_slice * j + i * tmp_size_x + tmp1));
						}
					}
					hist.SwitchHorizontal(direction_x);

					p += direction_x;
					q += direction_x;
				}

				*q = hist.GetMedian();

				if (y != input_size_y - 1)
				{
					// move histogram in the vertical direction
					tmp1 = direction_y * (lat_radius + 1) * tmp_size_x;
					for (int j = -axi_radius; j <= axi_radius; j++)
					{
						for (int i = -lat_radius; i <= lat_radius; i++)
						{
							hist.MoveVertical(direction_y, j * lat_diam + i + axi_size / 2, *(p + tmp_slice * j + i + tmp1));
						}
					}
					hist.SwitchVertical(direction_y);

					p += direction_y * tmp_size_x;
					q += direction_y * input_size_x;
				}

				direction_x *= -1;
			}

			*q = hist.GetMedian();

			if (z != input_size_z - 1)
			{
				// move histogram in the axial direction
				tmp1 = tmp_slice * (axi_radius + 1);
				for (int j = -lat_radius; j <= lat_radius; j++)
				{
					for (int i = -lat_radius; i <= lat_radius; i++)
					{
						hist.MoveAxial(j * lat_diam + i + lat_size / 2, *(p + tmp1 + j * tmp_size_x + i));
					}
				}
				hist.SwitchAxial();

				p += tmp_slice;
				q += input_slice;
			}

			direction_y *= -1;
		}
	}

	template <class T> void SeparableMedianFilter2D(const i3d::Image3d<T> &input, i3d::Image3d<T> &output, int radius)
	{
		output.CopyMetaData(input);
		MovingHist1D<T> hist(radius);

		size_t input_size_x = input.GetSizeX();
		size_t input_size_y = input.GetSizeY();

		const T *p = input.GetFirstVoxelAddr();
		T *q = output.GetFirstVoxelAddr();
		int tmp = radius + 1;
		
		// 1D median filtering in the x-axis
		for (size_t y = 0; y < input_size_y; y++)
		{
			hist.Init();

			for (int i = 0; i <= radius; i++)
			{
				hist.Add(i, *(p + i));
			}

			for (size_t x = 0; x < input_size_x - radius - 1; x++)
			{
				*q = hist.GetMedian();

				hist.Move(*(p + tmp));
				hist.SwitchPos();

				p++;
				q++;
			}

			for (int i = 0; i <= radius; i++)
			{
				*q = hist.GetMedian();

				hist.Move(T(0));
				hist.SwitchPos();

				p++;
				q++;
			}
		}

		tmp = (radius + 1) * input_size_x;

		// 1D median filtering in the y-axis
		for (size_t x = 0; x < input_size_x; x++)
		{
			hist.Init();
			q = output.GetFirstVoxelAddr() + x;

			for (int i = 0; i <= radius; i++)
			{
				hist.Add(i, *(q + i * input_size_x));
			}

			for (size_t y = 0; y < input_size_y - radius - 1; y++)
			{
				*q = hist.GetMedian();

				hist.Move(*(q + tmp));
				hist.SwitchPos();

				q += input_size_x;
			}

			for (int i = 0; i <= radius; i++)
			{
				*q = hist.GetMedian();

				hist.Move(T(0));
				hist.SwitchPos();

				q += input_size_x;
			}
		}
	}

	template <class T> void SeparableMedianFilter3D(const i3d::Image3d<T> &input, i3d::Image3d<T> &output, 
			                                        int lat_radius, int axi_radius)
	{
		output.CopyMetaData(input);
		MovingHist1D<T> lat_hist(lat_radius);
		MovingHist1D<T> axi_hist(axi_radius);

		size_t input_size_x = input.GetSizeX();
		size_t input_size_y = input.GetSizeY();
		size_t input_size_z = input.GetSizeZ();
		size_t slice_size = input_size_x * input_size_y;

		const T *p = input.GetFirstVoxelAddr();
		T *q = output.GetFirstVoxelAddr();
		int tmp = lat_radius + 1;
		
		// 1D median filtering in the x-axis
		for (size_t z = 0; z < input_size_z; z++)
		{
			for (size_t y = 0; y < input_size_y; y++)
			{
				lat_hist.Init();

				for (int i = 0; i <= lat_radius; i++)
				{
					lat_hist.Add(i, *(p + i));
				}

				for (size_t x = 0; x < input_size_x - lat_radius - 1; x++)
				{
					*q = lat_hist.GetMedian();

					lat_hist.Move(*(p + tmp));
					lat_hist.SwitchPos();

					p++;
					q++;
				}

				for (int i = 0; i <= lat_radius; i++)
				{
					*q = lat_hist.GetMedian();

					lat_hist.Move(T(0));
					lat_hist.SwitchPos();

					p++;
					q++;
				}
			}
		}

		tmp = (lat_radius + 1) * input_size_x;

		// 1D median filtering in the y-axis
		for (size_t z = 0; z < input_size_z; z++)
		{
			for (size_t x = 0; x < input_size_x; x++)
			{
				lat_hist.Init();
				q = output.GetFirstVoxelAddr() + x;

				for (int i = 0; i <= lat_radius; i++)
				{
					lat_hist.Add(i, *(q + i * input_size_x));
				}

				for (size_t y = 0; y < input_size_y - lat_radius - 1; y++)
				{
					*q = lat_hist.GetMedian();

					lat_hist.Move(*(q + tmp));
					lat_hist.SwitchPos();

					q += input_size_x;
				}

				for (int i = 0; i <= lat_radius; i++)
				{
					*q = lat_hist.GetMedian();

					lat_hist.Move(T(0));
					lat_hist.SwitchPos();

					q += input_size_x;
				}
			}
		}

		tmp = (axi_radius + 1) * slice_size;

		// 1D median filtering in the z-axis
		for (size_t y = 0; y < input_size_y; y++)
		{
			for (size_t x = 0; x < input_size_x; x++)
			{
				axi_hist.Init();
				q = output.GetFirstVoxelAddr() + y * input_size_x + x;

				for (int i = 0; i <= axi_radius; i++)
				{
					axi_hist.Add(i, *(q + i * slice_size));
				}

				for (size_t z = 0; z < input_size_z - axi_radius - 1; z++)
				{
					*q = axi_hist.GetMedian();

					axi_hist.Move(*(q + tmp));
					axi_hist.SwitchPos();

					q += slice_size;
				}

				for (int i = 0; i <= axi_radius; i++)
				{
					*q = axi_hist.GetMedian();

					axi_hist.Move(T(0));
					axi_hist.SwitchPos();

					q += slice_size;
				}
			}
		}
	}

	/////////////////////////////////////////////////////////////////
	//                                                             //
	//                        MedianFilter                         //
	//                                                             //
	/////////////////////////////////////////////////////////////////

	template <class T> I3D_DLLEXPORT 
		void MedianFilter(const Image3d<T>& img_in, Image3d<T>& img_out,
		const Neighbourhood& neib)
	{
		img_out.CopyMetaData(img_in);

		size_t i = 0;
		std::vector<const T*> win;
		T *p = img_out.GetFirstVoxelAddr();
		size_t image_size = img_in.GetImageSize();

		while (i < image_size)
		{ 
			size_t num = GetWindow(img_in, img_in.GetX(i), img_in.GetY(i), 
				img_in.GetZ(i), neib, win);

			for (size_t j = 0; j < num; ++j)
			{ 
				*p = GetMedian(win);
				MoveWindow(win);
				++p;     
			}
			i += num;
		}
	}

	template <class T> I3D_DLLEXPORT
		void MedianFilter(const i3d::Image3d<T> &input, i3d::Image3d<T> &output, int lat_radius, int axi_radius)
	{
		if (lat_radius * axi_radius <= 0 || (size_t) lat_radius > input.GetSizeX() || 
		                                    (size_t) lat_radius > input.GetSizeY() || 
						    (size_t) axi_radius > input.GetSizeZ())
		{
			throw i3d::InternalException("Incorrect radii of the rectangular neighborhood!");
		}
		else
		{
			if (input.GetSizeZ() == 1)
			{
				MedianFilter2D(input, output, lat_radius);
			}
			else
			{
				MedianFilter3D(input, output, lat_radius, axi_radius);
			}
		}
	}

	template <class T> I3D_DLLEXPORT
		void SeparableMedianFilter(const i3d::Image3d<T> &input, i3d::Image3d<T> &output, int lat_radius, int axi_radius)
	{
		if (lat_radius * axi_radius <= 0 || (size_t) lat_radius > input.GetSizeX() || 
		                                    (size_t) lat_radius > input.GetSizeY() || 
						    (size_t) axi_radius > input.GetSizeZ())
		{
			throw i3d::InternalException("Incorrect radii of the rectangular neighborhood!");
		}
		else
		{
			if (input.GetSizeZ() == 1)
			{
				SeparableMedianFilter2D(input, output, lat_radius);
			}
			else
			{
				SeparableMedianFilter3D(input, output, lat_radius, axi_radius);
			}
		}
	}

	/* Explicit instantiations */
	template I3D_DLLEXPORT void MedianFilter(const Image3d<GRAY16>& img_in, Image3d<GRAY16>& img_out, const Neighbourhood& neib);

	template I3D_DLLEXPORT void MedianFilter(const i3d::Image3d<i3d::GRAY16> &input, i3d::Image3d<i3d::GRAY16> &output, 
		                                     int lat_radius, int axi_radius);

	template I3D_DLLEXPORT void SeparableMedianFilter(const i3d::Image3d<i3d::GRAY16> &input, i3d::Image3d<i3d::GRAY16> &output, 
													  int lat_radius, int axi_radius);
	
} // namespace i3d ends here
