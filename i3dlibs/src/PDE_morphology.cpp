/*
 * PDEmorphology.cpp
 *
 * Computes basic morphology operations using PDE.
 *
 * Pavel Matula (pam@fi.muni.cz) 2007
 *
 */

#include "PDE_morphology.h"

namespace i3d {

	/******************************************************************************************/\

	template <class FLOATING_VOXEL> void PDE_ExplicitSolver<FLOATING_VOXEL>::ReflectBoundary(Image3d<FLOATING_VOXEL> *buf)
	{
		for (size_t i = 0; i < _border; ++i) {
			for (size_t y = 0; y < buf->GetSizeY() - 2*_border; ++y) {
				for (size_t x = 0; x < buf->GetSizeX() - 2*_border; ++x) {
					buf->SetVoxel(x + _border, y + _border, _border - i - 1, 
						buf->GetVoxel(x + _border, y + _border, _border + i));
					buf->SetVoxel(x + _border, y + _border, buf->GetSizeZ() - _border + i, 
						buf->GetVoxel(x + _border, y + _border, buf->GetSizeZ() - 1 - _border - i));
				}
			}
		}

		for (size_t i = 0; i < _border; ++i) {
			for (size_t z = 0; z < buf->GetSizeZ() - 2*_border; ++z) {
				for (size_t x = 0; x < buf->GetSizeX() - 2*_border; ++x) {
					buf->SetVoxel(x + _border, _border - i - 1, z + _border, 
						buf->GetVoxel(x + _border, _border + i, z + _border));
					buf->SetVoxel(x + _border, buf->GetSizeY() - _border + i, z + _border, 
						buf->GetVoxel(x + _border, buf->GetSizeY() - 1 - _border - i, z + _border));
				}
			}
		}

		for (size_t i = 0; i < _border; ++i) {
			for (size_t z = 0; z < buf->GetSizeZ() - 2*_border; ++z) {
				for (size_t y = 0; y < buf->GetSizeY() - 2*_border; ++y) {
					buf->SetVoxel(_border - i - 1, y + _border, z + _border, 
						buf->GetVoxel(_border + i, y + _border, z + _border));
					buf->SetVoxel(buf->GetSizeX() - _border + i, y + _border, z + _border, 
						buf->GetVoxel(buf->GetSizeX() - 1 - _border - i, y + _border, z + _border));
				}
			}
		}
	}

	/******************************************************************************************/\

	template <class FLOATING_VOXEL> void PDE_ExplicitSolver<FLOATING_VOXEL>::InitBuffers()
	{
		ReleaseBuffers();

		_buf1 = new Image3d<FLOATING_VOXEL>();
		_buf1->SetResolution(_img->GetResolution());
		_buf1->MakeRoom(_img->GetSizeX() + 2*_border, _img->GetSizeY() + 2*_border, _img->GetSizeZ() + 2*_border);

		_buf2 = new Image3d<FLOATING_VOXEL>();
		_buf2->SetResolution(_img->GetResolution());
		_buf2->MakeRoom(_img->GetSizeX() + 2*_border, _img->GetSizeY() + 2*_border, _img->GetSizeZ() + 2*_border);
	}

	/******************************************************************************************/

	template <class FLOATING_VOXEL> void PDE_ExplicitSolver<FLOATING_VOXEL>::ReleaseBuffers()
	{
		if (_buf1) delete _buf1;
		_buf1 = NULL;

		if (_buf2) delete _buf2;
		_buf2 = NULL;
	}

	/******************************************************************************************/

	template <class FLOATING_VOXEL> void PDE_ExplicitSolver<FLOATING_VOXEL>::BufferToImage(const Image3d<FLOATING_VOXEL> *buf)
	{
		if (!_img)
			throw InternalException("_img not set in PDE_ExplicitSolver::BufferToImage");
		if (!buf)
			throw InternalException("buf not set in PDE_ExplicitSolver::BufferToImage");
		if (_img->GetSizeX() + 2 * _border != buf->GetSizeX())
			throw InternalException("buffer x-size does not match in PDE_ExplicitSolver::BufferToImage");
		if (_img->GetSizeY() + 2 * _border != buf->GetSizeY())
			throw InternalException("buffer y-size does not match in PDE_ExplicitSolver::BufferToImage");
		if (_img->GetSizeZ() + 2 * _border != buf->GetSizeZ())
			throw InternalException("buffer z-size does not match in PDE_ExplicitSolver::BufferToImage");

		for (size_t z = 0; z < _img->GetSizeZ(); ++z)
			for (size_t y = 0; y < _img->GetSizeY(); ++y)
				for (size_t x = 0; x < _img->GetSizeX(); ++x) {
					_img->SetVoxel(x, y, z, buf->GetVoxel(x + _border, y + _border, z + _border));
				}
	}

	/******************************************************************************************/

	template <class FLOATING_VOXEL> void PDE_ExplicitSolver<FLOATING_VOXEL>::ImageToBuffer(Image3d<FLOATING_VOXEL> *buf)
	{
		if (!_img)
			throw InternalException("_img not set in PDE_ExplicitSolver::ImageToBuffer");
		if (!buf)
			throw InternalException("buf not set in PDE_ExplicitSolver::ImageToBuffer");
		if (_img->GetSizeX() + 2 * _border != buf->GetSizeX())
			throw InternalException("buffer x-size does not match in PDE_ExplicitSolver::ImageToBuffer");
		if (_img->GetSizeY() + 2 * _border != buf->GetSizeY())
			throw InternalException("buffer y-size does not match in PDE_ExplicitSolver::ImageToBuffer");
		if (_img->GetSizeZ() + 2 * _border != buf->GetSizeZ())
			throw InternalException("buffer z-size does not match in PDE_ExplicitSolver::ImageToBuffer");

		for (size_t z = 0; z < _img->GetSizeZ(); ++z)
			for (size_t y = 0; y < _img->GetSizeY(); ++y)
				for (size_t x = 0; x < _img->GetSizeX(); ++x) {
					buf->SetVoxel(x + _border, y + _border, z + _border, _img->GetVoxel(x, y, z));
				}
	}

	/******************************************************************************************/

	template <class FLOATING_VOXEL> void PDE_ExplicitSolver<FLOATING_VOXEL>::Execute() 
	{
		Image3d<FLOATING_VOXEL> *pom;

		InitBuffers();
		ImageToBuffer(_buf1);

		for (size_t i = 0; i < _num_iters; ++i) {
			PrepareBoundaryCondition(_buf1);
			ExecuteExplicitStep(_buf1, _buf2);
			pom = _buf1; _buf1 = _buf2; _buf2 = pom;
		}

		BufferToImage(_buf1);
		ReleaseBuffers();
	}

/******************************************************************************************/
	
	template <class FLOATING_VOXEL> void PDE_Dilation<FLOATING_VOXEL>::PrepareBoundaryCondition(Image3d<FLOATING_VOXEL> *img)
	{
		ReflectBoundary(img);
	}

/******************************************************************************************/

	template <class FLOATING_VOXEL> FLOATING_VOXEL UpWindSchemeN(FLOATING_VOXEL c, FLOATING_VOXEL x1, FLOATING_VOXEL x2, 
		FLOATING_VOXEL y1, FLOATING_VOXEL y2, FLOATING_VOXEL z1, FLOATING_VOXEL z2, FLOATING_VOXEL tau)
	{
	FLOATING_VOXEL dx1, dx2, dy1, dy2, dz1, dz2;
	dx1 = std::max(c - x1, 0.0f);
	dx2 = std::min(x2 - c, 0.0f);
	dy1 = std::max(c - y1, 0.0f);
	dy2 = std::min(y2 - c, 0.0f);
	dz1 = std::max(c - z1, 0.0f);
	dz2 = std::min(z2 - c, 0.0f);
	return c - tau * sqrt(dx1 * dx1 + dx2 * dx2 + dy1 * dy1 + dy2 * dy2 + dz1 * dz1 + dz2 * dz2);
	}

	template <class FLOATING_VOXEL> FLOATING_VOXEL UpWindSchemeP(FLOATING_VOXEL c, FLOATING_VOXEL x1, FLOATING_VOXEL x2, 
		FLOATING_VOXEL y1, FLOATING_VOXEL y2, FLOATING_VOXEL z1, FLOATING_VOXEL z2, FLOATING_VOXEL tau)
	{
	FLOATING_VOXEL dx1, dx2, dy1, dy2, dz1, dz2;
	dx1 = std::min(c - x1, 0.0f);
	dx2 = std::max(x2 - c, 0.0f);
	dy1 = std::min(c - y1, 0.0f);
	dy2 = std::max(y2 - c, 0.0f);
	dz1 = std::min(c - z1, 0.0f);
	dz2 = std::max(z2 - c, 0.0f);
	return c + tau * sqrt(dx1 * dx1 + dx2 * dx2 + dy1 * dy1 + dy2 * dy2 + dz1 * dz1 + dz2 * dz2);
	}

	template <class FLOATING_VOXEL> void PDE_Dilation<FLOATING_VOXEL>::ExecuteExplicitStep(Image3d<FLOATING_VOXEL> *img_in, Image3d<FLOATING_VOXEL> *img_out)
	{
		for (size_t z = this->_border; z < img_in->GetSizeZ() - this->_border; ++ z) {
			for (size_t y = this->_border; y < img_in->GetSizeY() - this->_border; ++ y) {
				for (size_t x = this->_border; x < img_in->GetSizeX() - this->_border; ++ x) {
					img_out->SetVoxel(x,y,z, UpWindSchemeN(
						img_in->GetVoxel(x    , y    , z), 
						img_in->GetVoxel(x + 1, y    , z), 
						img_in->GetVoxel(x - 1, y    , z), 
						img_in->GetVoxel(x    , y + 1, z), 
						img_in->GetVoxel(x    , y - 1, z), 
						img_in->GetVoxel(x    , y    , z + 1), 
						img_in->GetVoxel(x    , y    , z - 1), 
						this->_tau));
				}
			}
		}
	}

/******************************************************************************************/

	template <class FLOATING_VOXEL> void PDE_Erosion<FLOATING_VOXEL>::PrepareBoundaryCondition(Image3d<FLOATING_VOXEL> *img)
	{
		ReflectBoundary(img);
	}

/******************************************************************************************/

	template <class FLOATING_VOXEL> void PDE_Erosion<FLOATING_VOXEL>::ExecuteExplicitStep(Image3d<FLOATING_VOXEL> *img_in, Image3d<FLOATING_VOXEL> *img_out)
	{
		for (size_t z = this->_border; z < img_in->GetSizeZ() - this->_border; ++ z) {
			for (size_t y = this->_border; y < img_in->GetSizeY() - this->_border; ++ y) {
				for (size_t x = this->_border; x < img_in->GetSizeX() - this->_border; ++ x) {
					img_out->SetVoxel(x,y,z, UpWindSchemeP(
						img_in->GetVoxel(x    , y    , z), 
						img_in->GetVoxel(x + 1, y    , z), 
						img_in->GetVoxel(x - 1, y    , z), 
						img_in->GetVoxel(x    , y + 1, z), 
						img_in->GetVoxel(x    , y - 1, z), 
						img_in->GetVoxel(x    , y    , z + 1), 
						img_in->GetVoxel(x    , y    , z - 1), 
						this->_tau));
				}
			}
		}
	}

/******************************************************************************************/

template class I3D_DLLEXPORT PDE_Erosion<float>;
template class I3D_DLLEXPORT PDE_Dilation<float>;

}
