/*
 * i3dlib - image manipulation library
 *
 * Copyright (C) 2000-2006   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** \file morphology.cc
  \brief Mathematical morphology routines - implementation.

  This file contains bodies of mathematical morphology routines and related
  functions.

  \author Martin Vach 2001 (xvach@fi.muni.cz - BSc student)
  \author Petr Matula 2001 (pem@fi.muni.cz - his supervisor) - some updates

*/ 

/*Suppression of Deprecated warning for Visual studio 8*/
#ifdef _MSC_VER
#if _MSC_VER == 1400
#define _CRT_SECURE_NO_DEPRECATE 1
#endif
#endif

#include <iostream>
#include <functional>
#include <vector>
#include <queue>
#include <limits>

#include "image3d.h"
#include "morphology.h"
#include "threshold.h"
#include "DistanceTransform.h"
#include "draw.h"

#ifdef __GNUG__
#	pragma implementation
#endif

using std::numeric_limits;
using std::vector;
using std::queue;

namespace i3d {
	/*****************************************************************************\
	*
	*                            Local functions
	*
	\******************************************************************************/
	//--------------------------------------------------------------------------
	// Rec1DForward
	//--------------------------------------------------------------------------
	template <class T, class F, class G> bool Rec1DForward(
		const Image3d<T> &mask, 
		Image3d<T> &out, 
		size_t start1, 
		size_t start2,
		int num1, 
		int num2, 
		int num3, 
		int stride2, 
		int stride3,
		F func_f, 
		G func_g)
	{
		if (num1 == 0 || num2 == 0 || num3 == 0)
			return false;

		bool change = false;

		const T* s = mask.GetFirstVoxelAddr() + start1;
		T* t1 = out.GetFirstVoxelAddr() + start1;
		T* t2 = out.GetFirstVoxelAddr() + start2;
		T old;

		for (int i = 0; i < num3; ++i)
		{
			for (int j = 0; j < num2; ++j)
			{
				for (int k = 0; k < num1; ++k)
				{
					old = *t1;
					*t1 = func_f( *t1, *t2 );             
					*t1 = func_g( *t1, *s);    
					change |= (old != *t1);
					++t1;
					++t2;
					++s;
				}
				t1 += stride2;
				t2 += stride2;
				s  += stride2;
			}
			t1 += stride3;
			t2 += stride3;
			s  += stride3;
		}
		return change;
	} // Rec1DForward

	//--------------------------------------------------------------------------
	// Rec1DBackward
	//--------------------------------------------------------------------------
	template <class T, class F, class G> bool Rec1DBackward(
		const Image3d<T> &mask, 
		Image3d<T> &out, 
		size_t start1, 
		size_t start2,
		int num1, 
		int num2,
		int num3,
		int stride2, 
		int stride3,
		F func_f, 
		G func_g)
	{
		if (num1 == 0 || num2 == 0 || num3 == 0)
			return false;

		bool change = false;

		const T* s = mask.GetFirstVoxelAddr() + start1;
		T* t1 = out.GetFirstVoxelAddr() + start1;
		T* t2 = out.GetFirstVoxelAddr() + start2;
		T old;

		for (int i = 0; i < num3; ++i)
		{
			for (int j = 0; j < num2; ++j)
			{
				for (int k = 0; k < num1; ++k)
				{
					old = *t1;
					*t1 = func_f( *t1, *t2 );             
					*t1 = func_g( *t1, *s);    
					change |= (old != *t1);
					--t1;
					--t2;
					--s;
				}
				t1 -= stride2;
				t2 -= stride2;
				s  -= stride2;
			}
			t1 -= stride3;
			t2 -= stride3;
			s  -= stride3;
		}
		return change;
	} // Rec1DBackward

	/**************************************************************************\
	*
	*                   Mathematical morphology routines
	*
	\**************************************************************************/
	//--------------------------------------------------------------------------
	// Dilation
	//--------------------------------------------------------------------------
	template <class T> void Dilation(const Image3d<T> &in, Image3d<T> &out, const Neighbourhood &neib)
	{
		DilEro(in,out,neib,maximum<T>());
	} // Dilation

	//--------------------------------------------------------------------------
	// Explicit instantiations
	//--------------------------------------------------------------------------
	template I3D_DLLEXPORT void Dilation(
		const Image3d<GRAY8> &in,
		Image3d<GRAY8> &out, 
		const Neighbourhood &neib);

	template I3D_DLLEXPORT void Dilation(
		const Image3d<BINARY> &in, 
		Image3d<BINARY> &out, 
		const Neighbourhood &neib);

	template I3D_DLLEXPORT void Dilation(
		const Image3d<size_t> &in, 
		Image3d<size_t> &out, 
		const Neighbourhood &neib);

	template I3D_DLLEXPORT void Dilation(
		const Image3d<unsigned short> &in, 
		Image3d<unsigned short> &out, 
		const Neighbourhood &neib);

	//--------------------------------------------------------------------------
	// LocalDilation
	//--------------------------------------------------------------------------
	template <class T> void LocalDilation(const Image3d<T> &in, 
													  Image3d<T> &out, 
													  const Image3d<BINARY> &mask,
													  const Neighbourhood &neib)
	{
		LocalDilEro(in,out,mask,neib,maximum<T>());
	} // LocalDilation

	//--------------------------------------------------------------------------
	// Explicit instantiations
	//--------------------------------------------------------------------------
	template I3D_DLLEXPORT void LocalDilation(
		const Image3d<GRAY8> &in,
		Image3d<GRAY8> &out, 
		const Image3d<BINARY> &,
		const Neighbourhood &neib);

	template I3D_DLLEXPORT void LocalDilation(
		const Image3d<BINARY> &in, 
		Image3d<BINARY> &out, 
		const Image3d<BINARY> &,
		const Neighbourhood &neib);

	template I3D_DLLEXPORT void LocalDilation(
		const Image3d<size_t> &in, 
		Image3d<size_t> &out, 
		const Image3d<BINARY> &,
		const Neighbourhood &neib);

	template I3D_DLLEXPORT void LocalDilation(
		const Image3d<unsigned short> &in, 
		Image3d<unsigned short> &out, 
		const Image3d<BINARY> &,
		const Neighbourhood &neib);

	//--------------------------------------------------------------------------
	// Erosion
	//--------------------------------------------------------------------------
	template <class T> void Erosion(const Image3d<T> &in, Image3d<T> &out, const Neighbourhood &neib)
	{
		DilEro(in,out,neib,minimum<T>());
	} // Erosion

	//--------------------------------------------------------------------------
	// Explicit instantiations
	//--------------------------------------------------------------------------
	template I3D_DLLEXPORT void Erosion(
		const Image3d<GRAY8> &in, 
		Image3d<GRAY8> &out, 
		const Neighbourhood &neib);

	template I3D_DLLEXPORT void Erosion(
		const Image3d<BINARY> &in, 
		Image3d<BINARY> &out, 
		const Neighbourhood &neib);

	template I3D_DLLEXPORT void Erosion(
		const Image3d<size_t> &in, 
		Image3d<size_t> &out, 
		const Neighbourhood &neib);

	template I3D_DLLEXPORT void Erosion(
		const Image3d<unsigned short> &in, 
		Image3d<unsigned short> &out, 
		const Neighbourhood &neib);

	//--------------------------------------------------------------------------
	// LocalErosion
	//--------------------------------------------------------------------------
	template <class T> void LocalErosion(const Image3d<T> &in, 
													 Image3d<T> &out,
													 const Image3d<BINARY> &mask,
													 const Neighbourhood &neib)
	{
		LocalDilEro(in,out,mask,neib,minimum<T>());
	} // LocalErosion

	//--------------------------------------------------------------------------
	// Explicit instantiations
	//--------------------------------------------------------------------------
	template I3D_DLLEXPORT void LocalErosion(
		const Image3d<GRAY8> &in, 
		Image3d<GRAY8> &out, 
		const Image3d<BINARY> &,
		const Neighbourhood &neib);

	template I3D_DLLEXPORT void LocalErosion(
		const Image3d<BINARY> &in, 
		Image3d<BINARY> &out, 
		const Image3d<BINARY> &,
		const Neighbourhood &neib);

	template I3D_DLLEXPORT void LocalErosion(
		const Image3d<size_t> &in, 
		Image3d<size_t> &out, 
		const Image3d<BINARY> &,
		const Neighbourhood &neib);

	template I3D_DLLEXPORT void LocalErosion(
		const Image3d<unsigned short> &in, 
		Image3d<unsigned short> &out, 
		const Image3d<BINARY> &,
		const Neighbourhood &neib);

	//--------------------------------------------------------------------------
	// Closing
	//--------------------------------------------------------------------------
	template <class T> void Closing(const Image3d<T> &in, Image3d<T> &out, const Neighbourhood &neib)
	{
		Image3d<T> tmp;
		Dilation(in, tmp, neib);
		Erosion(tmp, out, neib);
	} // Closing

	//--------------------------------------------------------------------------
	// Explicit instantiations
	//--------------------------------------------------------------------------
	template I3D_DLLEXPORT void Closing(
		const Image3d<GRAY8> &in, 
		Image3d<GRAY8> &out,
		const Neighbourhood &neib);

	template I3D_DLLEXPORT void Closing(
		const Image3d<GRAY16> &in, 
		Image3d<GRAY16> &out,
		const Neighbourhood &neib);

	template I3D_DLLEXPORT void Closing(
		const Image3d<BINARY> &in, 
		Image3d<BINARY> &out,
		const Neighbourhood &neib);

	//--------------------------------------------------------------------------
	// LocalClosing
	//--------------------------------------------------------------------------
	template <class T> void  LocalClosing(const Image3d<T> &in, 
											  Image3d<T> &out, 
											  const Image3d<BINARY> &mask,
											  const Neighbourhood &neib)
	{
		Image3d<T> tmp;
		LocalDilation(in, tmp, mask, neib);
		LocalErosion(tmp, out, mask, neib);
	} // LocalClosing

	//--------------------------------------------------------------------------
	// Explicit instantiations
	//--------------------------------------------------------------------------
	template I3D_DLLEXPORT void  LocalClosing(
		const Image3d<GRAY8> &in, 
		Image3d<GRAY8> &out,
		const Image3d<BINARY> &,
		const Neighbourhood &neib);

	template I3D_DLLEXPORT void  LocalClosing(
		const Image3d<GRAY16> &in, 
		Image3d<GRAY16> &out,
		const Image3d<BINARY> &,
		const Neighbourhood &neib);

	template I3D_DLLEXPORT void  LocalClosing(
		const Image3d<BINARY> &in, 
		Image3d<BINARY> &out,
		const Image3d<BINARY> &,
		const Neighbourhood &neib);

	//--------------------------------------------------------------------------
	// Closing - Old version (use the version with the output image - Martin Maska 2008-04-06)
	//--------------------------------------------------------------------------
	template <class T> void Closing(Image3d<T> &in, const Neighbourhood &neib)
	{
		Image3d<T> img;
		Dilation(in, img, neib);
		Erosion(img, in, neib);
	} // Closing

	//--------------------------------------------------------------------------
	// Explicit instantiations
	//--------------------------------------------------------------------------
	template I3D_DLLEXPORT void Closing(
		Image3d<GRAY8> &in, 
		const Neighbourhood &neib);

	template I3D_DLLEXPORT void Closing(
		Image3d<GRAY16> &in, 
		const Neighbourhood &neib);

	template I3D_DLLEXPORT void Closing(
		Image3d<BINARY> &in, 
		const Neighbourhood &neib);

	//--------------------------------------------------------------------------
	// Opening
	//--------------------------------------------------------------------------
	template <class T> void Opening(const Image3d<T> &in, Image3d<T> &out, const Neighbourhood &neib)
	{
		Image3d<T> tmp;
		Erosion(in, tmp, neib);
		Dilation(tmp, out, neib);
	} // Opening

	//--------------------------------------------------------------------------
	// Explicit instantiations
	//--------------------------------------------------------------------------
	template I3D_DLLEXPORT void Opening(
		const Image3d<GRAY8> &in, 
		Image3d<GRAY8> &out,
		const Neighbourhood &neib);

	template I3D_DLLEXPORT void Opening(
		const Image3d<GRAY16> &in, 
		Image3d<GRAY16> &out,
		const Neighbourhood &neib);

	template I3D_DLLEXPORT void Opening(
		const Image3d<BINARY> &in, 
		Image3d<BINARY> &out,
		const Neighbourhood &neib);

	//--------------------------------------------------------------------------
	// LocalOpening
	//--------------------------------------------------------------------------
	template <class T> void LocalOpening(const Image3d<T> &in, 
													 Image3d<T> &out, 
													 const Image3d<BINARY> &mask,
													 const Neighbourhood &neib)
	{
		Image3d<T> tmp;
		LocalErosion(in, tmp, mask, neib);
		LocalDilation(tmp, out, mask, neib);
	} //  LocalOpening

	//--------------------------------------------------------------------------
	// Explicit instantiations
	//--------------------------------------------------------------------------
	template I3D_DLLEXPORT void  LocalOpening(
		const Image3d<GRAY8> &in, 
		Image3d<GRAY8> &out,
		const Image3d<BINARY> &,
		const Neighbourhood &neib);

	template I3D_DLLEXPORT void  LocalOpening(
		const Image3d<GRAY16> &in, 
		Image3d<GRAY16> &out,
		const Image3d<BINARY> &,
		const Neighbourhood &neib);

	template I3D_DLLEXPORT void LocalOpening(
		const Image3d<BINARY> &in, 
		Image3d<BINARY> &out,
		const Image3d<BINARY> &,
		const Neighbourhood &neib);

	//--------------------------------------------------------------------------
	// Opening - Old version (use the version with the output image - Martin Maska 2008-04-06)
	//--------------------------------------------------------------------------
	template <class T> void Opening(Image3d<T> &in, const Neighbourhood &neib)
	{
		Image3d<T> img;
		Erosion(in, img, neib);
		Dilation(img, in, neib);
	} // Opening

	//--------------------------------------------------------------------------
	// Explicit instantiations
	//--------------------------------------------------------------------------
	template I3D_DLLEXPORT void Opening(
		Image3d<GRAY8> &in, 
		const Neighbourhood &neib);

	template I3D_DLLEXPORT void Opening(
		Image3d<GRAY16> &in, 
		const Neighbourhood &neib);

	template I3D_DLLEXPORT void Opening(
		Image3d<BINARY> &in, 
		const Neighbourhood &neib);

	//--------------------------------------------------------------------------
	// WhiteTopHat
	//--------------------------------------------------------------------------
	template <class T> void WhiteTopHat(const Image3d<T> &in, Image3d<T> &out, const Neighbourhood &neib)
	{
		Opening(in, out, neib);

		transform(
			in.GetFirstVoxelAddr(),
			in.GetFirstVoxelAddr() + in.GetImageSize(),
			out.GetFirstVoxelAddr(),
			out.GetFirstVoxelAddr(), 
			std::minus<T>());
	} // WhiteTopHat

	//--------------------------------------------------------------------------
	// Explicit instantiations
	//--------------------------------------------------------------------------
	template I3D_DLLEXPORT void WhiteTopHat(
		const Image3d<GRAY8> &in,
		Image3d<GRAY8> &out,
		const Neighbourhood &neib);

	template I3D_DLLEXPORT void WhiteTopHat(
		const Image3d<GRAY16> &in,
		Image3d<GRAY16> &out,
		const Neighbourhood &neib);

	//--------------------------------------------------------------------------
	// BlackTopHat
	//--------------------------------------------------------------------------
	template <class T> void BlackTopHat(const Image3d<T> &in, Image3d<T> &out, const Neighbourhood &neib)
	{
		Closing(in, out, neib);

		transform(
			out.GetFirstVoxelAddr(),
			out.GetFirstVoxelAddr() + out.GetImageSize(),
			in.GetFirstVoxelAddr(),
			out.GetFirstVoxelAddr(), 
			std::minus<T>());
	} // BlackTopHat

	//--------------------------------------------------------------------------
	// Explicit instantiations
	//--------------------------------------------------------------------------
	template I3D_DLLEXPORT void BlackTopHat(
		const Image3d<GRAY8> &in,
		Image3d<GRAY8> &out,
		const Neighbourhood &neib);

	template I3D_DLLEXPORT void BlackTopHat(
		const Image3d<GRAY16> &in,
		Image3d<GRAY16> &out,
		const Neighbourhood &neib);

	//--------------------------------------------------------------------------
	// TopHat - Old version (use WhiteTopHat function - Martin Maska 2008-04-06)
	//--------------------------------------------------------------------------
	template <class T> void TopHat(Image3d<T> &in, const Neighbourhood &neib)
	{
		Image3d<T> tmp;
		Opening(in, tmp, neib);

		transform(
			in.GetFirstVoxelAddr(),
			in.GetFirstVoxelAddr() + in.GetImageSize(),
			tmp.GetFirstVoxelAddr(),
			in.GetFirstVoxelAddr(), 
			std::minus<T>());
	} // TopHat

	//--------------------------------------------------------------------------
	// Explicit instantiations
	//--------------------------------------------------------------------------
	template I3D_DLLEXPORT void TopHat(
		Image3d<GRAY8> &in, 
		const Neighbourhood &neib);

	//--------------------------------------------------------------------------
	// HitOrMiss
	//--------------------------------------------------------------------------
	template <class T> void HitOrMiss(
		const Image3d<T> &in, 
		Image3d<T> &out,
		const Neighbourhood &nb, 
		const Neighbourhood &n_w)
	{
		Neighbourhood n_b(nb);

		Image3d<T> img = in;
		vector<T*> win_b, win_w;
		T *s = out.GetFirstVoxelAddr(); 

		n_b.offset.push_back(Vector3d<int>(0,0,0));  // origin must be in black neib.

		for (size_t i = 0; i < in.GetImageSize(); ++i)
		{ 
			GetWindow(img, img.GetX(i), img.GetY(i), img.GetZ(i), n_b, win_b);    
			GetWindow(img, img.GetX(i), img.GetY(i), img.GetZ(i), n_w, win_w);    

			// test whether any points in neighbourhood don't lie out of image ...
			if ((win_b.size() == n_b.size()) && (win_w.size() == n_w.size()) 
				&& (Correspond(win_b, win_w)))  // ... and correspond colors
			{
				*s = false; 
			}
			else 
			{
				*s = true;         
			}

			++s;
		}
	} // HitOrMiss

	//--------------------------------------------------------------------------
	// Explicit instantiations
	//--------------------------------------------------------------------------
	template I3D_DLLEXPORT void HitOrMiss(
		const Image3d<BINARY> &in, 
		Image3d<BINARY> &out,
		const Neighbourhood &b, 
		const Neighbourhood &w);

	//--------------------------------------------------------------------------
	// Thinning
	//--------------------------------------------------------------------------
	template <class T> void Thinning(
		const Image3d<T> &in, 
		Image3d<T> &out, 
		const Neighbourhood &b1_par,
		const Neighbourhood &b2)
	{
		Neighbourhood b1(b1_par);
		b1.offset.push_back(Vector3d<int>(0,0,0));  // origin must be in neib. b1

		Image3d<T> dil;
		Image3d<T> ero;

		Dilation(in,dil,b1); 
		Erosion(in,ero,b2);

		T *p = dil.GetFirstVoxelAddr();  // dilation
		T *r = ero.GetFirstVoxelAddr();  // erosion
		const T *s = in.GetFirstVoxelAddr();  // input image
		T *t = out.GetFirstVoxelAddr();  // output image  

		for (size_t i = 0; i < in.GetImageSize(); ++i)
		{ 
			if ((*r < *s) && (*s == *p))  
			{
				*t = *r;
			}
			else
			{
				*t = *s; 
			}

			++p;
			++r;
			++s; 
			++t;
		}
	} // Thinning

	//--------------------------------------------------------------------------
	// Explicit instantiations
	//--------------------------------------------------------------------------
	template I3D_DLLEXPORT void Thinning(
		const Image3d<GRAY8> &in, 
		Image3d<GRAY8> &out,
		const Neighbourhood &b, 
		const Neighbourhood &w);

	template I3D_DLLEXPORT void Thinning(
		const Image3d<BINARY> &in, 
		Image3d<BINARY> &out,
		const Neighbourhood &b, 
		const Neighbourhood &w);

	//--------------------------------------------------------------------------
	// Thicking
	//--------------------------------------------------------------------------
	template <class T> void Thicking(
		const Image3d<T> &in, 
		Image3d<T> &out,
		const Neighbourhood &b1_par,
		const Neighbourhood &b2)
	{
		Neighbourhood b1(b1_par);
		b1.offset.push_back(Vector3d<int>(0,0,0));  // origin must be in neib. b1

		Image3d<T> dil;
		Image3d<T> ero;

		Dilation(in,dil,b2);  
		Erosion(in,ero,b1);

		T *p = dil.GetFirstVoxelAddr();  // dilation
		T *r = ero.GetFirstVoxelAddr();  // erosiotion
		const T *s = in.GetFirstVoxelAddr();  // input image
		T *t = out.GetFirstVoxelAddr();  // output image  

		for (size_t i = 0; i < in.GetImageSize(); ++i)
		{ 
			if ((*r == *s) && (*s < *p))
			{
				*t = *p;
			}
			else 
			{
				*t = *s;    
			}

			++p;
			++r;
			++s; 
			++t;
		}
	} // Thicking

	//--------------------------------------------------------------------------
	// Explicit instantiations
	//--------------------------------------------------------------------------
	template I3D_DLLEXPORT void Thicking(
		const Image3d<BINARY> &in,
		Image3d<BINARY> &out,
		const Neighbourhood &b, 
		const Neighbourhood &w);

	template I3D_DLLEXPORT void Thicking(
		const Image3d<GRAY8> &in,
		Image3d<GRAY8> &out,
		const Neighbourhood &b, 
		const Neighbourhood &w);

	//--------------------------------------------------------------------------
	// Effective operations with disk SE
	//--------------------------------------------------------------------------

	I3D_DLLEXPORT void Dilation(const Image3d<i3d::BINARY> &in, 
		Image3d<i3d::BINARY> &out, 
		const float radius)
	{
		i3d::Image3d<float> fimg;
		i3d::BinaryToGray(in, fimg);

		i3d::HybridSaito(fimg, 0, 0.0f);

		i3d::Threshold(fimg, out, 0.0f, radius * radius);
	}

	I3D_DLLEXPORT void Erosion(const Image3d<i3d::BINARY> &in, 
		Image3d<i3d::BINARY> &out, 
		const float radius)
	{
		i3d::Image3d<float> fimg;
		i3d::BinaryToGray(in, fimg);

		i3d::HybridSaito(fimg, 0, 1.0f);

		i3d::Threshold(fimg, out, radius * radius);
	}


	I3D_DLLEXPORT void Closing(const Image3d<i3d::BINARY> &in, 
		Image3d<i3d::BINARY> &out, 
		const float radius)
	{
		i3d::Vector3d<size_t> border_size = i3d::MicronsToPixels(i3d::Vector3d<float>(radius,radius,radius), 
			in.GetResolution());

		out = in;

		out.PadImage(border_size);

		i3d::Image3d<float> fimg;

		i3d::BinaryToGray(out, fimg);

		i3d::HybridSaito(fimg, 0, 0.0f);
		i3d::Threshold(fimg, 0.0f, radius * radius, 1.0f, 0.0f);
		i3d::HybridSaito(fimg, 0, 0.0f);

		i3d::Threshold(fimg, out, radius * radius);

		out.RemovePadding(border_size);
	}

	I3D_DLLEXPORT void Opening(const Image3d<i3d::BINARY> &in, 
		Image3d<i3d::BINARY> &out, 
		const float radius)
	{
		i3d::Vector3d<size_t> border_size = i3d::MicronsToPixels(i3d::Vector3d<float>(radius,radius,radius), 
			in.GetResolution());

		out = in;

		out.PadImage(border_size, BINARY(1));

		i3d::Image3d<float> fimg;

		i3d::BinaryToGray(out, fimg);

		i3d::HybridSaito(fimg, 0, 1.0f);
		i3d::Threshold(fimg, 0.0f, radius * radius, 0.0f, 1.0f);
		i3d::HybridSaito(fimg, 0, 1.0f);

		i3d::Threshold(fimg, out, 0.0f, radius * radius);

		out.RemovePadding(border_size);
	}

	//--------------------------------------------------------------------------
	// Geodesic_dilation
	//--------------------------------------------------------------------------
	template <class T> void Geodesic_dilation(
		const Image3d<T> &marker, 
		const Image3d<T> &mask,
		Image3d<T> &out,
		const Neighbourhood &neib)
	{    
		Dilation(marker,out,neib);  // dilation of marker

		const T *s = mask.GetFirstVoxelAddr();  // mask image
		T *t = out.GetFirstVoxelAddr();  // output image  

		for (size_t i = 0; i < mask.GetImageSize(); ++i)
		{
			*t = std::min(*t, *s);
			++s;
			++t;
		}
	} // Geodesic_dilation

	//--------------------------------------------------------------------------
	// Explicit instantiations
	//--------------------------------------------------------------------------
	template I3D_DLLEXPORT void Geodesic_dilation(
		const Image3d<GRAY8> &marker, 
		const Image3d<GRAY8> &mask,
		Image3d<GRAY8> &out, 
		const Neighbourhood &neib);

	//--------------------------------------------------------------------------
	// Geodesic_erosion
	//--------------------------------------------------------------------------
	template <class T> void Geodesic_erosion(
		const Image3d<T> &marker, 
		const Image3d<T> &mask,
		Image3d<T> &out, 
		const Neighbourhood &neib)
	{    
		Erosion(marker,out,neib);  // erosion of marker

		const T *s = mask.GetFirstVoxelAddr();
		T *t = out.GetFirstVoxelAddr();

		for (size_t i = 0; i < mask.GetImageSize(); ++i)
		{
			*t = std::max(*t, *s);
			++s;
			++t;
		}
	} // Geodesic_erosion

	//--------------------------------------------------------------------------
	// Explicit instantiations
	//--------------------------------------------------------------------------
	template I3D_DLLEXPORT void Geodesic_erosion(
		const Image3d<GRAY8> &marker, 
		const Image3d<GRAY8> &mask,
		Image3d<GRAY8> &out, 
		const Neighbourhood &neib);

	//--------------------------------------------------------------------------
	// h_Max
	//--------------------------------------------------------------------------
	template <class T> void h_Max(const Image3d<T> &in, T h, Image3d<T> &out)
	{
		i3d::Image3d<T> marker;
		marker.CopyMetaData(in);
		
		const T *src_px = in.GetFirstVoxelAddr();
		T *px = marker.GetFirstVoxelAddr();
		
		size_t num = in.GetImageSize();
		for (size_t i = 0; i < num; i++) 
		{
			*px = (h < *src_px)? *src_px - h : 0;
			src_px++;
			px++;
		}

		Reconstruction_by_dilation(marker, in, out);
	} // h_Max

	//--------------------------------------------------------------------------
	// Explicit instantiations
	//--------------------------------------------------------------------------
	template I3D_DLLEXPORT void h_Max(
		const Image3d<GRAY8> &in, 
		GRAY8 h, 
		Image3d<GRAY8> &out);

	template I3D_DLLEXPORT void h_Max(
		const Image3d<GRAY16> &in, 
		GRAY16 h, 
		Image3d<GRAY16> &out);

	template I3D_DLLEXPORT void h_Max(
		const Image3d<float> &in, 
		float h, 
		Image3d<float> &out);

	//--------------------------------------------------------------------------
	// h_Convex
	//--------------------------------------------------------------------------
	template <class T> void h_Convex(const Image3d<T> &in, T h, Image3d<T> &out)
	{
		h_Max(in, h, out);

		const T *in_ptr = in.GetFirstVoxelAddr();
		const T *in_last = in.GetFirstVoxelAddr() + in.GetImageSize();
		T *out_ptr = out.GetFirstVoxelAddr();

		while (in_ptr != in_last)
		{
			*out_ptr = *in_ptr - *out_ptr;
			in_ptr++;
			out_ptr++;
		}
	} // h_Convex

	//--------------------------------------------------------------------------
	// Explicit instantiations
	//--------------------------------------------------------------------------
	template I3D_DLLEXPORT void h_Convex(
		const Image3d<GRAY8> &in, 
		GRAY8 h, 
		Image3d<GRAY8> &out);

	template I3D_DLLEXPORT void h_Convex(
		const Image3d<GRAY16> &in, 
		GRAY16 h, 
		Image3d<GRAY16> &out);

	template I3D_DLLEXPORT void h_Convex(
		const Image3d<float> &in, 
		float h, 
		Image3d<float> &out);

	//--------------------------------------------------------------------------
	// r_Max_bysweeping
	//--------------------------------------------------------------------------
	template <class T> void r_Max_bysweeping(
		const Image3d<T> &in, 
		Image3d<T> &out)
	{
		out = in;

		size_t dx = in.GetSizeX();
		size_t dy = in.GetSizeY();
		size_t dz = in.GetSizeZ();

		T *out_ptr;
		const T *in_ptr1;
		const T *in_ptr2;

		bool change = true;

		while (change) {
			change = false;

			// sweep x
			for (size_t z = 0; z < dz; ++z) {
				for (size_t y = 0; y < dy; ++y) {
					// forward
					in_ptr1 = in.GetVoxelAddr(0, y, z);
					in_ptr2 = in_ptr1 + 1;
					out_ptr = out.GetVoxelAddr(1, y, z);
					
					for (size_t x = 1; x < dx; ++x) {
						if (*out_ptr != (T)0) {
							if (*in_ptr1 > *in_ptr2 
								|| (*in_ptr1 == *in_ptr2 && *(out_ptr - 1) == (T)0)) {
								change = true;
								*out_ptr = (T)0;
							} 
						}
						out_ptr++; in_ptr1++; in_ptr2++; 
					}

					// backward
					in_ptr1--;
					out_ptr-= 2;
					in_ptr2--;

					for (size_t x = 1; x < dx; ++x) {
						if (*out_ptr != (T)0) {
							if (*in_ptr2 > *in_ptr1
								|| (*in_ptr2 == *in_ptr1 && *(out_ptr + 1) == (T)0)) {
							change = true;
							*out_ptr = (T)0;
							}
						}
						out_ptr--; in_ptr1--; in_ptr2--; 
					}
				}
			}		

			// sweep y
			for (size_t z = 0; z < dz; ++z) {
				for (size_t x = 0; x < dx; ++x) {
					// forward
					in_ptr1 = in.GetVoxelAddr(x, 0, z);
					in_ptr2 = in_ptr1 + dx;
					out_ptr = out.GetVoxelAddr(x, 1, z);
					
					for (size_t y = 1; y < dy; ++y) {
						if (*out_ptr != (T)0) {
							if (*in_ptr1 > *in_ptr2 
								|| (*in_ptr1 == *in_ptr2 && *(out_ptr - dx) == (T)0)) {
								change = true;
								*out_ptr = (T)0;
							} 
						}
						out_ptr += dx; in_ptr1 += dx; in_ptr2 += dx; 
					}

					// backward
					in_ptr1 -= dx;
					out_ptr -= 2*dx;
					in_ptr2 -= dx;

					for (size_t y = 1; y < dy; ++y) {
						if (*out_ptr != (T)0) {
							if (*in_ptr2 > *in_ptr1
								|| (*in_ptr2 == *in_ptr1 && *(out_ptr + dx) == (T)0)) {
							change = true;
							*out_ptr = (T)0;
							}
						}
						out_ptr -= dx; in_ptr1 -= dx; in_ptr2 -= dx; 
					}
				}
			}
					
			// sweep z
			for (size_t x = 0; x < dx; ++x) {
				for (size_t y = 0; y < dy; ++y) {
					// forward
					in_ptr1 = in.GetVoxelAddr(x, y, 0);
					in_ptr2 = in_ptr1 + dx * dy;
					out_ptr = out.GetVoxelAddr(x, y, 1);
					
					for (size_t z = 1; z < dz; ++z) {
						if (*out_ptr != (T)0) {
							if (*in_ptr1 > *in_ptr2 
								|| (*in_ptr1 == *in_ptr2 && *(out_ptr - dx * dy) == (T)0)) {
								change = true;
								*out_ptr = (T)0;
							} 
						}
						out_ptr += dx * dy; in_ptr1 += dx * dy; in_ptr2 += dx * dy; 
					}

					// backward
					in_ptr1 -= dx * dy;
					out_ptr -= 2*dx*dy;
					in_ptr2 -= dx * dy;

					for (size_t z = 1; z < dz; ++z) {
						if (*out_ptr != (T)0) {
							if (*in_ptr2 > *in_ptr1
								|| (*in_ptr2 == *in_ptr1 && *(out_ptr + dx * dy) == (T)0)) {
							change = true;
							*out_ptr = (T)0;
							}
						}
						out_ptr -= dx * dy; in_ptr1 -= dx * dy; in_ptr2 -= dx * dy; 
					}
				}
			}
		}

		out_ptr = out.GetFirstVoxelAddr();
		for (size_t i = 0; i < out.GetImageSize(); ++i) {
			if (*out_ptr != (T)0) {
				*out_ptr = std::numeric_limits<T>::max();
			}
			out_ptr++;
		}
	} // r_Max_bysweeping

	// explicit instantiations
	template I3D_DLLEXPORT void r_Max_bysweeping(const Image3d<GRAY8> &in, Image3d<GRAY8> &out);
	template I3D_DLLEXPORT void r_Max_bysweeping(const Image3d<GRAY16> &in, Image3d<GRAY16> &out);
	template I3D_DLLEXPORT void r_Max_bysweeping(const Image3d<size_t> &in, Image3d<size_t> &out);

	//--------------------------------------------------------------------------
	// r_Extrema
	//--------------------------------------------------------------------------
	// computes regional extrema based on Breen And Jones article "Attribute Openings Thinnings, and Granulometries"
	template <typename T, typename compFuncType> static 
	void r_Extrema(const Image3d<T> &in, Image3d<T> &out, const Neighbourhood &neib, const T value)
	{
		compFuncType compFunc;
		const T *inPtr;
		T *outPtr, *tmpPtr;
		size_t i, j, k, l, num;
		vector<const T*> win;
		bool FFill;

		out = in;

		i = 0;
		outPtr = out.GetFirstVoxelAddr();
		inPtr = in.GetFirstVoxelAddr();
		tmpPtr = out.GetFirstVoxelAddr();

		num = in.GetImageSize();

		while (i < num) // for all pixels
		{
			j =  GetWindow(in, in.GetX(i), in.GetY(i), in.GetZ(i), neib, win);
			for (k = 0; k < j; ++k) 
			{
				if ((*outPtr) != value)
				{
					FFill = false;
					for (l = 0; l < win.size(); ++l) // for all pixels from neighbourhood
					{
						if (compFunc(*win[l], *inPtr)) // Flood Fill
						{
							FFill = true;
							break;
						}
					}
					if (FFill)
						FloodFill(out, value, out.GetX(outPtr - tmpPtr), out.GetY(outPtr - tmpPtr), 
						out.GetZ(outPtr - tmpPtr), neib);
				}
				MoveWindow(win);
				++outPtr;
				++inPtr;
			}
			i += j;
		}

		//Copy input to output
		outPtr =  out.GetFirstVoxelAddr();
		tmpPtr = out.GetVoxelAddr(out.GetImageSize());
		while (outPtr != tmpPtr)
		{
			*outPtr = (compFunc(*outPtr, value) ? std::numeric_limits<T>::max() : T(0));
			++outPtr; 
		}
	} // r_Extrema

	//--------------------------------------------------------------------------
	// r_Max
	//--------------------------------------------------------------------------
	template <class T> void r_Max(
		const Image3d<T> &in, 
		Image3d<T> &out, 
		const Neighbourhood &neib, 
		const T minVal)
	{
		if (neib == nb2D_4 || neib == nb3D_6)
		{
			r_Max_bysweeping(in, out);
			return;
		}

		r_Extrema<T, std::greater<T> >(in, out, neib, minVal);
	} // r_Max

	// explicit instantiations
	template I3D_DLLEXPORT void r_Max(const Image3d<GRAY8> &in, Image3d<GRAY8> &out, const Neighbourhood &neib, const GRAY8 minVal);
	template I3D_DLLEXPORT void r_Max(const Image3d<GRAY16> &in, Image3d<GRAY16> &out, const Neighbourhood &neib, const GRAY16 minVal);
	template I3D_DLLEXPORT void r_Max(const Image3d<size_t> &in, Image3d<size_t> &out, const Neighbourhood &neib, const size_t minVal);

	//--------------------------------------------------------------------------
	// r_Min
	//--------------------------------------------------------------------------
	template <typename T>
	void r_Min(const Image3d<T> &in, Image3d<T> &out, const Neighbourhood &neib)
	{
		r_Extrema<T, std::less<T> >(in, out, neib, std::numeric_limits<T>::max());
	} // r_Min

	// explicit instantiations
	template I3D_DLLEXPORT void r_Min(const Image3d<GRAY8> &in, Image3d<GRAY8> &out, const Neighbourhood &neib);
	template I3D_DLLEXPORT void r_Min(const Image3d<GRAY16> &in, Image3d<GRAY16> &out, const Neighbourhood &neib);
	template I3D_DLLEXPORT void r_Min(const Image3d<size_t> &in, Image3d<size_t> &out, const Neighbourhood &neib);
	template I3D_DLLEXPORT void r_Min(const Image3d<float> &in, Image3d<float> &out, const Neighbourhood &neib);

	//--------------------------------------------------------------------------
	// impose_minima
	//--------------------------------------------------------------------------
	template <class T> void impose_minima(Image3d<T> &in_out, const Image3d<BINARY> &mask_bin)
	{
		Image3d<T> marker;
		Image3d<T> mask;

		mask.MakeRoom(in_out.GetSize());
		marker.MakeRoom(in_out.GetSize());

		for (size_t i = 0; i < in_out.GetImageSize(); ++i) {
			if (mask_bin.GetVoxel(i)) {
				mask.SetVoxel(i, 0);
				marker.SetVoxel(i, 1);
			} else {
				mask.SetVoxel(i, in_out.GetVoxel(i) + 1);
				marker.SetVoxel(i, std::numeric_limits<T>::max());
			}
		}

		Reconstruction_by_erosion(marker, mask, in_out);
	} 

	//--------------------------------------------------------------------------
	// Explicit instantiations
	//--------------------------------------------------------------------------
	template I3D_DLLEXPORT void impose_minima(Image3d<GRAY8> &in_out, const Image3d<BINARY> &mask);
	template I3D_DLLEXPORT void impose_minima(Image3d<GRAY16> &in_out, const Image3d<BINARY> &mask);
	template I3D_DLLEXPORT void impose_minima(Image3d<float> &in_out, const Image3d<BINARY> &mask);

	//--------------------------------------------------------------------------
	// e_Max
	//--------------------------------------------------------------------------
	template <class T> void e_Max(const Image3d<T> &in, T h, Image3d<T> &out, const Neighbourhood &neib)
	{
		Image3d<T> tmp;
		//	img.SaveImage("debug_in.tif");
		//	cout << "computing HMAX\n";
		h_Max(in, h, tmp);
		//	tmp.SaveImage("debug_tmp.tif");
		//	cout << "computing RMAX\n";
		r_Max(tmp, out, neib);
		//	out.SaveImage("debug_out.tif");
	} // e_Max

	//--------------------------------------------------------------------------
	// Explicit instantiations
	//--------------------------------------------------------------------------
	template I3D_DLLEXPORT void e_Max(
		const Image3d<GRAY8> &in, 
		GRAY8 h, 
		Image3d<GRAY8> &out, 
		const Neighbourhood &neib);

	template I3D_DLLEXPORT void e_Max(
		const Image3d<GRAY16> &in, 
		GRAY16 h, 
		Image3d<GRAY16> &out, 
		const Neighbourhood &neib);

	template I3D_DLLEXPORT void e_Max(
		const Image3d<size_t> &in, 
		size_t h, 
		Image3d<size_t> &out, 
		const Neighbourhood &neib);

	//--------------------------------------------------------------------------
	// Fillhole
	//--------------------------------------------------------------------------
	template <class T> void Fillhole(const Image3d<T> &in, Image3d<T> &out)
	{
		size_t sx = in.GetSizeX();
		size_t sy = in.GetSizeY();
		size_t sz = in.GetSizeZ();
		size_t slice = sx * sy;

		T min, max;
		in.GetRange(min, max);

		i3d::Image3d<T> marker;
		marker.CopyMetaData(in);

		const T *in_ptr = in.GetFirstVoxelAddr();
		T *marker_ptr = marker.GetFirstVoxelAddr();

		// create marker image - copy input maximal intensity anywhere except image border
		if (sz > 1)
		{
			// 3D input image

			// copy the first slice from the input image
			for (size_t i = 0; i < slice; i++)
			{
				*marker_ptr = *in_ptr;
				marker_ptr++;
				in_ptr++;
			}

			for (size_t z = 1; z < sz - 1; z++)
			{
				// copy the first row of each slice from the input image
				for (size_t x = 0; x < sx; x++)
				{
					*marker_ptr = *in_ptr;
					marker_ptr++;
					in_ptr++;
				}

				for (size_t y = 1; y < sy - 1; y++)
				{
					// copy the first voxel of each row of each slice from the input image
					*marker_ptr = *in_ptr;
					marker_ptr++;
					in_ptr++;

					for (size_t x = 1; x < sx - 1; x++)
					{
						*marker_ptr = max;
						marker_ptr++;
						in_ptr++;
					}

					// copy last voxel of each row of each slice from the input image
					*marker_ptr = *in_ptr;
					marker_ptr++;
					in_ptr++;
				}
			
				// copy last row of each slice from the input image
				for (size_t x = 0; x < sx; x++)
				{
					*marker_ptr = *in_ptr;
					marker_ptr++;
					in_ptr++;
				}
			}

			// copy last slice from the input image
			for (size_t i = 0; i < slice; i++)
			{
				*marker_ptr = *in_ptr;
				marker_ptr++;
				in_ptr++;
			}
		}
		else
		{
			// 2D input image
			
			// copy the first row from the input image
			for (size_t x = 0; x < sx; x++)
			{
				*marker_ptr = *in_ptr;
				marker_ptr++;
				in_ptr++;
			}

			for (size_t y = 1; y < sy - 1; y++)
			{
				// copy the first voxel of each row from the input image
				*marker_ptr = *in_ptr;
				marker_ptr++;
				in_ptr++;

				for (size_t x = 1; x < sx - 1; x++)
				{
					*marker_ptr = max;
					marker_ptr++;
					in_ptr++;
				}

				// copy last voxel of each row from the input image
				*marker_ptr = *in_ptr;
				marker_ptr++;
				in_ptr++;
			}

			// copy last row from the input image
			for (size_t x = 0; x < sx; x++)
			{
				*marker_ptr = *in_ptr;
				marker_ptr++;
				in_ptr++;
			}
		}

		i3d::Reconstruction_by_DilEro(marker, in, out, minimum<T>(), maximum<T>());
		
	} // Fillhole

	//--------------------------------------------------------------------------
	// Explicit instantiations
	//--------------------------------------------------------------------------
	template I3D_DLLEXPORT void Fillhole(
		const Image3d<GRAY8> &in, 
		Image3d<GRAY8> &out);
		
	template I3D_DLLEXPORT void Fillhole(
		const Image3d<GRAY16> &in, 
		Image3d<GRAY16> &out);

	//--------------------------------------------------------------------------
	// Reconstruction_by_DilEro
	//--------------------------------------------------------------------------
	template <class T, class F, class G> inline void Reconstruction_by_DilEro(
		const Image3d<T> &marker, 
		const Image3d<T> &mask, 
		Image3d<T> &out, 
		const Neighbourhood &neib, 
		F func_f, 
		G func_g)
	{
		bool change = true;  // for detecting stability
		vector<T*> win;
		Neighbourhood n_plus;  // backward neighbourhood
		Neighbourhood n_minus;  // forward neighbourhood

		Split(neib, n_plus, n_minus);  // dividing neighbourhood
		n_plus.offset.push_back(Vector3d<int>(0,0,0));
		n_minus.offset.push_back(Vector3d<int>(0,0,0));  // origin must be in both neib.

		out = marker;  // all controls and results should be done in output image
		const T *s = mask.GetFirstVoxelAddr();
		T *t = out.GetFirstVoxelAddr();

		while (change)  // repeat until stability
		{
			change = false;
			size_t i = 0;
			T old;  // old value for detecting stability

			// Forward scan
			while (i < mask.GetImageSize())
			{ 
				size_t num = GetWindow(out,out.GetX(i),out.GetY(i),out.GetZ(i),n_minus,win);

				for (size_t j = 0; j < num; ++j)               
				{
					old = *t;    

					for (size_t k = 0; k < win.size(); ++k)  *t = func_f( *t, *win[k] );             
					*t = func_g( *t, *s);    

					if (old!=*t) change=true;

					MoveWindow(win);
					++s;  // moving mask image pointer
					++t;  // moving output image pointer
				}     
				i += num;     
			}

			// Backward scan
			t = out.GetFirstVoxelAddr()+out.GetImageSize()-1; // pointer to last pixel
			s = mask.GetFirstVoxelAddr()+mask.GetImageSize()-1;
			for (i=out.GetImageSize()-1; i>=0; --i)
			{ 
				GetWindow(out, out.GetX(i), out.GetY(i), out.GetZ(i), n_plus, win);

				old = *t;

				for (size_t k = 0; k < win.size(); ++k)  *t = func_f( *t, *win[k] );         
				*t = func_g( *t, *s);

				if (old!=*t) change=true;      

				--s;   
				--t;
			}   
		}
	} // Reconstruction_by_DilEro

	template <class T, class F, class G> inline void Reconstruction_by_DilEro(
		const Image3d<T> &marker, 
		const Image3d<T> &mask, 
		Image3d<T> &out, 
		F func_f, 
		G func_g)
	{
		bool change = true;  // for stability detection

		out = marker;  // the whole operation is done in the output image

		size_t sx = out.GetSizeX();
		size_t sy = out.GetSizeY();
		size_t sz = out.GetSizeZ();
		size_t last = sx*sy*sz - 1;

		while (change)  // repeat until stability
		{
			change = false;
			change |= Rec1DForward(mask, out,  1,     0,   sx - 1, sy,     sz,      1, 0,   func_f, func_g);
			change |= Rec1DForward(mask, out,  sx,    0,   sx,     sy - 1, sz,      0, sx,  func_f, func_g);
			change |= Rec1DForward(mask, out,  sx*sy, 0,   sx,     sy,     sz - 1,  0, 0,   func_f, func_g);

			change |= Rec1DBackward(mask, out,  last-1,     last,  sx - 1, sy,     sz,      1, 0,   func_f, func_g);
			change |= Rec1DBackward(mask, out,  last-sx,    last,  sx,     sy - 1, sz,      0, sx,  func_f, func_g);
			change |= Rec1DBackward(mask, out,  last-sx*sy, last,  sx,     sy,     sz - 1,  0, 0,   func_f, func_g);
		}
	} // Reconstruction_by_DilEro

	//--------------------------------------------------------------------------
	// Explicit instantiations
	//--------------------------------------------------------------------------
	template I3D_DLLEXPORT void Reconstruction_by_DilEro(
		const Image3d<GRAY8> &marker,
		const Image3d<GRAY8> &mask,
		Image3d<GRAY8> &out, 
		const Neighbourhood &neib,
		minimum<GRAY8> func_f, 
		maximum<GRAY8> func_g);

	template I3D_DLLEXPORT void Reconstruction_by_DilEro(
		const Image3d<BINARY> &marker,
		const Image3d<BINARY> &mask, 
		Image3d<BINARY> &out, 
		const Neighbourhood &neib,
		maximum<BINARY> func_f, 
		minimum<BINARY> func_g);

	template I3D_DLLEXPORT void Reconstruction_by_DilEro(
		const Image3d<GRAY8> &marker,
		const Image3d<GRAY8> &mask, 
		Image3d<GRAY8> &out, 
		maximum<GRAY8> func_f,
		minimum<GRAY8> func_g);

	template I3D_DLLEXPORT void Reconstruction_by_DilEro(
		const Image3d<float> &marker,
		const Image3d<float> &mask, 
		Image3d<float> &out, 
		maximum<float> func_f,
		minimum<float> func_g);
	//--------------------------------------------------------------------------
	// Reconstruction_by_dilation
	//--------------------------------------------------------------------------
	template <class T> void Reconstruction_by_dilation(
		const Image3d<T> &marker,
		const Image3d<T> &mask, 
		Image3d<T> &out, 
		const Neighbourhood &neib)
	{ 
		Reconstruction_by_DilEro(marker,mask,out,neib,maximum<T>(),minimum<T>());
	} // Reconstruction_by_dilation                                

	template <class T> void Reconstruction_by_dilation(
		const Image3d<T> &marker, 
		const Image3d<T> &mask, 
		Image3d<T> &out)
	{ 
		Reconstruction_by_DilEro(marker,mask,out,maximum<T>(),minimum<T>());
	} // Reconstruction_by_dilation

	//--------------------------------------------------------------------------
	// Explicit instantiations
	//--------------------------------------------------------------------------

	template I3D_DLLEXPORT void Reconstruction_by_dilation(
		const Image3d<float> &marker, 
		const Image3d<float> &mask, 
		Image3d<float> &out);

	template I3D_DLLEXPORT void Reconstruction_by_dilation(
		const Image3d<BINARY> &marker, 
		const Image3d<BINARY> &mask, 
		Image3d<BINARY> &out);


	//--------------------------------------------------------------------------
	// Reconstruction_by_erosion
	//--------------------------------------------------------------------------
	template <class T> void Reconstruction_by_erosion(
		const Image3d<T> &marker, 
		const Image3d<T> &mask, 
		Image3d<T> &out, 
		const Neighbourhood &neib)
	{ 
		Reconstruction_by_DilEro(marker,mask,out,neib,minimum<T>(),maximum<T>());
	} // Reconstruction_by_erosion

	template <class T> void Reconstruction_by_erosion(
		const Image3d<T> &marker, 
		const Image3d<T> &mask, 
		Image3d<T> &out)
	{ 
		Reconstruction_by_DilEro(marker,mask,out,minimum<T>(),maximum<T>());
	} // Reconstruction_by_erosion

	//--------------------------------------------------------------------------
	// Explicit instantiations
	//--------------------------------------------------------------------------
	template I3D_DLLEXPORT void Reconstruction_by_erosion(
		const Image3d<float> &marker, 
		const Image3d<float> &mask, 
		Image3d<float> &out);

	/**************************************************************************\
	*
	*				  Mathematical morphology related functions
	*
	\**************************************************************************/
	//--------------------------------------------------------------------------
	// DilEro
	//--------------------------------------------------------------------------
	template <class T, class F> inline void DilEro(
		const Image3d<T> &in, 
		Image3d<T> &out, 
		const Neighbourhood &neib, 
		F function)
	{ 
		size_t i,j;
		vector<const T*> win;

		out.CopyMetaData(in);

		T* p = out.GetFirstVoxelAddr();

		for (j=0, i=0; i < in.GetImageSize(); ++i, --j, ++p)
		{
			if (j == 0)
				j = GetWindow(in, in.GetX(i), in.GetY(i), in.GetZ(i), neib, win);
			else
				MoveWindow(win);

			*p = *win[0];
			for (size_t k = 1; k < win.size(); ++k)
			{
				*p = function( *p, *win[k] );
			}
		}
	} // DilEro
	//--------------------------------------------------------------------------
	// LocalDilEro - DirEro applied to selected area (defined by mask)
	//--------------------------------------------------------------------------
	template <class T, class F> inline void LocalDilEro(
		const Image3d<T> &in,  // input image
		Image3d<T> &out,  // output image
		const Image3d<BINARY> &mask, // mask defining the region where 
		// the DilEro shall be applied
		const Neighbourhood &neib, // neighbourhood definition
		F function) 
	{ 
		size_t i,j;
		Vector3d<size_t> pos;
		vector<const T*> win;
		vector<const BINARY*> bwin;

		// Copy the input image into the output one. The voxels lying within
		// the mask regions will be change while the others should remain 
		// unchanged.
		out = in;

		T* p = out.GetFirstVoxelAddr();

		for (j=0, i=0; i < in.GetImageSize(); ++i, --j, ++p)
		{
			// -- Window shifts --
			if (j == 0)
			{
				 pos = in.GetPos(i);
				 j = GetWindow(in, pos.x, pos.y, pos.z, neib, win);
				 GetWindow(mask, pos.x, pos.y, pos.z, neib, bwin);
			}
			else
			{
				MoveWindow(win);
				MoveWindow(bwin);
			}

			// -- Memory buffer update --
			// Change only the voxels whose position belongs to mask image.
			if (mask.GetVoxel(i) == BINARY(1))
			{
				 // Before the value of the neighbourhood voxel is taken
				 // into account, a binary mask check is performed.
				*p = (*bwin[0] == i3d::BINARY(1)) ? *win[0] : T(0);

				 for (size_t k = 1; k < win.size(); ++k)
				 {
					 *p = function( *p, (*bwin[k] == i3d::BINARY(1)) ? *win[k] : T(0));
				 }
			}
		}
	} // DilEro

	//--------------------------------------------------------------------------
	// Explicit instantiations
	//--------------------------------------------------------------------------
	/*template I3D_DLLEXPORT void DilEro(
	const Image3d<BINARY> &in, 
	Image3d<BINARY> &out, 
	const Neighbourhood &neib, 
	minimum<BINARY> function);

	template I3D_DLLEXPORT void DilEro(
	const Image3d<BINARY> &in, 
	Image3d<BINARY> &out, 
	const Neighbourhood &neib, 
	maximum<BINARY> function);

	template I3D_DLLEXPORT void DilEro(
	const Image3d<GRAY8> &in, 
	Image3d<GRAY8> &out, 
	const Neighbourhood &neib, 
	minimum<GRAY8> function);

	template I3D_DLLEXPORT void DilEro(
	const Image3d<GRAY8> &in,
	Image3d<GRAY8> &out, 
	const Neighbourhood &neib,
	maximum<GRAY8> function);

	template I3D_DLLEXPORT void DilEro(
	const Image3d<size_t> &in,
	Image3d<size_t> &out, 
	const Neighbourhood &neib,
	minimum<size_t> function);

	template I3D_DLLEXPORT void DilEro(
	const Image3d<size_t> &in, 
	Image3d<size_t> &out, 
	const Neighbourhood &neib, 
	maximum<size_t> function);*/

	//--------------------------------------------------------------------------
	// DiscreteDistance
	//--------------------------------------------------------------------------
	template <class T1, class T2, class Pred> void DiscreteDistance(
		const Image3d<T1> &in, 
		Image3d<T2> &out, 
		const Neighbourhood &neib, 
		Pred p)
	{
		Neighbourhood n_plus;  // backward neighbourhood
		Neighbourhood n_minus;  // forward neighbourhood

		Split(neib, n_plus, n_minus);

		out.CopyMetaData(in);

		// Forward scan of all input image pixels with backward neighbourhood
		size_t i = 0;
		vector<T2*> win;
		const T1 *r = in.GetFirstVoxelAddr();
		T2 *s = out.GetFirstVoxelAddr();

		while (i < in.GetImageSize())
		{ 
			size_t num = GetWindow(out,out.GetX(i),out.GetY(i),out.GetZ(i),n_minus,win);

			for (size_t j = 0; j < num; ++j)
			{       
				if (p(*r))
				{ 
					if (win.size()==0) *s = 0;
					else *s = numeric_limits<T2>::max();

					for (size_t k = 0; k < win.size(); ++k) 
						*s = std::min( *s, *win[k] );

					*s += 1; // *s is (minimal of n_minus neighbourhood) + 1

				} 
				else *s = 0;      

				MoveWindow(win);
				++r;  // moving input image pointer
				++s;  // moving output image pointer
			}
			i += num;    
		}

		// Backward scan of all input image pixels with forward neighbourhood
		s = out.GetFirstVoxelAddr()+out.GetImageSize()-1;  
		// s is pointer on last pixel

		for (long j=out.GetImageSize()-1; j>=0; --j)
		{ 
			if (*s!=0)
			{ 
				GetWindow(out, out.GetX(j), out.GetY(j), out.GetZ(j), n_plus, win);

				T2 t;
				if (win.size()==0) t = 0;
				else t = numeric_limits<T2>::max();

				for (size_t k = 0; k < win.size(); ++k) 
					t = std::min( t, *win[k] );

				t += 1;  // t is (minimal of n_plus neighbourhood) + 1

				*s = std::min( *s, t );
			}

			--s;  // moving output image pointer    
		}
	} // DiscreteDistance

	//--------------------------------------------------------------------------
	// Explicit instantiations
	//--------------------------------------------------------------------------
	template I3D_DLLEXPORT class less_than<BINARY>;
	template I3D_DLLEXPORT class less_than<GRAY8>;
	template I3D_DLLEXPORT class greater_than<BINARY>;
	template I3D_DLLEXPORT class greater_than<GRAY8>;

	template I3D_DLLEXPORT void DiscreteDistance(
		const Image3d<BINARY> &in, 
		Image3d<GRAY8> &out,
		const Neighbourhood &neib, 
		less_than<BINARY> p);

	template I3D_DLLEXPORT void DiscreteDistance(
		const Image3d<GRAY8> &in, 
		Image3d<GRAY8> &out,
		const Neighbourhood &neib, 
		less_than<GRAY8> p);

	template I3D_DLLEXPORT void DiscreteDistance(
		const Image3d<BINARY> &in, 
		Image3d<GRAY8> &out,
		const Neighbourhood &neib, 
		greater_than<BINARY> p);

	template I3D_DLLEXPORT void DiscreteDistance(
		const Image3d<GRAY8> &in, 
		Image3d<GRAY8> &out,
		const Neighbourhood &neib, 
		greater_than<GRAY8> p);

	template I3D_DLLEXPORT void DiscreteDistance(
		const Image3d<BINARY> &in, 
		Image3d<GRAY16> &out,
		const Neighbourhood &neib,
		less_than<BINARY> p);

	/**************************************************************************\
	*
	*						  Helper functions
	*
	\**************************************************************************/
	//--------------------------------------------------------------------------
	// Split
	//--------------------------------------------------------------------------
	void Split(const Neighbourhood &n, Neighbourhood &n_back, Neighbourhood &n_forw)
	{
		VectContainer::const_iterator l;

		for (l=n.offset.begin(); l!=n.offset.end(); ++l)   
		{
			if ((l->z<0) || ((l->z==0)&&(l->y<0)) || ((l->z==0)&&(l->y==0)&&(l->x<0))) 
			{
				if (*l != Vector3d<int>(0,0,0))
					n_forw.offset.push_back(*l); 
			}
			else 
			{
				n_back.offset.push_back(*l);
			}

			/// n_forw.offset.erase(Vector3d<int>(0,0,0));
		}
	} // Split

	//--------------------------------------------------------------------------
	// Correspond
	//--------------------------------------------------------------------------
	template <class T> bool Correspond(vector<T*> &b, vector<T*> &w)
	{   
		for (size_t i = 0; i < b.size(); ++i) // b must be black
			if (*b[i]) return (false);

		for (size_t j = 0; j < b.size(); ++j) // w must not contain any black
			if (*w[j]==0) return (false); 

		// Old code ...
#		if 0
		for (size_t i = 0; i < b.size(); ++i)
			if (*b[i]==1) return (false);

		for (size_t j = 0; j < b.size(); ++j)
			if (*w[j]==0) return (false); 
#		endif

		return (true);
	}

	/**************************************************************************\
	*
	*						  Basic morphological operations with a disk approximated by octagon
	*							Effective implementation using SE decomposition 
	*
	\**************************************************************************/
	//--------------------------------------------------------------------------
	// 
	//--------------------------------------------------------------------------
	template <class T> I3D_DLLEXPORT void DilationO(const Image3d<T> &in, Image3d<T> &out, const int radius_size)
	{
		Image3d<T> tmp;
		tmp = in;

		int i = 0;
		while (i < radius_size) {
			if ((i % 2) == 0) {
				i3d::Dilation(tmp, out, i3d::nb3D_o6);
			} else {
				i3d::Dilation(out, tmp, i3d::nb3D_o26);
			}
			++i;
		}

		if ((radius_size % 2) == 0)
			out = tmp;
	}

	template <class T> I3D_DLLEXPORT void ErosionO(const Image3d<T> &in, Image3d<T> &out, const int radius_size)
	{
		Image3d<T> tmp;
		tmp = in;

		int i = 0;
		while (i < radius_size) {
			if ((i % 2) == 0) {
				i3d::Erosion(tmp, out, i3d::nb3D_o6);
			} else {
				i3d::Erosion(out, tmp, i3d::nb3D_o26);
			}
			++i;
		}

		if ((radius_size % 2) == 0)
			out = tmp;
	}

	template <class T> I3D_DLLEXPORT void ClosingO(const Image3d<T> &in, Image3d<T> &out, const int radius_size)
	{
		Image3d<T> tmp;
		DilationO(in, tmp, radius_size);
		ErosionO(tmp, out, radius_size);
	}

	template <class T> I3D_DLLEXPORT void OpeningO(const Image3d<T> &in, Image3d<T> &out, const int radius_size)
	{
		Image3d<T> tmp;
		ErosionO(in, tmp, radius_size);
		DilationO(tmp, out, radius_size);
	}

	template <class T> I3D_DLLEXPORT void WhiteTopHatO(const Image3d<T> &in, Image3d<T> &out, const int radius_size)
	{
		OpeningO(in, out, radius_size);

		transform(
			in.GetFirstVoxelAddr(),
			in.GetFirstVoxelAddr() + in.GetImageSize(),
			out.GetFirstVoxelAddr(),
			out.GetFirstVoxelAddr(), 
			std::minus<T>());
	}

	template <class T> I3D_DLLEXPORT void BlackTopHatO(const Image3d<T> &in, Image3d<T> &out, const int radius_size)
	{
		ClosingO(in, out, radius_size);

		transform(
			out.GetFirstVoxelAddr(),
			out.GetFirstVoxelAddr() + out.GetImageSize(),
			in.GetFirstVoxelAddr(),
			out.GetFirstVoxelAddr(), 
			std::minus<T>());
	}

	//--------------------------------------------------------------------------
	// Explicit instantiations
	//--------------------------------------------------------------------------
	template I3D_DLLEXPORT void DilationO(const Image3d<BINARY> &in, Image3d<BINARY> &out, const int radius_size);
	template I3D_DLLEXPORT void DilationO(const Image3d<GRAY8> &in, Image3d<GRAY8> &out, const int radius_size);
	template I3D_DLLEXPORT void DilationO(const Image3d<GRAY16> &in, Image3d<GRAY16> &out, const int radius_size);

	template I3D_DLLEXPORT void ErosionO(const Image3d<BINARY> &in, Image3d<BINARY> &out, const int radius_size);
	template I3D_DLLEXPORT void ErosionO(const Image3d<GRAY8> &in, Image3d<GRAY8> &out, const int radius_size);
	template I3D_DLLEXPORT void ErosionO(const Image3d<GRAY16> &in, Image3d<GRAY16> &out, const int radius_size);

	template I3D_DLLEXPORT void ClosingO(const Image3d<BINARY> &in, Image3d<BINARY> &out, const int radius_size);
	template I3D_DLLEXPORT void ClosingO(const Image3d<GRAY8> &in, Image3d<GRAY8> &out, const int radius_size);
	template I3D_DLLEXPORT void ClosingO(const Image3d<GRAY16> &in, Image3d<GRAY16> &out, const int radius_size);

	template I3D_DLLEXPORT void OpeningO(const Image3d<BINARY> &in, Image3d<BINARY> &out, const int radius_size);
	template I3D_DLLEXPORT void OpeningO(const Image3d<GRAY8> &in, Image3d<GRAY8> &out, const int radius_size);
	template I3D_DLLEXPORT void OpeningO(const Image3d<GRAY16> &in, Image3d<GRAY16> &out, const int radius_size);

	template I3D_DLLEXPORT void WhiteTopHatO(const Image3d<GRAY8> &in, Image3d<GRAY8> &out, const int radius_size);
	template I3D_DLLEXPORT void WhiteTopHatO(const Image3d<GRAY16> &in, Image3d<GRAY16> &out, const int radius_size);

	template I3D_DLLEXPORT void BlackTopHatO(const Image3d<GRAY8> &in, Image3d<GRAY8> &out, const int radius_size);
	template I3D_DLLEXPORT void BlackTopHatO(const Image3d<GRAY16> &in, Image3d<GRAY16> &out, const int radius_size);

	/**************************************************************************\
	*
	*						  Basic morphological operations with a disk approximated by octagon
	*							Effective implementation using SE decomposition 
	*
	\**************************************************************************/
	//--------------------------------------------------------------------------
	// 
	//--------------------------------------------------------------------------
	template <class T> I3D_DLLEXPORT void MaximaDynamic(const Image3d<T> &in, Image3d<BINARY> &out, const T dynamic)
	{
		Image3d<T> tmp;
		out.CopyMetaData(in);

		h_Max(in, dynamic, tmp);
		for (size_t i = 0; i < in.GetImageSize(); ++i) {
			out.SetVoxel(i, (in.GetVoxel(i) - tmp.GetVoxel(i) == dynamic) ? i3d::BINARY(1) : i3d::BINARY(0));
		}
	}

	//--------------------------------------------------------------------------
	// Explicit instantiations
	//--------------------------------------------------------------------------
	template I3D_DLLEXPORT void MaximaDynamic(const Image3d<GRAY8> &in, Image3d<BINARY> &out, const GRAY8 dynamic);
	template I3D_DLLEXPORT void MaximaDynamic(const Image3d<GRAY16> &in, Image3d<BINARY> &out, const GRAY16 dynamic);

} // i3d namespace
