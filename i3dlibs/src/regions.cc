/*
 * i3dlib - image manipulation library
 *
 * Copyright (C) 2000-2006   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 * FILE: regions.h
 *
 * labeled 3d image
 * region labeling algorithm (for symetric neigbourhoods)
 *
 * Pavel Matula (pam@fi.muni.cz) 2001
 * Petr MejzlÌk (mejzlik@fi.muni.cz) 2001
 */

#ifdef __GNUG__
 #pragma implementation
#endif

#ifdef I3D_DEBUG
 #include <iostream>
#endif
#include <fstream>

#include <list>
#include <limits>
#include <map>
#include <set>
#include <vector>

#include "draw.h"
#include "regions.h"
#include "neighbours.h"
#include "transform.h"
#include "morphology.h"

#include "toolbox.h"
#include "imgfiles.h"
#include "vector3d.h"

//#define VERBOSE_DEBUG

using namespace std;

namespace i3d {

  /* Restrict neighbourhood such that you can see only formerly processed 
     voxels */
  static void Restrict(const Neighbourhood &nb, Neighbourhood &rnb);

  /* AdjustL2R set representative of all voxels in list l to new_r */
  template <class LABEL>
  static void AdjustL2R(std::vector<LABEL> &l2r, std::list<LABEL> &l, LABEL new_r);

  /* return values from win2[i], such that v == win1[i] */ 
  template <class LABEL, class VOXEL> 
  static void GetLabels(const VOXEL v, const std::vector<VOXEL *> &win1, 
                        const std::vector<LABEL *> &win2, std::vector<LABEL> &val);

  /** Compute transitive closure using DFS. */
  template <class LABEL>
  static void ComputeTransitiveClosure(std::map<LABEL, std::set<LABEL> > &neigh_list, 
                                       std::vector<LABEL> &remap_list);


  /****************************************************************/

  static void Restrict(const Neighbourhood &nb, Neighbourhood &rnb)
  {
    rnb.offset.clear();
    VectContainer::const_iterator off;
    for (off = nb.offset.begin(); off != nb.offset.end(); ++off)
      {
        if ((off->x <= 0 && off->y == 0 && off->z == 0)
            || (off->y < 0 && off->z == 0)
            || (off->y <= 0 && off->z < 0))
          {
            rnb.offset.push_back(*off);
          }
      }
  }

  /****************************************************************/

  template <class LABEL>
  static void AdjustL2R(std::vector<LABEL> &l2r, std::list<LABEL> &l, LABEL new_r)
  {
    typedef typename std::list<LABEL>::const_iterator LVI;
    for (LVI i = l.begin(); i != l.end(); ++i) {
      l2r[*i] = new_r;
    }
  }
  
  /****************************************************************/

  template <class LABEL, class VOXEL> 
  static void GetLabels(const VOXEL v, const std::vector<VOXEL *> &win1, 
                        const std::vector<LABEL *> &win2, std::vector<LABEL> &val)
  {
    val.resize(0); val.reserve(win1.size());
    size_t i = 0;
    while (i < win1.size()) {
      if (v == *(win1[i])) {
        val.push_back(*(win2[i]));
      }
      ++i;
    }
  }

  /****************************************************************/
  template <class LABEL>
  static void ComputeTransitiveClosure(std::map<LABEL, std::set<LABEL> > &neigh_list, 
                                       std::vector<LABEL> &remap_list)
  {
	  size_t num = neigh_list.size();

	  remap_list.clear();
	  remap_list.resize(num, LABEL(0));

	  LABEL parent, val, comp_id = LABEL(0);
	  typename std::map<LABEL, std::set<LABEL> >::iterator it = neigh_list.begin(), pit;
	  for (; it != neigh_list.end(); it++)
	  {
		  if (it -> second.empty())
		  {
			  continue;
		  }
		  
		  it -> second.erase(it -> second.begin());
		  parent = it -> first;
		  remap_list[parent - 1] = parent;
		  pit = it;
		  comp_id++;

		  while (true)
		  {
			  if (pit->second.empty())
			  {
				  // move upward on a tree
				  if (remap_list[parent - 1] == parent)
				  { 
					  // the root of a tree found
					  remap_list[parent - 1] = comp_id;
					  break;
				  }
				  else
				  {
					  // move to the parent node
					  val = remap_list[parent - 1];
					  remap_list[parent - 1] = comp_id;
					  parent = val;
					  pit = neigh_list.find(parent);
				  }
			  }
			  else
			  {
				  // move downward on a tree
				  val = *(pit->second.begin());
				  pit->second.erase(pit->second.begin());

				  if (remap_list[val - 1] == LABEL(0))
				  {
					  // an unlabelled node found
					  remap_list[val - 1] = parent;
					  pit = neigh_list.find(val);
					  parent = val;
				  }
			  }
		  }
	  }
  }

  /****************************************************************/

#ifdef VERBOSE_DEBUG
  template <class T>
  static void print(std::vector<T> &v) 
  {
    std::cerr << "[ ";
    for (size_t i = 0; i < v.size(); ++i) {
      std::cerr << v[i] << " ";
    }
    std::cerr << "]" << std::endl;
  }

  template <class LABEL>
  static void printl2r(std::vector<LABEL> &v, LABEL label) 
  {
    std::cerr << "l2r = [ ";
    for (size_t i = 0; i < label; ++i) {
      std::cerr << v[i] << " ";
    }
    std::cerr << "]" << std::endl;
  }

  template <class LABEL>
  static void printr2l(std::map<LABEL, std::list<LABEL> > &m) 
  {
    typedef std::map<LABEL, list<LABEL> >::const_iterator MCI;
    typedef std::list<LABEL>::const_iterator LCI;
    
    std::cerr << "r2l:" << std::endl;
    for (MCI j = m.begin(); j != m.end(); ++j) {
      std::cerr << j->first << " -> ";
      std::cerr << "[ ";
      for (LCI i = j->second.begin(); i != j->second.end(); ++i) {
        std::cerr << *i << " ";
      }
      std::cerr << "]" << std::endl;
    }
    std::cerr << std::endl;
  }
#endif // VERBOSE_DEBUG


  /****************************************************************/
  /*                       CreateRegionsOld()                     */
  /****************************************************************/

  template <class LABEL, class VOXEL>
  void LabeledImage3d<LABEL, VOXEL>::CreateRegionsOld(Image3d<VOXEL>& img, 
                                                      const Neighbourhood &nb)
  {
    SetOffset(img.GetOffset());
    SetResolution(img.GetResolution());
    MakeRoom(img.GetSizeX(), img.GetSizeY(), img.GetSizeZ());
    components.clear();
    
    Neighbourhood rnb;
    Neighbourhood new_nb;
    Restrict(nb, rnb);

    LABEL label = 0;
    // l2r transform label to representative
    std::vector<LABEL> l2r; 
    //    l2r.reserve(std::numeric_limits<size_t>::max());
    // r2l transform representatives to labels
    std::map<LABEL, std::list<LABEL> > r2l; 

    size_t i_voxel = 0;
    while  (i_voxel < img.GetImageSize()) {
      int j;
      size_t x, y, z;
      std::vector<VOXEL *> win1;
      std::vector<LABEL *> win2;
      std::vector<LABEL> nbh_labels;
      x = img.GetX(i_voxel);
      y = img.GetY(i_voxel);
      z = img.GetZ(i_voxel);

      j = GetNbh(img, x, y, z, rnb, new_nb);

#ifdef VERBOSE_DEBUG
      std::cerr << "Okoli = ";
      ;
      for (VectContainer::iterator k = new_nb.offset.begin();
           k != new_nb.offset.end(); ++k) {
        std::cerr << "(" << k->x << "," 
             << k->y << ","
             << k->z << ") ";
      } std::cerr << std::endl;
#endif
      
      MakeWindow(img, x, y, z, new_nb, win1);
      MakeWindow(*this, x, y, z, new_nb, win2);
      
      for (int k = 0; k < j; ++k) {
        x = img.GetX(i_voxel);
        y = img.GetY(i_voxel);
        z = img.GetZ(i_voxel);
        
        GetLabels(img.GetVoxel(i_voxel), win1, win2, nbh_labels);

#ifdef VERBOSE_DEBUG
        std::cerr << "nbh_labels = ";
        print(nbh_labels);
#endif

        if (nbh_labels.size() == 0) { 
          // create new region

#ifdef VERBOSE_DEBUG
          std::cerr << "New: SetVoxel i = " << i_voxel << " label = " << int(label) << std::endl;
#endif
	  if (label >= std::numeric_limits<LABEL>::max())
	  	throw InternalException("CreateRegions: Too many regions.");
          SetVoxel(i_voxel, label);
          l2r.push_back(label);
          r2l[label].push_back(label);
          components[label] = ComponentInfo<VOXEL>(\
	  	VOI<PIXELS>(x, y, z, 1, 1, 1), 1, img.GetVoxel(i_voxel),
		img.OnBorder(x, y, z));
          ++label;
        } else {

#ifdef VERBOSE_DEBUG
          std::cerr << "New: SetVoxel i = " << i_voxel << " label = " << int(l2r[nbh_labels[0]]) << std::endl;
#endif

          SetVoxel(i_voxel, l2r[nbh_labels[0]]);
          LABEL vr1 = this->GetVoxel(i_voxel);
          bool extended = false;
          
          for (size_t i = 0; i < nbh_labels.size(); ++i) {
            LABEL vr2 = l2r[nbh_labels[i]];
            if (vr1 == vr2) { 
              // region extension 

              if (!extended)
                {
#ifdef VERBOSE_DEBUG
                  std::cerr << "Extension" << std::endl;
#endif
                  /* jde napsat efektivneji bez pouziti sjednoceni VOI */
                  components[vr1].voi += VOI<PIXELS>(x, y, z, 1, 1, 1); 
                  ++components[vr1].volume;
		  components[vr1].border = components[vr1].border || \
		  			   img.OnBorder(x, y, z);
                  extended = true;
                }
            } else { 
              // join regions

#ifdef VERBOSE_DEBUG
              std::cerr << "Join" << std::endl;
#endif
              
              AdjustL2R(l2r, r2l[vr2], vr1);
              r2l[vr1].splice(r2l[vr1].begin(), 
                                r2l[vr2]);
              r2l.erase(vr2);
              components[vr1] += components[vr2];
              components.erase(vr2);
            }
          }
        }

#ifdef VERBOSE_DEBUG
        printl2r(l2r, label);
        printr2l(r2l);
#endif

        MoveWindow(win1); MoveWindow(win2);
        ++i_voxel;
      }
    }
    RemapIntensities(*this, l2r);
    max_label = components.empty()?0:components.rbegin()->first;
  }


  /****************************************************************/
  /*                         CreateRegions()                      */
  /****************************************************************/
  template <class LABEL, class VOXEL>
	  void LabeledImage3d<LABEL, VOXEL>::CreateRegions(Image3d<VOXEL> &img, const Neighbourhood &nb)
  {
	  // init labeled image
	  SetOffset(img.GetOffset());
	  SetResolution(img.GetResolution());
	  MakeRoom(img.GetSizeX(), img.GetSizeY(), img.GetSizeZ());
	  components.clear();

	  // get neighbourhood for formerly processed voxels
	  Neighbourhood rnb;
	  Restrict(nb, rnb);

	  typedef std::map<LABEL, std::set<LABEL> > Neigh_list;
	  Neigh_list dependencies;
	  std::vector<VOXEL *> img_win;
	  std::vector<LABEL *> limg_win;
	  std::vector<LABEL> nbh_labels;
	  Neighbourhood new_nb;
	  size_t x, y, z, shift, i_voxel = 0;
	  LABEL label = 1; //, min_nbh_label; - not used anywhere (xulman@fi)
	
	  // realize the first scan
	  while (i_voxel < img.GetImageSize()) 
	  {
		  x = img.GetX(i_voxel);
		  y = img.GetY(i_voxel);
		  z = img.GetZ(i_voxel);

		  shift = GetNbh(img, x, y, z, rnb, new_nb);
      
		  MakeWindow(img, x, y, z, new_nb, img_win);
		  MakeWindow(*this, x, y, z, new_nb, limg_win);

		  for (size_t i = 0; i < shift; i++) 
		  {
			  x = img.GetX(i_voxel);
			  y = img.GetY(i_voxel);
			  z = img.GetZ(i_voxel);

			  GetLabels(img.GetVoxel(i_voxel), img_win, limg_win, nbh_labels);

			  if (nbh_labels.size() == 0) 
			  {           
				  // new region found
				  if (label >= std::numeric_limits<LABEL>::max())
				  {
					  throw InternalException("CreateRegions: Too many regions.");
				  }

				  SetVoxel(i_voxel, label);

				  std::set<LABEL> set;
				  set.insert(label);
				  dependencies[label] = set;
				  label++;
			  }
			  else 
			  {
				  SetVoxel(i_voxel, nbh_labels[0]);
				  
				  for (size_t j = 1; j < nbh_labels.size(); j++)
				  {
					  if (nbh_labels[0] != nbh_labels[j])
					  {
						  dependencies.find(nbh_labels[0]) -> second.insert(nbh_labels[j]);
                          dependencies.find(nbh_labels[j]) -> second.insert(nbh_labels[0]);
					  }
				  }
			  }
			  
			  MoveWindow(img_win); 
			  MoveWindow(limg_win);
			  i_voxel++;
		  }
	  }

      // compute transitive closure of conflicts
	  std::vector<LABEL> remap_list;
	  ComputeTransitiveClosure(dependencies, remap_list);

	  // realize the second scan
	  LABEL val;
	  max_label = 0;
	  for (size_t z = 0; z < this->GetSizeZ(); z++)
	  {
		  for (size_t y = 0; y < this->GetSizeY(); y++)
		  {
			  for (size_t x = 0; x < this->GetSizeX(); x++)
			  {
				  val = remap_list[this->GetVoxel(x, y, z) - 1];
				  SetVoxel(x, y, z, val);

				  if (val > max_label)
				  {
					  // create a new component
					  components[val] = ComponentInfo<VOXEL>(VOI<PIXELS>(x, y, z, 1, 1, 1), 
															 1, 
															 img.GetVoxel(x, y, z), 
															 this->OnBorder(x, y, z));

					  max_label++;
				  }
				  else
				  {
  				      // extend a component
					  components[val].volume += 1;
					  components[val].border = components[val].border || this->OnBorder(x, y, z);
					  components[val].voi += VOI<PIXELS>(x, y, z, 1, 1, 1);
				  }
				  
			  }
		  }
	  }
  }
  
  /****************************************************************/
  /*                       CreateRegionsFF()                        */
  /****************************************************************/

  template <class LABEL, class VOXEL>
  void LabeledImage3d<LABEL, VOXEL>::CreateRegionsFF(
						Image3d<VOXEL>& img, const Neighbourhood &nb)
  {
    SetOffset(img.GetOffset());
    SetResolution(img.GetResolution());
    MakeRoom(img.GetSizeX(), img.GetSizeY(), img.GetSizeZ());
    memset(this->GetFirstVoxelAddr(), 0, this->GetImageSize()*sizeof(LABEL));
    components.clear();
   
    LABEL label = 0;

    for (size_t i_voxel = 0; i_voxel < img.GetImageSize(); ++i_voxel) 
	 {
      size_t x, y, z;
      x = img.GetX(i_voxel);
      y = img.GetY(i_voxel);
      z = img.GetZ(i_voxel);
      if (this->GetVoxel(i_voxel) == 0) 
		{
        // new region
        if (++label >= std::numeric_limits<LABEL>::max())
          throw InternalException("CreateRegions: Too many regions.");
        components[label] = FloodFill(img, *this, x, y, z, label, nb);
      }
    }
    max_label = label;
  }

  /****************************************************************/
  template <class LABEL, class VOXEL>
  void LabeledImage3d<LABEL, VOXEL>::CreateForegroundRegionsFF(
						const Image3d<VOXEL>& img, const Neighbourhood &nb)
  {
    SetOffset(img.GetOffset());
    SetResolution(img.GetResolution());
    MakeRoom(img.GetSize());
    memset(this->GetFirstVoxelAddr(), 0, this->GetImageSize()*sizeof(LABEL));
    components.clear();
   
    LABEL label = 0;

    for (size_t i_voxel = 0; i_voxel < img.GetImageSize(); i_voxel++) 
		{
		// test if the input image voxel color is different from black (0)
		// and the region has not been coloured with any label yet!
      if ((img.GetVoxel(i_voxel) != 0) && (this->GetVoxel(i_voxel) == 0)) 
			{
         if (++label >= std::numeric_limits<LABEL>::max())
            throw InternalException("CreateRegions: Too many regions.");

         // new region
	      size_t x, y, z;
	      x = img.GetX(i_voxel);
	      y = img.GetY(i_voxel);
	      z = img.GetZ(i_voxel);

         components[label] = FloodFill(img, *this, x, y, z, label, nb);
      	}
    	}
    max_label = label;
  }

  /****************************************************************/
  /*                      SaveImage()                             */
  /****************************************************************/
  template <class LABEL, class VOXEL>
  void LabeledImage3d<LABEL, VOXEL>::SaveImage (const char *filename, FileFormat format) const
  {                        
      std::string fullname = filename, path, name, ext, img_name, l3d_name;

      SplitFilename (fullname, path, name, ext);
      ext = "." + FileFormatToExtension (format);
      img_name = path + name + ext;
      l3d_name = path + name + ".l3d";

      // Save image
      Image3d<LABEL>::SaveImage (img_name.c_str(), format);
      
      // Save l3d file
      img_name = name + ext;
      std::ofstream ofs (l3d_name.c_str(), std::ios_base::out | std::ios_base::trunc);
      
      if (!ofs.good())
          throw IOException ("LabeledImage3d::SaveImage: Unable to create .l3d output file.");
      
      ofs << "image=" << img_name << '\n';
      ofs << "displacement=" << displacement << '\n';
      ofs << "components=" << NumberOfComponents() << '\n';
      ofs << "max_label=" << max_label << '\n';

      typename ComponentInfo<VOXEL>::Container::const_iterator i;
      for (i = components.begin(); i != components.end(); i++) 
      {
          size_t vox_id = i->first;
          const ComponentInfo<VOXEL> *comp = &i->second;
          ofs << vox_id << ' ' << comp->voi << ' ' << comp->volume << ' ' << comp->border;
          ofs << ' ' << int(comp->voxel_value) << '\n';
      }

      if (!ofs.good())
          throw IOException ("LabeledImage3d::SaveImage: Saving component info failed");

      ofs.close();
  }

  /****************************************************************/
  /*                      ReadImage()                             */
  /****************************************************************/
  template <class LABEL, class VOXEL>
  void LabeledImage3d<LABEL, VOXEL>::ReadImage (const char *filename, const VOI<PIXELS> *voi, bool isregexp, size_t channel)
  {
      std::string fullname = filename, path, name, ext, img_name;
      size_t comp_count;

      SplitFilename (fullname, path, name, ext);
      
      // Append extension if missing and open file
      if (ext.empty())  
          fullname += ".l3d";
      
      std::ifstream ifs (fullname.c_str());
      if (!ifs.good())
      {
          std::string err_msg ("LabeledImage3d::ReadImage: Unable to open file ");
          err_msg += fullname;
          throw IOException (err_msg);
      }
      
      // Read header
      std::map<std::string,std::string> items;
      size_t line_no = 1;

      while (!ifs.eof() && line_no++ < 5)
      {
        std::string s;
        std::string key, val;
        getline (ifs, s, '\n');

        size_t pos = s.find('=');
        key = Trim(s.substr(0,pos));
        val = (pos == std::string::npos) ? string("") : Trim(s.substr(pos+1,std::string::npos));

        if (val.empty() || key.empty())
          throw IOException("LabeledImage3d::ReadImage: Bad format of l3d file");

        items[key] = val;
      }
            
      if (items["components"].empty() || items["max_label"].empty() || 
          items["displacement"].empty() || items["image"].empty())
         throw IOException("LabeledImage3d::ReadImage: Missing parameters.");

      img_name = path + items["image"];
      comp_count = (size_t) std::max (0L, atol (items["components"].c_str()));
      max_label = (size_t) std::max (0L, atol (items["max_label"].c_str()));
      if (!StrToVector3d (items["displacement"].c_str(), displacement))
          throw IOException("LabeledImage3d::ReadImage: Displacement vector has bad format.");

      // Load 3d data and convert
      if (std::numeric_limits<LABEL>::max() < max_label)
          throw IOException ("LabeledImage3d::ReadImage: Label data conversion not possible.");

      ImgVoxelType vox_type = ReadImageType (img_name.c_str());

      if (vox_type == Gray8Voxel)
      {
         Image3d<GRAY8> img8;
         img8.ReadImage (img_name.c_str(), voi, channel);
         MakeRoom (img8.GetSize());
         this->SetResolution (img8.GetResolution());
         this->SetOffset (img8.GetOffset());
         LABEL *dest = this->GetFirstVoxelAddr();
         GRAY8 *src = img8.GetFirstVoxelAddr();
         for (size_t i = 0; i < img8.GetImageSize(); i++, dest++, src++)
            *dest = (LABEL) *src;
      }
      else if (vox_type == Gray16Voxel)
      {
         Image3d<GRAY16> img16;
         img16.ReadImage (img_name.c_str(), voi, channel);
         MakeRoom (img16.GetSize());
         this->SetResolution (img16.GetResolution());
         this->SetOffset (img16.GetOffset());
         LABEL *dest = this->GetFirstVoxelAddr();
         GRAY16 *src = img16.GetFirstVoxelAddr();
         for (size_t i = 0; i < img16.GetImageSize(); i++, dest++, src++)
            *dest = (LABEL) *src;
      }
      else
        throw IOException ("LabeledImage3d::ReadImage: Unsupported image type.");

      // Load components list
      size_t lb_label, lb_voxel;
      ComponentInfo<VOXEL> lb_ci;
      char c1, c2, c3;      

      components.clear();
      for (size_t i = 0; i < comp_count; i++)
      {
         ifs >> lb_label;

         // Read VOI
         ifs >> c1 >> lb_ci.voi.offset >> c2 >> lb_ci.voi.size >> c3;
         if (c1 != '[' || c2 != ';' || c3 != ']')
             throw IOException ("LabeledImage3d::ReadImage: Bad component info format.");

         // Read volume, border and voxel_value
         ifs >> lb_ci.volume;
         ifs >> lb_ci.border;
         ifs >> lb_voxel;         
         lb_ci.voxel_value = lb_voxel;

         if (!ifs.good())
             throw IOException ("LabeledImage3d::ReadImage: Loading component info failed.");

         // If VOI is specified - recompute component VOIs and discard components lying outside.
         // Also find the new correct max_label
         if (voi)
         {
             lb_ci.voi *= *voi;
             lb_ci.voi.offset -= voi->offset;
             if (!i || lb_label > max_label)
                 max_label = lb_label;
         }

         if (lb_ci.voi.size.x * lb_ci.voi.size.y * lb_ci.voi.size.z > 0)
             components[lb_label] = lb_ci;
      }

      ifs.close();
  }

  /****************************************************************/
  /*                      RemoveComponent()                       */
  /****************************************************************/
  template <class LABEL, class VOXEL>
	void LabeledImage3d<LABEL, VOXEL>::RemoveComponent(LABEL label)
  {
	  // Check whether a component with given label is available
	  typename ComponentInfo<VOXEL>::Container::const_iterator it;
  
	  if ((it = components.find(label)) == components.end())
	  {
		  throw i3d::InternalException("LabeledImage3d::RemoveComponent: No such label.");
	  }

	  // set all the voxels of the component to 0
	  for (size_t z = 0; z < it -> second.voi.size.z; z++)
	  {
		  for (size_t y = 0; y < it -> second.voi.size.y; y++)
		  {
			  for (size_t x = 0; x < it -> second.voi.size.x; x++)
			  {
				  this -> SetVoxel(it -> second.voi.offset.x + x, 
					               it -> second.voi.offset.y + y, 
						 		   it -> second.voi.offset.z + z,
								   (LABEL) 0);
			  }
		  }
	  }

	  // remove the component from the list of components
	  components.erase(label);

	  // update maximal label
	  if (components.size())
	  {
		  if (label == max_label)
		  {
			  max_label = components.rbegin() -> first;
		  }
	  }
	  else
	  {
		  max_label = 0;
	  }
  }

  /****************************************************************/
  /*                      MergeComponents()                       */
  /****************************************************************/

  template <class LABEL, class VOXEL>
  void LabeledImage3d<LABEL, VOXEL>::MergeComponents(LABEL comp1, LABEL comp2)
  {
    if (comp1 == comp2) 
      return;
    ComponentInfo<VOXEL> c2 = components[comp2];
    for (size_t z = 0; z < c2.voi.size.z; ++z)
      for (size_t y = 0; y < c2.voi.size.y; ++y)
        for (size_t x = 0; x < c2.voi.size.x; ++x) {
          size_t xx = c2.voi.offset.x + x;
          size_t yy = c2.voi.offset.y + y;
          size_t zz = c2.voi.offset.z + z;
          if (this->GetVoxel(xx, yy, zz) == comp2) {
            this->SetVoxel(xx, yy, zz, comp1);
          }
        }
    components[comp1] += components[comp2];
    components.erase(comp2);
  }

  /****************************************************************/
  /*                  AddNewComponent()                        */
  /****************************************************************/
//----------------------------------------------------------------------

template <class LABEL, class VOXEL> LABEL 
LabeledImage3d<LABEL, VOXEL>::AddNewComponent(const Image3d<BINARY> &mask)
  {
		if (this->GetResolution().GetRes() != mask.GetResolution().GetRes())
			 throw LibException("i3dlib::LabeledImage3d - different resolution");

		if (mask.GetVoxelData().max() == (LABEL) 0) // nothing to add
			 return (LABEL) 0; // return background

		ComponentInfo<VOXEL> component;
		size_t x, y, z;

		max_label++;

		Offset img_offset = this->GetOffset(),
				 comp_offset = mask.GetOffset(),
				 r_offset = comp_offset - img_offset;

		Vector3d<size_t> pos = MicronsToPixels(r_offset, this->GetResolution());

		for (size_t i=0; i<mask.GetImageSize(); i++)
		{
			 if (mask.GetVoxel(i) != BINARY(0))
			 {
				  x = pos.x + mask.GetX(i);
				  y = pos.y + mask.GetY(i);
				  z = pos.z + mask.GetZ(i);

				  if (!this->Include(x,y,z))
						throw LibException("i3dlib::LabeledImage3d - invalid position");

				  this->SetVoxel(x, y, z, max_label);

				  if (component.volume == 0)
						component.voi = VOI<PIXELS>(x,y,z,1,1,1);
				  else
						component.voi += VOI<PIXELS>(x,y,z,1,1,1);

				  component.volume++;
			 }
		}

		Vector3d<size_t> ph = 
				  Vector3d<size_t>(component.voi.offset) 
				  + component.voi.size 
				  - Vector3d<size_t>(1,1,1);

		component.border = mask.OnBorder(component.voi.offset.x, 
													component.voi.offset.y, 
													component.voi.offset.z) || 
				  mask.OnBorder (ph.x, ph.y, ph.z);

		component.voxel_value = std::numeric_limits<VOXEL>::max();

      components[max_label] = component;

		return max_label;
  }


  /****************************************************************/
  /*                  UpdateComponent()                        */
  /****************************************************************/

template <class LABEL, class VOXEL> void
LabeledImage3d<LABEL, VOXEL>::UpdateComponent(LABEL lbl, const Image3d<BINARY> &mask)
  {
		if (this->GetResolution().GetRes() != mask.GetResolution().GetRes())
			 throw LibException("i3dlib::LabeledImage3d - different resolution");

		ComponentInfo<VOXEL> component;
		size_t x, y, z;

		Offset img_offset = this->GetOffset(),
				 comp_offset = mask.GetOffset(),
				 r_offset = comp_offset - img_offset;

		Vector3d<size_t> pos = MicronsToPixels(r_offset, this->GetResolution());

		for (size_t i=0; i<mask.GetImageSize(); i++)
		{
			 if (mask.GetVoxel(i) != BINARY(0))
			 {
				  x = pos.x + mask.GetX(i);
				  y = pos.y + mask.GetY(i);
				  z = pos.z + mask.GetZ(i);

				  if (!this->Include(x,y,z))
						throw LibException("i3dlib::LabeledImage3d - invalid position");

				  this->SetVoxel(x, y, z, lbl);

				  if (component.volume == 0)
						component.voi = VOI<PIXELS>(x,y,z,1,1,1);
				  else
						component.voi += VOI<PIXELS>(x,y,z,1,1,1);

				  component.volume++;
			 }
		}

		Vector3d<size_t> ph = 
				  Vector3d<size_t>(component.voi.offset)
				  + component.voi.size 
				  - Vector3d<size_t>(1,1,1);

		component.border = mask.OnBorder(component.voi.offset.x, 
													component.voi.offset.y, 
													component.voi.offset.z) || 
				  mask.OnBorder (ph.x, ph.y, ph.z);

		component.voxel_value = std::numeric_limits<VOXEL>::max();

      components[lbl] = component;

	  return;
  }


  /****************************************************************/
  /*                  GetComponentMask()                        */
  /****************************************************************/
template <class LABEL, class VOXEL>
void LabeledImage3d<LABEL,VOXEL>::GetComponentMask(LABEL lbl, Image3d<BINARY> &mask) const
{
	 typename ComponentInfo<VOXEL>::Container::const_iterator it;
    it = this->components.find(lbl); 
	 
	 if (it == this->components.end()) 
	 {
		  throw LibException("i3dlib::LabeledImage3d - wrong component index");
	 }

	 VOI<PIXELS> block = it->second.voi;
	 Offset r_offset = PixelsToMicrons(block.offset, this->GetResolution());

	 mask.SetResolution(this->GetResolution());
	 mask.SetOffset(r_offset + this->GetOffset());
	 mask.MakeRoom(block.size);
	 mask.Clear();

	 for (size_t z=0; z<block.size.z; z++)
		  for (size_t y=0; y<block.size.y; y++)
				for (size_t x=0; x<block.size.x; x++)
				{
					 if (this->GetVoxel(block.offset.x + x,
											  block.offset.y + y,
											  block.offset.z + z) == lbl)
					 {
						  mask.SetVoxel(x,y,z, BINARY(1));
					 }
				}
}

  /****************************************************************/
  /*                  FindAndSetMaxLabel()                        */
  /****************************************************************/
  
  template <class LABEL, class VOXEL>
  size_t LabeledImage3d<LABEL,VOXEL>::FindAndSetMaxLabel ()
  {
      if (components.size())
      {
          typename ComponentInfo<VOXEL>::Container::const_iterator i = components.begin();

          max_label = i->first;
          while (++i != components.end())
              if (i->first > max_label)
                  max_label = i->first;
      }
      else
          max_label = 0;
      
      return max_label;
  }

  /****************************************************************/
  /*                      WeightedCenter()                        */
  /****************************************************************/

  template <class ORIGVOXEL, class LABEL, class VOXEL> 
  bool WeightedCenter(const Image3d<ORIGVOXEL> &img,
                    const LabeledImage3d<LABEL, VOXEL> &labels, LABEL lbl, 
		    Vector3d<float>& center)
  {
    #if defined(__GNUG__) && __GNUG__ < 3
	typename map<size_t, ComponentInfo<VOXEL> >::const_iterator ci;
    #else
	typename ComponentInfo<VOXEL>::Container::const_iterator ci;
    #endif
    if ((ci = labels.components.find(lbl)) == labels.components.end())
      throw InternalException("WeightedCenter: No such label.");
    
    float sum_x = 0, sum_y = 0, sum_z = 0;
    float sum = 0;
	ORIGVOXEL min_val = 0;

    // disp is the offset of the indexed image according to img.
    Vector3d<int> disp = labels.GetDisplacement(); 

	 // r = region in limg with label lbl
    const ComponentInfo<VOXEL> &r  = ci->second; 
    VOI<PIXELS> voi(r.voi.offset, r.voi.size);

    // If the indexed image has a negative displacement, shrink the voi
    // so that it won't reference voxels out of the original image (img):
    Vector3d<int> v = max(-disp, Vector3d<int>(0,0,0));
    voi.offset += Vector3d<PIXELS::T_offset>(v);
    voi.size -= Vector3d<PIXELS::T_size>(v);

    // We will take into account voxels from interval voi in the indexed
    // image.
    if (!le(disp + voi.offset + Vector3d<int>(voi.size), 
				Vector3d<int>(img.GetSize())))
	 {
      throw InternalException("WeightedCenter: VOI out of bounds.");
	 }

    for (int z = voi.offset.z; z < voi.offset.z + (int)voi.size.z; z++)
      for (int y = voi.offset.y; y < voi.offset.y + (int)voi.size.y; y++)
        {
          const LABEL* l = labels.GetVoxelAddr(voi.offset.x, y, z);

          for (int x = voi.offset.x; x < voi.offset.x + (int)voi.size.x; x++)
            {
              ORIGVOXEL v = img.GetVoxel(x+disp.x, y+disp.y, z+disp.z);

              // For images with negative values it does not make much sense to calculate weighted center
              if (v < min_val)
              {
                  center = Vector3d<float> (0, 0, 0);
                  return false;
              }

              if (*l == lbl)
                {
                  sum_x += x * v;
                  sum_y += y * v;
                  sum_z += z * v;
                  sum   += v;
                }
              l++;
            }
        }

    if (sum <= 0) // we test by <= instead of == due to float arithmetic
    {
        // If sum is 0 then: the label is either not present in the VOI or all voxels have 0 intensity.
        // For the second case we will return the non-weighted center
        for (int z = voi.offset.z; z < voi.offset.z + (int)voi.size.z; z++)
            for (int y = voi.offset.y; y < voi.offset.y + (int)voi.size.y; y++)
            {
                const LABEL* l = labels.GetVoxelAddr(voi.offset.x, y, z);

                for (int x = voi.offset.x; x < voi.offset.x + (int)voi.size.x; x++)
                {
                    if (*l == lbl)
                    {
                        sum_x += x;
                        sum_y += y;
                        sum_z += z;
                        sum++;;
                    }
                    l++;
                }
            }

        if (sum <= 0) // we test by <= instead of == due to float arithmetic
            throw InternalException("WeightedCenter: Label not found in defined VOI.");
    }

    center = PixelsToMicrons( Vector3d<float> (sum_x/sum +float(disp.x),
                                               sum_y/sum +float(disp.y),
                                               sum_z/sum +float(disp.z)),
	                                           labels.GetResolution());
    return true;
  }

template I3D_DLLEXPORT bool WeightedCenter(const Image3d<GRAY8>&,
                                           const LabeledImage3d<size_t, BINARY>&, size_t, Vector3d<float>&);

template I3D_DLLEXPORT bool WeightedCenter(const Image3d<GRAY8>&,
                                           const LabeledImage3d<size_t, GRAY8>&, size_t, Vector3d<float>&);

template I3D_DLLEXPORT bool WeightedCenter(const Image3d<GRAY8>&,
                                           const LabeledImage3d<BINARY, BINARY>&, BINARY, Vector3d<float>&);

template I3D_DLLEXPORT bool WeightedCenter(const Image3d<GRAY8>&,
                                           const LabeledImage3d<unsigned short, BINARY>&, unsigned short, 
                                           Vector3d<float>&);

template I3D_DLLEXPORT bool WeightedCenter(const Image3d<GRAY16>&,
                                           const LabeledImage3d<unsigned short, BINARY>&, unsigned short, 
                                           Vector3d<float>&);

template I3D_DLLEXPORT bool WeightedCenter(const Image3d<GRAY16>&,
                                           const LabeledImage3d<size_t, GRAY16>&, size_t, 
                                           Vector3d<float>&);

  /****************************************************************/
  /*                          Center()                            */
  /****************************************************************/

template <class LABEL, class VOXEL> 
bool Center(const LabeledImage3d<LABEL, VOXEL> &labels, LABEL lbl, 
	    Vector3d<float>& center)
  {
    #if defined(__GNUG__) && __GNUG__ < 3
	typename map<size_t, ComponentInfo<VOXEL> >::const_iterator ci;
    #else
	typename ComponentInfo<VOXEL>::Container::const_iterator ci;
    #endif
    if ((ci = labels.components.find(lbl)) == labels.components.end())
      throw InternalException("Center: No such label.");
    
    float sum_x = 0.0, sum_y = 0.0, sum_z = 0.0, sum = 0.0;
    
    const ComponentInfo<VOXEL> &r  = ci->second;
    Vector3d<int> disp = labels.GetDisplacement(); 
    VOI<PIXELS> voi(r.voi.offset, r.voi.size);
    // If the indexed image has a negative displacement, shrink the voi
    // so that it won't reference voxels out of the original image 
    // (to be compatible with WeightedCenter):
    Vector3d<int> v = max(-disp, Vector3d<int>(0,0,0));
    voi.offset += Vector3d<PIXELS::T_offset>(v);
    voi.size -= Vector3d<PIXELS::T_size>(v);
    // We will take into account voxels from interval voi in the indexed
    // image.


    for (int z = voi.offset.z; z < voi.offset.z + (int) voi.size.z; z++)
      for (int y = voi.offset.y; y < voi.offset.y + (int) voi.size.y; y++)
        {
          const LABEL* l = labels.GetVoxelAddr(voi.offset.x, y, z);

          for (int x = voi.offset.x; x < voi.offset.x + (int) voi.size.x; x++)
            {
              if (*l == lbl)
                {
                  sum_x += x;
                  sum_y += y;
                  sum_z += z;
                  sum++;
                }
              l++;
            }
        }
    if (sum == 0) {
      center = Vector3d<float>(0.0, 0.0, 0.0);
      return false;
    }
    else {
      center = PixelsToMicrons( Vector3d<float>
               (sum_x/sum +disp.x, sum_y/sum +disp.y, sum_z/sum +disp.z),
	       labels.GetResolution());
      return true;
    }
  }

template I3D_DLLEXPORT bool Center(const LabeledImage3d<size_t, BINARY>&, size_t, 
		                           Vector3d<float>&);

template I3D_DLLEXPORT bool Center(const LabeledImage3d<size_t, GRAY8>&, size_t, 
		                           Vector3d<float>&);

template I3D_DLLEXPORT bool Center(const LabeledImage3d<BINARY, BINARY>&, BINARY, 
		                           Vector3d<float>&);


  /****************************************************************/
  /*                      Surface()                               */
  /****************************************************************/

namespace { // local items used in Surface()


// Cube: this class implements the running window of pointers to an image,
// which is needed in te Surface algorithm. This window is a 3x3x3 cube
// for 3D images and 3x3 square for 2D images.

/* Numbering in each xy layer of the cube: 
            
           __________________  (1,1)
        Y /  1  /  8  /  7  / 
         /_____/_____/_____/
        /  2  /  0  /  6  /
       /_____/_____/_____/
      /  3  /  4  /  5  /
     /_____/_____/_____/
 (-1,-1)               X


   The xy layer with the relative z coordinate ==  0 has offset 0,
   the xy layer with the relative z coordinate == -1 has offset 9,
   the xy layer with the relative z coordinate ==  1 has offset 18.
   Therefore, the neighbourhood for the 2D case is defined by first 9 items
   of the offset array.
*/

const int cube3x3x3_off[] = { \
 0, 0, 0, -1, 1, 0, -1, 0, 0, -1,-1, 0,  0,-1, 0,
           1,-1, 0,  1, 0, 0,  1, 1, 0,  0, 1, 0,
 0, 0,-1, -1, 1,-1, -1, 0,-1, -1,-1,-1,  0,-1,-1,
           1,-1,-1,  1, 0,-1,  1, 1,-1,  0, 1,-1,
 0, 0, 1, -1, 1, 1, -1, 0, 1, -1,-1, 1,  0,-1, 1,
           1,-1, 1,  1, 0, 1,  1, 1, 1,  0, 1, 1 };


template <class T>
class Cube {
	bool is_3d_; // do we work in 3D?
	std::vector <const T*> p_; // the pointer window
	Neighbourhood nbh; // neighborhood from which the pointer window 
			   // is generated
	size_t how_long;   // how many time we can move the window by 
			   // incrementing the pointers
	const Image3d<T>& img_; // the image, to which the pointers point
	size_t c_voxel; // number of the current voxel in img_
	T c_val;	// value of the current voxel

	inline bool free(int i) 
	/* Is the i-th voxel of the window free, i.e. is its value different
	   from the value of the current voxel? If the i-th voxel is outside 
	   of th image, it's supposed that it's free. */
	{ return !p_[i] || *p_[i] != c_val; }

	Cube() {}; // private implicit constructor: Cube instance must 
		   // be created by a public constructor
public:
	Cube(const Image3d<T>& img, size_t x=0, size_t y=0, size_t z=0) 
	// stores pointers of neighbours of voxel (x,y,z) of image img to p_.
	// If some of the voxels is out of the image, the corresponding
	// pointer is set to 0.
	: is_3d_(img.GetNumSlices() > 1), p_(is_3d_ ? 27 : 9), 
	  nbh(cube3x3x3_off, is_3d_ ? 27 : 9), img_(img)
	{ how_long = GetFullWindow(img, x,y,z, nbh, p_);
	  c_voxel = img_.GetIndex(x,y,z);
	  c_val = img_.GetVoxel(c_voxel);
	}
	
	void Move() // moves the running window by 1 voxel
	{ c_voxel++;
	  if (how_long) { 
	    MoveFullWindow(p_); 
	    how_long--;
	  }
	  else
	    how_long = GetFullWindow(img_, \
	               img_.GetX(c_voxel), img_.GetY(c_voxel), img_.GetZ(c_voxel), \
		       nbh, p_);
	  c_val = img_.GetVoxel(c_voxel);
	}

	void MoveTo(size_t x, size_t y, size_t z) // moves the window to voxel (x,y,z)
	{ 
	  size_t old_voxel = c_voxel;
	  c_voxel = img_.GetIndex(x,y,z);
	  long diff = c_voxel - old_voxel;

	  if (c_voxel == old_voxel) return; // no change

	  if (diff >=0 && diff <= (long)how_long) { // move pointers
	    typename std::vector<const T*>::iterator i;
	    for (i = p_.begin(); i != p_.end(); ++i) 
	    	if (*i) *i += diff;
	    how_long -= diff;
	  }
	  else  // recompute the window
	    how_long = GetFullWindow(img_, x,y,z, nbh, p_); 

	  c_val = img_.GetVoxel(c_voxel);
	}
		
	const T operator[](size_t i) const {return p_[i];}
	

	inline byte sides_xy() 
	/* Counts, how many xy sides of the central voxel are free 
	   (i.e. the corresponding neighbour voxel has a value 
	   different from value of the current voxel). */
	{ return is_3d_ ? free(9) + free(18) : 0; }
	  
	inline byte sides_xz() // number of free sides in the xz plane
	{ return free(4) + free(8); }
	  
	inline byte sides_yz() // number of free sides in the yz plane
	{ return free(2) + free(6); }


	inline byte edges_xy()
	/* Counts, how many edges perpendicular to the xy plane are free
	   (i.e. the neighbour voxel that shares the edge has a value
	   different from value of the current voxel and neighbour sides
	   of the edge are not free). */
	{ return (free(1) && !free(2) && !free(8)) + \
	         (free(3) && !free(2) && !free(4)) + \
	         (free(5) && !free(4) && !free(6)) + \
	         (free(7) && !free(6) && !free(8));
	}
	
	
	inline byte edges_xz() // number of free edges perpendicular to the xz plane
	{ if (!is_3d_) return 0;
	  return (free(11) && !free( 2) && !free( 9)) + \
	         (free(15) && !free( 6) && !free( 9)) + \
	         (free(20) && !free( 2) && !free(18)) + \
	         (free(24) && !free( 6) && !free(18));
	}

	inline byte edges_yz() // number of free edges perpendicular to the yz plane
	{ if (!is_3d_) return 0;
	  return (free(13) && !free( 4) && !free( 9)) + \
	         (free(17) && !free( 8) && !free( 9)) + \
	         (free(22) && !free( 4) && !free(18)) + \
	         (free(26) && !free( 8) && !free(18));
	}


	inline byte corners() 
	/* number of free corners surrounded by three voxels, whuch have 
	   the same velue as the current voxel. */
	{ if (!is_3d_) return 0;
	  return (free(10) && !free( 1) && !free(17) && !free(11)) + \
	         (free(12) && !free( 3) && !free(11) && !free(13)) + \
	         (free(14) && !free( 5) && !free(13) && !free(15)) + \
	         (free(16) && !free( 7) && !free(15) && !free(17)) + \
	         (free(19) && !free( 1) && !free(26) && !free(20)) + \
	         (free(21) && !free( 3) && !free(20) && !free(22)) + \
	         (free(23) && !free( 5) && !free(22) && !free(24)) + \
	         (free(25) && !free( 7) && !free(24) && !free(26));
	}

}; // class Cube


} // anonymous namespace


template <class LABEL, class VOXEL>
double Surface(const LabeledImage3d<LABEL, VOXEL> &limg, LABEL label, size_t& volume)
{

	// Check, if a component with the given label is available:
	
	#if defined(__GNUG__) && __GNUG__ < 3
          typename map<size_t, ComponentInfo<VOXEL> >::const_iterator ci;
	#else
          typename ComponentInfo<VOXEL>::Container::const_iterator ci;
	#endif
	if ((ci = limg.components.find(label)) == limg.components.end())
	  throw InternalException("LabeledImage3d: No such label.");

	const VOI<PIXELS>& voi = ci->second.voi;

	// Estimate surface of the component:
	
	// Set the voxel size:

	double dX, dY, dZ; // voxel dimensions in microns
	bool is3d = limg.GetNumSlices() > 1;
	Resolution r = limg.GetResolution();
	if (!r.IsDefined()) r = DefaultResolution;
	dX = 1.0 / r.GetX(); 
	dY = 1.0 / r.GetY();
	dZ = is3d ? 1.0 / r.GetZ() : 1.0;

	// Surfaces of voxel sides and diagonal traversals:

	double S_xy = dX * dY;
	double S_xz = dX * dZ;
	double S_yz = dY * dZ;

	double D_xy = dZ * sqrt(dX*dX + dY*dY);
	double D_xz = dY * sqrt(dX*dX + dZ*dZ);
	double D_yz = dX * sqrt(dY*dY + dZ*dZ);

	// Number of sides, edges and corners not belonging to
	// the given object:

	size_t nsides_xy = 0, nsides_xz = 0, nsides_yz = 0,
	       nedges_xy = 0, nedges_xz = 0, nedges_yz = 0,
	       ncorners = 0;
	
	// Count nsides/nedges/ncorners for all voxels:

	Cube<LABEL> win(limg); // running window 3x3x3 of pointers to limg
	int x, y, z;
	volume = 0;
	for (z = voi.offset.z; z < voi.offset.z + (int) voi.size.z; z++)
	  for (y = voi.offset.y; y < voi.offset.y + (int) voi.size.y; y++)
	    for (x = voi.offset.x; x < voi.offset.x + (int) voi.size.x; x++)
	      if (limg.GetVoxel(x,y,z) == label) { 
	      // voxel (x,y,z) is a part of the object
	        volume++;

		win.MoveTo(x,y,z);

		nsides_xy += win.sides_xy();
		nsides_xz += win.sides_xz();
		nsides_yz += win.sides_yz();
		nedges_xy += win.edges_xy();
		nedges_xz += win.edges_xz();
		nedges_yz += win.edges_yz();
		ncorners  += win.corners();
	      } // if


	// Finally compute the surface estimate:

	double S = S_xy + S_xz + S_yz;
	double D = D_xy + D_xz + D_yz;

	double 
	surface = nsides_xy * S_xy + nsides_xz * S_xz + nsides_yz * S_yz \
		+ nedges_xy * (D_xy - S_xz - S_yz) \
		+ nedges_xz * (D_xz - S_xy - S_yz) \
		+ nedges_yz * (D_yz - S_xy - S_xz) \
		+ ncorners * (S - D/2.0);

#ifdef VERBOSE_DEBUG
std::cout << std::endl << "Surface(label = " << int(label) << "):" << std::endl;
std::cout << "S_xy: " << S_xy << " S_xz: " << S_xz << " S_yz: " << S_yz << std::endl;
std::cout << "xy sides: " << nsides_xy << " xz sides: " << nsides_xz << \
" yz sides: " << nsides_yz << std::endl;
std::cout << "xy edges: " << nedges_xy << " xz edges: " << nedges_xz << \
" yz edges: " << nedges_yz << std::endl;
std::cout << "corners: " << ncorners << std::endl;
std::cout << "surface = " << surface << std::endl;
#endif

	return surface;
}

template I3D_DLLEXPORT double Surface(const LabeledImage3d<size_t, GRAY8>&, size_t, size_t&);
template I3D_DLLEXPORT double Surface(const LabeledImage3d<size_t, BINARY>&, size_t, size_t&);
template I3D_DLLEXPORT double Surface(const LabeledImage3d<BINARY, BINARY>&, BINARY, size_t&);
template I3D_DLLEXPORT double Surface(const LabeledImage3d<unsigned short, BINARY>&, unsigned short, size_t&);


template <class LABEL, class VOXEL>
double Roundness(const LabeledImage3d<LABEL, VOXEL> &limg, LABEL label)
{
  // Check, if a component with the given label is available:
  #if defined(__GNUG__) && __GNUG__ < 3
     typename map<size_t, ComponentInfo<VOXEL> >::const_iterator ci;
  #else
     typename ComponentInfo<VOXEL>::Container::const_iterator ci;
  #endif
  if ((ci = limg.components.find(label)) == limg.components.end())
	throw InternalException("LabeledImage3d: No such label.");

  size_t volume;  // number of component voxels (pixels for 2D)
  double surf = Surface(limg, label, volume);  // component surface in micrometers^2 (perimeter in micrometers for 2D) 
  
  bool is3d = limg.GetNumSlices() > 1;
  Resolution res = limg.GetResolution();
  if (!res.IsDefined()) 
	 res = DefaultResolution;
    
  // Compute roundness
  if (!is3d)
  {
    double area = volume / (res.GetX() * res.GetY());  // component area in micrometers^2 for 2D
    return area*4*M_PI/(surf*surf);  
  }

  double vol = volume / (res.GetX() * res.GetY() * res.GetZ());  // component volume in micrometers^3 for 3D
  return vol*vol*36*M_PI/(surf*surf*surf);
}

template I3D_DLLEXPORT double Roundness(const LabeledImage3d<size_t, GRAY8>&, size_t);

template <class LABEL, class VOXEL>
double RoundnessFactor (const LabeledImage3d<LABEL, VOXEL> &limg, LABEL label, 
                        const Vector3d<float> &wcenter, double volume)
{
    // Check, if a component with the given label is available:
    #if defined(__GNUG__) && __GNUG__ < 3
        typename map<size_t, ComponentInfo<VOXEL> >::const_iterator ci;
    #else
        typename ComponentInfo<VOXEL>::Container::const_iterator ci;
    #endif
    if ((ci = limg.components.find(label)) == limg.components.end())
        return -1;

    if (ci->second.volume == 1)
        return 1;

    if (volume == -1)
        volume = MC_Estimate (limg, label, i3d::MC_ESTIMATE_VOLUME);    

    double d_s, d_o = 0;
    float dx, dy, dz;

    Vector3d<float> res = limg.GetResolution().GetRes();

    // Compute average distance to the center of gravity

    const VOI<PIXELS> &voi = ci->second.voi;
    size_t x, y, z;

    for (z = voi.offset.z; z < voi.offset.z + voi.size.z; z++)
        for (y = voi.offset.y; y < voi.offset.y + voi.size.y; y++)
            for (x = voi.offset.x; x < voi.offset.x + voi.size.x; x++)
                if (limg.GetVoxel(x,y,z) == label)
                {
                    dx = wcenter.x - x / res.x;
                    dy = wcenter.y - y / res.y;
                    dz = wcenter.z - z / res.z;
                    d_o += sqrt (dx*dx + dy*dy + dz*dz);
                }

    d_o /= ci->second.volume;
    d_s = 0.75 * pow ((0.75 * volume) / M_PI, 1.0 / 3.0);  // 3/4 from radius of sphere with given volume

    return d_s / d_o;
}

template I3D_DLLEXPORT double RoundnessFactor(const LabeledImage3d<GRAY16, BINARY> &limg, GRAY16 label, 
                                              const Vector3d<float> &wcenter, double volume);

template <class LABEL, class VOXEL>
Boundary GetComponentBoundary(const LabeledImage3d<LABEL, VOXEL> &limg, LABEL label)
{
  // Check, if a component with the given label is available:
  #if defined(__GNUG__) && __GNUG__ < 3
     typename map<size_t, ComponentInfo<VOXEL> >::const_iterator ci;
  #else
     typename ComponentInfo<VOXEL>::Container::const_iterator ci;
  #endif
  if ((ci = limg.components.find(label)) == limg.components.end())
	throw InternalException("LabeledImage3d: No such label.");

  const VOI<PIXELS>& voi = ci->second.voi;
  bool is3d = limg.GetNumSlices() > 1;
  Boundary bdr;

  for (size_t z = voi.offset.z; z < voi.offset.z + voi.size.z; z++)
    for (size_t y = voi.offset.y; y < voi.offset.y + voi.size.y; y++) {
      const LABEL* l = limg.GetVoxelAddr(voi.offset.x, y, z);
      for (size_t x = voi.offset.x; x < voi.offset.x + voi.size.x; x++) {
        if (*l == label) 
		{   
		  // voxel (x,y,z) is a part of the component, check their neighbours
		  if ( limg.OnBorder(x,y,z) || 
			   limg.GetVoxel(x+1,y,z) != label || limg.GetVoxel(x-1,y,z) != label ||
			   limg.GetVoxel(x,y+1,z) != label || limg.GetVoxel(x,y-1,z) != label ||
               (is3d && (limg.GetVoxel(x,y,z+1) != label || limg.GetVoxel(x,y,z-1) != label))
              )
            bdr.push_back(Vector3d<size_t>(x, y, z));
		}
        l++;
      }
	}

  return bdr;
}

template I3D_DLLEXPORT Boundary GetComponentBoundary(const LabeledImage3d<size_t, GRAY8>&, size_t);


template <class LABEL, class VOXEL>
void Radius(const LabeledImage3d<LABEL, VOXEL> &limg, LABEL label, const Vector3d<float>& center, 
			float &min, float &max, float &mean)
{ 
  // Check, if a component with the given label is available:
  #if defined(__GNUG__) && __GNUG__ < 3
     typename map<size_t, ComponentInfo<VOXEL> >::const_iterator ci;
  #else
     typename ComponentInfo<VOXEL>::Container::const_iterator ci;
  #endif
  if ((ci = limg.components.find(label)) == limg.components.end())
	throw InternalException("LabeledImage3d: No such label.");

  float distSum = 0;
  min = std::numeric_limits<float>::max();
  max = 0;
  Boundary bdr = GetComponentBoundary(limg, label);  // component boundary voxels
  
  for (size_t i = 0; i < bdr.size(); i++ )  // process all component boundary voxels
  { 
    Vector3d<float> bdrVoxel = PixelsToMicrons(bdr[i],limg.GetResolution().GetRes());
    float dx = bdrVoxel.x - center.x;
	float dy = bdrVoxel.y - center.y;
	float dz = bdrVoxel.z - center.z;
    float dist = sqrt(dx*dx + dy*dy + dz*dz);
    if (dist > max) 
	  max = dist;
    if (dist < min) 
	  min = dist;
    distSum += dist;
  }
  
  mean = distSum / bdr.size();
}

template I3D_DLLEXPORT void Radius(const LabeledImage3d<size_t, GRAY8>&, size_t, const Vector3d<float>&,
								   float&, float&, float&);


template <class ORIGVOXEL, class LABEL, class VOXEL> 
void IntensityHist(const Image3d<ORIGVOXEL> &img, const LabeledImage3d<LABEL, VOXEL> &limg, LABEL label, Histogram &hist)
{
  // Check, if a component with the given label is available:
  #if defined(__GNUG__) && __GNUG__ < 3
     typename map<size_t, ComponentInfo<VOXEL> >::const_iterator ci;
  #else
     typename ComponentInfo<VOXEL>::Container::const_iterator ci;
  #endif
  if ((ci = limg.components.find(label)) == limg.components.end())
	throw InternalException("LabeledImage3d: No such label.");

  const VOI<PIXELS>& voi = ci->second.voi;
  
  // init histogram
  hist.resize(std::numeric_limits<ORIGVOXEL>::max() + 1);
  hist = 0; 

  for (size_t z = voi.offset.z; z < voi.offset.z + voi.size.z; z++)
    for (size_t y = voi.offset.y; y < voi.offset.y + voi.size.y; y++) {
      const LABEL* l = limg.GetVoxelAddr(voi.offset.x, y, z);
      for (size_t x = voi.offset.x; x < voi.offset.x + voi.size.x; x++) {
        if (*l == label) 
    	{   
		  // voxel (x,y,z) is a part of the component
		  ++hist[img.GetVoxel(x,y,z)];
		}
        l++;
      }
    }
}

template I3D_DLLEXPORT void IntensityHist(const Image3d<GRAY8>&, const LabeledImage3d<size_t, GRAY8>&, size_t, Histogram&);

template <class ORIGVOXEL, class LABEL, class VOXEL> 
void MinMaxComponentValue(const Image3d<ORIGVOXEL> &img, const LabeledImage3d<LABEL, VOXEL> &limg, LABEL label, 
                          ORIGVOXEL &min, ORIGVOXEL &max)
{
  // Check, if a component with the given label is available:
  #if defined(__GNUG__) && __GNUG__ < 3
     typename map<size_t, ComponentInfo<VOXEL> >::const_iterator ci;
  #else
     typename ComponentInfo<VOXEL>::Container::const_iterator ci;
  #endif
  if ((ci = limg.components.find(label)) == limg.components.end())
	throw InternalException("LabeledImage3d: No such label.");

  const VOI<PIXELS>& voi = ci->second.voi;
  
  min = std::numeric_limits<ORIGVOXEL>::max();
  max = std::numeric_limits<ORIGVOXEL>::min();
  
  for (size_t z = voi.offset.z; z < voi.offset.z + voi.size.z; z++)
    for (size_t y = voi.offset.y; y < voi.offset.y + voi.size.y; y++) {
      const LABEL* l = limg.GetVoxelAddr(voi.offset.x, y, z);
      for (size_t x = voi.offset.x; x < voi.offset.x + voi.size.x; x++) {
        if (*l == label) 
    	{   
		  // voxel (x,y,z) is a part of the component
		  ORIGVOXEL value = img.GetVoxel(x,y,z);
		  if (value > max) 
	        max = value;
          if (value < min) 
	        min = value;
		}
        l++;
      }
    }
}

template I3D_DLLEXPORT void MinMaxComponentValue(const Image3d<float>&, const LabeledImage3d<size_t, GRAY8>&, size_t, 
												 float&, float&);
template I3D_DLLEXPORT void MinMaxComponentValue(const Image3d<float>&, const LabeledImage3d<unsigned short, i3d::BINARY>&, 
                                                 unsigned short, float&, float&);


template <class ORIGVOXEL, class LABEL, class VOXEL, class Predicate>
void RemoveComponents(Image3d<ORIGVOXEL> &img, const LabeledImage3d<LABEL, VOXEL> &limg, Predicate &pred)
{
  #if defined(__GNUG__) && __GNUG__ < 3
     typename map<size_t, ComponentInfo<VOXEL> >::const_iterator ci;
  #else
     typename ComponentInfo<VOXEL>::Container::const_iterator ci;
  #endif
  
  for (ci = limg.components.begin(); ci != limg.components.end(); ++ci) 
    if (pred(ci->second,img)) 
	{  
	   const VOI<PIXELS>& voi = ci->second.voi;
       const LABEL label = ci->first;
	   const ORIGVOXEL new_voxel_value = ColorInversion(ci->second.voxel_value);

       for (size_t z = voi.offset.z; z < voi.offset.z + voi.size.z; z++)
         for (size_t y = voi.offset.y; y < voi.offset.y + voi.size.y; y++) 
         {
           const LABEL* l = limg.GetVoxelAddr(voi.offset.x, y, z);
           for (size_t x = voi.offset.x; x < voi.offset.x + voi.size.x; x++) 
           {
             if (*l == label) 
			 {   
		       img.SetVoxel(x,y,z,new_voxel_value);
		     }
             l++;
           }
         }
	}
}

template I3D_DLLEXPORT void RemoveComponents(Image3d<GRAY8>&, const LabeledImage3d<size_t,GRAY8>&, ComponentToRemove<GRAY8>&);
template I3D_DLLEXPORT void RemoveComponents(Image3d<GRAY16>&, const LabeledImage3d<size_t,GRAY16>&, ComponentToRemove<GRAY16>&);

template <class ORIGVOXEL, class LABEL, class VOXEL, class Predicate>
void EraseComponents(Image3d<ORIGVOXEL> &img, LabeledImage3d<LABEL, VOXEL> &limg, Predicate &pred)
{
  #if defined(__GNUG__) && __GNUG__ < 3
     typename map<size_t, ComponentInfo<VOXEL> >::iterator ci, cur_ci;
  #else
     typename ComponentInfo<VOXEL>::Container::iterator ci, cur_ci;
  #endif
  
  bool max_label_changed = false;
  cur_ci = limg.components.begin();

  while (cur_ci != limg.components.end())
  {
    ci = cur_ci++;

    if (pred(ci->second, img)) 
	{  
	   const VOI<PIXELS>& voi = ci->second.voi;
       const LABEL label = ci->first;
	   const ORIGVOXEL new_voxel_value = ColorInversion(ci->second.voxel_value);

       // Check if the labeled image max label has changed (been removed)
       if (label == limg.get_max_label())
           max_label_changed = true;

       // Delete component from image and from labeled image
       for (size_t z = voi.offset.z; z < voi.offset.z + voi.size.z; z++)
         for (size_t y = voi.offset.y; y < voi.offset.y + voi.size.y; y++) 
         {
           LABEL* l = limg.GetVoxelAddr(voi.offset.x, y, z);
           for (size_t x = voi.offset.x; x < voi.offset.x + voi.size.x; x++) 
           {
             if (*l == label) 
			 {   
		       img.SetVoxel(x,y,z,new_voxel_value);
               *l = 0;
		     }
             l++;
           }
         }

       // Delete component from component list
       limg.components.erase (ci);
	}        
  }

  // Find the new max label
  if (max_label_changed)
      limg.FindAndSetMaxLabel();
}

template I3D_DLLEXPORT void EraseComponents(Image3d<BINARY>&, LabeledImage3d<GRAY16,BINARY>&, ComponentToRemove<BINARY>&);
template I3D_DLLEXPORT void EraseComponents(Image3d<GRAY16>&, LabeledImage3d<size_t,GRAY16>&, ComponentToRemove<GRAY16>&);

bool HasIntersection(const VOI<PIXELS> &img_voi, 
							const VOI<PIXELS> &obj_voi, 
							bool bdr_x, 
							bool bdr_y, 
							bool bdr_z)
{ 
  bool x(bdr_x);
  bool y(bdr_y);
  bool z(bdr_z);

  if (x)
  {
    x = (img_voi.size.x != 1 && 
			(obj_voi.offset.x==0 || obj_voi.offset.x==((int)img_voi.size.x-1))) ||
        (img_voi.size.x != 1 && 
			((obj_voi.offset.x+(int)obj_voi.size.x-1)==0 || 
			 (obj_voi.offset.x+(int)obj_voi.size.x-1)==((int)img_voi.size.x-1)));
  }

  if (y)
  {
    y = (img_voi.size.y != 1 && 
			(obj_voi.offset.y==0 || obj_voi.offset.y==((int)img_voi.size.y-1))) ||
        (img_voi.size.y != 1 && 
			((obj_voi.offset.y+(int)obj_voi.size.y-1)==0 ||
			 (obj_voi.offset.y+(int)obj_voi.size.y-1)==((int)img_voi.size.y-1)));
  }

  if (z)
  {
    z = (img_voi.size.z != 1 && 
			(obj_voi.offset.z==0 || obj_voi.offset.z==((int)img_voi.size.z-1))) ||
        (img_voi.size.z != 1 && 
			((obj_voi.offset.z+(int)obj_voi.size.z-1)==0 ||
			 (obj_voi.offset.z+(int)obj_voi.size.z-1)==((int)img_voi.size.z-1)));
  }

  return (x || y || z);
}

/*************************************************************************************************/

template <class VOXEL>
bool ComponentToRemove<VOXEL>::operator()(const ComponentInfo<VOXEL> &c, Image3d<VOXEL> &img)
{  
  if (use_value && value != c.voxel_value)    // component has not the right value of voxels
    return false;

  VOI<PIXELS> img_voi (Vector3d<size_t>(0), img.GetSize()); // voi of the input image

  if (full_voi && c.voi == img_voi)
      return true;
  
  if (HasIntersection(img_voi, c.voi, bdr_x, bdr_y, bdr_z))  // border component filtering
  {
    return ComponentSize(c, use_bdr_min_sz, use_bdr_max_sz, bdr_min_sz, bdr_max_sz);  // border component size filtering
  }
  
  return !ComponentSize(c, use_min_sz, use_max_sz, min_sz, max_sz);  // component size filtering 

};

template <class VOXEL>
bool ComponentToRemove<VOXEL>::ComponentSize(const ComponentInfo<VOXEL> &c, bool use_min, bool use_max, size_t min, size_t max)
{ 
  bool ret(true);

  if (use_voi_volume) {
	  if (use_min)                // is the component voi volume greater than minimal volume ?
	  {
		  ret = (c.voi_volume() >= min);
	  }

	  if (use_max)                // is the component voi volume less than maximal volume ?
	  {
		  ret = ret && (c.voi_volume() <= max);
	  }
  } else {
	  if (use_min)                // is the component volume greater than minimal volume ?
	  {
		  ret = (c.volume >= min);
	  }

	  if (use_max)                // is the component volume less than maximal volume ?
	  {
		  ret = ret && (c.volume <= max);
	  }
  } 

  return ret;
}

template class I3D_DLLEXPORT ComponentToRemove<GRAY8>;
template class I3D_DLLEXPORT ComponentToRemove<GRAY16>;
template class I3D_DLLEXPORT ComponentToRemove<BINARY>;

/**********************************************************************************************************/

/** Added by Vladimir Ulman (xulman@fi.muni.cz), 14.11.2004 */
/**
 * Copy content of bbox from limg into tmp, the "tmp_box" will be one voxel wider at each side.
 * In the case (bbox.size.z == 1) is true, i.e. bbox is 2D, the two dimensionality when widering
 * is preserved which means no "layer" below and above is added. Added voxels (by widering)
 * as well as voxels from original limg, which are not equal to pvalue parameter, are set to 0 --
 * this actually extracts exactly component labelled with pvalue from given bbox.
 */
template <class LABEL,class VOXEL>
void bbox_init_and_copy(Image3d<LABEL> &tmp,const LabeledImage3d<LABEL,VOXEL> &limg,const VOI<PIXELS> &bbox,const LABEL pvalue) {
	size_t x,y,z;
	LABEL *da;

	tmp.SetResolution(limg.GetResolution());

	if (bbox.size.z == 1) {
		//when bbox is actually 2D we shall not wider it in z-axis;
		//if we do make it wider than the erosion will complete remove the content of bbox
		tmp.MakeRoom(bbox.size+Vector3d<size_t>(2,2,0));
		size_t lx=tmp.GetSizeX();
		LABEL *db;
		const LABEL *dc;

		//first and last x-axis line in tmp_box
		da=tmp.GetFirstVoxelAddr();
		db=tmp.GetVoxelAddr(0,tmp.GetSizeY()-1,0);
		for (x=0; x < lx; x++,da++,db++) { *da=0; *db=0; }

		//x-axis lines in between
		da=tmp.GetVoxelAddr(0,1,0);
		for (y=0; y < bbox.size.y; y++) {
			dc=limg.GetVoxelAddr(bbox.offset.x,y+bbox.offset.y,bbox.offset.z);

			*da=0; da++;
			for (x=0; x < bbox.size.x; x++,da++,dc++) *da=(*dc != pvalue)? 0 : pvalue;
			*da=0; da++;
		}
	} else {
		//real 3D bbox, i.e. bbox.size.z > 1
		tmp.MakeRoom(bbox.size+Vector3d<size_t>(2,2,2));
		da=tmp.GetFirstVoxelAddr();

		//first z-slice
		for (y=0; y < tmp.GetSizeY(); y++)
			for (x=0; x < tmp.GetSizeX(); x++,da++) *da=0;

		const LABEL *db;

		//in between first and last z-slices
		for (z=0; z < bbox.size.z; z++) {
			//here in every z-slice: fill first and last x-axis line with zeros
			//and also every first and last voxel at each intermediate x-axis line

			//first whole x-axis line
			for (x=0; x < tmp.GetSizeX(); x++,da++) *da=0;

			//intermediate x-axis line, set first, copy from original, set last...
			for (y=0; y < bbox.size.y; y++) {
				db=limg.GetVoxelAddr(bbox.offset.x,y+bbox.offset.y,z+bbox.offset.z);

				*da=0; da++;
				for (x=0; x < bbox.size.x; x++,da++,db++) *da=(*db != pvalue)? 0 : pvalue;
				*da=0; da++;
			}

			//last whole x-axis line
			for (x=0; x < tmp.GetSizeX(); x++,da++) *da=0;
		}

		//last z-slice
		for (y=0; y < tmp.GetSizeY(); y++)
			for (x=0; x < tmp.GetSizeX(); x++,da++) *da=0;
	}
}


  /****************************************************************/
  /*                       ExtractBorderPoints()                  */
  /****************************************************************/
//for detail description please refer to header file
template <class LABEL>
std::vector< Vector3d<float> >* ExtractBorderPoints(const Image3d<LABEL> &img,const Vector3d<size_t> &offset) {
	Image3d<LABEL> tmpE;
	std::vector< Vector3d<float> > *points=new std::vector< Vector3d<float> >;
	size_t x,y,z,xI,yI,zI;

	tmpE.MakeRoom(img.GetSize());
	Erosion(img,tmpE,nb3D_o6);
#ifdef VERBOSE_DEBUG
	cout << "border_points: tmp_bbox size=(" << img.GetSizeX() << "," << img.GetSizeY() << "," << img.GetSizeZ() << ")\n";
	//cout << "matrix: tmp_bbox first pixel value is " << img.GetVoxel(1,1,0) << endl;
#endif
	Vector3d<float> pt(0.0,0.0,0.0);
            
	//scan through whole VOI of given component which is represented via img parameter
	for (z = 0,zI=offset.z; z < img.GetSizeZ(); z++,zI++)
		for (y = 0,yI=offset.y; y < img.GetSizeY(); y++,yI++) {
			//img and tmpE are of the same dimensions, indexes are valid in both of them
			size_t idx=img.GetIndex(0,y,z);
	
			for (x = 0,xI=offset.x; x < img.GetSizeX(); x++,xI++,idx++)
				if (img.GetVoxel(idx) > tmpE.GetVoxel(idx)) {
					//border voxel detected
					pt+=PixelsToMicrons(Vector3d<float>(xI,yI,zI),img.GetResolution());
	
					//add point pt to list of border points
					points->push_back(pt);

					//set pt vector back to zeros
					pt.x=0.0; pt.y=0.0; pt.z=0.0; //confidental zeroing
				}
		}

	return(points);
}


  /****************************************************************/
  /*                       CreateDistMatrix()                     */
  /****************************************************************/
//for detail description please refer to header file
template <class LABEL,class VOXEL>
components_distance_matrix* CreateDistMatrix(const LabeledImage3d<LABEL,VOXEL> &limgA,const LabeledImage3d<LABEL,VOXEL> &limgB,const double max_dist) {
	//iterators
	typename ComponentInfo<VOXEL>::Container::const_iterator itA;
	typename ComponentInfo<VOXEL>::Container::const_iterator itB;

	//output array
	components_distance_matrix *matrix=\
	  new components_distance_matrix(limgA.get_max_label()+1,limgB.get_max_label()+1);

#ifdef VERBOSE_DEBUG
	cout << "matrix: dimension is " << limgA.get_max_label() << " x " << limgB.get_max_label() << endl;
#endif
	double max_dist_powered=max_dist*max_dist;

	//in order to make the whole computation faster we will implement cache which will store
	//previous border_points_lists of components from limgA; we won't proceed first with
	//extracting all boundary points, storing them and then computing mutual distances,
	//rather we will use cache (bounding box distance and max_dist threshold may discard
	//some outlier from further computations which also means to discard them from extracting
	//border points)
	std::vector< Vector3d<float> > **pointsA_cache;

	size_t i=limgA.get_max_label()+1;
	pointsA_cache=(std::vector< Vector3d<float> >**)malloc(i * sizeof(std::vector< Vector3d<float> >));
	if (pointsA_cache == NULL) 
		throw InternalException("CreateDistMatrix: Not enough memory for border points cache.");
	//initiate cache to be empty
	for (; i > 0; i--) pointsA_cache[i-1]=NULL;

	//how shell we increment "for cycle variables" used in intersection determination?
	//my guess: "at the finer resolution" of each direction respectively
	float x_for=(limgA.GetResolution().GetX() < limgB.GetResolution().GetX())? \
		1.0/limgB.GetResolution().GetX() : 1.0/limgA.GetResolution().GetX();

	float y_for=(limgA.GetResolution().GetY() < limgB.GetResolution().GetY())? \
		1.0/limgB.GetResolution().GetY() : 1.0/limgA.GetResolution().GetY();

	float z_for=(limgA.GetResolution().GetZ() < limgB.GetResolution().GetZ())? \
		1.0/limgB.GetResolution().GetZ() : 1.0/limgA.GetResolution().GetZ();

	//we'll use distance threshold in order to claim when two
	//pixels are considered to be the same, the threshold is determined
	//as half of the finest resolution across all directions
	double zero_threshold;

	zero_threshold=limgA.GetResolution().GetX();
	zero_threshold=(zero_threshold < limgA.GetResolution().GetY())? limgA.GetResolution().GetY() : zero_threshold;
	zero_threshold=(zero_threshold < limgA.GetResolution().GetZ())? limgA.GetResolution().GetZ() : zero_threshold;
	zero_threshold=(zero_threshold < limgB.GetResolution().GetX())? limgB.GetResolution().GetX() : zero_threshold;
	zero_threshold=(zero_threshold < limgB.GetResolution().GetY())? limgB.GetResolution().GetY() : zero_threshold;
	zero_threshold=(zero_threshold < limgB.GetResolution().GetZ())? limgB.GetResolution().GetZ() : zero_threshold;
	zero_threshold=0.5 / zero_threshold; //invert to get distance in microns per one pixel

	//limgA components are along rows (x dimen.) while limgB components are along columns (y dimen.)
	for (itB=limgB.components.begin(); itB != limgB.components.end(); ++itB)
		for (itA=limgA.components.begin(); itA != limgA.components.end(); ++itA) {
			//it[AB]->first is a size_t index into (stl container) map variable components
			//it[AB]->second is a class ComponentInfo
#ifdef VERBOSE_DEBUG
			cout << "------------------\n";
			cout << "matrix: processing: region A #" << itA->first << ", region B #" << itB->first << endl;
			cout << "matrix: bbox of A: offset=" << itA->second.voi.offset << ", size=" << itA->second.voi.size << endl;
			cout << "matrix: bbox of B: offset=" << itB->second.voi.offset << ", size=" << itB->second.voi.size << endl;
#endif
			//first bbox distance...
			//let (ai,bi) and (xi,yi) be the offset and size of i-th rectangle bounding box
			//distance in each axis A from {x,y,z} is given as v_A=a2_A-a1_A; dist_A=max(-v_A - x2_A; v_A - x1_A; 0)

			//bounding box distance in microns, we cannot compute distance first in voxels and
			//then convert it into microns, imagine two labelled images each at different resolution
			Vector3d<float> micro_coordV=PixelsToMicrons(itB->second.voi.offset,limgB.GetResolution());
			micro_coordV-=PixelsToMicrons(itA->second.voi.offset,limgA.GetResolution());

			Vector3d<float> micro_coordA=PixelsToMicrons(itA->second.voi.size,limgA.GetResolution());
			Vector3d<float> micro_coordB=PixelsToMicrons(itB->second.voi.size,limgB.GetResolution());

			double dist=0,q,w;
			q=-micro_coordV.x - micro_coordB.x;
			w= micro_coordV.x - micro_coordA.x;
			w=(q < w)? ((w<0)? 0:w) : ((q<0)?0:q);
			dist+=w*w;

			q=-micro_coordV.y - micro_coordB.y;
			w= micro_coordV.y - micro_coordA.y;
			w=(q < w)? ((w<0)? 0:w) : ((q<0)?0:q);
			dist+=w*w;

			q=-micro_coordV.z - micro_coordB.z;
			w= micro_coordV.z - micro_coordA.z;
			w=(q < w)? ((w<0)? 0:w) : ((q<0)?0:q);
			dist+=w*w;
/*
 			//bounding box distance in pixels
			signed long x,y,z,a,v;

			v=(signed)itB->second.voi.offset.x - (signed)itA->second.voi.offset.x;
			a=-v - (signed)itB->second.voi.size.x;
			x=v - (signed)itA->second.voi.size.x;
			x=(a < x)? ((x<0)? 0:x) : ((a<0)?0:a);

			v=(signed)itB->second.voi.offset.y - (signed)itA->second.voi.offset.y;
			a=-v - (signed)itB->second.voi.size.y;
			y=v - (signed)itA->second.voi.size.y;
			y=(a < y)? ((y<0)? 0:y) : ((a<0)?0:a);

			v=(signed)itB->second.voi.offset.z - (signed)itA->second.voi.offset.z;
			a=-v - (signed)itB->second.voi.size.z;
			z=v - (signed)itA->second.voi.size.z;
			z=(a < z)? ((z<0)? 0:z) : ((a<0)?0:a);

			double dist;
			//bbox distance...
			dist=(double)x*(double)x + (double)y*(double)y+ (double)z*(double)z;
*/
#ifdef VERBOSE_DEBUG
			cout << "matrix: powered bbox distance=" << dist << ", max_dist_powered=" << max_dist_powered << endl;
#endif
			if (dist > max_dist_powered) {
				//bboxes of comp. too far away
				dist=DMAX_FAR_AWAY;
#ifdef VERBOSE_DEBUG
				cout << "matrix: bboxes too far, setting DMAX_FAR_AWAY\n";
#endif
			} else {
				//bboxes close, let's compute the distance more precisely
#ifdef VERBOSE_DEBUG
				cout << "matrix: bboxes close, counting precisely...\n";
#endif
				dist=max_dist;

				//first of all, test non-emptines of intersection;
				//if not empty then distance of components is clearly zero
				//assumption: images are registered

				//problems concerning resolution...
				//in case of limgA and limgB not being at the same resolution,
				//we must compare positions of pixels (used when computing
				//intersection) in microns

				//intersection is given as: offset_left_top=max( voiA_offset , voiB_offset ),
				//offset_right_bottom=min( voiA_offset+voiA_size-1 , voiB_offset+voiB_size-1 )

				//micro_coord[AB] still holds respective voi.size in microns;
				//determining min... will be stored in micro_coordA
				micro_coordA+=PixelsToMicrons(itA->second.voi.offset-Vector3d<int>(1,1,1),limgA.GetResolution());
				micro_coordB+=PixelsToMicrons(itB->second.voi.offset-Vector3d<int>(1,1,1),limgB.GetResolution());

				micro_coordA.x=(micro_coordA.x < micro_coordB.x)? micro_coordA.x : micro_coordB.x;
				micro_coordA.y=(micro_coordA.y < micro_coordB.y)? micro_coordA.y : micro_coordB.y;
				micro_coordA.z=(micro_coordA.z < micro_coordB.z)? micro_coordA.z : micro_coordB.z;

				//determining max... will be stored in micro_coordB
				micro_coordB=PixelsToMicrons(itA->second.voi.offset,limgA.GetResolution());
				micro_coordV=PixelsToMicrons(itB->second.voi.offset,limgB.GetResolution());

				micro_coordB.x=(micro_coordB.x > micro_coordV.x)? micro_coordB.x : micro_coordV.x;
				micro_coordB.y=(micro_coordB.y > micro_coordV.y)? micro_coordB.y : micro_coordV.y;
				micro_coordB.z=(micro_coordB.z > micro_coordV.z)? micro_coordB.z : micro_coordV.z;
#ifdef VERBOSE_DEBUG
				cout << "matrix: intersection in um: " << micro_coordB << " <-> " << micro_coordA << endl;
				cout << "matrix: intersection in terms of A: " << MicronsToPixels(micro_coordB,limgA.GetResolution()) << " <-> " << MicronsToPixels(micro_coordA,limgA.GetResolution()) << endl;
				cout << "matrix: intersection in terms of B: " << MicronsToPixels(micro_coordB,limgB.GetResolution()) << " <-> " << MicronsToPixels(micro_coordA,limgB.GetResolution()) << endl;
				cout << "matrix: step resolution in um: x_for=" << x_for << ", y_for=" << y_for << ", z_for=" << z_for << endl;
#endif
				//scanning through bboxes' intersection testing whether there are voxels from respective
				//components at given position at the same time
				for (micro_coordV.z=micro_coordB.z; micro_coordV.z <= micro_coordA.z; micro_coordV.z+=z_for) 
				  for (micro_coordV.y=micro_coordB.y; micro_coordV.y <= micro_coordA.y; micro_coordV.y+=y_for) 
				    for (micro_coordV.x=micro_coordB.x; micro_coordV.x <= micro_coordA.x; micro_coordV.x+=x_for) {
#ifdef VERBOSE_DEBUG
				      //cout << "matrix: pixels from A: " << MicronsToPixels(micro_coordV,limgA.GetResolution()) << ", B: " << MicronsToPixels(micro_coordV,limgB.GetResolution()) << endl;
#endif
				      if ((limgA.GetVoxel(MicronsToPixels(micro_coordV,limgA.GetResolution())) == itA->first) && \
				          (limgB.GetVoxel(MicronsToPixels(micro_coordV,limgB.GetResolution())) == itB->first)) {
				        micro_coordV.x=micro_coordA.x+1; //stops all three "for cycles"
					micro_coordV.y=micro_coordA.y+1;
					micro_coordV.z=micro_coordA.z+100; //+100 serves as detection how cycles stopped
				      }
				    }

				if (micro_coordV.z >= micro_coordA.z+100) {
					//found point in intersection
					dist=0.0;
				} else if (max_dist > 0.0) {
					//no point in intersection was found while max_dist > 0 which stands for situation:
					//bboxes of components intersects, no point in common intersection and we shall
					//look for some real minimum distance, therefore we must proceed with further computation
					//(max_dist == 0 in fact states for determining whether two componets intersect or not,
					//if they do, 0 is returned in comp._dist._matrix, else max_dist is returned)

				//erosion behaves inapplicable at voxels touching bounding box,
				//we must surround original bbox with one voxel at each side wider one
				Image3d<LABEL> tmp; //empty image

				//because of surrounding original bbox
				Vector3d<int> coord_shift(1,1,1);

				if (pointsA_cache[itA->first] == NULL) {
#ifdef VERBOSE_DEBUG
					cout << "matrix: cache miss for A #" << itA->first << ", computing border points\n";
#endif
					bbox_init_and_copy(tmp,limgA,itA->second.voi,itA->first);
					coord_shift.z=(itA->second.voi.size.z > 1)? 1 : 0;
					pointsA_cache[itA->first]=ExtractBorderPoints(tmp,itA->second.voi.offset+coord_shift);
				} else {
#ifdef VERBOSE_DEBUG
					cout << "matrix: cache hit for A #" << itA->first << endl;
#endif
				}
				std::vector< Vector3d<float> > *pointsA=pointsA_cache[itA->first];

				bbox_init_and_copy(tmp,limgB,itB->second.voi,itB->first);
				coord_shift.z=(itB->second.voi.size.z > 1)? 1 : 0;
				std::vector< Vector3d<float> > *pointsB=ExtractBorderPoints(tmp,itB->second.voi.offset+coord_shift);

				//tmp is not needed now
				tmp.DisposeData();
#ifdef VERBOSE_DEBUG
				cout << "matrix: A #" << itA->first << ": pointsA count=" << pointsA->size() << "; B #" << itB->first << ": pointsB count=" << pointsB->size() << endl;
#endif
				//compute the minimal distance from the two lists of border points of given two components
				//<BRUTE FORCE>
				std::vector< Vector3d<float> >::const_iterator ipA=pointsA->begin(),ipB;
				Vector3d<float> pt(0.0,0.0,0.0);

				for (; ((ipA != pointsA->end()) && (dist > zero_threshold)); ipA++)
					for (ipB=pointsB->begin(); ((ipB != pointsB->end()) && (dist > zero_threshold)); ipB++) {
						pt.x=0.0; pt.y=0.0; pt.z=0.0;
						pt+=*ipB;
						pt-=*ipA;
						q=Norm(pt); //Euclidean norm

						//looking for minimum distance
						if (q < dist) dist=q;
					}
				//</BRUTE FORCE>
				if (dist <= zero_threshold) dist=0.0;

				delete pointsB;
				}
			}

			matrix->set_value(itA->first,itB->first,dist);
#ifdef VERBOSE_DEBUG
			cout << "matrix: value of dist (" << dist << ") stored into comp._dist._matrix[" << itA->first << "," << itB->first << "]\n";
#endif
		}

	//remove content of cache
	i=limgA.get_max_label()+1;
	for (; i > 0; i--) if (pointsA_cache[i-1] != NULL) delete pointsA_cache[i-1];
	//remove cache itself
	free(pointsA_cache);

	return(matrix);
  }

  /************************************************************************
  *
  *   Surface/volume estimation based on marching cubes/squares algorithnm 
  *  
  ***************************************************************************/

  // Some useful macros
  // Convert 8-bit binary to decimal
  #define BD(a,b,c,d,e,f,g,h) ((a << 7) | (b << 6) | (c << 5) | (d << 4) | (e << 3) | (f << 2) | (g << 1) | h)
  // Convert 4-bit binary to decimal  
  #define BD2(a,b,c,d) ((a << 3) | (b << 2) | (c << 1) | d)
  #define SQR(x) ((x) * (x))

  /** Build lookup table for perimeter estimation */
  static void MC_BuildPerimeterLookup (Vector3d<float> res, double *lt)
  {
      // Predpocitani hodnot - x, y, z jsou polovina velikosti strany kvadru (voxelu)
      double x = 1 / (2 * res.x), y = 1 / (2 * res.y);
      double tr = sqrt (x*x + y*y);

      // Case 0 - zadny nebo vsechny body - 2 moznosti / 2 - OK
      lt[BD2(0,0,0,0)] = lt[BD2(1,1,1,1)] = 0;

      // Case 1 - 1 bod + inverze - 8 moznosti / 10 - OK
      lt[BD2(1,0,0,0)] = lt[BD2(0,1,1,1)] = tr;
      lt[BD2(0,1,0,0)] = lt[BD2(1,0,1,1)] = tr;
      lt[BD2(0,0,1,0)] = lt[BD2(1,1,0,1)] = tr;
      lt[BD2(0,0,0,1)] = lt[BD2(1,1,1,0)] = tr;

      // Case 2 - 2 body vedle sebe - 4 moznosti / 14 OK
      lt[BD2(1,1,0,0)] = lt[BD2(0,0,1,1)] = 2 * x;
      lt[BD2(0,1,1,0)] = lt[BD2(1,0,0,1)] = 2 * y;

      // Case 3 - 2 body naproti sobe - 2 moznosti / 16 OK
      lt[BD2(1,0,1,0)] = lt[BD2(0,1,0,1)] = 2 * tr;
  }

  /** Build lookup table for surface estimation */
  void MC_BuildSurfaceLookup (Vector3d<float> res, double *lt)
  {
      // TODO: Zatim se neresi inverze nekterych kombinaci, pro ktere by mel byt vypocet jiny

      // Predpocitani hodnot - x, y, z jsou polovina velikosti strany kvadru (voxelu)
      double x = 1 / (2 * res.x), y = 1 / (2 * res.y), z = 1 / (2 * res.z);

      double tr2 = sqrt (SQR(x*y) + SQR(y*z) + SQR(x*z));
      double tr_n = tr2 / 2;
      double ct_x = 4 * y * z;
      double ct_y = 4 * x * z;
      double ct_z = 4 * x * y;
      double ob_x = sqrt (y*y + z*z) * 2*x;
      double ob_y = sqrt (x*x + z*z) * 2*y;
      double ob_z = sqrt (y*y + x*x) * 2*z;

      // Case 0 - 2 moznosti / 2 - OK
      lt[BD(0,0,0,0,0,0,0,0)] = lt[BD(1,1,1,1,1,1,1,1)] = 0;

      // Case 1 - jeden bod - 16 moznosti / 18 - OK
      lt[BD(1,0,0,0,0,0,0,0)] = lt[BD(0,1,1,1,1,1,1,1)] = tr_n;
      lt[BD(0,1,0,0,0,0,0,0)] = lt[BD(1,0,1,1,1,1,1,1)] = tr_n;
      lt[BD(0,0,1,0,0,0,0,0)] = lt[BD(1,1,0,1,1,1,1,1)] = tr_n;
      lt[BD(0,0,0,1,0,0,0,0)] = lt[BD(1,1,1,0,1,1,1,1)] = tr_n;
      lt[BD(0,0,0,0,1,0,0,0)] = lt[BD(1,1,1,1,0,1,1,1)] = tr_n;
      lt[BD(0,0,0,0,0,1,0,0)] = lt[BD(1,1,1,1,1,0,1,1)] = tr_n;
      lt[BD(0,0,0,0,0,0,1,0)] = lt[BD(1,1,1,1,1,1,0,1)] = tr_n;
      lt[BD(0,0,0,0,0,0,0,1)] = lt[BD(1,1,1,1,1,1,1,0)] = tr_n;

      // Case 2 - 2 body vedle sebe - 24 moznosti / 42 - OK
      lt[BD(1,1,0,0,0,0,0,0)] = lt[BD(0,0,1,1,1,1,1,1)] = ob_x;
      lt[BD(0,1,1,0,0,0,0,0)] = lt[BD(1,0,0,1,1,1,1,1)] = ob_y;
      lt[BD(0,0,1,1,0,0,0,0)] = lt[BD(1,1,0,0,1,1,1,1)] = ob_x;
      lt[BD(1,0,0,1,0,0,0,0)] = lt[BD(0,1,1,0,1,1,1,1)] = ob_y;

      lt[BD(0,0,0,0,1,1,0,0)] = lt[BD(1,1,1,1,0,0,1,1)] = ob_x;
      lt[BD(0,0,0,0,0,1,1,0)] = lt[BD(1,1,1,1,1,0,0,1)] = ob_y;
      lt[BD(0,0,0,0,0,0,1,1)] = lt[BD(1,1,1,1,1,1,0,0)] = ob_x;
      lt[BD(0,0,0,0,1,0,0,1)] = lt[BD(1,1,1,1,0,1,1,0)] = ob_y;

      lt[BD(1,0,0,0,1,0,0,0)] = lt[BD(0,1,1,1,0,1,1,1)] = ob_z;
      lt[BD(0,1,0,0,0,1,0,0)] = lt[BD(1,0,1,1,1,0,1,1)] = ob_z;
      lt[BD(0,0,1,0,0,0,1,0)] = lt[BD(1,1,0,1,1,1,0,1)] = ob_z;
      lt[BD(0,0,0,1,0,0,0,1)] = lt[BD(1,1,1,0,1,1,1,0)] = ob_z;

      // Case 3 - 2 body napric stenou - 12 moznosti / 54 - OK
      lt[BD(1,0,1,0,0,0,0,0)] = tr2;
      lt[BD(1,0,0,0,0,1,0,0)] = tr2;
      lt[BD(1,0,0,0,0,0,0,1)] = tr2;
      lt[BD(0,1,0,1,0,0,0,0)] = tr2;
      lt[BD(0,1,0,0,0,0,1,0)] = tr2;
      lt[BD(0,0,1,0,1,0,0,0)] = tr2;
      lt[BD(0,0,1,0,0,1,0,0)] = tr2;
      lt[BD(0,0,1,0,0,0,0,1)] = tr2;
      lt[BD(0,0,0,1,1,0,0,0)] = tr2;
      lt[BD(0,0,0,1,0,0,1,0)] = tr2;
      lt[BD(0,0,0,0,1,0,1,0)] = tr2;
      lt[BD(0,0,0,0,0,1,0,1)] = tr2;

      // Case 3 inverze - 12 moznosti / 66 - OK
      lt[BD(0,1,0,1,1,1,1,1)] = 3 * tr2;
      lt[BD(0,1,1,1,1,0,1,1)] = 3 * tr2; 
      lt[BD(0,1,1,1,1,1,0,1)] = 3 * tr2;
      lt[BD(1,0,1,0,1,1,1,1)] = 3 * tr2;
      lt[BD(1,0,1,1,1,1,0,1)] = 3 * tr2;
      lt[BD(1,1,0,1,0,1,1,1)] = 3 * tr2;
      lt[BD(1,1,0,1,1,0,1,1)] = 3 * tr2;
      lt[BD(1,1,0,1,1,1,1,0)] = 3 * tr2;
      lt[BD(1,1,1,0,0,1,1,1)] = 3 * tr2;
      lt[BD(1,1,1,0,1,1,0,1)] = 3 * tr2;
      lt[BD(1,1,1,1,0,1,0,1)] = 3 * tr2;
      lt[BD(1,1,1,1,1,0,1,0)] = 3 * tr2;

      // Case 4 - 2 body napric kostkou - 8 moznosti / 74 - OK
      lt[BD(1,0,0,0,0,0,1,0)] = lt[BD(0,1,1,1,1,1,1,0)] = tr2;
      lt[BD(0,1,0,0,0,0,0,1)] = lt[BD(1,0,1,1,1,1,1,0)] = tr2;
      lt[BD(0,1,0,0,1,0,0,0)] = lt[BD(1,0,1,1,0,1,1,1)] = tr2;
      lt[BD(0,0,0,1,0,1,0,0)] = lt[BD(1,1,1,0,1,0,1,1)] = tr2;

      // Case 5 - 3 body na jedne stene - 48 moznosti / 122 - OK
      lt[BD(1,1,1,0,0,0,0,0)] = lt[BD(0,0,0,1,1,1,1,1)] = tr_n + tr2 + 0.5 * ct_z;
      lt[BD(0,1,1,1,0,0,0,0)] = lt[BD(1,0,0,0,1,1,1,1)] = tr_n + tr2 + 0.5 * ct_z;
      lt[BD(1,0,1,1,0,0,0,0)] = lt[BD(0,1,0,0,1,1,1,1)] = tr_n + tr2 + 0.5 * ct_z;
      lt[BD(1,1,0,1,0,0,0,0)] = lt[BD(0,0,1,0,1,1,1,1)] = tr_n + tr2 + 0.5 * ct_z;

      lt[BD(0,0,0,0,1,1,1,0)] = lt[BD(1,1,1,1,0,0,0,1)] = tr_n + tr2 + 0.5 * ct_z;
      lt[BD(0,0,0,0,0,1,1,1)] = lt[BD(1,1,1,1,1,0,0,0)] = tr_n + tr2 + 0.5 * ct_z;
      lt[BD(0,0,0,0,1,0,1,1)] = lt[BD(1,1,1,1,0,1,0,0)] = tr_n + tr2 + 0.5 * ct_z;
      lt[BD(0,0,0,0,1,1,0,1)] = lt[BD(1,1,1,1,0,0,1,0)] = tr_n + tr2 + 0.5 * ct_z;

      lt[BD(1,1,0,0,1,0,0,0)] = lt[BD(0,0,1,1,0,1,1,1)] = tr_n + tr2 + 0.5 * ct_y;
      lt[BD(1,1,0,0,0,1,0,0)] = lt[BD(0,0,1,1,1,0,1,1)] = tr_n + tr2 + 0.5 * ct_y;
      lt[BD(1,0,0,0,1,1,0,0)] = lt[BD(0,1,1,1,0,0,1,1)] = tr_n + tr2 + 0.5 * ct_y;
      lt[BD(0,1,0,0,1,1,0,0)] = lt[BD(1,0,1,1,0,0,1,1)] = tr_n + tr2 + 0.5 * ct_y;

      lt[BD(0,1,1,0,0,1,0,0)] = lt[BD(1,0,0,1,1,0,1,1)] = tr_n + tr2 + 0.5 * ct_x;
      lt[BD(0,1,1,0,0,0,1,0)] = lt[BD(1,0,0,1,1,1,0,1)] = tr_n + tr2 + 0.5 * ct_x;
      lt[BD(0,1,0,0,0,1,1,0)] = lt[BD(1,0,1,1,1,0,0,1)] = tr_n + tr2 + 0.5 * ct_x;
      lt[BD(0,0,1,0,0,1,1,0)] = lt[BD(1,1,0,1,1,0,0,1)] = tr_n + tr2 + 0.5 * ct_x;

      lt[BD(0,0,1,1,0,0,1,0)] = lt[BD(1,1,0,0,1,1,0,1)] = tr_n + tr2 + 0.5 * ct_y;
      lt[BD(0,0,1,1,0,0,0,1)] = lt[BD(1,1,0,0,1,1,1,0)] = tr_n + tr2 + 0.5 * ct_y;
      lt[BD(0,0,1,0,0,0,1,1)] = lt[BD(1,1,0,1,1,1,0,0)] = tr_n + tr2 + 0.5 * ct_y;
      lt[BD(0,0,0,1,0,0,1,1)] = lt[BD(1,1,1,0,1,1,0,0)] = tr_n + tr2 + 0.5 * ct_y;

      lt[BD(1,0,0,1,1,0,0,0)] = lt[BD(0,1,1,0,0,1,1,1)] = tr_n + tr2 + 0.5 * ct_x;
      lt[BD(1,0,0,1,0,0,0,1)] = lt[BD(0,1,1,0,1,1,1,0)] = tr_n + tr2 + 0.5 * ct_x;
      lt[BD(1,0,0,0,1,0,0,1)] = lt[BD(0,1,1,1,0,1,1,0)] = tr_n + tr2 + 0.5 * ct_x;
      lt[BD(0,0,0,1,1,0,0,1)] = lt[BD(1,1,1,0,0,1,1,0)] = tr_n + tr2 + 0.5 * ct_x;

      // Case 6 - hrana a bod naproti - 48 moznosti / 170 - OK
      lt[BD(1,1,0,0,0,0,1,0)] = lt[BD(0,0,1,1,1,1,0,1)] = tr_n + ob_x;
      lt[BD(1,1,0,0,0,0,0,1)] = lt[BD(0,0,1,1,1,1,1,0)] = tr_n + ob_x;
      lt[BD(0,1,1,0,1,0,0,0)] = lt[BD(1,0,0,1,0,1,1,1)] = tr_n + ob_y;
      lt[BD(0,1,1,0,0,0,0,1)] = lt[BD(1,0,0,1,1,1,1,0)] = tr_n + ob_y;
      lt[BD(0,0,1,1,1,0,0,0)] = lt[BD(1,1,0,0,0,1,1,1)] = tr_n + ob_x;
      lt[BD(0,0,1,1,0,1,0,0)] = lt[BD(1,1,0,0,1,0,1,1)] = tr_n + ob_x;
      lt[BD(1,0,0,1,0,1,0,0)] = lt[BD(0,1,1,0,1,0,1,1)] = tr_n + ob_y;
      lt[BD(1,0,0,1,0,0,1,0)] = lt[BD(0,1,1,0,1,1,0,1)] = tr_n + ob_y;

      lt[BD(0,0,1,0,1,1,0,0)] = lt[BD(1,1,0,1,0,0,1,1)] = tr_n + ob_x;
      lt[BD(0,0,0,1,1,1,0,0)] = lt[BD(1,1,1,0,0,0,1,1)] = tr_n + ob_x;
      lt[BD(1,0,0,0,0,1,1,0)] = lt[BD(0,1,1,1,1,0,0,1)] = tr_n + ob_y;
      lt[BD(0,0,0,1,0,1,1,0)] = lt[BD(1,1,1,0,1,0,0,1)] = tr_n + ob_y;
      lt[BD(1,0,0,0,0,0,1,1)] = lt[BD(0,1,1,1,1,1,0,0)] = tr_n + ob_x;
      lt[BD(0,1,0,0,0,0,1,1)] = lt[BD(1,0,1,1,1,1,0,0)] = tr_n + ob_x;
      lt[BD(0,1,0,0,1,0,0,1)] = lt[BD(1,0,1,1,0,1,1,0)] = tr_n + ob_y;
      lt[BD(0,0,1,0,1,0,0,1)] = lt[BD(1,1,0,1,0,1,1,0)] = tr_n + ob_y;

      lt[BD(1,0,1,0,1,0,0,0)] = lt[BD(0,1,0,1,0,1,1,1)] = tr_n + ob_z;
      lt[BD(1,0,0,0,1,0,1,0)] = lt[BD(0,1,1,1,0,1,0,1)] = tr_n + ob_z;
      lt[BD(0,1,0,1,0,1,0,0)] = lt[BD(1,0,1,0,1,0,1,1)] = tr_n + ob_z;
      lt[BD(0,1,0,0,0,1,0,1)] = lt[BD(1,0,1,1,1,0,1,0)] = tr_n + ob_z;
      lt[BD(1,0,1,0,0,0,1,0)] = lt[BD(0,1,0,1,1,1,0,1)] = tr_n + ob_z;
      lt[BD(0,0,1,0,1,0,1,0)] = lt[BD(1,1,0,1,0,1,0,1)] = tr_n + ob_z;
      lt[BD(0,1,0,1,0,0,0,1)] = lt[BD(1,0,1,0,1,1,1,0)] = tr_n + ob_z;
      lt[BD(0,0,0,1,0,1,0,1)] = lt[BD(1,1,1,0,1,0,1,0)] = tr_n + ob_z;

      // Case 7 - 3 nesousedici body - 8 moznosti / 178 - OK
      lt[BD(1,0,1,0,0,1,0,0)] = 3 * tr_n;
      lt[BD(1,0,1,0,0,0,0,1)] = 3 * tr_n;
      lt[BD(0,1,0,1,1,0,0,0)] = 3 * tr_n;
      lt[BD(0,1,0,1,0,0,1,0)] = 3 * tr_n;
      lt[BD(0,1,0,0,1,0,1,0)] = 3 * tr_n;
      lt[BD(0,0,0,1,1,0,1,0)] = 3 * tr_n;
      lt[BD(1,0,0,0,0,1,0,1)] = 3 * tr_n;
      lt[BD(0,0,1,0,0,1,0,1)] = 3 * tr_n;

      // Case 7 inverze - 8 moznosti / 186 - OK
      lt[BD(0,1,0,1,1,0,1,1)] = tr_n + 3 * tr2;
      lt[BD(0,1,0,1,1,1,1,0)] = tr_n + 3 * tr2;
      lt[BD(1,0,1,0,0,1,1,1)] = tr_n + 3 * tr2;
      lt[BD(1,0,1,0,1,1,0,1)] = tr_n + 3 * tr2;
      lt[BD(1,0,1,1,0,1,0,1)] = tr_n + 3 * tr2;
      lt[BD(1,1,1,0,0,1,0,1)] = tr_n + 3 * tr2;
      lt[BD(0,1,1,1,1,0,1,0)] = tr_n + 3 * tr2;
      lt[BD(1,1,0,1,1,0,1,0)] = tr_n + 3 * tr2;

      // Case 8 - jedna strana - 6 moznosti / 192 - OK
      lt[BD(1,1,1,1,0,0,0,0)] = lt[BD(0,0,0,0,1,1,1,1)] = ct_z;
      lt[BD(1,1,0,0,1,1,0,0)] = lt[BD(0,0,1,1,0,0,1,1)] = ct_y;
      lt[BD(0,1,1,0,0,1,1,0)] = lt[BD(1,0,0,1,1,0,0,1)] = ct_x;

      // Case 9 - 4 body s jednim centralnim  - 8 moznosti / 200 - OK
      lt[BD(1,1,0,1,1,0,0,0)] = lt[BD(0,0,1,0,0,1,1,1)] = 3 * tr2;
      lt[BD(1,1,1,0,0,1,0,0)] = lt[BD(0,0,0,1,1,0,1,1)] = 3 * tr2;
      lt[BD(0,1,1,1,0,0,1,0)] = lt[BD(1,0,0,0,1,1,0,1)] = 3 * tr2;
      lt[BD(1,0,1,1,0,0,0,1)] = lt[BD(0,1,0,0,1,1,1,0)] = 3 * tr2;

      // Case 10 - 2 hrany naproti - 6 moznosti / 206 - OK
      lt[BD(1,0,1,0,1,0,1,0)] = lt[BD(0,1,0,1,0,1,0,1)] = 2 * ob_z;
      lt[BD(1,1,0,0,0,0,1,1)] = lt[BD(0,0,1,1,1,1,0,0)] = 2 * ob_x;
      lt[BD(0,1,1,0,1,0,0,1)] = lt[BD(1,0,0,1,0,1,1,0)] = 2 * ob_y;

      // Case 11 a 14 - cik cak had pres 4 body - 24 moznosti *** / 230
      lt[BD(1,1,1,0,1,0,0,0)] = 2 * tr2 + 0.5 * ob_y + 0.5 * ob_z;
      lt[BD(1,1,0,1,0,1,0,0)] = 2 * tr2 + 0.5 * ob_y + 0.5 * ob_z;
      lt[BD(1,1,1,0,0,0,1,0)] = 2 * tr2 + 0.5 * ob_x + 0.5 * ob_z;
      lt[BD(0,1,1,1,0,1,0,0)] = 2 * tr2 + 0.5 * ob_x + 0.5 * ob_z;
      lt[BD(0,1,1,1,0,0,0,1)] = 2 * tr2 + 0.5 * ob_y + 0.5 * ob_z;
      lt[BD(1,0,1,1,0,0,1,0)] = 2 * tr2 + 0.5 * ob_y + 0.5 * ob_z;
      lt[BD(1,1,0,1,0,0,0,1)] = 2 * tr2 + 0.5 * ob_x + 0.5 * ob_z;
      lt[BD(1,0,1,1,1,0,0,0)] = 2 * tr2 + 0.5 * ob_x + 0.5 * ob_z;
      lt[BD(1,0,0,0,1,1,1,0)] = 2 * tr2 + 0.5 * ob_y + 0.5 * ob_z;
      lt[BD(0,1,0,0,1,1,0,1)] = 2 * tr2 + 0.5 * ob_y + 0.5 * ob_z;
      lt[BD(0,1,0,0,0,1,1,1)] = 2 * tr2 + 0.5 * ob_x + 0.5 * ob_z;
      lt[BD(0,0,1,0,1,1,1,0)] = 2 * tr2 + 0.5 * ob_x + 0.5 * ob_z;
      lt[BD(0,0,0,1,0,1,1,1)] = 2 * tr2 + 0.5 * ob_y + 0.5 * ob_z;
      lt[BD(0,0,1,0,1,0,1,1)] = 2 * tr2 + 0.5 * ob_y + 0.5 * ob_z;
      lt[BD(1,0,0,0,1,0,1,1)] = 2 * tr2 + 0.5 * ob_x + 0.5 * ob_z;
      lt[BD(0,0,0,1,1,1,0,1)] = 2 * tr2 + 0.5 * ob_x + 0.5 * ob_z;
      lt[BD(1,0,0,1,1,1,0,0)] = 2 * tr2 + 0.5 * ob_x + 0.5 * ob_y;
      lt[BD(1,1,0,0,1,0,0,1)] = 2 * tr2 + 0.5 * ob_x + 0.5 * ob_y;
      lt[BD(1,1,0,0,0,1,1,0)] = 2 * tr2 + 0.5 * ob_x + 0.5 * ob_y;
      lt[BD(0,1,1,0,1,1,0,0)] = 2 * tr2 + 0.5 * ob_x + 0.5 * ob_y;
      lt[BD(0,1,1,0,0,0,1,1)] = 2 * tr2 + 0.5 * ob_x + 0.5 * ob_y;
      lt[BD(0,0,1,1,0,1,1,0)] = 2 * tr2 + 0.5 * ob_x + 0.5 * ob_y;
      lt[BD(1,0,0,1,0,0,1,1)] = 2 * tr2 + 0.5 * ob_x + 0.5 * ob_y;
      lt[BD(0,0,1,1,1,0,0,1)] = 2 * tr2 + 0.5 * ob_x + 0.5 * ob_y;

      // Case 12 - 3 body a protilehly roh - 24 moznosti / 254 - OK
      lt[BD(1,1,1,0,0,0,0,1)] = lt[BD(0,0,0,1,1,1,1,0)] = tr_n + tr2 + 0.5 * ct_z;
      lt[BD(0,1,1,1,1,0,0,0)] = lt[BD(1,0,0,0,0,1,1,1)] = tr_n + tr2 + 0.5 * ct_z;
      lt[BD(1,0,1,1,0,1,0,0)] = lt[BD(0,1,0,0,1,0,1,1)] = tr_n + tr2 + 0.5 * ct_z;
      lt[BD(1,1,0,1,0,0,1,0)] = lt[BD(0,0,1,0,1,1,0,1)] = tr_n + tr2 + 0.5 * ct_z;

      lt[BD(0,1,1,0,1,0,1,0)] = lt[BD(1,0,0,1,0,1,0,1)] = tr_n + tr2 + 0.5 * ct_x;
      lt[BD(1,0,1,0,0,1,1,0)] = lt[BD(0,1,0,1,1,0,0,1)] = tr_n + tr2 + 0.5 * ct_x;
      lt[BD(0,1,0,1,0,1,1,0)] = lt[BD(1,0,1,0,1,0,0,1)] = tr_n + tr2 + 0.5 * ct_x;
      lt[BD(0,1,1,0,0,1,0,1)] = lt[BD(1,0,0,1,1,0,1,0)] = tr_n + tr2 + 0.5 * ct_x;

      lt[BD(1,1,0,0,0,1,0,1)] = lt[BD(0,0,1,1,1,0,1,0)] = tr_n + tr2 + 0.5 * ct_y;
      lt[BD(0,1,0,1,1,1,0,0)] = lt[BD(1,0,1,0,0,0,1,1)] = tr_n + tr2 + 0.5 * ct_y;
      lt[BD(1,0,1,0,1,1,0,0)] = lt[BD(0,1,0,1,0,0,1,1)] = tr_n + tr2 + 0.5 * ct_y;
      lt[BD(1,1,0,0,1,0,1,0)] = lt[BD(0,0,1,1,0,1,0,1)] = tr_n + tr2 + 0.5 * ct_y;

      // Case 13 - 4 body, 2 a 2 napric - 2 moznosti / 256 - OK
      lt[BD(1,0,1,0,0,1,0,1)] = lt[BD(0,1,0,1,1,0,1,0)] = 4 * tr_n;
  }

  /** Build lookup table for area estimation */
  static void MC_BuildAreaLookup (Vector3d<float> res, double *lt)
  {
      // Case 0 - zadny nebo vsechny body - 2 moznosti / 2 - OK
      lt[BD2(0,0,0,0)] = 0;
      lt[BD2(1,1,1,1)] = 1;

      // Case 1 - 1 bod + inverze - 8 moznosti / 10 - OK
      lt[BD2(1,0,0,0)] = lt[BD2(0,1,0,0)] = lt[BD2(0,0,1,0)] = lt[BD2(0,0,0,1)] = 0.125;
      lt[BD2(0,1,1,1)] = lt[BD2(1,0,1,1)] = lt[BD2(1,1,0,1)] = lt[BD2(1,1,1,0)] = 0.875;

      // Case 2 - 2 body vedle sebe - 4 moznosti / 14 OK
      lt[BD2(1,1,0,0)] = lt[BD2(0,0,1,1)] = lt[BD2(0,1,1,0)] = lt[BD2(1,0,0,1)] = 0.5;

      // Case 3 - 2 body naproti sobe - 2 moznosti / 16 OK
      lt[BD2(1,0,1,0)] = lt[BD2(0,1,0,1)] = 0.25;

      // Adjust table according to the given resolution
      double ratio = res.x * res.y;
      for (int i = 0; i < 16; i++)
          lt[i] /= ratio;
  }

  /** Build lookup table for volume estimation */
  void MC_BuildVolumeLookup (Vector3d<float> res, double *lt)
  {
      // Case 0 - 2 moznosti / 2 - OK
      lt[BD(0,0,0,0,0,0,0,0)] = 0;
      lt[BD(1,1,1,1,1,1,1,1)] = 1;

      // Case 1 - jeden bod - 8 moznosti / 10 - OK
      lt[BD(1,0,0,0,0,0,0,0)] = lt[BD(0,1,0,0,0,0,0,0)] = lt[BD(0,0,1,0,0,0,0,0)] = lt[BD(0,0,0,1,0,0,0,0)] = 0.02083;
      lt[BD(0,0,0,0,1,0,0,0)] = lt[BD(0,0,0,0,0,1,0,0)] = lt[BD(0,0,0,0,0,0,1,0)] = lt[BD(0,0,0,0,0,0,0,1)] = 0.02083;

      // Case 1 inverze - 8 moznosti / 18 - OK
      lt[BD(0,1,1,1,1,1,1,1)] = lt[BD(1,0,1,1,1,1,1,1)] = lt[BD(1,1,0,1,1,1,1,1)] = lt[BD(1,1,1,0,1,1,1,1)] = 0.97917;
      lt[BD(1,1,1,1,0,1,1,1)] = lt[BD(1,1,1,1,1,0,1,1)] = lt[BD(1,1,1,1,1,1,0,1)] = lt[BD(1,1,1,1,1,1,1,0)] = 0.97917;

      // Case 2 - 2 body vedle sebe - 12 moznosti / 30 - OK
      lt[BD(1,1,0,0,0,0,0,0)] = lt[BD(0,1,1,0,0,0,0,0)] = lt[BD(0,0,1,1,0,0,0,0)] = lt[BD(1,0,0,1,0,0,0,0)] = 0.125;
      lt[BD(0,0,0,0,1,1,0,0)] = lt[BD(0,0,0,0,0,1,1,0)] = lt[BD(0,0,0,0,0,0,1,1)] = lt[BD(0,0,0,0,1,0,0,1)] = 0.125;
      lt[BD(1,0,0,0,1,0,0,0)] = lt[BD(0,1,0,0,0,1,0,0)] = lt[BD(0,0,1,0,0,0,1,0)] = lt[BD(0,0,0,1,0,0,0,1)] = 0.125;

      // Case 2 inverze - 12 moznosti / 42 - OK
      lt[BD(0,0,1,1,1,1,1,1)] = lt[BD(1,0,0,1,1,1,1,1)] = lt[BD(1,1,0,0,1,1,1,1)] = lt[BD(0,1,1,0,1,1,1,1)] = 0.875;
      lt[BD(1,1,1,1,0,0,1,1)] = lt[BD(1,1,1,1,1,0,0,1)] = lt[BD(1,1,1,1,1,1,0,0)] = lt[BD(1,1,1,1,0,1,1,0)] = 0.875;
      lt[BD(0,1,1,1,0,1,1,1)] = lt[BD(1,0,1,1,1,0,1,1)] = lt[BD(1,1,0,1,1,1,0,1)] = lt[BD(1,1,1,0,1,1,1,0)] = 0.875;

      // Case 3 - 2 body napric stenou - 12 moznosti / 54 - OK
      lt[BD(1,0,1,0,0,0,0,0)] = lt[BD(1,0,0,0,0,1,0,0)] = lt[BD(1,0,0,0,0,0,0,1)] = lt[BD(0,1,0,1,0,0,0,0)] = 0.04166;
      lt[BD(0,1,0,0,0,0,1,0)] = lt[BD(0,0,1,0,1,0,0,0)] = lt[BD(0,0,1,0,0,1,0,0)] = lt[BD(0,0,1,0,0,0,0,1)] = 0.04166;
      lt[BD(0,0,0,1,1,0,0,0)] = lt[BD(0,0,0,1,0,0,1,0)] = lt[BD(0,0,0,0,1,0,1,0)] = lt[BD(0,0,0,0,0,1,0,1)] = 0.04166;

      // Case 3 inverze - 12 moznosti / 66 - OK
      lt[BD(0,1,0,1,1,1,1,1)] = lt[BD(0,1,1,1,1,0,1,1)] = lt[BD(0,1,1,1,1,1,0,1)] = lt[BD(1,0,1,0,1,1,1,1)] = 0.95834;
      lt[BD(1,0,1,1,1,1,0,1)] = lt[BD(1,1,0,1,0,1,1,1)] = lt[BD(1,1,0,1,1,0,1,1)] = lt[BD(1,1,0,1,1,1,1,0)] = 0.95834;
      lt[BD(1,1,1,0,0,1,1,1)] = lt[BD(1,1,1,0,1,1,0,1)] = lt[BD(1,1,1,1,0,1,0,1)] = lt[BD(1,1,1,1,1,0,1,0)] = 0.95834;

      // Case 4 - 2 body napric kostkou - 8 moznosti / 74 - OK
      lt[BD(1,0,0,0,0,0,1,0)] = lt[BD(0,1,0,0,0,0,0,1)] = lt[BD(0,1,0,0,1,0,0,0)] = lt[BD(0,0,0,1,0,1,0,0)] = 0.04166;
      lt[BD(0,1,1,1,1,1,1,0)] = lt[BD(1,0,1,1,1,1,1,0)] = lt[BD(1,0,1,1,0,1,1,1)] = lt[BD(1,1,1,0,1,0,1,1)] = 0.95834;

      // Case 5 - 3 body na jedne stene - 48 moznosti / 122 - OK
      lt[BD(1,1,1,0,0,0,0,0)] = lt[BD(0,1,1,1,0,0,0,0)] = lt[BD(1,0,1,1,0,0,0,0)] = lt[BD(1,1,0,1,0,0,0,0)] = 0.33333;
      lt[BD(0,0,0,1,1,1,1,1)] = lt[BD(1,0,0,0,1,1,1,1)] =lt[BD(0,1,0,0,1,1,1,1)] =  lt[BD(0,0,1,0,1,1,1,1)] = 0.77777;

      lt[BD(0,0,0,0,1,1,1,0)] = lt[BD(0,0,0,0,0,1,1,1)] = lt[BD(0,0,0,0,1,0,1,1)] = lt[BD(0,0,0,0,1,1,0,1)] = 0.33333;
      lt[BD(1,1,1,1,0,0,0,1)] = lt[BD(1,1,1,1,1,0,0,0)] = lt[BD(1,1,1,1,0,1,0,0)] = lt[BD(1,1,1,1,0,0,1,0)] = 0.77777;

      lt[BD(1,1,0,0,1,0,0,0)] = lt[BD(1,1,0,0,0,1,0,0)] = lt[BD(1,0,0,0,1,1,0,0)] = lt[BD(0,1,0,0,1,1,0,0)] = 0.33333;
      lt[BD(0,0,1,1,0,1,1,1)] = lt[BD(0,0,1,1,1,0,1,1)] = lt[BD(0,1,1,1,0,0,1,1)] = lt[BD(1,0,1,1,0,0,1,1)] = 0.77777;

      lt[BD(0,1,1,0,0,1,0,0)] = lt[BD(0,1,1,0,0,0,1,0)] = lt[BD(0,1,0,0,0,1,1,0)] = lt[BD(0,0,1,0,0,1,1,0)] = 0.33333;
      lt[BD(1,0,0,1,1,0,1,1)] = lt[BD(1,0,0,1,1,1,0,1)] = lt[BD(1,0,1,1,1,0,0,1)] = lt[BD(1,1,0,1,1,0,0,1)] = 0.77777;

      lt[BD(0,0,1,1,0,0,1,0)] = lt[BD(0,0,1,1,0,0,0,1)] = lt[BD(0,0,1,0,0,0,1,1)] = lt[BD(0,0,0,1,0,0,1,1)] = 0.33333;
      lt[BD(1,1,0,0,1,1,0,1)] = lt[BD(1,1,0,0,1,1,1,0)] = lt[BD(1,1,0,1,1,1,0,0)] = lt[BD(1,1,1,0,1,1,0,0)] = 0.77777;

      lt[BD(1,0,0,1,1,0,0,0)] = lt[BD(1,0,0,1,0,0,0,1)] = lt[BD(1,0,0,0,1,0,0,1)] = lt[BD(0,0,0,1,1,0,0,1)] = 0.33333;
      lt[BD(0,1,1,0,0,1,1,1)] = lt[BD(0,1,1,0,1,1,1,0)] = lt[BD(0,1,1,1,0,1,1,0)] = lt[BD(1,1,1,0,0,1,1,0)] = 0.77777;

      // Case 6 - hrana a bod naproti - 24 moznosti / 146 - OK
      lt[BD(1,1,0,0,0,0,1,0)] = lt[BD(1,1,0,0,0,0,0,1)] = lt[BD(0,1,1,0,1,0,0,0)] = lt[BD(0,1,1,0,0,0,0,1)] = 0.14583;
      lt[BD(0,0,1,1,1,0,0,0)] = lt[BD(0,0,1,1,0,1,0,0)] = lt[BD(1,0,0,1,0,1,0,0)] = lt[BD(1,0,0,1,0,0,1,0)] = 0.14583;

      lt[BD(0,0,1,0,1,1,0,0)] = lt[BD(0,0,0,1,1,1,0,0)] = lt[BD(1,0,0,0,0,1,1,0)] = lt[BD(0,0,0,1,0,1,1,0)] = 0.14583;
      lt[BD(1,0,0,0,0,0,1,1)] = lt[BD(0,1,0,0,0,0,1,1)] = lt[BD(0,1,0,0,1,0,0,1)] = lt[BD(0,0,1,0,1,0,0,1)] = 0.14583;

      lt[BD(1,0,1,0,1,0,0,0)] = lt[BD(1,0,0,0,1,0,1,0)] = lt[BD(0,1,0,1,0,1,0,0)] = lt[BD(0,1,0,0,0,1,0,1)] = 0.14583;
      lt[BD(1,0,1,0,0,0,1,0)] = lt[BD(0,0,1,0,1,0,1,0)] = lt[BD(0,1,0,1,0,0,0,1)] = lt[BD(0,0,0,1,0,1,0,1)] = 0.14583;

      // Case 6 inverze - 24 moznosti / 170 - OK
      lt[BD(0,0,1,1,1,1,0,1)] = lt[BD(0,0,1,1,1,1,1,0)] = lt[BD(1,0,0,1,0,1,1,1)] = lt[BD(1,0,0,1,1,1,1,0)] = 0.85417;
      lt[BD(1,1,0,0,0,1,1,1)] = lt[BD(1,1,0,0,1,0,1,1)] = lt[BD(0,1,1,0,1,0,1,1)] = lt[BD(0,1,1,0,1,1,0,1)] = 0.85417;

      lt[BD(1,1,0,1,0,0,1,1)] = lt[BD(1,1,1,0,0,0,1,1)] = lt[BD(0,1,1,1,1,0,0,1)] = lt[BD(1,1,1,0,1,0,0,1)] = 0.85417;
      lt[BD(0,1,1,1,1,1,0,0)] = lt[BD(1,0,1,1,1,1,0,0)] = lt[BD(1,0,1,1,0,1,1,0)] = lt[BD(1,1,0,1,0,1,1,0)] = 0.85417;

      lt[BD(0,1,0,1,0,1,1,1)] = lt[BD(0,1,1,1,0,1,0,1)] = lt[BD(1,0,1,0,1,0,1,1)] = lt[BD(1,0,1,1,1,0,1,0)] = 0.85417;
      lt[BD(0,1,0,1,1,1,0,1)] = lt[BD(1,1,0,1,0,1,0,1)] = lt[BD(1,0,1,0,1,1,1,0)] = lt[BD(1,1,1,0,1,0,1,0)] = 0.85417;

      // Case 7 - 3 nesousedici body - 8 moznosti / 178 - OK
      lt[BD(1,0,1,0,0,1,0,0)] = 0.06249;
      lt[BD(1,0,1,0,0,0,0,1)] = 0.06249;
      lt[BD(0,1,0,1,1,0,0,0)] = 0.06249;
      lt[BD(0,1,0,1,0,0,1,0)] = 0.06249;
      lt[BD(0,1,0,0,1,0,1,0)] = 0.06249;
      lt[BD(0,0,0,1,1,0,1,0)] = 0.06249;
      lt[BD(1,0,0,0,0,1,0,1)] = 0.06249;
      lt[BD(0,0,1,0,0,1,0,1)] = 0.06249;

      // Case 7 inverze - 8 moznosti / 186 - OK
      lt[BD(0,1,0,1,1,0,1,1)] = 0.93751;
      lt[BD(0,1,0,1,1,1,1,0)] = 0.93751;
      lt[BD(1,0,1,0,0,1,1,1)] = 0.93751;
      lt[BD(1,0,1,0,1,1,0,1)] = 0.93751;
      lt[BD(1,0,1,1,0,1,0,1)] = 0.93751;
      lt[BD(1,1,1,0,0,1,0,1)] = 0.93751;
      lt[BD(0,1,1,1,1,0,1,0)] = 0.93751;
      lt[BD(1,1,0,1,1,0,1,0)] = 0.93751;

      // Case 8 - jedna strana - 6 moznosti / 192 - OK
      lt[BD(1,1,1,1,0,0,0,0)] = lt[BD(0,0,0,0,1,1,1,1)] = 0.5;
      lt[BD(1,1,0,0,1,1,0,0)] = lt[BD(0,0,1,1,0,0,1,1)] = 0.5;
      lt[BD(0,1,1,0,0,1,1,0)] = lt[BD(1,0,0,1,1,0,0,1)] = 0.5;

      // Case 9 - 4 body s jednim centralnim  - 8 moznosti / 200 - OK
      lt[BD(1,1,0,1,1,0,0,0)] = lt[BD(0,0,1,0,0,1,1,1)] = 0.5;
      lt[BD(1,1,1,0,0,1,0,0)] = lt[BD(0,0,0,1,1,0,1,1)] = 0.5;
      lt[BD(0,1,1,1,0,0,1,0)] = lt[BD(1,0,0,0,1,1,0,1)] = 0.5;
      lt[BD(1,0,1,1,0,0,0,1)] = lt[BD(0,1,0,0,1,1,1,0)] = 0.5;

      // Case 10 - 2 hrany naproti - 6 moznosti / 206 - OK
      lt[BD(1,0,1,0,1,0,1,0)] = lt[BD(0,1,0,1,0,1,0,1)] = 0.25;
      lt[BD(1,1,0,0,0,0,1,1)] = lt[BD(0,0,1,1,1,1,0,0)] = 0.25;
      lt[BD(0,1,1,0,1,0,0,1)] = lt[BD(1,0,0,1,0,1,1,0)] = 0.25;

      // Case 11 a 14 - cik cak had pres 4 body - 24 moznosti *** / 230
      lt[BD(1,1,1,0,1,0,0,0)] = 0.5;
      lt[BD(1,1,0,1,0,1,0,0)] = 0.5;
      lt[BD(1,1,1,0,0,0,1,0)] = 0.5;
      lt[BD(0,1,1,1,0,1,0,0)] = 0.5;
      lt[BD(0,1,1,1,0,0,0,1)] = 0.5;
      lt[BD(1,0,1,1,0,0,1,0)] = 0.5;
      lt[BD(1,1,0,1,0,0,0,1)] = 0.5;
      lt[BD(1,0,1,1,1,0,0,0)] = 0.5;
      lt[BD(1,0,0,0,1,1,1,0)] = 0.5;
      lt[BD(0,1,0,0,1,1,0,1)] = 0.5;
      lt[BD(0,1,0,0,0,1,1,1)] = 0.5;
      lt[BD(0,0,1,0,1,1,1,0)] = 0.5;
      lt[BD(0,0,0,1,0,1,1,1)] = 0.5;
      lt[BD(0,0,1,0,1,0,1,1)] = 0.5;
      lt[BD(1,0,0,0,1,0,1,1)] = 0.5;
      lt[BD(0,0,0,1,1,1,0,1)] = 0.5;
      lt[BD(1,0,0,1,1,1,0,0)] = 0.5;
      lt[BD(1,1,0,0,1,0,0,1)] = 0.5;
      lt[BD(1,1,0,0,0,1,1,0)] = 0.5;
      lt[BD(0,1,1,0,1,1,0,0)] = 0.5;
      lt[BD(0,1,1,0,0,0,1,1)] = 0.5;
      lt[BD(0,0,1,1,0,1,1,0)] = 0.5;
      lt[BD(1,0,0,1,0,0,1,1)] = 0.5;
      lt[BD(0,0,1,1,1,0,0,1)] = 0.5;

      // Case 12 - 3 body a protilehly roh - 24 moznosti / 254 - OK
      lt[BD(1,1,1,0,0,0,0,1)] = lt[BD(0,0,0,1,1,1,1,0)] = 0.35416;
      lt[BD(0,1,1,1,1,0,0,0)] = lt[BD(1,0,0,0,0,1,1,1)] = 0.35416;
      lt[BD(1,0,1,1,0,1,0,0)] = lt[BD(0,1,0,0,1,0,1,1)] = 0.35416;
      lt[BD(1,1,0,1,0,0,1,0)] = lt[BD(0,0,1,0,1,1,0,1)] = 0.35416;

      lt[BD(0,1,1,0,1,0,1,0)] = lt[BD(1,0,0,1,0,1,0,1)] = 0.35416;
      lt[BD(1,0,1,0,0,1,1,0)] = lt[BD(0,1,0,1,1,0,0,1)] = 0.35416;
      lt[BD(0,1,0,1,0,1,1,0)] = lt[BD(1,0,1,0,1,0,0,1)] = 0.35416;
      lt[BD(0,1,1,0,0,1,0,1)] = lt[BD(1,0,0,1,1,0,1,0)] = 0.35416;

      lt[BD(1,1,0,0,0,1,0,1)] = lt[BD(0,0,1,1,1,0,1,0)] = 0.35416;
      lt[BD(0,1,0,1,1,1,0,0)] = lt[BD(1,0,1,0,0,0,1,1)] = 0.35416;
      lt[BD(1,0,1,0,1,1,0,0)] = lt[BD(0,1,0,1,0,0,1,1)] = 0.35416;
      lt[BD(1,1,0,0,1,0,1,0)] = lt[BD(0,0,1,1,0,1,0,1)] = 0.35416;

      // Case 13 - 4 body, 2 a 2 napric - 2 moznosti / 256 - OK
      lt[BD(1,0,1,0,0,1,0,1)] = 0.08332;
      lt[BD(0,1,0,1,1,0,1,0)] = 0.08332;

      // Adjust table according to the given resolution
      double ratio = res.x * res.y * res.z;
      for (int i = 0; i < 256; i++)
          lt[i] /= ratio;
  }

  // Surface/volume estimation. See regions.h for info.
  template <class LABEL, class VOXEL>
  double MC_Estimate (const LabeledImage3d<LABEL, VOXEL> &limg, LABEL label, McEstimateType type)
  {
      // Check, if a component with the given label is available:
      #if defined(__GNUG__) && __GNUG__ < 3
        typename map<size_t, ComponentInfo<VOXEL> >::const_iterator ci;
      #else
        typename ComponentInfo<VOXEL>::Container::const_iterator ci;
      #endif

      if ((ci = limg.components.find(label)) == limg.components.end())
          return 0;

      size_t x, y, z;
      double lt[256], surface = 0;
      const i3d::VOI<PIXELS> &voi = ci->second.voi;    

      if (limg.GetSizeX() > 1 && limg.GetSizeY() > 1 && limg.GetSizeZ() > 1)     // 3D image
      {
          // Fill look-up table
          if (type == MC_ESTIMATE_SURFACE)
            MC_BuildSurfaceLookup (limg.GetResolution().GetRes(), lt);
          else
            MC_BuildVolumeLookup (limg.GetResolution().GetRes(), lt);

          // Estimate surface/volume
          for (z = voi.offset.z; z < voi.offset.z + voi.size.z + 1; z++)
              for (y = voi.offset.y; y < voi.offset.y + voi.size.y + 1; y++)
                  for (x = voi.offset.x; x < voi.offset.x + voi.size.x + 1; x++)
                  {                    
                      int n = 0;

                      // Top cube side
                      if (z > 0)
                      {
                          if (y > 0)
                          {
                              if (x > 0)
                                  if (limg.GetVoxel (x - 1, y - 1, z - 1) == label) n |= 1 << 7;
                              if (x < limg.GetSizeX())
                                  if (limg.GetVoxel (x, y - 1, z - 1) == label) n |= 1 << 6;
                          }

                          if (y < limg.GetSizeY())
                          {
                              if (x > 0)
                                  if (limg.GetVoxel (x - 1, y, z - 1) == label) n |= 1 << 4;
                              if (x < limg.GetSizeX())
                                  if (limg.GetVoxel (x, y, z - 1) == label) n |= 1 << 5;
                          }
                      }

                      // Bottom cube side
                      if (z < limg.GetSizeZ())
                      {
                          if (y > 0)
                          {
                              if (x > 0)
                                  if (limg.GetVoxel (x - 1, y - 1, z) == label) n |= 1 << 3;
                              if (x < limg.GetSizeX())
                                  if (limg.GetVoxel (x, y - 1, z) == label) n |= 1 << 2;
                          }

                          if (y < limg.GetSizeY())
                          {
                              if (x > 0)
                                  if (limg.GetVoxel (x - 1, y, z) == label) n |= 1;
                              if (x < limg.GetSizeX())
                                  if (limg.GetVoxel (x, y, z) == label) n |= 1 << 1;
                          }
                      }

                      surface += lt[n];
                  }
      }

      if (limg.GetSizeX() > 1 && limg.GetSizeY() > 1 && limg.GetSizeZ() == 1)  // 2D image in XY plane
      {
          // Fill look-up table
          if (type == MC_ESTIMATE_SURFACE)
              MC_BuildPerimeterLookup (limg.GetResolution().GetRes(), lt);
          else
              MC_BuildAreaLookup (limg.GetResolution().GetRes(), lt);

          // Estimate perimeter/area
          for (y = voi.offset.y; y < voi.offset.y + voi.size.y + 1; y++)
              for (x = voi.offset.x; x < voi.offset.x + voi.size.x + 1; x++)
              {
                  int n = 0;

                  if (y > 0)
                  {
                      if (x > 0)
                          if (limg.GetVoxel (x - 1, y - 1, 0) == label) n |= 0x08;
                      if (x < limg.GetSizeX())
                          if (limg.GetVoxel (x, y - 1, 0) == label) n |= 0x04;
                  }

                  if (y < limg.GetSizeY())
                  {
                      if (x > 0)
                          if (limg.GetVoxel (x - 1, y, 0) == label) n |= 0x01;
                      if (x < limg.GetSizeX())
                          if (limg.GetVoxel (x, y, 0) == label) n |= 0x02;
                  }

                  surface += lt[n];
              }
      }

      return surface;
  }

  template I3D_DLLEXPORT double MC_Estimate (const LabeledImage3d<size_t, BINARY>&, 
      size_t, McEstimateType);
  template I3D_DLLEXPORT double MC_Estimate (const LabeledImage3d<unsigned short, BINARY>&, 
      unsigned short, McEstimateType);



/* Explicit instantiation */

template class I3D_DLLEXPORT LabeledImage3d<size_t, GRAY8>;
template class I3D_DLLEXPORT LabeledImage3d<byte, GRAY8>;
template class I3D_DLLEXPORT LabeledImage3d<size_t, BINARY>;
template class I3D_DLLEXPORT LabeledImage3d<byte, BINARY>;
template class I3D_DLLEXPORT LabeledImage3d<unsigned short, BINARY>;
template class I3D_DLLEXPORT LabeledImage3d<size_t, GRAY16>;
template class I3D_DLLEXPORT LabeledImage3d<byte, GRAY16>;

template struct I3D_DLLEXPORT components_distance_matrix* CreateDistMatrix(const LabeledImage3d<size_t,BINARY> &,const LabeledImage3d<size_t,BINARY> &,const double);

template struct I3D_DLLEXPORT components_distance_matrix* CreateDistMatrix(const LabeledImage3d<size_t,GRAY8> &,const LabeledImage3d<size_t,GRAY8> &,const double);

template I3D_DLLEXPORT void bbox_init_and_copy(Image3d<size_t> &tmp,const LabeledImage3d<size_t,GRAY8> &limg,const VOI<PIXELS> &bbox,const size_t pvalue);

template I3D_DLLEXPORT std::vector< Vector3d<float> >* ExtractBorderPoints(const Image3d<size_t> &img,const Vector3d<size_t> &offset);

} // namespace i3d
