/*
 * hrca_histogram.cpp
 *
 * Pavel Matula (pam@fi.muni.cz) 2006
 */

#include <string.h>
#include "hrca_histogram.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

namespace i3d {

/************************************************************/

template <class T> void HRCA_Histogram<T>::Compute(const T *data, long n)
{
	Clear();
	for (long i = 0; i < n; ++i) {
		Add(data[i]);
	}
}


template <class T> long HRCA_Histogram<T>::GetLeftPercentilID(int percent) const 
{
	if (!(percent >= 0 && percent <= 100))
	  throw LibException("GetLeftPercentilID(): ASSERT FAILED: NOT(percent >= 0 && percent <= 100)");

	if (!(_suma > 0))
	  throw LibException("GetLeftPercentilID(): ASSERT FAILED: NOT(_suma > 0)");

	long k = 0;
	long b = _suma*percent/100;
	long i;

	for (i = 0; i < _size; ++i) {
		if (k > b) 
			return i - 1;
		k += _hist[i]; 
	}
	return _size - 1;
};

/************************************************************/

template <class T> long HRCA_Histogram<T>::GetRightPercentilID(int percent) const 
{
	if (!(percent >= 0 && percent <= 100))
	  throw LibException("GetRightPercentilID(): ASSERT FAILED: NOT(percent >= 0 && percent <= 100)");

	if (!(_suma > 0))
	  throw LibException("GetRightPercentilID(): ASSERT FAILED: NOT(_suma > 0)");

	long k = 0;
	long b = _suma*percent/100;
	long i;

	for (i = _size - 1; i >= 0; --i) {
		if (k > b) 
			return i + 1;
		k += _hist[i]; 
	}
	return 0;
};

/************************************************************/

template class HRCA_Histogram<GRAY8>;
template class HRCA_Histogram<GRAY16>;
template class HRCA_Histogram<size_t>;

} // namespace hrca
