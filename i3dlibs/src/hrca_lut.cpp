/*
 * hrca_lut.cpp
 *
 * Pavel Matula (pam@fi.muni.cz) 2006
 */

#include <string.h>
#include "hrca_lut.h"
#ifndef _MSC_VER
#include <cmath>
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

namespace i3d {

/*************************************************************************************/

template <class ITYPE, class OTYPE> 
void HRCA_LUT<ITYPE, OTYPE>::CreateLinearStretch(const HRCA_Histogram<ITYPE> &histogram)
{
	CreatePercentileStretch(histogram, 0, 0);
}

/*************************************************************************************/

template <class ITYPE, class OTYPE> 
void HRCA_LUT<ITYPE, OTYPE>::CreatePercentileStretch(const HRCA_Histogram<ITYPE> &histogram, int lp, int rp)
{
	if (!(lp >= 0))
	  throw LibException("CreatePercentileStretch(): ASSERT FAILED: NOT(lp >= 0)");
	if (!(rp >= 0))
	  throw LibException("CreatePercentileStretch(): ASSERT FAILED: NOT(rp >= 0)");
	if (!(lp <= 100))
	  throw LibException("CreatePercentileStretch(): ASSERT FAILED: NOT(lp <= 100)");
	if (!(rp <= 100))
	  throw LibException("CreatePercentileStretch(): ASSERT FAILED: NOT(rp <= 100)");
	if (!(lp + rp <= 100))
	  throw LibException("CreatePercentileStretch(): ASSERT FAILED: NOT(lp + rp <= 100)");

	long li = histogram.GetLeftPercentilID(lp);
	long ri = histogram.GetRightPercentilID(rp);
	
	if (!(li <= ri))
	  throw LibException("CreatePercentileStretch(): ASSERT FAILED: NOT(li <= ri)");
	float a, b;

	if (li == ri) {
		a = 1.0;
		b = -li;
	} else {
		a = (std::numeric_limits<OTYPE>::max() + 1.0)/(ri - li);
		b = - a * li;
	}

	LUT<ITYPE,OTYPE>::CreateAffine(a, b);
}

/*************************************************************************************/

template <> void HRCA_LUT<GRAY8, RGB>::CreatePercentileStretch(const HRCA_Histogram<GRAY8> &histogram, int lp, int rp)
{
	if (!(lp >= 0))
	  throw LibException("CreatePercentileStretch(): ASSERT FAILED: NOT(lp >= 0)");
	if (!(rp >= 0))
	  throw LibException("CreatePercentileStretch(): ASSERT FAILED: NOT(rp >= 0)");
	if (!(lp <= 100))
	  throw LibException("CreatePercentileStretch(): ASSERT FAILED: NOT(lp <= 100)");
	if (!(rp <= 100))
	  throw LibException("CreatePercentileStretch(): ASSERT FAILED: NOT(rp <= 100)");
	if (!(lp + rp <= 100))
	  throw LibException("CreatePercentileStretch(): ASSERT FAILED: NOT(lp + rp <= 100)");

	long li = histogram.GetLeftPercentilID(lp);
	long ri = histogram.GetRightPercentilID(rp);
	
	if (!(li <= ri))
	  throw LibException("CreatePercentileStretch(): ASSERT FAILED: NOT(li <= ri)");
	float a, b;

	if (li == ri) {
		a = 1.0;
		b = -li;
	} else {
		a = (std::numeric_limits<GRAY8>::max() + 1.0)/(ri - li);
		b = - a * li;
	}

	float r;
	for (long i = 0; i < _lut_size; ++i) 
    {
		r = a * i + b;
        if (r < std::numeric_limits<i3d::GRAY8>::min()) {
			_lut[i] = i3d::RGB (static_cast<i3d::GRAY8>(std::numeric_limits<i3d::GRAY8>::min()));
		} else if (r > std::numeric_limits<i3d::GRAY8>::max()) {
			_lut[i] = i3d::RGB (static_cast<i3d::GRAY8>(std::numeric_limits<i3d::GRAY8>::max()));
		} else {
			_lut[i] = i3d::RGB (static_cast<i3d::GRAY8>(round(r)));
		}
	} 
}

/*************************************************************************************/

template <> 
void HRCA_LUT<GRAY16, RGB>::CreatePercentileStretch(const HRCA_Histogram<GRAY16> &histogram, int lp, int rp)
{ 
	throw InternalException("i3d::HRCA_LUT<i3d::GRAY16, i3d::RGB>::CreatePercentileStretch "
		                    "cannot be used for RGB images.");
}

/*************************************************************************************/

template <class ITYPE, class OTYPE> 
void HRCA_LUT<ITYPE, OTYPE>::CreateEqualization(const HRCA_Histogram<ITYPE> &histogram)
{
	throw LibException("CreateEqualization(): todo, Not implemented!");
}

/*************************************************************************************/

template <class ITYPE, class OTYPE> void HRCA_LUT<ITYPE, OTYPE>::CreateGamma(const HRCA_Histogram<ITYPE> &histogram, float gamma)
{
	throw LibException("CreateGamma(): todo, Not implemented!");
}

/*************************************************************************************/

template class I3D_DLLEXPORT HRCA_LUT<GRAY8, GRAY8>;
template class I3D_DLLEXPORT HRCA_LUT<GRAY16, GRAY8>;
template class I3D_DLLEXPORT HRCA_LUT<GRAY8, GRAY16>;
template class I3D_DLLEXPORT HRCA_LUT<GRAY16, GRAY16>;
template class I3D_DLLEXPORT HRCA_LUT<GRAY8, RGB>;
template class I3D_DLLEXPORT HRCA_LUT<GRAY16, RGB>;
template class I3D_DLLEXPORT HRCA_LUT<size_t, RGB>;

/*************************************************************************************/

} // namespace hrca
