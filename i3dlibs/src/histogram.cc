/*
 * i3dlib - image manipulation library
 *
 * Copyright (C) 2000-2006   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

 /*
 * FILE: histogram.cc
 *
 * histogram manipulation
 *
 * Petr Matula (pem@fi.muni.cz) 2001
 */

#ifdef __GNUG__
#pragma implementation
#endif

#include <limits>
#include <vector>
#include <algorithm>

#include "histogram.h"

namespace i3d {

/****************************************************************************/

void ClearHist(Histogram &hist) 
{ 
	hist = 0; 
}

/****************************************************************************/

void IntensityHist(const Image3d<float> &img, Histogram &hist, size_t NumberOfBins, size_t SizeOfStep)
{
    if (NumberOfBins ==(size_t) (((size_t) std::numeric_limits<GRAY8>::max()) + 1))
    {
	Image3d<GRAY8> gimg;
	FloatToGray(img, gimg);
	IntensityHist(gimg, hist);
    }
    else
	
    {
	 float max, step;
	 const float * voxel, * last;
	 size_t idx;
	 max = std::numeric_limits<float>::min();
	 voxel =  img.GetVoxelAddr(0); 
	 last = img.GetVoxelAddr(img.GetSizeX()-1,img.GetSizeY()-1,img.GetSizeZ()-1) + 1;
	 while (voxel != last)
	 {
	     if (*voxel > max)
		 max = * voxel;
	     voxel++;
	 }
	 hist.resize(NumberOfBins);
	 if ( ((size_t) round(max)) < NumberOfBins * SizeOfStep)
	     NumberOfBins = ((size_t) round(max / ((float) SizeOfStep)));
	 
	 voxel =  img.GetVoxelAddr(0);
	 step = (float) SizeOfStep;
	 while (voxel != last)
	 {
	     idx = (size_t)(round ((*voxel) / step));
	     if (idx < NumberOfBins)
		 ++hist[idx];
	     voxel++;
	 }
    }
}

/****************************************************************************/
  void IntensityHist(const Image3d<float> &img, Histogram &hist, const std::gslice &slc)
	{
		Image3d<GRAY8> gimg;
		FloatToGray(img, gimg);
		IntensityHist(gimg, hist, slc);
	}

	/****************************************************************************/

  void IntensityHist(const Image3d<double> &img, Histogram &hist, size_t NumberOfBins, size_t SizeOfStep)
  {
	throw InternalException("Histogram for VOXEL = double is not implemented");
	return;
  }

/****************************************************************************/
  void IntensityHist(const Image3d<double> &img, Histogram &hist, const std::gslice &slc)
  {
	throw InternalException("Histogram for VOXEL = double is not implemented");
	return;
  }

/****************************************************************************/
  //the following two functions are only patches necessary for VectField3d class usage (Z.Hrncir):
  void IntensityHist(const Image3d<Vector3d<float> > &img, Histogram &hist, const std::gslice &slc) {
    throw InternalException("Histogram for VOXEL = Vector3d<float> is not implemented");
    return;
  }

/****************************************************************************/
   void IntensityHist(const Image3d<Vector3d<double> > &img, Histogram &hist, const std::gslice &slc) {
    throw InternalException("Histogram for VOXEL = Vector3d<double> is not implemented");
    return;
  }

/****************************************************************************/
  template <class T> void InitHist(Histogram &hist)
  {
#ifdef I3D_DEBUG
    if (std::numeric_limits<T>::min() != 0)
      throw InternalException("Histogram: Incompatible input type");
#endif
    
    hist.resize(std::numeric_limits<T>::max() + 1);
    
    ClearHist(hist);
  }

/****************************************************************************/
  template <class T> void ComputeHist(const T* data, Histogram &hist, size_t size, size_t stride = 1)
  {
    static size_t i;
    if (stride == 1)
      for (i = 0; i < size; i++, data++)
        ++hist[*data];
    else
      for (i = 0; i < size; i++, data += stride)
        ++hist[*data];
  }

/****************************************************************************/

  template <class T, class MT> void ComputeHist(const T* data, Histogram &hist, size_t size, const MT* mask)
  {
    static size_t i;
	for (i = 0; i < size; i++, data++, mask++) {
		if (*mask != 0)
			++hist[*data];
	}
  }
/****************************************************************************/

/****************************************************************************/
  void IntensityHist(const Image3d<RGB> &img, Histogram &hist, int channel)
  {
    hist.resize(std::numeric_limits<GRAY8>::max() + 1);
    ClearHist(hist);

	if (channel < 0 || channel > 2)
		return;

	ComputeHist(((unsigned char *) img.GetFirstVoxelAddr()) + channel, hist, img.GetImageSize(), 3);
  }

/****************************************************************************/
  template <class T>
  void IntensityHist(const Image3d<T> &img, Histogram &hist)
  {
    InitHist<T>(hist);
    ComputeHist(img.GetFirstVoxelAddr(), hist, img.GetImageSize());
    // cout << "Sum of the histogram: " << hist.sum() << endl;
  }


/****************************************************************************/

  template <class T, class MT> 
  void IntensityHist(const Image3d<T> &img, Histogram &hist, const Image3d<MT> &mask)
  {
	  InitHist<T>(hist);
	  ComputeHist(img.GetFirstVoxelAddr(), hist, img.GetImageSize(), mask.GetFirstVoxelAddr());
  }

/****************************************************************************/
  
  template <class T>
  void IntensityHist(const Image3d<T> &img, Histogram &hist, const std::gslice &slc)
  {
    static size_t j,k;
    const T *first = img.GetVoxelAddr(slc.start());
    const T *voxel;
    
    InitHist<T>(hist);
#if 0 //def I3D_DEBUG
    cout << "gslice: " << slc.start() << ",";
    for (j = 0; j < slc.size().size(); j++)
      cout << "(" << slc.size()[j] << "," << slc.stride()[j] << ")";
    cout << endl;
    cout << "Max index: " << img.GetImageSize() << endl;
#endif

    switch (slc.size().size())
      {
      case 1:
        ComputeHist(first, hist, slc.size()[0], slc.stride()[0]);
        break;
        
      case 2:
        for (j = 0, voxel = first; j < slc.size()[1]; j++, voxel += slc.stride()[1])
          ComputeHist(voxel, hist, slc.size()[0], slc.stride()[0]);
        break;
        
      case 3:
        for (k = 0; k < slc.size()[2]; k++)
          {
            voxel = first + slc.stride()[2]*k;
            for (j = 0; j < slc.size()[1]; j++)
              {
                ComputeHist(voxel, hist, slc.size()[0], slc.stride()[0]);
                voxel += slc.stride()[1];
              }
          }
        break;
        
      default: 
        throw InternalException("IntesityHist: gslice size is too big");
      }
  }


/****************************************************************************/
  void ComputeHistInfo(const Histogram &hist, HistInfo &hi, \
  	double thres_factor)
  {
    size_t i, j;
    #define NEGLIGIBLE(i) (i < (hist_sum/50000))
    // test, if i is negligible in comparison with the size of
    // the image

    #define NOT_MUCH_LESS(x,y) (x > 0.75 * y)

    size_t len = hist.size();
    size_t hist_sum = hist.sum();


	// cout << " hist_sum = " << hist_sum << endl;

    // Find global maximum for a sum of three consecutive columns:

    Histogram hs1(hist);
    for (i = 1; i < len -1; i++) hs1[i] += hist[i-1] + hist[i+1];
    hi.mean1 = std::max_element(&hs1[0], &hs1[len]) - &hs1[0];
    // index of the max element
    
    // Estimate std. dev. of the distribution around the maximum:

    long SumLeftHisto, dist;
    double Hstddev;

    if (hi.mean1 <= 1)
      hi.stdDev1 = 0;
    else {
      Hstddev = 0.0; SumLeftHisto = 0;
      for (i = 0; i < hi.mean1; i++) {
        SumLeftHisto += hist[i];
	dist = hi.mean1 - i;
	Hstddev += ((double)hist[i])*dist*dist;
      }
      hi.stdDev1 = (size_t)ceil(sqrt(2*Hstddev/(2*SumLeftHisto+hist[hi.mean1]-1)));
      // std. dev. is computed from the formula
      // sqrt(1/(n-1)*sum_i(dist_i)), 
      // where n is the sum of histogram columns on the left from the maximum (mean1)
      // + the same sum as if this portion of the histogram would be mirrored 
      //   around the maximum
      // + hist[mean1] itself.
    }
      
    size_t range = std::max(hi.mean1/25, (size_t)1);
    size_t SumLocal, SumLast;
    bool descending;

    // Find next maximum after the global one on sums of (2*range+1) consecutive
    // columns, which is located after the first step up. More exactly, we are 
    // searching the righmost point which is not much lower than the highest of
    // such points.

    unsigned long maxSum = 0; 
    SumLocal = 0;
    descending = true; 

    i = hi.mean1 + (int)(2*hi.stdDev1);
    if (i < range || i + range > len-1) {
      hi.mean2 =0; hi.stdDev2 = 0; hi.threshold = 0;
      return;
    }

    for (j = i-range; j <= i+range; j++) SumLocal += hist[j];

    for (i++; i < len-range; i++) {
       SumLast = SumLocal;
       SumLocal = SumLocal - hist[i-range-1] + hist[i+range];
       if (SumLocal > SumLast) descending = false;
       if (!descending && NOT_MUCH_LESS(SumLocal, maxSum)) { 
         // we are somewhere after the first rise after mean1+2stdDev1
         // and the current column is higher or at lest comparable to the
         // best column yet found 
         hi.mean2 = i;
	 if (SumLocal > maxSum) maxSum = SumLocal;
       }
    }

    // Find minimum for a sum of three consecutive columns between 
    // mean1 and mean2 (approx. lowest point between both maxima):

    if (!NEGLIGIBLE(maxSum)) { // HistoDualStatistics():  if (maxSum > 20)
      hi.threshold = std::min_element(&hs1[hi.mean1], &hs1[hi.mean2]) - &hs1[0];
      // index of the min. point
    }
    else
      hi.threshold = 0;

    // Shift threshold according to thres_factor:

    size_t minLocal = hs1[hi.threshold];

    if (hi.threshold > hi.mean1)
    {
      if (thres_factor >= 1.0) // shift the min. point to the left
        // find the first column less than (min. point value)*thres_factor:
	for (i = hi.mean1; i < hi.threshold; i++) {
	  if (hs1[i] < minLocal*thres_factor) {
            hi.threshold =i;
	    break;
	  }
        }
      else // thres_factor < 1.0: shift the min. point to the right
        // find the last column less than (min. point value)/thres_factor:
	for (i = hi.mean2; i > hi.threshold; i--) {
	  if (hs1[i] < minLocal/thres_factor) {
            hi.threshold =i;
	    break;
	  }
	}
    }

    // Estimate std. dev. of the distribution around the second maximum:
    
    if (hi.mean2 <= 1)
      hi.stdDev2 = 0;
    else {
      Hstddev = 0.0; SumLeftHisto = 0;
      for (i = hi.threshold; i < hi.mean2; i++) {
        SumLeftHisto += hist[i];
	dist = hi.mean2 - i;
	Hstddev += ((double)hist[i])*dist*dist;
      }
      hi.stdDev2 = (size_t)ceil(sqrt(2*Hstddev/(2*SumLeftHisto+hist[hi.mean2]-1)));
      // std. dev. is computed from the formula
      // sqrt(1/(n-1)*sum_i(dist_i)), 
      // where n is the sum of histogram columns between the min. point (threshold)
      //   and the second maximum (mean2)
      // + the same sum as if this portion of the histogram would be mirrored 
      //   around the second maximum
      // + hist[mean2] itself.
    }
  }


/****************************************************************************/
  void SmoothHist(const Histogram &in, Histogram &out)
  {
    size_t len = in.size();
    if (len < 2) { out = in; return;}

    out.resize(len);
    for (size_t i = 1; i < len-1; i++)
      out[i] = (in[i-1] + in[i] + in[i+1]) / 3;

    out[0]     = (in[0] + in[1]) / 2;
    out[len-1] = (in[len-1] + in[len-2]) / 2;
  }

/****************************************************************************/
  void SmoothHist(const Histogram &in, Histogram &out, size_t diff)
  {
    long s;
    size_t l, u;
    size_t len = in.size();
    if (len < 2*diff) { out = in; return;}

    out.resize(len);
    for (size_t i = 0; i < len; i++) {
      s = 0; 
      l = std::max((long)0, (long)i-(long)diff);
      u = std::min(len, i+diff+1);
      for (size_t j = l; j < u; j++) s += in[j]; 
      out[i] = s / (u-l);
    }
  }

/****************************************************************************/

  unsigned long CumulativeHistogram(const Histogram &in, Histogram &out)
  {
	  out.resize(in.size());
	  unsigned long sum = 0;
	  for (size_t i = 0; i < in.size(); i++) {
		  sum += in[i];
		  out[i] = sum;
	  }
	  return sum;
  }

/****************************************************************************/
  void DiffHist(const Histogram &in, Histogram &out)
  {
    size_t len = in.size();
    if (len < 2) { out = in; return;}

    out.resize(len);
    for (size_t i = 1; i < len-1; i++)
      out[i] = in[i-1] - in[i+1];

    out[0]     = -in[1];
    out[len-1] = in[len-2];
  }

/****************************************************************************/
  void RecDilHist(const Histogram &mask, Histogram &out)
  {
    size_t i;
    size_t len = mask.size();
    
    for (i = 1; i < len; i++)
      out[i] = std::min(mask[i], std::max(out[i],out[i-1]));
    for (i = len-1; i > 0; i--)
      out[i-1] = std::min(mask[i-1], std::max(out[i],out[i-1]));
  }

/****************************************************************************/
  void RecEroHist(const Histogram &mask, Histogram &out)
  {
    size_t i;
    size_t len = mask.size();
    
    for (i = 1; i < len; i++)
      out[i] = std::max(mask[i], std::min(out[i],out[i-1]));
    for (i = len-1; i > 0; i--)
      out[i-1] = std::max(mask[i-1], std::min(out[i],out[i-1]));
  }

   
/****************************************************************************/
  typedef struct {
    int i; // index of maximum
    unsigned long v; // value of maximum
    int li; // index of nearest maximum bigger than v with index < i
    unsigned long lv; // value of nearest maximum bigger than v with index < i
    int ri; // index of nearest maximum bigger than v with index > i
    unsigned long rv; // value of nearest maximum bigger than v with index > i
  } maxstruct;
  
/****************************************************************************/
  void DynMax(const Histogram &in, Histogram &out)
  {
    // find regional maxima
    RegMaxHist(in,out);

    std::vector<maxstruct> maxs;
    maxs.resize(std::count(&out[0],&out[out.size()], (unsigned long)1));

    size_t j = 0;
    unsigned long gmax = in[0];
    unsigned long gmin = in[0];

	{
    // find global maximum, minimum and prepare maxs array
    for (size_t i = 0; i < out.size(); i++)
      {
        gmax = std::max(gmax, in[i]);  
        gmin = std::min(gmin, in[i]);
        
        if (out[i] == 1)
          {
            maxs[j].i = i;
            maxs[j].v = in[i];
            j++;
          }
      }
	}

	{
    // Forward scan - find nearest maximum bigger than v on the left
    maxs[0].li = -1;
    maxs[0].lv = gmax + 1;
    for (size_t i = 1; i < maxs.size(); i++)
      {
        size_t j = i - 1;
        
        if (maxs[i].v < maxs[j].v)
          {
            maxs[i].li = maxs[j].i;
            maxs[i].lv = maxs[j].v;
          }
        else
          {
            while (maxs[i].v >= maxs[j].lv)
              j--;
            
            maxs[i].li = maxs[j].li;
            maxs[i].lv = maxs[j].lv;
          }
      }
    }

	{
    // Backward scan - find nearest maximum bigger than v on the right
    maxs[maxs.size() - 1].ri = -1;
    maxs[maxs.size() - 1].rv = gmax + 1;
    
	
    for (int i = maxs.size()-2; i >= 0; i--)
      {
        size_t j = i + 1;

        if (maxs[i].v < maxs[j].v)
          {
            maxs[i].ri = maxs[j].i;
            maxs[i].rv = maxs[j].v;
          }
        else
          {
            while (maxs[i].v >= maxs[j].rv)
              j++;
            
            maxs[i].ri = maxs[j].ri;
            maxs[i].rv = maxs[j].rv;
          }
      }
	}

#ifdef I3D_DEBUG
//      for (size_t i = 0; i < maxs.size(); i++)
//        {
//          cout << i << ": (" << maxs[i].i  << "," << maxs[i].v  << ") "
//               << "Left : (" << maxs[i].li << "," << maxs[i].lv << ") "
//               << "Right: (" << maxs[i].ri << "," << maxs[i].rv << ")"
//               << endl;
//            }
#endif
	
    // Compute dynamics
    for (size_t i = 0; i < maxs.size(); i++)
      {
        if (maxs[i].li < 0 && maxs[i].ri < 0) // global maximum
          {
            out[maxs[i].i] = maxs[i].v - gmin;
            continue;
          }
        
        unsigned long lmin_left = maxs[i].v;
        
        // left maximum
        if (maxs[i].li > 0)
          {
            int j = maxs[i].i - 1;
            
            while (j >= maxs[i].li)
              {
                lmin_left = std::min(lmin_left,in[j]);
                j--;
              }
          }
        
        unsigned long lmin_right = maxs[i].v;
        
        // right maximum
        if (maxs[i].ri > 0)
          {
            int j = maxs[i].i + 1;
            
            while (j <= maxs[i].ri)
              {
                lmin_right = std::min(lmin_right,in[j]);
                j++;
              }
          }

        out[maxs[i].i] = maxs[i].v - std::max(lmin_left, lmin_right);
      }
    
  }

/****************************************************************************/
  template <class T>
  void MapLevels(const Histogram &h,std::vector<T> &map_func, int thres)
  {
    size_t i, max_pos = HistMax(h);
    
    size_t first = max_pos;
    while (first > 0 && h[first] > 0)
      first--;
    
    size_t last = max_pos;
    while (last < h.size() - 1 && h[last] > 0)
      last++;

    //first = (max_pos + first) / 2;
    first = max_pos + thres;

    map_func.resize(h.size());
    
    for (i = 0; i < first; i++)
      map_func[i] = 0;
    for (i = first; i < last; i++)
      map_func[i] = ((i - first) * h.size()) / (last - first);
    for (i = last; i < h.size(); i++)
      map_func[i] = h.size();
  }

/****************************************************************************/
  template <class T>
  void MapPermileLevels(const Histogram &h, std::vector<T> &map_func, int perm)
  {  
    map_func.resize(h.size());

	 if (h.size() == 0)
		 return;
    
    size_t sum = 0;
	size_t i;
	for (i = 0; i < h.size(); ++i)
		sum += h[i];

	size_t bound = (sum*perm)/1000;
	
	size_t left = 0;
	size_t ls = 0;
	for (left = 0; left < h.size(); ++left)
	{ 
		ls += h[left];
		if (ls > bound)
			break;
	}

   size_t right = 0;
   size_t rs = 0;

	for (right = h.size(); right > 0; --right)
	{ 
		rs += h[right-1];
		if (rs > bound)
			break;
	}

    for (i = 0; i < left; i++)
      map_func[i] = 0;
    for (i = left; i < right; i++)
      map_func[i] = ((i - left) * h.size()) / (right - left);
    for (i = right; i < h.size(); i++)
      map_func[i] = h.size() - 1;
  }

/****************************************************************************
 * unimodal threshold finder - a straigth line is drawn from the peak
 * to the high end of the histogram. The threshold point is selected
 * as the histogram index "i" that maximises the perpendicular distance
 * between the line and the point (i, hist[i])
 * 
 * - source: Rosin, P. L. Unimodal thresholding,  Pattern recongnition 
 *   34 (2001) 2083-2096, published by Elsevier
 *
 */
size_t FindUnimodalThreshold (const Histogram & hist)
{
  size_t m = HistMax (hist);

  size_t last = hist.size () - 1;

  while ((last > m) && (hist[last] == 0))
    {
      last--;
    }

  if (last == m)
    return m;

  float a, b, c, d;
  b = -1.0;
  a = ((float) hist[last] - hist[m]) / ((float) last - m);
  c = (float) hist[m] - a * m;
  d = sqrt (a * a + b * b);

  float max_dist = 0.0;
  size_t max_idx = m;

  for (size_t x = m; x < last; x++)
    {
      float
        dist = fabs (a * x + b * hist[x] + c) / d;
      if (dist > max_dist)
        {
          max_dist = dist;
          max_idx = x;
        }
    }

  return max_idx;
}


/****************************************************************************
 * Otsu threshold finder 
 *
 */
size_t FindOtsuThreshold (const Histogram & hist)
{  
  size_t first_nonzero = 0; // first histogram index with nonzero number of pixels

  while (first_nonzero < hist.size() && hist[first_nonzero] == 0) {
    ++first_nonzero;
  }
  
  size_t number_of_pixels = 0; // total number of pixels in the histogram
    
  for (size_t i = 0; i < hist.size(); ++i) {
    number_of_pixels += hist[i];
  }
  
  double d_number_of_pixels = (double) number_of_pixels;
  
  double d_omega_0 = 0.0;
  double d_omega_1 = 0.0;
  
  double d_mean_0 = 0.0;
  double d_mean_1 = 0.0;
  
  double d_mi_0;
  double d_mi_1;

  double d_temp_div;
  
  for (size_t i = 0; i <= first_nonzero; ++i) {
    d_temp_div = (double) hist[i] / d_number_of_pixels;
    d_omega_0 += d_temp_div;
    d_mean_0 += (double) i * d_temp_div;
  }
  
  d_omega_1 = 1.0 - d_omega_0;
  
  for (size_t i = first_nonzero + 1; i < hist.size(); ++i) {
    d_mean_1 += ((double) i * (double) hist[i]) / d_number_of_pixels;
  }
  
  size_t threshold = first_nonzero;
  double d_sigma_s_2, d_max_sigma_s_2 = 0.0;
 

  for (size_t current_threshold = first_nonzero + 1; current_threshold < hist.size(); ++current_threshold) {
    
    if (d_omega_0 != 0.0 && d_omega_1 != 0.0) {
      d_mi_0 = d_mean_0 / d_omega_0;
      d_mi_1 = d_mean_1 / d_omega_1;     
      
      d_sigma_s_2 = d_omega_0 * d_omega_1 * (d_mi_1 - d_mi_0) * (d_mi_1 - d_mi_0);
      
      if (d_sigma_s_2 > d_max_sigma_s_2) {
	d_max_sigma_s_2 = d_sigma_s_2;
	threshold = current_threshold - 1;
      }

    }

    d_temp_div = (double) hist[current_threshold] / d_number_of_pixels;
    
    d_omega_0 += d_temp_div;
    d_omega_1 -= d_temp_div;
    
    d_temp_div *= (double) current_threshold;

    d_mean_0 += d_temp_div;
    d_mean_1 -= d_temp_div;    

  }

  return threshold;

}

/****************************************************************************/

/****************************************************************************
 * ISODATA threshold finder 
 *
 */
size_t FindIsoDataThreshold (const Histogram & hist, int init_thr_type)
{
	size_t threshold;
	size_t last_threshold = 0; 

	double mu_fg, mu_bg;
	size_t sum;

	// compute initial threshold
	switch (init_thr_type) {
	case 1:
		mu_fg = 0.0;
		sum = 0;
		for (size_t i = 0; i < hist.size(); ++i) {
			sum += hist[i];
			mu_fg += hist[i] * i;
		}
		if (sum) 
			threshold = size_t(mu_fg / sum + 0.5);
		else
			init_thr_type = 0;
		break;
	case 2:
		{
			size_t lowest_value = hist.size() - 1;
			size_t highest_value = 0;
			for (size_t i = 0; i < hist.size(); ++i) {
				if (hist[i]) {
					if (i < lowest_value)
						lowest_value = i;
					if (i > highest_value)
						highest_value = i;
				}
			}
			if (lowest_value <= highest_value)
				threshold = lowest_value + size_t((highest_value - lowest_value) / 2.0 + 0.5);
			else
				init_thr_type = 0;
		}
		break;
	}

	if (!init_thr_type)
		threshold = hist.size()/2;

	// do the iterations
	while (threshold != last_threshold) {
		sum = 0;
		mu_bg = 0.0;
		mu_fg = 0.0;
		last_threshold = threshold;

		for (size_t i = 0; i < threshold; ++i) {
			sum += hist[i];
			mu_bg += hist[i] * i;
		}

		if (sum) 
			mu_bg /= sum;

		sum = 0;
		for (size_t i = threshold; i < hist.size(); ++i) {
			sum += hist[i];
			mu_fg += hist[i] * i;
		}

		if (sum) 
			mu_fg /= sum;

		threshold = size_t((mu_fg + mu_bg) / 2 + 0.5);
	}

  return threshold;

}

/****************************************************************************/


/* explicit instantiation - begin */
  template I3D_DLLEXPORT void MapLevels(const Histogram &h, std::vector<GRAY8> &map_func, int thres);
  template I3D_DLLEXPORT void MapLevels(const Histogram &h, std::vector<GRAY16> &map_func, int thres);
  template I3D_DLLEXPORT void MapPermileLevels(const Histogram &h, std::vector<GRAY8> &map_func, int thres);
  template I3D_DLLEXPORT void MapPermileLevels(const Histogram &h, std::vector<GRAY16> &map_func, int thres);
  template I3D_DLLEXPORT void IntensityHist(const Image3d<GRAY8> &img, Histogram &hist);
  template I3D_DLLEXPORT void IntensityHist(const Image3d<GRAY8> &img, Histogram &hist, const Image3d<GRAY8> &mask);
  template I3D_DLLEXPORT void IntensityHist(const Image3d<GRAY16> &img, Histogram &hist, const Image3d<GRAY8> &mask);
  template I3D_DLLEXPORT void IntensityHist(const Image3d<GRAY16> &img, Histogram &hist, const Image3d<GRAY16> &mask);
  template I3D_DLLEXPORT void IntensityHist(const Image3d<GRAY8> &img, Histogram &hist, const Image3d<int> &mask);
  template I3D_DLLEXPORT void IntensityHist(const Image3d<BINARY> &img, Histogram &hist);
  template I3D_DLLEXPORT void IntensityHist(const Image3d<GRAY16> &img, Histogram &hist);
  template I3D_DLLEXPORT void IntensityHist(const Image3d<GRAY8> &img, Histogram &hist, const std::gslice &slc);
  template I3D_DLLEXPORT void IntensityHist(const Image3d<GRAY16> &img, Histogram &hist, const std::gslice &slc);
  template I3D_DLLEXPORT void IntensityHist(const Image3d<int> &img, Histogram &hist, const std::gslice &slc);
  template I3D_DLLEXPORT void IntensityHist(const Image3d<size_t> &img, Histogram &hist, const std::gslice &slc);
  /* explicit instantiation - end */


}
