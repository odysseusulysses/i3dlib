/*
 * i3dlib - image manipulation library
 *
 * Copyright (C) 2000-2006   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 * FILE: contours.cc
 *
 * Functions for finding contours (isolines) in 2D images
 *
 * Honza Huben� (xhubeny@fi.muni.cz) 2004
 *
 */

#include "contours.h"
#include <vector>
#include "draw.h"
namespace i3d {

/*
   Cell Cases:
   x -- points in
   o -- points out
   
   x-------x  x-------x  x-------x  x-------x  x-----\-o  x-----\-x  x---|---o  
   |       |  |       |  |       |  |       |  |      \|  |      \|  |   |   |  
   |       |  \       |  |       /  ---------  |       \  \       \  |   |   |  
   |       |  |\      |  |      /|  |       |  |       |  |\      |  |   |   |  
   x-------x  o-\-----x  x-----/-o  o-------o  x-------x  x-\-----x  x---|---o  
   Case 0     Case 1     Case 2     Case 3     Case 4     Case 5     Case 6    

   x-/-----o  o-/-----x  o---|---x  o-/-----x  o-----\-x  o-------o  o-------o  
   |/      |  |/      |  |   |   |  |/      |  |      \|  |       |  |       |  
   /       |  /       |  |   |   |  /       /  |       \  ---------  |       /  
   |       |  |       |  |   |   |  |      /|  |       |  |       |  |      /|  
   o-------o  x-------x  o---|---x  x-----/-o  o-------o  x-------x  o-----/-x             
   Case 7     Case 8     Case 9     Case 10    Case 11    Case 12    Case 13   

   o-------o  o-------o  
   |       |  |       |  
   \       |  |       |  
   |\      |  |       |  
   x-\-----o  o-------o  
   Case 14    Case 15         

   This cases are used in ApplyMarchingSquaresOnOneSlide by using the
   variable mask the variable state is chosen.
 */


/*
   Edge Cases
 o -- Cell verteces
 x -- contour verteces

 3    2    2
  o---x---o
  |       |
  |       |
  |       |
3 x       x 1
  |       |
  |       |
  |       |
  o---x---o
 0    0    1
*/
typedef int edge[5];

static edge edgeCases[16]={
 {-1, -1, -1, -1, -1},
 {0, 3, -1, -1, -1}, // the edge for case 1 (see Cell Cases above) is from point x-0 to x-3 (see Edge Cases above) 
 {1, 0, -1, -1, -1}, // the edge for case 2 (see Cell Cases above) is from point x-1 to point x-0
 {1, 3, -1, -1, -1}, // and so on...
 {2, 1, -1, -1, -1},
 {0, 3, 2, 1, -1},
 {2, 0, -1, -1, -1},
 {2, 3, -1, -1, -1},
 {3, 2, -1, -1, -1},
 {0, 2, -1, -1, -1},
 {1, 0, 3, 2, -1},
 {1, 2, -1, -1, -1},
 {3, 1, -1, -1, -1},
 {0, 1, -1, -1, -1},
 {3, 0, -1, -1, -1},
 {-1, -1, -1, -1, -1}};

//Definition of o-vertices (See Edge Cases above) between which lies the x-vertices (See Edge Cases above)
 //Point x-0 lies between o-0 and o-1 x-2 lies between o-2 and o-3 and so on.
 static  int edgeVert [4][2] = {{0,1}, {1,3}, {2,3}, {0,2}}; 
 
 static int mask[4] = {1,2,8,4}; // static mask used to estimate the state


template <class VOXEL>
void ApplyMarchingSquaresOnOneSlice (i3d::Image3d<VOXEL> & ImgIn,VOXEL value,
                                     std::vector<Point3d<float> >& points, size_t start,
                                     size_t * cellSize, size_t * incr, size_t * dir)
{

    //cout << "start: " << start << " cellSize[0]: " << cellSize[0] << " cellSize[1]: " << cellSize[1] << " incr[0]: " << incr[0] << " incr[1]: " << incr[1] << " dir[0]: " << dir[0] << " dir[1]: " << dir[1] << " dir[2]: " << dir[2] << endl;
    VOXEL * pvert[4]; // pointers to values in four vertexes of one cell
    size_t i,j,k,kk; // temporary variable
    size_t state; // the state of one cell
    float pts[4][3]; // float coordinates of the cell verteces
    float * p1, * p2;
    Point3d<float> p;
    
    int * e;
    edge * eCases;
    int *vert;
    float rate;
    size_t fixed=2; // coordinates of the fixed coord;
    //find the fixed coord
    if (ImgIn.GetX(start) == ImgIn.GetX(start+(cellSize[0])*incr[0] + incr[1]))
    {
	fixed = ImgIn.GetX(start);
	//cout <<"X fixed: " << fixed;
    }
    else
	if (ImgIn.GetY(start) == ImgIn.GetY(start+(cellSize[0])*incr[0] + incr[1]))
	{
	    fixed = ImgIn.GetY(start);
	  //  cout <<"Y fixed: " << fixed;
	}
	else
	    if (ImgIn.GetZ(start) == ImgIn.GetZ(start+(cellSize[0])*incr[0] + incr[1] ))
	    {
		fixed = ImgIn.GetZ(start);
	    //    cout <<"Z fixed: " << fixed;
	    }
    //Initialization of the fixed dimension coord of all cell verteces
    for (k=0; k < 4; k++)
    {
	    pts[k][dir[2]] = fixed;
    }
    p[dir[2]] = fixed;
    //Initialization of pointers
    pvert[0] = ImgIn.GetVoxelAddr(start);
    pvert[1] = ImgIn.GetVoxelAddr(start + incr[0]);
    pvert[2] = ImgIn.GetVoxelAddr(start + (cellSize[0]-1)*incr[0] + incr[1]);
    pvert[3] = ImgIn.GetVoxelAddr(start + (cellSize[0])*incr[0] + incr[1]);
//    incr[1] -= incr[0];
  
    
    for (i = 0; i < cellSize[1]; i++) // for all rows of the slice
    {
	for (j = 0; j< cellSize[0]; j++) // for all cells of the row
	{

	    state = 0;
	    /* smazat jen kvuli ladeni 
	     pts[0][dir[0]] = j;
	    pts[0][dir[1]] = i;
	    pts[1][dir[0]] = j+1;
	    pts[1][dir[1]] = i;
	    pts[2][dir[0]] = j;
	    pts[2][dir[1]] = i+1;
	    pts[3][dir[0]] = j+1;
	    pts[3][dir[1]] = i+1;*/
		//cout << "Cell Verteces: 1: "<< pts[0][0]  << "," <<  pts[0][1] << " - " << (int )(* pvert[0])<< "     2: " << pts[1][0] << "," <<  pts[1][1] << " - " << (int )(* pvert[1])<< "     3: " << pts[2][0] << "," <<  pts[2][1] <<" - " << (int )(* pvert[2])<< "     4: " << pts[3][0] << "," <<  pts[3][1] <<" - " << (int )(* pvert[3])<< endl;
		/* konec smazat*/
	    for (k = 0; k < 4 ; k++)
	    {
//		cout << (int)(*pvert[k]) << " >= " << (int) value << endl;
		if ( (*pvert[k]) >= value )
		{
		    state |= mask[k];
		}
	    }
//	    cout << state << endl;
	    if ( (state == 0) || (state == 15) )
	    {
		goto NEXT_CELL;
		continue; //no contour line in the cell
	    }
	    
	    

	    // Initialization of the verteces of the cell 
	    pts[0][dir[0]] = j;
	    pts[0][dir[1]] = i;
	    pts[1][dir[0]] = j+1;
	    pts[1][dir[1]] = i;
	    pts[2][dir[0]] = j;
	    pts[2][dir[1]] = i+1;
	    pts[3][dir[0]] = j+1;
	    pts[3][dir[1]] = i+1;
	    
	    eCases = edgeCases;
	    eCases += state; // Choose Edge Cases (See commentary on top)
	    e = *eCases;
	    //cout << (int) (*pvert[0]) << ", " << (int) (*pvert[1]) << ", " << (int) (*pvert[2]) << ", " << (int) (*pvert[3]) << ", " << endl;
	    for (; e[0] > -1; e+=2)// for all edges in the cell
	    {
		for (k=0; k<2; k++) // for two end points of the edge
		{
		    vert = edgeVert[e[k]];
		    p1 = pts[vert[0]];
		    p2 = pts[vert[1]];
//		    cout << vert[0] << ", " << vert[1] << endl;
		    rate = ( ((float)value) - ((float)(*pvert[vert[0]])) ) / ( ((float)(*pvert[vert[1]])) - ((float)(*pvert[vert[0]]))); //interpolate
		    for (kk = 0; kk < 2; kk++) // for two coords of the end point
		    {
			p[dir[kk]] = p1[dir[kk]] + rate * (p2[dir[kk]] - p1[dir[kk]]);
//			cout << p.coords[dir[kk]] << ", " <<  p.coords[dir[kk]]<< endl;
		    }
		    points.push_back(p);
		}
//		cout << "***\n";
	    }
NEXT_CELL:
	    //Move the pointers to the verteces of the cell to the next cell
//	    cout << "Moving to next cell\n";
	    for (k=0; k<4; k++)
	    {
		pvert[k] += incr[0];
	    }
	}
	
	
	//Move the pointers to the verteces of the cell to the next row
//      cout << "Moving to next row\n";
	for (k=0; k<4; k++)
	{
	    
	    pvert[k] += incr[1] - incr[0];
	}
    }
    
}


template <class VOXEL>
void MarchingSquaresOneSlice(i3d::Image3d<VOXEL>& ImgIn, VOXEL Value, Direction Axis,
                             std::vector<Point3d<float> >& Lines, size_t SliceNumber)
{
    size_t cellSize[2];
    size_t incr[2];
    size_t dir[3];
    size_t startinc;

    switch (Axis)
    {
	case AXIS_X: // yz plane
	    cellSize[0] = ImgIn.GetSizeY()-1;
	    cellSize[1] = ImgIn.GetSizeZ()-1;
	    incr[0] = ImgIn.GetSizeX();
	    incr[1] = (ImgIn.GetSizeX() * ImgIn.GetSizeY()) - (ImgIn.GetSizeX() *  (ImgIn.GetSizeY()-2));
	    dir[0] = 1; 
	    dir[1] = 2;
	    dir[2] = 0;
	    startinc=1;
	    break;
	case AXIS_Y: // xz plane
	    cellSize[0] = ImgIn.GetSizeX()-1;
	    cellSize[1] = ImgIn.GetSizeZ()-1;
	    incr[0] = 1;
	    incr[1] = (ImgIn.GetSizeX() * ImgIn.GetSizeY()) - (ImgIn.GetSizeX() -2);
	    dir[0] = 0; 
	    dir[1] = 2;
	    dir[2] = 1;
	    startinc=ImgIn.GetSizeX();
	    break;
	case AXIS_Z: // xy plane
	    cellSize[0] = ImgIn.GetSizeX()-1;
	    cellSize[1] = ImgIn.GetSizeY()-1;
	    incr[0] = 1;
	    incr[1] = 2;
	    dir[0] = 0; 
	    dir[1] = 1;
	    dir[2] = 2;
	    startinc = ImgIn.GetSizeX() * ImgIn.GetSizeY();
	    break;
	default:
	    startinc = ImgIn.GetSizeX() * ImgIn.GetSizeY();
	    break;
    }

	
    ApplyMarchingSquaresOnOneSlice(ImgIn, Value, Lines, startinc * SliceNumber,cellSize, incr, dir);

}


template <class VOXELIN, class VOXELOUT>
void MarchingSquares (i3d::Image3d<VOXELIN>& ImgIn, i3d::Image3d<VOXELOUT>& ImgOut,
                      VOXELIN Value,Direction Axis, VOXELOUT Colour)
{
    
    std::vector<Point3d<float> > points;
    points.reserve(500);
    i3d::Vector3d<size_t> p1, p2;
    size_t max,i,j;

    switch (Axis)
    {
	case AXIS_X: // yz plane
	    max = ImgIn.GetSizeX();
	    break;
	case AXIS_Y: // xz plane
	    max = ImgIn.GetSizeY();
	    break;
	case AXIS_Z: // xy plane
	    max = ImgIn.GetSizeZ();
	    break;
	default:
	    max = ImgIn.GetSizeZ();
	    break;
    }
    for (i=0; i < max; i++)
    {
	MarchingSquaresOneSlice(ImgIn,Value,Axis,points,i);
	for (j=0; j < points.size(); j += 2)
	{
	    p1.x = (size_t) (round(points[j][0]));
	    p1.y = (size_t) (round(points[j][1]));
	    p1.z = (size_t) (round(points[j][2]));
	    p2.x = (size_t) (round(points[j+1][0]));
	    p2.y = (size_t) (round(points[j+1][1]));
	    p2.z = (size_t) (round(points[j+1][2]));
	    DrawLineBres(ImgOut, p1, p2, Colour);
	}
	points.resize(0);
    }
    
}


template I3D_DLLEXPORT void MarchingSquares (i3d::Image3d<float>& ImgIn, i3d::Image3d<GRAY8>& ImgOut,
                                             float Value, Direction Axis, GRAY8 Colour);
}
