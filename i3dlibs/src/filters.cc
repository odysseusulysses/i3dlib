/*
 * i3dlib - image manipulation library
 *
 * Copyright (C) 2000-2006   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 * FILE: filters.cc
 *
 * Image filtering
 *
 * Petr Mejzl�k (mejzlik@fi.muni.cz) 2000
 * Honza Huben� (xhubeny@fi.muni.cz) 2004
 */

#ifdef __GNUG__
#pragma implementation
#endif

#include "filters.h"
#include <typeinfo>
#include <algorithm>

namespace i3d {
  using namespace std;
  using std::abs;


template <class VOXEL>
void SimpleGauss(Image3d<VOXEL>& img, double sigma, bool exact)
{
	if (img.GetSizeZ() > 1)
		Gauss(img, sigma, sigma, sigma, 1.0/sigma, exact);
	else if (img.GetSizeY() > 1)
		Gauss(img, sigma, sigma, 0, 1.0/sigma, exact);
	else if (img.GetSizeX() > 1)
		Gauss(img, sigma, 0, 0, 1.0/sigma, exact);
}

template <typename NUMBER>
Filter<NUMBER>& Filter<NUMBER>::operator= (const Filter<NUMBER>& src)
        { 
		 	 if (this != &src)  // not a self-assignment
			  {
					size_t filterSize = src.size.x * src.size.y * src.size.z;
  	   	  		delete[] win;
		        	win = new NUMBER[filterSize];
					memcpy(win, src.win, filterSize*sizeof(NUMBER));
		        	size = src.size;
   	       	sum = src.sum;
	  	  		}
			  return *this;
    		}

template <typename NUMBER>
Filter<NUMBER>::Filter(const Filter& src)
        { 
			  win = 0; 
			  *this = src; 
		  }

template <typename NUMBER> template <typename NUMBER2>
Filter<NUMBER>& Filter<NUMBER>::operator= (const Filter<NUMBER2>& src)
		{
        Convert(*this, src);
  		  return *this;
		}

template <typename NUMBER> template <typename NUMBER2>
Filter<NUMBER>::Filter(const Filter<NUMBER2>& src)
        { 
			  win = 0; 
			  *this = src; 
		  }


template <class NUMBER, class NUMBER2>
        void Convert(Filter<NUMBER>& out, const Filter<NUMBER2>& in)
{
        double val;
        NUMBER min = numeric_limits<NUMBER>::min();
        NUMBER max = numeric_limits<NUMBER>::max();
        size_t filterSize = in.Size();
        delete[] out.win;
        out.win = new NUMBER[filterSize];
        out.sum = 0;
        for (size_t i = 0; i<filterSize; i++) {
                val = (NUMBER) (in.win[i] + 0.5); // round
                if (val < min || val > max) {
			string range_err_msg = "Convert: Can't convert a";
			range_err_msg +=  typeid(NUMBER2).name();
			range_err_msg +=  ": out of range.";
			throw InternalException(range_err_msg);
		}
                else out.sum += out.win[i] = (NUMBER) val;
        }
        out.size = in.size;
}

Filter<double> MakeGaussFilter1D(double sigma, double widthFactor)
{
        Filter<double> res;
        int i;

        if (sigma < 0.01 || sigma > 32.0 || widthFactor <= 0.0)
                throw InternalException("MakeGaussFilter1D: invalid input values.");
        int halfsize = (int) ceil(widthFactor*sigma);
        res.win = new double[2*halfsize+1];
        for (i=0; i<= halfsize; i++)
                res.win[halfsize+i] = res.win[halfsize-i] = \
                                        exp(-i*i/(2*sigma*sigma));
//     double min = res.win[0];
//     double mult = 1.0/min;
//     for (i=0; i<= 2*halfsize; i++) res.win[i] *= mult;
        res.size.x = 2*halfsize+1; res.size.y = res.size.z = 1;
        res.sum = res.win[halfsize];
        for (i=1; i<= halfsize; i++)
                res.sum += 2.0 * res.win[halfsize+i];
//	cout << "[ ";
        for (i=0; i< 2 * halfsize+1; i++)
	{
                     res.win[i] /= res.sum;
//		     cout << res.win[i] << ", ";
	}
//	cout << "]\n";
        return res;
}

template <>  // RGB special
	void ApplyGauss1D(RGB* data, const slice& slc, Filter<double>& f, 
				Buffer& b, bool divide)
{
	for (int i=0; i<3; i++) 
	  // Apply the filter to each component separately
	  ApplyGauss1D(((GRAY8*) data) + i, 
		slice(3*slc.start(), slc.size(), 3*slc.stride()), f, b,
			divide);
}

template <>  // RGB16 special
	void ApplyGauss1D(RGB16* data, const slice& slc, Filter<double>& f, 
				Buffer& b, bool divide)
{
	for (int i=0; i<3; i++) 
	  // Apply the filter to each component separately
	  ApplyGauss1D(((GRAY16*) data) + i, 
		slice(3*slc.start(), slc.size(), 3*slc.stride()), f, b,
			divide);
}
template <>  // RGBD special
	void ApplyGauss1D(RGBD* data, const slice& slc, Filter<double>& f, 
				Buffer& b, bool divide)
{
	for (int i=0; i<3; i++) 
	  // Apply the filter to each component separately
	  ApplyGauss1D(((double*) data) + i, 
		slice(3*slc.start(), slc.size(), 3*slc.stride()), f, b,
			divide);
}


template <class VOXEL> void ApplyGauss1D(VOXEL* restrict data, const slice& slc, Filter<double>& f, Buffer& b, bool divide)
{
	size_t i,
	       target,   // index of the currect target voxel
	       bufstart; // current beginning of the roll-out buffer
	
	if (slc.size() == 0) return;
	size_t lastVoxelIndex = slc.start() + (slc.size() - 1) * slc.stride();
	VOXEL lastVoxel = data[lastVoxelIndex]; //VLADO
	VOXEL firstVoxel = data[slc.start()]; //VLADO
	VOXEL max = numeric_limits<VOXEL>::max();
	typename SumOf<VOXEL>::SUMTYPE s;
	
	VOXEL * restrict buf = (VOXEL*) b.start; // voxel buffer
	/* The buffer is periodically filled by the original data
	   and the transformed data is written into the target.
	   At the beginning and end of the array, the first or the last
	   element is copied to get the full width of the filter window. */

	target = slc.start();
	size_t stride = slc.stride(); // just to ease optimization
	size_t winsize = f.Size();

	/* Initialize the buffer: */

	/* Fill the right half of the buffer: */
	for (i=0; i < std::min(f.Size()/2 +1, slc.size()); i++)
	        buf[f.Size()/2 + i] = data[target + i*stride];
	/* Fill the left half of the buffer by copies of the first element: */
	for (i=0; i < f.Size()/2; i++) buf[i] = firstVoxel; //VLADO
	//for (i=0; i < f.Size()/2; i++) buf[i] = 0;
	/* Fill the right end of the buffer in the (rare) case when
	   the filter window is more than twice as big as the data vector: */
	for (i=slc.size(); i < f.Size()/2 +1; i++)
	        buf[f.Size()/2 + i] = lastVoxel; //VLADO
	        //buf[f.Size()/2 + i] = 0;

	bufstart = 0;
	while (target <= lastVoxelIndex) {
		/* Compute the transformed value for data[target]: */
		s = 0;
		for (i=0; i < winsize; i++)
		{
			s += f.win[i] * buf[(bufstart+i) % winsize];
//			cout <<  f.win[i] << " * " << buf[(bufstart+i) % winsize] << " =  " << f.win[i] * buf[(bufstart+i) % winsize] << endl;
		}
//		if (divide) s /= f.sum;
//		if (s != 255.0)
  //                  cout << s << endl; 
		if ((VOXEL) s > max) 
			throw InternalException(\
			"ApplyGauss1D: result of the Gaussian filter can't be represented by this voxel type.");
		else data[target] = (VOXEL) s;
		
		target += stride;

		/* Fetch the next data element into the buffer: */
		i = target + winsize/2 * stride; 
		// i = index of the last data element needed to compute 
		//     data[target]
		if (i > lastVoxelIndex) buf[bufstart] = lastVoxel; //VLADO
		//if (i > lastVoxelIndex) buf[bufstart] = 0;
		else buf[bufstart] = data[i];
		bufstart++;
		bufstart %= f.Size();
	}
}

// GaussDivideAndCopy: internal function used in Gauss()

namespace {
/*template<class VOXEL_IN, class VOXEL_OUT> void 
GaussDivideAndCopy(VOXEL_IN* inter, VOXEL_OUT* data, size_t imgsize, long divide_by);*/
template<class VOXEL_IN, class VOXEL_OUT> /*static*/ void 
GaussDivideAndCopy(VOXEL_IN* inter, VOXEL_OUT* data, size_t imgsize, long divide_by)
{
	VOXEL_OUT max = std::numeric_limits<VOXEL_OUT>::max();
	for (size_t i=0; i<imgsize; i++) {
//		inter[i] /= divide_by;
		if ((VOXEL_OUT)inter[i] > max)
		{
		  throw InternalException(
		  "Gauss: result of the Gaussian filter can't be represented by this voxel type.");
		}
		else data[i] = (VOXEL_OUT) inter[i];
	}
}

template<> void 
GaussDivideAndCopy(RGBD* inter, RGB* data, size_t imgsize, long divide_by)
{
	GaussDivideAndCopy((RGBD::ELEMENT_TYPE *) inter, 
			   (RGB::ELEMENT_TYPE *) data, 3*imgsize, divide_by);
}


template<> void 
GaussDivideAndCopy(RGB16* inter, RGB* data, size_t imgsize, long divide_by)
{
	GaussDivideAndCopy((RGB16::ELEMENT_TYPE *) inter, 
			   (RGB::ELEMENT_TYPE *) data, 3*imgsize, divide_by);
}


} // end of the unnamed namespace (containing a local function)

template <class VOXEL> void Gauss(Image3d<VOXEL>& img, 
	double sigmax, double sigmay, double sigmaz, double widthFactor,
	bool exact)
{
	size_t i, j;
	VOXEL* data = img.GetFirstVoxelAddr();
	size_t noLines, // number of x/y/z-lines
	       lineSize, // size of one x/y/z-lines in voxels
	       stride, // distance between consecutive elements in voxels
	       numSlices, sliceSize;
	Filter<double> f;
	Buffer rbuf, // running buffer
	       vbuf; // temporary buffer for the whole image
	
	// ExtendedVoxel is a voxel type, which should be sufficient
	// for storing scalar products of VOXELs during the Gauss 
	// filter computation:

	typedef typename SumOf<VOXEL>::SUMTYPE ExtendedVoxel; 

	// inter points to the start of the buffer with intermediate results:
	ExtendedVoxel* inter = 0; 

	size_t imgsize = img.GetImageSize();

	// Resolution is needed to find out whether we must compute anisotropis gauusian filter
	Resolution res = img.GetResolution();

	float minres = res.GetX();
	if (minres > res.GetY())
	    minres = res.GetY();
	if (minres > res.GetZ())
	    minres = res.GetZ();
	sigmax = (res.GetX() / minres) * sigmax;
	sigmay = (res.GetY() / minres) * sigmay;
	sigmaz = (res.GetZ() / minres) * sigmaz;
	//cout << " minres: " << minres<< " sigmax: " << sigmax << " sigmay: " << sigmay << " sigmaz: " << sigmaz << endl;

	if (exact) {
		// Allocate temp. buffer for intermediate results 
		// of 1D filtering:
		vbuf.allocate(sizeof(ExtendedVoxel) * imgsize);
		inter = (ExtendedVoxel*)(vbuf.start);

		// Copy voxel data to the temp. buffer:
		for (i=0; i<imgsize; i++) inter[i] = data[i];
	}

	// Allocate the running buffer for in situ Gauss filtering:
	size_t bufferwidth = 1 + 2* (size_t) ceil(widthFactor*sigmax);
	bufferwidth = std::max(bufferwidth, 
			1 + 2* (size_t) ceil(widthFactor*sigmay));
	bufferwidth = std::max(bufferwidth, 
			1 + 2* (size_t) ceil(widthFactor*sigmaz));
	if (exact) 
		rbuf.allocate(sizeof(ExtendedVoxel) * bufferwidth);
	else     
		rbuf.allocate(sizeof(VOXEL) * bufferwidth);

	//long divide_by = 1;

	if (sigmax != 0.0) {
		f = MakeGaussFilter1D(sigmax, widthFactor);
	//	divide_by *= f.sum;
		noLines = img.GetHeight() * img.GetNumSlices();
		lineSize = img.GetWidth();
		stride = 1;
		for (i=0; i < noLines; i++) /* Transform one x-line: */
		  if (exact)
		    ApplyGauss1D(inter, slice(i*lineSize, lineSize, 1), f, rbuf, !exact);
		  else
		    ApplyGauss1D(data, slice(i*lineSize, lineSize, 1), f, rbuf, !exact);
	}
	
	if (sigmay != 0.0) {
		f = MakeGaussFilter1D(sigmay, widthFactor);
	//	divide_by *= f.sum;
		noLines = img.GetWidth();
		lineSize = img.GetHeight();
		stride = img.GetWidth();
		numSlices = img.GetNumSlices();
		sliceSize = img.GetSliceSize();
		for (j=0; j < numSlices; j++)
		  for (i=0; i < noLines; i++) /* Transform one y-line: */
		    if (exact)
		      ApplyGauss1D(inter, slice(j*sliceSize+i, lineSize, stride), f, rbuf, !exact);
		    else
		      ApplyGauss1D(data, slice(j*sliceSize+i, lineSize, stride), f, rbuf, !exact);
	}

	if (sigmaz != 0.0) {
		f = MakeGaussFilter1D(sigmaz, widthFactor);
	//	divide_by *= f.sum;
		noLines = img.GetSliceSize();
		lineSize = img.GetNumSlices();
		stride = img.GetSliceSize();
		for (i=0; i < noLines; i++) /* Transform one z-line: */
		  if (exact)
		    ApplyGauss1D(inter, slice(i, lineSize, stride), f, rbuf, !exact);
		  else
		    ApplyGauss1D(data, slice(i, lineSize, stride), f, rbuf, !exact);
	}

	if (exact) {

		// Divide the result by the product of sums of filters 
		// and copy it back to the image:
	  //  divide_by = 1;
		GaussDivideAndCopy(inter, data, imgsize, 1);
	}

}


// -----------------------------
// Vlado's FIR Gauss starts here
// -----------------------------

template <class VOXEL> void EstimateGaussKernel(struct Separable3dFilter <VOXEL > &filter,
						   const VOXEL sigmaX,
						   const VOXEL sigmaY,
						   const VOXEL sigmaZ,
						   const float widthX,
						   const float widthY,
						   const float widthZ)
{
    filter.DisposeData();
    filter.xData = filter.yData = filter.zData = NULL;

    filter.xLength = 0;
    filter.yLength = 0;
    filter.zLength = 0;

    filter.xDiv = 1;
    filter.yDiv = 1;
    filter.zDiv = 1;

    if ((sigmaX <= 0.0) && (sigmaY <= 0.0) && (sigmaZ <= 0.0))
    {
    	std::cerr << "EstimateGaussKernel(): Warning! Requested creation of an empty filter!\n";

    	return;
    }

    int i;
    VOXEL value;

    if (sigmaX > 0) {
    	const int halfsize = static_cast <int>(ceil(widthX * sigmaX));
    	const int length = halfsize * 2 + 1;

	filter.xData = new VOXEL[length];
    	filter.xLength = length;

    	for (i = 0; i <= halfsize; ++i)
    	{
        	value = exp(static_cast <VOXEL> ((-(i * i)) / (2.0 * sigmaX * sigmaX)));
        	filter.xData[halfsize + i] = value;
        	filter.xData[halfsize - i] = value;
    	}

    	filter.xDiv = filter.xData[halfsize];
    	for (i = 1; i <= halfsize; ++i)
    	{
        	filter.xDiv += filter.xData[i + halfsize] * 2.0;
    	}
    }

    if (sigmaY > 0) {
    	const int halfsize = static_cast <int>(ceil(widthY * sigmaY));
    	const int length = halfsize * 2 + 1;

	filter.yData = new VOXEL[length];
    	filter.yLength = length;

    	for (i = 0; i <= halfsize; ++i)
    	{
        	value = exp(static_cast <VOXEL> ((-(i * i)) / (2.0 * sigmaY * sigmaY)));
        	filter.yData[halfsize + i] = value;
        	filter.yData[halfsize - i] = value;
    	}

    	filter.yDiv = filter.yData[halfsize];
    	for (i = 1; i <= halfsize; ++i)
    	{
        	filter.yDiv += filter.yData[i + halfsize] * 2.0;
    	}
    }

    if (sigmaZ > 0) {
    	const int halfsize = static_cast <int>(ceil(widthZ * sigmaZ));
    	const int length = halfsize * 2 + 1;

	filter.zData = new VOXEL[length];
    	filter.zLength = length;

    	for (i = 0; i <= halfsize; ++i)
    	{
        	value = exp(static_cast <VOXEL> ((-(i * i)) / (2.0 * sigmaZ * sigmaZ)));
        	filter.zData[halfsize + i] = value;
        	filter.zData[halfsize - i] = value;
    	}

    	filter.zDiv = filter.zData[halfsize];
    	for (i = 1; i <= halfsize; ++i)
    	{
        	filter.zDiv += filter.zData[i + halfsize] * 2.0;
    	}
    }
}

template <class VOXEL> void PrepareZeroBoundaryForFilter(struct Separable3dFilter <VOXEL > const &filter, BorderPadding<VOXEL> &b)
{
    b.DisposeData();
    b.xLength = 0;
    b.yLength = 0;
    b.zLength = 0;

    if (filter.xLength > 0) {
        b.xLength = filter.xLength / 2;
	b.xDataPre=new VOXEL[b.xLength];
	b.xDataPost=b.xDataPre;
	for (long i=0; i < b.xLength; ++i) b.xDataPre[i]=0;
    }
    if (filter.yLength > 0) {
        b.yLength = filter.yLength / 2;
	b.yDataPre=new VOXEL[b.yLength];
	b.yDataPost=b.yDataPre;
	for (long i=0; i < b.yLength; ++i) b.yDataPre[i]=0;
    }
    if (filter.zLength > 0) {
        b.zLength = filter.zLength / 2;
	b.zDataPre=new VOXEL[b.zLength];
	b.zDataPost=b.zDataPre;
	for (long i=0; i < b.zLength; ++i) b.zDataPre[i]=0;
    }
}

template <class VOXEL> void GaussFIR(i3d::Image3d <VOXEL> const &In, i3d::Image3d <VOXEL> &Out,
				const VOXEL Sigma, const float Width)
{
	Separable3dFilter<VOXEL> f;
	BorderPadding<VOXEL> b;

	const VOXEL SigmaX=(In.GetSizeX() > 1)? Sigma : 0;
	const VOXEL SigmaY=(In.GetSizeY() > 1)? Sigma : 0;
	const VOXEL SigmaZ=(In.GetSizeZ() > 1)? Sigma : 0;

	EstimateGaussKernel<VOXEL>(f,SigmaX,SigmaY,SigmaZ,Width,Width,Width);
	PrepareZeroBoundaryForFilter<VOXEL>(f,b);

	SeparableConvolution<VOXEL>(In,Out,f,b);
}

template <class VOXEL> void GaussFIR(i3d::Image3d <VOXEL> &img,
				const VOXEL Sigma, const float Width)
{
	Image3d<VOXEL> in(img);
	GaussFIR<VOXEL>(in,img,Sigma,Width);
}

template <class VOXEL> void GaussFIR(i3d::Image3d <VOXEL> const &In, i3d::Image3d <VOXEL> &Out,
				const VOXEL SigmaX, const VOXEL SigmaY, const VOXEL SigmaZ,
				const float WidthX, const float WidthY, const float WidthZ)
{
	Separable3dFilter<VOXEL> f;
	BorderPadding<VOXEL> b;

	EstimateGaussKernel<VOXEL>(f,SigmaX,SigmaY,SigmaZ,WidthX,WidthY,WidthZ);
	PrepareZeroBoundaryForFilter<VOXEL>(f,b);

	SeparableConvolution<VOXEL>(In,Out,f,b);
}

template <class VOXEL> void GaussFIR(i3d::Image3d <VOXEL> &img,
				const VOXEL SigmaX, const VOXEL SigmaY, const VOXEL SigmaZ,
				const float WidthX, const float WidthY, const float WidthZ)
{
	Image3d<VOXEL> in(img);
	GaussFIR<VOXEL>(in,img,SigmaX,SigmaY,SigmaZ,WidthX,WidthY,WidthZ);
}
// ---------------------------
// Vlado's FIR Gauss ends here
// ---------------------------


// -----------------------------
// Vlado's IIR Gauss starts here
// -----------------------------

namespace { //internal functions, start of the unnamed namespace (containing a local function)
/**
 \ingroup gf_helper
 Compute the IIR filter coefficients for Gauss filter with zero mean and given \e Sigma.
 This is a part of IIR version of Gauss filter.

 \note This is related to [Vliet, Young and Ginkel, 2002].

 \param[in] sigma	the desired sigma of the filter
 \param[out] B		the FIR part coefficient
 \param[out] b1,b2,b3	the IIR part coefficients
*/
void ComputeBsFromSigma(const double sigma, double &B, double &b1, double &b2, double &b3) {
	//the code was borrowed from Geusebrock 2002 ( //http://staff.science.uva.nl/~mark/anigauss.c ) in great part
	double q, qsq;
	double scale;

	/* initial values */
	double m0 = 1.16680, m1 = 1.10783, m2 = 1.40586;
	double m1sq = m1*m1, m2sq = m2*m2;

	/* calculate q */
	if(sigma < 3.556)
		q = -0.2568 + 0.5784 * sigma + 0.0561 * sigma * sigma;
	else
		q = 2.5091 + 0.9804 * (sigma - 3.556);

	qsq = q*q;

	/* calculate scale, and b[0,1,2,3] */
	scale = (m0 + q) * (m1sq + m2sq + 2*m1*q + qsq);
	b1 = -q * (2*m0*m1 + m1sq + m2sq + (2*m0 + 4*m1)*q + 3*qsq) / scale;
	b2 = qsq * (m0 + 2*m1 + 3*q) / scale;
	b3 = - qsq * q / scale;
    
	/* calculate B */
	B = (m0 * (m1sq + m2sq))/scale; B*=B;
}

/**
 \ingroup gf_helper
 Estimate the boundary settings for Young and van Vliet's shaped IIR filters according
 to the [Triggs and Sdika, 2006]. It is applicable only to ApplyIIRGauss2() and ApplyIIRGauss3() functions.

 \b Literature: B. Triggs and M. Sdika. Boundary conditions for Young-van Vliet recursive filtering.
 In \b Signal \b processing 54, 5. 2006.

 \param[in] b1,b2,b3	IIR filter parameters, should be the same for both forward and backward directions
 \param[out] M		pointer to boundary conditions adjusting matrix

 \note \e M should be initialized beforehand and should be able to hold at least 9 \c FLOAT values.
*/
template <typename FLOAT>
void SuggestIIRBoundaries(const FLOAT b1,const FLOAT b2,const FLOAT b3, FLOAT *M) {
	FLOAT scale = 1.0/((1.0+b1-b2+b3)*(1.0-b1-b2-b3)*(1.0+b2+(b1-b3)*b3));

	M[0] = scale*(-b3*b1+1.0-b3*b3-b2);
	M[1] = scale*(b3+b1)*(b2+b3*b1);
	M[2] = scale*b3*(b1+b3*b2);
	M[3] = scale*(b1+b3*b2);
	M[4] = -scale*(b2-1.0)*(b2+b3*b1);
	M[5] = -scale*b3*(b3*b1+b3*b3+b2-1.0);
	M[6] = scale*(b3*b1+b2+b1*b1-b2*b2);
	M[7] = scale*(b1*b2+b3*b2*b2-b1*b3*b3-b3*b3*b3-b3*b2+b3);
	M[8] = scale*b3*(b1+b3*b2);
}

} // end of the unnamed namespace (containing a local function)


//the following three macros enable debuging outputs (very very detailed) of
//the core of the GaussIIR() and the SeparableConvolution() functions

//#define DEBUG_CORE_X
//#define DEBUG_CORE_Y
//#define DEBUG_CORE_Z

//#define REMOVE_REMAINDER_FORWARD(val)	      val=(val < 0.0005)? 0 : val;	//removes
#define REMOVE_REMAINDER_FORWARD(val)   					//leaves
//#define REMOVE_REMAINDER_BACKWARD(val)      val=(val < 0.0005)? 0 : val;	//removes
#define REMOVE_REMAINDER_BACKWARD(val)  					//leaves

// see header file for docs; 2002 version
template <class VOXEL>
  void GaussIIR(Image3d <VOXEL> &img, const VOXEL Sigma) {

    //input/output image dimensions
    const signed long x_size = (signed long) img.GetWidth();
    const signed long y_size = (signed long) img.GetHeight();
    const signed long z_size = (signed long) img.GetNumSlices();
    const signed long s_size = x_size * y_size;     //just a helper variables (timesavers)
    const signed long x_2size = 2*x_size;
    const signed long x_3size = 3*x_size;
    const signed long s_2size = 2*s_size;
    const signed long s_3size = 3*s_size;

    if ((x_size > 1) && (x_size < 4)) {
    	std::cerr << "GaussIIR(): Input image too small along x-axis, min is 4px.\n";
	return;
    }
    if ((y_size > 1) && (y_size < 4)) {
    	std::cerr << "GaussIIR(): Input image too small along y-axis, min is 4px.\n";
	return;
    }
    if ((z_size > 1) && (z_size < 4)) {
    	std::cerr << "GaussIIR(): Input image too small along z-axis, min is 4px.\n";
	return;
    }

    //setting the filter shape
    double b1D,b2D,b3D,BD;
    double S=static_cast<double>(Sigma);
    ComputeBsFromSigma(S,BD,b1D,b2D,b3D);

    VOXEL b1=static_cast<VOXEL>(b1D),
    	  b2=static_cast<VOXEL>(b2D),
  	  b3=static_cast<VOXEL>(b3D),
	  B=static_cast<VOXEL>(BD);

    VOXEL *M=new VOXEL[9];		//boundary handling matrix
    if (M == NULL) {
    	std::cerr << "GaussIIR(): No memory left for boundary matrix.\n";
	return;
    }
    SuggestIIRBoundaries(-b1,-b2,-b3,M);
    const VOXEL denom=1.0 +b1+b2+b3;	//boundary handling...

#ifdef I3D_DEBUG
    std::cout << "GaussIIR: input image of size " << x_size << "x" << y_size << "x" << z_size << " voxels\n";
    std::cout << "GaussIIR: B=" << B << ", b1=" << b1 << ", b2=" << b2 << ", b3=" << b3 << std::endl;
#endif
    //temporary variables
    signed long i = 0, c = 0;

    VOXEL *wrk = img.GetFirstVoxelAddr();

    // ---- X AXIS ----
    if (img.GetSizeX() > 1)
    {
        //x-axis filter is present...
#ifdef I3D_DEBUG
        std::cout << "GaussIIR: convolving in x-axis...\n";
#endif
        for (signed long rows = 0; rows < y_size * z_size; rows++)
        {
            //no need to shift wrk pointer because x-lines are sequentially in memory

	    //boundary handling...
	    const VOXEL bnd=*wrk/denom;

            // PREFIX, we do it "manually" (aka loop-unrolling)
#ifdef DEBUG_CORE_X
	    std::cout << "GaussIIR:   forward: prefix starts, ";
#endif
	    *wrk=*wrk - bnd*(b1+b2+b3); //i=0
	    REMOVE_REMAINDER_FORWARD(*wrk)
	    ++wrk;

	    *wrk=*wrk  - *(wrk-1)*b1 - bnd*(b2+b3); //i=1
	    REMOVE_REMAINDER_FORWARD(*wrk)
	    ++wrk;

	    *wrk=*wrk  - *(wrk-1)*b1 - *(wrk-2)*b2 - bnd*b3; //i=2
	    REMOVE_REMAINDER_FORWARD(*wrk)
	    ++wrk;

#ifdef DEBUG_CORE_X
	    std::cout << "convolutions of i=0,1,2 (row=" << rows << ") is finished\n";
#endif
            // INFIX
            for (i = 3; i < (x_size-1); i++)
            {
#ifdef DEBUG_CORE_X
                std::cout << "GaussIIR:   forward: infix starts, ";
#endif
		*wrk=*wrk  - *(wrk-1)*b1 - *(wrk-2)*b2 - *(wrk-3)*b3;
		REMOVE_REMAINDER_FORWARD(*wrk)
#ifdef DEBUG_CORE_X
                std::cout << "convolution of " << i << " (row=" << rows << ") is finished\n";
#endif
                ++wrk;
            }

	    ++wrk;
        }

	wrk=img.GetVoxelAddr(img.GetImageSize()-1); //useless

        for (signed long rows = 0; rows < y_size * z_size; rows++)
        {
            //no need to shift wrk pointer because x-lines are sequentially in memory

	    VOXEL up=*wrk/denom; //a copy, btw: this equals to u_plus in (15) in [Triggs and Sdika, 2006]
	    
	    *wrk=*wrk  - *(wrk-1)*b1 - *(wrk-2)*b2 - *(wrk-3)*b3; //finishing the forward convolution
	    REMOVE_REMAINDER_FORWARD(*wrk)
	    //the existence of (wrk-3) is guaranteed by the minimum image size test at the begining of this routine

	    //boundary handling...
	    const VOXEL bnd1_=*wrk-up, //new *wrk values after the entire forward convolution are used now
			bnd2_=*(wrk-1)-up,
			bnd3_=*(wrk-2)-up;
	    up/=denom; //this equals to v_plus in (15) in [Triggs and Sdika, 2006]

			*wrk=(M[0]*bnd1_ + M[1]*bnd2_ + M[2]*bnd3_ +up) *B;
	    const VOXEL bnd2=(M[3]*bnd1_ + M[4]*bnd2_ + M[5]*bnd3_ +up) *B;
	    const VOXEL bnd3=(M[6]*bnd1_ + M[7]*bnd2_ + M[8]*bnd3_ +up) *B;

            // PREFIX, we do it "manually" (aka loop-unrolling)
#ifdef DEBUG_CORE_X
	    std::cout << "GaussIIR:   backward: prefix starts, ";
#endif
	    //*wrk*=B; //i=0
	    REMOVE_REMAINDER_BACKWARD(*wrk)
	    --wrk;

	    //*wrk=(*wrk)*B  - *(wrk+1)*b1 - bnd2*B*b2 - bnd3*B*b3; //i=1
	    *wrk=(*wrk)*B  - *(wrk+1)*b1 - bnd2*b2 - bnd3*b3; //i=1
	    REMOVE_REMAINDER_BACKWARD(*wrk)
	    --wrk;

	    //*wrk=(*wrk)*B  - *(wrk+1)*b1 - *(wrk+2)*b2 - bnd2*B*b3; //i=2
	    *wrk=(*wrk)*B  - *(wrk+1)*b1 - *(wrk+2)*b2 - bnd2*b3; //i=2
	    REMOVE_REMAINDER_BACKWARD(*wrk)
	    --wrk;

#ifdef DEBUG_CORE_X
	    std::cout << "convolutions of i=" << x_size-1 << "," << x_size-2 << "," << x_size-3 \
	    	      << " (row=" << rows << ") is finished\n";
#endif
            // INFIX
            for (i = 3; i < x_size; i++) //growing of i is unimportant in this case
            {
#ifdef DEBUG_CORE_X
                std::cout << "GaussIIR:   backward: infix starts, ";
#endif
		*wrk=(*wrk)*B  - *(wrk+1)*b1 - *(wrk+2)*b2 - *(wrk+3)*b3;
		REMOVE_REMAINDER_BACKWARD(*wrk)
#ifdef DEBUG_CORE_X
                std::cout << "convolution of " << x_size-1-i << " (row=" << rows << ") is finished\n";
#endif
                --wrk;
            }
        }

	wrk=img.GetFirstVoxelAddr(); //important
    }

    // ---- Y AXIS ----
    if (img.GetSizeY() > 1)
    {
        //y-axis filter is present...
        VOXEL *wrk2 = wrk;

#ifdef I3D_DEBUG
        std::cout << "GaussIIR: convolving in y-axis...\n";
#endif
	//storage of original input values before any y-axis related convolution
	VOXEL *bnd=new VOXEL[x_size];

        for (signed long slices = 0; slices < z_size; slices++)
        {
            wrk = wrk2 + (slices * s_size);

            // PREFIX, we do it "manually" (aka loop-unrolling)
#ifdef DEBUG_CORE_Y
	    std::cout << "GaussIIR:   forward: prefix starts, ";
#endif
	    for (c = 0; c < x_size; c++) {
		*(bnd+c)=*(wrk+c)/denom; //boundary handling...

	    	*(wrk+c)=*(wrk+c) - *(bnd+c)*(b1+b2+b3); //i=0
		REMOVE_REMAINDER_FORWARD(*(wrk+c))
	    }
	    wrk += x_size;

	    for (c = 0; c < x_size; c++) {
	    	*(wrk+c)=*(wrk+c)  - *(wrk+c-x_size)*b1 - *(bnd+c)*(b2+b3); //i=1
		REMOVE_REMAINDER_FORWARD(*(wrk+c))
	    }
	    wrk += x_size;

	    for (c = 0; c < x_size; c++) {
	    	*(wrk+c)=*(wrk+c)  - *(wrk+c-x_size)*b1 - *(wrk+c-x_2size)*b2 - *(bnd+c)*b3; //i=2
		REMOVE_REMAINDER_FORWARD(*(wrk+c))
	    }
	    wrk += x_size;

#ifdef DEBUG_CORE_Y
	    std::cout << "convolutions of i=0,1,2 (slice=" << slices << ") is finished\n";
#endif
            // INFIX
            for (i = 3; i < (y_size-1); i++)
            {
#ifdef DEBUG_CORE_Y
                std::cout << "GaussIIR:   forward: infix starts, ";
#endif
		for (c = 0; c < x_size; c++) {
		    *(wrk+c)=*(wrk+c)  - *(wrk+c-x_size)*b1 - *(wrk+c-x_2size)*b2 - *(wrk+c-x_3size)*b3;
		    REMOVE_REMAINDER_FORWARD(*(wrk+c))
		}
#ifdef DEBUG_CORE_Y
                std::cout << "convolution of " << i << " (slice=" << slices << ") is finished\n";
#endif
                wrk += x_size;
            }

            wrk += x_size;
        }

	wrk2=img.GetVoxelAddr(img.GetImageSize()) -x_size; //important

	VOXEL *bnd2=new VOXEL[x_size];
	VOXEL *bnd3=new VOXEL[x_size];
	VOXEL *vp=new VOXEL[x_size]; //to store v_plus in (15) in [Triggs and Sdika, 2006]

	VOXEL *Bnd2=new VOXEL[x_size];
	VOXEL *Bnd3=new VOXEL[x_size];

        for (signed long slices = 0; slices < z_size; slices++)
        {
            wrk = wrk2 - (slices * s_size);

	    //copy the voxel's value at the end of y-line and finish the forward convolution
	    for (c = 0; c < x_size; c++) {
		*(bnd+c)=*(wrk+c)/denom; //will be used *bnd instead of *up, just for this occassion

		//finishes the forward convolution
		*(wrk+c)=*(wrk+c) - *(wrk+c-x_size)*b1 - *(wrk+c-x_2size)*b2 - *(wrk+c-x_3size)*b3;
		REMOVE_REMAINDER_FORWARD(*(wrk+c))
	    }

	    for (c = 0; c < x_size; c++) *(vp+c)=*(bnd+c)/denom;
	    for (c = 0; c < x_size; c++) *(bnd3+c)=*(wrk+c-x_2size)-*(bnd+c);
	    for (c = 0; c < x_size; c++) *(bnd2+c)=*(wrk+c-x_size )-*(bnd+c);
	    for (c = 0; c < x_size; c++) *(bnd +c)=*(wrk+c        )-*(bnd+c);

	    //for (c = 0; c < x_size; c++) *(wrk +c)=( *(bnd+c)*M[0] + *(bnd2+c)*M[1] + *(bnd3+c)*M[2] + *(vp+c) ) *B;
	    for (c = 0; c < x_size; c++) *(Bnd2+c)=( *(bnd+c)*M[3] + *(bnd2+c)*M[4] + *(bnd3+c)*M[5] + *(vp+c) ) *B;
	    for (c = 0; c < x_size; c++) *(Bnd3+c)=( *(bnd+c)*M[6] + *(bnd2+c)*M[7] + *(bnd3+c)*M[8] + *(vp+c) ) *B;

            // PREFIX, we do it "manually" (aka loop-unrolling)
#ifdef DEBUG_CORE_Y
	    std::cout << "GaussIIR:   backward: prefix starts, ";
#endif
	    for (c = 0; c < x_size; c++) {
		*(wrk+c)=( *(bnd+c)*M[0] + *(bnd2+c)*M[1] + *(bnd3+c)*M[2] + *(vp+c) ) *B;
	        REMOVE_REMAINDER_BACKWARD(*(wrk+c))
	    }
	    wrk -= x_size;

	    for (c = 0; c < x_size; c++) {
	        *(wrk+c)=*(wrk+c)*B  - *(wrk+c+x_size)*b1 - *(Bnd2+c)*b2 - *(Bnd3+c)*b3; //i=1
	        REMOVE_REMAINDER_BACKWARD(*(wrk+c))
	    }
	    wrk -= x_size;

	    for (c = 0; c < x_size; c++) {
	        *(wrk+c)=*(wrk+c)*B  - *(wrk+c+x_size)*b1 - *(wrk+c+x_2size)*b2 - *(Bnd2+c)*b3; //i=2
	        REMOVE_REMAINDER_BACKWARD(*(wrk+c))
	    }
	    wrk -= x_size;

#ifdef DEBUG_CORE_Y
	    std::cout << "convolutions of i=" << y_size-1 << "," << y_size-2 << "," << y_size-3 \
	    	      << " (slice=" << slices << ") is finished\n";
#endif
            // INFIX
            for (i = 3; i < y_size; i++)
            {
#ifdef DEBUG_CORE_Y
                std::cout << "GaussIIR:   backward: infix starts, ";
#endif
		for (c = 0; c < x_size; c++) {
		    *(wrk+c)=*(wrk+c) * B  - *(wrk+c+x_size)*b1 - *(wrk+c+x_2size)*b2 - *(wrk+c+x_3size)*b3;
	            REMOVE_REMAINDER_BACKWARD(*(wrk+c))
		}
#ifdef DEBUG_CORE_Y
                std::cout << "convolution of " << y_size-1-i << " (slice=" << slices << ") is finished\n";
#endif
                wrk -= x_size;
            }
        }

	//release allocated memory
	delete[] bnd;
	delete[] bnd2;
	delete[] bnd3;
	delete[] vp;
	delete[] Bnd2;
	delete[] Bnd3;

	wrk=img.GetFirstVoxelAddr(); //important
    }

    // ---- Z AXIS ----
    if (img.GetSizeZ() > 1)
    {
        //z-axis filter is present...
        VOXEL *wrk2 = wrk;

#ifdef I3D_DEBUG
        std::cout << "GaussIIR: convolving in z-axis...\n";
#endif
	//storage of original input values before any y-axis related convolution
	VOXEL *bnd=new VOXEL[x_size];

        for (signed long cols = 0; cols < y_size; cols++)
        {
            wrk = wrk2 + (cols * x_size);

            // PREFIX, we do it "manually" (aka loop-unrolling)
#ifdef DEBUG_CORE_Z
	    std::cout << "GaussIIR:   forward: prefix starts, ";
#endif
	    for (c = 0; c < x_size; c++) {
		*(bnd+c)=*(wrk+c)/denom; //boundary handling...

	    	*(wrk+c)=*(wrk+c) - *(bnd+c)*(b1+b2+b3); //i=0
		REMOVE_REMAINDER_FORWARD(*(wrk+c))
	    }
	    wrk += s_size;

	    for (c = 0; c < x_size; c++) {
	    	*(wrk+c)=*(wrk+c)  - *(wrk+c-s_size)*b1 - *(bnd+c)*(b2+b3); //i=1
		REMOVE_REMAINDER_FORWARD(*(wrk+c))
	    }
	    wrk += s_size;

	    for (c = 0; c < x_size; c++) {
	    	*(wrk+c)=*(wrk+c)  - *(wrk+c-s_size)*b1 - *(wrk+c-s_2size)*b2 - *(bnd+c)*b3; //i=2
		REMOVE_REMAINDER_FORWARD(*(wrk+c))
	    }
	    wrk += s_size;

#ifdef DEBUG_CORE_Z
	    std::cout << "convolutions of i=0,1,2 (col=" << cols << ") is finished\n";
#endif
            // INFIX
            for (i = 3; i < (z_size-1); i++)
            {
#ifdef DEBUG_CORE_Z
                std::cout << "GaussIIR:   forward: infix starts, ";
#endif
		for (c = 0; c < x_size; c++) {
		    *(wrk+c)=*(wrk+c)  - *(wrk+c-s_size)*b1 - *(wrk+c-s_2size)*b2 - *(wrk+c-s_3size)*b3;
		    REMOVE_REMAINDER_FORWARD(*(wrk+c))
		}
#ifdef DEBUG_CORE_Z
                std::cout << "convolution of " << i << " (col=" << cols << ") is finished\n";
#endif
                wrk += s_size;
            }

            wrk += s_size;
        }

	wrk2=img.GetVoxelAddr(img.GetImageSize()) -s_size;

	VOXEL *bnd2=new VOXEL[x_size];
	VOXEL *bnd3=new VOXEL[x_size];
	VOXEL *vp=new VOXEL[x_size]; //to store v_plus in (15) in [Triggs and Sdika, 2006]

	VOXEL *Bnd2=new VOXEL[x_size];
	VOXEL *Bnd3=new VOXEL[x_size];

        for (signed long cols = 0; cols < y_size; cols++)
        {
            wrk = wrk2 + (cols * x_size);

	    //copy the voxel's value at the end of z-line and finish the forward convolution
	    for (c = 0; c < x_size; c++) {
		*(bnd+c)=*(wrk+c)/denom; //will be used *bnd instead of *up, just for this occassion

		//finishes the forward convolution
		*(wrk+c)=*(wrk+c) - *(wrk+c-s_size)*b1 - *(wrk+c-s_2size)*b2 - *(wrk+c-s_3size)*b3;
		REMOVE_REMAINDER_FORWARD(*(wrk+c))
	    }

	    for (c = 0; c < x_size; c++) *(vp+c)=*(bnd+c)/denom;
	    for (c = 0; c < x_size; c++) *(bnd3+c)=*(wrk+c-s_2size)-*(bnd+c);
	    for (c = 0; c < x_size; c++) *(bnd2+c)=*(wrk+c-s_size )-*(bnd+c);
	    for (c = 0; c < x_size; c++) *(bnd +c)=*(wrk+c        )-*(bnd+c);

	    //for (c = 0; c < x_size; c++) *(wrk +c)=( *(bnd+c)*M[0] + *(bnd2+c)*M[1] + *(bnd3+c)*M[2] + *(vp+c) ) *B;
	    for (c = 0; c < x_size; c++) *(Bnd2+c)=( *(bnd+c)*M[3] + *(bnd2+c)*M[4] + *(bnd3+c)*M[5] + *(vp+c) ) *B;
	    for (c = 0; c < x_size; c++) *(Bnd3+c)=( *(bnd+c)*M[6] + *(bnd2+c)*M[7] + *(bnd3+c)*M[8] + *(vp+c) ) *B;

            // PREFIX, we do it "manually" (aka loop-unrolling)
#ifdef DEBUG_CORE_Z
	    std::cout << "GaussIIR:   backward: prefix starts, ";
#endif
	    for (c = 0; c < x_size; c++) {
		*(wrk+c)=( *(bnd+c)*M[0] + *(bnd2+c)*M[1] + *(bnd3+c)*M[2] + *(vp+c) ) *B;
	        REMOVE_REMAINDER_BACKWARD(*(wrk+c))
	    }
	    wrk -= s_size;

	    for (c = 0; c < x_size; c++) {
	        *(wrk+c)=*(wrk+c)*B  - *(wrk+c+s_size)*b1 - *(Bnd2+c)*b2 - *(Bnd3+c)*b3; //i=1
	        REMOVE_REMAINDER_BACKWARD(*(wrk+c))
	    }
	    wrk -= s_size;

	    for (c = 0; c < x_size; c++) {
	        *(wrk+c)=*(wrk+c)*B  - *(wrk+c+s_size)*b1 - *(wrk+c+s_2size)*b2 - *(Bnd2+c)*b3; //i=2
	        REMOVE_REMAINDER_BACKWARD(*(wrk+c))
	    }
	    wrk -= s_size;

#ifdef DEBUG_CORE_Z
	    std::cout << "convolutions of i=" << z_size-1 << "," << z_size-2 << "," << z_size-3 \
	    	      << " (col=" << cols << ") is finished\n";
#endif
            // INFIX
            for (i = 3; i < z_size; i++)
            {
#ifdef DEBUG_CORE_Z
                std::cout << "GaussIIR:   backward: infix starts, ";
#endif
		for (c = 0; c < x_size; c++) {
		    *(wrk+c)=*(wrk+c) * B  - *(wrk+c+s_size)*b1 - *(wrk+c+s_2size)*b2 - *(wrk+c+s_3size)*b3;
	            REMOVE_REMAINDER_BACKWARD(*(wrk+c))
		}
#ifdef DEBUG_CORE_Z
                std::cout << "convolution of " << z_size-1-i << " (col=" << cols << ") is finished\n";
#endif
                wrk -= s_size;
            }
        }

	//release allocated memory
	delete[] bnd;
	delete[] bnd2;
	delete[] bnd3;
	delete[] vp;
	delete[] Bnd2;
	delete[] Bnd3;
    }

    delete[] M;				//release boundary handling matrix
}

// see header file for docs; 2002 version
template <class VOXEL>
  void GaussIIR(Image3d <VOXEL> &img, const VOXEL SigmaX, const VOXEL SigmaY, const VOXEL SigmaZ) {

    //input/output image dimensions
    const signed long x_size = (signed long) img.GetWidth();
    const signed long y_size = (signed long) img.GetHeight();
    const signed long z_size = (signed long) img.GetNumSlices();
    const signed long s_size = x_size * y_size;     //just a helper variables (timesavers)
    const signed long x_2size = 2*x_size;
    const signed long x_3size = 3*x_size;
    const signed long s_2size = 2*s_size;
    const signed long s_3size = 3*s_size;

    if ((x_size > 1) && (x_size < 4)) {
    	std::cerr << "GaussIIR(): Input image too small along x-axis, min is 4px.\n";
	return;
    }
    if ((y_size > 1) && (y_size < 4)) {
    	std::cerr << "GaussIIR(): Input image too small along y-axis, min is 4px.\n";
	return;
    }
    if ((z_size > 1) && (z_size < 4)) {
    	std::cerr << "GaussIIR(): Input image too small along z-axis, min is 4px.\n";
	return;
    }

    //setting the filter shape
    double b1D,b2D,b3D,BD;
    VOXEL B,b1,b2,b3;

    VOXEL *M=new VOXEL[9];		//boundary handling matrix
    if (M == NULL) {
    	std::cerr << "GaussIIR(): No memory left for boundary matrix.\n";
	return;
    }

#ifdef I3D_DEBUG
    std::cout << "GaussIIR: input image of size " << x_size << "x" << y_size << "x" << z_size << " voxels\n";
#endif
    //temporary variables
    signed long i = 0, c = 0;

    VOXEL *wrk = img.GetFirstVoxelAddr();

    // ---- X AXIS ----
    if ((img.GetSizeX() > 1) && (SigmaX >= 1.0))
    {
	double S=static_cast<double>(SigmaX);
	ComputeBsFromSigma(S,BD,b1D,b2D,b3D);

	b1=static_cast<VOXEL>(b1D);
	b2=static_cast<VOXEL>(b2D);
	b3=static_cast<VOXEL>(b3D);
	B=static_cast<VOXEL>(BD);

	SuggestIIRBoundaries(-b1,-b2,-b3,M);
	const VOXEL denom=1.0 +b1+b2+b3;	//boundary handling...

        //x-axis filter is present...
#ifdef I3D_DEBUG
        std::cout << "GaussIIR: convolving in x-axis...\n";
#endif
#ifdef I3D_DEBUG
	std::cout << "GaussIIR: B=" << B << ", b1=" << b1 << ", b2=" << b2 << ", b3=" << b3 << std::endl;
#endif
        for (signed long rows = 0; rows < y_size * z_size; rows++)
        {
            //no need to shift wrk pointer because x-lines are sequentially in memory

	    //boundary handling...
	    const VOXEL bnd=*wrk/denom;

            // PREFIX, we do it "manually" (aka loop-unrolling)
#ifdef DEBUG_CORE_X
	    std::cout << "GaussIIR:   forward: prefix starts, ";
#endif
	    *wrk=*wrk - bnd*(b1+b2+b3); //i=0
	    REMOVE_REMAINDER_FORWARD(*wrk)
	    ++wrk;

	    *wrk=*wrk  - *(wrk-1)*b1 - bnd*(b2+b3); //i=1
	    REMOVE_REMAINDER_FORWARD(*wrk)
	    ++wrk;

	    *wrk=*wrk  - *(wrk-1)*b1 - *(wrk-2)*b2 - bnd*b3; //i=2
	    REMOVE_REMAINDER_FORWARD(*wrk)
	    ++wrk;

#ifdef DEBUG_CORE_X
	    std::cout << "convolutions of i=0,1,2 (row=" << rows << ") is finished\n";
#endif
            // INFIX
            for (i = 3; i < (x_size-1); i++)
            {
#ifdef DEBUG_CORE_X
                std::cout << "GaussIIR:   forward: infix starts, ";
#endif
		*wrk=*wrk  - *(wrk-1)*b1 - *(wrk-2)*b2 - *(wrk-3)*b3;
		REMOVE_REMAINDER_FORWARD(*wrk)
#ifdef DEBUG_CORE_X
                std::cout << "convolution of " << i << " (row=" << rows << ") is finished\n";
#endif
                ++wrk;
            }

	    ++wrk;
        }

	wrk=img.GetVoxelAddr(img.GetImageSize()-1); //useless

        for (signed long rows = 0; rows < y_size * z_size; rows++)
        {
            //no need to shift wrk pointer because x-lines are sequentially in memory

	    VOXEL up=*wrk/denom; //a copy, btw: this equals to u_plus in (15) in [Triggs and Sdika, 2006]
	    
	    *wrk=*wrk  - *(wrk-1)*b1 - *(wrk-2)*b2 - *(wrk-3)*b3; //finishing the forward convolution
	    REMOVE_REMAINDER_FORWARD(*wrk)
	    //the existence of (wrk-3) is guaranteed by the minimum image size test at the begining of this routine

	    //boundary handling...
	    const VOXEL bnd1_=*wrk-up, //new *wrk values after the entire forward convolution are used now
			bnd2_=*(wrk-1)-up,
			bnd3_=*(wrk-2)-up;
	    up/=denom; //this equals to v_plus in (15) in [Triggs and Sdika, 2006]

			*wrk=(M[0]*bnd1_ + M[1]*bnd2_ + M[2]*bnd3_ +up) *B;
	    const VOXEL bnd2=(M[3]*bnd1_ + M[4]*bnd2_ + M[5]*bnd3_ +up) *B;
	    const VOXEL bnd3=(M[6]*bnd1_ + M[7]*bnd2_ + M[8]*bnd3_ +up) *B;

            // PREFIX, we do it "manually" (aka loop-unrolling)
#ifdef DEBUG_CORE_X
	    std::cout << "GaussIIR:   backward: prefix starts, ";
#endif
	    //done already few lines above, i=0
	    REMOVE_REMAINDER_BACKWARD(*wrk)
	    --wrk;

	    *wrk=(*wrk)*B  - *(wrk+1)*b1 - bnd2*b2 - bnd3*b3; //i=1
	    REMOVE_REMAINDER_BACKWARD(*wrk)
	    --wrk;

	    *wrk=(*wrk)*B  - *(wrk+1)*b1 - *(wrk+2)*b2 - bnd2*b3; //i=2
	    REMOVE_REMAINDER_BACKWARD(*wrk)
	    --wrk;

#ifdef DEBUG_CORE_X
	    std::cout << "convolutions of i=" << x_size-1 << "," << x_size-2 << "," << x_size-3 \
	    	      << " (row=" << rows << ") is finished\n";
#endif
            // INFIX
            for (i = 3; i < x_size; i++) //growing of i is unimportant in this case
            {
#ifdef DEBUG_CORE_X
                std::cout << "GaussIIR:   backward: infix starts, ";
#endif
		*wrk=(*wrk)*B  - *(wrk+1)*b1 - *(wrk+2)*b2 - *(wrk+3)*b3;
		REMOVE_REMAINDER_BACKWARD(*wrk)
#ifdef DEBUG_CORE_X
                std::cout << "convolution of " << x_size-1-i << " (row=" << rows << ") is finished\n";
#endif
                --wrk;
            }
        }

	wrk=img.GetFirstVoxelAddr(); //important
    }

    // ---- Y AXIS ----
    if ((img.GetSizeY() > 1) && (SigmaY >= 1.0))
    {
	double S=static_cast<double>(SigmaY);
	ComputeBsFromSigma(S,BD,b1D,b2D,b3D);

	b1=static_cast<VOXEL>(b1D);
	b2=static_cast<VOXEL>(b2D);
	b3=static_cast<VOXEL>(b3D);
	B=static_cast<VOXEL>(BD);

	SuggestIIRBoundaries(-b1,-b2,-b3,M);
	const VOXEL denom=1.0 +b1+b2+b3;	//boundary handling...

        //y-axis filter is present...
        VOXEL *wrk2 = wrk;

#ifdef I3D_DEBUG
        std::cout << "GaussIIR: convolving in y-axis...\n";
#endif
#ifdef I3D_DEBUG
	std::cout << "GaussIIR: B=" << B << ", b1=" << b1 << ", b2=" << b2 << ", b3=" << b3 << std::endl;
#endif
	//storage of original input values before any y-axis related convolution
	VOXEL *bnd=new VOXEL[x_size];

        for (signed long slices = 0; slices < z_size; slices++)
        {
            wrk = wrk2 + (slices * s_size);

            // PREFIX, we do it "manually" (aka loop-unrolling)
#ifdef DEBUG_CORE_Y
	    std::cout << "GaussIIR:   forward: prefix starts, ";
#endif
	    for (c = 0; c < x_size; c++) {
		*(bnd+c)=*(wrk+c)/denom; //boundary handling...

	    	*(wrk+c)=*(wrk+c) - *(bnd+c)*(b1+b2+b3); //i=0
		REMOVE_REMAINDER_FORWARD(*(wrk+c))
	    }
	    wrk += x_size;

	    for (c = 0; c < x_size; c++) {
	    	*(wrk+c)=*(wrk+c)  - *(wrk+c-x_size)*b1 - *(bnd+c)*(b2+b3); //i=1
		REMOVE_REMAINDER_FORWARD(*(wrk+c))
	    }
	    wrk += x_size;

	    for (c = 0; c < x_size; c++) {
	    	*(wrk+c)=*(wrk+c)  - *(wrk+c-x_size)*b1 - *(wrk+c-x_2size)*b2 - *(bnd+c)*b3; //i=2
		REMOVE_REMAINDER_FORWARD(*(wrk+c))
	    }
	    wrk += x_size;

#ifdef DEBUG_CORE_Y
	    std::cout << "convolutions of i=0,1,2 (slice=" << slices << ") is finished\n";
#endif
            // INFIX
            for (i = 3; i < (y_size-1); i++)
            {
#ifdef DEBUG_CORE_Y
                std::cout << "GaussIIR:   forward: infix starts, ";
#endif
		for (c = 0; c < x_size; c++) {
		    *(wrk+c)=*(wrk+c)  - *(wrk+c-x_size)*b1 - *(wrk+c-x_2size)*b2 - *(wrk+c-x_3size)*b3;
		    REMOVE_REMAINDER_FORWARD(*(wrk+c))
		}
#ifdef DEBUG_CORE_Y
                std::cout << "convolution of " << i << " (slice=" << slices << ") is finished\n";
#endif
                wrk += x_size;
            }

            wrk += x_size;
        }

	wrk2=img.GetVoxelAddr(img.GetImageSize()) -x_size; //important

	VOXEL *bnd2=new VOXEL[x_size];
	VOXEL *bnd3=new VOXEL[x_size];
	VOXEL *vp=new VOXEL[x_size]; //to store v_plus in (15) in [Triggs and Sdika, 2006]

	VOXEL *Bnd2=new VOXEL[x_size];
	VOXEL *Bnd3=new VOXEL[x_size];

        for (signed long slices = 0; slices < z_size; slices++)
        {
            wrk = wrk2 - (slices * s_size);

	    //copy the voxel's value at the end of y-line and finish the forward convolution
	    for (c = 0; c < x_size; c++) {
		*(bnd+c)=*(wrk+c)/denom; //will be used *bnd instead of *up, just for this occassion

		//finishes the forward convolution
		*(wrk+c)=*(wrk+c) - *(wrk+c-x_size)*b1 - *(wrk+c-x_2size)*b2 - *(wrk+c-x_3size)*b3;
		REMOVE_REMAINDER_FORWARD(*(wrk+c))
	    }

	    for (c = 0; c < x_size; c++) *(vp+c)=*(bnd+c)/denom;
	    for (c = 0; c < x_size; c++) *(bnd3+c)=*(wrk+c-x_2size)-*(bnd+c);
	    for (c = 0; c < x_size; c++) *(bnd2+c)=*(wrk+c-x_size )-*(bnd+c);
	    for (c = 0; c < x_size; c++) *(bnd +c)=*(wrk+c        )-*(bnd+c);

	    //for (c = 0; c < x_size; c++) *(wrk +c)=( *(bnd+c)*M[0] + *(bnd2+c)*M[1] + *(bnd3+c)*M[2] + *(vp+c) ) *B;
	    for (c = 0; c < x_size; c++) *(Bnd2+c)=( *(bnd+c)*M[3] + *(bnd2+c)*M[4] + *(bnd3+c)*M[5] + *(vp+c) ) *B;
	    for (c = 0; c < x_size; c++) *(Bnd3+c)=( *(bnd+c)*M[6] + *(bnd2+c)*M[7] + *(bnd3+c)*M[8] + *(vp+c) ) *B;

            // PREFIX, we do it "manually" (aka loop-unrolling)
#ifdef DEBUG_CORE_Y
	    std::cout << "GaussIIR:   backward: prefix starts, ";
#endif
	    for (c = 0; c < x_size; c++) {
		*(wrk+c)=( *(bnd+c)*M[0] + *(bnd2+c)*M[1] + *(bnd3+c)*M[2] + *(vp+c) ) *B;
	        REMOVE_REMAINDER_BACKWARD(*(wrk+c))
	    }
	    wrk -= x_size;

	    for (c = 0; c < x_size; c++) {
	        *(wrk+c)=*(wrk+c)*B  - *(wrk+c+x_size)*b1 - *(Bnd2+c)*b2 - *(Bnd3+c)*b3; //i=1
	        REMOVE_REMAINDER_BACKWARD(*(wrk+c))
	    }
	    wrk -= x_size;

	    for (c = 0; c < x_size; c++) {
	        *(wrk+c)=*(wrk+c)*B  - *(wrk+c+x_size)*b1 - *(wrk+c+x_2size)*b2 - *(Bnd2+c)*b3; //i=2
	        REMOVE_REMAINDER_BACKWARD(*(wrk+c))
	    }
	    wrk -= x_size;

#ifdef DEBUG_CORE_Y
	    std::cout << "convolutions of i=" << y_size-1 << "," << y_size-2 << "," << y_size-3 \
	    	      << " (slice=" << slices << ") is finished\n";
#endif
            // INFIX
            for (i = 3; i < y_size; i++)
            {
#ifdef DEBUG_CORE_Y
                std::cout << "GaussIIR:   backward: infix starts, ";
#endif
		for (c = 0; c < x_size; c++) {
		    *(wrk+c)=*(wrk+c) * B  - *(wrk+c+x_size)*b1 - *(wrk+c+x_2size)*b2 - *(wrk+c+x_3size)*b3;
	            REMOVE_REMAINDER_BACKWARD(*(wrk+c))
		}
#ifdef DEBUG_CORE_Y
                std::cout << "convolution of " << y_size-1-i << " (slice=" << slices << ") is finished\n";
#endif
                wrk -= x_size;
            }
        }

	//release allocated memory
	delete[] bnd;
	delete[] bnd2;
	delete[] bnd3;
	delete[] vp;
	delete[] Bnd2;
	delete[] Bnd3;

	wrk=img.GetFirstVoxelAddr(); //important
    }

    // ---- Z AXIS ----
    if ((img.GetSizeZ() > 1) && (SigmaZ >= 1.0))
    {
	double S=static_cast<double>(SigmaZ);
	ComputeBsFromSigma(S,BD,b1D,b2D,b3D);

	b1=static_cast<VOXEL>(b1D);
	b2=static_cast<VOXEL>(b2D);
	b3=static_cast<VOXEL>(b3D);
	B=static_cast<VOXEL>(BD);

	SuggestIIRBoundaries(-b1,-b2,-b3,M);
	const VOXEL denom=1.0 +b1+b2+b3;	//boundary handling...

        //z-axis filter is present...
        VOXEL *wrk2 = wrk;

#ifdef I3D_DEBUG
        std::cout << "GaussIIR: convolving in z-axis...\n";
#endif
#ifdef I3D_DEBUG
	std::cout << "GaussIIR: B=" << B << ", b1=" << b1 << ", b2=" << b2 << ", b3=" << b3 << std::endl;
#endif
	//storage of original input values before any y-axis related convolution
	VOXEL *bnd=new VOXEL[x_size];

        for (signed long cols = 0; cols < y_size; cols++)
        {
            wrk = wrk2 + (cols * x_size);

            // PREFIX, we do it "manually" (aka loop-unrolling)
#ifdef DEBUG_CORE_Z
	    std::cout << "GaussIIR:   forward: prefix starts, ";
#endif
	    for (c = 0; c < x_size; c++) {
		*(bnd+c)=*(wrk+c)/denom; //boundary handling...

	    	*(wrk+c)=*(wrk+c) - *(bnd+c)*(b1+b2+b3); //i=0
		REMOVE_REMAINDER_FORWARD(*(wrk+c))
	    }
	    wrk += s_size;

	    for (c = 0; c < x_size; c++) {
	    	*(wrk+c)=*(wrk+c)  - *(wrk+c-s_size)*b1 - *(bnd+c)*(b2+b3); //i=1
		REMOVE_REMAINDER_FORWARD(*(wrk+c))
	    }
	    wrk += s_size;

	    for (c = 0; c < x_size; c++) {
	    	*(wrk+c)=*(wrk+c)  - *(wrk+c-s_size)*b1 - *(wrk+c-s_2size)*b2 - *(bnd+c)*b3; //i=2
		REMOVE_REMAINDER_FORWARD(*(wrk+c))
	    }
	    wrk += s_size;

#ifdef DEBUG_CORE_Z
	    std::cout << "convolutions of i=0,1,2 (col=" << cols << ") is finished\n";
#endif
            // INFIX
            for (i = 3; i < (z_size-1); i++)
            {
#ifdef DEBUG_CORE_Z
                std::cout << "GaussIIR:   forward: infix starts, ";
#endif
		for (c = 0; c < x_size; c++) {
		    *(wrk+c)=*(wrk+c)  - *(wrk+c-s_size)*b1 - *(wrk+c-s_2size)*b2 - *(wrk+c-s_3size)*b3;
		    REMOVE_REMAINDER_FORWARD(*(wrk+c))
		}
#ifdef DEBUG_CORE_Z
                std::cout << "convolution of " << i << " (col=" << cols << ") is finished\n";
#endif
                wrk += s_size;
            }

            wrk += s_size;
        }

	wrk2=img.GetVoxelAddr(img.GetImageSize()) -s_size;

	VOXEL *bnd2=new VOXEL[x_size];
	VOXEL *bnd3=new VOXEL[x_size];
	VOXEL *vp=new VOXEL[x_size]; //to store v_plus in (15) in [Triggs and Sdika, 2006]

	VOXEL *Bnd2=new VOXEL[x_size];
	VOXEL *Bnd3=new VOXEL[x_size];

        for (signed long cols = 0; cols < y_size; cols++)
        {
            wrk = wrk2 + (cols * x_size);

	    //copy the voxel's value at the end of z-line and finish the forward convolution
	    for (c = 0; c < x_size; c++) {
		*(bnd+c)=*(wrk+c)/denom; //will be used *bnd instead of *up, just for this occassion

		//finishes the forward convolution
		*(wrk+c)=*(wrk+c) - *(wrk+c-s_size)*b1 - *(wrk+c-s_2size)*b2 - *(wrk+c-s_3size)*b3;
		REMOVE_REMAINDER_FORWARD(*(wrk+c))
	    }

	    for (c = 0; c < x_size; c++) *(vp+c)=*(bnd+c)/denom;
	    for (c = 0; c < x_size; c++) *(bnd3+c)=*(wrk+c-s_2size)-*(bnd+c);
	    for (c = 0; c < x_size; c++) *(bnd2+c)=*(wrk+c-s_size )-*(bnd+c);
	    for (c = 0; c < x_size; c++) *(bnd +c)=*(wrk+c        )-*(bnd+c);

	    //for (c = 0; c < x_size; c++) *(wrk +c)=( *(bnd+c)*M[0] + *(bnd2+c)*M[1] + *(bnd3+c)*M[2] + *(vp+c) ) *B;
	    for (c = 0; c < x_size; c++) *(Bnd2+c)=( *(bnd+c)*M[3] + *(bnd2+c)*M[4] + *(bnd3+c)*M[5] + *(vp+c) ) *B;
	    for (c = 0; c < x_size; c++) *(Bnd3+c)=( *(bnd+c)*M[6] + *(bnd2+c)*M[7] + *(bnd3+c)*M[8] + *(vp+c) ) *B;

            // PREFIX, we do it "manually" (aka loop-unrolling)
#ifdef DEBUG_CORE_Z
	    std::cout << "GaussIIR:   backward: prefix starts, ";
#endif
	    for (c = 0; c < x_size; c++) {
		*(wrk+c)=( *(bnd+c)*M[0] + *(bnd2+c)*M[1] + *(bnd3+c)*M[2] + *(vp+c) ) *B;
	        REMOVE_REMAINDER_BACKWARD(*(wrk+c))
	    }
	    wrk -= s_size;

	    for (c = 0; c < x_size; c++) {
	        *(wrk+c)=*(wrk+c)*B  - *(wrk+c+s_size)*b1 - *(Bnd2+c)*b2 - *(Bnd3+c)*b3; //i=1
	        REMOVE_REMAINDER_BACKWARD(*(wrk+c))
	    }
	    wrk -= s_size;

	    for (c = 0; c < x_size; c++) {
	        *(wrk+c)=*(wrk+c)*B  - *(wrk+c+s_size)*b1 - *(wrk+c+s_2size)*b2 - *(Bnd2+c)*b3; //i=2
	        REMOVE_REMAINDER_BACKWARD(*(wrk+c))
	    }
	    wrk -= s_size;

#ifdef DEBUG_CORE_Z
	    std::cout << "convolutions of i=" << z_size-1 << "," << z_size-2 << "," << z_size-3 \
	    	      << " (col=" << cols << ") is finished\n";
#endif
            // INFIX
            for (i = 3; i < z_size; i++)
            {
#ifdef DEBUG_CORE_Z
                std::cout << "GaussIIR:   backward: infix starts, ";
#endif
		for (c = 0; c < x_size; c++) {
		    *(wrk+c)=*(wrk+c) * B  - *(wrk+c+s_size)*b1 - *(wrk+c+s_2size)*b2 - *(wrk+c+s_3size)*b3;
	            REMOVE_REMAINDER_BACKWARD(*(wrk+c))
		}
#ifdef DEBUG_CORE_Z
                std::cout << "convolution of " << z_size-1-i << " (col=" << cols << ") is finished\n";
#endif
                wrk -= s_size;
            }
        }

	//release allocated memory
	delete[] bnd;
	delete[] bnd2;
	delete[] bnd3;
	delete[] vp;
	delete[] Bnd2;
	delete[] Bnd3;
    }

    delete[] M;				//release boundary handling matrix
}

// ---------------------------
// Vlado's IIR Gauss ends here
// ---------------------------




void MakeFWindow(int *f, const Neighbourhood &nb, vector<int> &win) 
{
  win.resize(nb.size());
  VectContainer::const_iterator off;
  int i = 0;
  for (off = nb.offset.begin(); off != nb.offset.end(); ++off)
    {
//        cout << "x = " << off->x << endl;
//        cout << "y = " << off->y << endl;
//        cout << "z = " << off->z << endl;
      size_t tmp = ((1 + off->y) * 3 + 1 + off->x);
//        cout << "win[" << i << "] = " << "f[" << tmp << "] = " << f[tmp] << endl;
      win[i++] = f[tmp];
    }
}

template<class VOXEL> VOXEL ApplFilter(const vector<VOXEL *> &img, 
                                       const vector<int> &filter)
{
  int sum = 0;
  int f = 0;
//    cout << "img.size() = " << img.size() << endl;
//    cout << "filter.size() = " << filter.size() << endl;
  for (size_t i = 0; i < img.size(); ++i) {
//      cout << "filter[i]" << endl;
//      cout << filter[i] << endl;
//      cout << img[i] << endl;
//      cout << "sum + = " << int(*(img[i])) << " * " << filter[i] << endl;
    sum += (*img[i]) * filter[i];
    f += filter[i];
  }
  sum = ((f == 0) ? sum : sum/f);
//    cout << "sum = " << abs(sum) << endl;
  return ::abs(sum/(int)img.size());
}

template<class VOXEL> void Sobel(Image3d<VOXEL>& img, 
	bool do_vertical, bool do_horizontal)
{
  int f_vert[9] = { 1, 0, -1, 2, 0, -2, 1, 0, -1};
  int f_horiz[9] = { 1, 2, 1, 0, 0, 0, -1, -2, -1};

  size_t x, y;
  int j;
  for (size_t z = 0; z < img.GetNumSlices(); ++z) {
    VOI<PIXELS> slice(0, 0, z, img.GetSizeX(), img.GetSizeY(), 1);
    Image3d<VOXEL> orig(img, &slice);
    size_t i = 0;
    vector<VOXEL *> winI;
    vector<int> winFv;
    vector<int> winFh;
    Neighbourhood nb;
    VOXEL v, h;

    while (i < orig.GetImageSize()) {
      x = orig.GetX(i); y = orig.GetY(i);
      j = GetNbh(orig, x, y, 0, nb2D_8, nb);
      MakeWindow(orig, x, y, 0, nb, winI);
      MakeFWindow(f_vert, nb, winFv);
      MakeFWindow(f_horiz, nb, winFh);

//        cout << "i = " << i << endl;
      for (int k = 0; k < j; ++k) {
//          cout << "k = " << k << endl;
        v = do_vertical ? ApplFilter(winI, winFv) : 0;
        h = do_horizontal ? ApplFilter(winI, winFh) : 0;
        if (do_vertical && do_horizontal) {
          v = (VOXEL)ceil(sqrt((double) v * v + h * h));
        } else {
          v = v + h;
        }
        img.SetVoxel(orig.GetX(i + k), orig.GetY(i + k), z, v);
        MoveWindow(winI);
      }
      
      i += j;
    }
  }
}


template <class VOXEL> void Laplace(const Image3d<VOXEL>&in, Image3d<VOXEL>&out)
{
	out.MakeRoom(in.GetSize());
	out.SetOffset(in.GetOffset());
	out.SetResolution(in.GetResolution());
	out.Clear();

	const VOXEL *p_left, *p_right, *p;
	VOXEL *q;
	size_t dx = in.GetSizeX();
	size_t dy = in.GetSizeY();
	for (size_t z = 0; z < in.GetSizeZ(); ++z) {
		for (size_t y = 0; y < in.GetSizeY(); ++y) {
			q = out.GetVoxelAddr(1,y,z);
			p = in.GetVoxelAddr(1,y,z);
			p_left = p - 1;
			p_right = p + 1;
			for (size_t x = 1; x < in.GetSizeX() - 1; ++x) {
				*q = *p_left + *p_right - 2 * *p;
				q ++; p_left = p; p = p_right; p_right ++;
			}
		}
	}
	for (size_t z = 0; z < in.GetSizeZ(); ++z) {
		for (size_t x = 0; x < in.GetSizeX(); ++x) {
			q = out.GetVoxelAddr(x,1,z);
			p = in.GetVoxelAddr(x,1,z);
			p_left = p - dx;
			p_right = p + dx;
			for (size_t y = 1; y < in.GetSizeY() - 1; ++y) {
				*q += *p_left + *p_right - 2 * *p;
				q += dx; p_left = p; p = p_right; p_right += dx;
			}
		}
	}

	for (size_t y = 0; y < in.GetSizeY(); ++y) {
		for (size_t x = 0; x < in.GetSizeX(); ++x) {
			q = out.GetVoxelAddr(x,y,1);
			p = in.GetVoxelAddr(x,y,1);
			p_left = p - dx * dy;
			p_right = p + dx * dy;
			for (size_t z = 1; z < in.GetSizeZ() - 1; ++z) {
				*q += *p_left + *p_right - 2 * *p;
				q += dx * dy; p_left = p; p = p_right; p_right += dx * dy;
			}
		}
	}
}


template <class T> void SuppresHotPixels(const Image3d<T>& in, Image3d<T>& out,
                      const Neighbourhood& neib, T thres)
{
  out.MakeRoom(in.GetSizeX(),in.GetSizeY(),in.GetSizeZ());
  out.SetOffset(in.GetOffset());
  out.SetResolution(in.GetResolution());
  
  size_t i = 0;
  vector<const T*> win;
  T* p = out.GetFirstVoxelAddr();
  
  while (i < in.GetImageSize())
    { 
      size_t num = GetWindow(in, in.GetX(i), in.GetY(i), in.GetZ(i),
                             neib, win);
      
      for (size_t j = 0; j < num; ++j)
        { 
          int s = int(*win[0]);
          for (size_t k = 1; k < win.size(); ++k)
            s = s + int(*win[k]);
          s /= win.size();
          
          *p = ::abs(s - int(*p)) < thres ? in.GetVoxel(i+j) : GetMedian<T>(win);

          MoveWindow(win);
          ++p;     
        }
      i += num;
    }
}



template <class VOXEL> void SigmaFilter(const Image3d<VOXEL>& in, Image3d<VOXEL>& out,
                 const Neighbourhood& neib, const Neighbourhood & neib1,
		 VOXEL sigma, int tres)
{
  out.MakeRoom(in.GetSizeX(),in.GetSizeY(),in.GetSizeZ());
  out.SetOffset(in.GetOffset());
  out.SetResolution(in.GetResolution());
  
  size_t i = 0;
  vector<const VOXEL*> win;
  vector <const VOXEL*> win1;
  typename StrictSumOf<VOXEL>::SUMTYPE low, high, s, s1, div;
  
  const VOXEL* ip = in.GetFirstVoxelAddr();
  VOXEL* op = out.GetFirstVoxelAddr();
  
  while (i < in.GetImageSize())
    { 
      size_t num = GetWindow(in, in.GetX(i), in.GetY(i), in.GetZ(i),
                             neib, win);
      GetWindow(in, in.GetX(i), in.GetY(i), in.GetZ(i),
                             neib1, win1);
      
      for (size_t j = 0; j < num; ++j)
        { 
	   
          low = high = s = (int)(*ip);
	  div = 1;
	  (low>=sigma?low-=sigma:low=0);
	  (high<=(255 - sigma)?high+=sigma:high=255);

          for (size_t k = 0; k < win.size(); ++k)
	  {
	      if (((*win[k]) >= low ) && ((*win[k]) <= high))
	      {
		  s +=  int(*win[k]);
		  div++;
	      }
	  }
          s /= div;
	  

	  if (div <= tres)
	  {
	      s1 = (int) (*win1[0]);
	      for (size_t k = 1; k < win1.size(); ++k)
	      {
		  s1 += int(*win1[k]);
	      }
	      s1 /= win1.size();
	      *op = s1;
	  }
	  else
	      *op = s;
          
          MoveWindow(win);
          MoveWindow(win1);
          ip++;     
          op++;     
        }
      i += num;
    }
}


// ----------------------------------------
// Vlado's SeparableConvolution starts here
// ----------------------------------------

// --------- struct Separable3DFilter ---------
template <class VOXEL> Separable3dFilter<VOXEL>::Separable3dFilter(struct Separable3dFilter<VOXEL> const &f, int mirror)
{
    // safeback init...
    xData=yData=zData=NULL;
    xLength=yLength=zLength=0;
    xDiv=yDiv=zDiv=1;

    VOXEL **tmp = new VOXEL *[3];
    tmp[0] = f.xData;
    tmp[1] = f.yData;
    tmp[2] = f.zData;
    int i, j;

    // "reduce" the number of pointers
    for (i = 0; i < 2; ++i)
        for (j = i + 1; j < 3; ++j)
            if (tmp[i] == tmp[j]) tmp[j] = NULL;

    // (mirror-)copy relevant data
    if (tmp[0] != NULL)
    {
        xData=new VOXEL[f.xLength];
        xLength=f.xLength; xDiv=f.xDiv;

	if (mirror) { for (i=0; i < xLength; ++i) xData[i]=f.xData[xLength-1 -i]; }
	else { for (i=0; i < xLength; ++i) xData[i]=f.xData[i]; }
    }
    if (tmp[1] != NULL)
    {
        yData=new VOXEL[f.yLength];
        yLength=f.yLength; yDiv=f.yDiv;

	if (mirror) { for (i=0; i < yLength; ++i) yData[i]=f.yData[yLength-1 -i]; }
	else { for (i=0; i < yLength; ++i) yData[i]=f.yData[i]; }
    }
    if (tmp[2] != NULL)
    {
        zData=new VOXEL[f.zLength];
        zLength=f.zLength; zDiv=f.zDiv;

	if (mirror) { for (i=0; i < zLength; ++i) zData[i]=f.zData[zLength-1 -i]; }
	else { for (i=0; i < zLength; ++i) zData[i]=f.zData[i]; }
    }

    delete[]tmp;

    // "bump-up back" the number of pointers
    if (f.yData == f.xData) { yData=xData; yLength=xLength; yDiv=xDiv; }
    if (f.zData == f.yData) { zData=yData; zLength=yLength; zDiv=yDiv; }
    else if (f.zData == f.xData) { zData=xData; zLength=xLength; zDiv=xDiv; }
}

template <class VOXEL> void Separable3dFilter<VOXEL>::PrintFilter(void) {
	std::cout << "3D filter with the following content:\nX: ";
	if (xLength > 0) {
		std::cout << xLength << " values: ";
		for (int i=0; i < (xLength-1); ++i) std::cout << xData[i] << ",";
		std::cout << xData[xLength-1] << " @" << reinterpret_cast<long>(xData) << ", ";
		std::cout << "divisor is " << xDiv << "\nY: ";

	} else std::cout << "empty\nY: ";
	if (yLength > 0) {
		std::cout << yLength << " values: ";
		for (int i=0; i < (yLength-1); ++i) std::cout << yData[i] << ",";
		std::cout << yData[yLength-1] << " @" << reinterpret_cast<long>(yData) << ", ";
		std::cout << "divisor is " << yDiv << "\nZ: ";

	} else std::cout << "empty\nZ: ";
	if (zLength > 0) {
		std::cout << zLength << " values: ";
		for (int i=0; i < (zLength-1); ++i) std::cout << zData[i] << ",";
		std::cout << zData[zLength-1] << " @" << reinterpret_cast<long>(zData) << ", ";
		std::cout << "divisor is " << zDiv << "\n\n";

	} else std::cout << "empty\n\n";
}

template <class VOXEL> void Separable3dFilter<VOXEL>::DisposeData(void)
{
    VOXEL **tmp = new VOXEL *[3];
    tmp[0] = xData;
    tmp[1] = yData;
    tmp[2] = zData;
    int i, j, imax = 3;

    for (i = 0; i < imax - 1; ++i)
        for (j = i + 1; j < imax; ++j)
        {
            if (tmp[i] == tmp[j])
                tmp[j] = NULL;
        }

    for (i = 0; i < imax; ++i)
    {
        if (tmp[i] != NULL)
            delete[]tmp[i];
        tmp[i] = NULL;
    }
    delete[]tmp;
}

template <class VOXEL> Separable3dFilter<VOXEL>::~Separable3dFilter()
{
	this->DisposeData();
}


// --------- struct BorderPadding ---------
template <class VOXEL> BorderPadding<VOXEL>::BorderPadding(struct BorderPadding<VOXEL> const &f, int mirror)
{
    // safeback init...
    xDataPre=yDataPre=zDataPre=NULL;
    xDataPost=yDataPost=zDataPost=NULL;
    xLength=yLength=zLength=0;

    VOXEL **tmp = new VOXEL *[6];
    tmp[0] = f.xDataPre;
    tmp[1] = f.xDataPost;
    tmp[2] = f.yDataPre;
    tmp[3] = f.yDataPost;
    tmp[4] = f.zDataPre;
    tmp[5] = f.zDataPost;
    int i, j;

    // "reduce" the number of pointers
    for (i = 0; i < 5; ++i)
        for (j = i + 1; j < 6; ++j)
            if (tmp[i] == tmp[j]) tmp[j] = NULL;

    // (mirror-)copy relevant data
    if (tmp[0] != NULL)
    {
        xDataPre=new VOXEL[f.xLength];
        if (f.xDataPre != f.xDataPost) xDataPost=new VOXEL[f.xLength]; else xDataPost=xDataPre;
        xLength=f.xLength;

	if (mirror) {
		for (i=0; i < xLength; ++i) xDataPre[i]=f.xDataPre[xLength-1 -i];
		if (f.xDataPre != f.xDataPost) for (i=0; i < xLength; ++i) xDataPost[i]=f.xDataPost[xLength-1 -i];
	} else {
		for (i=0; i < xLength; ++i) xDataPre[i]=f.xDataPre[i];
		if (f.xDataPre != f.xDataPost) for (i=0; i < xLength; ++i) xDataPost[i]=f.xDataPost[i];
	}
    }
    if (tmp[2] != NULL)
    {
        yDataPre=new VOXEL[f.yLength];
        if (f.yDataPre != f.yDataPost) yDataPost=new VOXEL[f.yLength]; else yDataPost=yDataPre;
        yLength=f.yLength;

	if (mirror) {
		for (i=0; i < yLength; ++i) yDataPre[i]=f.yDataPre[yLength-1 -i];
		if (f.yDataPre != f.yDataPost) for (i=0; i < xLength; ++i) yDataPost[i]=f.yDataPost[xLength-1 -i];
	} else {
		for (i=0; i < yLength; ++i) yDataPre[i]=f.yDataPre[i];
		if (f.yDataPre != f.yDataPost) for (i=0; i < xLength; ++i) yDataPost[i]=f.yDataPost[i];
	}
    }
    if (tmp[4] != NULL)
    {
        zDataPre=new VOXEL[f.zLength];
        if (f.zDataPre != f.zDataPost) zDataPost=new VOXEL[f.zLength]; else zDataPost=zDataPre;
        zLength=f.zLength;

	if (mirror) {
		for (i=0; i < zLength; ++i) zDataPre[i]=f.zDataPre[zLength-1 -i];
		if (f.zDataPre != f.zDataPost) for (i=0; i < xLength; ++i) zDataPost[i]=f.zDataPost[xLength-1 -i];
	} else {
		for (i=0; i < zLength; ++i) zDataPre[i]=f.zDataPre[i];
		if (f.zDataPre != f.zDataPost) for (i=0; i < xLength; ++i) zDataPost[i]=f.zDataPost[i];
	}
    }

    delete[]tmp;

    // "bump-up back" the number of pointers
    if (f.yDataPre == f.xDataPre) { yDataPre=xDataPre; yDataPost=xDataPost; yLength=xLength; }
    if (f.zDataPre == f.yDataPre) { zDataPre=yDataPre; zDataPost=yDataPost; zLength=yLength; }
    else if (f.zDataPre == f.xDataPre) { zDataPre=xDataPre; zDataPost=xDataPost; zLength=xLength; }
}

template <class VOXEL> void BorderPadding<VOXEL>::PrintPaddings(void) {
	std::cout << "Border paddings with the following content:\nX: ";
	if (xLength > 0) {
		std::cout << xLength << " values: ";
		for (int i=0; i < (xLength-1); ++i) std::cout << xDataPre[i] << ",";
		std::cout << xDataPre[xLength-1] << " @" << reinterpret_cast<long>(xDataPre) << " <-> ";
		for (int i=0; i < (xLength-1); ++i) std::cout << xDataPost[i] << ",";
		std::cout << xDataPost[xLength-1] << " @" << reinterpret_cast<long>(xDataPost) << "\nY: ";

	} else std::cout << "empty\nY: ";
	if (yLength > 0) {
		std::cout << yLength << " values: ";
		for (int i=0; i < (yLength-1); ++i) std::cout << yDataPre[i] << ",";
		std::cout << yDataPre[yLength-1] << " @" << reinterpret_cast<long>(yDataPre) << " <-> ";
		for (int i=0; i < (yLength-1); ++i) std::cout << yDataPost[i] << ",";
		std::cout << yDataPost[yLength-1] << " @" << reinterpret_cast<long>(yDataPost) << "\nZ: ";

	} else std::cout << "empty\nZ: ";
	if (zLength > 0) {
		std::cout << zLength << " values: ";
		for (int i=0; i < (zLength-1); ++i) std::cout << zDataPre[i] << ",";
		std::cout << zDataPre[zLength-1] << " @" << reinterpret_cast<long>(zDataPre) << " <-> ";
		for (int i=0; i < (zLength-1); ++i) std::cout << zDataPost[i] << ",";
		std::cout << zDataPost[zLength-1] << " @" << reinterpret_cast<long>(zDataPost) << "\n\n";

	} else std::cout << "empty\n\n";
}

template <class VOXEL> void BorderPadding<VOXEL>::DisposeData(void)
{
    VOXEL **tmp = new VOXEL *[6];
    tmp[0] = xDataPre;
    tmp[1] = xDataPost;
    tmp[2] = yDataPre;
    tmp[3] = yDataPost;
    tmp[4] = zDataPre;
    tmp[5] = zDataPost;

    int i, j, imax = 6;

    for (i = 0; i < imax - 1; ++i)
        for (j = i + 1; j < imax; ++j)
        {
            if (tmp[i] == tmp[j])
                tmp[j] = NULL;
        }

    for (i = 0; i < imax; ++i)
    {
        if (tmp[i] != NULL)
            delete[]tmp[i];
        tmp[i] = NULL;
    }
    delete[]tmp;
}

template <class VOXEL> BorderPadding<VOXEL>::~BorderPadding()
{
	this->DisposeData();
}


// --------- convolution function ---------
template <class VOXEL>
  int SeparableConvolution(i3d::Image3d<VOXEL> const &d, 
				 i3d::Image3d<VOXEL> &res,
				 struct Separable3dFilter<VOXEL> const &f,
				 struct BorderPadding<VOXEL> const &b)
{
    //first of all, a few integrity tests:
    //filter sizes must be odd or zero
    if (!(f.xLength & 1) && (f.xLength != 0))
    {
        std::cerr << "SeparableConvolution(): x-axis filter length is wrong!\n";
        return (1);
    }
    if (!(f.yLength & 1) && (f.yLength != 0))
    {
        std::cerr << "SeparableConvolution(): y-axis filter length is wrong!\n";
        return (1);
    }
    if (!(f.zLength & 1) && (f.zLength != 0))
    {
        std::cerr << "SeparableConvolution(): z-axis filter length is wrong!\n";
        return (1);
    }
#ifdef I3D_DEBUG
    std::cout << "SeparableConvolution(): filter sizes tests passed\n";
#endif

    //padding data length over both sides of given axis must be
    //exactly the filter length minus 1 for the given axis
    if ((2 * b.xLength != f.xLength - 1) && (f.xLength != 0))
    {

        std::cerr << "SeparableConvolution(): Padding in x-axis is insufficient!\n";
        return (2);
    }
    if ((2 * b.yLength != f.yLength - 1) && (f.yLength != 0))
    {
        std::cerr << "SeparableConvolution(): Padding in y-axis is insufficient!\n";
        return (2);
    }
    if ((2 * b.zLength != f.zLength - 1) && (f.zLength != 0))
    {
        std::cerr << "SeparableConvolution(): Padding in z-axis is insufficient!\n";
        return (2);
    }
#ifdef I3D_DEBUG
    std::cout << "SeparableConvolution(): border sizes vs filter sizes tests passed\n";
#endif

    //input/output image dimensions
    signed long x_size = (signed long) d.GetWidth();
    signed long y_size = (signed long) d.GetHeight();
    signed long z_size = (signed long) d.GetNumSlices();

    //determine in which image to store the intermediate results first: res or tmp?
    short output_selector=0;
    output_selector+=(f.xLength > 0)? 1: 0;
    output_selector+=(f.yLength > 0)? 1: 0;
    output_selector+=(f.zLength > 0)? 1: 0;

    res.SetOffset(d.GetOffset());
    res.SetResolution(d.GetResolution());
    if (output_selector > 0) res.MakeRoom(d.GetSize());

    //an alternating output image and its metadata
    Image3d <VOXEL> tmp;
    tmp.SetOffset(d.GetOffset());
    tmp.SetResolution(d.GetResolution());
    if (output_selector > 1) tmp.MakeRoom(d.GetSize());

#ifdef I3D_DEBUG
    std::cout << "SeparableConvolution(): input image of size " << x_size << "x" << y_size << "x" << z_size << " voxels\n";
    std::cout << "SeparableConvolution(): borders are " << b.xLength << ", " << b.yLength << ", " << b.zLength << " voxels wide\n";
    std::cout << "SeparableConvolution(): filter sizes are " << f.xLength << ", " << f.yLength << ", " << f.zLength << " voxels\n";
#endif

    //temporary variables
    signed long i = 0, j = 0, k = 0, c = 0;     //,c2=0;

    //in points to memory from where input for current convolution direction goes in
    //out points to memory where output for current convolution direction should go
    const VOXEL *in = d.GetFirstVoxelAddr();    //the very first input data
    VOXEL *out = res.GetFirstVoxelAddr();
    if (output_selector == 2) {
    	out=tmp.GetFirstVoxelAddr();
#ifdef I3D_DEBUG
        std::cout << "SeparableConvolution(): input set on the original input image\n";
        std::cout << "SeparableConvolution(): output set to the temporary image\n";
#endif
    } else {
#ifdef I3D_DEBUG
        std::cout << "SeparableConvolution(): input set on the original input image\n";
        std::cout << "SeparableConvolution(): output set to the output image\n";
#endif
    }

    // ---- X AXIS ----
    if (f.xLength > 0)
    {
        //x-axis filter is present...
#ifdef I3D_DEBUG
        std::cout << "SeparableConvolution(): convolving in x-axis...\n";
#endif
        //since prefix and postfix convolution code works with just constants,
        //we can precompute the results of all partial convolutions; this saves
        //some time in the main loop
        VOXEL *conv_pre = new VOXEL[b.xLength + 1];
        VOXEL *conv_post = new VOXEL[b.xLength + 1];
        if ((!conv_pre) || (!conv_post))
        {
            std::cerr << "SeparableConvolution(): No memory to store convolution constants!\n";

            if (conv_pre)
                delete[]conv_pre;
            if (conv_post)
                delete[]conv_post;
            return (3);
        }
        //initiate memory
        for (j = 0; j <= b.xLength; j++)
        {
            conv_pre[j] = 0;
            conv_post[j] = 0;
        }

        //compute partial convolutions
        for (j = 0; j < b.xLength; j++)
            for (k = 0; k < b.xLength - j; k++)
            {
                conv_pre[b.xLength - j] += b.xDataPre[k + j] * f.xData[k];
                conv_post[b.xLength - j] += b.xDataPost[k + j] * f.xData[b.xLength + 1 + k];
            }
#ifdef I3D_DEBUG
        std::cout << "SeparableConvolution():   partial convolutions precomputed\n";
#endif
        //for each row...
        signed long min = (b.xLength < x_size) ? b.xLength : x_size;
        signed long max = ((x_size - b.xLength) > b.xLength) ? (x_size - b.xLength) : b.xLength;

        for (signed long rows = 0; rows < y_size * z_size; rows++)
        {
            //no need to shift in and out pointers because
            //x-lines are sequentially in memory

            //j relative position, k position in filter

            // PREFIX
            for (i = 0; i < min; i++)
            {
#ifdef DEBUG_CORE_X
                std::cout << "SeparableConvolution(): PRE: prefix starts (k=0), ";
#endif
                //prefix
                k = b.xLength - i;
                *out = conv_pre[k];
#ifdef DEBUG_CORE_X
                std::cout << "infix starts (k=" << k << "), ";
#endif
                //infix
                for (j = -b.xLength + k; (i + j < x_size) && (k < f.xLength); j++, k++)
                    *out += *(in + j) * f.xData[k];
#ifdef DEBUG_CORE_X
                std::cout << "postfix starts (k=" << k << "), ";
#endif
                //postfix
                k = (k < f.xLength) ? f.xLength - k : 0;
                *out += conv_post[k];

                //this is better to compute afterwards, after all summings
                *out /= f.xDiv;
#ifdef DEBUG_CORE_X
                std::cout << "convolution of " << i << " (row=" << rows << ") is finished\n";
#endif
                in++;
                out++;
            }

            // INFIX
            for (i = b.xLength; i < (x_size - b.xLength); i++)
            {
                k = 0;
                *out = 0;
#ifdef DEBUG_CORE_X
                std::cout << "SeparableConvolution(): IN: infix starts (k=0), ";
#endif
                //infix
                for (j = -b.xLength; k < f.xLength; j++, k++)
                    *out += *(in + j) * f.xData[k];

                //this is better to compute afterwards, after all summings
                *out /= f.xDiv;
#ifdef DEBUG_CORE_X
                std::cout << "convolution of " << i << " (row=" << rows << ") is finished\n";
#endif
                in++;
                out++;
            }

            // POSTFIX
            for (i = max; i < x_size; i++)
            {
                k = 0;
                *out = 0;
#ifdef DEBUG_CORE_X
                std::cout << "SeparableConvolution(): POST: infix starts (k=0), ";
#endif
                //infix
                for (j = -b.xLength; i + j < x_size; j++, k++)
                    *out += *(in + j) * f.xData[k];
#ifdef DEBUG_CORE_X
                std::cout << "postfix starts (k=" << k << "), ";
#endif
                //postfix
                *out += conv_post[f.xLength - k];

                //this is better to compute afterwards, after all summings
                *out /= f.xDiv;
#ifdef DEBUG_CORE_X
                std::cout << "convolution of " << i << " (row=" << rows << ") is finished\n";
#endif
                in++;
                out++;
            }
        }

	if (output_selector == 2) {
	    //we were storing into tmp
	    in=tmp.GetFirstVoxelAddr();
	    out=res.GetFirstVoxelAddr();
#ifdef I3D_DEBUG
            std::cout << "SeparableConvolution():   input set on the temporary image\n";
            std::cout << "SeparableConvolution():   output set to the output image\n";
#endif
	} else {
	    //we were storing into res
	    //note: there might be no other convolution taking place (output_selector == 1)
	    in=res.GetFirstVoxelAddr();
	    out=tmp.GetFirstVoxelAddr();
#ifdef I3D_DEBUG
            std::cout << "SeparableConvolution():   input set on the output image\n";
            std::cout << "SeparableConvolution():   output set to the temporary image\n";
#endif
	}
    	--output_selector;

        delete[]conv_pre;
        delete[]conv_post;
        conv_pre = conv_post = NULL;
    }

    // ---- Y AXIS ----
    if (f.yLength > 0)
    {
        //y-axis filter is present...
#ifdef I3D_DEBUG
        std::cout << "SeparableConvolution(): convolving in y-axis...\n";
#endif
        //since prefix and postfix convolution code works with just constants,
        //we can precompute the results of all partial convolutions; this saves
        //some time in the main loop
        VOXEL *conv_pre = new VOXEL[b.yLength + 1];
        VOXEL *conv_post = new VOXEL[b.yLength + 1];
        if ((!conv_pre) || (!conv_post))
        {
            std::cerr << "SeparableConvolution(): No memory to store convolution constants!\n";

            if (conv_pre)
                delete[]conv_pre;
            if (conv_post)
                delete[]conv_post;
            return (3);
        }
        //initiate memory
        for (j = 0; j <= b.yLength; j++)
        {
            conv_pre[j] = 0;
            conv_post[j] = 0;
        }

        //compute partial convolutions
        for (j = 0; j < b.yLength; j++)
            for (k = 0; k < b.yLength - j; k++)
            {
                conv_pre[b.yLength - j] += b.yDataPre[k + j] * f.yData[k];
                conv_post[b.yLength - j] += b.yDataPost[k + j] * f.yData[b.yLength + 1 + k];
            }
#ifdef I3D_DEBUG
        std::cout << "SeparableConvolution():   partial convolutions precomputed\n";
#endif
        //we don't know the image to which the in pointer belongs, we need to copy it;
        //this is required for setting correct offsets of each column
        const VOXEL *in2 = in;
        VOXEL *out2 = out;

        //for each column...
        signed long min = (b.yLength < y_size) ? b.yLength : y_size;
        signed long max = ((y_size - b.yLength) > b.yLength) ? (y_size - b.yLength) : b.yLength;

        for (signed long slices = 0; slices < z_size; slices++)
        {
            in = in2 + (slices * y_size * x_size);
            out = out2 + (slices * y_size * x_size);

            //j relative position, k position in filter

            // PREFIX
            for (i = 0; i < min; i++)
            {
#ifdef DEBUG_CORE_Y
                std::cout << "SeparableConvolution(): PRE: prefix starts (k=0), ";
#endif
                //prefix
                k = b.yLength - i;
                //must go, since it initiates output memory
                for (c = 0; c < x_size; c++)
                    *(out + c) = conv_pre[k];
#ifdef DEBUG_CORE_Y
                std::cout << "infix starts (k=" << k << "), ";
#endif
                //infix
                for (j = -b.yLength + k; (i + j < y_size) && (k < f.yLength); j++, k++)
                    for (c = 0; c < x_size; c++)
                        *(out + c) += *(in + (j * x_size) + c) * f.yData[k];
#ifdef DEBUG_CORE_Y
                std::cout << "postfix starts (k=" << k << "), ";
#endif
                //postfix, the last two steps together...
                k = (k < f.yLength) ? f.yLength - k : 0;
                for (c = 0; c < x_size; c++)
                    *(out + c) = (*(out + c) + conv_post[k]) / f.yDiv;
#ifdef DEBUG_CORE_Y
                std::cout << "convolution of " << i << " (slice=" << slices << ") is finished\n";
#endif
                in += x_size;
                out += x_size;
            }

            // INFIX
            for (i = b.yLength; i < (y_size - b.yLength); i++)
            {
                k = 0;
                //must go, since it initiates output memory
                for (c = 0; c < x_size; c++)
                    *(out + c) = 0;
#ifdef DEBUG_CORE_Y
                std::cout << "SeparableConvolution(): IN: infix starts (k=0), ";
#endif
                //infix
                for (j = -b.yLength; k < f.yLength; j++, k++)
                    for (c = 0; c < x_size; c++)
                        *(out + c) += *(in + (j * x_size) + c) * f.yData[k];

                //this is better to compute afterwards, after all summings
                for (c = 0; c < x_size; c++)
                    *(out + c) /= f.yDiv;
#ifdef DEBUG_CORE_Y
                std::cout << "convolution of " << i << " (slice=" << slices << ") is finished\n";
#endif
                in += x_size;
                out += x_size;
            }

            // POSTFIX
            for (i = max; i < y_size; i++)
            {
                k = 0;
                //must go, since it initiates output memory
                for (c = 0; c < x_size; c++)
                    *(out + c) = 0;
#ifdef DEBUG_CORE_Y
                std::cout << "SeparableConvolution(): POST: infix starts (k=0), ";
#endif
                //infix
                for (j = -b.yLength; i + j < y_size; j++, k++)
                    for (c = 0; c < x_size; c++)
                        *(out + c) += *(in + (j * x_size) + c) * f.yData[k];
#ifdef DEBUG_CORE_Y
                std::cout << "postfix starts (k=" << k << "), ";
#endif
                //postfix, the last two steps together...
                for (c = 0; c < x_size; c++)
                    *(out + c) = (*(out + c) + conv_post[f.yLength - k]) / f.yDiv;
#ifdef DEBUG_CORE_Y
                std::cout << "convolution of " << i << " (slice=" << slices << ") is finished\n";
#endif
                in += x_size;
                out += x_size;
            }
        }

	in=out2; //that's the only sure thing
	if (out2 == tmp.GetFirstVoxelAddr()) {
	    out=res.GetFirstVoxelAddr();
#ifdef I3D_DEBUG
            std::cout << "SeparableConvolution():   input set to the temporary image\n";
            std::cout << "SeparableConvolution():   output set to the output image\n";
#endif
	} else {
	    out=tmp.GetFirstVoxelAddr();
#ifdef I3D_DEBUG
            std::cout << "SeparableConvolution():   input set to the output image\n";
            std::cout << "SeparableConvolution():   output set to the temporary image\n";
#endif
	}
    	--output_selector;

        delete[]conv_pre;
        delete[]conv_post;
        conv_pre = conv_post = NULL;
    }

    // ---- Z AXIS ----
    if (f.zLength > 0)
    {
        //z-axis filter is present...
#ifdef I3D_DEBUG
        std::cout << "SeparableConvolution(): convolving in z-axis...\n";
#endif
        //since prefix and postfix convolution code works with just constants,
        //we can precompute the results of all partial convolutions; this saves
        //some time in the main loop
        VOXEL *conv_pre = new VOXEL[b.zLength + 1];
        VOXEL *conv_post = new VOXEL[b.zLength + 1];
        if ((!conv_pre) || (!conv_post))
        {
            std::cerr << "SeparableConvolution(): No memory to store convolution constants!\n";

            if (conv_pre)
                delete[]conv_pre;
            if (conv_post)
                delete[]conv_post;
            return (3);
        }
        //initiate memory
        for (j = 0; j <= b.zLength; j++)
        {
            conv_pre[j] = 0;
            conv_post[j] = 0;
        }

        //compute partial convolutions
        for (j = 0; j < b.zLength; j++)
            for (k = 0; k < b.zLength - j; k++)
            {
                conv_pre[b.zLength - j] += b.zDataPre[k + j] * f.zData[k];
                conv_post[b.zLength - j] += b.zDataPost[k + j] * f.zData[b.zLength + 1 + k];
            }
#ifdef I3D_DEBUG
        std::cout << "SeparableConvolution():   partial convolutions precomputed\n";
#endif
        //we don't know the image to which the in pointer belongs, we need to copy it;
        //this is required for setting correct offsets of each column
        const VOXEL *in2 = in;
        VOXEL *out2 = out;

        const signed long s_size = x_size * y_size;     //just helping variable

        //for each slice...
        signed long min = (b.zLength < z_size) ? b.zLength : z_size;
        signed long max = ((z_size - b.zLength) > b.zLength) ? (z_size - b.zLength) : b.zLength;

        for (signed long cols = 0; cols < y_size; cols++)
        {
            in = in2 + (cols * x_size);
            out = out2 + (cols * x_size);

            //j relative position, k position in filter

            // PREFIX
            for (i = 0; i < min; i++)
            {
#ifdef DEBUG_CORE_Z
                std::cout << "SeparableConvolution(): PRE: prefix starts (k=0), ";
#endif
                //prefix
                k = b.zLength - i;
                //must go, since it initiates output memory
                for (c = 0; c < x_size; c++)
                    *(out + c) = conv_pre[k];
#ifdef DEBUG_CORE_Z
                std::cout << "infix starts (k=" << k << "), ";
#endif
                //infix
                for (j = -b.zLength + k; (i + j < z_size) && (k < f.zLength); j++, k++)
                    for (c = 0; c < x_size; c++)
                        *(out + c) += *(in + (j * s_size) + c) * f.zData[k];
#ifdef DEBUG_CORE_Z
                std::cout << "postfix starts (k=" << k << "), ";
#endif
                //postfix, the last two steps together...
                k = (k < f.zLength) ? f.zLength - k : 0;
                for (c = 0; c < x_size; c++)
                    *(out + c) = (*(out + c) + conv_post[k]) / f.zDiv;
#ifdef DEBUG_CORE_Z
                std::cout << "convolution of " << i << " (col=" << cols << ") is finished\n";
#endif
                in += s_size;
                out += s_size;
            }

            // INFIX
            for (i = b.zLength; i < (z_size - b.zLength); i++)
            {
                k = 0;
                //must go, since it initiates output memory
                for (c = 0; c < x_size; c++)
                    *(out + c) = 0;
#ifdef DEBUG_CORE_Z
                std::cout << "SeparableConvolution(): IN: infix starts (k=0), ";
#endif
                //infix
                for (j = -b.zLength; k < f.zLength; j++, k++)
                    for (c = 0; c < x_size; c++)
                        *(out + c) += *(in + (j * s_size) + c) * f.zData[k];

                //this is better to compute afterwards, after all summings
                for (c = 0; c < x_size; c++)
                    *(out + c) /= f.zDiv;
#ifdef DEBUG_CORE_Z
                std::cout << "convolution of " << i << " (col=" << cols << ") is finished\n";
#endif
                in += s_size;
                out += s_size;
            }

            // POSTFIX
            for (i = max; i < z_size; i++)
            {
                k = 0;
                //must go, since it initiates output memory
                for (c = 0; c < x_size; c++)
                    *(out + c) = 0;
#ifdef DEBUG_CORE_Z
                std::cout << "SeparableConvolution(): POST: infix starts (k=0), ";
#endif
                //infix
                for (j = -b.zLength; i + j < z_size; j++, k++)
                    for (c = 0; c < x_size; c++)
                        *(out + c) += *(in + (j * s_size) + c) * f.zData[k];
#ifdef DEBUG_CORE_Z
                std::cout << "postfix starts (k=" << k << "), ";
#endif
                //postfix, the last two steps together...
                for (c = 0; c < x_size; c++)
                    *(out + c) = (*(out + c) + conv_post[f.zLength - k]) / f.zDiv;
#ifdef DEBUG_CORE_Z
                std::cout << "convolution of " << i << " (col=" << cols << ") is finished\n";
#endif
                in += s_size;
                out += s_size;
            }
        }

    	--output_selector;
        in = out = NULL;
        delete[]conv_pre;
        delete[]conv_post;
        conv_pre = conv_post = NULL;
    }

    tmp.DisposeData();
    return (0);
}
// --------------------------------------
// Vlado's SeparableConvolution ends here
// --------------------------------------


// ----------------------------------------
// Vlado's NaiveConvolution starts here
// ----------------------------------------
template <class VOXEL>
int NaiveConvolution(Image3d<VOXEL> const &in, Image3d<VOXEL> const &kern, Image3d<VOXEL> &out, const VOXEL border)
{
	//questionable timesaver...
	const int xLength=kern.GetSizeX();
	const int yLength=kern.GetSizeY();
	const int zLength=kern.GetSizeZ();

	//stupid convolution should have stupid requirements, right?
	if ( ((xLength & 1) == 0) || ((yLength & 1) == 0) || ((zLength & 1) == 0) ) {
		std::cerr << "NaiveConvolution(): The kernel image should have odd sizes " \
			  << "but it has (" << xLength << "," << yLength << "," << zLength << ").\n";
		return(-1);
	}

	//prepare the memory for output
	out.MakeRoom(in.GetSize());
	out.SetResolution(in.GetResolution());
	out.SetOffset(in.GetOffset());

#ifdef I3D_DEBUG
	std::cout << "NaiveConvolution(): Output image is prepared, size is (" << out.GetSizeX() << "," << out.GetSizeY() << "," << out.GetSizeZ() << ")\n";
	std::cout << "NaiveConvolution(): Will do 3D full convolution with kernel (" << kern.GetSizeX() << "," << kern.GetSizeY() << "," << kern.GetSizeZ() << ")\n";
#endif

	//helper variables
	VOXEL *O=out.GetFirstVoxelAddr();
	int x,y,z;
	int i,j,k;

	//size of half-span of the filter (here and below is why we need the stupid requirement/test above...)
	const int hx=(xLength-1)/2;
	const int hy=(yLength-1)/2;
	const int hz=(zLength-1)/2;

	if (border == 0.0) {
	//
	//we may "ignore" the outside voxels as they all are zero and can't contribute to the convolution
	//
	for (z=0; z < (signed)in.GetSizeZ(); ++z) {
#ifdef I3D_DEBUG
	  std::cout << "NaiveConvolution(): doing slice " << z << "\n";
#endif

	  for (y=0; y < (signed)in.GetSizeY(); ++y) {
	    for (x=0; x < (signed)in.GetSizeX(); ++x) {
		//starting (bEGINNING) voxels of the kernel not to reach beyond the border
		const int bx=(x < hx)? hx-x : 0;
		const int by=(y < hy)? hy-y : 0;
		const int bz=(z < hz)? hz-z : 0;

		//ending (sTOPPING) voxels of the kernel not to reach beyond the border
		int sx=in.GetSizeX()-x; sx=(sx > hx)? xLength : xLength -hx +sx-1;
		int sy=in.GetSizeY()-y; sy=(sy > hy)? yLength : yLength -hy +sy-1;
		int sz=in.GetSizeZ()-z; sz=(sz > hz)? zLength : zLength -hz +sz-1;

		*O=0; //init the convolution sum

		const VOXEL *K=kern.GetVoxelAddr(bx,by,bz);
		const VOXEL *I=in.GetVoxelAddr(x-hx+bx, y-hy+by, z-hz+bz);
		
		for (k=bz; k < sz; ++k) {
		  for (j=by; j < sy; ++j) {
		    for (i=bx; i < sx; ++i, ++K, ++I) *O += *K * *I;

		    K+=(xLength-sx) + bx;
		    I+=in.GetSizeX() - (sx-bx);
		  }

		  K+=((yLength-sy) + by) *xLength;
		  I+=(in.GetSizeY() - (sy-by)) *in.GetSizeX();
		}

	    	++O;				//move on to the next voxel
	    }
	  }
	}

	} else {
	//
	//hmm... we may need the following helper arrays
	//
        VOXEL *conv_zpre = new VOXEL[hz+1];
        VOXEL *conv_zpost = new VOXEL[hz+1];
	if ( (!conv_zpre) || (!conv_zpre) ) {
		std::cerr << "NaiveConvolution(): Couldn't grab memory for extension arrays!\n";

		if (conv_zpre)  delete[] conv_zpre;
		if (conv_zpost) delete[] conv_zpost;

		return(-2);
	}

	const VOXEL *K=kern.GetFirstVoxelAddr();

	//precompute portions of convolutions to save some computational time later
	conv_zpre[0]=0;
	//up to hz slices may be skipped
	for (k=0; k < hz; ++k) {

	  //initiate a current slice to continue with the value of the previous slice
	  conv_zpre[k+1]=conv_zpre[k];

	  for (j=0; j < yLength; ++j)
	    for (i=0; i < xLength; ++i, ++K)
	    	conv_zpre[k+1]+=*K * border;
	}

	//point to the very last voxel in the kernel image
	K=kern.GetVoxelAddr(kern.GetImageSize()) -1;

	conv_zpost[0]=0;
	//up to hz slices may be skipped
	for (k=0; k < hz; ++k) {

	  //initiate a current slice to continue with the value of the previous slice
	  conv_zpost[k+1]=conv_zpost[k];

	  for (j=0; j < yLength; ++j)
	    for (i=0; i < xLength; ++i, --K)
	    	conv_zpost[k+1]+=*K * border;
	}

#ifdef I3D_DEBUG
	std::cout << "NaiveConvolution(): Helper extension arrays were successfully precomputed.\n";
#endif

	for (z=0; z < (signed)in.GetSizeZ(); ++z) {
#ifdef I3D_DEBUG
	  std::cout << "NaiveConvolution(): doing slice " << z << "\n";
#endif

	  for (y=0; y < (signed)in.GetSizeY(); ++y) {
	    for (x=0; x < (signed)in.GetSizeX(); ++x) {
		//starting (bEGINNING) voxels of the kernel not to reach beyond the border
		const int bx=(x < hx)? hx-x : 0;
		const int by=(y < hy)? hy-y : 0;
		const int bz=(z < hz)? hz-z : 0;

		//ending (sTOPPING) voxels of the kernel not to reach beyond the border
		int sx=in.GetSizeX()-x; sx=(sx > hx)? xLength : xLength -hx +sx-1;
		int sy=in.GetSizeY()-y; sy=(sy > hy)? yLength : yLength -hy +sy-1;
		int sz=in.GetSizeZ()-z; sz=(sz > hz)? zLength : zLength -hz +sz-1;

		const VOXEL *K=kern.GetVoxelAddr(0,0,bz);
		const VOXEL *I=in.GetVoxelAddr(x-hx+bx, y-hy+by, z-hz+bz);
		
		//the first bz slices of the kernel would be skipped
		*O=conv_zpre[bz];				   //but it won't happen ;-)

		for (k=bz; k < sz; ++k) {

		  //the first by rows of the kernel (at certain slice!) would be skipped;
		  //
		  //the "certain slice" complicates the use of precomputed values since
		  //there may be different values in the same rows but different slices
		  //in the kernel image, and so we would have to precompute rows for every
		  //slice separately and that would need some more memory
		  //
		  //the same holds for columns in the next sub-cycle

		  for (j=0; j < by; ++j) for (i=0; i < xLength; ++i, ++K) *O += *K * border;

		  for (j=by; j < sy; ++j) {

		    //the first bx cols of the kernel (on certain row!) would be skipped
		    for (i=0; i < bx; ++i, ++K) *O += *K * border;

		    for (i=bx; i < sx; ++i, ++K, ++I) *O += *K * *I;

		    //the last xLength-sx cols of the kernel would be skipped
		    for (i=sx; i < xLength; ++i, ++K) *O += *K * border;

		    I+=in.GetSizeX() - (sx-bx);
		  }

		  //the last yLength-sy rows of the kernel would be skipped
		  for (j=sy; j < yLength; ++j) for (i=0; i < xLength; ++i, ++K) *O += *K * border;

		  I+=(in.GetSizeY() - (sy-by)) *in.GetSizeX();
		}

		//the last zLength-sz slices of the kernel would be skipped
		*O+=conv_zpost[zLength-sz];

	    	++O;				//move on to the next voxel
	    }
	  }
	}

	//frees extension arrays
	delete[] conv_zpre;
	delete[] conv_zpost;
	}

	return(0);
}
// ----------------------------------------
// Vlado's NaiveConvolution ends here
// ----------------------------------------


/***************************************************************************/ 
					 

/* Explicit instantiations: */
template class I3D_DLLEXPORT Filter<double>;
template class I3D_DLLEXPORT Filter<GRAY8>;
template class I3D_DLLEXPORT Filter<GRAY16>;

/*template Filter<GRAY8>::Filter  (const Filter<double>&);
template Filter<GRAY16>::Filter (const Filter<double>&);
template Filter<GRAY8>::Filter  (const Filter<GRAY16>&);
template Filter<GRAY16>::Filter (const Filter<GRAY8>&);*/

template I3D_DLLEXPORT void Convert(Filter<double>&, const Filter<double>&);
template I3D_DLLEXPORT void Convert(Filter<GRAY8>&,  const Filter<GRAY8>&);
template I3D_DLLEXPORT void Convert(Filter<GRAY16>&, const Filter<GRAY16>&);
template I3D_DLLEXPORT void Convert(Filter<GRAY8>&,  const Filter<double>&);
template I3D_DLLEXPORT void Convert(Filter<GRAY16>&, const Filter<double>&);
template I3D_DLLEXPORT void Convert(Filter<GRAY8>&,  const Filter<GRAY16>&);
template I3D_DLLEXPORT void Convert(Filter<GRAY16>&, const Filter<GRAY8>&);

template I3D_DLLEXPORT void Gauss(Image3d<RGB>&, double, double, double, double, bool);
template I3D_DLLEXPORT void Gauss(Image3d<GRAY8>&, double, double, double, double, bool);
template I3D_DLLEXPORT void Gauss(Image3d<GRAY16>&, double, double, double, double, bool);
template I3D_DLLEXPORT void Gauss(Image3d<float>&, double, double, double, double, bool);

template I3D_DLLEXPORT void GaussIIR(Image3d<float>&, const float);
template I3D_DLLEXPORT void GaussIIR(Image3d<double>&, const double);
template I3D_DLLEXPORT void GaussIIR(Image3d<float>&, const float, const float, const float);
template I3D_DLLEXPORT void GaussIIR(Image3d<double>&, const double, const double, const double);

template I3D_DLLEXPORT void GaussFIR(Image3d<float> const &, Image3d<float>&, const float, const float);
template I3D_DLLEXPORT void GaussFIR(Image3d<double> const &, Image3d<double>&, const double, const float);
template I3D_DLLEXPORT void GaussFIR(Image3d<float>&, const float, const float);
template I3D_DLLEXPORT void GaussFIR(Image3d<double>&, const double, const float);
template I3D_DLLEXPORT void GaussFIR(Image3d<float> const &, Image3d <float>&, const float, const float, const float, const float, const float, const float);
template I3D_DLLEXPORT void GaussFIR(Image3d<double> const &, Image3d <double>&, const double, const double, const double, const float, const float, const float);
template I3D_DLLEXPORT void GaussFIR(Image3d<float>&, const float, const float, const float, const float, const float, const float);
template I3D_DLLEXPORT void GaussFIR(Image3d<double>&, const double, const double, const double, const float, const float, const float);

template I3D_DLLEXPORT void Sobel(Image3d<GRAY8>&, bool, bool);

template I3D_DLLEXPORT void Laplace(const Image3d<float>&in, Image3d<float>&out);

template I3D_DLLEXPORT void SuppresHotPixels(const Image3d<GRAY8>&, Image3d<GRAY8>&, const Neighbourhood&, GRAY8);
template I3D_DLLEXPORT void SuppresHotPixels(const Image3d<GRAY16>&, Image3d<GRAY16>&, const Neighbourhood&, GRAY16);
template I3D_DLLEXPORT void SuppresHotPixels(const Image3d<float>&, Image3d<float>&, const Neighbourhood&, float);

template I3D_DLLEXPORT void ApplyGauss1D(unsigned long* restrict data, const slice& slc, Filter<double>& f, Buffer& b, bool divide);

template I3D_DLLEXPORT void SigmaFilter(const Image3d<GRAY8>& in, Image3d<GRAY8>& out,
                                        const Neighbourhood& neib, const Neighbourhood & neib1,
		                                GRAY8 sigma, int tres);
template I3D_DLLEXPORT void SimpleGauss(Image3d<GRAY8>& img, double sigma, bool exact);
template I3D_DLLEXPORT void SimpleGauss(Image3d<float>& img, double sigma, bool exact);

template I3D_DLLEXPORT struct Separable3dFilter<float>;
template I3D_DLLEXPORT struct Separable3dFilter<double>;
template I3D_DLLEXPORT struct BorderPadding<float>;
template I3D_DLLEXPORT struct BorderPadding<double>;
template I3D_DLLEXPORT void PrepareZeroBoundaryForFilter(struct Separable3dFilter <float> const &filter, BorderPadding<float> &b);
template I3D_DLLEXPORT void PrepareZeroBoundaryForFilter(struct Separable3dFilter <double> const &filter, BorderPadding<double> &b);

template I3D_DLLEXPORT void EstimateGaussKernel(struct Separable3dFilter<float> &, const float, const float, const float, const float, const float, const float);
template I3D_DLLEXPORT void EstimateGaussKernel(struct Separable3dFilter<double> &, const double, const double, const double, const float, const float, const float);

template I3D_DLLEXPORT int SeparableConvolution(i3d::Image3d<float> const &d,
					i3d::Image3d<float> &res,
					struct Separable3dFilter<float> const &f,
					struct BorderPadding<float> const &b);

template I3D_DLLEXPORT int SeparableConvolution(i3d::Image3d<double> const &d,
					i3d::Image3d<double> &res,
					struct Separable3dFilter<double> const &f,
					struct BorderPadding<double> const &b);

template I3D_DLLEXPORT int NaiveConvolution(Image3d<float> const &, Image3d<float> const &, Image3d<float> &, const float);

template I3D_DLLEXPORT int NaiveConvolution(Image3d<double> const &, Image3d<double> const &, Image3d<double> &, const double);

template I3D_DLLEXPORT int NaiveConvolution(Image3d<GRAY8> const &, Image3d<GRAY8> const &, Image3d<GRAY8> &, const GRAY8);

template I3D_DLLEXPORT int NaiveConvolution(Image3d<GRAY16> const &, Image3d<GRAY16> const &, Image3d<GRAY16> &, const GRAY16);

}
