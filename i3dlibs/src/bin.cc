/*
 * i3dlib - image manipulation library
 *
 * Copyright (C) 2000-2006   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/* FILE: bin.cc
 *
 * routines for handling with binary images
 *
 * David Svoboda <svoboda@fi.muni.cz> 2002, 2009
 *
 */

#include <vector>

#include "basic.h"
#include "neighbours.h"
#include "bin.h"
#include "draw.h"
#include "vector3d.h"

#ifdef __GNUG__
#pragma implementation
#endif


namespace i3d
{
   using namespace std;

   const unsigned int NumOfCuts = 9;
   const int cuts[][3 * 8] = {
    {-1, 1, 0, 0, 1, 0, 1, 1, 0, 1, 0, 0, 1, -1, 0, 0, -1, 0, -1, -1, 0, -1,
     0, 0},
    {-1, 0, 1, -1, 0, 0, -1, 0, -1, 0, 0, -1, 1, 0, -1, 1, 0, 0, 1, 0, 1, 0,
     0, 1},
    {0, 1, 1, 0, 1, 0, 0, 1, -1, 0, 0, -1, 0, -1, -1, 0, -1, 0, 0, -1, 1, 0,
     0, 1},
    {-1, 1, 1, -1, 1, 0, -1, 1, -1, 0, 0, -1, 1, -1, -1, 1, -1, 0, 1, -1, 1,
     0, 0, 1},
    {-1, -1, 1, -1, -1, 0, -1, -1, -1, 0, 0, -1, 1, 1, -1, 1, 1, 0, 1, 1, 1,
     0, 0, 1},
    {-1, 1, 1, -1, 0, 0, -1, -1, -1, 0, -1, -1, 1, -1, -1, 1, 0, 0, 1, 1, 1,
     0, 1, 1},
    {-1, 1, -1, 0, 1, -1, 1, 1, -1, 1, 0, 0, 1, -1, 1, 0, -1, 1, -1, -1, 1,
     -1, 0, 0},
    {-1, 1, 1, 0, 1, 0, 1, 1, -1, 1, 0, -1, 1, -1, -1, 0, -1, 0, -1, -1, 1,
     -1, 0, 1},
    {-1, 1, -1, 0, 1, 0, 1, 1, 1, 1, 0, 1, 1, -1, 1, 0, -1, 0, -1, -1, -1, -1,
     0, -1}
  };

/************************************************************************/
/* BinFillHoles */
/************************************************************************/
  void BinFillHoles (Image3d <BINARY> &img, 
							size_t x, 
							size_t y, 
							size_t z,
							const Neighbourhood &nb)
  {
#ifdef I3D_DEBUG
    std::cout << "Filling holes." << std::endl;
#endif

    // Swamp the image with water. Origin of the flood is placed
    // into the (x,y,z) position. Default is (0,0,0)
	 Image3d<BINARY> flood_img;
	 flood_img.CopyMetaData(img);

	 FloodFill (img, flood_img, x, y, z, BINARY(1), nb);

    // While the image background is flooded with water,
	 // background voxels hidden in the objects stay dry.
    // These dry places should be filled
	 BINARY *it_orig = img.GetFirstVoxelAddr(),
			  *it_flood = flood_img.GetFirstVoxelAddr();

	 for (size_t i=0; i<img.GetImageSize(); i++)
	 {
		  if ((*it_flood == BINARY(0)) && (*it_orig == BINARY(0))) 
		  {
				*it_orig = BINARY(1);
		  }

		  it_orig++;
		  it_flood++;
	 }

#ifdef I3D_DEBUG
    std::cout << "... done." << std::endl;
#endif

  }

/************************************************************************/
  void EraseProtrusionVoxels (Image3d < BINARY > &img, 
										const Image3d<BINARY> & img_boundary)
  {
    std::vector <BINARY*> adjacent;

	 for (size_t i=0; i<img.GetImageSize(); i++)
	 {
        if (img_boundary.GetVoxel (i) == BINARY(1))
          {
            // We are interested in current voxel and its neighbourhood.
            GetWindow (img, 
							  img.GetX (i), img.GetY (i), img.GetZ (i),
                       nb3D_6, 
							  adjacent);

            unsigned fg_voxels = 0;

            for (size_t k=0; k<adjacent.size() && (fg_voxels==0); k++)
				{
              if (*adjacent[k] == BINARY(1))
                fg_voxels++;
				}

				// If the voxel is alone (there are no neighbours), remove it
            if (fg_voxels == 0)
				{
              img.SetVoxel (i, BINARY(0));
				}
          }
      }
  }

/************************************************************************/
bool EraseUnambiguousVoxels (Image3d<BINARY> &img, 
									  const Image3d<BINARY> &img_boundary)
{
    size_t i = 0, j, k, boundary_voxels;
    std::vector <const BINARY*> adjacent;
    bool unambiguous = true;
    Neighbourhood nb2D_8cuts[NumOfCuts];

    // create the appropriate neighbourhoods
    for (j = 0; j < NumOfCuts; j++)
	 {
      nb2D_8cuts[j] = Neighbourhood (cuts[j], 8);
	 }

    // search the entire of the image       
    while (i < img.GetImageSize ())
      {
        if (img_boundary.GetVoxel (i) == BINARY(1))
          {
            for (j = 0; j < NumOfCuts; j++)
              {
                boundary_voxels = 0;

                GetWindow (img_boundary,
									img_boundary.GetX (i),
									img_boundary.GetY (i), 
									img_boundary.GetZ (i),
                           nb2D_8cuts[j], 
									adjacent);

                for (k = 0; k < adjacent.size (); k++)
                  {
                    if (*adjacent[k] == BINARY(1))
                      boundary_voxels++;
                  }

                if (boundary_voxels > 2)
                  {
                    img.SetVoxel (i, BINARY(0));
                    unambiguous = false;
                    break;
                  }
              }
          }
        i++;
      }

    return (!unambiguous);
}

/************************************************************************/
void FindBoundaryVoxels(const Image3d<BINARY>& img, 
								Image3d<BINARY> &img_boundary)
	{
	size_t i=0, j, k, num;
	std::vector<const BINARY*> adjacent;
	bool WinSkipped = true;
	bool boundary_voxel;

	img_boundary.Clear();
  
   // track the whole (3D) image
   while (i < img.GetImageSize())
		{ 
		// Is this the interesting voxel? If not, there is no point to take care
		// of neighbouring window mask
		if (img.GetVoxel(i) == BINARY(1))
			{
			// We are interested in current voxel and its neighbourhood.
			num =	GetWindow(img, img.GetX(i), img.GetY(i), img.GetZ(i), 
								 nb3D_6, adjacent);
			WinSkipped = false;

			// Now we shift the neighbouring window for one voxel to the latter
			for (j=0 ; (j<num) && (!WinSkipped); j++)
				{
				// Get the current voxel value 
				if (img.GetVoxel(i+j) == BINARY(1))
					{
					// mark the boundary voxels with temporary value
					boundary_voxel = false;

					for (k=0; k<adjacent.size() && (!boundary_voxel); k++)
						boundary_voxel = (*adjacent[k] == BINARY(0));

					if (boundary_voxel)
						img_boundary.SetVoxel(i+j, BINARY(1));
							  
					MoveWindow(adjacent); 
					}
				else
					{
					WinSkipped = true;
					}
				}	  
			i += j-1;
			}
		// It is uninteresting voxel - we move to the next one without 
		// recomputing neighbouring window. 
		else
			{
			i++;
			WinSkipped = true;
			}
		}
	}
  
/************************************************************************/
/* BinUnambiguous */
/************************************************************************/
  void BinUnambiguous (Image3d < BINARY > &img)
  {

#ifdef I3D_DEBUG
    std::cout << "Making the boundary of objects unambiguous." << std::endl;
#endif

	 // Initialize the temporary image
	 Image3d<BINARY> boundary;
	 boundary.CopyMetaData(img);

    // Adjust the boundary to be unambiguous
    bool is_modified;
    do
      {
        // Find and mark all boundary voxels
		  FindBoundaryVoxels(img, boundary);

        // Erase protrusion voxels
        EraseProtrusionVoxels (img, boundary);

        // Erase unambiguous voxels
        is_modified = EraseUnambiguousVoxels (img, boundary);

      }
    while (is_modified);

#ifdef I3D_DEBUG
    std::cout << "... done." << std::endl;
#endif


  }


/************************************************************************/
/* BinPartialConvex */
/************************************************************************/
  void BinPartialConvex (Image3d < BINARY > &img, size_t Range)
  {
	const int rect[] = 
	{ 
		 1, 0, 0, 
		 1, -1, 0, 
		 0, -1, 0, 
		 -1, -1, 0, 
		 -1, 0, 0, 
		 -1, 1, 0, 
		 0, 1, 0, 
		 1, 1, 0
	};
	const Neighbourhood nb2D_conv(rect, 8);
	  
    long int First_x, First_y, Curr_x, Curr_y;
    size_t FirstVoxelOffset = 0, CurrentVoxelOffset, LastVoxelOffset;
    size_t BoundaryLength, NumOfVoxels, VoxelNum, NextVoxelOffset;
    size_t LocalRange, max_y = img.GetSizeY() - 1;
    bool FoundFirst, FoundAdjVoxel, intersection;
    int Delta, dX1, dY1, dX2, dY2;
    double MaxAngle, CurrentAngle, CosAngle, norm1, norm2;
    Vector3d < size_t > LinePoint, StartPoint, EndPoint;
    std::list < Vector3d < int > >line;
    std::list < Vector3d < int > >::iterator p;

    std::vector < size_t > Boundary;
    size_t i, j, k;
    std::vector < BINARY * >adjacent;

#ifdef I3D_DEBUG
    cout << "Making partial convex hull (convex in each slice)." << endl;
#endif

	Image3d<BINARY> img_boundary;
	img_boundary.CopyMetaData(img);

    // Find boundary voxels
	 FindBoundaryVoxels(img, img_boundary);

    // Make the convex hull for each of the slices  
    for (size_t slice = 0; slice < img.GetSizeZ (); slice++)
      {
        // Go through all the objects in image       
		  do
          {
            // Find one of the boundary voxels in the current slice
            FoundFirst = false;

				for (size_t y = 0; (y < img.GetSizeY()) && (!FoundFirst); y++)
					for (size_t x = 0; (x < img.GetSizeX()) && (!FoundFirst); x++)
						if (img_boundary.GetVoxel(x, max_y - y, slice) == BINARY(1))
						{
								  FoundFirst = true;
								  FirstVoxelOffset = 
											 img_boundary.GetIndex(x, max_y - y, slice);
						}

            // If no boundary voxels was found, the are two explanations
            // - all objects have been already convex hull covered
            // - there are no objects in the image
            if (!FoundFirst)
                break;

            // Choose the first voxel - among all the boundary voxels
            // of current object - and save its position
            First_x = img_boundary.GetX (FirstVoxelOffset);
            First_y = img_boundary.GetY (FirstVoxelOffset);

            // Collect the boundary voxels and build up the sequence
            // called "Boundary"
            NumOfVoxels = 0;
            CurrentVoxelOffset = FirstVoxelOffset;
            Boundary.clear ();

            do
              {
                Boundary.push_back (CurrentVoxelOffset);
                img_boundary.SetVoxel (CurrentVoxelOffset, BINARY(0));

                GetWindow (img_boundary,
                           img_boundary.GetX (CurrentVoxelOffset),
                           img_boundary.GetY (CurrentVoxelOffset),
                           img_boundary.GetZ (CurrentVoxelOffset), 
									nb2D_conv, 
									adjacent);

                FoundAdjVoxel = false;
                for (k = 0;  k < adjacent.size() && (!FoundAdjVoxel); k++)
                  if (*adjacent[k] == BINARY(1))
                    {
                      CurrentVoxelOffset =
                        adjacent[k] - img_boundary.GetFirstVoxelAddr ();
                      FoundAdjVoxel = true;
                    }
              }
            while (FoundAdjVoxel);

            // If the boundary graph doesn't form a circle, there's no reason
            // to continue with computation
            Curr_x = img_boundary.GetX (CurrentVoxelOffset);
            Curr_y = img_boundary.GetY (CurrentVoxelOffset);

            if ((std::abs (Curr_x - First_x) > 1) || 
					 (std::abs (Curr_y - First_y) > 1))
              {
                throw
                  InternalException
                  ("i3d::BinPartialConvex::Unable to compute convex hull."
						 " Disconnected boundary.");
              }

            // Modified Jarvis march:
            CurrentVoxelOffset = FirstVoxelOffset;
            LastVoxelOffset = FirstVoxelOffset;
            LastVoxelOffset--;
            VoxelNum = 0;
            BoundaryLength = Boundary.size ();

            while (VoxelNum < BoundaryLength)
              {
                Delta = 0;
                MaxAngle = 0;
                LocalRange = std::min (Range, BoundaryLength - 1);

                // We're looking for the best line with maximum 
					 // length of LocalRange covering the nearest cave. 
                for (i = 1; i <= LocalRange; i++)
                  {
                    dX1 =
                      img_boundary.GetX (LastVoxelOffset) -
                      img_boundary.GetX (CurrentVoxelOffset);
                    dY1 =
                      img_boundary.GetY (LastVoxelOffset) -
                      img_boundary.GetY (CurrentVoxelOffset);

                    NextVoxelOffset =
                      Boundary[(VoxelNum + i) % BoundaryLength];

                    dX2 =
                      img_boundary.GetX (NextVoxelOffset) -
                      img_boundary.GetX (CurrentVoxelOffset);
                    dY2 =
                      img_boundary.GetY (NextVoxelOffset) -
                      img_boundary.GetY (CurrentVoxelOffset);

                    norm1 = sqrt (double(dX1 * dX1 + dY1 * dY1));
                    norm2 = sqrt (double(dX2 * dX2 + dY2 * dY2));

                    CosAngle = (dX1 * dX2 + dY1 * dY2) / (norm1 * norm2);
                    if (CosAngle < -1)
                      CurrentAngle = M_PI;
                    else if (CosAngle > 1)
                      CurrentAngle = 0;
                    else
                      {
                        CurrentAngle = acos (CosAngle);

                        if ((dX1 * dY2 - dY1 * dX2) < 0)
                          CurrentAngle = 2 * M_PI - CurrentAngle;
                      }

                    if (CurrentAngle > MaxAngle)
                      {
                        MaxAngle = CurrentAngle;
                        Delta = i;
                      }
                  }
					 

                // Unmark all the boundary voxels, that would be covered 
					 // with the line constructing one part of the convex hull.
                for (j = VoxelNum; j < VoxelNum + Delta; j++)
                  {
                    img_boundary.SetVoxel (Boundary[j % BoundaryLength], 
														 BINARY(0));
                  }

                // Required line starts at "CurrentVoxel" and 
					 // ends at "LastVoxel".
                LastVoxelOffset = CurrentVoxelOffset;
                VoxelNum += Delta;
                CurrentVoxelOffset = Boundary[VoxelNum % BoundaryLength];

                // Rasterize the line and save the voxels to the list.
                line.clear ();
                EndPoint = 
								Vector3d<size_t>(img_boundary.GetX(LastVoxelOffset),
													  img_boundary.GetY(LastVoxelOffset),
													  img_boundary.GetZ(LastVoxelOffset));
                StartPoint =
                  Vector3d<size_t>(img_boundary.GetX(CurrentVoxelOffset),
											  img_boundary.GetY(CurrentVoxelOffset),
											  img_boundary.GetZ(CurrentVoxelOffset));

                DrawLineBres (line, StartPoint, EndPoint);

                // Before drawing new line we have to find out if there is
                // possibility of joining two disjoint components.
                line.pop_front ();
                line.pop_back ();
                intersection = false;
                for (p = line.begin (); p != line.end () && !intersection;
                     p++)
                  {
                    if (img_boundary.GetVoxel (p->x, p->y, p->z) == BINARY(1))
                      intersection = true;
                  }

                // If the intersection is nonempty we won't draw the line
                if (!intersection)
                  {
                    while (!line.empty ())
                      {
                        LinePoint = line.front ();
                        line.pop_front ();

                        img.SetVoxel (LinePoint.x, 
												  LinePoint.y, 
												  LinePoint.z,
                                      BINARY(1));
                      }
                  }
              }

          } while (FoundFirst); // loop through all the objects

      } // loop through the slices

    BinFillHoles (img, 0, 0, 0, nb2D_conv);

#ifdef I3D_DEBUG
    std::cout << "... done." << std::endl;
#endif

  }


}
