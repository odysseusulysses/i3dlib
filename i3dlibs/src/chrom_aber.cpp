/*
* i3dlib - image manipulation library
*
* Copyright (C) 2000-2006   Centre for Biomedical Image Analysis (CBIA)
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Library General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Library General Public License for more details.
*
* You should have received a copy of the GNU Library General Public
* License along with this library; if not, write to the Free
* Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

/**
* @file chrom_aber.cpp
* @brief %Chromatic aberration correction
*
* @author Marek Ka��k (xkasik1@fi.muni.cz)
* @author Martin Ma�ka (xmaska@fi.muni.cz)
* @date 2008
*/

#include "chrom_aber.h"

	/////////////////////////////////////////////////////////////////
	//                                                             //
	//                      Local Functions                        //
	//                                                             //
	/////////////////////////////////////////////////////////////////

	template <class T> double AGetVoxel2D(const i3d::Image3d<T> &input, double x, double y)
	{
		T neighbourhood[2][2];
		neighbourhood[0][0]= input.GetVoxel(static_cast<size_t>(floor(x)), static_cast<size_t>(floor(y)), 0);
		neighbourhood[1][0]= input.GetVoxel(static_cast<size_t>(ceil(x)), static_cast<size_t>(floor(y)), 0);
		neighbourhood[0][1]= input.GetVoxel(static_cast<size_t>(floor(x)), static_cast<size_t>(ceil(y)), 0);
		neighbourhood[1][1]= input.GetVoxel(static_cast<size_t>(ceil(x)), static_cast<size_t>(ceil(y)), 0);
		
		double dx = x - floor(x);
		double dy = y - floor(y);

		double interpolationX[2];
		interpolationX[0] = neighbourhood[0][0] + dx * (neighbourhood[1][0] - neighbourhood[0][0]);
		interpolationX[1] = neighbourhood[0][1] + dx * (neighbourhood[1][1] - neighbourhood[0][1]);

		return (interpolationX[0] + dy * (interpolationX[1] - interpolationX[0]));
	}

/****************************************************************************/	

	template <class T> double AGetVoxel3D(const i3d::Image3d<T> &input, double x, double y, double z)
	{
		T neighbourhood[2][2][2];
		neighbourhood[0][0][0] = input.GetVoxel(static_cast<size_t>(floor(x)), static_cast<size_t>(floor(y)), static_cast<size_t>(floor(z)));
		neighbourhood[1][0][0] = input.GetVoxel(static_cast<size_t>(ceil(x)), static_cast<size_t>(floor(y)), static_cast<size_t>(floor(z)));
		neighbourhood[0][1][0] = input.GetVoxel(static_cast<size_t>(floor(x)), static_cast<size_t>(ceil(y)), static_cast<size_t>(floor(z)));
		neighbourhood[1][1][0] = input.GetVoxel(static_cast<size_t>(ceil(x)), static_cast<size_t>(ceil(y)), static_cast<size_t>(floor(z)));
		neighbourhood[0][0][1] = input.GetVoxel(static_cast<size_t>(floor(x)), static_cast<size_t>(floor(y)), static_cast<size_t>(ceil(z)));
		neighbourhood[1][0][1] = input.GetVoxel(static_cast<size_t>(ceil(x)), static_cast<size_t>(floor(y)), static_cast<size_t>(ceil(z)));
		neighbourhood[0][1][1] = input.GetVoxel(static_cast<size_t>(floor(x)), static_cast<size_t>(ceil(y)), static_cast<size_t>(ceil(z)));
		neighbourhood[1][1][1] = input.GetVoxel(static_cast<size_t>(ceil(x)), static_cast<size_t>(ceil(y)), static_cast<size_t>(ceil(z)));

		double dx = x - floor(x);
		double dy = y - floor(y);
		double dz = z - floor(z);

		double interpolationX[2][2];
		interpolationX[0][0] = neighbourhood[0][0][0] + dx * (neighbourhood[1][0][0] - neighbourhood[0][0][0]);
		interpolationX[1][0] = neighbourhood[0][1][0] + dx * (neighbourhood[1][1][0] - neighbourhood[0][1][0]);
		interpolationX[0][1] = neighbourhood[0][0][1] + dx * (neighbourhood[1][0][1] - neighbourhood[0][0][1]);
		interpolationX[1][1] = neighbourhood[0][1][1] + dx * (neighbourhood[1][1][1] - neighbourhood[0][1][1]);
		
		double interpolationY[2];
		interpolationY[0] = interpolationX[0][0] + dy * (interpolationX[1][0] - interpolationX[0][0]);
		interpolationY[1] = interpolationX[0][1] + dy * (interpolationX[1][1] - interpolationX[0][1]);

		return (interpolationY[0] + dz * (interpolationY[1] - interpolationY[0]));
	}

/****************************************************************************/	

	template <class T> static void CorrectImage2D(const i3d::Image3d<T> &input, i3d::Image3d<T> &output, 
												  const i3d::ChromAberParams &params)
	{
		i3d::Vector3d<size_t> size = input.GetSize();

		if (params.IsIdentity2D())
		{
			// just copy the input image to the output one
			output = input;
		}
		else
		{
			double value;
			i3d::Vector3d<double> corrected, correction, coord, c;
			i3d::Vector3d<double> offset = input.GetOffset();
			i3d::Vector3d<double> min(0, 0, 0), max(size.x - 1, size.y - 1, 0);

			// prepare the output image
			output.CopyMetaData(input);

			for (size_t y = 0; y < size.y; y++)
			{
				for (size_t x = 0; x < size.x; x++)
				{
					coord = i3d::PixelsToMicrons(i3d::Vector3d<double>(x, y, 0), input.GetResolution());
					c = coord + offset;

					correction.x = params.x_plane[0] * c.x + params.x_plane[1] * c.y + params.x_plane[2];
					correction.y = params.y_plane[0] * c.x + params.y_plane[1] * c.y + params.y_plane[2];
					correction.z = 0;

					corrected = MicronsToPixels(coord - correction, input.GetResolution());

					if (corrected.x >= 0 && corrected.x <= (size.x - 1) && 
						corrected.y >= 0 && corrected.y <= (size.y - 1))
					{
						value = AGetVoxel2D(input, corrected.x, corrected.y);
					}
					else
					{
						value = 0;
					}

					if (value > std::numeric_limits<T>::max()) 
					{ 
						output.SetVoxel(x, y, 0, std::numeric_limits<T>::max());
					}
					else
					{ 
						if (value < std::numeric_limits<T>::min())
						{
							output.SetVoxel(x, y, 0, std::numeric_limits<T>::min());
						}
						else
						{
							if (std::numeric_limits<T>::is_integer)
							{
								output.SetVoxel(x, y, 0, static_cast<T>(round(value)));
							}
							else
							{
								output.SetVoxel(x, y, 0, static_cast<T>(value));
							}
						}
					}
				}
			}
		}
	}

/****************************************************************************/	

	static void GetCuttingPosition2D(const i3d::Offset &img_offset, const i3d::Resolution &img_resolution, 
		                             const i3d::Vector3d<size_t> &img_size, const i3d::ChromAberParams &params,
									 i3d::Vector3d<size_t> &minimum, i3d::Vector3d<size_t> &maximum)
	{
		if (params.IsIdentity2D())
		{
			// the output image does not have to be cut
			minimum = i3d::Vector3d<size_t>(0, 0, 0);
			maximum = i3d::Vector3d<size_t>(img_size.x - 1, img_size.y - 1, 0);
		}
		else
		{
			i3d::Vector3d<double> corrected, correction, coord, c;
			i3d::Vector3d<double> offset = img_offset;
			i3d::Vector3d<double> min(0, 0, 0), max(img_size.x - 1, img_size.y - 1, 0);

			// find maximal correction in the y axis for the first row
			for (size_t x = 0; x < img_size.x; x++)
			{
				coord = i3d::PixelsToMicrons(i3d::Vector3d<double>(x, 0, 0), img_resolution);
				c = coord + offset;

				correction.x = params.x_plane[0] * c.x + params.x_plane[1] * c.y + params.x_plane[2];
				correction.y = params.y_plane[0] * c.x + params.y_plane[1] * c.y + params.y_plane[2];
				correction.z = 0;

				corrected = MicronsToPixels(coord + correction, img_resolution);

				if (corrected.x >= 0 && corrected.x <= (img_size.x - 1) && 
					corrected.y >= 0 && corrected.y <= (img_size.y - 1))
				{
					if (corrected.y > min.y)
					{
						min.y = corrected.y;
					}
				}
			}
			
			// find maximal correction in the y axis for the last row
			for (size_t x = 0; x < img_size.x; x++)
			{
				coord = i3d::PixelsToMicrons(i3d::Vector3d<double>(x, img_size.y - 1, 0), img_resolution);
				c = coord + offset;

				correction.x = params.x_plane[0] * c.x + params.x_plane[1] * c.y + params.x_plane[2];
				correction.y = params.y_plane[0] * c.x + params.y_plane[1] * c.y + params.y_plane[2];
				correction.z = 0;

				corrected = MicronsToPixels(coord + correction, img_resolution);

				if (corrected.x >= 0 && corrected.x <= (img_size.x - 1) && 
					corrected.y >= 0 && corrected.y <= (img_size.y - 1))
				{
					if (corrected.y < max.y)
					{
						max.y = corrected.y;
					}
				}
			}

			// find maximal correction in the x axis for the first column
			for (size_t y = 0; y < img_size.y; y++)
			{
				coord = i3d::PixelsToMicrons(i3d::Vector3d<double>(0, y, 0), img_resolution);
				c = coord + offset;

				correction.x = params.x_plane[0] * c.x + params.x_plane[1] * c.y + params.x_plane[2];
				correction.y = params.y_plane[0] * c.x + params.y_plane[1] * c.y + params.y_plane[2];
				correction.z = 0;

				corrected = MicronsToPixels(coord + correction, img_resolution);

				if (corrected.x >= 0 && corrected.x <= (img_size.x - 1) && 
					corrected.y >= 0 && corrected.y <= (img_size.y - 1))
				{
					if (corrected.x > min.x)
					{
						min.x = corrected.x;
					}
				}
			}

			// find maximal correction in the x axis for the last column
			for (size_t y = 0; y < img_size.y; y++)
			{
				coord = i3d::PixelsToMicrons(i3d::Vector3d<double>(img_size.x - 1, y, 0), img_resolution);
				c = coord + offset;

				correction.x = params.x_plane[0] * c.x + params.x_plane[1] * c.y + params.x_plane[2];
				correction.y = params.y_plane[0] * c.x + params.y_plane[1] * c.y + params.y_plane[2];
				correction.z = 0;

				corrected = MicronsToPixels(coord + correction, img_resolution);

				if (corrected.x >= 0 && corrected.x <= (img_size.x - 1) && 
					corrected.y >= 0 && corrected.y <= (img_size.y - 1))
				{
					if (corrected.x < max.x)
					{
						max.x = corrected.x;
					}
				}
			}

			minimum.x = static_cast<size_t>(ceil(min.x));
			minimum.y = static_cast<size_t>(ceil(min.y));
		    minimum.z = 0;
			maximum.x = static_cast<size_t>(floor(max.x));
			maximum.y = static_cast<size_t>(floor(max.y));
			maximum.z = img_size.z - 1;
		}
	}

/****************************************************************************/	

	template <class T> static void CorrectImage3D(const i3d::Image3d<T> &input, i3d::Image3d<T> &output, 
												  const i3d::ChromAberParams &params)
	{
		i3d::Vector3d<size_t> size = input.GetSize();

		if (params.IsIdentity3D())
		{
			// just copy the input image to the output one
			output = input;
		}
		else
		{
			double value;
			i3d::Vector3d<double> corrected, correction, coord, c;
			i3d::Vector3d<double> offset = input.GetOffset();
			i3d::Vector3d<double> min(0, 0, 0), max(size.x - 1, size.y - 1, size.z - 1);

			// prepare the output image
			output.CopyMetaData(input);

			for (size_t z = 0; z < size.z; z++)
			{
				for (size_t y = 0; y < size.y; y++)
				{
					for (size_t x = 0; x < size.x; x++)
					{
						coord = i3d::PixelsToMicrons(i3d::Vector3d<double>(x, y, z), input.GetResolution());
						c = coord + offset;

						correction.x = params.x_plane[0] * c.x + params.x_plane[1] * c.y + params.x_plane[2];
						correction.y = params.y_plane[0] * c.x + params.y_plane[1] * c.y + params.y_plane[2];
						correction.z = params.z_surf[0] * c.x * c.x + params.z_surf[1] * c.y * c.y +
							           params.z_surf[2] * c.x * c.y + params.z_surf[3] * c.x + params.z_surf[4] * c.y +
									   params.z_surf[5];

						corrected = MicronsToPixels(coord - correction, input.GetResolution());

						if (corrected.x >= 0 && corrected.x <= (size.x - 1) && 
							corrected.y >= 0 && corrected.y <= (size.y - 1) &&
							corrected.z >= 0 && corrected.z <= (size.z - 1))
						{
							value = AGetVoxel3D(input, corrected.x, corrected.y, corrected.z);
						}
						else
						{
							value = 0;
						}

						if (value > std::numeric_limits<T>::max()) 
						{ 
							output.SetVoxel(x, y, z, std::numeric_limits<T>::max());
						}
						else
						{ 
							if (value < std::numeric_limits<T>::min())
							{
								output.SetVoxel(x, y, z, std::numeric_limits<T>::min());
							}
							else
							{
								if (std::numeric_limits<T>::is_integer)
								{
									output.SetVoxel(x, y, z, static_cast<T>(round(value)));
								}
								else
								{
									output.SetVoxel(x, y, z, static_cast<T>(value));
								}
							}
						}
					}
				}
			}
		}
	}
	
/****************************************************************************/

	static void GetCuttingPosition3D(const i3d::Offset &img_offset, const i3d::Resolution &img_resolution, 
		                             const i3d::Vector3d<size_t> &img_size, const i3d::ChromAberParams &params,
									 i3d::Vector3d<size_t> &minimum, i3d::Vector3d<size_t> &maximum)
	{
		if (params.IsIdentity3D())
		{
			// the output image does not have to be cut
			minimum = i3d::Vector3d<size_t>(0, 0, 0);
			maximum = i3d::Vector3d<size_t>(img_size.x - 1, img_size.y - 1, img_size.z - 1);
		}
		else
		{
			i3d::Vector3d<double> corrected, correction, coord, c;
			i3d::Vector3d<double> offset = img_offset;
			i3d::Vector3d<double> min(0, 0, 0), max(img_size.x - 1, img_size.y - 1, img_size.z - 1);

			// find maximal correction in the z axis for the first xy plane
			for (size_t y = 0; y < img_size.y; y++)
			{
				for (size_t x = 0; x < img_size.x; x++)
				{
					coord = i3d::PixelsToMicrons(i3d::Vector3d<double>(x, y, 0), img_resolution);
					c = coord + offset;

					correction.x = params.x_plane[0] * c.x + params.x_plane[1] * c.y + params.x_plane[2];
					correction.y = params.y_plane[0] * c.x + params.y_plane[1] * c.y + params.y_plane[2];
					correction.z = params.z_surf[0] * c.x * c.x + params.z_surf[1] * c.y * c.y +
						           params.z_surf[2] * c.x * c.y + params.z_surf[3] * c.x + params.z_surf[4] * c.y +
								   params.z_surf[5];

					corrected = MicronsToPixels(coord + correction, img_resolution);

					if (corrected.x >= 0 && corrected.x <= (img_size.x - 1) && 
						corrected.y >= 0 && corrected.y <= (img_size.y - 1) &&
						corrected.z >= 0 && corrected.z <= (img_size.z - 1))
					{
						if (corrected.z > min.z)
						{
							min.z = corrected.z;
						}
					}
				}
			}

			// find maximal correction in the z axis for the last xy plane
			for (size_t y = 0; y < img_size.y; y++)
			{
				for (size_t x = 0; x < img_size.x; x++)
				{
					coord = i3d::PixelsToMicrons(i3d::Vector3d<double>(x, y, img_size.z - 1), img_resolution);
					c = coord + offset;

					correction.x = params.x_plane[0] * c.x + params.x_plane[1] * c.y + params.x_plane[2];
					correction.y = params.y_plane[0] * c.x + params.y_plane[1] * c.y + params.y_plane[2];
					correction.z = params.z_surf[0] * c.x * c.x + params.z_surf[1] * c.y * c.y +
						           params.z_surf[2] * c.x * c.y + params.z_surf[3] * c.x + params.z_surf[4] * c.y +
								   params.z_surf[5];

					corrected = MicronsToPixels(coord + correction, img_resolution);

					if (corrected.x >= 0 && corrected.x <= (img_size.x - 1) && 
						corrected.y >= 0 && corrected.y <= (img_size.y - 1) &&
						corrected.z >= 0 && corrected.z <= (img_size.z - 1))
					{
						if (corrected.z < max.z)
						{
							max.z = corrected.z;
						}
					}
				}
			}

			// find maximal correction in the y axis for the first xz plane
			for (size_t z = 0; z < img_size.z; z++)
			{
				for (size_t x = 0; x < img_size.x; x++)
				{
					coord = i3d::PixelsToMicrons(i3d::Vector3d<double>(x, 0, z), img_resolution);
					c = coord + offset;

					correction.x = params.x_plane[0] * c.x + params.x_plane[1] * c.y + params.x_plane[2];
					correction.y = params.y_plane[0] * c.x + params.y_plane[1] * c.y + params.y_plane[2];
					correction.z = params.z_surf[0] * c.x * c.x + params.z_surf[1] * c.y * c.y +
						           params.z_surf[2] * c.x * c.y + params.z_surf[3] * c.x + params.z_surf[4] * c.y +
								   params.z_surf[5];

					corrected = MicronsToPixels(coord + correction, img_resolution);

					if (corrected.x >= 0 && corrected.x <= (img_size.x - 1) && 
						corrected.y >= 0 && corrected.y <= (img_size.y - 1) &&
						corrected.z >= 0 && corrected.z <= (img_size.z - 1))
					{
						if (corrected.y > min.y)
						{
							min.y = corrected.y;
						}
					}
				}
			}

			// find maximal correction in the y axis for the last xz plane
			for (size_t z = 0; z < img_size.z; z++)
			{
				for (size_t x = 0; x < img_size.x; x++)
				{
					coord = i3d::PixelsToMicrons(i3d::Vector3d<double>(x, img_size.y - 1, z), img_resolution);
					c = coord + offset;

					correction.x = params.x_plane[0] * c.x + params.x_plane[1] * c.y + params.x_plane[2];
					correction.y = params.y_plane[0] * c.x + params.y_plane[1] * c.y + params.y_plane[2];
					correction.z = params.z_surf[0] * c.x * c.x + params.z_surf[1] * c.y * c.y +
						           params.z_surf[2] * c.x * c.y + params.z_surf[3] * c.x + params.z_surf[4] * c.y +
								   params.z_surf[5];

					corrected = MicronsToPixels(coord + correction, img_resolution);

					if (corrected.x >= 0 && corrected.x <= (img_size.x - 1) && 
						corrected.y >= 0 && corrected.y <= (img_size.y - 1) &&
						corrected.z >= 0 && corrected.z <= (img_size.z - 1))
					{
						if (corrected.y < max.y)
						{
							max.y = corrected.y;
						}
					}
				}
			}

			// find maximal correction in the x axis for the first yz plane
			for (size_t z = 0; z < img_size.z; z++)
			{
				for (size_t y = 0; y < img_size.y; y++)
				{
					coord = i3d::PixelsToMicrons(i3d::Vector3d<double>(0, y, z), img_resolution);
					c = coord + offset;

					correction.x = params.x_plane[0] * c.x + params.x_plane[1] * c.y + params.x_plane[2];
					correction.y = params.y_plane[0] * c.x + params.y_plane[1] * c.y + params.y_plane[2];
					correction.z = params.z_surf[0] * c.x * c.x + params.z_surf[1] * c.y * c.y +
						           params.z_surf[2] * c.x * c.y + params.z_surf[3] * c.x + params.z_surf[4] * c.y +
								   params.z_surf[5];

					corrected = MicronsToPixels(coord + correction, img_resolution);

					if (corrected.x >= 0 && corrected.x <= (img_size.x - 1) && 
						corrected.y >= 0 && corrected.y <= (img_size.y - 1) &&
						corrected.z >= 0 && corrected.z <= (img_size.z - 1))
					{
						if (corrected.x > min.x)
						{
							min.x = corrected.x;
						}
					}
				}
			}

			// find maximal correction in the x axis for the last yz plane
			for (size_t z = 0; z < img_size.z; z++)
			{
				for (size_t y = 0; y < img_size.y; y++)
				{
					coord = i3d::PixelsToMicrons(i3d::Vector3d<double>(img_size.x - 1, y, z), img_resolution);
					c = coord + offset;

					correction.x = params.x_plane[0] * c.x + params.x_plane[1] * c.y + params.x_plane[2];
					correction.y = params.y_plane[0] * c.x + params.y_plane[1] * c.y + params.y_plane[2];
					correction.z = params.z_surf[0] * c.x * c.x + params.z_surf[1] * c.y * c.y +
						           params.z_surf[2] * c.x * c.y + params.z_surf[3] * c.x + params.z_surf[4] * c.y +
								   params.z_surf[5];

					corrected = MicronsToPixels(coord + correction, img_resolution);

					if (corrected.x >= 0 && corrected.x <= (img_size.x - 1) && 
						corrected.y >= 0 && corrected.y <= (img_size.y - 1) &&
						corrected.z >= 0 && corrected.z <= (img_size.z - 1))
					{
						if (corrected.x < max.x)
						{
							max.x = corrected.x;
						}
					}
				}
			}
			
			minimum.x = static_cast<size_t>(ceil(min.x));
			minimum.y = static_cast<size_t>(ceil(min.y));
			minimum.z = static_cast<size_t>(ceil(min.z));
			maximum.x = static_cast<size_t>(floor(max.x));
			maximum.y = static_cast<size_t>(floor(max.y));
			maximum.z = static_cast<size_t>(floor(max.z));
		}
	}

/****************************************************************************/	

namespace i3d
{
	/////////////////////////////////////////////////////////////////
	//                                                             //
	//                   Struct ChromAberParams                    //
	//                                                             //
	/////////////////////////////////////////////////////////////////

	ChromAberParams::ChromAberParams()
	{
		SetIdentity();
	}

/****************************************************************************/

	ChromAberParams::~ChromAberParams()
	{
	}

/****************************************************************************/
	void ChromAberParams::SetIdentity()
	{
		for (size_t i = 0; i < 3; i++)
		{
			x_plane[i] = 0.0;
			y_plane[i] = 0.0;
		}

		for (size_t i = 0; i < 6; i++)
		{
			z_surf[i] = 0.0;
		}
	}

/****************************************************************************/

	bool ChromAberParams::IsIdentity2D() const 
	{ 
		return (x_plane[0] == 0.0 && x_plane[1] == 0.0 && x_plane[2] == 0.0 &&
			    y_plane[0] == 0.0 && y_plane[1] == 0.0 && y_plane[2] == 0.0);
	}

/****************************************************************************/

	bool ChromAberParams::IsIdentity3D() const 
	{
		return (IsIdentity2D() && z_surf[0] == 0.0 && z_surf[1] == 0.0 && z_surf[2] == 0.0 
			                   && z_surf[3] == 0.0 && z_surf[4] == 0.0 && z_surf[5] == 0.0);
	}

/****************************************************************************/

	/////////////////////////////////////////////////////////////////
	//                                                             //
	//              Function CorrectChromaticAberration            //
	//                                                             //
	/////////////////////////////////////////////////////////////////

	template <class T> void CorrectChromaticAberration(const i3d::Image3d<T> &input, i3d::Image3d<T> &output,
                                                 	   const ChromAberParams &params, bool cut)
	{
		if (input.IsEmpty())
		{
			throw i3d::InternalException("Input image is empty!");
		}
		else
		{
			i3d::Vector3d<size_t> minimum, maximum;

			if (input.GetSizeZ() == 1)
			{
				CorrectImage2D(input, output, params);

				if (cut)
				{					
					GetCuttingPosition2D(input.GetOffset(), input.GetResolution(), input.GetSize(), params, minimum, maximum);
				}
			}
			else
			{
				CorrectImage3D(input, output, params);

				if (cut)
				{
					GetCuttingPosition3D(input.GetOffset(), input.GetResolution(), input.GetSize(), params, minimum, maximum);
				}
			}

			if (cut)
			{
				i3d::Image3d<T> tmp;
				tmp.SetResolution(input.GetResolution());
				tmp.SetOffset(input.GetOffset() + i3d::PixelsToMicrons(minimum, input.GetResolution()));
				tmp.MakeRoom(maximum - minimum + i3d::Vector3d<size_t>(1, 1, 1));

				for(size_t z = minimum.z; z <= maximum.z; z++)
				{
					for(size_t y = minimum.y; y <= maximum.y; y++)
					{
						for(size_t x = minimum.x; x <= maximum.x; x++)
						{
							tmp.SetVoxel(x - minimum.x, y - minimum.y, z - minimum.z, output.GetVoxel(x, y, z));
						}
					}
				}

				output = tmp;
			}
		}
	}

/****************************************************************************/
	
	/////////////////////////////////////////////////////////////////
	//                                                             //
	//                    Explicit Instantiations                  //
	//                                                             //
	/////////////////////////////////////////////////////////////////

	template I3D_DLLEXPORT void CorrectChromaticAberration(
		const i3d::Image3d<i3d::GRAY8> &input, 
		i3d::Image3d<i3d::GRAY8> &output,
		const ChromAberParams &params, 
		bool cut);

	template I3D_DLLEXPORT void CorrectChromaticAberration(
		const i3d::Image3d<i3d::GRAY16> &input, 
		i3d::Image3d<i3d::GRAY16> &output,
		const ChromAberParams &params, 
		bool cut);
	
	template I3D_DLLEXPORT void CorrectChromaticAberration(
		const i3d::Image3d<float> &input, 
		i3d::Image3d<float> &output,
		const ChromAberParams &params, 
		bool cut);

/****************************************************************************/

	/////////////////////////////////////////////////////////////////
	//                                                             //
	//              Function CorrectChromaticAberration            //
	//                                                             //
	/////////////////////////////////////////////////////////////////

	template <class T> void CorrectChromaticAberration(
		const i3d::Image3d<i3d::RGB_generic<T> > &input, 
		i3d::Image3d<i3d::RGB_generic<T> > &output, 
		const ChromAberParams &red_params,  
		const ChromAberParams &green_params, 
		const ChromAberParams &blue_params, 
		bool cut)
	{
		if (input.IsEmpty())
		{
			throw i3d::InternalException("Input image is empty!");
		}
		else
		{
			i3d::Image3d<T> red_in, red_out, green_in, green_out, blue_in, blue_out;
			i3d::Vector3d<size_t> red_min, red_max, green_min, green_max, blue_min, blue_max;

			i3d::RGBtoGray(input, red_in, green_in, blue_in);

			if (input.GetSizeZ() == 1)
			{
				CorrectImage2D(red_in, red_out, red_params);
				CorrectImage2D(green_in, green_out, green_params);
				CorrectImage2D(blue_in, blue_out, blue_params);
			}
			else
			{
				CorrectImage3D(red_in, red_out, red_params);
				CorrectImage3D(green_in, green_out, green_params);
				CorrectImage3D(blue_in, blue_out, blue_params);
			}

			if (cut)
			{
				i3d::Vector3d<size_t> minimum, maximum;
				GetCuttingPosition(input.GetOffset(), input.GetResolution(), input.GetSize(), red_params, green_params, 
					               blue_params, minimum, maximum);
				
				output.SetResolution(input.GetResolution());
				output.SetOffset(input.GetOffset() + i3d::PixelsToMicrons(minimum, input.GetResolution()));
				output.MakeRoom(maximum - minimum + i3d::Vector3d<size_t>(1, 1, 1));

				i3d::RGB_generic<T> value;
				for(size_t z = minimum.z; z <= maximum.z; z++)
				{
					for(size_t y = minimum.y; y <= maximum.y; y++)
					{
						for(size_t x = minimum.x; x <= maximum.x; x++)
						{
							value.red = red_out.GetVoxel(x, y, z);
							value.green = green_out.GetVoxel(x, y, z);
							value.blue = blue_out.GetVoxel(x, y, z);

							output.SetVoxel(x - minimum.x, y - minimum.y, z - minimum.z, value);
						}
					}
				}
			}
			else
			{
				i3d::GrayToRGB(red_out, green_out, blue_out, output);
			}
		}
	}

/****************************************************************************/
	
	/////////////////////////////////////////////////////////////////
	//                                                             //
	//                    Explicit Instantiations                  //
	//                                                             //
	/////////////////////////////////////////////////////////////////

	template I3D_DLLEXPORT void CorrectChromaticAberration(
		const i3d::Image3d<i3d::RGB_generic<i3d::GRAY8> > &input, 
		i3d::Image3d<i3d::RGB_generic<i3d::GRAY8> > &output,
		const ChromAberParams &red_params, 
		const ChromAberParams &green_params, 
		const ChromAberParams &blue_params, 
		bool cut);

	template I3D_DLLEXPORT void CorrectChromaticAberration(
		const i3d::Image3d<i3d::RGB_generic<i3d::GRAY16> > &input, 
		i3d::Image3d<i3d::RGB_generic<i3d::GRAY16> > &output,
		const ChromAberParams &red_params, 
		const ChromAberParams &green_params,
		const ChromAberParams &blue_params, 
		bool cut);
	
/****************************************************************************/

	/////////////////////////////////////////////////////////////////
	//                                                             //
	//                 Function GetCuttingPosition                 //
	//                                                             //
	/////////////////////////////////////////////////////////////////

	void GetCuttingPosition(
		const i3d::Offset &img_offset, 
		const i3d::Resolution &img_resolution,
		const i3d::Vector3d<size_t> &img_size, 
		const i3d::ChromAberParams &params,     
		i3d::Vector3d<size_t> &minimum, 
		i3d::Vector3d<size_t> &maximum)
	{
		if (img_size.x * img_size.y * img_size.z == 0)
		{
			throw i3d::InternalException("Input image is empty!");
		}
		else
		{
			if (img_size.z == 1)
			{
				GetCuttingPosition2D(img_offset, img_resolution, img_size, params, minimum, maximum);
			}
			else
			{
				GetCuttingPosition3D(img_offset, img_resolution, img_size, params, minimum, maximum);
			}
		}
	}

/****************************************************************************/

	/////////////////////////////////////////////////////////////////
	//                                                             //
	//                 Function GetCuttingPosition                 //
	//                                                             //
	/////////////////////////////////////////////////////////////////

	void GetCuttingPosition(
		const i3d::Offset &img_offset, 
		const i3d::Resolution &img_resolution,
		const i3d::Vector3d<size_t> &img_size, 
		const ChromAberParams &red_params,  
		const ChromAberParams &green_params, 
		const ChromAberParams &blue_params, 
		i3d::Vector3d<size_t> &minimum, 
		i3d::Vector3d<size_t> &maximum)
	{
		if (img_size.x * img_size.y * img_size.z == 0)
		{
			throw i3d::InternalException("Input image is empty!");
		}
		else
		{
			i3d::Vector3d<size_t> red_min, red_max, green_min, green_max, blue_min, blue_max;

			if (img_size.z == 1)
			{
				GetCuttingPosition2D(img_offset, img_resolution, img_size, red_params, red_min, red_max);
				GetCuttingPosition2D(img_offset, img_resolution, img_size, green_params, green_min, green_max);
				GetCuttingPosition2D(img_offset, img_resolution, img_size, blue_params, blue_min, blue_max);
			}
			else
			{
				GetCuttingPosition3D(img_offset, img_resolution, img_size, red_params, red_min, red_max);
				GetCuttingPosition3D(img_offset, img_resolution, img_size, green_params, green_min, green_max);
				GetCuttingPosition3D(img_offset, img_resolution, img_size, blue_params, blue_min, blue_max);
			}

			minimum.x = std::max(std::max(red_min.x, green_min.x), blue_min.x);
			minimum.y = std::max(std::max(red_min.y, green_min.y), blue_min.y);
			minimum.z = std::max(std::max(red_min.z, green_min.z), blue_min.z);

			maximum.x = std::min(std::min(red_max.x, green_max.x), blue_max.x);
			maximum.y = std::min(std::min(red_max.y, green_max.y), blue_max.y);
			maximum.z = std::min(std::min(red_max.z, green_max.z), blue_max.z);
		}
	}

/****************************************************************************/

} // namespace i3d

