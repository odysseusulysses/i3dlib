/*
 * i3dlib - image manipulation library
 *
 * Copyright (C) 2000-2006   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 * FILE: edge.cc
 *
 * 2D/3D edge detection operators
 *
 * Marek Kasik (xkasik1@fi.muni.cz) 2003
 */

#define LEVEL 0

//#define EXPORT_GRADIENT_DIRECTION

/*

	LEVEL 0 - bez kontrolnich vypisu
	LEVEL 1 - vypise se vstoupeni/vystoupeni do/z funkce
	LEVEL 2 - vypisi se i jednorazove vypocty
	LEVEL 3 - vypisi se i informace o prubehu vypoctu po vrstvach
	LEVEL 4 - vypise se vypocet pro kazdy voxel 
	          (velice velice pomale) - nepouzito
	LEVEL 5 - nepouzito (vzhledem ke kratkemu zivotu lidi)

*/

#ifdef __GNUG__
#pragma implementation
#endif

#include <typeinfo>
#include <algorithm>
#include <stack>
#include <list>
#include <iostream>
#include <iomanip>
#include <cstdlib>

#include "filters.h"
#include "edge.h"

#ifdef WITH_FFTW
	#include <fftw3.h>
#endif //fftw

#define Minimalni_velikost_gradientu 0.001

namespace i3d{

// Auxiliary functions
template <class T> static inline T Sqr (T x) { return x * x; }
template <class T> static inline T Abs (T x) { return x < 0 ? -x : x; }
template <class T> static inline T Max (T x, T y, T z)
{
	if (x < y)
	{
		return y > z ? y : z;
	}
	return x > z ? x : z;
}

/* Vrati 3d vektor, ve kterem je rozliseni vstupniho obrazu zmenseno tak, aby
   nejvyssi rozliseni na mikrometr melo hodnotu 1.0, ostatni jsou prislusne
	 naskalovany.
   Priklad: pro obrazek s rozlisenim (4 , 10 , 5) vrati vektor (0.4 , 1.0 , 0.5)
*/
template <class VOXEL> 
static Vector3d<float> get_relative_resolution(const Image3d<VOXEL> &in)
{
	float						max;
	Vector3d<float> resolution,relative_resolution;
	Resolution			res;

	res=in.GetResolution();
	resolution=res.GetRes();

	if(resolution.x>resolution.y){
		if(resolution.x>resolution.z)max=resolution.x;
		else max=resolution.z;
	}
	else{
		if(resolution.y>resolution.z)max=resolution.y;
		else max=resolution.z;
	}
	relative_resolution.x=resolution.x/max;
	relative_resolution.y=resolution.y/max;
	relative_resolution.z=resolution.z/max;
		
	return relative_resolution;
}

/* Jednoduchy predikat, ktery vrati true, pokud je vstupni obraz anisotropicky
   a false pokud neni anisotropicky.
*/
template <class VOXEL> 
static bool is_anisotropic(const Image3d<VOXEL> &in)
{
	Vector3d<float> resolution = in.GetResolution().GetRes();

	return (resolution.x!=resolution.y ||
            resolution.x!=resolution.z ||
			resolution.y!=resolution.z);
}

/* BEGIN Compass3D */
/* Toto pole adresuje hodnoty zakladnich konvolucnich jader 3D hranovych
   operatoru pro 42 ruznych rotaci.
*/
static int  Maska[42][3][3][3]={	{{{13,12,13},{12,11,12},{13,12,13}},{{23,22,23},{22,00,22},{23,22,23}},{{33,32,33},{32,31,32},{33,32,33}}},
				{{{23,13,12},{22,12,11},{23,13,12}},{{33,22,13},{32,00,12},{33,22,13}},{{32,33,23},{31,32,22},{32,33,23}}},
				{{{13,23,33},{12,22,32},{13,23,33}},{{12,22,32},{11,00,31},{12,22,32}},{{13,23,33},{12,22,32},{13,23,33}}},
				{{{12,13,23},{11,12,22},{12,13,23}},{{13,22,33},{12,00,32},{13,22,33}},{{23,33,32},{22,32,31},{23,33,32}}},
				{{{33,23,13},{32,22,12},{33,23,13}},{{32,22,12},{31,00,11},{32,22,12}},{{33,23,13},{32,22,12},{33,23,13}}},
				{{{32,33,23},{31,32,22},{32,33,23}},{{33,22,13},{32,00,12},{33,22,13}},{{23,13,12},{22,12,11},{23,13,12}}},
				{{{33,32,33},{32,31,32},{33,32,33}},{{23,22,23},{22,00,22},{23,22,23}},{{13,12,13},{12,11,12},{13,12,13}}},
				{{{12,13,23},{11,12,22},{12,13,23}},{{13,22,33},{12,00,32},{13,22,33}},{{23,33,32},{22,32,31},{23,33,32}}},
				{{{12,11,12},{13,12,13},{23,22,23}},{{13,12,13},{22,00,22},{33,32,33}},{{23,22,23},{33,32,33},{32,31,32}}},
				{{{13,12,13},{23,22,23},{33,32,33}},{{12,11,12},{22,00,22},{32,31,32}},{{13,12,13},{23,22,23},{33,32,33}}},
				{{{23,22,23},{33,32,33},{32,31,32}},{{13,12,13},{22,00,22},{33,32,33}},{{12,11,12},{13,12,13},{23,22,23}}},
				{{{32,31,32},{33,32,33},{23,22,23}},{{33,32,33},{22,00,22},{13,12,13}},{{23,22,23},{13,12,13},{12,11,12}}},
				{{{33,32,33},{23,22,23},{13,12,13}},{{32,31,32},{22,00,22},{12,11,12}},{{33,32,33},{23,22,23},{13,12,13}}},
				{{{23,22,23},{13,12,13},{12,11,12}},{{33,32,33},{22,00,22},{13,12,13}},{{32,31,32},{33,32,33},{23,22,23}}},
				{{{32,33,23},{33,22,13},{23,13,12}},{{31,32,22},{32,00,12},{22,12,11}},{{32,33,23},{33,22,13},{23,13,12}}},
				{{{23,13,12},{33,22,13},{32,33,23}},{{22,12,11},{32,00,12},{31,32,22}},{{23,13,12},{33,22,13},{32,33,23}}},
				{{{12,13,23},{13,22,33},{23,33,32}},{{11,12,22},{12,00,32},{22,32,31}},{{12,13,23},{13,22,33},{23,33,32}}},
				{{{23,33,32},{13,22,33},{12,13,23}},{{22,32,31},{12,00,32},{11,12,22}},{{23,33,32},{13,22,33},{12,13,23}}},


				{{{13,12,11},{22,12,12},{23,22,13}},{{23,13,12},{33,00,13},{32,33,23}},{{33,22,23},{32,32,22},{31,32,33}}},
				{{{11,12,13},{12,12,22},{13,22,23}},{{12,13,23},{13,00,33},{23,33,32}},{{23,22,33},{22,32,32},{33,32,31}}},
				{{{13,22,23},{12,12,22},{11,12,13}},{{23,33,32},{13,00,33},{12,13,23}},{{33,32,31},{22,32,32},{23,22,33}}},
				{{{23,22,13},{22,12,12},{13,12,11}},{{32,33,23},{33,00,13},{23,13,12}},{{31,32,33},{32,32,22},{33,22,23}}},
				{{{33,22,13},{32,32,22},{31,32,23}},{{23,13,12},{33,00,13},{32,33,23}},{{23,12,11},{22,12,12},{33,22,13}}},
				{{{31,32,23},{32,32,22},{33,22,13}},{{32,33,23},{33,00,13},{23,13,12}},{{33,22,13},{22,12,12},{23,12,11}}},
				{{{23,12,11},{22,12,12},{33,22,13}},{{23,13,12},{33,00,13},{32,33,23}},{{33,22,13},{32,32,22},{31,32,23}}},
				{{{33,22,13},{22,12,12},{23,12,11}},{{32,33,23},{33,00,13},{23,13,12}},{{31,32,23},{32,32,22},{33,22,13}}},
				{{{13,22,23},{22,32,32},{33,32,31}},{{12,13,23},{13,00,33},{23,33,32}},{{11,12,13},{12,12,22},{23,22,33}}},
				{{{23,22,13},{32,32,22},{31,32,33}},{{23,13,12},{33,00,13},{32,33,23}},{{13,12,11},{22,12,12},{33,22,23}}},
				{{{11,12,13},{12,12,22},{23,22,33}},{{12,13,23},{13,00,33},{23,33,32}},{{13,22,23},{22,32,32},{33,32,31}}},
				{{{13,12,11},{22,12,12},{33,22,23}},{{23,13,12},{33,00,13},{32,33,23}},{{23,22,13},{32,32,22},{31,32,33}}},


				{{{33,32,31},{22,32,32},{23,22,33}},{{23,33,32},{13,00,33},{12,13,23}},{{13,22,23},{12,12,22},{11,12,13}}},
				{{{31,32,33},{32,32,22},{33,22,23}},{{32,33,23},{33,00,13},{23,13,12}},{{23,22,13},{22,12,12},{13,12,11}}},
				{{{33,22,23},{32,32,22},{31,32,33}},{{23,13,12},{33,00,13},{32,33,23}},{{13,12,11},{22,12,12},{23,22,13}}},
				{{{23,22,33},{22,32,32},{33,32,31}},{{12,13,23},{13,00,33},{23,33,32}},{{11,12,13},{12,12,22},{13,22,23}}},
				{{{13,22,33},{12,12,22},{11,12,23}},{{23,33,32},{13,00,33},{12,13,23}},{{23,32,31},{22,32,32},{13,22,33}}},
				{{{11,12,23},{12,12,22},{13,22,33}},{{12,13,23},{13,00,33},{23,33,32}},{{13,22,33},{22,32,32},{23,32,31}}},
				{{{23,32,31},{22,32,32},{13,22,33}},{{23,33,32},{13,00,33},{12,13,23}},{{13,22,33},{12,12,22},{11,12,23}}},
				{{{13,22,33},{22,32,32},{23,32,31}},{{12,13,23},{13,00,33},{23,33,32}},{{11,12,23},{12,12,22},{13,22,33}}},
				{{{33,22,23},{22,12,12},{13,12,11}},{{32,33,23},{33,00,13},{23,13,12}},{{31,32,33},{32,32,22},{23,22,13}}},
				{{{23,22,33},{12,12,22},{11,12,13}},{{23,33,32},{13,00,33},{12,13,23}},{{33,32,31},{22,32,32},{13,22,23}}},
				{{{31,32,33},{32,32,22},{23,22,13}},{{32,33,23},{33,00,13},{23,13,12}},{{33,22,23},{22,12,12},{13,12,11}}},
				{{{33,32,31},{22,32,32},{13,22,23}},{{23,33,32},{13,00,33},{12,13,23}},{{23,22,33},{12,12,22},{11,12,13}}}};

/* Zakladni 3d konvolucni jadro hranoveho operatoru Sobel.
*/
static int	CompassSobel3DKernel[3][3][3]={{{1,2,1},{2,4,2},{1,2,1}},{{0,0,0},{0,0,0},{0,0,0}},{{-1,-2,-1},{-2,-4,-2},{-1,-2,-1}}};

/* Zakladni 3d konvolucni jadro hranoveho operatoru Prewitt.
*/
static int	Prewitt3DKernel[3][3][3]={{{1,1,1},{1,1,1},{1,1,1}},{{0,0,0},{0,0,0},{0,0,0}},{{-1,-1,-1},{-1,-1,-1},{-1,-1,-1}}};

/* Zakladni 3d konvolucni jadro hranoveho operatoru Kirsch.
*/
static int	Kirsch3DKernel[3][3][3]={{{17,17,17},{17,17,17},{17,17,17}},{{-9,-9,-9},{-9,0,-9},{-9,-9,-9}},{{-9,-9,-9},{-9,-9,-9},{-9,-9,-9}}};

/* Zakladni 3d konvolucni jadrohranoveho operatoru Robinson.
*/
static int	Robinson3DKernel[3][3][3]={{{-1,-1,-1},{-1,-1,-1},{-1,-1,-1}},{{1,1,1},{1,-8,1},{1,1,1}},{{1,1,1},{1,1,1},{1,1,1}}};

/* Datova struktura JADRO. Do teto struktury se vytvari jednotliva otoceni
   zakladnich konvolucnich jader s pomoci pole Maska uvedeneho vyse.
*/
typedef struct{
	int kernel[42][3][3][3];
}JADRO;

/* Tato funkce vytvori vsech 42 moznych otoceni zakladniho konvolucniho
   jadra.
*/
static void Make3DKernel(int vstup[3][3][3],JADRO& vystup){
	int e11,e12,e13,e00,e22,e23,e31,e32,e33,x,y,z,i;
	JADRO   jadro;

	e11=vstup[0][1][1];
	e12=vstup[0][1][2];
	e13=vstup[0][2][2];

	e00=vstup[1][1][1];
	e22=vstup[1][1][2];
	e23=vstup[1][2][2];

	e31=vstup[2][1][1];
	e32=vstup[2][1][2];
	e33=vstup[2][2][2];

	for(i=0;i<42;i++)
		for(z=0;z<3;z++)
			for(y=0;y<3;y++)
				for(x=0;x<3;x++){
					switch (Maska[i][z][y][x]){
						case 11 : jadro.kernel[i][z][y][x]=e11;break;
						case 12 : jadro.kernel[i][z][y][x]=e12;break;
						case 13 : jadro.kernel[i][z][y][x]=e13;break;
						case 00 : jadro.kernel[i][z][y][x]=e00;break;
						case 22 : jadro.kernel[i][z][y][x]=e22;break;
						case 23 : jadro.kernel[i][z][y][x]=e23;break;
						case 31 : jadro.kernel[i][z][y][x]=e31;break;
						case 32 : jadro.kernel[i][z][y][x]=e32;break;
						case 33 : jadro.kernel[i][z][y][x]=e33;break;
						default : jadro.kernel[i][z][y][x]=4;break;
					}
				}

	#if LEVEL >= 2
	for(i=0;i<42;i++){
		cout<<endl;
		for(y=0;y<3;y++){
			cout<<setw(2)<<jadro.kernel[i][0][y][0]<<" "<<setw(2)<<jadro.kernel[i][0][y][1]<<" "<<setw(2)<<jadro.kernel[i][0][y][2]<<"    "
				<<setw(2)<<jadro.kernel[i][1][y][0]<<" "<<setw(2)<<jadro.kernel[i][1][y][1]<<" "<<setw(2)<<jadro.kernel[i][1][y][2]<<"    "
				<<setw(2)<<jadro.kernel[i][2][y][0]<<" "<<setw(2)<<jadro.kernel[i][2][y][1]<<" "<<setw(2)<<jadro.kernel[i][2][y][2]<<"    "<<endl;
		}
	}
	#endif
	vystup=jadro;
}

/* Aplikuje na vstupni obraz vsech 42 konvolucnich jader vzniklych otocenim
   zakladniho konvolucniho jadra a do vysledku ulozi maximalni odezvu.
*/
template <class VOXEL> 
static void Apply3DCompassFilter(const Image3d<VOXEL> &in,Image3d<VOXEL> &out,JADRO filtr)
{
	int			xx,yy,zz,k,delitel=0;
	size_t  x,y,z;
	int			pole[42],max,uloz;
	float		suma;

	#if LEVEL >= 1
	cout<<"Entering Apply3DCompassFilter"<<endl;
	#endif

	for(zz=0;zz<3;zz++)
		for(yy=0;yy<3;yy++)
			for(xx=0;xx<3;xx++)
				if(filtr.kernel[0][zz][yy][xx]>0)delitel+=filtr.kernel[0][zz][yy][xx];

	for(z=1;z<in.GetSizeZ()-1;z++){
		#if LEVEL >= 3
		cout<<z<<endl;
		#endif
		for(y=1;y<in.GetSizeY()-1;y++)
			for(x=1;x<in.GetSizeX()-1;x++){

				for(k=0;k<42;k++){
					suma=0.0;
					for(zz=0;zz<3;zz++)
						for(yy=0;yy<3;yy++)
							for(xx=0;xx<3;xx++){
								suma+=(in.GetVoxel(x+xx-1,y+yy-1,z+zz-1)*filtr.kernel[k][zz][yy][xx]);
							}
					pole[k]=static_cast<int>(Abs(suma));
				}

				max=0;
				for(k=0;k<42;k++){
					if(pole[k]>max)max=pole[k];
				}

				uloz=max/delitel;

				if(uloz>std::numeric_limits<VOXEL>::max()) uloz=static_cast<int>(std::numeric_limits<VOXEL>::max());

				out.SetVoxel(x,y,z,static_cast<VOXEL>(uloz));

		}
	}
	#if LEVEL >= 1
	cout<<"Leaving Apply3DCompassFilter"<<endl;
	#endif
}

/* END Compass3D */

/* BEGIN AGetVoxel */
/* Vrati hodnotu voxelu, ktery se vyskytuje na pozici jejiz souradnice jsou necelociselne.
   Tato funkce pouziva trilinearni interpolaci. Nekontroluje to, zda-li se dany voxel
   nachazi uvnitr objemu.
*/
template <class VOXEL> 
static float AGetVoxel(const Image3d<VOXEL>& in,float x,float y,float z)
{
	VOXEL   neighbourhood[2][2][2];
	float   dx,dy,dz,interpolationX[2][2],interpolationY[2],interpolationZ;

	neighbourhood[0][0][0]=in.GetVoxel((size_t)floor(x) ,(size_t)floor(y)   ,(size_t)floor(z));
	neighbourhood[1][0][0]=in.GetVoxel((size_t)ceil(x)  ,(size_t)floor(y)   ,(size_t)floor(z));
	neighbourhood[0][1][0]=in.GetVoxel((size_t)floor(x) ,(size_t)ceil(y)    ,(size_t)floor(z));
	neighbourhood[1][1][0]=in.GetVoxel((size_t)ceil(x)  ,(size_t)ceil(y)    ,(size_t)floor(z));

	neighbourhood[0][0][1]=in.GetVoxel((size_t)floor(x) ,(size_t)floor(y)   ,(size_t)ceil(z));
	neighbourhood[1][0][1]=in.GetVoxel((size_t)ceil(x)  ,(size_t)floor(y)   ,(size_t)ceil(z));
	neighbourhood[0][1][1]=in.GetVoxel((size_t)floor(x) ,(size_t)ceil(y)    ,(size_t)ceil(z));
	neighbourhood[1][1][1]=in.GetVoxel((size_t)ceil(x)  ,(size_t)ceil(y)    ,(size_t)ceil(z));

	dx=x-floor(x);
	dy=y-floor(y);
	dz=z-floor(z);

	interpolationX[0][0]=neighbourhood[0][0][0]+dx*(neighbourhood[1][0][0]-neighbourhood[0][0][0]);
	interpolationX[1][0]=neighbourhood[0][1][0]+dx*(neighbourhood[1][1][0]-neighbourhood[0][1][0]);

	interpolationX[0][1]=neighbourhood[0][0][1]+dx*(neighbourhood[1][0][1]-neighbourhood[0][0][1]);
	interpolationX[1][1]=neighbourhood[0][1][1]+dx*(neighbourhood[1][1][1]-neighbourhood[0][1][1]);

	interpolationY[0]=interpolationX[0][0]+dy*(interpolationX[1][0]-interpolationX[0][0]);
	interpolationY[1]=interpolationX[0][1]+dy*(interpolationX[1][1]-interpolationX[0][1]);

	interpolationZ=interpolationY[0]+dz*(interpolationY[1]-interpolationY[0]);

	return interpolationZ;
}

/* Vrati hodnotu vektoru, ktery se vyskytuje na pozici jejiz souradnice jsou necelociselne.
   Tato funkce pouziva trilinearni interpolaci. Vstupni obraz je pole 3d vektoru.
*/
template <class VOXEL> 
static Vector3d<float> AGetVoxel(const VectField3d<VOXEL>& in,float x,float y,float z){
	Vector3d<VOXEL> neighbourhood[2][2][2];
	Vector3d<float> interpolationX[2][2],interpolationY[2],interpolationZ;
	float           dx,dy,dz;

	neighbourhood[0][0][0]=in.GetVoxel((size_t)floor(x) ,(size_t)floor(y)   ,(size_t)floor(z));
	neighbourhood[1][0][0]=in.GetVoxel((size_t)ceil(x)  ,(size_t)floor(y)   ,(size_t)floor(z));
	neighbourhood[0][1][0]=in.GetVoxel((size_t)floor(x) ,(size_t)ceil(y)    ,(size_t)floor(z));
	neighbourhood[1][1][0]=in.GetVoxel((size_t)ceil(x)  ,(size_t)ceil(y)    ,(size_t)floor(z));

	neighbourhood[0][0][1]=in.GetVoxel((size_t)floor(x) ,(size_t)floor(y)   ,(size_t)ceil(z));
	neighbourhood[1][0][1]=in.GetVoxel((size_t)ceil(x)  ,(size_t)floor(y)   ,(size_t)ceil(z));
	neighbourhood[0][1][1]=in.GetVoxel((size_t)floor(x) ,(size_t)ceil(y)    ,(size_t)ceil(z));
	neighbourhood[1][1][1]=in.GetVoxel((size_t)ceil(x)  ,(size_t)ceil(y)    ,(size_t)ceil(z));

	dx=x-floor(x);
	dy=y-floor(y);
	dz=z-floor(z);

	interpolationX[0][0]=neighbourhood[0][0][0]+dx*(neighbourhood[1][0][0]-neighbourhood[0][0][0]);
	interpolationX[1][0]=neighbourhood[0][1][0]+dx*(neighbourhood[1][1][0]-neighbourhood[0][1][0]);

	interpolationX[0][1]=neighbourhood[0][0][1]+dx*(neighbourhood[1][0][1]-neighbourhood[0][0][1]);
	interpolationX[1][1]=neighbourhood[0][1][1]+dx*(neighbourhood[1][1][1]-neighbourhood[0][1][1]);

	interpolationY[0]=interpolationX[0][0]+dy*(interpolationX[1][0]-interpolationX[0][0]);
	interpolationY[1]=interpolationX[0][1]+dy*(interpolationX[1][1]-interpolationX[0][1]);

	interpolationZ=interpolationY[0]+dz*(interpolationY[1]-interpolationY[0]);

	return interpolationZ;
}

/* END   AGetVoxel */

/* BEGIN Robinson3D edge detector */
/* Aplikuje 3d Robinsonuv hranovy operator na vstupni obraz.
*/
template <class VOXEL> void Robinson3D(const Image3d<VOXEL> &in,Image3d<VOXEL> &out){
	JADRO   filtr;

	out.CopyMetaData(in);

	#if LEVEL >= 1
	cout<<"Entering Robinson3D"<<endl;
	#endif

	Make3DKernel(Robinson3DKernel,filtr);
	Apply3DCompassFilter(in,out,filtr);

	#if LEVEL >= 1
	cout<<"Leaving Robinson3D"<<endl;
	#endif
}

template <> I3D_DLLEXPORT void Robinson3D(const Image3d<RGB> &in,Image3d<RGB> &out){
	JADRO   filtr;
	Image3d<GRAY8>  inR,inG,inB,outR,outG,outB;

	out.CopyMetaData(in);

	#if LEVEL >= 1
	cout<<"Entering Robinson3D"<<endl;
	#endif

	inR.MakeRoom(in.GetSize());
	inG.MakeRoom(in.GetSize());
	inB.MakeRoom(in.GetSize());
	outR.MakeRoom(in.GetSize());
	outG.MakeRoom(in.GetSize());
	outB.MakeRoom(in.GetSize());

	RGBtoGray(in,inR,inG,inB);

	Make3DKernel(Robinson3DKernel,filtr);
	Apply3DCompassFilter(inR,outR,filtr);
	Apply3DCompassFilter(inG,outG,filtr);
	Apply3DCompassFilter(inB,outB,filtr);

	GrayToRGB(outR,outG,outB,out);

	#if LEVEL >= 1
	cout<<"Leaving Robinson3D"<<endl;
	#endif
}

template <> I3D_DLLEXPORT void Robinson3D(const Image3d<RGB16> &in,Image3d<RGB16> &out){
	JADRO   filtr;
	Image3d<GRAY16> inR,inG,inB,outR,outG,outB;

	out.CopyMetaData(in);

	#if LEVEL >= 1
	cout<<"Entering Robinson3D"<<endl;
	#endif

	inR.MakeRoom(in.GetSize());
	inG.MakeRoom(in.GetSize());
	inB.MakeRoom(in.GetSize());
	outR.MakeRoom(in.GetSize());
	outG.MakeRoom(in.GetSize());
	outB.MakeRoom(in.GetSize());

	RGBtoGray(in,inR,inG,inB);

	Make3DKernel(Robinson3DKernel,filtr);
	Apply3DCompassFilter(inR,outR,filtr);
	Apply3DCompassFilter(inG,outG,filtr);
	Apply3DCompassFilter(inB,outB,filtr);

	GrayToRGB(outR,outG,outB,out);

	#if LEVEL >= 1
	cout<<"Leaving Robinson3D"<<endl;
	#endif
}

/* END Robinson3D edge detector */

/* BEGIN CompassSobel3D edge detector */

/* Aplikuje 3d Sobeluv hranovy operator na vstupni obraz.
*/
template <class VOXEL> void CompassSobel3D(const Image3d<VOXEL> &in,Image3d<VOXEL> &out){
	JADRO   filtr;

	out.CopyMetaData(in);

	#if LEVEL >= 1
	cout<<"Entering CompassSobel3D"<<endl;
	#endif

	Make3DKernel(CompassSobel3DKernel,filtr);
	Apply3DCompassFilter(in,out,filtr);

	#if LEVEL >= 1
	cout<<"Leaving CompassSobel3D"<<endl;
	#endif
}

template <> I3D_DLLEXPORT void CompassSobel3D(const Image3d<RGB> &in,Image3d<RGB> &out){
	JADRO   filtr;
	Image3d<GRAY8>  inR,inG,inB,outR,outG,outB;

	out.CopyMetaData(in);

	#if LEVEL >= 1
	cout<<"Entering CompassSobel3D"<<endl;
	#endif

	inR.MakeRoom(in.GetSize());
	inG.MakeRoom(in.GetSize());
	inB.MakeRoom(in.GetSize());
	outR.MakeRoom(in.GetSize());
	outG.MakeRoom(in.GetSize());
	outB.MakeRoom(in.GetSize());

	RGBtoGray(in,inR,inG,inB);

	Make3DKernel(CompassSobel3DKernel,filtr);
	Apply3DCompassFilter(inR,outR,filtr);
	Apply3DCompassFilter(inG,outG,filtr);
	Apply3DCompassFilter(inB,outB,filtr);

	GrayToRGB(outR,outG,outB,out);

	#if LEVEL >= 1
	cout<<"Leaving CompassSobel3D"<<endl;
	#endif
}

template <> I3D_DLLEXPORT void CompassSobel3D(const Image3d<RGB16> &in,Image3d<RGB16> &out){
	JADRO   filtr;
	Image3d<GRAY16> inR,inG,inB,outR,outG,outB;

	out.CopyMetaData(in);

	#if LEVEL >= 1
	cout<<"Entering CompassSobel3D"<<endl;
	#endif

	inR.MakeRoom(in.GetSize());
	inG.MakeRoom(in.GetSize());
	inB.MakeRoom(in.GetSize());
	outR.MakeRoom(in.GetSize());
	outG.MakeRoom(in.GetSize());
	outB.MakeRoom(in.GetSize());

	RGBtoGray(in,inR,inG,inB);

	Make3DKernel(CompassSobel3DKernel,filtr);
	Apply3DCompassFilter(inR,outR,filtr);
	Apply3DCompassFilter(inG,outG,filtr);
	Apply3DCompassFilter(inB,outB,filtr);

	GrayToRGB(outR,outG,outB,out);

	#if LEVEL >= 1
	cout<<"Leaving CompassSobel3D"<<endl;
	#endif
}

/* END CompassSobel3D edge detector */

/* BEGIN Prewitt3D edge detector */

/* Aplikuje 3d Prewittuv hranovy operator na vstupni obraz.
*/
template <class VOXEL> void Prewitt3D(const Image3d<VOXEL> &in,Image3d<VOXEL> &out){
	JADRO   filtr;

	out.CopyMetaData(in);

	#if LEVEL >= 1
	cout<<"Entering Prewitt3D"<<endl;
	#endif

	Make3DKernel(Prewitt3DKernel,filtr);
	Apply3DCompassFilter(in,out,filtr);

	#if LEVEL >= 1
	cout<<"Leaving Prewitt3D"<<endl;
	#endif
}

template <> I3D_DLLEXPORT void Prewitt3D(const Image3d<RGB> &in,Image3d<RGB> &out){
	JADRO   filtr;
	Image3d<GRAY8>  inR,inG,inB,outR,outG,outB;

	out.CopyMetaData(in);

	#if LEVEL >= 1
	cout<<"Entering Prewitt3D"<<endl;
	#endif

	inR.MakeRoom(in.GetSize());
	inG.MakeRoom(in.GetSize());
	inB.MakeRoom(in.GetSize());
	outR.MakeRoom(in.GetSize());
	outG.MakeRoom(in.GetSize());
	outB.MakeRoom(in.GetSize());

	RGBtoGray(in,inR,inG,inB);

	Make3DKernel(Prewitt3DKernel,filtr);
	Apply3DCompassFilter(inR,outR,filtr);
	Apply3DCompassFilter(inG,outG,filtr);
	Apply3DCompassFilter(inB,outB,filtr);

	GrayToRGB(outR,outG,outB,out);

	#if LEVEL >= 1
	cout<<"Leaving Prewitt3D"<<endl;
	#endif
}

template <> I3D_DLLEXPORT void Prewitt3D(const Image3d<RGB16> &in,Image3d<RGB16> &out){
	JADRO   filtr;
	Image3d<GRAY16> inR,inG,inB,outR,outG,outB;

	out.CopyMetaData(in);

	#if LEVEL >= 1
	cout<<"Entering Prewitt3D"<<endl;
	#endif

	inR.MakeRoom(in.GetSize());
	inG.MakeRoom(in.GetSize());
	inB.MakeRoom(in.GetSize());
	outR.MakeRoom(in.GetSize());
	outG.MakeRoom(in.GetSize());
	outB.MakeRoom(in.GetSize());

	RGBtoGray(in,inR,inG,inB);

	Make3DKernel(Prewitt3DKernel,filtr);
	Apply3DCompassFilter(inR,outR,filtr);
	Apply3DCompassFilter(inG,outG,filtr);
	Apply3DCompassFilter(inB,outB,filtr);

	GrayToRGB(outR,outG,outB,out);

	#if LEVEL >= 1
	cout<<"Leaving Prewitt3D"<<endl;
	#endif
}

/* END Prewitt3D edge detector */

/* BEGIN Kirsch3D edge detector */

/* Aplikuje 3d Kirschuv hranovy operator na vstupni obraz.
*/
template <class VOXEL> void Kirsch3D(const Image3d<VOXEL> &in,Image3d<VOXEL> &out){
	JADRO   filtr;

	out.CopyMetaData(in);

	#if LEVEL >= 1
	cout<<"Entering Kirsch3D"<<endl;
	#endif

	Make3DKernel(Kirsch3DKernel,filtr);
	Apply3DCompassFilter(in,out,filtr);

	#if LEVEL >= 1
	cout<<"Leaving Kirsch3D"<<endl;
	#endif
}

template <> I3D_DLLEXPORT void Kirsch3D(const Image3d<RGB> &in,Image3d<RGB> &out){
	JADRO   filtr;
	Image3d<GRAY8>  inR,inG,inB,outR,outG,outB;

	out.CopyMetaData(in);

	#if LEVEL >= 1
	cout<<"Entering Kirsch3D"<<endl;
	#endif

	inR.MakeRoom(in.GetSize());
	inG.MakeRoom(in.GetSize());
	inB.MakeRoom(in.GetSize());
	outR.MakeRoom(in.GetSize());
	outG.MakeRoom(in.GetSize());
	outB.MakeRoom(in.GetSize());

	RGBtoGray(in,inR,inG,inB);

	Make3DKernel(Kirsch3DKernel,filtr);
	Apply3DCompassFilter(inR,outR,filtr);
	Apply3DCompassFilter(inG,outG,filtr);
	Apply3DCompassFilter(inB,outB,filtr);

	GrayToRGB(outR,outG,outB,out);

	#if LEVEL >= 1
	cout<<"Leaving Kirsch3D"<<endl;
	#endif
}

template <> I3D_DLLEXPORT void Kirsch3D(const Image3d<RGB16> &in,Image3d<RGB16> &out){
	JADRO   filtr;
	Image3d<GRAY16> inR,inG,inB,outR,outG,outB;

	out.CopyMetaData(in);

	#if LEVEL >= 1
	cout<<"Entering Kirsch3D"<<endl;
	#endif

	inR.MakeRoom(in.GetSize());
	inG.MakeRoom(in.GetSize());
	inB.MakeRoom(in.GetSize());
	outR.MakeRoom(in.GetSize());
	outG.MakeRoom(in.GetSize());
	outB.MakeRoom(in.GetSize());

	RGBtoGray(in,inR,inG,inB);

	Make3DKernel(Kirsch3DKernel,filtr);
	Apply3DCompassFilter(inR,outR,filtr);
	Apply3DCompassFilter(inG,outG,filtr);
	Apply3DCompassFilter(inB,outB,filtr);

	GrayToRGB(outR,outG,outB,out);

	#if LEVEL >= 1
	cout<<"Leaving Kirsch3D"<<endl;
	#endif
}

/* END Kirsch3D edge detector */

/* Tato funkce vrati nejblizsi vyssi nebo stejne cislo, ktere je mocninou dvou.
   Vztazeno ke vstupnimu parametru 'length'.
*/
static int optimal_length(int length){
	double exponent;
	int    result;

	exponent=log(static_cast<double>(length))/log(2.0);
	exponent=ceil(exponent);
	result=static_cast<int>(round(pow(2.0,exponent)));

	return result;
}

/* BEGIN Shen-Castan3D edge detector */

/* Vrati hodnotu filtru ISEF (infinite symmetric exponential filter) v bode x
   pro parametr p.
*/
static double ISEF1D(double x,double p)
{
	return ((p/2)*exp(-p*Abs(x)));
}

/*

	osa:
	0 - x
	1 - y
	2 - z

*/

#ifdef WITH_FFTW

/* Aplikuje jednorozmerny filtr na vstupni obraz 'in', filtr uvnitr ma rozmer
   pouze v ose x. Funkce predpoklada lichy rozmer filtru (stred je uprostred).
	 Filtr se aplikuje ve zvolene ose. Tato verze vyuziva fftw.

   osy:
     0 - x
     1 - y
     2 - z
*/
template <class VOXEL,class REAL> 
static void ApplyFilter1D2(const Image3d<VOXEL> &in,Image3d<REAL> *out,Filter<double> &filtr,int osa)
{
	fftw_complex *complex_array=0,*ft_kernel=0;
	fftw_plan    p1,p2,p3;
	double       a,b,c,d,*real_array=0,*kernel=0;
	int          kernel_length,c_length,halfsize,i,width,height,depth,x,y,z;

	kernel_length=filtr.size.x;
	halfsize=(kernel_length-1)/2;
	width=in.GetSizeX();
	height=in.GetSizeY();
	depth=in.GetSizeZ();

	if(osa==0){
		c_length=width+2*halfsize;
		c_length=optimal_length(c_length);

		real_array=(double *)fftw_malloc(sizeof(double)*c_length);
		complex_array=(fftw_complex *)fftw_malloc(sizeof(fftw_complex)*c_length);
		kernel=(double *)fftw_malloc(sizeof(double)*c_length);
		ft_kernel=(fftw_complex *)fftw_malloc(sizeof(fftw_complex)*c_length);

		p1=fftw_plan_dft_r2c_1d(c_length,kernel,ft_kernel,FFTW_ESTIMATE);

		for(i=0;i<c_length;i++){
			kernel[i]=0.0;
		}

		for(i=0;i<=halfsize;i++){
			kernel[i]=filtr.win[i+halfsize];
		}

		for(i=1;i<=halfsize;i++){
			kernel[c_length-i]=filtr.win[halfsize-i];
		}

		fftw_execute(p1);

		p2=fftw_plan_dft_r2c_1d(c_length,real_array,complex_array,FFTW_ESTIMATE);
		p3=fftw_plan_dft_c2r_1d(c_length,complex_array,real_array,FFTW_ESTIMATE);

    for(z=0;z<depth;z++){
	    for(y=0;y<height;y++){
				for(x=0;x<halfsize;x++){
					real_array[x]=in.GetVoxel(0,y,z);
				}
				for(x=0;x<width;x++){
					real_array[halfsize+x]=in.GetVoxel(x,y,z);
				}
				for(x=(halfsize+width);x<c_length;x++){
					real_array[x]=in.GetVoxel(width-1,y,z);
				}


				fftw_execute(p2);

				for(i=0;i<c_length;i++){
					a=complex_array[i][0];
					b=complex_array[i][1];
					c=ft_kernel[i][0];
					d=ft_kernel[i][1];
					complex_array[i][0]=a*c-b*d;
					complex_array[i][1]=a*d+b*c;
				}

				fftw_execute(p3);

				for(x=0;x<width;x++){
					out->SetVoxel(x,y,z,real_array[halfsize+x]/c_length);
				}
			}
		}
	}

	if(osa==1){
		c_length=height+2*halfsize;
		c_length=optimal_length(c_length);

		real_array=(double *)fftw_malloc(sizeof(double)*c_length);
		complex_array=(fftw_complex *)fftw_malloc(sizeof(fftw_complex)*c_length);
		kernel=(double *)fftw_malloc(sizeof(double)*c_length);
		ft_kernel=(fftw_complex *)fftw_malloc(sizeof(fftw_complex)*c_length);

		p1=fftw_plan_dft_r2c_1d(c_length,kernel,ft_kernel,FFTW_ESTIMATE);

		for(i=0;i<c_length;i++){
			kernel[i]=0.0;
		}

		for(i=0;i<=halfsize;i++){
			kernel[i]=filtr.win[i+halfsize];
		}

		for(i=1;i<=halfsize;i++){
			kernel[c_length-i]=filtr.win[halfsize-i];
		}

		fftw_execute(p1);

		p2=fftw_plan_dft_r2c_1d(c_length,real_array,complex_array,FFTW_ESTIMATE);
		p3=fftw_plan_dft_c2r_1d(c_length,complex_array,real_array,FFTW_ESTIMATE);

    for(z=0;z<depth;z++){
	    for(x=0;x<width;x++){
				for(y=0;y<halfsize;y++){
					real_array[y]=in.GetVoxel(x,0,z);
				}
				for(y=0;y<height;y++){
					real_array[halfsize+y]=in.GetVoxel(x,y,z);
				}
				for(y=(halfsize+height);y<c_length;y++){
					real_array[y]=in.GetVoxel(x,height-1,z);
				}

				fftw_execute(p2);

				for(i=0;i<c_length;i++){
					a=complex_array[i][0];
					b=complex_array[i][1];
					c=ft_kernel[i][0];
					d=ft_kernel[i][1];
					complex_array[i][0]=a*c-b*d;
					complex_array[i][1]=a*d+b*c;
				}

				fftw_execute(p3);

				for(y=0;y<height;y++){
					out->SetVoxel(x,y,z,real_array[halfsize+y]/c_length);
				}
			}
		}
	}

	if(osa==2){
		c_length=depth+2*halfsize;
		c_length=optimal_length(c_length);

		real_array=(double *)fftw_malloc(sizeof(double)*c_length);
		complex_array=(fftw_complex *)fftw_malloc(sizeof(fftw_complex)*c_length);
		kernel=(double *)fftw_malloc(sizeof(double)*c_length);
		ft_kernel=(fftw_complex *)fftw_malloc(sizeof(fftw_complex)*c_length);

		p1=fftw_plan_dft_r2c_1d(c_length,kernel,ft_kernel,FFTW_ESTIMATE);

		for(i=0;i<c_length;i++){
			kernel[i]=0.0;
		}

		for(i=0;i<=halfsize;i++){
			kernel[i]=filtr.win[i+halfsize];
		}

		for(i=1;i<=halfsize;i++){
			kernel[c_length-i]=filtr.win[halfsize-i];
		}

		fftw_execute(p1);

		p2=fftw_plan_dft_r2c_1d(c_length,real_array,complex_array,FFTW_ESTIMATE);
		p3=fftw_plan_dft_c2r_1d(c_length,complex_array,real_array,FFTW_ESTIMATE);

    for(x=0;x<width;x++){
	    for(y=0;y<height;y++){
				for(z=0;z<halfsize;z++){
					real_array[z]=in.GetVoxel(x,y,0);
				}
				for(z=0;z<depth;z++){
					real_array[halfsize+z]=in.GetVoxel(x,y,z);
				}
				for(z=(halfsize+depth);z<c_length;z++){
					real_array[z]=in.GetVoxel(x,y,depth-1);
				}

				fftw_execute(p2);

				for(i=0;i<c_length;i++){
					a=complex_array[i][0];
					b=complex_array[i][1];
					c=ft_kernel[i][0];
					d=ft_kernel[i][1];
					complex_array[i][0]=a*c-b*d;
					complex_array[i][1]=a*d+b*c;
				}

				fftw_execute(p3);

				for(z=0;z<depth;z++){
					out->SetVoxel(x,y,z,real_array[halfsize+z]/c_length);
				}
			}
		}
	}

	fftw_free(real_array);
	fftw_free(complex_array);
}

#else 
template <class VOXEL,class REAL> void ApplyFilter1D2(Image3d<VOXEL> &in,Image3d<REAL> *out,Filter<double> &filtr,int osa){
	std::cout << "Warning, you have selected I3Dlib without FFTW support!\nFunction ApplyFilter1D2() is empty!\n";
}

#endif //fftw


/* Aplikuje jednorozmerny filtr na vstupni obraz 'in', filtr uvnitr ma rozmer
   pouze v ose x. Funkce predpoklada lichy rozmer filtru (stred je uprostred).
	 Filtr se aplikuje ve zvolene ose. Tato verze nevyuziva fftw!

   osy:
     0 - x
     1 - y
     2 - z
*/
static void ApplyFilter1D(Image3d<float> *in,Filter<double> &filtr,int osa)
{
	Vector3d<float> relative_resolution;
	size_t          x,y,z,width,height,depth;
	float           soucet,*pole,low,high,c;
	int             halfsize,i,ilow,ihigh;

	#if LEVEL >= 3
	cout<<"Entering ApplyFilter1D"<<endl;
	#endif

	relative_resolution=get_relative_resolution(*in);

	halfsize=(filtr.size.x-1)/2;
	width=in->GetSizeX();
	height=in->GetSizeY();
	depth=in->GetSizeZ();

	if(osa==0){
		pole=new float[width];

		for(z=0;z<depth;z++){
			for(y=0;y<height;y++){

				for(x=0;x<width;x++)
					pole[x]=in->GetVoxel(x,y,z);

				for(x=0;x<width;x++){
					soucet=0.0;
					
					for(i=-halfsize;i<=halfsize;i++){
						c=x+i*relative_resolution.x;
						ilow=static_cast<int>(floor(c));
						ihigh=static_cast<int>(ceil(c));
						if(ilow>=0 && ihigh<static_cast<int>(width)){
							low=pole[ilow];
							high=pole[ihigh];
						}
						else{
							low=high=0.0;
						}
						soucet+=(low+(c-floor(c))*(high-low))*filtr.win[i+halfsize];
					}

					in->SetVoxel(x,y,z,soucet);
				}
			}
		}
		delete pole;
	}

	if(osa==1){
		pole=new float[height];

		for(z=0;z<depth;z++){
			for(x=0;x<width;x++){

				for(y=0;y<height;y++)
					pole[y]=in->GetVoxel(x,y,z);

				for(y=0;y<height;y++){
					soucet=0.0;
					
					for(i=-halfsize;i<=halfsize;i++){
						c=y+i*relative_resolution.y;
						ilow=static_cast<int>(floor(c));
						ihigh=static_cast<int>(ceil(c));
						if(ilow>=0 && ihigh<static_cast<int>(height)){
							low=pole[ilow];
							high=pole[ihigh];
						}
						else{
							low=high=0.0;
						}
						soucet+=(low+(c-floor(c))*(high-low))*filtr.win[i+halfsize];
					}
					
					in->SetVoxel(x,y,z,soucet);
				}
			}
		}
		delete pole;
	}

	if(osa==2){
		pole=new float[depth];

		for(y=0;y<height;y++){
			for(x=0;x<width;x++){

				for(z=0;z<depth;z++)
					pole[z]=in->GetVoxel(x,y,z);

				for(z=0;z<depth;z++){
					soucet=0.0;
					
					for(i=-halfsize;i<=halfsize;i++){
						c=z+i*relative_resolution.z;
						ilow=static_cast<int>(floor(c));
						ihigh=static_cast<int>(ceil(c));
						if(ilow>=0 && ihigh<static_cast<int>(depth)){
							low=pole[ilow];
							high=pole[ihigh];
						}
						else{
							low=high=0.0;
						}
						soucet+=(low+(c-floor(c))*(high-low))*filtr.win[i+halfsize];
					}
					
					in->SetVoxel(x,y,z,soucet);
				}
			}
		}
		delete pole;
	}
	#if LEVEL >= 3
	cout<<"Leaving  ApplyFilter1D"<<endl;
	#endif
}

/* Tato funkce vytvori 1d filtr typu ISEF. Velikost filtru je dana vztahem
   widthFactor/p.
*/
static Filter<double>  MakeISEF1DFilter(double p,double widthFactor)
{
	Filter<double>  filtr;
	int x,halfsize;

	#if LEVEL >= 1
	cout<<"Entering MakeISEF1DFilter"<<endl;
	#endif

	halfsize=static_cast<int>(widthFactor/p);

	filtr.win=new double[(2*halfsize+1)];

	#if LEVEL >= 2
	cout<<"HalfSize of kernel is "<<halfsize<<" and p is "<<p<<"."<<endl;
	#endif

	filtr.size.x=2*halfsize+1;
	filtr.size.y=filtr.size.z=1;

	#if LEVEL >= 2
	printf("\n");
	#endif

	for(x=-halfsize;x<=halfsize;x++){
		filtr.win[x+halfsize]=ISEF1D(x,p);

		#if LEVEL >= 2
		printf("%f ",filtr.win[x+halfsize]);
		#endif

	}

	#if LEVEL >= 2
	printf("\n");
	#endif

	#if LEVEL >= 1
	cout<<"Leaving MakeISEF1DFilter"<<endl;
	#endif

	return filtr;

}

/* Zjisti zda voxel x,y,z je kandidat na hranu. To udela tak, ze jako kandidata
   na hranu oznaci voxel, ktery bud prechazi pres nulu kladne a ma kladnou
   druhou derivaci nebo prechazi pres nulu zaporne a ma zapornou druhou
   derivaci.
*/
template <class VOXEL> 
static bool IsCandidateEdge3D(const Image3d<float> *smoothed,Image3d<VOXEL> &out,size_t x,size_t y,size_t z){
	VOXEL   v,middle;
	float   l,r;

	middle=(out.Include(x,y,z))?out.GetVoxel(x,y,z):0;
	v=(out.Include(x+1,y,z))?out.GetVoxel(x+1,y,z):0;
	/* Kladny prechod pres nulu v ose x */
	if(middle==1 && v==0){

		l=((x+1)<out.GetSizeX())?smoothed->GetVoxel(x+1,y,z):0;
		r=(x>1)?smoothed->GetVoxel(x-1,y,z):0;

		/* Kladna druha derivace v ose x */
		if((l-r)>0)return true;
		else return false;

	}
	else{
		middle=(out.Include(x,y,z))?out.GetVoxel(x,y,z):0;
		v=(out.Include(x,y+1,z))?out.GetVoxel(x,y+1,z):0;
		/* Kladny prechod pres nulu v ose y */
		if(middle==1 && v==0){

			l=((y+1)<out.GetSizeY())?smoothed->GetVoxel(x,y+1,z):0;
			r=(y>1)?smoothed->GetVoxel(x,y-1,z):0;

			/* Kladna druha derivace v ose y */
			if((l-r)>0)return true;
			else return false;

		}
		else{
			middle=(out.Include(x,y,z))?out.GetVoxel(x,y,z):0;
			v=(out.Include(x,y,z+1))?out.GetVoxel(x,y,z+1):0;
			/* Kladny prechod pres nulu v ose z */
			if(middle==1 && v==0){

				l=((z+1)<out.GetSizeZ())?smoothed->GetVoxel(x,y,z+1):0;
				r=(z>1)?smoothed->GetVoxel(x,y,z-1):0;

				/* Kladna druha derivace v ose z */
				if((l-r)>0)return true;
				else return false;

			}
			else{
				middle=(out.Include(x,y,z))?out.GetVoxel(x,y,z):0;
				v=(out.Include(static_cast<int>(x-1),y,z))?out.GetVoxel(x-1,y,z):0;
				/* Zaporny prechod pres nulu v ose x */
				if(middle==1 && v==0){

					l=((x+1)<out.GetSizeX())?smoothed->GetVoxel(x+1,y,z):0;
					r=(x>1)?smoothed->GetVoxel(x-1,y,z):0;

					/* Zaporna druha derivace v ose x */
					if((l-r)<0)return true;
					else return false;

				}
				else{
					middle=(out.Include(x,y,z))?out.GetVoxel(x,y,z):0;
					v=(out.Include(x,static_cast<int>(y-1),z))?out.GetVoxel(x,y-1,z):0;
					/* Zaporny prechod pres nulu v ose y */
					if(middle==1 && v==0){

						l=((y+1)<out.GetSizeY())?smoothed->GetVoxel(x,y+1,z):0;
						r=(y>1)?smoothed->GetVoxel(x,y-1,z):0;

						/* Zaporna druha derivace v ose y */
						if((l-r)<0)return true;
						else return false;

					}
					else{
						middle=(out.Include(x,y,z))?out.GetVoxel(x,y,z):0;
						v=(out.Include(x,y,static_cast<int>(z-1)))?out.GetVoxel(x,y,z-1):0;
						/* Zaporny prechod pres nulu v ose z */
						if(middle==1 && v==0){

						l=((z+1)<out.GetSizeZ())?smoothed->GetVoxel(x,y,z+1):0;
							r=(z>1)?smoothed->GetVoxel(x,y,z-1):0;

							/* Zaporna druha derivace v ose z */
							if((l-r)<0)return true;
							else return false;

						}
					}
				}
			}
		}
	}

	return false;
}

/* Pro kazdy voxel, ktery je kandidatem na hranu se spocita gradient tak, ze ve
   vymezenem okne se spocita prumer hodnot voxelu, ktere maji druhou derivaci
   kladnou a take tech s druhou derivaci zapornou a odecte je od sebe.
*/
template <class VOXEL> 
static void ComputeAdaptiveGradient3D(Image3d<float> *smoothed,const Image3d<VOXEL> &out,int halfsize,float &min,float &max)
{
	std::vector<Image3d<float> *> buffer;
	Image3d<float>           *tmp;
	float                    sum_on,sum_off,avg_on,avg_off,gradient,v;
	VOXEL                    code;
	int                      i,j,k,c,num_on,num_off,osa=0,x,y,z,bx,by,bz,width,height,depth;

	#if LEVEL >= 1
	cout<<"Entering ComputeAdaptiveGradient3D"<<endl;
	#endif

	width=smoothed->GetSizeX();
	height=smoothed->GetSizeY();
	depth=smoothed->GetSizeZ();

	buffer.resize(halfsize+1);

	if((width*height)<(width*depth)){
		if((width*height)<(height*depth))
			osa=2;
		else
			osa=0;
	}
	else{
		if((width*depth)<(height*depth))
			osa=1;
		else
			osa=0;
	}

	// zde se to aplikuje v ruznych osach kvuli uspore pameti
	if(osa==0){
		/*Alokuje buffer velikosti halfsize+1*/
		for(i=0;i<halfsize+1;i++){
			buffer[i]=new Image3d<float>();
			buffer[i]->MakeRoom(1,height,depth);
		}

		for(x=0;x<width;x++){
			#if LEVEL >= 3
			cout<<x<<endl;
			#endif

			tmp=new Image3d<float>();
			tmp->MakeRoom(1,height,depth);
			for(bz=0;bz<depth;bz++)
				for(by=0;by<height;by++)
					tmp->SetVoxel(0,by,bz,smoothed->GetVoxel(x,by,bz));

			delete buffer[0];
			for(bx=0;bx<halfsize;bx++) buffer[bx]=buffer[bx+1];
			buffer[halfsize]=tmp;

			for(y=0;y<height;y++)
				for(z=0;z<depth;z++){
					code=out.GetVoxel(x,y,z);
					if(code>=10){

						sum_on=sum_off=0.0;
						num_on=num_off=0;

						for(k=-halfsize;k<=halfsize;k++)
							for(j=-halfsize;j<=halfsize;j++)
								for(i=-halfsize;i<=halfsize;i++){

									if(i<=0){
										v=((static_cast<int>(x+i))<width && (static_cast<int>(x+i))>=0 &&
											(static_cast<int>(y+j))<height && (static_cast<int>(y+j))>=0 &&
											(static_cast<int>(z+k))<depth && (static_cast<int>(z+k))>=0)?
												buffer[i+halfsize]->GetVoxel(0,y+j,z+k):0;
									}
									else{
										v=((static_cast<int>(x+i))<width && (static_cast<int>(x+i))>=0 &&
											(static_cast<int>(y+j))<height && (static_cast<int>(y+j))>=0 &&
											(static_cast<int>(z+k))<depth && (static_cast<int>(z+k))>=0)?
												smoothed->GetVoxel(x+i,y+j,z+k):0;
									}

									c=static_cast<int>((out.Include(static_cast<int>(x+i),static_cast<int>(y+j),static_cast<int>(z+k)))?out.GetVoxel(x+i,y+j,z+k):0);

									if((c % 10)==1){
										sum_on+=v;
										num_on++;
									}
									else{
										sum_off+=v;
										num_off++;
									}
								}

					if(sum_on>0)
						avg_on=sum_on/static_cast<float>(num_on);
					else
						avg_on=0.0;

					if(sum_off>0)
						avg_off=sum_off/static_cast<float>(num_off);
					else
						avg_off=0.0;

					gradient=avg_off-avg_on;

					smoothed->SetVoxel(x,y,z,gradient);
					if(gradient<min)min=gradient;
					if(gradient>max)max=gradient;

				}
				else{
					smoothed->SetVoxel(x,y,z,std::numeric_limits<float>::min());
				}
			}
		}
	}

	if(osa==1){
		/*Alokuje buffer velikosti halfsize+1*/
		for(i=0;i<halfsize+1;i++){
			buffer[i]=new Image3d<float>();
			buffer[i]->MakeRoom(width,1,depth);
		}

		for(y=0;y<height;y++){
			#if LEVEL >= 3
			cout<<y<<endl;
			#endif

			tmp=new Image3d<float>();
			tmp->MakeRoom(width,1,depth);
			for(bz=0;bz<depth;bz++)
				for(bx=0;bx<width;bx++)
					tmp->SetVoxel(bx,0,bz,smoothed->GetVoxel(bx,y,bz));

			delete buffer[0];
			for(by=0;by<halfsize;by++) buffer[by]=buffer[by+1];
			buffer[halfsize]=tmp;

			for(z=0;z<depth;z++)
				for(x=0;x<width;x++){
					code=out.GetVoxel(x,y,z);
					if(code>=10){

						sum_on=sum_off=0.0;
						num_on=num_off=0;

						for(k=-halfsize;k<=halfsize;k++)
							for(j=-halfsize;j<=halfsize;j++)
								for(i=-halfsize;i<=halfsize;i++){

									if(j<=0){
										v=(static_cast<int>(x+i)<width && static_cast<int>(x+i)>=0 &&
											static_cast<int>(y+j)<height && static_cast<int>(y+j)>=0 &&
											static_cast<int>(z+k)<depth && static_cast<int>(z+k)>=0)?
												buffer[j+halfsize]->GetVoxel(x+i,0,z+k):0;
									}
									else{
										v=(static_cast<int>(x+i)<width && static_cast<int>(x+i)>=0 &&
											static_cast<int>(y+j)<height && static_cast<int>(y+j)>=0 &&
											static_cast<int>(z+k)<depth && static_cast<int>(z+k)>=0)?
												smoothed->GetVoxel(x+i,y+j,z+k):0;
									}

									c=static_cast<int>((out.Include(static_cast<int>(x+i),static_cast<int>(y+j),static_cast<int>(z+k)))?out.GetVoxel(x+i,y+j,z+k):0);
									if((c % 10)==1){
										sum_on+=v;
										num_on++;
									}
									else{
										sum_off+=v;
										num_off++;
									}
								}

					if(sum_on>0)
						avg_on=sum_on/static_cast<float>(num_on);
					else
						avg_on=0.0;

					if(sum_off>0)
						avg_off=sum_off/static_cast<float>(num_off);
					else
						avg_off=0.0;

					gradient=avg_off-avg_on;
					smoothed->SetVoxel(x,y,z,gradient);
					if(gradient<min)min=gradient;
					if(gradient>max)max=gradient;

				}
				else{
					smoothed->SetVoxel(x,y,z,std::numeric_limits<float>::min());
				}
			}
		}
	}

	if(osa==2){
		/*Alokuje buffer velikosti halfsize+1*/
		for(i=0;i<halfsize+1;i++){
			buffer[i]=new Image3d<float>();
			buffer[i]->MakeRoom(width,height,1);
		}

		for(z=0;z<depth;z++){
			#if LEVEL >= 3
			cout<<z<<endl;
			#endif

			tmp=new Image3d<float>();
			tmp->MakeRoom(width,height,1);
			for(by=0;by<height;by++)
				for(bx=0;bx<width;bx++)
					tmp->SetVoxel(bx,by,0,smoothed->GetVoxel(bx,by,z));

			delete buffer[0];
			for(bz=0;bz<halfsize;bz++) buffer[bz]=buffer[bz+1];
			buffer[halfsize]=tmp;

			for(y=0;y<height;y++)
				for(x=0;x<width;x++){
					code=out.GetVoxel(x,y,z);
					if(code>=10){

						sum_on=sum_off=0.0;
						num_on=num_off=0;

						for(k=-halfsize;k<=halfsize;k++)
							for(j=-halfsize;j<=halfsize;j++)
								for(i=-halfsize;i<=halfsize;i++){

									if(k<=0){
										v=(static_cast<int>(x+i)<width && static_cast<int>(x+i)>=0 &&
											static_cast<int>(y+j)<height && static_cast<int>(y+j)>=0 &&
											static_cast<int>(z+k)<depth && static_cast<int>(z+k)>=0)?
												buffer[k+halfsize]->GetVoxel(x+i,y+j,0):0;
									}
									else{
									v=(static_cast<int>(x+i)<width && static_cast<int>(x+i)>=0 &&
										static_cast<int>(y+j)<height && static_cast<int>(y+j)>=0 &&
										static_cast<int>(z+k)<depth && static_cast<int>(z+k)>=0)?
											smoothed->GetVoxel(x+i,y+j,z+k):0;
									}

									c=static_cast<int>((out.Include(static_cast<int>(x+i),static_cast<int>(y+j),static_cast<int>(z+k)))?out.GetVoxel(x+i,y+j,z+k):0);
									if((c % 10)==1){
										sum_on+=v;
										num_on++;
									}
									else{
										sum_off+=v;
										num_off++;
									}
								}

					if(sum_on>0)
						avg_on=sum_on/static_cast<float>(num_on);
					else
						avg_on=0.0;

					if(sum_off>0)
						avg_off=sum_off/static_cast<float>(num_off);
					else
						avg_off=0.0;

					gradient=avg_off-avg_on;
					smoothed->SetVoxel(x,y,z,gradient);
					if(gradient<min)min=gradient;
					if(gradient>max)max=gradient;

				}
				else{
					smoothed->SetVoxel(x,y,z,std::numeric_limits<float>::min());
				}
			}
		}
	}
	#if LEVEL >= 1
	cout<<"Leaving ComputeAdaptiveGradient3D"<<endl;
	#endif
}

/* Tato funkce aplikuje 3d Shen-Castanuv detektor hran na vstupni obraz 'in'.
   Jeji parametry jsou 'p' a 'WinHalfSize'. Parametr p je parametrem funkce
	 ISEF, ktera se aplikuje na obraz. WinHalfSize pak urcuje velikost okoli, ze
	 ktereho se pocita velikost gradientu.
	 p musi byt v intervalu (0.0 , 1.0), velikost filtru zavisi na 1/p
*/
template <class VOXEL> void ApplyShenCastan3D(const Image3d<VOXEL> &in,Image3d<VOXEL> &out,double p,int WinHalfSize){
	Vector3d<float> resolution;
	Image3d<float>	*s1;
	Filter<double>  filtr;
	Resolution      res;
	size_t					x,y,z,width,height,depth;
	float						gradient,min=std::numeric_limits<float>::max(),max=std::numeric_limits<float>::min();
	VOXEL						suma;
	int							halfsize;

	#if LEVEL >= 1
	cout<<"Entering ApplyShenCastan3D"<<endl;
	#endif

	filtr=MakeISEF1DFilter(p,5.0);

	width=in.GetSizeX();
	height=in.GetSizeY();
	depth=in.GetSizeZ();

	res=in.GetResolution();
	resolution=res.GetRes();

	s1=new Image3d<float>();
	s1->MakeRoom(in.GetSize());
	s1->SetResolution(in.GetResolution());

	halfsize=(filtr.size.x-1)/2;

	/* Zkopiruje vstup do objemu realnych cisel */
	for(z=0;z<depth;z++)
		for(y=0;y<height;y++)
			for(x=0;x<width;x++)
				s1->SetVoxel(x,y,z,static_cast<float>(in.GetVoxel(x,y,z)));

	/* Aplikuje ISEF v ose x */
	ApplyFilter1D(s1,filtr,0);
	/* Aplikuje ISEF v ose y */
	ApplyFilter1D(s1,filtr,1);
	/* Aplikuje ISEF v ose z */
	ApplyFilter1D(s1,filtr,2);

	/* Spocita Laplacian of the Image a zakoduje ho do 0 (zaporne hodnoty s nulou) a 1 (kladne hodnoty) do vystupniho objemu.
	*/
	for(z=0;z<depth;z++)
		for(y=0;y<height;y++)
			for(x=0;x<width;x++){
				suma=((s1->GetVoxel(x,y,z)-static_cast<float>(in.GetVoxel(x,y,z)))>0.0)?1:0;
				out.SetVoxel(x,y,z,static_cast<VOXEL>(suma));
			}

	/* Zjisti kandidaty na hranu a ve vystupnim objemu jim pricte hodnotu 10.
	*/
	for(z=0;z<depth;z++){
		#if LEVEL >= 3
		cout<<z<<endl;
		#endif
		for(y=0;y<height;y++)
			for(x=0;x<width;x++){
			if(IsCandidateEdge3D(s1,out,x,y,z)){
				out.SetVoxel(x,y,z,out.GetVoxel(x,y,z)+10);
			}
		}
	}

	/* Pro kandidaty na hranu spocita gradient do objemu realnych cisel s1,
	   do promenych min a max ulozi nejmensi a nejvetsi hodnotu gradientu,
	   WinHalfSize urcuje v jak velikem okne se bude pocitat gradient.
	*/
	ComputeAdaptiveGradient3D(s1,out,WinHalfSize,min,max);

	/* Vynuluje vystupni objem */
	for(z=0;z<depth;z++)
		for(y=0;y<height;y++)
			for(x=0;x<width;x++){
				out.SetVoxel(x,y,z,static_cast<VOXEL>(0));
			}

	/*Srazi velikost gradientu do intervalu 0..maxima elementarniho typu, vyjma minima elementarniho typu
	 (to znaci,ze voxel neni kandidatem na hranu)*/
	for(z=1;z<depth-1;z++)
		for(y=1;y<height-1;y++)
			for(x=1;x<width-1;x++){

				gradient=s1->GetVoxel(x,y,z);

				if(gradient==std::numeric_limits<float>::min())
					gradient=0.0;
				else
					gradient=std::numeric_limits<VOXEL>::max()*(gradient-min)/(max-min);

				out.SetVoxel(x,y,z,static_cast<VOXEL>(gradient));
			}

	delete s1;

	#if LEVEL >= 1
	cout<<"Leaving ApplyShenCastan3D"<<endl;
	#endif
}

template <class VOXEL> void ShenCastan3D(const Image3d<VOXEL> &in,Image3d<VOXEL> &out,double p,int WinHalfSize){
	if(p<=0 || p>1.0)
		throw InternalException("ShenCastan3D: invalid input value of p");
	if(WinHalfSize<1)
		throw InternalException("ShenCastan3D: WinHalfSize is lower than 1");

	out.CopyMetaData(in);

	Vector3d<float> resolution;
	Resolution			res;
	res=in.GetResolution();
	resolution=res.GetRes();

	if(resolution.x!=resolution.y || resolution.y!=resolution.z || resolution.x!=resolution.z)
		throw InternalException("ShenCastan3D: volume has not equal resolutions in all axises");

	#if LEVEL >= 1
	cout<<"Entering ShenCastan3D"<<endl;
	#endif

	ApplyShenCastan3D(in,out,p,WinHalfSize);

	#if LEVEL >= 1
	cout<<"Leaving ShenCastan3D"<<endl;
	#endif
}

template <> I3D_DLLEXPORT void ShenCastan3D(const Image3d<RGB> &in,Image3d<RGB> &out,double p,int WinHalfSize){
	Image3d<GRAY8>  inR,inG,inB,outR,outG,outB;

	if(p<=0 || p>1.0)
		throw InternalException("ShenCastan3D: invalid input value of p");
	if(WinHalfSize<1)
		throw InternalException("ShenCastan3D: WinHalfSize is lower than 1");

	out.CopyMetaData(in);

	Vector3d<float> resolution;
	Resolution			res;
	res=in.GetResolution();
	resolution=res.GetRes();

	if(resolution.x!=resolution.y || resolution.y!=resolution.z || resolution.x!=resolution.z)
		throw InternalException("ShenCastan3D: volume has not equal resolutions in all axises");

	#if LEVEL >= 1
	cout<<"Entering ShenCastan3D"<<endl;
	#endif

	inR.MakeRoom(in.GetSize());
	inG.MakeRoom(in.GetSize());
	inB.MakeRoom(in.GetSize());
	outR.MakeRoom(in.GetSize());
	outG.MakeRoom(in.GetSize());
	outB.MakeRoom(in.GetSize());

	RGBtoGray(in,inR,inG,inB);

	ApplyShenCastan3D(inR,outR,p,WinHalfSize);
	ApplyShenCastan3D(inG,outG,p,WinHalfSize);
	ApplyShenCastan3D(inB,outB,p,WinHalfSize);

	GrayToRGB(outR,outG,outB,out);

	#if LEVEL >= 1
	cout<<"Leaving ShenCastan3D"<<endl;
	#endif
}

template <> I3D_DLLEXPORT void ShenCastan3D(const Image3d<RGB16> &in,Image3d<RGB16> &out,double p,int WinHalfSize){
	Image3d<GRAY16> inR,inG,inB,outR,outG,outB;

	if(p<=0 || p>1.0)
		throw InternalException("ShenCastan3D: invalid input value of p");
	if(WinHalfSize<1)
		throw InternalException("ShenCastan3D: WinHalfSize is lower than 1");

	out.CopyMetaData(in);

	Vector3d<float> resolution;
	Resolution			res;
	res=in.GetResolution();
	resolution=res.GetRes();

	if(resolution.x!=resolution.y || resolution.y!=resolution.z || resolution.x!=resolution.z)
		throw InternalException("ShenCastan3D: volume has not equal resolutions in all axises");

	#if LEVEL >= 1
	cout<<"Entering ShenCastan3D"<<endl;
	#endif

	inR.MakeRoom(in.GetSize());
	inG.MakeRoom(in.GetSize());
	inB.MakeRoom(in.GetSize());
	outR.MakeRoom(in.GetSize());
	outG.MakeRoom(in.GetSize());
	outB.MakeRoom(in.GetSize());

	RGBtoGray(in,inR,inG,inB);

	ApplyShenCastan3D(inR,outR,p,WinHalfSize);
	ApplyShenCastan3D(inG,outG,p,WinHalfSize);
	ApplyShenCastan3D(inB,outB,p,WinHalfSize);

	GrayToRGB(outR,outG,outB,out);

	#if LEVEL >= 1
	cout<<"Leaving ShenCastan3D"<<endl;
	#endif
}

/* END   Shen-Castan3D edge detector */

/* BEGIN Shen-Castan2D edge detector */

/* Zjisti zda voxel x,y,z je kandidat na hranu. To udela tak, ze jako kandidata
   na hranu oznaci voxel, ktery bud prechazi pres nulu kladne a ma kladnou
   druhou derivaci nebo prechazi pres nulu zaporne a ma zapornou druhou
   derivaci.
*/
template <class VOXEL> 
static bool IsCandidateEdge2D(const Image3d<float> *smoothed,Image3d<VOXEL> &out,size_t x,size_t y,size_t z)
{
	VOXEL   v,middle;
	float   l,r;

		middle=(out.Include(x,y,z))?out.GetVoxel(x,y,z):0;
		v=(out.Include(x+1,y,z))?out.GetVoxel(x+1,y,z):0;

		/* Kladny prechod pres nulu v ose x */
		if(middle==1 && v==0){
			l=((x+1)<out.GetSizeX())?smoothed->GetVoxel(x+1,y,z):0;
			r=(x>1)?smoothed->GetVoxel(x-1,y,z):0;

			/* Kladna druha derivace v ose x */
			if((l-r)>0)
				return true;
			else
				return false;
		}
		else{
			middle=(out.Include(x,y,z))?out.GetVoxel(x,y,z):0;
			v=(out.Include(x,y+1,z))?out.GetVoxel(x,y+1,z):0;

			/* Kladny prechod pres nulu v ose y */
			if(middle==1 && v==0){
				l=((y+1)<out.GetSizeY())?smoothed->GetVoxel(x,y+1,z):0;
				r=(y>1)?smoothed->GetVoxel(x,y-1,z):0;

				/* Kladna druha derivace v ose y */
				if((l-r)>0)
					return true;
				else
					return false;
			}
			else{
				middle=(out.Include(x,y,z))?out.GetVoxel(x,y,z):0;
				v=(out.Include(static_cast<int>(x-1),y,z))?out.GetVoxel(x-1,y,z):0;

				/* Zaporny prechod pres nulu v ose x */
				if(middle==1 && v==0){
					l=((x+1)<out.GetSizeX())?smoothed->GetVoxel(x+1,y,z):0;
					r=(x>1)?smoothed->GetVoxel(x-1,y,z):0;

					/* Zaporna druha derivace v ose x */
					if((l-r)<0)
						return true;
					else
						return false;
				}
				else{
					middle=(out.Include(x,y,z))?out.GetVoxel(x,y,z):0;
					v=(out.Include(x,static_cast<int>(y-1),z))?out.GetVoxel(x,y-1,z):0;

					/* Zaporny prechod pres nulu v ose y */
					if(middle==1 && v==0){
						l=((y+1)<out.GetSizeY())?smoothed->GetVoxel(x,y+1,z):0;
						r=(y>1)?smoothed->GetVoxel(x,y-1,z):0;

						/* Zaporna druha derivace v ose y */
						if((l-r)<0)
							return true;
						else
							return false;
					}
				}
			}
		}

	return false;
}

/* Pro kazdy voxel, ktery je kandidatem na hranu se spocita gradient tak, ze ve
   vymezenem okne se spocita prumer hodnot voxelu, ktere maji druhou derivaci
   kladnou a take tech s druhou derivaci zapornou a odecte je od sebe.
*/
template <class VOXEL> 
static void ComputeAdaptiveGradient2D(Image3d<float> *smoothed,const Image3d<VOXEL> &out,int halfsize,float &min,float &max)
{
	Image3d<float> *buffer;
	VOXEL          code;
	float          sum_on,sum_off,avg_on,avg_off,gradient,v;
	int            i,j,c,x,y,z,width,height,depth,num_on,num_off;

	#if LEVEL >= 1
	cout<<"Entering ComputeAdaptiveGradient2D"<<endl;
	#endif

	width=smoothed->GetSizeX();
	height=smoothed->GetSizeY();
	depth=smoothed->GetSizeZ();

	/*Alokuje buffer velikosti halfsize+1*/
	buffer=new Image3d<float>();
	buffer->MakeRoom(width,height,1);

	for(z=0;z<depth;z++){

		#if LEVEL >= 3
		cout<<z<<endl;
		#endif

		for(x=0;x<width;x++)
			for(y=0;y<height;y++){
				code=out.GetVoxel(x,y,z);
				if(code>=10){

					sum_on=sum_off=0.0;
					num_on=num_off=0;

					for(j=-halfsize;j<=halfsize;j++)
						for(i=-halfsize;i<=halfsize;i++){

							v=(static_cast<int>(x+i)<width && static_cast<int>(x+i)>=0 &&
								static_cast<int>(y+j)<height && static_cast<int>(y+j)>=0)?
									smoothed->GetVoxel(x+i,y+j,z):0;

							c=static_cast<int>((out.Include(static_cast<int>(x+i),static_cast<int>(y+j),z))?out.GetVoxel(x+i,y+j,z):0);

							if((c % 10)==1){
								sum_on+=v;
								num_on++;
							}
							else{
								sum_off+=v;
								num_off++;
							}
						}

					if(sum_on>0)
						avg_on=sum_on/static_cast<float>(num_on);
					else
						avg_on=0.0;

					if(sum_off>0)
						avg_off=sum_off/static_cast<float>(num_off);
					else
						avg_off=0.0;

					gradient=avg_off-avg_on;

					buffer->SetVoxel(x,y,0,gradient);
					if(gradient<min)min=gradient;
					if(gradient>max)max=gradient;

				}
				else{
					buffer->SetVoxel(x,y,0,std::numeric_limits<float>::min());
				}
			}

		for(x=0;x<width;x++)
			for(y=0;y<height;y++)
				smoothed->SetVoxel(x,y,z,buffer->GetVoxel(x,y,0));
	}

	#if LEVEL >= 1
	cout<<"Leaving ComputeAdaptiveGradient2D"<<endl;
	#endif
}

/* Tato funkce aplikuje 2d Shen-Castanuv detektor hran na vstupni obraz 'in'.
   Jeji parametry jsou 'p' a 'WinHalfSize'. Parametr p je parametrem funkce
	 ISEF, ktera se aplikuje na obraz. WinHalfSize pak urcuje velikost okoli, ze
	 ktereho se pocita velikost gradientu.
*/
template <class VOXEL> 
static void ApplyShenCastan2D(const Image3d<VOXEL> &in,Image3d<VOXEL> &out,double p,int WinHalfSize)
{
	Vector3d<float> resolution;
	Image3d<float>  *s1;
	Filter<double>  filtr;
	Resolution      res;
	size_t          x,y,z,width,height,depth;
	VOXEL           suma;
	float           gradient,min=std::numeric_limits<float>::max(),max=std::numeric_limits<float>::min();
	int             halfsize;

	#if LEVEL >= 1
	cout<<"Entering ApplyShenCastan2D"<<endl;
	#endif

	filtr=MakeISEF1DFilter(p,5.0);

	width=in.GetSizeX();
	height=in.GetSizeY();
	depth=in.GetSizeZ();

	res=in.GetResolution();
	resolution=res.GetRes();

	s1=new Image3d<float>();
	s1->MakeRoom(in.GetSize());
	s1->SetResolution(in.GetResolution());

	halfsize=(filtr.size.x-1)/2;

	/* Zkopiruje vstup do objemu realnych cisel */
	for(z=0;z<depth;z++)
		for(y=0;y<height;y++)
			for(x=0;x<width;x++)
				s1->SetVoxel(x,y,z,static_cast<float>(in.GetVoxel(x,y,z)));

	/* Aplikuje ISEF v ose x */
	ApplyFilter1D(s1,filtr,0);
	/* Aplikuje ISEF v ose y */
	ApplyFilter1D(s1,filtr,1);

	/* Spocita Laplacian of the Image a zakoduje ho do 0 (zaporne hodnoty s nulou)
	   a 1 (kladne hodnoty) do vystupniho objemu.
	*/
	for(z=0;z<depth;z++)
		for(y=0;y<height;y++)
			for(x=0;x<width;x++){
				suma=((s1->GetVoxel(x,y,z)-static_cast<float>(in.GetVoxel(x,y,z)))>0.0)?1:0;
				out.SetVoxel(x,y,z,static_cast<VOXEL>(suma));
			}

	/* Zjisti kandidaty na hranu a ve vystupnim objemu jim pricte hodnotu 10.
	*/
	for(z=0;z<depth;z++){
		#if LEVEL >= 3
		cout<<z<<endl;
		#endif
		for(y=0;y<height;y++)
			for(x=0;x<width;x++){
				if(IsCandidateEdge2D(s1,out,x,y,z)){
					out.SetVoxel(x,y,z,out.GetVoxel(x,y,z)+10);
				}
			}
	}

	/* Pro kandidaty na hranu spocita gradient do objemu realnych cisel s1,
	   do promenych min a max ulozi nejmensi a nejvetsi hodnotu gradientu,
	   WinHalfSize urcuje v jak velikem okne se bude pocitat gradient.
	*/
	ComputeAdaptiveGradient2D(s1,out,WinHalfSize,min,max);

	/* Vynuluje vystupni objem */
	for(z=0;z<depth;z++)
		for(y=0;y<height;y++)
			for(x=0;x<width;x++){
				out.SetVoxel(x,y,z,static_cast<VOXEL>(0));
			}

	/*Srazi velikost gradientu do intervalu 0..maxima elementarniho typu, vyjma minima elementarniho typu
	 (to znaci,ze voxel neni kandidatem na hranu)*/

		for(z=0;z<depth;z++)
			for(y=1;y<height-1;y++)
				for(x=1;x<width-1;x++){

					gradient=s1->GetVoxel(x,y,z);

					if(gradient==std::numeric_limits<float>::min())
						gradient=0.0;
					else
						gradient=std::numeric_limits<VOXEL>::max()*(gradient-min)/(max-min);

					out.SetVoxel(x,y,z,static_cast<VOXEL>(gradient));
				}

	delete s1;

	#if LEVEL >= 1
	cout<<"Leaving ApplyShenCastan2D"<<endl;
	#endif
}

template <class VOXEL> void ShenCastan2D(const Image3d<VOXEL> &in,Image3d<VOXEL> &out,double p,int WinHalfSize){
	if(p<=0 || p>1.0)
		throw InternalException("ShenCastan2D: invalid input value of p");
	if(WinHalfSize<1)
		throw InternalException("ShenCastan2D: WinHalfSize is lower than 1");

	out.CopyMetaData(in);

	Vector3d<float> resolution;
	Resolution			res;
	res=in.GetResolution();
	resolution=res.GetRes();

	if(resolution.x!=resolution.y || resolution.y!=resolution.z || resolution.x!=resolution.z)
		throw InternalException("ShenCastan2D: volume has not equal resolutions in all axises");

	#if LEVEL >= 1
	cout<<"Entering ShenCastan2D"<<endl;
	#endif

	ApplyShenCastan2D(in,out,p,WinHalfSize);

	#if LEVEL >= 1
	cout<<"Leaving ShenCastan2D"<<endl;
	#endif
}

template <> I3D_DLLEXPORT void ShenCastan2D(const Image3d<RGB> &in,Image3d<RGB> &out,double p,int WinHalfSize){
	Image3d<GRAY8>  inR,inG,inB,outR,outG,outB;

	if(p<=0 || p>1.0)
		throw InternalException("ShenCastan2D: invalid input value of p");
	if(WinHalfSize<1)
		throw InternalException("ShenCastan2D: WinHalfSize is lower than 1");

	out.CopyMetaData(in);

	Vector3d<float> resolution;
	Resolution			res;
	res=in.GetResolution();
	resolution=res.GetRes();

	if(resolution.x!=resolution.y || resolution.y!=resolution.z || resolution.x!=resolution.z)
		throw InternalException("ShenCastan2D: volume has not equal resolutions in all axises");

	#if LEVEL >= 1
	cout<<"Entering ShenCastan2D"<<endl;
	#endif

	inR.MakeRoom(in.GetSize());
	inG.MakeRoom(in.GetSize());
	inB.MakeRoom(in.GetSize());
	outR.MakeRoom(in.GetSize());
	outG.MakeRoom(in.GetSize());
	outB.MakeRoom(in.GetSize());

	RGBtoGray(in,inR,inG,inB);

	ApplyShenCastan2D(inR,outR,p,WinHalfSize);
	ApplyShenCastan2D(inG,outG,p,WinHalfSize);
	ApplyShenCastan2D(inB,outB,p,WinHalfSize);

	GrayToRGB(outR,outG,outB,out);

	#if LEVEL >= 1
	cout<<"Leaving ShenCastan2D"<<endl;
	#endif
}

template <> I3D_DLLEXPORT void ShenCastan2D(const Image3d<RGB16> &in,Image3d<RGB16> &out,double p,int WinHalfSize){
	Image3d<GRAY16> inR,inG,inB,outR,outG,outB;

	if(p<=0 || p>1.0)
		throw InternalException("ShenCastan2D: invalid input value of p");
	if(WinHalfSize<1)
		throw InternalException("ShenCastan2D: WinHalfSize is lower than 1");

	out.CopyMetaData(in);

	Vector3d<float> resolution;
	Resolution			res;
	res=in.GetResolution();
	resolution=res.GetRes();

	if(resolution.x!=resolution.y || resolution.y!=resolution.z || resolution.x!=resolution.z)
		throw InternalException("ShenCastan2D: volume has not equal resolutions in all axises");

	#if LEVEL >= 1
	cout<<"Entering ShenCastan2D"<<endl;
	#endif

	inR.MakeRoom(in.GetSize());
	inG.MakeRoom(in.GetSize());
	inB.MakeRoom(in.GetSize());
	outR.MakeRoom(in.GetSize());
	outG.MakeRoom(in.GetSize());
	outB.MakeRoom(in.GetSize());

	RGBtoGray(in,inR,inG,inB);

	ApplyShenCastan2D(inR,outR,p,WinHalfSize);
	ApplyShenCastan2D(inG,outG,p,WinHalfSize);
	ApplyShenCastan2D(inB,outB,p,WinHalfSize);

	GrayToRGB(outR,outG,outB,out);

	#if LEVEL >= 1
	cout<<"Leaving ShenCastan2D"<<endl;
	#endif
}

/* END   Shen-Castan2D edge detector */

/* BEGIN Marr-Hildreth3D edge detector */

/* Funkce vracejici hodnotu 1d Gausse normovaneho na jednicku
   vzhledem k integralu od minus nekonecna do plus nekonecna.
*/
static double Gauss1D(double x,double sigma){
	return (1.0/((sigma*sqrt(2.0*M_PI)))*exp(-(x*x)/(2.0*sigma*sigma)));
}

/*
	ZeroCross nastane kdyz nastane nektera z techto konfiguraci:

	11 33
	33 11
	13 31
	31 13

*/

/* Tato funkce spocita ZeroCross v bode (x,y,z), coz zde znamena, ze okolo
   zkoumaneho voxelu existuje nejaka dvojice voxelu, ktere maji symetrickou
   polohu ku zkoumanemu voxelu a jejich znamenka jsou odlisna (tedy v tomto
   voxelu se prechazi pres nulu).
*/
template <class VOXEL> 
static bool ZeroCross3D(const Image3d<VOXEL> &in,size_t x,size_t y,size_t z)
{
	int	i1,i2;

	i1=((in.Include(x+1,y+1,z+1))?in.GetVoxel(x+1,y+1,z+1):22) % 100;
	i2=((in.Include(static_cast<int>(x-1),static_cast<int>(y-1),static_cast<int>(z-1)))?in.GetVoxel(x-1,y-1,z-1):22) % 100;
	if((i1==11 && i2==33) || (i1==33 && i2==11) || (i1==13 && i2==31) || (i1==31 && i2==13))	return true;

	i1=((in.Include(x,y+1,z+1))?in.GetVoxel(x,y+1,z+1):22) % 100;
	i2=((in.Include(x,static_cast<int>(y-1),static_cast<int>(z-1)))?in.GetVoxel(x,y-1,z-1):22) % 100;
	if((i1==11 && i2==33) || (i1==33 && i2==11) || (i1==13 && i2==31) || (i1==31 && i2==13))	return true;

	i1=((in.Include(static_cast<int>(x-1),y+1,z+1))?in.GetVoxel(x-1,y+1,z+1):22) % 100;
	i2=((in.Include(x+1,static_cast<int>(y-1),static_cast<int>(z-1)))?in.GetVoxel(x+1,y-1,z-1):22) % 100;
	if((i1==11 && i2==33) || (i1==33 && i2==11) || (i1==13 && i2==31) || (i1==31 && i2==13))	return true;

	i1=((in.Include(x+1,y,z+1))?in.GetVoxel(x+1,y,z+1):22) % 100;
	i2=((in.Include(static_cast<int>(x-1),y,static_cast<int>(z-1)))?in.GetVoxel(x-1,y,z-1):22) % 100;
	if((i1==11 && i2==33) || (i1==33 && i2==11) || (i1==13 && i2==31) || (i1==31 && i2==13))	return true;

	i1=((in.Include(x,y,z+1))?in.GetVoxel(x,y,z+1):22) % 100;
	i2=((in.Include(x,y,static_cast<int>(z-1)))?in.GetVoxel(x,y,z-1):22) % 100;
	if((i1==11 && i2==33) || (i1==33 && i2==11) || (i1==13 && i2==31) || (i1==31 && i2==13))	return true;

	i1=((in.Include(static_cast<int>(x-1),y,z+1))?in.GetVoxel(x-1,y,z+1):22) % 100;
	i2=((in.Include(x+1,y,static_cast<int>(z-1)))?in.GetVoxel(x+1,y,z-1):22) % 100;
	if((i1==11 && i2==33) || (i1==33 && i2==11) || (i1==13 && i2==31) || (i1==31 && i2==13))	return true;

	i1=((in.Include(x+1,static_cast<int>(y-1),z+1))?in.GetVoxel(x+1,y-1,z+1):22) % 100;
	i2=((in.Include(static_cast<int>(x-1),y+1,static_cast<int>(z-1)))?in.GetVoxel(x-1,y+1,z-1):22) % 100;
	if((i1==11 && i2==33) || (i1==33 && i2==11) || (i1==13 && i2==31) || (i1==31 && i2==13))	return true;

	i1=((in.Include(x,static_cast<int>(y-1),z+1))?in.GetVoxel(x,y-1,z+1):22) % 100;
	i2=((in.Include(x,y+1,static_cast<int>(z-1)))?in.GetVoxel(x,y+1,z-1):22) % 100;
	if((i1==11 && i2==33) || (i1==33 && i2==11) || (i1==13 && i2==31) || (i1==31 && i2==13))	return true;

	i1=((in.Include(static_cast<int>(x-1),static_cast<int>(y-1),z+1))?in.GetVoxel(x-1,y-1,z+1):22) % 100;
	i2=((in.Include(x+1,y+1,static_cast<int>(z-1)))?in.GetVoxel(x+1,y+1,z-1):22) % 100;
	if((i1==11 && i2==33) || (i1==33 && i2==11) || (i1==13 && i2==31) || (i1==31 && i2==13))	return true;

	i1=((in.Include(x+1,y+1,z))?in.GetVoxel(x+1,y+1,z):22) % 100;
	i2=((in.Include(static_cast<int>(x-1),static_cast<int>(y-1),z))?in.GetVoxel(x-1,y-1,z):22) % 100;
	if((i1==11 && i2==33) || (i1==33 && i2==11) || (i1==13 && i2==31) || (i1==31 && i2==13))	return true;

	i1=((in.Include(x+1,static_cast<int>(y-1),z))?in.GetVoxel(x+1,y-1,z):22) % 100;
	i2=((in.Include(static_cast<int>(x-1),y+1,z))?in.GetVoxel(x-1,y+1,z):22) % 100;
	if((i1==11 && i2==33) || (i1==33 && i2==11) || (i1==13 && i2==31) || (i1==31 && i2==13))	return true;

	i1=((in.Include(x+1,y,z))?in.GetVoxel(x+1,y,z):22) % 100;
	i2=((in.Include(static_cast<int>(x-1),y,z))?in.GetVoxel(x-1,y,z):22) % 100;
	if((i1==11 && i2==33) || (i1==33 && i2==11) || (i1==13 && i2==31) || (i1==31 && i2==13))	return true;

	i1=((in.Include(x,y+1,z))?in.GetVoxel(x,y+1,z):22) % 100;
	i2=((in.Include(x,static_cast<int>(y-1),z))?in.GetVoxel(x,y-1,z):22) % 100;
	if((i1==11 && i2==33) || (i1==33 && i2==11) || (i1==13 && i2==31) || (i1==31 && i2==13))	return true;

	return false;
}

/* Tato funkce vytvori 1d filtr z funkce Gauss1D. Sirka filtru je
   (2*sigma*widthFactor)+1.
*/
static Filter<double>  MakeGauss1DFilter(double sigma,double widthFactor)
{

	Filter<double>	filtr;
	int							x,halfsize;
	double					sum=0;

	#if LEVEL >= 1
	cout<<"Entering MakeGauss1DFilter"<<endl;
	#endif

	halfsize=static_cast<int>(round(widthFactor*sigma));

	filtr.win=new double[(2*halfsize+1)];

	#if LEVEL >= 2
	cout<<"HalfSize of kernel is "<<halfsize<<" and sigma is "<<sigma<<"."<<endl;
	#endif

	filtr.size.x=2*halfsize+1;
	filtr.size.y=filtr.size.z=1;

	for(x=-halfsize;x<=halfsize;x++){
		filtr.win[x+halfsize]=Gauss1D(x,sigma);
		sum=sum+Abs(filtr.win[x+halfsize]);
	}

	for(x=-halfsize;x<=halfsize;x++){
		filtr.win[x+halfsize]=filtr.win[x+halfsize]/sum;
		#if LEVEL >= 2
		printf("%f ",filtr.win[x+halfsize]);
		#endif

	}

	#if LEVEL >= 2
	printf("\n");
	#endif

	#if LEVEL >= 1
	cout<<"Leaving MakeGauss1DFilter"<<endl;
	#endif

	return filtr;

}

/*
	Zakodovani vysledku Laplaceova operatoru:

	1 - hodnota < 0
	2 - hodnota = 0
	3 - hodnota > 0

	xy - filtr1 filtr2
*/

/* Aplikuje na vstup Gaussuv filtr a na vysledek aplikuje filtr typu Laplace,
   ktery zakoduje do vystupu podle predchozi tabulky, toto udela pro Gaussovy
   filtry filtr1 a filtr2.
*/
template <class VOXEL> 
static void ApplyLaplaceOnGauss3D(const Image3d<VOXEL> &in,Image3d<VOXEL> &out,Filter<double> &filtr1,Filter<double> &filtr2)
{
	Vector3d<float> resolution;
	Image3d<float>  *s1;
	Resolution      res;
	size_t          x,y,z,width,height,depth;
	double          suma1,suma2;
	VOXEL           suma=0,vystup;
	int             halfsize;

	#if LEVEL >= 1
	cout<<"Entering ApplyLaplaceOnGauss3D"<<endl;
	#endif

	res=in.GetResolution();
	resolution=res.GetRes();

	width=in.GetSizeX();
	height=in.GetSizeY();
	depth=in.GetSizeZ();

	s1=new Image3d<float>();
	s1->MakeRoom(in.GetSize());
	s1->SetResolution(in.GetResolution());

	halfsize=(filtr1.size.x-1)/2;

	/* Zkopiruje vstup do objemu realnych cisel */
	for(z=0;z<depth;z++)
		for(y=0;y<height;y++)
			for(x=0;x<width;x++)
				s1->SetVoxel(x,y,z,static_cast<float>(in.GetVoxel(x,y,z)));

	/* Aplikuje Gausse v ose x */
	ApplyFilter1D(s1,filtr1,0);

	/* Aplikuje Gausse v ose y */
	ApplyFilter1D(s1,filtr1,1);

	/* Aplikuje Gausse v ose z */
	ApplyFilter1D(s1,filtr1,2);

	for(z=halfsize;z<depth-halfsize;z++){
		#if LEVEL >= 3
		cout<<z<<endl;
		#endif
		for(y=halfsize;y<height-halfsize;y++)
			for(x=halfsize;x<width-halfsize;x++){

				suma1=-6*s1->GetVoxel(x,y,z);
				suma1+=((x+1)<width)?(s1->GetVoxel(x+1,y,z)):0;
				suma1+=((x>=1) && ((x-1)<width))?(s1->GetVoxel(x-1,y,z)):0;
				suma1+=((y+1)<height)?(s1->GetVoxel(x,y+1,z)):0;
				suma1+=((y>=1) && ((y-1)<height))?(s1->GetVoxel(x,y-1,z)):0;
				suma1+=((z+1)<depth)?(s1->GetVoxel(x,y,z+1)):0;
				suma1+=((z>=1) && ((z-1)<depth))?(s1->GetVoxel(x,y,z-1)):0;

				if(suma1 <  0)  suma=10;
				if(suma1 == 0)  suma=20;
				if(suma1 >  0)  suma=30;

				out.SetVoxel(x,y,z,static_cast<VOXEL>(suma));
			}
	}

	halfsize=(filtr2.size.x-1)/2;

	/* Zkopiruje vstup do objemu realnych cisel */
	for(z=0;z<depth;z++)
		for(y=0;y<height;y++)
			for(x=0;x<width;x++)
				s1->SetVoxel(x,y,z,static_cast<float>(in.GetVoxel(x,y,z)));

	/* Aplikuje Gausse v ose x */
	ApplyFilter1D(s1,filtr2,0);

	/* Aplikuje Gausse v ose y */
	ApplyFilter1D(s1,filtr2,1);

	/* Aplikuje Gausse v ose z */
	ApplyFilter1D(s1,filtr2,2);

	for(z=halfsize;z<depth-halfsize;z++){
		#if LEVEL >= 3
		cout<<z<<endl;
		#endif
		for(y=halfsize;y<height-halfsize;y++)
			for(x=halfsize;x<width-halfsize;x++){

				suma2=-6*s1->GetVoxel(x,y,z);
				suma2+=((x+1)<width)?(s1->GetVoxel(x+1,y,z)):0;
				suma2+=((x>=1) && ((x-1)<width))?(s1->GetVoxel(x-1,y,z)):0;
				suma2+=((y+1)<height)?(s1->GetVoxel(x,y+1,z)):0;
				suma2+=((y>=1) && ((y-1)<height))?(s1->GetVoxel(x,y-1,z)):0;
				suma2+=((z+1)<depth)?(s1->GetVoxel(x,y,z+1)):0;
				suma2+=((z>=1) && ((z-1)<depth))?(s1->GetVoxel(x,y,z-1)):0;

				if(suma2 <  0)  suma=1;
				if(suma2 == 0)  suma=2;
				if(suma2 >  0)  suma=3;

				vystup=out.GetVoxel(x,y,z);
				out.SetVoxel(x,y,z,static_cast<VOXEL>(suma+vystup));
			}
	}

	/* Tyto cykly vynuluji oblasti na hranici objemu ve kterych nelze dostat
	   korektni vysledky.
	*/
	for(z=0;z<(size_t)halfsize;z++)
		for(y=0;y<height;y++)
			for(x=0;x<width;x++){
				out.SetVoxel(x,y,z,static_cast<VOXEL>(0));
			}
	for(z=depth-(size_t)halfsize;z<depth;z++)
		for(y=0;y<height;y++)
			for(x=0;x<width;x++){
				out.SetVoxel(x,y,z,static_cast<VOXEL>(0));
			}
	for(y=0;y<(size_t)halfsize;y++)
		for(z=0;z<depth;z++)
			for(x=0;x<width;x++){
				out.SetVoxel(x,y,z,static_cast<VOXEL>(0));
			}
	for(y=height-(size_t)halfsize;y<height;y++)
		for(z=0;z<depth;z++)
			for(x=0;x<width;x++){
				out.SetVoxel(x,y,z,static_cast<VOXEL>(0));
			}
	for(x=0;x<(size_t)halfsize;x++)
		for(y=0;y<height;y++)
			for(z=0;z<depth;z++){
				out.SetVoxel(x,y,z,static_cast<VOXEL>(0));
			}
	for(x=width-(size_t)halfsize;x<width;x++)
		for(y=0;y<height;y++)
			for(z=0;z<depth;z++){
				out.SetVoxel(x,y,z,static_cast<VOXEL>(0));
			}

	#if LEVEL >= 1
	cout<<"Leaving ApplyLaplaceOnGauss3D"<<endl;
	#endif
}

/* Tato funkce aplikuje na vstupni obraz 'in' detektor hran Marr-Hildreth.
   Vytvori Gaussovy filtry filtr1 a filtr2 pro sigma1 a sigma2, ktere preda
   funkci ApplyLaplaceOnGauss3D, podle vystupu teto funkce urci pro kazdy voxel,
   zda v nem nastal ZeroCross (viz funkce ZeroCross). Nastane-li ZeroCross, tak
	 se takovy voxel oznaci za hranu (v tomto pripade se nastavi na maximalni
	 hodnotu voxelu).
*/
template <class VOXEL> 
static void ApplyMarrHildreth3D(const Image3d<VOXEL> &in,Image3d<VOXEL> &out,double sigma1,double sigma2)
{
	size_t			x,y,z;
	VOXEL				vstup;
	Filter<double>  filtr1,filtr2;
	double			widthFactor;
	int					halfsize;

	#if LEVEL >= 1
	cout<<"Entering ApplyMarrHildreth3D"<<endl;
	#endif

	widthFactor=4.0;

	filtr1=MakeGauss1DFilter(sigma1,widthFactor);
	filtr2=MakeGauss1DFilter(sigma2,widthFactor);

	halfsize=((filtr2.size.x-1)/2);

	ApplyLaplaceOnGauss3D(in,out,filtr1,filtr2);

	for(z=halfsize;z<in.GetSizeZ()-halfsize;z++)
		for(y=halfsize;y<in.GetSizeY()-halfsize;y++)
			for(x=halfsize;x<in.GetSizeX()-halfsize;x++){

				vstup=(out.Include(x,y,z))?out.GetVoxel(x,y,z):0;

				if(ZeroCross3D(out,x,y,z)){
					vstup=vstup+100;
				}

				out.SetVoxel(x,y,z,static_cast<VOXEL>(vstup));
			}

	for(z=halfsize;z<in.GetSizeZ()-halfsize;z++)
		for(y=halfsize;y<in.GetSizeY()-halfsize;y++)
			for(x=halfsize;x<in.GetSizeX()-halfsize;x++){

				vstup=(out.Include(x,y,z))?out.GetVoxel(x,y,z):0;

				if(vstup>=100)	vstup=std::numeric_limits<VOXEL>::max();
				else						vstup=std::numeric_limits<VOXEL>::min();

				out.SetVoxel(x,y,z,static_cast<VOXEL>(vstup));
			}

	#if LEVEL >= 1
	cout<<"Leaving ApplyMarrHildreth3D"<<endl;
	#endif
}

template <class VOXEL> void MarrHildreth3D(const Image3d<VOXEL> &in,Image3d<VOXEL> &out,double sigma1,double sigma2)
{
	double  s1,s2;

	if(sigma1<1.0 || sigma1>32.0)
		throw InternalException("MarrHildreth3D: invalid input value of sigma1");
	if(sigma2<1.0 || sigma2>32.0)
		throw InternalException("MarrHildreth3D: invalid input value of sigma2");

	out.CopyMetaData(in);

	Vector3d<float> resolution;
	Resolution			res;
	res=in.GetResolution();
	resolution=res.GetRes();

	if(resolution.x!=resolution.y || resolution.y!=resolution.z || resolution.x!=resolution.z)
		throw InternalException("MarrHildreth3D: volume has not equal resolutions in all axises");

	#if LEVEL >= 1
	cout<<"Entering MarrHildreth3D"<<endl;
	#endif

	if(sigma2>sigma1){
		s1=sigma1;
		s2=sigma2;
	}
	else{
		s1=sigma2;
		s2=sigma1;
	}

	ApplyMarrHildreth3D(in,out,s1,s2);

	#if LEVEL >= 1
	cout<<"Leaving MarrHildreth3D"<<endl;
	#endif
}

template <> I3D_DLLEXPORT void MarrHildreth3D(const Image3d<RGB> &in,Image3d<RGB> &out,double sigma1,double sigma2){
	Image3d<GRAY8>  inR,inG,inB,outR,outG,outB;
	double  s1,s2;

	if(sigma1<1.0 || sigma1>32.0)
		throw InternalException("MarrHildreth3D: invalid input value of sigma1");
	if(sigma2<1.0 || sigma2>32.0)
		throw InternalException("MarrHildreth3D: invalid input value of sigma2");

	out.CopyMetaData(in);

	Vector3d<float> resolution;
	Resolution			res;
	res=in.GetResolution();
	resolution=res.GetRes();

	if(resolution.x!=resolution.y || resolution.y!=resolution.z || resolution.x!=resolution.z)
		throw InternalException("MarrHildreth3D: volume has not equal resolutions in all axises");

	#if LEVEL >= 1
	cout<<"Entering MarrHildreth3D"<<endl;
	#endif

	inR.MakeRoom(in.GetSize());
	inG.MakeRoom(in.GetSize());
	inB.MakeRoom(in.GetSize());
	outR.MakeRoom(in.GetSize());
	outG.MakeRoom(in.GetSize());
	outB.MakeRoom(in.GetSize());

	RGBtoGray(in,inR,inG,inB);

	if(sigma2>sigma1){
		s1=sigma1;
		s2=sigma2;
	}
	else{
		s1=sigma2;
		s2=sigma1;
	}

	ApplyMarrHildreth3D(inR,outR,s1,s2);
	ApplyMarrHildreth3D(inG,outG,s1,s2);
	ApplyMarrHildreth3D(inB,outB,s1,s2);

	GrayToRGB(outR,outG,outB,out);

	#if LEVEL >= 1
	cout<<"Leaving MarrHildreth3D"<<endl;
	#endif
}

template <> void MarrHildreth3D(const Image3d<RGB16> &in,Image3d<RGB16> &out,double sigma1,double sigma2){
	Image3d<GRAY16> inR,inG,inB,outR,outG,outB;
	double  s1,s2;

	if(sigma1<1.0 || sigma1>32.0)
		throw InternalException("MarrHildreth3D: invalid input value of sigma1");
	if(sigma2<1.0 || sigma2>32.0)
		throw InternalException("MarrHildreth3D: invalid input value of sigma2");

	out.CopyMetaData(in);

	Vector3d<float> resolution;
	Resolution			res;
	res=in.GetResolution();
	resolution=res.GetRes();

	if(resolution.x!=resolution.y || resolution.y!=resolution.z || resolution.x!=resolution.z)
		throw InternalException("MarrHildreth3D: volume has not equal resolutions in all axises");

	#if LEVEL >= 1
	cout<<"Entering MarrHildreth3D"<<endl;
	#endif

	inR.MakeRoom(in.GetSize());
	inG.MakeRoom(in.GetSize());
	inB.MakeRoom(in.GetSize());
	outR.MakeRoom(in.GetSize());
	outG.MakeRoom(in.GetSize());
	outB.MakeRoom(in.GetSize());

	RGBtoGray(in,inR,inG,inB);

	if(sigma2>sigma1){
		s1=sigma1;
		s2=sigma2;
	}
	else{
		s1=sigma2;
		s2=sigma1;
	}

	ApplyMarrHildreth3D(inR,outR,s1,s2);
	ApplyMarrHildreth3D(inG,outG,s1,s2);
	ApplyMarrHildreth3D(inB,outB,s1,s2);

	GrayToRGB(outR,outG,outB,out);

	#if LEVEL >= 1
	cout<<"Leaving MarrHildreth3D"<<endl;
	#endif
}

/* END Marr-Hildreth3D edge detector */

/* BEGIN Marr-Hildreth2D edge detector */

/*
    ZeroCross nastane kdyz:
    
    11 33
    33 11
    13 31
    31 13

*/

/* Tato funkce spocita ZeroCross v bode (x,y,z), coz zde znamena, ze okolo
   zkoumaneho voxelu existuje nejaka dvojice voxelu, ktere maji symetrickou
   polohu ku zkoumanemu voxelu a jejich znamenka jsou odlisna (tedy v tomto
   voxelu se prechazi pres nulu).
*/
template <class VOXEL> 
static bool ZeroCross2D(const Image3d<VOXEL> &in,size_t x,size_t y,size_t z)
{
	int		i1,i2;

		i1=((in.Include(x+1,y+1,z))?in.GetVoxel(x+1,y+1,z):22) % 100;
		i2=((in.Include(static_cast<int>(x-1),static_cast<int>(y-1),z))?in.GetVoxel(x-1,y-1,z):22) % 100;
		if((i1==11 && i2==33) || (i1==33 && i2==11) || (i1==13 && i2==31) || (i1==31 && i2==13))	return true;

		i1=((in.Include(x+1,static_cast<int>(y-1),z))?in.GetVoxel(x+1,y-1,z):22) % 100;
		i2=((in.Include(static_cast<int>(x-1),y+1,z))?in.GetVoxel(x-1,y+1,z):22) % 100;
		if((i1==11 && i2==33) || (i1==33 && i2==11) || (i1==13 && i2==31) || (i1==31 && i2==13))	return true;

		i1=((in.Include(x+1,y,z))?in.GetVoxel(x+1,y,z):22) % 100;
		i2=((in.Include(static_cast<int>(x-1),y,z))?in.GetVoxel(x-1,y,z):22) % 100;
		if((i1==11 && i2==33) || (i1==33 && i2==11) || (i1==13 && i2==31) || (i1==31 && i2==13))	return true;

		i1=((in.Include(x,y+1,z))?in.GetVoxel(x,y+1,z):22) % 100;
		i2=((in.Include(x,static_cast<int>(y-1),z))?in.GetVoxel(x,y-1,z):22) % 100;
		if((i1==11 && i2==33) || (i1==33 && i2==11) || (i1==13 && i2==31) || (i1==31 && i2==13))	return true;

	return false;
}



/*
    Zakodovani vysledku Laplaceova operatoru:

    1 - hodnota < 0
    2 - hodnota = 0
    3 - hodnota > 0
    
    xy - filtr1 filtr2    
*/

/* Aplikuje na vstup Gaussuv filtr a na vysledek aplikuje filtr typu Laplace,
   ktery zakoduje do vystupu podle predchozi tabulky, toto udela pro Gaussovy
   filtry filtr1 a filtr2.
*/
template <class VOXEL> 
static void ApplyLaplaceOnGauss2D(const Image3d<VOXEL> &in,Image3d<VOXEL> &out,Filter<double> &filtr1,Filter<double> &filtr2)
{
	Vector3d<float> resolution;
	Image3d<float>  *s1;
	Resolution      res;
	double          suma1,suma2;
	VOXEL           suma=0,vystup,value;
	int             halfsize=0,width,height,depth,x,y,z;

#if LEVEL >= 1
	cout<<"Entering ApplyLaplaceOnGauss2D"<<endl;
#endif

	res=in.GetResolution();
	resolution=res.GetRes();

	width=in.GetSizeX();
	height=in.GetSizeY();
	depth=in.GetSizeZ();

	s1=new Image3d<float>();
	s1->MakeRoom(in.GetSize());
	s1->SetResolution(in.GetResolution());

	halfsize=(filtr1.size.x-1)/2;

	/* Zkopiruje vstup do objemu realnych cisel */
	for(z=0;z<depth;z++)
		for(y=0;y<height;y++)
			for(x=0;x<width;x++)
				s1->SetVoxel(x,y,z,static_cast<float>(in.GetVoxel(x,y,z)));



	/* Aplikuje Gausse v ose x */
	ApplyFilter1D(s1,filtr1,0);

	/* Aplikuje Gausse v ose y */
	ApplyFilter1D(s1,filtr1,1);


	for(z=0;z<depth;z++){
#if LEVEL >= 3
		cout<<z<<endl;
#endif	
		for(y=halfsize;y<height-halfsize;y++)
			for(x=halfsize;x<width-halfsize;x++){

						suma1=-4*s1->GetVoxel(x,y,z);
						suma1+=((x+1)<width)?(s1->GetVoxel(x+1,y,z)):0;
						suma1+=((x>=1) && ((x-1)<width))?(s1->GetVoxel(x-1,y,z)):0;
						suma1+=((y+1)<height)?(s1->GetVoxel(x,y+1,z)):0;
						suma1+=((y>=1) && ((y-1)<height))?(s1->GetVoxel(x,y-1,z)):0;
		
						if(suma1 <  0)	suma=10;
						if(suma1 == 0)	suma=20;
						if(suma1 >  0)	suma=30;

						out.SetVoxel(x,y,z,static_cast<VOXEL>(suma));
			}
	}

	halfsize=(filtr2.size.x-1)/2;

	/* Zkopiruje vstup do objemu realnych cisel */
	for(z=0;z<depth;z++)
		for(y=0;y<height;y++)
			for(x=0;x<width;x++)
				s1->SetVoxel(x,y,z,static_cast<float>(in.GetVoxel(x,y,z)));

	/*Aplikuje Gausse v ose x */
	ApplyFilter1D(s1,filtr2,0);

	/*Aplikuje Gausse v ose y */
	ApplyFilter1D(s1,filtr2,1);

	for(z=0;z<depth;z++){
#if LEVEL >= 3
		cout<<z<<endl;
#endif	
		for(y=halfsize;y<height-halfsize;y++)
			for(x=halfsize;x<width-halfsize;x++){

				suma2=-4*s1->GetVoxel(x,y,z);
				suma2+=((x+1)<width)?(s1->GetVoxel(x+1,y,z)):0;
				suma2+=((x>=1) && ((x-1)<width))?(s1->GetVoxel(x-1,y,z)):0;
				suma2+=((y+1)<height)?(s1->GetVoxel(x,y+1,z)):0;
				suma2+=((y>=1) && ((y-1)<height))?(s1->GetVoxel(x,y-1,z)):0;
		
				if(suma2 <  0)	suma=1;
				if(suma2 == 0)	suma=2;
				if(suma2 >  0)	suma=3;

				vystup=out.GetVoxel(x,y,z);
				out.SetVoxel(x,y,z,static_cast<VOXEL>(suma+vystup));
			}
	}


	/* Tyto cykly vynuluji oblasti na hranici objemu ve kterych nelze dostat
	   korektni vysledky.
	*/
	value=0;
	for(x=0;x<halfsize;x++)
		for(y=0;y<height;y++)
			for(z=0;z<depth;z++)
				out.SetVoxel(x,y,z,value);

	for(x=width-halfsize;x<width;x++)
		for(y=0;y<height;y++)
			for(z=0;z<depth;z++)
				out.SetVoxel(x,y,z,value);

	for(y=0;y<halfsize;y++)
		for(z=0;z<depth;z++)
			for(x=0;x<width;x++)
				out.SetVoxel(x,y,z,value);

	for(y=height-halfsize;y<height;y++)
		for(z=0;z<depth;z++)
			for(x=0;x<width;x++)
				out.SetVoxel(x,y,z,value);

#if LEVEL >= 1
		cout<<"Leaving ApplyLaplaceOnGauss2D"<<endl;
#endif
}


/* Tato funkce aplikuje na vstupni obraz 'in' detektor hran Marr-Hildreth.
   Vytvori Gaussovy filtry filtr1 a filtr2 pro sigma1 a sigma2, ktere preda
   funkci ApplyLaplaceOnGauss3D, podle vystupu teto funkce urci pro kazdy voxel,
   zda v nem nastal ZeroCross (viz funkce ZeroCross). Nastane-li ZeroCross, tak
	 se takovy voxel oznaci za hranu (v tomto pripade se nastavi na maximalni
	 hodnotu voxelu).
*/
template <class VOXEL> 
static void ApplyMarrHildreth2D(const Image3d<VOXEL> &in,Image3d<VOXEL> &out,double sigma1,double sigma2){
	Filter<double> filtr1,filtr2;
	double         widthFactor;
	size_t         x,y,z,width,height,depth;
	VOXEL          vstup;
	int            halfsize;

#if LEVEL >= 1
    cout<<"Entering ApplyMarrHildreth2D"<<endl;
#endif

	widthFactor=4.0;

	filtr1=MakeGauss1DFilter(sigma1,widthFactor);
	filtr2=MakeGauss1DFilter(sigma2,widthFactor);
    
	halfsize=((filtr2.size.x-1)/2);
	width=in.GetSizeX();
	height=in.GetSizeY();
	depth=in.GetSizeZ();
	
	ApplyLaplaceOnGauss2D(in,out,filtr1,filtr2);
    
	for(z=0;z<depth;z++)
		for(y=halfsize;y<height-halfsize;y++)
			for(x=halfsize;x<width-halfsize;x++){
				vstup=(out.Include(x,y,z))?out.GetVoxel(x,y,z):0;
				if(ZeroCross2D(out,x,y,z))
					vstup=vstup+100;
				out.SetVoxel(x,y,z,static_cast<VOXEL>(vstup));
			}


	for(z=0;z<in.GetSizeZ();z++)
		for(y=halfsize;y<in.GetSizeY()-halfsize;y++)
			for(x=halfsize;x<in.GetSizeX()-halfsize;x++){
				vstup=(out.Include(x,y,z))?out.GetVoxel(x,y,z):0;
				if(vstup>=100)
					vstup=std::numeric_limits<VOXEL>::max();
				else
					vstup=std::numeric_limits<VOXEL>::min();
				out.SetVoxel(x,y,z,vstup);
	    }

#if LEVEL >= 1
    cout<<"Leaving ApplyMarrHildreth2D"<<endl;
#endif
}

template <class VOXEL> void MarrHildreth2D(const Image3d<VOXEL> &in,Image3d<VOXEL> &out,double sigma1,double sigma2){
	Vector3d<float> resolution;
	Resolution      res;
	double          s1,s2;

	if(sigma1<1.0 || sigma1>32.0)
		throw InternalException("MarrHildreth2D: invalid input value of sigma1");
	if(sigma2<1.0 || sigma2>32.0)
		throw InternalException("MarrHildreth2D: invalid input value of sigma2");

	out.CopyMetaData(in);

	res=in.GetResolution();
	resolution=res.GetRes();

	if(resolution.x!=resolution.y || resolution.y!=resolution.z || resolution.x!=resolution.z)
		throw InternalException("MarrHildreth2D: volume has not equal resolutions in all axises");

#if LEVEL >= 1
	cout<<"Entering MarrHildreth2D"<<endl;
#endif

	if(sigma2>sigma1){
		s1=sigma1;
		s2=sigma2;
	}
	else{
		s1=sigma2;
		s2=sigma1;
	}

	ApplyMarrHildreth2D(in,out,s1,s2);
    
#if LEVEL >= 1
	cout<<"Leaving MarrHildreth2D"<<endl;
#endif
}

template <> I3D_DLLEXPORT void MarrHildreth2D(const Image3d<RGB> &in,Image3d<RGB> &out,double sigma1,double sigma2){
	Vector3d<float> resolution;
	Image3d<GRAY8>  inR,inG,inB,outR,outG,outB;
	Resolution      res;
	double          s1,s2;

	if(sigma1<1.0 || sigma1>32.0)
		throw InternalException("MarrHildreth2D: invalid input value of sigma1");
	if(sigma2<1.0 || sigma2>32.0)
		throw InternalException("MarrHildreth2D: invalid input value of sigma2");

	out.CopyMetaData(in);

	res=in.GetResolution();
	resolution=res.GetRes();

	if(resolution.x!=resolution.y || resolution.y!=resolution.z || resolution.x!=resolution.z)
		throw InternalException("MarrHildreth2D: volume has not equal resolutions in all axises");

#if LEVEL >= 1
	cout<<"Entering MarrHildreth2D"<<endl;
#endif

	inR.MakeRoom(in.GetSize());
	inG.MakeRoom(in.GetSize());
	inB.MakeRoom(in.GetSize());
	outR.MakeRoom(in.GetSize());
	outG.MakeRoom(in.GetSize());
	outB.MakeRoom(in.GetSize());

	RGBtoGray(in,inR,inG,inB);

	if(sigma2>sigma1){
		s1=sigma1;
		s2=sigma2;
	}
	else{
		s1=sigma2;
		s2=sigma1;
	}
    
	ApplyMarrHildreth2D(inR,outR,s1,s2);
	ApplyMarrHildreth2D(inG,outG,s1,s2);
	ApplyMarrHildreth2D(inB,outB,s1,s2);

	GrayToRGB(outR,outG,outB,out);

#if LEVEL >= 1
	cout<<"Leaving MarrHildreth2D"<<endl;
#endif
}

template <> I3D_DLLEXPORT void MarrHildreth2D(const Image3d<RGB16> &in,Image3d<RGB16> &out,double sigma1,double sigma2){
	Vector3d<float> resolution;
	Image3d<GRAY16> inR,inG,inB,outR,outG,outB;
	Resolution      res;
	double          s1,s2;

	if(sigma1<1.0 || sigma1>32.0)
		throw InternalException("MarrHildreth2D: invalid input value of sigma1");
	if(sigma2<1.0 || sigma2>32.0)
		throw InternalException("MarrHildreth2D: invalid input value of sigma2");

	out.CopyMetaData(in);

	res=in.GetResolution();
	resolution=res.GetRes();

	if(resolution.x!=resolution.y || resolution.y!=resolution.z || resolution.x!=resolution.z)
		throw InternalException("MarrHildreth2D: volume has not equal resolutions in all axises");

#if LEVEL >= 1
	cout<<"Entering MarrHildreth2D"<<endl;
#endif

	inR.MakeRoom(in.GetSize());
	inG.MakeRoom(in.GetSize());
	inB.MakeRoom(in.GetSize());
	outR.MakeRoom(in.GetSize());
	outG.MakeRoom(in.GetSize());
	outB.MakeRoom(in.GetSize());

	RGBtoGray(in,inR,inG,inB);

	if(sigma2>sigma1){
		s1=sigma1;
		s2=sigma2;
	}
	else{
		s1=sigma2;
		s2=sigma1;
	}

	ApplyMarrHildreth2D(inR,outR,s1,s2);
	ApplyMarrHildreth2D(inG,outG,s1,s2);
	ApplyMarrHildreth2D(inB,outB,s1,s2);

	GrayToRGB(outR,outG,outB,out);

#if LEVEL >= 1
	cout<<"Leaving MarrHildreth2D"<<endl;
#endif
}


/* END Marr-Hildreth2D edge detector */

/* BEGIN Laplace3D edge detector */

/* Tato funkce aplikuje 3d Laplaceuv filtr velikosti 3x3x3 na vstupni obraz
   'in'.
*/
template <class VOXEL> 
static void ApplyLaplace3D(const Image3d<VOXEL> &in,Image3d<VOXEL> &out)
{
	size_t          x,y,z,width,height,depth;
	int             suma;

	#if LEVEL >= 1
	cout<<"Entering ApplyLaplace3D"<<endl;
	#endif

	width=in.GetSizeX();
	height=in.GetSizeY();
	depth=in.GetSizeZ();

	for(z=1;z<depth-1;z++){
		#if LEVEL >= 3
		cout<<z<<endl;
		#endif
		for(y=1;y<height-1;y++)
			for(x=1;x<width-1;x++){

			suma=static_cast<int>(-6*in.GetVoxel(x,y,z)
				+in.GetVoxel(x+1,y,z)
				+in.GetVoxel(x-1,y,z)
				+in.GetVoxel(x,y+1,z)
				+in.GetVoxel(x,y-1,z)
				+in.GetVoxel(x,y,z+1)
				+in.GetVoxel(x,y,z-1));

			if(suma<0)
				suma=0;
			if(suma>std::numeric_limits<VOXEL>::max())
				suma=static_cast<int>(std::numeric_limits<VOXEL>::max());

			out.SetVoxel(x,y,z,static_cast<VOXEL>(suma));
		}
	}

	#if LEVEL >= 1
	cout<<"Leaving ApplyLaplace3D"<<endl;
	#endif
}

template <class VOXEL> void Laplace3D(const Image3d<VOXEL> &in,Image3d<VOXEL> &out){
	out.CopyMetaData(in);

	#if LEVEL >= 1
	cout<<"Entering Laplace3D"<<endl;
	#endif

	ApplyLaplace3D(in,out);

	#if LEVEL >= 1
	cout<<"Leaving Laplace3D"<<endl;
	#endif
}

template <> I3D_DLLEXPORT void Laplace3D(const Image3d<RGB> &in,Image3d<RGB> &out){
	Image3d<GRAY8>  inR,inG,inB,outR,outG,outB;

	out.CopyMetaData(in);

	#if LEVEL >= 1
	cout<<"Entering Laplace3D"<<endl;
	#endif

	inR.MakeRoom(in.GetSize());
	inG.MakeRoom(in.GetSize());
	inB.MakeRoom(in.GetSize());
	outR.MakeRoom(in.GetSize());
	outG.MakeRoom(in.GetSize());
	outB.MakeRoom(in.GetSize());

	RGBtoGray(in,inR,inG,inB);

	ApplyLaplace3D(inR,outR);
	ApplyLaplace3D(inG,outG);
	ApplyLaplace3D(inB,outB);

	GrayToRGB(outR,outG,outB,out);

	#if LEVEL >= 1
	cout<<"Leaving Laplace3D"<<endl;
	#endif
}

template <> I3D_DLLEXPORT void Laplace3D(const Image3d<RGB16> &in,Image3d<RGB16> &out){
	Image3d<GRAY16> inR,inG,inB,outR,outG,outB;

	out.CopyMetaData(in);

	#if LEVEL >= 1
	cout<<"Entering Laplace3D"<<endl;
	#endif

	inR.MakeRoom(in.GetSize());
	inG.MakeRoom(in.GetSize());
	inB.MakeRoom(in.GetSize());
	outR.MakeRoom(in.GetSize());
	outG.MakeRoom(in.GetSize());
	outB.MakeRoom(in.GetSize());

	RGBtoGray(in,inR,inG,inB);

	ApplyLaplace3D(inR,outR);
	ApplyLaplace3D(inG,outG);
	ApplyLaplace3D(inB,outB);

	GrayToRGB(outR,outG,outB,out);

	#if LEVEL >= 1
	cout<<"Leaving Laplace3D"<<endl;
	#endif
}

/* END Laplace3D edge detector */

/* BEGIN Roberts3D edge detector */

/* Tato funkce aplikuje 3d Robertsuv filtr na vstupni obraz 'in'.
*/
template <class VOXEL> 
static void ApplyRoberts3D(const Image3d<VOXEL> &in,Image3d<VOXEL> &out)
{
	size_t          x,y,z,width,height,depth;
	int             suma;

	#if LEVEL >= 1
	cout<<"Entering ApplyRoberts3D"<<endl;
	#endif

	width=in.GetSizeX();
	height=in.GetSizeY();
	depth=in.GetSizeZ();

	for(z=0;z<depth-1;z++){
		#if LEVEL >= 3
		cout<<z<<endl;
		#endif
		for(y=0;y<height-1;y++)
			for(x=0;x<width-1;x++){

			suma=static_cast<int>(round(sqrt(static_cast<double>(Sqr(((in.Include(x,y,z))?in.GetVoxel(x,y,z):0)
				-((in.Include(x+1,y+1,z+1))?AGetVoxel(in,x+1,y+1,z+1):0))+
				Sqr(((in.Include(x,y+1,z))?AGetVoxel(in,x,y+1,z):0)
				-((in.Include(x+1,y,z+1))?AGetVoxel(in,x+1,y,z+1):0))+
				Sqr(((in.Include(x+1,y,z))?AGetVoxel(in,x+1,y,z):0)
				-((in.Include(x,y+1,z+1))?AGetVoxel(in,x,y+1,z+1):0))+
				Sqr(((in.Include(x+1,y+1,z))?AGetVoxel(in,x+1,y+1,z):0)
				-((in.Include(x,y,z+1))?AGetVoxel(in,x,y,z+1):0))))));

			if(suma<0)
				suma=0;
			if(suma>std::numeric_limits<VOXEL>::max())
				suma=static_cast<int>(std::numeric_limits<VOXEL>::max());

			out.SetVoxel(x,y,z,static_cast<VOXEL>(suma));
		}
	}

	#if LEVEL >= 1
	cout<<"Leaving ApplyRoberts3D"<<endl;
	#endif
}

template <class VOXEL> void Roberts3D(const Image3d<VOXEL> &in,Image3d<VOXEL> &out){
	out.CopyMetaData(in);

	#if LEVEL >= 1
	cout<<"Entering Roberts3D"<<endl;
	#endif

	ApplyRoberts3D(in,out);

	#if LEVEL >= 1
	cout<<"Leaving Roberts3D"<<endl;
	#endif
}

template <> I3D_DLLEXPORT void Roberts3D(const Image3d<RGB> &in,Image3d<RGB> &out){
	Image3d<GRAY8>  inR,inG,inB,outR,outG,outB;

	out.CopyMetaData(in);

	#if LEVEL >= 1
	cout<<"Entering Roberts3D"<<endl;
	#endif

	inR.MakeRoom(in.GetSize());
	inG.MakeRoom(in.GetSize());
	inB.MakeRoom(in.GetSize());
	outR.MakeRoom(in.GetSize());
	outG.MakeRoom(in.GetSize());
	outB.MakeRoom(in.GetSize());

	RGBtoGray(in,inR,inG,inB);

	ApplyRoberts3D(inR,outR);
	ApplyRoberts3D(inG,outG);
	ApplyRoberts3D(inB,outB);

	GrayToRGB(outR,outG,outB,out);

	#if LEVEL >= 1
	cout<<"Leaving Roberts3D"<<endl;
	#endif
}

template <> I3D_DLLEXPORT void Roberts3D(const Image3d<RGB16> &in,Image3d<RGB16> &out){
	Image3d<GRAY16> inR,inG,inB,outR,outG,outB;

	out.CopyMetaData(in);

	#if LEVEL >= 1
	cout<<"Entering Roberts3D"<<endl;
	#endif

	inR.MakeRoom(in.GetSize());
	inG.MakeRoom(in.GetSize());
	inB.MakeRoom(in.GetSize());
	outR.MakeRoom(in.GetSize());
	outG.MakeRoom(in.GetSize());
	outB.MakeRoom(in.GetSize());

	RGBtoGray(in,inR,inG,inB);

	ApplyRoberts3D(inR,outR);
	ApplyRoberts3D(inG,outG);
	ApplyRoberts3D(inB,outB);

	GrayToRGB(outR,outG,outB,out);

	#if LEVEL >= 1
	cout<<"Leaving Roberts3D"<<endl;
	#endif
}

/* END Roberts3D edge detector */

/* BEGIN AnisotropicRoberts3D edge detector */

/* Tato funkce aplikuje 3d anisotropni Robertsuv filtr na vstupni obraz 'in'.
*/
template <class VOXEL> 
static void ApplyAnisotropicRoberts3D(const Image3d<VOXEL> &in,Image3d<VOXEL> &out)
{
	Vector3d<float> relative_resolution;
	size_t          x,y,z,width,height,depth;
	int             suma;

	#if LEVEL >= 1
	cout<<"Entering ApplyRoberts3D"<<endl;
	#endif

	relative_resolution=get_relative_resolution(in);
	width=in.GetSizeX();
	height=in.GetSizeY();
	depth=in.GetSizeZ();

	for(z=0;z<depth-1;z++){
		#if LEVEL >= 3
		cout<<z<<endl;
		#endif
		for(y=0;y<height-1;y++)
			for(x=0;x<width-1;x++){

			suma=static_cast<int>(round(sqrt(static_cast<double>(Sqr(((in.Include(x,y,z))?in.GetVoxel(x,y,z):0)
				-((in.Include(x+1,y+1,z+1))?AGetVoxel(in,x+relative_resolution.x,y+relative_resolution.y,z+relative_resolution.z):0))+
				Sqr(((in.Include(x,y+1,z))?AGetVoxel(in,x,y+relative_resolution.y,z):0)
				-((in.Include(x+1,y,z+1))?AGetVoxel(in,x+relative_resolution.x,y,z+relative_resolution.z):0))+
				Sqr(((in.Include(x+1,y,z))?AGetVoxel(in,x+relative_resolution.x,y,z):0)
				-((in.Include(x,y+1,z+1))?AGetVoxel(in,x,y+relative_resolution.y,z+relative_resolution.z):0))+
				Sqr(((in.Include(x+1,y+1,z))?AGetVoxel(in,x+relative_resolution.x,y+relative_resolution.y,z):0)
				-((in.Include(x,y,z+1))?AGetVoxel(in,x,y,z+relative_resolution.z):0))))));

			if(suma<0)                 suma=0;
			if(suma>std::numeric_limits<VOXEL>::max()) suma=static_cast<int>(std::numeric_limits<VOXEL>::max());

			out.SetVoxel(x,y,z,static_cast<VOXEL>(suma));
		}
	}

	#if LEVEL >= 1
	cout<<"Leaving ApplyRoberts3D"<<endl;
	#endif
}

template <class VOXEL> 
void AnisotropicRoberts3D(const Image3d<VOXEL> &in,Image3d<VOXEL> &out)
{
	out.CopyMetaData(in);

	#if LEVEL >= 1
	cout<<"Entering Roberts3D"<<endl;
	#endif

	ApplyAnisotropicRoberts3D(in,out);

	#if LEVEL >= 1
	cout<<"Leaving Roberts3D"<<endl;
	#endif
}

template <> I3D_DLLEXPORT 
void AnisotropicRoberts3D(const Image3d<RGB> &in,Image3d<RGB> &out){
	Image3d<GRAY8>  inR,inG,inB,outR,outG,outB;

	out.CopyMetaData(in);

	#if LEVEL >= 1
	cout<<"Entering Roberts3D"<<endl;
	#endif

	inR.MakeRoom(in.GetSize());
	inG.MakeRoom(in.GetSize());
	inB.MakeRoom(in.GetSize());
	outR.MakeRoom(in.GetSize());
	outG.MakeRoom(in.GetSize());
	outB.MakeRoom(in.GetSize());

	RGBtoGray(in,inR,inG,inB);

	ApplyAnisotropicRoberts3D(inR,outR);
	ApplyAnisotropicRoberts3D(inG,outG);
	ApplyAnisotropicRoberts3D(inB,outB);

	GrayToRGB(outR,outG,outB,out);

	#if LEVEL >= 1
	cout<<"Leaving Roberts3D"<<endl;
	#endif
}

template <> I3D_DLLEXPORT 
void AnisotropicRoberts3D(const Image3d<RGB16> &in,Image3d<RGB16> &out){
	Image3d<GRAY16> inR,inG,inB,outR,outG,outB;

	out.CopyMetaData(in);

	#if LEVEL >= 1
	cout<<"Entering Roberts3D"<<endl;
	#endif

	inR.MakeRoom(in.GetSize());
	inG.MakeRoom(in.GetSize());
	inB.MakeRoom(in.GetSize());
	outR.MakeRoom(in.GetSize());
	outG.MakeRoom(in.GetSize());
	outB.MakeRoom(in.GetSize());

	RGBtoGray(in,inR,inG,inB);

	ApplyAnisotropicRoberts3D(inR,outR);
	ApplyAnisotropicRoberts3D(inG,outG);
	ApplyAnisotropicRoberts3D(inB,outB);

	GrayToRGB(outR,outG,outB,out);

	#if LEVEL >= 1
	cout<<"Leaving Roberts3D"<<endl;
	#endif
}

/* END AnisotropicRoberts3D edge detector */

/* BEGIN EstimateGradient1_3D edge detector */

/* Tato funkce aplikuje 3d filtr odhadu gradientu na vstupni obraz 'in'.
*/
template <class VOXEL> 
static void ApplyEstimateGradient1_3D(const Image3d<VOXEL> &in,Image3d<VOXEL> &out,bool clamp)
{
	size_t          x,y,z,width,height,depth;
	VOXEL           xyz;
	int             suma;

	#if LEVEL >= 1
	cout<<"Entering ApplyEstimateGradient1_3D"<<endl;
	#endif

	width=in.GetSizeX();
	height=in.GetSizeY();
	depth=in.GetSizeZ();

	if(clamp){
		for(z=0;z<depth;z++){
			#if LEVEL >= 3
			cout<<z<<endl;
			#endif
			for(y=0;y<height;y++)
				for(x=0;x<width;x++){

				xyz=((in.Include(x,y,z))?in.GetVoxel(x,y,z):static_cast<VOXEL>(0));

				suma=static_cast<int>(round(sqrt(static_cast<double>(Sqr(xyz-((in.Include(static_cast<int>(x-1),y,z))?in.GetVoxel(x-1,y,z):xyz))+
					Sqr(xyz-((in.Include(x,static_cast<int>(y-1),z))?in.GetVoxel(x,y-1,z):xyz))+
					Sqr(xyz-((in.Include(x,y,static_cast<int>(z-1)))?in.GetVoxel(x,y,z-1):xyz))))));

				if(suma>std::numeric_limits<VOXEL>::max())
					suma=std::numeric_limits<VOXEL>::max();

				out.SetVoxel(x,y,z,static_cast<VOXEL>(suma));
			}
		}
	}
	else{
		for(z=1;z<depth;z++){
			#if LEVEL >= 3
			cout<<z<<endl;
			#endif
			for(y=1;y<height;y++)
				for(x=1;x<width;x++){

				xyz=((in.Include(x,y,z))?in.GetVoxel(x,y,z):static_cast<BINARY>(0));

				suma=static_cast<int>(round(sqrt(static_cast<double>(Sqr(xyz-((in.Include(static_cast<int>(x-1),y,z))?in.GetVoxel(x-1,y,z):0))+
					Sqr(xyz-((in.Include(x,static_cast<int>(y-1),z))?in.GetVoxel(x,y-1,z):0))+
					Sqr(xyz-((in.Include(x,y,static_cast<int>(z-1)))?in.GetVoxel(x,y,z-1):0))))));

				if(suma>std::numeric_limits<VOXEL>::max())
					suma=std::numeric_limits<VOXEL>::max();

				out.SetVoxel(x,y,z,static_cast<VOXEL>(suma));
			}
		}
	}

	#if LEVEL >= 1
	cout<<"Leaving ApplyEstimateGradient1_3D"<<endl;
	#endif
}

template <class VOXEL> void EstimateGradient1_3D(const Image3d<VOXEL> &in,Image3d<VOXEL> &out,bool clamp){
	out.CopyMetaData(in);

	#if LEVEL >= 1
	cout<<"Entering EstimateGradient1_3D"<<endl;
	#endif

	ApplyEstimateGradient1_3D(in,out,clamp);

	#if LEVEL >= 1
	cout<<"Leaving EstimateGradient1_3D"<<endl;
	#endif
}

/* Tato funkce aplikuje 3d filtr odhadu gradientu na vstupni obraz 'in'.
   Hodnoty intenzit obrazu 'in' jsou realna cisla. Bere se ohled na
   vzdalenost mezi voxely.
*/
template <> I3D_DLLEXPORT void EstimateGradient1_3D(const Image3d<float> &in,Image3d<float> &out,bool clamp){
	size_t      x,y,z,width,height,depth;
	float       suma;
	float       xyz;
	Vector3d<float> resolution,distance;
	Resolution  r;

	out.CopyMetaData(in);

	#if LEVEL >= 1
	cout<<"Entering ApplyEstimateGradient1_3D"<<endl;
	#endif

	r=in.GetResolution();
	resolution=r.GetRes();
	distance.x=1.0/resolution.x;
	distance.y=1.0/resolution.y;
	distance.z=1.0/resolution.z;

	width=in.GetSizeX();
	height=in.GetSizeY();
	depth=in.GetSizeZ();

	#if LEVEL >= 2
	cout<<"Distance of voxels in axis x: "<<distance.x<<endl;
	cout<<"Distance of voxels in axis y: "<<distance.y<<endl;
	cout<<"Distance of voxels in axis z: "<<distance.z<<endl;
	#endif

	if(clamp){
		for(z=0;z<depth;z++){
			#if LEVEL >= 3
			cout<<z<<endl;
			#endif
			for(y=0;y<height;y++)
				for(x=0;x<width;x++){

				xyz=((in.Include(x,y,z))?in.GetVoxel(x,y,z):0);

				suma=static_cast<float>(sqrt(static_cast<double>(Sqr((xyz-((in.Include(static_cast<int>(x-1),y,z))?in.GetVoxel(x-1,y,z):xyz))/distance.x)+
					Sqr((xyz-((in.Include(x,static_cast<int>(y-1),z))?in.GetVoxel(x,y-1,z):xyz))/distance.y)+
					Sqr((xyz-((in.Include(x,y,static_cast<int>(z-1)))?in.GetVoxel(x,y,z-1):xyz))/distance.z))));

				if(suma>std::numeric_limits<float>::max()) suma=std::numeric_limits<float>::max();

				out.SetVoxel(x,y,z,suma);
			}
		}
	}
	else{
		for(z=1;z<depth;z++){
			#if LEVEL >= 3
			cout<<z<<endl;
			#endif
			for(y=1;y<height;y++)
				for(x=1;x<width;x++){

				xyz=((in.Include(x,y,z))?in.GetVoxel(x,y,z):0);

				suma=static_cast<float>(sqrt(static_cast<double>(Sqr((xyz-((in.Include(static_cast<int>(x-1),y,z))?in.GetVoxel(x-1,y,z):0))/distance.x)+
					Sqr((xyz-((in.Include(x,static_cast<int>(y-1),z))?in.GetVoxel(x,y-1,z):0))/distance.y)+
					Sqr((xyz-((in.Include(x,y,static_cast<int>(z-1)))?in.GetVoxel(x,y,z-1):0))/distance.z))));

				if(suma>std::numeric_limits<float>::max()) suma=std::numeric_limits<float>::max();

				out.SetVoxel(x,y,z,suma);
			}
		}
	}

	#if LEVEL >= 1
	cout<<"Leaving ApplyEstimateGradient1_3D"<<endl;
	#endif
}

template <> I3D_DLLEXPORT void EstimateGradient1_3D(const Image3d<RGB> &in,Image3d<RGB> &out,bool clamp){
	Image3d<GRAY8>  inR,inG,inB,outR,outG,outB;

	out.CopyMetaData(in);

	#if LEVEL >= 1
	cout<<"Entering EstimateGradient1_3D"<<endl;
	#endif

	inR.MakeRoom(in.GetSize());
	inG.MakeRoom(in.GetSize());
	inB.MakeRoom(in.GetSize());
	outR.MakeRoom(in.GetSize());
	outG.MakeRoom(in.GetSize());
	outB.MakeRoom(in.GetSize());

	RGBtoGray(in,inR,inG,inB);

	ApplyEstimateGradient1_3D(inR,outR,clamp);
	ApplyEstimateGradient1_3D(inG,outG,clamp);
	ApplyEstimateGradient1_3D(inB,outB,clamp);

	GrayToRGB(outR,outG,outB,out);

	#if LEVEL >= 1
	cout<<"Leaving EstimateGradient1_3D"<<endl;
	#endif
}

template <> I3D_DLLEXPORT void EstimateGradient1_3D(const Image3d<RGB16> &in,Image3d<RGB16> &out,bool clamp){
	Image3d<GRAY16> inR,inG,inB,outR,outG,outB;

	out.CopyMetaData(in);

	#if LEVEL >= 1
	cout<<"Entering EstimateGradient1_3D"<<endl;
	#endif

	inR.MakeRoom(in.GetSize());
	inG.MakeRoom(in.GetSize());
	inB.MakeRoom(in.GetSize());
	outR.MakeRoom(in.GetSize());
	outG.MakeRoom(in.GetSize());
	outB.MakeRoom(in.GetSize());

	RGBtoGray(in,inR,inG,inB);

	ApplyEstimateGradient1_3D(inR,outR,clamp);
	ApplyEstimateGradient1_3D(inG,outG,clamp);
	ApplyEstimateGradient1_3D(inB,outB,clamp);

	GrayToRGB(outR,outG,outB,out);

	#if LEVEL >= 1
	cout<<"Leaving EstimateGradient1_3D"<<endl;
	#endif
}

/* END EstimateGradient1_3D edge detector */

/* BEGIN EstimateGradient2_3D edge detector */

/* Tato funkce aplikuje 3d filtr odhadu gradientu na vstupni obraz 'in'.
*/
template <class VOXEL> 
static void ApplyEstimateGradient2_3D(const Image3d<VOXEL> &in,Image3d<VOXEL> &out,bool clamp)
{
	size_t          x,y,z;
	int             suma;

	#if LEVEL >= 1
	cout<<"Entering ApplyEstimateGradient2_3D"<<endl;
	#endif

	if(clamp){
		for(z=0;z<in.GetSizeZ();z++){
			#if LEVEL >= 3
			cout<<z<<endl;
			#endif
			for(y=0;y<in.GetSizeY();y++)
				for(x=0;x<in.GetSizeX();x++){

				suma=static_cast<int>(round(sqrt(static_cast<double>(Sqr(((in.Include(x+1,y,z))?in.GetVoxel(x+1,y,z):in.GetVoxel(x,y,z))
					-((in.Include(static_cast<int>(x-1),y,z))?in.GetVoxel(x-1,y,z):in.GetVoxel(x,y,z)))+
					Sqr(((in.Include(x,y+1,z))?in.GetVoxel(x,y+1,z):in.GetVoxel(x,y,z))
					-((in.Include(x,static_cast<int>(y-1),z))?in.GetVoxel(x,y-1,z):in.GetVoxel(x,y,z)))+
					Sqr(((in.Include(x,y,z+1))?in.GetVoxel(x,y,z+1):in.GetVoxel(x,y,z))
					-((in.Include(x,y,static_cast<int>(z-1)))?in.GetVoxel(x,y,z-1):in.GetVoxel(x,y,z)))))));

				if(suma>std::numeric_limits<VOXEL>::max()) suma=std::numeric_limits<VOXEL>::max();

				out.SetVoxel(x,y,z,static_cast<VOXEL>(suma));
			}
		}
	}
	else{
		for(z=1;z<in.GetSizeZ()-1;z++){
			#if LEVEL >= 3
			cout<<z<<endl;
			#endif
			for(y=1;y<in.GetSizeY()-1;y++)
				for(x=1;x<in.GetSizeX()-1;x++){

				suma=static_cast<int>(round(sqrt(static_cast<double>(Sqr(((in.Include(x+1,y,z))?in.GetVoxel(x+1,y,z):0)
					-((in.Include(static_cast<int>(x-1),y,z))?in.GetVoxel(x-1,y,z):0))+
					Sqr(((in.Include(x,y+1,z))?in.GetVoxel(x,y+1,z):0)
					-((in.Include(x,static_cast<int>(y-1),z))?in.GetVoxel(x,y-1,z):0))+
					Sqr(((in.Include(x,y,z+1))?in.GetVoxel(x,y,z+1):0)
					-((in.Include(x,y,static_cast<int>(z-1)))?in.GetVoxel(x,y,z-1):0))))));

				if(suma>std::numeric_limits<VOXEL>::max()) suma=std::numeric_limits<VOXEL>::max();

				out.SetVoxel(x,y,z,static_cast<VOXEL>(suma));
			}
		}
	}

	#if LEVEL >= 1
	cout<<"Leaving ApplyEstimateGradient2_3D"<<endl;
	#endif
}

template <class VOXEL> void EstimateGradient2_3D(const Image3d<VOXEL> &in,Image3d<VOXEL> &out,bool clamp){
	out.CopyMetaData(in);

	#if LEVEL >= 1
	cout<<"Entering EstimateGradient2_3D"<<endl;
	#endif

	ApplyEstimateGradient2_3D(in,out,clamp);

	#if LEVEL >= 1
	cout<<"Leaving EstimateGradient2_3D"<<endl;
	#endif
}

/* Tato funkce aplikuje 3d filtr odhadu gradientu na vstupni obraz 'in'.
   Hodnoty intenzit obrazu 'in' jsou realna cisla. Bere se ohled na
   vzdalenost mezi voxely.
*/
template <> 
void ApplyEstimateGradient2_3D(const Image3d<float> &in,Image3d<float> &out,bool clamp)
{
	size_t          x,y,z;
	float           suma;
	Vector3d<float> resolution,distance;
	Resolution      r;

	#if LEVEL >= 1
	cout<<"Entering ApplyEstimateGradient2_3D"<<endl;
	#endif

	r=in.GetResolution();
	resolution=r.GetRes();
	distance.x=1.0/resolution.x;
	distance.y=1.0/resolution.y;
	distance.z=1.0/resolution.z;

	#if LEVEL >= 2
	cout<<"Distance of voxels in axis x: "<<distance.x<<endl;
	cout<<"Distance of voxels in axis y: "<<distance.y<<endl;
	cout<<"Distance of voxels in axis z: "<<distance.z<<endl;
	#endif

	if(clamp){
		for(z=0;z<in.GetSizeZ();z++){
			#if LEVEL >= 3
			cout<<z<<endl;
			#endif
			for(y=0;y<in.GetSizeY();y++)
				for(x=0;x<in.GetSizeX();x++){

				suma=static_cast<float>(sqrt(static_cast<double>(Sqr((((in.Include(x+1,y,z))?in.GetVoxel(x+1,y,z):in.GetVoxel(x,y,z))
					-((in.Include(static_cast<int>(x-1),y,z))?in.GetVoxel(x-1,y,z):in.GetVoxel(x,y,z)))/(2*distance.x))+
					Sqr((((in.Include(x,y+1,z))?in.GetVoxel(x,y+1,z):in.GetVoxel(x,y,z))
					-((in.Include(x,static_cast<int>(y-1),z))?in.GetVoxel(x,y-1,z):in.GetVoxel(x,y,z)))/(2*distance.y))+
					Sqr((((in.Include(x,y,z+1))?in.GetVoxel(x,y,z+1):in.GetVoxel(x,y,z))
					-((in.Include(x,y,static_cast<int>(z-1)))?in.GetVoxel(x,y,z-1):in.GetVoxel(x,y,z)))/(2*distance.z)))));

				out.SetVoxel(x,y,z,suma);
			}
		}
	}
	else{
		for(z=1;z<in.GetSizeZ()-1;z++){
			#if LEVEL >= 3
			cout<<z<<endl;
			#endif
			for(y=1;y<in.GetSizeY()-1;y++)
				for(x=1;x<in.GetSizeX()-1;x++){

				suma=static_cast<float>(sqrt(static_cast<double>(Sqr((((in.Include(x+1,y,z))?in.GetVoxel(x+1,y,z):0)
					-((in.Include(static_cast<int>(x-1),y,z))?in.GetVoxel(x-1,y,z):0))/(2*distance.x))+
					Sqr((((in.Include(x,y+1,z))?in.GetVoxel(x,y+1,z):0)
					-((in.Include(x,static_cast<int>(y-1),z))?in.GetVoxel(x,y-1,z):0))/(2*distance.y))+
					Sqr((((in.Include(x,y,z+1))?in.GetVoxel(x,y,z+1):0)
					-((in.Include(x,y,static_cast<int>(z-1)))?in.GetVoxel(x,y,z-1):0))/(2*distance.z)))));

				out.SetVoxel(x,y,z,suma);
			}
		}
	}

	#if LEVEL >= 1
	cout<<"Leaving ApplyEstimateGradient2_3D"<<endl;
	#endif
}

template <> I3D_DLLEXPORT void EstimateGradient2_3D(const Image3d<RGB> &in,Image3d<RGB> &out,bool clamp){
	Image3d<GRAY8>  inR,inG,inB,outR,outG,outB;

	out.CopyMetaData(in);

	#if LEVEL >= 1
	cout<<"Entering EstimateGradient2_3D"<<endl;
	#endif

	inR.MakeRoom(in.GetSize());
	inG.MakeRoom(in.GetSize());
	inB.MakeRoom(in.GetSize());
	outR.MakeRoom(in.GetSize());
	outG.MakeRoom(in.GetSize());
	outB.MakeRoom(in.GetSize());

	RGBtoGray(in,inR,inG,inB);

	ApplyEstimateGradient2_3D(inR,outR,clamp);
	ApplyEstimateGradient2_3D(inG,outG,clamp);
	ApplyEstimateGradient2_3D(inB,outB,clamp);

	GrayToRGB(outR,outG,outB,out);

	#if LEVEL >= 1
	cout<<"Leaving EstimateGradient2_3D"<<endl;
	#endif
}

template <> I3D_DLLEXPORT void EstimateGradient2_3D(const Image3d<RGB16> &in,Image3d<RGB16> &out,bool clamp){
	Image3d<GRAY16> inR,inG,inB,outR,outG,outB;

	out.CopyMetaData(in);

	#if LEVEL >= 1
	cout<<"Entering EstimateGradient2_3D"<<endl;
	#endif

	inR.MakeRoom(in.GetSize());
	inG.MakeRoom(in.GetSize());
	inB.MakeRoom(in.GetSize());
	outR.MakeRoom(in.GetSize());
	outG.MakeRoom(in.GetSize());
	outB.MakeRoom(in.GetSize());

	RGBtoGray(in,inR,inG,inB);

	ApplyEstimateGradient2_3D(inR,outR,clamp);
	ApplyEstimateGradient2_3D(inG,outG,clamp);
	ApplyEstimateGradient2_3D(inB,outB,clamp);

	GrayToRGB(outR,outG,outB,out);

	#if LEVEL >= 1
	cout<<"Leaving EstimateGradient2_3D"<<endl;
	#endif
}

/* END EstimateGradient2_3D edge detector */

/* BEGIN Sobel3D edge detector */

static int XX[3][3][3]={{{1,2,1},{2,4,2},{1,2,1}},{{0,0,0},{0,0,0},{0,0,0}},{{-1,-2,-1},{-2,-4,-2},{-1,-2,-1}}};
static int YY[3][3][3]={{{-1,0,1},{-2,0,2},{-1,0,1}},{{-2,0,2},{-4,0,4},{-2,0,2}},{{-1,0,1},{-2,0,2},{-1,0,1}}};
static int ZZ[3][3][3]={{{1,2,1},{0,0,0},{-1,-2,-1}},{{2,4,2},{0,0,0},{-2,-4,-2}},{{1,2,1},{0,0,0},{-1,-2,-1}}};

/* Tato funkce aplikuje 3d Sobeluv filtr velikosti 3x3x3 na vstupni obraz 'in'.
*/
template <class VOXEL> 
static void ApplySobel3D(const Image3d<VOXEL> &in,Image3d<VOXEL> &out)
{
	size_t          x,y,z;
	float           sumaX=0,sumaY=0,sumaZ=0,vstup;
	int             xx,yy,zz,suma;

	#if LEVEL >= 1
	cout<<"Entering ApplySobel3D"<<endl;
	#endif

	for(z=1;z<in.GetSizeZ()-1;z++){
		#if LEVEL >= 3
		cout<<z<<endl;
		#endif
		for(y=1;y<in.GetSizeY()-1;y++)
			for(x=1;x<in.GetSizeX()-1;x++){

			sumaX=0;
			sumaY=0;
			sumaZ=0;

			for(zz=0;zz<3;zz++)
				for(yy=0;yy<3;yy++)
					for(xx=0;xx<3;xx++){
						vstup=in.GetVoxel(x+xx-1,y+yy-1,z+zz-1);
						sumaX+=vstup*XX[xx][yy][zz];
						sumaY+=vstup*YY[xx][yy][zz];
						sumaZ+=vstup*ZZ[xx][yy][zz];
					}

			sumaX/=16.0;
			sumaY/=16.0;
			sumaZ/=16.0;

			suma=static_cast<int>(round(sqrt(sumaX*sumaX+sumaY*sumaY+sumaZ*sumaZ)));

			if(suma<0)
				suma=0;
			if(suma>std::numeric_limits<VOXEL>::max())
				suma=static_cast<int>(std::numeric_limits<VOXEL>::max());

			out.SetVoxel(x,y,z,static_cast<VOXEL>(suma));
		}
	}

	#if LEVEL >= 1
	cout<<"Leaving ApplySobel3D"<<endl;
	#endif
}

template <class VOXEL> void Sobel3D(const Image3d<VOXEL> &in,Image3d<VOXEL> &out){
	out.CopyMetaData(in);

	#if LEVEL >= 1
	cout<<"Entering Sobel3D"<<endl;
	#endif

	ApplySobel3D(in,out);

	#if LEVEL >= 1
	cout<<"Leaving Sobel3D"<<endl;
	#endif
}

template <> I3D_DLLEXPORT void Sobel3D(const Image3d<RGB> &in,Image3d<RGB> &out){
	Image3d<GRAY8>  inR,inG,inB,outR,outG,outB;

	out.CopyMetaData(in);

	#if LEVEL >= 1
	cout<<"Entering Sobel3D"<<endl;
	#endif

	inR.MakeRoom(in.GetSize());
	inG.MakeRoom(in.GetSize());
	inB.MakeRoom(in.GetSize());
	outR.MakeRoom(in.GetSize());
	outG.MakeRoom(in.GetSize());
	outB.MakeRoom(in.GetSize());

	RGBtoGray(in,inR,inG,inB);

	ApplySobel3D(inR,outR);
	ApplySobel3D(inG,outG);
	ApplySobel3D(inB,outB);

	GrayToRGB(outR,outG,outB,out);

	#if LEVEL >= 1
	cout<<"Leaving Sobel3D"<<endl;
	#endif
}

template <> I3D_DLLEXPORT void Sobel3D(const Image3d<RGB16> &in,Image3d<RGB16> &out){
	Image3d<GRAY16> inR,inG,inB,outR,outG,outB;

	out.CopyMetaData(in);

	#if LEVEL >= 1
	cout<<"Entering Sobel3D"<<endl;
	#endif

	inR.MakeRoom(in.GetSize());
	inG.MakeRoom(in.GetSize());
	inB.MakeRoom(in.GetSize());
	outR.MakeRoom(in.GetSize());
	outG.MakeRoom(in.GetSize());
	outB.MakeRoom(in.GetSize());

	RGBtoGray(in,inR,inG,inB);

	ApplySobel3D(inR,outR);
	ApplySobel3D(inG,outG);
	ApplySobel3D(inB,outB);

	GrayToRGB(outR,outG,outB,out);

	#if LEVEL >= 1
	cout<<"Leaving Sobel3D"<<endl;
	#endif
}

/* END Sobel3D edge detector */

/* BEGIN AnisoSobel3D edge detector */

/* Tato funkce aplikuje 3d anisotropni verzi Sobelova filtru na vstupni obraz
   'in'.
*/
template <class VOXEL> 
static void ApplyAnisoSobel3D(const Image3d<VOXEL> &in,Image3d<VOXEL> &out){
	Vector3d<float> relative_resolution;
	size_t          x,y,z;
	float           sumaX=0,sumaY=0,sumaZ=0,vstup;
	int             xx,yy,zz,suma;

	#if LEVEL >= 1
	cout<<"Entering ApplyAnisoSobel3D"<<endl;
	#endif

	relative_resolution=get_relative_resolution(in);

	for(z=1;z<in.GetSizeZ()-1;z++){
		#if LEVEL >= 3
		cout<<z<<endl;
		#endif
		for(y=1;y<in.GetSizeY()-1;y++)
			for(x=1;x<in.GetSizeX()-1;x++){

			sumaX=0;
			sumaY=0;
			sumaZ=0;

			for(zz=0;zz<3;zz++)
				for(yy=0;yy<3;yy++)
					for(xx=0;xx<3;xx++){
						vstup=AGetVoxel(in,x+(xx-1.0)*relative_resolution.x,y+(yy-1.0)*relative_resolution.y,z+(zz-1.0)*relative_resolution.z);
						sumaX+=vstup*XX[xx][yy][zz];
						sumaY+=vstup*YY[xx][yy][zz];
						sumaZ+=vstup*ZZ[xx][yy][zz];
					}

			sumaX/=16.0;
			sumaY/=16.0;
			sumaZ/=16.0;

			suma=static_cast<int>(round(sqrt(sumaX*sumaX+sumaY*sumaY+sumaZ*sumaZ)));

			if(suma<0)                 suma=0;
			if(suma>std::numeric_limits<VOXEL>::max()) suma=static_cast<int>(std::numeric_limits<VOXEL>::max());

			out.SetVoxel(x,y,z,static_cast<VOXEL>(suma));
		}
	}

	#if LEVEL >= 1
	cout<<"Leaving ApplyAnisoSobel3D"<<endl;
	#endif
}

template <class VOXEL> 
void AnisoSobel3D(const Image3d<VOXEL> &in,Image3d<VOXEL> &out){
	out.CopyMetaData(in);

	#if LEVEL >= 1
	cout<<"Entering AnisoSobel3D"<<endl;
	#endif

	ApplyAnisoSobel3D(in,out);

	#if LEVEL >= 1
	cout<<"Leaving AnisoSobel3D"<<endl;
	#endif
}

template <> I3D_DLLEXPORT 
void AnisoSobel3D(const Image3d<RGB> &in,Image3d<RGB> &out){
	Image3d<GRAY8>  inR,inG,inB,outR,outG,outB;

	out.CopyMetaData(in);

	#if LEVEL >= 1
	cout<<"Entering AnisoSobel3D"<<endl;
	#endif

	inR.MakeRoom(in.GetSize());
	inG.MakeRoom(in.GetSize());
	inB.MakeRoom(in.GetSize());
	outR.MakeRoom(in.GetSize());
	outG.MakeRoom(in.GetSize());
	outB.MakeRoom(in.GetSize());

	RGBtoGray(in,inR,inG,inB);

	ApplySobel3D(inR,outR);
	ApplySobel3D(inG,outG);
	ApplySobel3D(inB,outB);

	GrayToRGB(outR,outG,outB,out);

	#if LEVEL >= 1
	cout<<"Leaving AnisoSobel3D"<<endl;
	#endif
}

template <> I3D_DLLEXPORT 
void AnisoSobel3D(const Image3d<RGB16> &in,Image3d<RGB16> &out){
	Image3d<GRAY16> inR,inG,inB,outR,outG,outB;

	out.CopyMetaData(in);

	#if LEVEL >= 1
	cout<<"Entering AnisoSobel3D"<<endl;
	#endif

	inR.MakeRoom(in.GetSize());
	inG.MakeRoom(in.GetSize());
	inB.MakeRoom(in.GetSize());
	outR.MakeRoom(in.GetSize());
	outG.MakeRoom(in.GetSize());
	outB.MakeRoom(in.GetSize());

	RGBtoGray(in,inR,inG,inB);

	ApplyAnisoSobel3D(inR,outR);
	ApplyAnisoSobel3D(inG,outG);
	ApplyAnisoSobel3D(inB,outB);

	GrayToRGB(outR,outG,outB,out);

	#if LEVEL >= 1
	cout<<"Leaving AnisoSobel3D"<<endl;
	#endif
}

/* END AnisoSobel3D edge detector */

/* BEGIN Canny3D edge detector */

/* Tato funkce vrati hodnotu derivace Gausse v bode x pro 'sigma'.
*/
static float dG (float x,float sigma){
	float vystup;

	vystup=-0.25*sqrt(2.0)*x*exp(-0.5*x*x/(sigma*sigma))/(sqrt(M_PI)*sigma*sigma*sigma);

	return vystup;
}

/* Vytvori 1d filtr derivace Gausse.
*/
static Filter<double> MakeDiffGauss1DFilter(double sigma,double widthFactor){
	Filter<double>  pom;
	double          sum=0;
	int             i,halfsize;

	#if LEVEL >= 1
	cout<<"Entering makeDG"<<endl;
	#endif

	halfsize=static_cast<int>(round(sigma*widthFactor));
	pom.win=new double[2*halfsize+1];
	pom.size.x=2*halfsize+1;
	pom.size.y=pom.size.z=1;

	for(i=-halfsize;i<=halfsize;i++){
		pom.win[i+halfsize]=dG(i,sigma);
	}

	for(i=0;i<=halfsize;i++){
		sum=sum+Abs(pom.win[i]);
	}

	for(i=0;i<(2*halfsize+1);i++){
		pom.win[i]=pom.win[i]/sum;
	}

	#if LEVEL >= 1
	cout<<"Leaving makeDG"<<endl;
	#endif

	return pom;
}

/* Tato funkce spocita gradienty v objemu zadaneho pomoci parametru 'offset' a
   'size'. Jako parametry ma filtry 'diff_gauss_filter' a 'gauss_filter'.
	 Jestlize je ukazatel na vystup nulovy, tak si funkce alokuje sama obraz pro
   vystup. Jestlize nulovy neni, tak jej nealokuje.
   Parametr 'offset' a soucet 'offset+size' predpokladame, ze se nachazi uvnitr
   objemu.
*/
template <class VOXEL> void Gradient3D(Filter<double> diff_gauss_filter,Filter<double> gauss_filter,
									   const Image3d<VOXEL> &vstup,Vector3d<int> offset,
									   Vector3d<int> size,VectField3d<float> *&vystup)
{
	Vector3d<float> gradient;
	Image3d<float>  *gaussed,*temporary;
	Vector3d<int>   extended_size,head,tail;
	int             diff_gauss_filter_length,gauss_filter_length,x,y,z,width,height,depth,halfsize;

	if(vystup==0){
		vystup=new VectField3d<float>();
	}
	vystup->MakeRoom(size);

	width=vstup.GetSizeX();
	height=vstup.GetSizeY();
	depth=vstup.GetSizeZ();

	diff_gauss_filter_length=diff_gauss_filter.size.x;
	halfsize=(diff_gauss_filter.size.x-1)/2;
	gauss_filter_length=gauss_filter.size.x;

	if(diff_gauss_filter_length!=gauss_filter_length){
		std::cout<<"diff_gauss_filter_length = "<<diff_gauss_filter_length<<std::endl;
		std::cout<<"gauss_filter_length = "<<gauss_filter_length<<std::endl;
		throw InternalException("Canny3D-gradient3D: filters doesn't have same length");
	}
	

	if((offset.x+size.x+halfsize)>width) tail.x=width-offset.x-size.x;
	else                                 tail.x=halfsize;

	if((offset.x-halfsize)<0) head.x=offset.x;
	else                      head.x=halfsize;

	if((offset.y+size.y+halfsize)>height) tail.y=height-offset.y-size.y;
	else                                  tail.y=halfsize;

	if((offset.y-halfsize)<0) head.y=offset.y;
	else                      head.y=halfsize;

	if((offset.z+size.z+halfsize)>depth) tail.z=depth-offset.z-size.z;
	else                                 tail.z=halfsize;

	if((offset.z-halfsize)<0) head.z=offset.z;
	else                      head.z=halfsize;

	extended_size=size+head+tail;
/*
	cout<<"halfsize = "<<halfsize<<endl;
	cout<<"size = "<<width<<" "<<height<<" "<<depth<<endl;
	cout<<"extended size = "<<extended_size.x<<" "<<extended_size.y<<" "<<extended_size.z<<endl;
	cout<<"offset = "<<offset.x<<" "<<offset.y<<" "<<offset.z<<endl;
	cout<<"head = "<<head.x<<" "<<head.y<<" "<<head.z<<endl;
	cout<<"tail = "<<tail.x<<" "<<tail.y<<" "<<tail.z<<endl;
*/
	gaussed=new Image3d<float>();
	gaussed->MakeRoom(extended_size);

	temporary=new Image3d<float>();
	temporary->MakeRoom(extended_size);

	for(x=0;x<(head.x+size.x+tail.x);x++)
		for(y=0;y<(head.y+size.y+tail.y);y++)
			for(z=0;z<(head.z+size.z+tail.z);z++){
				temporary->SetVoxel(x,y,z,vstup.GetVoxel(offset.x+x-head.x,offset.y+y-head.y,offset.z+z-head.z));
			}


	gradient=Vector3d<float>(0.0,0.0,0.0);

	ApplyFilter1D2<float,float>(*temporary,gaussed,gauss_filter,1);
	ApplyFilter1D2<float,float>(*gaussed,temporary,gauss_filter,2);
	ApplyFilter1D2<float,float>(*temporary,gaussed,diff_gauss_filter,0);

	for(x=0;x<size.x;x++)
		for(y=0;y<size.y;y++)
			for(z=0;z<size.z;z++){
				gradient.x=gaussed->GetVoxel(head.x+x,head.y+y,head.z+z);
				vystup->SetVoxel(x,y,z,gradient);
			}

	for(x=0;x<(head.x+size.x+tail.x);x++)
		for(y=0;y<(head.y+size.y+tail.y);y++)
			for(z=0;z<(head.z+size.z+tail.z);z++){
				temporary->SetVoxel(x,y,z,vstup.GetVoxel(offset.x+x-head.x,offset.y+y-head.y,offset.z+z-head.z));
			}

	ApplyFilter1D2<float,float>(*temporary,gaussed,gauss_filter,0);
	ApplyFilter1D2<float,float>(*gaussed,temporary,gauss_filter,2);
	ApplyFilter1D2<float,float>(*temporary,gaussed,diff_gauss_filter,1);

	for(x=0;x<size.x;x++)
		for(y=0;y<size.y;y++)
			for(z=0;z<size.z;z++){
				gradient=vystup->GetVoxel(x,y,z);
				gradient.y=gaussed->GetVoxel(head.x+x,head.y+y,head.z+z);
				vystup->SetVoxel(x,y,z,gradient);
			}

	for(x=0;x<(head.x+size.x+tail.x);x++)
		for(y=0;y<(head.y+size.y+tail.y);y++)
			for(z=0;z<(head.z+size.z+tail.z);z++){
				temporary->SetVoxel(x,y,z,vstup.GetVoxel(offset.x+x-head.x,offset.y+y-head.y,offset.z+z-head.z));
			}

	ApplyFilter1D2<float,float>(*temporary,gaussed,gauss_filter,0);
	ApplyFilter1D2<float,float>(*gaussed,temporary,gauss_filter,1);
	ApplyFilter1D2<float,float>(*temporary,gaussed,diff_gauss_filter,2);

	for(x=0;x<size.x;x++)
		for(y=0;y<size.y;y++)
			for(z=0;z<size.z;z++){
				gradient=vystup->GetVoxel(x,y,z);
				gradient.z=gaussed->GetVoxel(head.x+x,head.y+y,head.z+z);
				vystup->SetVoxel(x,y,z,gradient);
			}

	delete gaussed;
	delete temporary;
}

/* Spocita velikost gradientu, ktery se nachazi v miste, ktere protne vektor
   gradientu na stene pomyslne kostky, ktera se nachazi okolo bodu (x,y,z).
   Parametr axis nam rika, kterou stenu gradient protne
	 (0 - yz, 1 - xz, 2 - xy). V promenne 'gradients' jsou ulozene gradienty.
*/
static float InterpolateGradient(int axis,Vector3d<float> gradient,int x,int y,int z,VectField3d<float> *gradients){
	Vector3d<float> g[2][2],f[2],h;
	float           gradientMagnitude,value_1=0.0,value_2=0.0;
	int             s;

	if(axis==0){
		if(gradient.x>0.0) s=x+1;
		else               s=x-1;
		if(gradient.y>=0 && gradient.z>=0){
			g[0][0]=gradients->GetVoxel(s,y,z);
			g[0][1]=gradients->GetVoxel(s,y,z+1);
			g[1][0]=gradients->GetVoxel(s,y+1,z);
			g[1][1]=gradients->GetVoxel(s,y+1,z+1);
		}
		else
		if(gradient.y<0 && gradient.z>=0){
			g[0][0]=gradients->GetVoxel(s,y,z);
			g[0][1]=gradients->GetVoxel(s,y,z+1);
			g[1][0]=gradients->GetVoxel(s,y-1,z);
			g[1][1]=gradients->GetVoxel(s,y-1,z+1);
		}
		else
		if(gradient.y>=0 && gradient.z<0){
			g[0][0]=gradients->GetVoxel(s,y,z);
			g[0][1]=gradients->GetVoxel(s,y,z-1);
			g[1][0]=gradients->GetVoxel(s,y+1,z);
			g[1][1]=gradients->GetVoxel(s,y+1,z-1);
		}                 
		else              
		if(gradient.y<0 && gradient.z<0){
			g[0][0]=gradients->GetVoxel(s,y,z);
			g[0][1]=gradients->GetVoxel(s,y,z-1);
			g[1][0]=gradients->GetVoxel(s,y-1,z);
			g[1][1]=gradients->GetVoxel(s,y-1,z-1);
		}
		value_1=Abs(gradient.y);
		value_2=Abs(gradient.z);
	}

	if(axis==1){
		if(gradient.y>0.0) s=y+1;
		else               s=y-1;
		if(gradient.x>=0 && gradient.z>=0){
			g[0][0]=gradients->GetVoxel(x,s,z);
			g[0][1]=gradients->GetVoxel(x,s,z+1);
			g[1][0]=gradients->GetVoxel(x+1,s,z);
			g[1][1]=gradients->GetVoxel(x+1,s,z+1);
		}
		else
		if(gradient.x<0 && gradient.z>=0){
			g[0][0]=gradients->GetVoxel(x,s,z);
			g[0][1]=gradients->GetVoxel(x,s,z+1);
			g[1][0]=gradients->GetVoxel(x-1,s,z);
			g[1][1]=gradients->GetVoxel(x-1,s,z+1);
		}
		else
		if(gradient.x>=0 && gradient.z<0){
			g[0][0]=gradients->GetVoxel(x,s,z);
			g[0][1]=gradients->GetVoxel(x,s,z-1);
			g[1][0]=gradients->GetVoxel(x+1,s,z);
			g[1][1]=gradients->GetVoxel(x+1,s,z-1);
		}                 
		else              
		if(gradient.x<0 && gradient.z<0){
			g[0][0]=gradients->GetVoxel(x,s,z);
			g[0][1]=gradients->GetVoxel(x,s,z-1);
			g[1][0]=gradients->GetVoxel(x-1,s,z);
			g[1][1]=gradients->GetVoxel(x-1,s,z-1);
		}
		value_1=Abs(gradient.x);
		value_2=Abs(gradient.z);
	}

	if(axis==2){
		if(gradient.z>0.0) s=z+1;
		else               s=z-1;
		if(gradient.x>=0 && gradient.y>=0){
			g[0][0]=gradients->GetVoxel(x,y,s);
			g[0][1]=gradients->GetVoxel(x,y+1,s);
			g[1][0]=gradients->GetVoxel(x+1,y,s);
			g[1][1]=gradients->GetVoxel(x+1,y+1,s);
		}
		else
		if(gradient.x<0 && gradient.y>=0){
			g[0][0]=gradients->GetVoxel(x,y,s);
			g[0][1]=gradients->GetVoxel(x,y+1,s);
			g[1][0]=gradients->GetVoxel(x-1,y,s);
			g[1][1]=gradients->GetVoxel(x-1,y+1,s);
		}
		else
		if(gradient.x>=0 && gradient.y<0){
			g[0][0]=gradients->GetVoxel(x,y,s);
			g[0][1]=gradients->GetVoxel(x,y-1,s);
			g[1][0]=gradients->GetVoxel(x+1,y,s);
			g[1][1]=gradients->GetVoxel(x+1,y-1,s);
		}                 
		else
		if(gradient.x<0 && gradient.y<0){
			g[0][0]=gradients->GetVoxel(x,y,s);
			g[0][1]=gradients->GetVoxel(x,y-1,s);
			g[1][0]=gradients->GetVoxel(x-1,y,s);
			g[1][1]=gradients->GetVoxel(x-1,y-1,s);
		}
		value_1=Abs(gradient.x);
		value_2=Abs(gradient.y);
	}

	f[0]=g[0][0]+(g[1][0]-g[0][0])*value_1;
	f[1]=g[0][1]+(g[1][1]-g[0][1])*value_1;
	h=f[0]+(f[1]-f[0])*value_2;

	gradientMagnitude=Norm(h);

	return gradientMagnitude;
}

/* Tato funkce aplikuje na vstupni obraz 'gradients' postup zvany
   Non-Max-Suppression. Ten odstrani z obrazu ty body, ktere nejsou maximalni ve
   smeru gradientu. Je-li napriklad velikost gradientu v bode (x,y,z) 5.0 a ve
   smeru gradientu se nachazeji gradietny o velikosti 6.0 a 7.0, tak bude bod
   (x,y,z) odstranen (bude mu prirazena hodnota 0).

   POZOR nepocita nonmaxsuppression pro nejkrajnejsi voxely, ani nema z ceho, je
	 potreba na to pamatovat pri volani teto funkce. Typicky se totiz pouzije jen
   na cast skutecne zpracovavaneho obrazu, kvuli uspore pameti. Pak musime delat
   presahy.
*/
template <class VOXEL> 
static void NonMaxSuppression3D(Image3d<VOXEL> &vystup,VectField3d<float> *&gradients,Vector3d<int> offset,Vector3d<int> size){
	Vector3d<float> gradient;
	VOXEL           result;
	float           maximum,gradient_size,gm[2];
	int             x,y,z;

	for(x=1;x<(size.x-1);x++)
		for(y=1;y<(size.y-1);y++)
			for(z=1;z<(size.z-1);z++){
				gradient=gradients->GetVoxel(x,y,z);
				gradient_size=Norm(gradient);

				maximum=Max(Abs(gradient.x),Abs(gradient.y),Abs(gradient.z));

				gradient/=maximum;

				if(Abs(gradient.x)==1.0){
					gm[0]=InterpolateGradient(0,gradient,x,y,z,gradients);
					gm[1]=InterpolateGradient(0,-gradient,x,y,z,gradients);
				}
				else
				if(Abs(gradient.y)==1.0){
					gm[0]=InterpolateGradient(1,gradient,x,y,z,gradients);
					gm[1]=InterpolateGradient(1,-gradient,x,y,z,gradients);
				}
				else
				if(Abs(gradient.z)==1.0){
					gm[0]=InterpolateGradient(2,gradient,x,y,z,gradients);
					gm[1]=InterpolateGradient(2,-gradient,x,y,z,gradients);
				}

				if(gradient_size<gm[0] || gradient_size<gm[1]) result=static_cast<VOXEL>(0);
				else result=static_cast<VOXEL>(round(gradient_size));

				if(fabs(gradient.x)<Minimalni_velikost_gradientu && fabs(gradient.y)<Minimalni_velikost_gradientu && fabs(gradient.z)<Minimalni_velikost_gradientu)
				result=0;

				vystup.SetVoxel(offset.x+x,offset.y+y,offset.z+z,result);
			}

	delete gradients;
	gradients=0;
}

#ifdef WITH_FFTW

/* Tato funkce aplikuje Cannyho detektor hran na vstupni obraz 'vstup'.
*/
template <class VOXEL> 
static void ApplyCanny3D(const Image3d<VOXEL> &vstup,Image3d<VOXEL> &vystup,float sigma,float scale){
	VectField3d<float> *gradients=0;
	Filter<double>     gauss_filter,diff_gauss_filter;
	Vector3d<int>      offset,size;
	int                width,height,depth,i,count,part;

	#if LEVEL >= 1
	cout<<"Entering ApplyCanny3D"<<endl;
	#endif

	width=vstup.GetSizeX();
	height=vstup.GetSizeY();
	depth=vstup.GetSizeZ();

	diff_gauss_filter=MakeDiffGauss1DFilter(sigma,4.0);
	gauss_filter=MakeGauss1DFilter(sigma,4.0);

	count=static_cast<int>(round(sqrt(static_cast<float>(width))));

	part=width/count;

	size=Vector3d<int>(part+1,height,depth);
	offset=Vector3d<int>(0,0,0);

	Gradient3D(diff_gauss_filter,gauss_filter,vstup,offset,size,gradients);
	NonMaxSuppression3D(vystup,gradients,offset,size);

	size=Vector3d<int>(part+2,height,depth);
	for(i=1;i<(count-1);i++){
		offset=Vector3d<int>(part*i-1,0,0);

		Gradient3D(diff_gauss_filter,gauss_filter,vstup,offset,size,gradients);
		NonMaxSuppression3D(vystup,gradients,offset,size);
	}

	size=Vector3d<int>(width-(count-1)*part+1,height,depth);
	offset=Vector3d<int>((count-1)*part-1,0,0);

	Gradient3D(diff_gauss_filter,gauss_filter,vstup,offset,size,gradients);
	NonMaxSuppression3D(vystup,gradients,offset,size);

	#if LEVEL >= 1
	cout<<"Leaving ApplyCanny3D"<<endl;
	#endif
}

#endif //fftw

/* Aplikuje Hysteresis Threshold. Tedy odprahujeme data nejdrive prahem
   lowThreshold a pak prahem HighThreshold. Do vysledku zahrneme pouze ty
   oblasti, ktere jsou odprahovany prahem highThreshold a ty ktere jsou
   odprahovane prahem lowThreshold a zaroven po nich vede cesta do oblasti
   odprahovane prahem highThreshold.
*/
template <class VOXEL> void hysteresisThreshold3D(Image3d<VOXEL> &data,unsigned short lowThreshold,unsigned short highThreshold){
	int             i,j,k;
	size_t          x,y,z;
	VOXEL           h;
	std::stack< Vector3d<size_t> >   zasob;
	Vector3d<size_t>        sour,pom;

	#if LEVEL >= 1
	cout<<"Entering hysteresisThreshold3D"<<endl;
	#endif

	/*Prvotni projdeni objemu a odprahovani prahy lowThreshold a highThreshold,
	  voxel vyhovujici prahu highThreshold je nastaven na maximum a
	  voxel vyhovujici prahu lowThreshold je nastaven na hodnotu 127.*/
	for(z=0;z<data.GetSizeZ();z++)
		for(y=0;y<data.GetSizeY();y++)
			for(x=0;x<data.GetSizeX();x++){

				h=data.GetVoxel(x,y,z);

				if(h>=lowThreshold){

					data.SetVoxel(x,y,z,std::numeric_limits<VOXEL>::max());

					if(h<highThreshold) data.SetVoxel(x,y,z,static_cast<VOXEL>(127));
				}

			}

	/*Druhe projdeni objemu. Nyni se souradnice voxelu z oblasti odprahovane prahem lowThreshold
	  sousedici s voxely odprahovanymi prahem highThreshold umisti do zasobniku a nastavi na maximum.*/
	for(z=0;z<data.GetSizeZ();z++)
		for(y=0;y<data.GetSizeY();y++)
			for(x=0;x<data.GetSizeX();x++){

				if(data.GetVoxel(x,y,z)==std::numeric_limits<VOXEL>::max()){

					for(i=-1;i<2;i++)
						for(j=-1;j<2;j++)
							for(k=-1;k<2;k++){

								h=(data.Include(static_cast<int>(x+i),static_cast<int>(y+j),static_cast<int>(z+k)))?data.GetVoxel(x+i,y+j,z+k):0;
								if(h==127){
									sour.x=x+i;
									sour.y=y+j;
									sour.z=z+k;
									zasob.push(sour);

									data.SetVoxel(x+i,y+j,z+k,std::numeric_limits<VOXEL>::max());
								}
							}

				}
			}

	/*Nyni se prochazi zasobnik a testuji se voxely sousedici s voxely jejichz souradnice jsou v zsobniku zda-li maji hodnotu 127.
	  Souradnice takovychto voxelu jsou presunuty do zasobniku a hodnota takovehoto voxelu je nastavena na maximum.*/
	while(!(zasob.empty())){

		sour=zasob.top();
		zasob.pop();
		h=(data.Include(sour.x,sour.y,sour.z))?data.GetVoxel(sour.x,sour.y,sour.z):0;

		for(i=-1;i<2;i++)
			for(j=-1;j<2;j++)
				for(k=-1;k<2;k++){

					h=(data.Include(static_cast<int>(sour.x+i),static_cast<int>(sour.y+j),static_cast<int>(sour.z+k)))?data.GetVoxel(sour.x+i,sour.y+j,sour.z+k):0;
					if(h==127){
						pom.x=sour.x+i;
						pom.y=sour.y+j;
						pom.z=sour.z+k;
						zasob.push(pom);

						data.SetVoxel(sour.x+i,sour.y+j,sour.z+k,std::numeric_limits<VOXEL>::max());
					}
				}

	}

	/*Nyni se vynuluji voxely, ktere nesousedi s zadnou oblasti s prahem highThreshold.*/
	for(z=0;z<data.GetSizeZ();z++)
		for(y=0;y<data.GetSizeY();y++)
			for(x=0;x<data.GetSizeX();x++)
				if(data.GetVoxel(x,y,z)==std::numeric_limits<VOXEL>::max()){
					data.SetVoxel(x,y,z,std::numeric_limits<VOXEL>::max());
				}
				else{
					data.SetVoxel(x,y,z,static_cast<VOXEL>(0));
				}

	#if LEVEL >= 1
	cout<<"Leaving hysteresisThreshold3D"<<endl;
	#endif
}

template <class VOXEL> void hystThres3D(Image3d<VOXEL> &data,unsigned short lowThreshold,unsigned short highThreshold){

	if(lowThreshold>std::numeric_limits<VOXEL>::max() ||
		highThreshold>std::numeric_limits<VOXEL>::max())
		throw InternalException("hystThres3D: invalid input value of threshold");

	#if LEVEL >= 1
	cout<<"Entering hystThres3D"<<endl;
	#endif

	hysteresisThreshold3D(data,lowThreshold,highThreshold);

	#if LEVEL >= 1
	cout<<"Leaving hystThres3D"<<endl;
	#endif
}

template <> I3D_DLLEXPORT void hystThres3D(Image3d<RGB> &data,unsigned short lowThreshold,unsigned short highThreshold){
	Image3d<GRAY8>  dataR,dataG,dataB;

	if(lowThreshold>std::numeric_limits<unsigned short>::max() ||
		highThreshold>std::numeric_limits<unsigned short>::max())
		throw InternalException("hystThres3D: invalid input value of threshold");

	#if LEVEL >= 1
	cout<<"Entering hystThres3D"<<endl;
	#endif

	dataR.MakeRoom(data.GetSize());
	dataG.MakeRoom(data.GetSize());
	dataB.MakeRoom(data.GetSize());

	RGBtoGray(data,dataR,dataG,dataB);

	hysteresisThreshold3D(dataR,lowThreshold,highThreshold);
	hysteresisThreshold3D(dataG,lowThreshold,highThreshold);
	hysteresisThreshold3D(dataB,lowThreshold,highThreshold);

	GrayToRGB(dataR,dataG,dataB,data);

	#if LEVEL >= 1
	cout<<"Leaving hystThres3D"<<endl;
	#endif
}

template <> I3D_DLLEXPORT void hystThres3D(Image3d<RGB16> &data,unsigned short lowThreshold,unsigned short highThreshold){
	Image3d<GRAY16> dataR,dataG,dataB;

	if(lowThreshold>std::numeric_limits<GRAY16>::max() ||
		highThreshold>std::numeric_limits<GRAY16>::max())
		throw InternalException("hystThres3D: invalid input value of threshold");

	#if LEVEL >= 1
	cout<<"Entering hystThres3D"<<endl;
	#endif

	dataR.MakeRoom(data.GetSize());
	dataG.MakeRoom(data.GetSize());
	dataB.MakeRoom(data.GetSize());

	RGBtoGray(data,dataR,dataG,dataB);

	hysteresisThreshold3D(dataR,lowThreshold,highThreshold);
	hysteresisThreshold3D(dataG,lowThreshold,highThreshold);
	hysteresisThreshold3D(dataB,lowThreshold,highThreshold);

	GrayToRGB(dataR,dataG,dataB,data);

	#if LEVEL >= 1
	cout<<"Leaving hystThres3D"<<endl;
	#endif
}

#ifdef WITH_FFTW

template <class VOXEL> void Canny3D(const Image3d<VOXEL> &in,Image3d<VOXEL> &out,float sigma,float scale){
	if(sigma<1.0 || sigma>32.0)
		throw InternalException("Canny3D: invalid input value of sigma");
	if(scale<=0.0)
		throw InternalException("Canny3D: scale is zero");

	out.CopyMetaData(in);

	#if LEVEL >= 1
	cout<<"Entering Canny3D"<<endl;
	#endif

	ApplyCanny3D(in,out,sigma,scale);

	#if LEVEL >= 1
	cout<<"Leaving Canny3D"<<endl;
	#endif
}

template <> I3D_DLLEXPORT void Canny3D(const Image3d<RGB> &in,Image3d<RGB> &out,float sigma,float scale){
	Image3d<GRAY8>  inR,inG,inB,outR,outG,outB;

	if(sigma<1.0 || sigma>32.0)
		throw InternalException("Canny3D: invalid input value of sigma");
	if(scale<=0.0)
		throw InternalException("Canny3D: scale is zero");

	out.CopyMetaData(in);

	#if LEVEL >= 1
	cout<<"Entering Canny3D"<<endl;
	#endif

	inR.MakeRoom(in.GetSize());
	inG.MakeRoom(in.GetSize());
	inB.MakeRoom(in.GetSize());
	outR.MakeRoom(in.GetSize());
	outG.MakeRoom(in.GetSize());
	outB.MakeRoom(in.GetSize());

	RGBtoGray(in,inR,inG,inB);

	ApplyCanny3D(inR,outR,sigma,scale);
	ApplyCanny3D(inG,outG,sigma,scale);
	ApplyCanny3D(inB,outB,sigma,scale);

	GrayToRGB(outR,outG,outB,out);

	#if LEVEL >= 1
	cout<<"Leaving Canny3D"<<endl;
	#endif
}

/* END Canny3D edge detector */

/* BEGIN Canny2D edge detector */

/* Tato funkce spocita gradienty vstupniho obrazu. Jako vstupy ma vstupni obraz,
   Gaussuv filtr a derivaci Gaussova filtru. Vystup se uklada do vektoroveho pole
   'vystup'. Parametr 'z' rika funkci, ktery rez ma zpracovat a umistit do vystupu.
*/
template <class VOXEL> void Gradient2D(Filter<double> diff_gauss_filter,Filter<double> gauss_filter,const Image3d<VOXEL> &vstup,VectField3d<double> *&vystup,int z){
	Vector3d<double> gradient;
	Image3d<double>  *gaussed,*temporary;
	int              diff_gauss_filter_length,gauss_filter_length,x,y,width,height,depth,halfsize;

	width=vstup.GetSizeX();
	height=vstup.GetSizeY();
	depth=vstup.GetSizeZ();

	if(vystup==0){
		vystup=new VectField3d<double>();
	}
	vystup->MakeRoom(width,height,1);

	diff_gauss_filter_length=diff_gauss_filter.size.x;
	halfsize=(diff_gauss_filter.size.x-1)/2;
	gauss_filter_length=gauss_filter.size.x;

	if(diff_gauss_filter_length!=gauss_filter_length){
		std::cout<<"diff gauss filter length = "<<diff_gauss_filter_length<<std::endl;
		std::cout<<"gauss filter length = "<<gauss_filter_length<<std::endl;
		throw InternalException("Canny3D-gradient3D: filters doesn't have same length");
	}

	gaussed=new Image3d<double>();
	gaussed->MakeRoom(width,height,1);

	temporary=new Image3d<double>();
	temporary->MakeRoom(width,height,1);

	for(x=0;x<width;x++)
		for(y=0;y<height;y++){
			temporary->SetVoxel(x,y,0,vstup.GetVoxel(x,y,z));
		}

	gradient=Vector3d<double>(0.0,0.0,0.0);

	ApplyFilter1D2(*temporary,gaussed,gauss_filter,1);
	ApplyFilter1D2(*gaussed,temporary,diff_gauss_filter,0);

	for(x=0;x<width;x++)
		for(y=0;y<height;y++){
			gradient.x=temporary->GetVoxel(x,y,0);
			vystup->SetVoxel(x,y,0,gradient);
		}

	for(x=0;x<width;x++)
		for(y=0;y<height;y++){
			temporary->SetVoxel(x,y,0,vstup.GetVoxel(x,y,z));
		}

	ApplyFilter1D2(*temporary,gaussed,gauss_filter,0);
	ApplyFilter1D2(*gaussed,temporary,diff_gauss_filter,1);

	for(x=0;x<width;x++)
		for(y=0;y<height;y++){
			gradient=vystup->GetVoxel(x,y,0);
			gradient.y=temporary->GetVoxel(x,y,0);
			vystup->SetVoxel(x,y,0,gradient);
		}

	delete gaussed;
	delete temporary;
}

#endif //fftw

/* Spocita velikost gradientu, ktery se nachazi v miste, ktere protne vektor
   gradientu na stene pomyslne kostky, ktera se nachazi okolo bodu (x,y,z).
   Parametr axis nam rika, kterou stenu gradient protne
	 (0 - yz, 1 - xz, 2 - xy). V promenne 'gradients' jsou ulozene gradienty.
*/
double InterpolateGradient2D(int axis,Vector3d<double> gradient,int x,int y,VectField3d<double> *gradients){
	Vector3d<double> g[2],f;
	double           gradientMagnitude,value=0.0;
	int              s;

	if(axis==0){
		if(gradient.x>0.0) s=x+1;
		else               s=x-1;
		if(gradient.y>=0){
			g[0]=gradients->GetVoxel(s,y,0);
			g[1]=gradients->GetVoxel(s,y+1,0);
		}
		else
		if(gradient.y<0){
			g[0]=gradients->GetVoxel(s,y,0);
			g[1]=gradients->GetVoxel(s,y-1,0);
		}
		value=Abs(gradient.y);
	}

	if(axis==1){
		if(gradient.y>0.0) s=y+1;
		else               s=y-1;
		if(gradient.x>=0){
			g[0]=gradients->GetVoxel(x,s,0);
			g[1]=gradients->GetVoxel(x+1,s,0);
		}
		else
		if(gradient.x<0){
			g[0]=gradients->GetVoxel(x,s,0);
			g[1]=gradients->GetVoxel(x-1,s,0);
		}
		value=Abs(gradient.x);
	}

	f=g[0]+(g[1]-g[0])*value;

	gradientMagnitude=Norm(f);

	return gradientMagnitude;
}

/* Tato funkce aplikuje na vstupni obraz 'gradients' postup zvany
   Non-Max-Suppression. Ten odstrani z obrazu ty body, ktere nejsou maximalni ve
   smeru gradientu. Je-li napriklad velikost gradientu v bode (x,y,z) 5.0 a ve
   smeru gradientu se nachazeji gradietny o velikosti 6.0 a 7.0, tak bude bod
   (x,y,z) odstranen (bude mu prirazena hodnota 0).
*/
template <class VOXEL> void NonMaxSuppression2D(Image3d<VOXEL> &vystup,VectField3d<double> *&gradients,float scale,int z){
	Vector3d<double> gradient;
	VOXEL            result;
	double           maximum,gradient_size,gm[2];
	int              x,y,width,height;

	width=vystup.GetSizeX();
	height=vystup.GetSizeY();

	for(x=1;x<(width-1);x++)
		for(y=1;y<(height-1);y++){
			gradient=gradients->GetVoxel(x,y,0);
			gradient_size=Norm(gradient);

			maximum=Max(Abs(gradient.x),Abs(gradient.y),Abs(gradient.z));

			gradient/=maximum;

			if(Abs(gradient.x)==1.0){
				gm[0]=InterpolateGradient2D(0,gradient,x,y,gradients);
				gm[1]=InterpolateGradient2D(0,-gradient,x,y,gradients);
			}
			else{
				gm[0]=InterpolateGradient2D(1,gradient,x,y,gradients);
				gm[1]=InterpolateGradient2D(1,-gradient,x,y,gradients);
			}

			if(gradient_size<gm[0] || gradient_size<gm[1])	result=static_cast<VOXEL>(0);
			else result=(scale*gradient_size <= std::numeric_limits<VOXEL>::max())?static_cast<VOXEL>(round(scale*gradient_size)):std::numeric_limits<VOXEL>::max();

			if(Abs(gradient.x)<Minimalni_velikost_gradientu && Abs(gradient.y)<Minimalni_velikost_gradientu)
			result=0;

			vystup.SetVoxel(x,y,z,result);
		}

	delete gradients;
	gradients=0;
}

#ifdef WITH_FFTW

template <class VOXEL> 
static void ApplyCanny2D(const Image3d<VOXEL> &vstup,Image3d<VOXEL> &vystup,float sigma,float scale)
{
	VectField3d<double> *gradients=0;
	Filter<double>     gauss_filter,diff_gauss_filter;
	Vector3d<int>      offset,size;
	int                width,height,depth,z;

	#if LEVEL >= 1
	cout<<"Entering ApplyCanny3D"<<endl;
	#endif

	width=vstup.GetSizeX();
	height=vstup.GetSizeY();
	depth=vstup.GetSizeZ();

	#ifdef EXPORT_GRADIENT_DIRECTION
		char *gradients_path,*template_path;
		FILE *gradients_file;

		template_path=new char[256];
		sprintf(template_path,"./directions_XXXXXX");
		gradients_path=mktemp(template_path);


		if((gradients_file=fopen(gradients_path,"w"))==0){
			throw InternalException("Canny2D: gradients file couldn't be opened!");
		}
	#endif

	diff_gauss_filter=MakeDiffGauss1DFilter(sigma,4.0);
	gauss_filter=MakeGauss1DFilter(sigma,4.0);

	for(z=0;z<depth;z++){
		Gradient2D(diff_gauss_filter,gauss_filter,vstup,gradients,z);

		#ifdef EXPORT_GRADIENT_DIRECTION
			Vector3d<double> gradient;
			float            lg;
			int              x,y;

			for(y=0;y<height;y++)
				for(x=0;x<width;x++){
					gradient=gradients->GetVoxel(x,y,0);
					lg=sqrt(gradient.x*gradient.x+gradient.y*gradient.y);
					if(lg==0.0){
						fprintf(gradients_file,"%f\n",0.0);
					}
					else{
						if(gradient.y<0.0)
							fprintf(gradients_file,"%f\n",180.0/M_PI*(2.0*M_PI-acos(gradient.x/lg)));
						else
							fprintf(gradients_file,"%f\n",180.0/M_PI*acos(gradient.x/lg));
					}
				}
		#endif

		NonMaxSuppression2D(vystup,gradients,scale,z);
	}

	#ifdef EXPORT_GRADIENT_DIRECTION
		cout<<"Gradients were saved to "<<gradients_path<<" ."<<endl;
		fclose(gradients_file);
	#endif

	#if LEVEL >= 1
	cout<<"Leaving ApplyCanny3D"<<endl;
	#endif
}

#endif //fftw

/* Aplikuje Hysteresis Threshold. Je velice podobna procedure
   hysteresisThreshold3D, kde je i podrobnejsi popis.
*/
template <class VOXEL> void hysteresisThreshold2D(Image3d<VOXEL> &data,unsigned short lowThreshold,unsigned short highThreshold){
	int             i,j;
	size_t          x,y,z;
	VOXEL           h;
	std::stack< Vector3d<size_t> >   zasob;
	Vector3d<size_t>        sour,pom;

	#if LEVEL >= 1
	cout<<"Entering hysteresisThreshold2D"<<endl;
	#endif

	for(z=0;z<data.GetSizeZ();z++)
		for(y=0;y<data.GetSizeY();y++)
			for(x=0;x<data.GetSizeX();x++){
				h=data.GetVoxel(x,y,z);
				if(h>=lowThreshold){
					data.SetVoxel(x,y,z,std::numeric_limits<VOXEL>::max());
					if(h<highThreshold) data.SetVoxel(x,y,z,static_cast<VOXEL>(127));
				}
			}

	for(z=0;z<data.GetSizeZ();z++)
		for(y=0;y<data.GetSizeY();y++)
			for(x=0;x<data.GetSizeX();x++){

				if(data.GetVoxel(x,y,z)==std::numeric_limits<VOXEL>::max()){
					for(i=-1;i<2;i++)
						for(j=-1;j<2;j++){
							h=(data.Include(static_cast<int>(x+i),static_cast<int>(y+j),z))?data.GetVoxel(x+i,y+j,z):0;
							if(h==127){
								sour.x=x+i;
								sour.y=y+j;
								sour.z=z;
								zasob.push(sour);
								data.SetVoxel(x+i,y+j,z,std::numeric_limits<VOXEL>::max());
							}
						}
				}
			}

	while(!(zasob.empty())){
		sour=zasob.top();
		zasob.pop();
		h=(data.Include(sour.x,sour.y,sour.z))?data.GetVoxel(sour.x,sour.y,sour.z):0;

		for(i=-1;i<2;i++)
			for(j=-1;j<2;j++){
				h=(data.Include(static_cast<int>(sour.x+i),static_cast<int>(sour.y+j),sour.z))?data.GetVoxel(sour.x+i,sour.y+j,sour.z):0;
				if(h==127){
					pom.x=sour.x+i;
					pom.y=sour.y+j;
					pom.z=sour.z;
					zasob.push(pom);
					data.SetVoxel(sour.x+i,sour.y+j,sour.z,std::numeric_limits<VOXEL>::max());
				}
			}
	}

	for(z=0;z<data.GetSizeZ();z++)
		for(y=0;y<data.GetSizeY();y++)
			for(x=0;x<data.GetSizeX();x++)
				if(data.GetVoxel(x,y,z)==std::numeric_limits<VOXEL>::max()){
					data.SetVoxel(x,y,z,std::numeric_limits<VOXEL>::max());
				}
				else{
					data.SetVoxel(x,y,z,static_cast<VOXEL>(0));
				}

	#if LEVEL >= 1
	cout<<"Leaving hysteresisThreshold2D"<<endl;
	#endif

}

template <class VOXEL> void hystThres2D(Image3d<VOXEL> &data,unsigned short lowThreshold,unsigned short highThreshold){

	if(lowThreshold>std::numeric_limits<VOXEL>::max() ||
		highThreshold>std::numeric_limits<VOXEL>::max())
		throw InternalException("hystThres2D: invalid input value of threshold");

	#if LEVEL >= 1
	cout<<"Entering hystThres2D"<<endl;
	#endif

	hysteresisThreshold2D(data,lowThreshold,highThreshold);

	#if LEVEL >= 1
	cout<<"Leaving hystThres2D"<<endl;
	#endif
}

template <> I3D_DLLEXPORT void hystThres2D(Image3d<RGB> &data,unsigned short lowThreshold,unsigned short highThreshold){
	Image3d<GRAY8>  dataR,dataG,dataB;

	if(lowThreshold>std::numeric_limits<unsigned short>::max() ||
		highThreshold>std::numeric_limits<unsigned short>::max())
		throw InternalException("hystThres2D: invalid input value of threshold");

	#if LEVEL >= 1
	cout<<"Entering hystThres2D"<<endl;
	#endif

	dataR.MakeRoom(data.GetSize());
	dataG.MakeRoom(data.GetSize());
	dataB.MakeRoom(data.GetSize());

	RGBtoGray(data,dataR,dataG,dataB);

	hysteresisThreshold2D(dataR,lowThreshold,highThreshold);
	hysteresisThreshold2D(dataG,lowThreshold,highThreshold);
	hysteresisThreshold2D(dataB,lowThreshold,highThreshold);

	GrayToRGB(dataR,dataG,dataB,data);

	#if LEVEL >= 1
	cout<<"Leaving hystThres2D"<<endl;
	#endif
}

template <> I3D_DLLEXPORT void hystThres2D(Image3d<RGB16> &data,unsigned short lowThreshold,unsigned short highThreshold){
	Image3d<GRAY16> dataR,dataG,dataB;

	if(lowThreshold>std::numeric_limits<GRAY16>::max() ||
		highThreshold>std::numeric_limits<GRAY16>::max())
		throw InternalException("hystThres2D: invalid input value of threshold");

	#if LEVEL >= 1
	cout<<"Entering hystThres2D"<<endl;
	#endif

	dataR.MakeRoom(data.GetSize());
	dataG.MakeRoom(data.GetSize());
	dataB.MakeRoom(data.GetSize());

	RGBtoGray(data,dataR,dataG,dataB);

	hysteresisThreshold2D(dataR,lowThreshold,highThreshold);
	hysteresisThreshold2D(dataG,lowThreshold,highThreshold);
	hysteresisThreshold2D(dataB,lowThreshold,highThreshold);

	GrayToRGB(dataR,dataG,dataB,data);

	#if LEVEL >= 1
	cout<<"Leaving hystThres2D"<<endl;
	#endif
}

#ifdef WITH_FFTW

template <class VOXEL> void Canny2D(const Image3d<VOXEL> &in,Image3d<VOXEL> &out,float sigma,float scale){
	if(sigma<0.5 || sigma>32.0)
		throw InternalException("Canny2D: invalid input value of sigma");
	if(scale<=0.0)
		throw InternalException("Canny2D: scale is zero");

	out.CopyMetaData(in);

	#if LEVEL >= 1
	cout<<"Entering Canny2D"<<endl;
	#endif

	ApplyCanny2D(in,out,sigma,scale);

	#if LEVEL >= 1
	cout<<"Leaving Canny2D"<<endl;
	#endif
}

template <> I3D_DLLEXPORT void Canny2D(const Image3d<RGB> &in,Image3d<RGB> &out,float sigma,float scale){
	Image3d<GRAY8>  inR,inG,inB,outR,outG,outB;

	if(sigma<0.5 || sigma>32.0)
		throw InternalException("Canny2D: invalid input value of sigma");
	if(scale<=0.0)
		throw InternalException("Canny2D: scale is zero");

	out.CopyMetaData(in);

	#if LEVEL >= 1
	cout<<"Entering Canny2D"<<endl;
	#endif

	inR.MakeRoom(in.GetSize());
	inG.MakeRoom(in.GetSize());
	inB.MakeRoom(in.GetSize());
	outR.MakeRoom(in.GetSize());
	outG.MakeRoom(in.GetSize());
	outB.MakeRoom(in.GetSize());

	RGBtoGray(in,inR,inG,inB);

	ApplyCanny2D(inR,outR,sigma,scale);
	ApplyCanny2D(inG,outG,sigma,scale);
	ApplyCanny2D(inB,outB,sigma,scale);

	GrayToRGB(outR,outG,outB,out);

	#if LEVEL >= 1
	cout<<"Leaving Canny2D"<<endl;
	#endif
}

#endif //fftw

/* END Canny2D edge detector */

/* BEGIN Sobel2D edge detector */

int X2D[3][3]={{-1,-2,-1},{	0,0,0},{1,2,1}};
int Y2D[3][3]={{-1,0,1},{-2,0,2},{-1,0,1}};

/* Tato funkce aplikuje 2d Sobeluv filtr na vstupni obraz 'in'.
*/
template <class VOXEL> 
static void ApplySobel2D(const Image3d<VOXEL> &in,Image3d<VOXEL> &out)
{
	size_t x,y,z;
	float  sumaX=0.0,sumaY=0.0,vstup;
	int    xx,yy,suma;

	#if LEVEL >= 1
	cout<<"Entering ApplySobel2D"<<endl;
	#endif

	for(z=0;z<in.GetSizeZ();z++)
		for(y=1;y<in.GetSizeY()-1;y++)
			for(x=1;x<in.GetSizeX()-1;x++){

				sumaX=0.0;
				sumaY=0.0;

				for(xx=0;xx<3;xx++)
					for(yy=0;yy<3;yy++){
					vstup=in.GetVoxel(x+xx-1,y+yy-1,z);
					sumaX+=vstup*X2D[xx][yy];
					sumaY+=vstup*Y2D[xx][yy];
				}

				sumaX/=4.0;
				sumaY/=4.0;

				suma=static_cast<int>(round(sqrt(sumaX*sumaX+sumaY*sumaY)));

				if(suma<0)  suma=0;
				if(suma>std::numeric_limits<VOXEL>::max())  suma=static_cast<int>(std::numeric_limits<VOXEL>::max());

				out.SetVoxel(x,y,z,static_cast<VOXEL>(suma));
			}

	#if LEVEL >= 1
	cout<<"Leaving ApplySobel2D"<<endl;
	#endif

}

template <class VOXEL> void Sobel2D(const Image3d<VOXEL> &in,Image3d<VOXEL> &out){
	out.CopyMetaData(in);

	#if LEVEL >= 1
	cout<<"Entering Sobel2D"<<endl;
	#endif

	ApplySobel2D(in,out);

	#if LEVEL >= 1
	cout<<"Leaving Sobel2D"<<endl;
	#endif
}

template <> I3D_DLLEXPORT void Sobel2D(const Image3d<RGB> &in,Image3d<RGB> &out){
	Image3d<GRAY8> inR,inG,inB,outR,outG,outB;

	out.CopyMetaData(in);

	#if LEVEL >= 1
	cout<<"Entering Sobel2D"<<endl;
	#endif

	inR.MakeRoom(in.GetSize());
	inG.MakeRoom(in.GetSize());
	inB.MakeRoom(in.GetSize());
	outR.MakeRoom(in.GetSize());
	outG.MakeRoom(in.GetSize());
	outB.MakeRoom(in.GetSize());

	RGBtoGray(in,inR,inG,inB);

	ApplySobel2D(inR,outR);
	ApplySobel2D(inG,outG);
	ApplySobel2D(inB,outB);

	GrayToRGB(outR,outG,outB,out);

	#if LEVEL >= 1
	cout<<"Leaving Sobel2D"<<endl;
	#endif
}

template <> I3D_DLLEXPORT void Sobel2D(const Image3d<RGB16> &in,Image3d<RGB16> &out){
	Image3d<GRAY16> inR,inG,inB,outR,outG,outB;

	out.CopyMetaData(in);

	#if LEVEL >= 1
	cout<<"Entering Sobel2D"<<endl;
	#endif

	inR.MakeRoom(in.GetSize());
	inG.MakeRoom(in.GetSize());
	inB.MakeRoom(in.GetSize());
	outR.MakeRoom(in.GetSize());
	outG.MakeRoom(in.GetSize());
	outB.MakeRoom(in.GetSize());

	RGBtoGray(in,inR,inG,inB);

	ApplySobel2D(inR,outR);
	ApplySobel2D(inG,outG);
	ApplySobel2D(inB,outB);

	GrayToRGB(outR,outG,outB,out);

	#if LEVEL >= 1
	cout<<"Leaving Sobel2D"<<endl;
	#endif
}

/* END Sobel2D edge detector */

/* BEGIN Laplace2D edge detector */

/* Tato funkce aplikuje 2d Laplaceuv filtr na vstupni obraz 'in'.
*/
template <class VOXEL> 
static void ApplyLaplace2D(const Image3d<VOXEL> &in,Image3d<VOXEL> &out)
{
	size_t	x,y,z;
	int			suma;

	#if LEVEL >= 1
	cout<<"Entering ApplyLaplace2D"<<endl;
	#endif

	for(z=0;z<in.GetSizeZ();z++)
		for(y=1;y<in.GetSizeY()-1;y++)
			for(x=1;x<in.GetSizeX()-1;x++){

				suma=static_cast<int>(Abs(-4*in.GetVoxel(x,y,z)
					+in.GetVoxel(x+1,y,z)
					+in.GetVoxel(x-1,y,z)
					+in.GetVoxel(x,y+1,z)
					+in.GetVoxel(x,y-1,z)));

				if(suma<0)                 
					suma=0;
				if(suma>std::numeric_limits<VOXEL>::max()) suma=static_cast<int>(std::numeric_limits<VOXEL>::max());

				out.SetVoxel(x,y,z,static_cast<VOXEL>(suma));
			}

	#if LEVEL >= 1
	cout<<"Leaving ApplyLaplace2D"<<endl;
	#endif
}

template <class VOXEL> void Laplace2D(const Image3d<VOXEL> &in,Image3d<VOXEL> &out){
	out.CopyMetaData(in);

	#if LEVEL >= 1
	cout<<"Entering Laplace2D"<<endl;
	#endif

	ApplyLaplace2D(in,out);

	#if LEVEL >= 1
	cout<<"Leaving Laplace2D"<<endl;
	#endif
}

template <> I3D_DLLEXPORT void Laplace2D(const Image3d<RGB> &in,Image3d<RGB> &out){
	Image3d<GRAY8>  inR,inG,inB,outR,outG,outB;

	out.CopyMetaData(in);

	#if LEVEL >= 1
	cout<<"Entering Laplace2D"<<endl;
	#endif

	inR.MakeRoom(in.GetSize());
	inG.MakeRoom(in.GetSize());
	inB.MakeRoom(in.GetSize());
	outR.MakeRoom(in.GetSize());
	outG.MakeRoom(in.GetSize());
	outB.MakeRoom(in.GetSize());

	RGBtoGray(in,inR,inG,inB);

	ApplyLaplace2D(inR,outR);
	ApplyLaplace2D(inG,outG);
	ApplyLaplace2D(inB,outB);

	GrayToRGB(outR,outG,outB,out);

	#if LEVEL >= 1
	cout<<"Leaving Laplace2D"<<endl;
	#endif
}

template <> I3D_DLLEXPORT void Laplace2D(const Image3d<RGB16> &in,Image3d<RGB16> &out){
	Image3d<GRAY16> inR,inG,inB,outR,outG,outB;

	out.CopyMetaData(in);

	#if LEVEL >= 1
	cout<<"Entering Laplace2D"<<endl;
	#endif

	inR.MakeRoom(in.GetSize());
	inG.MakeRoom(in.GetSize());
	inB.MakeRoom(in.GetSize());
	outR.MakeRoom(in.GetSize());
	outG.MakeRoom(in.GetSize());
	outB.MakeRoom(in.GetSize());

	RGBtoGray(in,inR,inG,inB);

	ApplyLaplace2D(inR,outR);
	ApplyLaplace2D(inG,outG);
	ApplyLaplace2D(inB,outB);

	GrayToRGB(outR,outG,outB,out);

	#if LEVEL >= 1
	cout<<"Leaving Laplace2D"<<endl;
	#endif
}

/* END Laplace2D edge detector */

/* Tato funkce aplikuje na vstup in 8 2D konvolucnich jader ulozenych v promenne
   filtr v rovine xy, zjisti maximalni hodnotu z techto osmi aplikaci, tuto
   vydeli souctem kladnych hodnot v jadre a ulozi do vystupu 'out'. Vyuzivaji ji
   operatory Prewitt, Kirsch, CompassSobel a Robinson.
*/
template <class VOXEL> 
static void Apply2DCompassFilter(const Image3d<VOXEL> &in,Image3d<VOXEL> &out,int filtr[8][3][3])
{
	int     xx,yy,k,delitel=0,vystup;
	size_t  x,y,z;
	int     pole[8],max;
	float		suma,vstup;

	#if LEVEL >= 1
	cout<<"Entering Apply2DCompassFilter"<<endl;
	#endif

	for(yy=0;yy<3;yy++)
		for(xx=0;xx<3;xx++)
			if(filtr[0][xx][yy]>0)delitel+=filtr[0][xx][yy];

	for(z=0;z<in.GetSizeZ();z++){
		#if LEVEL >= 3
		cout<<z<<endl;
		#endif
		for(y=1;y<in.GetSizeY()-1;y++)
			for(x=1;x<in.GetSizeX()-1;x++){

			for(k=0;k<8;k++){
				suma=0;
				for(xx=0;xx<3;xx++)
					for(yy=0;yy<3;yy++){
					vstup=in.GetVoxel(x+xx-1,y+yy-1,z);
					suma+=vstup*filtr[k][xx][yy];
				}
				pole[k]=Abs(static_cast<int>(suma));
			}

			max=0;
			for(k=0;k<8;k++){
				if(pole[k]>max)max=pole[k];
			}

			vystup=max/delitel;

			if(vystup>std::numeric_limits<VOXEL>::max()) vystup=static_cast<int>(std::numeric_limits<VOXEL>::max());

			out.SetVoxel(x,y,z,static_cast<VOXEL>(vystup));
		}
	}

	#if LEVEL >= 1
	cout<<"Leaving Apply2DCompassFilter"<<endl;
	#endif
}

/* BEGIN Kirsch2D edge detector */

static int	K2D[8][3][3]={	{{-3,-3,5},{-3,0,5},{-3,-3,5}},
			{{-3,5,5},{-3,0,5},{-3,-3,-3}},
			{{5,5,5},{-3,0,-3},{-3,-3,-3}},
			{{5,5,-3},{5,0,-3},{-3,-3,-3}},
			{{5,-3,-3},{5,0,-3},{5,-3,-3}},
			{{-3,-3,-3},{5,0,-3},{5,5,-3}},
			{{-3,-3,-3},{-3,0,-3},{5,5,5}},
			{{-3,-3,-3},{-3,0,5},{-3,5,5}}};

/* Tato funkce aplikuje 2d Kirschuv filtr na vstupni obraz 'in'.
*/
template <class VOXEL> void Kirsch2D(const Image3d<VOXEL> &in,Image3d<VOXEL> &out){
	out.CopyMetaData(in);

	#if LEVEL >= 1
	cout<<"Entering Kirsch2D"<<endl;
	#endif

	Apply2DCompassFilter(in,out,K2D);

	#if LEVEL >= 1
	cout<<"Leaving Kirsch2D"<<endl;
	#endif
}

template <> I3D_DLLEXPORT void Kirsch2D(const Image3d<RGB> &in,Image3d<RGB> &out){
	Image3d<GRAY8>  inR,inG,inB,outR,outG,outB;

	out.CopyMetaData(in);

	#if LEVEL >= 1
	cout<<"Entering Kirsch2D"<<endl;
	#endif

	inR.MakeRoom(in.GetSize());
	inG.MakeRoom(in.GetSize());
	inB.MakeRoom(in.GetSize());
	outR.MakeRoom(in.GetSize());
	outG.MakeRoom(in.GetSize());
	outB.MakeRoom(in.GetSize());

	RGBtoGray(in,inR,inG,inB);

	Apply2DCompassFilter(inR,outR,K2D);
	Apply2DCompassFilter(inG,outG,K2D);
	Apply2DCompassFilter(inB,outB,K2D);

	GrayToRGB(outR,outG,outB,out);

	#if LEVEL >= 1
	cout<<"Leaving Kirsch2D"<<endl;
	#endif
}

template <> I3D_DLLEXPORT void Kirsch2D(const Image3d<RGB16> &in,Image3d<RGB16> &out){
	Image3d<GRAY16> inR,inG,inB,outR,outG,outB;

	out.CopyMetaData(in);

	#if LEVEL >= 1
	cout<<"Entering Kirsch2D"<<endl;
	#endif

	inR.MakeRoom(in.GetSize());
	inG.MakeRoom(in.GetSize());
	inB.MakeRoom(in.GetSize());
	outR.MakeRoom(in.GetSize());
	outG.MakeRoom(in.GetSize());
	outB.MakeRoom(in.GetSize());

	RGBtoGray(in,inR,inG,inB);

	Apply2DCompassFilter(inR,outR,K2D);
	Apply2DCompassFilter(inG,outG,K2D);
	Apply2DCompassFilter(inB,outB,K2D);

	GrayToRGB(outR,outG,outB,out);

	#if LEVEL >= 1
	cout<<"Leaving Kirsch2D"<<endl;
	#endif
}

/* END Kirsch2D edge detector */

/* BEGIN Prewitt2D edge detector */

static int	P2D[8][3][3]={	{{-1,-1,-1},{0,0,0},{1,1,1}},
			{{0,-1,-1},{1,0,-1},{1,1,0}},
			{{1,0,-1},{1,0,-1},{1,0,-1}},
			{{1,1,0},{1,0,-1},{0,-1,-1}},
			{{1,1,1},{0,0,0},{-1,-1,-1}},
			{{0,1,1},{-1,0,1},{-1,-1,0}},
			{{-1,0,1},{-1,0,1},{-1,0,1}},
			{{-1,-1,0},{-1,0,1},{0,1,1}}};

/* Tato funkce aplikuje 2d Prewittuv filtr na vstupni obraz 'in'.
*/
template <class VOXEL> void Prewitt2D(const Image3d<VOXEL> &in,Image3d<VOXEL> &out){
	out.CopyMetaData(in);

	#if LEVEL >= 1
	cout<<"Entering Prewitt2D"<<endl;
	#endif

	Apply2DCompassFilter(in,out,P2D);

	#if LEVEL >= 1
	cout<<"Leaving Prewitt2D"<<endl;
	#endif
}

template <> I3D_DLLEXPORT void Prewitt2D(const Image3d<RGB> &in,Image3d<RGB> &out){
	Image3d<GRAY8>  inR,inG,inB,outR,outG,outB;

	out.CopyMetaData(in);

	#if LEVEL >= 1
	cout<<"Entering Prewitt2D"<<endl;
	#endif

	inR.MakeRoom(in.GetSize());
	inG.MakeRoom(in.GetSize());
	inB.MakeRoom(in.GetSize());
	outR.MakeRoom(in.GetSize());
	outG.MakeRoom(in.GetSize());
	outB.MakeRoom(in.GetSize());

	RGBtoGray(in,inR,inG,inB);

	Apply2DCompassFilter(inR,outR,P2D);
	Apply2DCompassFilter(inG,outG,P2D);
	Apply2DCompassFilter(inB,outB,P2D);

	GrayToRGB(outR,outG,outB,out);

	#if LEVEL >= 1
	cout<<"Leaving Prewitt2D"<<endl;
	#endif
}

template <> I3D_DLLEXPORT void Prewitt2D(const Image3d<RGB16> &in,Image3d<RGB16> &out){
	Image3d<GRAY16> inR,inG,inB,outR,outG,outB;

	out.CopyMetaData(in);

	#if LEVEL >= 1
	cout<<"Entering Prewitt2D"<<endl;
	#endif

	inR.MakeRoom(in.GetSize());
	inG.MakeRoom(in.GetSize());
	inB.MakeRoom(in.GetSize());
	outR.MakeRoom(in.GetSize());
	outG.MakeRoom(in.GetSize());
	outB.MakeRoom(in.GetSize());

	RGBtoGray(in,inR,inG,inB);

	Apply2DCompassFilter(inR,outR,P2D);
	Apply2DCompassFilter(inG,outG,P2D);
	Apply2DCompassFilter(inB,outB,P2D);

	GrayToRGB(outR,outG,outB,out);

	#if LEVEL >= 1
	cout<<"Leaving Prewitt2D"<<endl;
	#endif
}

/* END Prewitt2D edge detector */

/* BEGIN Robinson2D edge detector */

static int	R2D[8][3][3]={	{{1,1,1},{1,-2,1},{-1,-1,-1}},
			{{1,1,1},{-1,-2,1},{-1,-1,1}},			
			{{-1,1,1},{-1,-2,1},{-1,1,1}},
			{{-1,-1,1},{-1,-2,1},{1,1,1}},
			{{-1,-1,-1},{1,-2,1},{1,1,1}},
			{{1,-1,-1},{1,-2,-1},{1,1,1}},
			{{1,1,-1},{1,-2,-1},{1,1,-1}},
			{{1,1,1},{1,-2,-1},{1,-1,-1}}};

/* Tato funkce aplikuje 2d Robinsonuv filtr na vstupni obraz 'in'.
*/
template <class VOXEL> void Robinson2D(const Image3d<VOXEL> &in,Image3d<VOXEL> &out){
	out.CopyMetaData(in);

	#if LEVEL >= 1
	cout<<"Entering Robinson2D"<<endl;
	#endif

	Apply2DCompassFilter(in,out,R2D);

	#if LEVEL >= 1
	cout<<"Leaving Robinson2D"<<endl;
	#endif
}

template <> I3D_DLLEXPORT void Robinson2D(const Image3d<RGB> &in,Image3d<RGB> &out){
	Image3d<GRAY8>  inR,inG,inB,outR,outG,outB;

	out.CopyMetaData(in);

	#if LEVEL >= 1
	cout<<"Entering Robinson2D"<<endl;
	#endif

	inR.MakeRoom(in.GetSize());
	inG.MakeRoom(in.GetSize());
	inB.MakeRoom(in.GetSize());
	outR.MakeRoom(in.GetSize());
	outG.MakeRoom(in.GetSize());
	outB.MakeRoom(in.GetSize());

	RGBtoGray(in,inR,inG,inB);

	Apply2DCompassFilter(inR,outR,R2D);
	Apply2DCompassFilter(inG,outG,R2D);
	Apply2DCompassFilter(inB,outB,R2D);

	GrayToRGB(outR,outG,outB,out);

	#if LEVEL >= 1
	cout<<"Leaving Robinson2D"<<endl;
	#endif
}

template <> I3D_DLLEXPORT void Robinson2D(const Image3d<RGB16> &in,Image3d<RGB16> &out){
	Image3d<GRAY16> inR,inG,inB,outR,outG,outB;

	out.CopyMetaData(in);

	#if LEVEL >= 1
	cout<<"Entering Robinson2D"<<endl;
	#endif

	inR.MakeRoom(in.GetSize());
	inG.MakeRoom(in.GetSize());
	inB.MakeRoom(in.GetSize());
	outR.MakeRoom(in.GetSize());
	outG.MakeRoom(in.GetSize());
	outB.MakeRoom(in.GetSize());

	RGBtoGray(in,inR,inG,inB);

	Apply2DCompassFilter(inR,outR,R2D);
	Apply2DCompassFilter(inG,outG,R2D);
	Apply2DCompassFilter(inB,outB,R2D);

	GrayToRGB(outR,outG,outB,out);

	#if LEVEL >= 1
	cout<<"Leaving Robinson2D"<<endl;
	#endif
}

/* END Robinson2D edge detector */

/* BEGIN CompassSobel2D edge detector */

static int	S2D[8][3][3]={	{{-1,-2,-1},{0,0,0},{1,2,1}},
			{{0,-1,-2},{1,0,-1},{2,1,0}},
			{{1,0,-1},{2,0,-2},{1,0,-1}},
			{{2,1,0},{1,0,-1},{0,-1,-2}},
			{{1,2,1},{0,0,0},{-1,-2,-1}},
			{{0,1,2},{-1,0,1},{-2,-1,0}},
			{{-1,0,1},{-2,0,2},{-1,0,1}},
			{{-2,-1,0},{-1,0,1},{0,1,2}}};

/* Tato funkce aplikuje 2d Sobeluv filtr na vstupni obraz 'in'.
*/
template <class VOXEL> void CompassSobel2D(const Image3d<VOXEL> &in,Image3d<VOXEL> &out){
	out.CopyMetaData(in);

	#if LEVEL >= 1
	cout<<"Entering CompassSobel2D"<<endl;
	#endif

	Apply2DCompassFilter(in,out,S2D);

	#if LEVEL >= 1
	cout<<"Leaving CompassSobel2D"<<endl;
	#endif
}

template <> I3D_DLLEXPORT void CompassSobel2D(const Image3d<RGB> &in,Image3d<RGB> &out){
	Image3d<GRAY8>  inR,inG,inB,outR,outG,outB;

	out.CopyMetaData(in);

	#if LEVEL >= 1
	cout<<"Entering CompassSobel2D"<<endl;
	#endif

	inR.MakeRoom(in.GetSize());
	inG.MakeRoom(in.GetSize());
	inB.MakeRoom(in.GetSize());
	outR.MakeRoom(in.GetSize());
	outG.MakeRoom(in.GetSize());
	outB.MakeRoom(in.GetSize());

	RGBtoGray(in,inR,inG,inB);

	Apply2DCompassFilter(inR,outR,S2D);
	Apply2DCompassFilter(inG,outG,S2D);
	Apply2DCompassFilter(inB,outB,S2D);

	GrayToRGB(outR,outG,outB,out);

	#if LEVEL >= 1
	cout<<"Leaving CompassSobel2D"<<endl;
	#endif
}

template <> I3D_DLLEXPORT void CompassSobel2D(const Image3d<RGB16> &in,Image3d<RGB16> &out){
	Image3d<GRAY16> inR,inG,inB,outR,outG,outB;

	out.CopyMetaData(in);

	#if LEVEL >= 1
	cout<<"Entering CompassSobel2D"<<endl;
	#endif

	inR.MakeRoom(in.GetSize());
	inG.MakeRoom(in.GetSize());
	inB.MakeRoom(in.GetSize());
	outR.MakeRoom(in.GetSize());
	outG.MakeRoom(in.GetSize());
	outB.MakeRoom(in.GetSize());

	RGBtoGray(in,inR,inG,inB);

	Apply2DCompassFilter(inR,outR,S2D);
	Apply2DCompassFilter(inG,outG,S2D);
	Apply2DCompassFilter(inB,outB,S2D);

	GrayToRGB(outR,outG,outB,out);

	#if LEVEL >= 1
	cout<<"Leaving CompassSobel2D"<<endl;
	#endif
}

/* END CompassSobel2D edge detector */

/* BEGIN Roberts2D edge detector */

/* Tato funkce aplikuje 2d Robertsuv filtr na vstupni obraz 'in'.
*/
template <class VOXEL> 
static void ApplyRoberts2D(const Image3d<VOXEL> &in,Image3d<VOXEL> &out)
{
	size_t  x,y,z;
	int     suma;

	#if LEVEL >= 1
	cout<<"Entering ApplyRoberts2D"<<endl;
	#endif

	for(z=0;z<in.GetSizeZ();z++)
		for(y=0;y<in.GetSizeY()-1;y++)
			for(x=0;x<in.GetSizeX()-1;x++){

				suma=static_cast<int>(round(sqrt(static_cast<double>(Sqr(in.GetVoxel(x,y,z)-in.GetVoxel(x+1,y+1,z))+
					Sqr(in.GetVoxel(x,y+1,z)-in.GetVoxel(x+1,y,z))))));

				if(suma<0)                 suma=0;
				if(suma>std::numeric_limits<VOXEL>::max()) suma=static_cast<int>(std::numeric_limits<VOXEL>::max());

				out.SetVoxel(x,y,z,static_cast<VOXEL>(suma));
			}

	#if LEVEL >= 1
	cout<<"Leaving ApplyRoberts2D"<<endl;
	#endif
}

template <class VOXEL> void Roberts2D(const Image3d<VOXEL> &in,Image3d<VOXEL> &out){
	out.CopyMetaData(in);

	#if LEVEL >= 1
	cout<<"Entering Roberts2D"<<endl;
	#endif

	ApplyRoberts2D(in,out);

	#if LEVEL >= 1
	cout<<"Leaving Roberts2D"<<endl;
	#endif
}

template <> I3D_DLLEXPORT void Roberts2D(const Image3d<RGB> &in,Image3d<RGB> &out){
	Image3d<GRAY8>  inR,inG,inB,outR,outG,outB;

	out.CopyMetaData(in);

	#if LEVEL >= 1
	cout<<"Entering Roberts2D"<<endl;
	#endif

	inR.MakeRoom(in.GetSize());
	inG.MakeRoom(in.GetSize());
	inB.MakeRoom(in.GetSize());
	outR.MakeRoom(in.GetSize());
	outG.MakeRoom(in.GetSize());
	outB.MakeRoom(in.GetSize());

	RGBtoGray(in,inR,inG,inB);

	ApplyRoberts2D(inR,outR);
	ApplyRoberts2D(inG,outG);
	ApplyRoberts2D(inB,outB);

	GrayToRGB(outR,outG,outB,out);

	#if LEVEL >= 1
	cout<<"Leaving Roberts2D"<<endl;
	#endif
}

template <> I3D_DLLEXPORT void Roberts2D(const Image3d<RGB16> &in,Image3d<RGB16> &out){
	Image3d<GRAY16> inR,inG,inB,outR,outG,outB;

	out.CopyMetaData(in);

	#if LEVEL >= 1
	cout<<"Entering Roberts2D"<<endl;
	#endif

	inR.MakeRoom(in.GetSize());
	inG.MakeRoom(in.GetSize());
	inB.MakeRoom(in.GetSize());
	outR.MakeRoom(in.GetSize());
	outG.MakeRoom(in.GetSize());
	outB.MakeRoom(in.GetSize());

	RGBtoGray(in,inR,inG,inB);

	ApplyRoberts2D(inR,outR);
	ApplyRoberts2D(inG,outG);
	ApplyRoberts2D(inB,outB);

	GrayToRGB(outR,outG,outB,out);

	#if LEVEL >= 1
	cout<<"Leaving Roberts2D"<<endl;
	#endif
}

/* END Roberts2D edge detector */

/* BEGIN EstimateGradient1 edge detector */

/* Tato funkce aplikuje 2d filtr odhadu gradientu na vstupni obraz 'in'.
*/
template <class VOXEL> 
static void ApplyEstimateGradient1_2D(const Image3d<VOXEL> &in,Image3d<VOXEL> &out,bool clamp){
	size_t  x,y,z;
	float       xyz;
	int         suma;

	#if LEVEL >= 1
	cout<<"Entering ApplyEstimateGradient1_2D"<<endl;
	#endif

	if(clamp){
			for(z=0;z<in.GetSizeZ();z++)
				for(y=0;y<in.GetSizeY();y++)
					for(x=0;x<in.GetSizeX();x++){

						xyz=((in.Include(x,y,z))?in.GetVoxel(x,y,z):0);

						suma=static_cast<int>(round(sqrt(static_cast<double>(Sqr(xyz-((in.Include(x-1,y,z))?in.GetVoxel(x-1,y,z):xyz))+
							Sqr(xyz-((in.Include(x,y-1,z))?in.GetVoxel(x,y-1,z):xyz))))));

						if(suma>std::numeric_limits<VOXEL>::max()) suma=std::numeric_limits<VOXEL>::max();

						out.SetVoxel(x,y,z,static_cast<VOXEL>(suma));
					}
	}
	else{
			for(z=0;z<in.GetSizeZ();z++)
				for(y=1;y<in.GetSizeY();y++)
					for(x=1;x<in.GetSizeX();x++){

						xyz=((in.Include(x,y,z))?in.GetVoxel(x,y,z):0);

						suma=static_cast<int>(round(sqrt(static_cast<double>(Sqr(xyz-((in.Include(x-1,y,z))?in.GetVoxel(x-1,y,z):0))+
							Sqr(xyz-((in.Include(x,y-1,z))?in.GetVoxel(x,y-1,z):0))))));

						if(suma>std::numeric_limits<VOXEL>::max()) suma=std::numeric_limits<VOXEL>::max();

						out.SetVoxel(x,y,z,static_cast<VOXEL>(suma));
					}
	}
	#if LEVEL >= 1
	cout<<"Leaving ApplyEstimateGradient1_2D"<<endl;
	#endif
}

template <class VOXEL> void EstimateGradient1_2D(const Image3d<VOXEL> &in,Image3d<VOXEL> &out,bool clamp){
	out.CopyMetaData(in);

	#if LEVEL >= 1
	cout<<"Entering EstimateGradient1_2D"<<endl;
	#endif

	ApplyEstimateGradient1_2D(in,out,clamp);

	#if LEVEL >= 1
	cout<<"Leaving EstimateGradient1_2D"<<endl;
	#endif
}

/* Tato funkce aplikuje anisotropni verzi 2d filtru odhadu gradientu na vstupni
   obraz 'in'.
*/
template <> 
void ApplyEstimateGradient1_2D(const Image3d<float> &in,Image3d<float> &out,bool clamp){
	size_t  x,y,z;
	float   suma;
	float   xyz;
	Vector3d<float> resolution,distance;
	Resolution  r;

	#if LEVEL >= 1
	cout<<"Entering ApplyEstimateGradient1_2D"<<endl;
	#endif

	r=in.GetResolution();
	resolution=r.GetRes();
	distance.x=1.0/resolution.x;
	distance.y=1.0/resolution.y;
	distance.z=1.0/resolution.z;

	#if LEVEL >= 2
	cout<<"Distance of voxels in axis x: "<<distance.x<<endl;
	cout<<"Distance of voxels in axis y: "<<distance.y<<endl;
	cout<<"Distance of voxels in axis z: "<<distance.z<<endl;
	#endif

	if(clamp){
			for(z=0;z<in.GetSizeZ();z++)
				for(y=0;y<in.GetSizeY();y++)
					for(x=0;x<in.GetSizeX();x++){

						xyz=((in.Include(x,y,z))?in.GetVoxel(x,y,z):0);

						suma=static_cast<float>(sqrt(static_cast<double>( Sqr((xyz-((in.Include(x-1,y,z))?in.GetVoxel(x-1,y,z):xyz))/distance.x)+
							Sqr((xyz-((in.Include(x,y-1,z))?in.GetVoxel(x,y-1,z):xyz))/distance.y))));

						if(suma>std::numeric_limits<float>::max()) suma=std::numeric_limits<float>::max();

						out.SetVoxel(x,y,z,suma);
					}
	}
	else{
			for(z=0;z<in.GetSizeZ();z++)
				for(y=1;y<in.GetSizeY();y++)
					for(x=1;x<in.GetSizeX();x++){

						xyz=((in.Include(x,y,z))?in.GetVoxel(x,y,z):0);

						suma=static_cast<float>(sqrt(static_cast<double>( Sqr((xyz-((in.Include(x-1,y,z))?in.GetVoxel(x-1,y,z):0))/distance.x)+
							Sqr((xyz-((in.Include(x,y-1,z))?in.GetVoxel(x,y-1,z):0))/distance.y))));

						if(suma>std::numeric_limits<float>::max()) suma=std::numeric_limits<float>::max();

						out.SetVoxel(x,y,z,suma);
					}
	}

	#if LEVEL >= 1
	cout<<"Leaving ApplyEstimateGradient1_2D"<<endl;
	#endif
}

template <> void I3D_DLLEXPORT EstimateGradient1_2D(const Image3d<RGB> &in,Image3d<RGB> &out,bool clamp){
	Image3d<GRAY8>  inR,inG,inB,outR,outG,outB;

	out.CopyMetaData(in);

	#if LEVEL >= 1
	cout<<"Entering EstimateGradient1_2D"<<endl;
	#endif

	inR.MakeRoom(in.GetSize());
	inG.MakeRoom(in.GetSize());
	inB.MakeRoom(in.GetSize());
	outR.MakeRoom(in.GetSize());
	outG.MakeRoom(in.GetSize());
	outB.MakeRoom(in.GetSize());

	RGBtoGray(in,inR,inG,inB);

	ApplyEstimateGradient1_2D(inR,outR,clamp);
	ApplyEstimateGradient1_2D(inG,outG,clamp);
	ApplyEstimateGradient1_2D(inB,outB,clamp);

	GrayToRGB(outR,outG,outB,out);

	#if LEVEL >= 1
	cout<<"Leaving EstimateGradient1_2D"<<endl;
	#endif
}

template <> void I3D_DLLEXPORT EstimateGradient1_2D(const Image3d<RGB16> &in,Image3d<RGB16> &out,bool clamp){
	Image3d<GRAY16> inR,inG,inB,outR,outG,outB;

	out.CopyMetaData(in);

	#if LEVEL >= 1
	cout<<"Entering EstimateGradient1_2D"<<endl;
	#endif

	inR.MakeRoom(in.GetSize());
	inG.MakeRoom(in.GetSize());
	inB.MakeRoom(in.GetSize());
	outR.MakeRoom(in.GetSize());
	outG.MakeRoom(in.GetSize());
	outB.MakeRoom(in.GetSize());

	RGBtoGray(in,inR,inG,inB);

	ApplyEstimateGradient1_2D(inR,outR,clamp);
	ApplyEstimateGradient1_2D(inG,outG,clamp);
	ApplyEstimateGradient1_2D(inB,outB,clamp);

	GrayToRGB(outR,outG,outB,out);

	#if LEVEL >= 1
	cout<<"Leaving EstimateGradient1_2D"<<endl;
	#endif
}

/* END EstimateGradient1_2D edge detector */

/* BEGIN EstimateGradient2_2D edge detector */

/* Tato funkce aplikuje 2d filtr odhadu gradientu na vstupni obraz 'in'.
*/
template <class VOXEL> 
static void ApplyEstimateGradient2_2D(const Image3d<VOXEL> &in,Image3d<VOXEL> &out,bool clamp){
	size_t  x,y,z;
	int     suma;

	#if LEVEL >= 1
	cout<<"Entering ApplyEstimateGradient2_2D"<<endl;
	#endif

	if(clamp){
			for(z=0;z<in.GetSizeZ();z++)
				for(y=0;y<in.GetSizeY();y++)
					for(x=0;x<in.GetSizeX();x++){

						suma=static_cast<int>(round(sqrt(static_cast<double>( Sqr(((in.Include(x+1,y,z))?in.GetVoxel(x+1,y,z):in.GetVoxel(x,y,z))
							-((in.Include(x-1,y,z))?in.GetVoxel(x-1,y,z):in.GetVoxel(x,y,z)))+
							Sqr(((in.Include(x,y+1,z))?in.GetVoxel(x,y+1,z):in.GetVoxel(x,y,z))
							-((in.Include(x,y-1,z))?in.GetVoxel(x,y-1,z):in.GetVoxel(x,y,z)))))));

						if(suma>std::numeric_limits<VOXEL>::max()) suma=std::numeric_limits<VOXEL>::max();

						out.SetVoxel(x,y,z,static_cast<VOXEL>(suma));
					}
	}
	else{
			for(z=0;z<in.GetSizeZ();z++)
				for(y=1;y<in.GetSizeY()-1;y++)
					for(x=1;x<in.GetSizeX()-1;x++){

						suma=static_cast<int>(round(sqrt(static_cast<double>( Sqr(((in.Include(x+1,y,z))?in.GetVoxel(x+1,y,z):0.0)
							-((in.Include(x-1,y,z))?in.GetVoxel(x-1,y,z):0.0))+
							Sqr(((in.Include(x,y+1,z))?in.GetVoxel(x,y+1,z):0.0)
							-((in.Include(x,y-1,z))?in.GetVoxel(x,y-1,z):0.0))))));

						if(suma>std::numeric_limits<VOXEL>::max()) suma=std::numeric_limits<VOXEL>::max();

						out.SetVoxel(x,y,z,static_cast<VOXEL>(suma));
					}
	}

	#if LEVEL >= 1
	cout<<"Leaving ApplyEstimateGradient2_2D"<<endl;
	#endif
}

template <class VOXEL> void EstimateGradient2_2D(const Image3d<VOXEL> &in,Image3d<VOXEL> &out,bool clamp){
	out.CopyMetaData(in);

	#if LEVEL >= 1
	cout<<"Entering EstimateGradient2_2D"<<endl;
	#endif

	ApplyEstimateGradient2_2D(in,out,clamp);

	#if LEVEL >= 1
	cout<<"Leaving EstimateGradient2_2D"<<endl;
	#endif
}

/* Tato funkce aplikuje anisotropni verzi 2d filtru odhadu gradientu na vstupni
   obraz 'in'.
*/
template <> 
void ApplyEstimateGradient2_2D(const Image3d<float> &in,Image3d<float> &out,bool clamp){
	size_t  x,y,z;
	float   suma;
	Vector3d<float> resolution,distance;
	Resolution  r;

	#if LEVEL >= 1
	cout<<"Entering ApplyEstimateGradient2_2D"<<endl;
	#endif

	r=in.GetResolution();
	resolution=r.GetRes();
	distance.x=1.0/resolution.x;
	distance.y=1.0/resolution.y;
	distance.z=1.0/resolution.z;

	#if LEVEL >= 2
	cout<<"Distance of voxels in axis x: "<<distance.x<<endl;
	cout<<"Distance of voxels in axis y: "<<distance.y<<endl;
	cout<<"Distance of voxels in axis z: "<<distance.z<<endl;
	#endif

	if(clamp){
			for(z=0;z<in.GetSizeZ();z++)
				for(y=0;y<in.GetSizeY();y++)
					for(x=0;x<in.GetSizeX();x++){

						suma=static_cast<float>(sqrt(static_cast<double>( Sqr((((in.Include(x+1,y,z))?in.GetVoxel(x+1,y,z):in.GetVoxel(x,y,z))
							-((in.Include(x-1,y,z))?in.GetVoxel(x-1,y,z):in.GetVoxel(x,y,z)))/(2*distance.x))+
							Sqr((((in.Include(x,y+1,z))?in.GetVoxel(x,y+1,z):in.GetVoxel(x,y,z))
							-((in.Include(x,y-1,z))?in.GetVoxel(x,y-1,z):in.GetVoxel(x,y,z)))/(2*distance.y)))));

						if(suma>std::numeric_limits<float>::max()) suma=std::numeric_limits<float>::max();

						out.SetVoxel(x,y,z,suma);
					}
	}
	else{
			for(z=0;z<in.GetSizeZ();z++)
				for(y=1;y<in.GetSizeY()-1;y++)
					for(x=1;x<in.GetSizeX()-1;x++){

						suma=static_cast<float>(sqrt(static_cast<double>( Sqr((((in.Include(x+1,y,z))?in.GetVoxel(x+1,y,z):0)
							-((in.Include(x-1,y,z))?in.GetVoxel(x-1,y,z):0))/(2*distance.x))+
							Sqr((((in.Include(x,y+1,z))?in.GetVoxel(x,y+1,z):0)
							-((in.Include(x,y-1,z))?in.GetVoxel(x,y-1,z):0))/(2*distance.y)))));

						if(suma>std::numeric_limits<float>::max()) suma=std::numeric_limits<float>::max();

						out.SetVoxel(x,y,z,suma);
					}
	}

	#if LEVEL >= 1
	cout<<"Leaving ApplyEstimateGradient2_2D"<<endl;
	#endif
}

template <> I3D_DLLEXPORT void EstimateGradient2_2D(const Image3d<RGB> &in,Image3d<RGB> &out,bool clamp){
	Image3d<GRAY8>  inR,inG,inB,outR,outG,outB;

	out.CopyMetaData(in);

	#if LEVEL >= 1
	cout<<"Entering EstimateGradient2_2D"<<endl;
	#endif

	inR.MakeRoom(in.GetSize());
	inG.MakeRoom(in.GetSize());
	inB.MakeRoom(in.GetSize());
	outR.MakeRoom(in.GetSize());
	outG.MakeRoom(in.GetSize());
	outB.MakeRoom(in.GetSize());

	RGBtoGray(in,inR,inG,inB);

	ApplyEstimateGradient2_2D(inR,outR,clamp);
	ApplyEstimateGradient2_2D(inG,outG,clamp);
	ApplyEstimateGradient2_2D(inB,outB,clamp);

	GrayToRGB(outR,outG,outB,out);

	#if LEVEL >= 1
	cout<<"Leaving EstimateGradient2_2D"<<endl;
	#endif
}

template <> I3D_DLLEXPORT 
void EstimateGradient2_2D(const Image3d<RGB16> &in,Image3d<RGB16> &out,bool clamp)
{
	Image3d<GRAY16> inR,inG,inB,outR,outG,outB;

	out.CopyMetaData(in);

	#if LEVEL >= 1
	cout<<"Entering EstimateGradient2_2D"<<endl;
	#endif

	inR.MakeRoom(in.GetSize());
	inG.MakeRoom(in.GetSize());
	inB.MakeRoom(in.GetSize());
	outR.MakeRoom(in.GetSize());
	outG.MakeRoom(in.GetSize());
	outB.MakeRoom(in.GetSize());

	RGBtoGray(in,inR,inG,inB);

	ApplyEstimateGradient2_2D(inR,outR,clamp);
	ApplyEstimateGradient2_2D(inG,outG,clamp);
	ApplyEstimateGradient2_2D(inB,outB,clamp);

	GrayToRGB(outR,outG,outB,out);

	#if LEVEL >= 1
	cout<<"Leaving EstimateGradient2_2D"<<endl;
	#endif
}

/* END EstimateGradient2_2D edge detector */

/***************************************************************************/

/* Explicit instantiations: */

template I3D_DLLEXPORT void Robinson3D(const Image3d<GRAY8> &in,Image3d<GRAY8> &out);
template I3D_DLLEXPORT void Robinson3D(const Image3d<GRAY16> &in,Image3d<GRAY16> &out);
template I3D_DLLEXPORT void Robinson3D(const Image3d<BINARY> &in,Image3d<BINARY> &out);
template I3D_DLLEXPORT void Robinson3D(const Image3d<float> &in,Image3d<float> &out);
template I3D_DLLEXPORT void CompassSobel3D(const Image3d<GRAY8> &in,Image3d<GRAY8> &out);
template I3D_DLLEXPORT void CompassSobel3D(const Image3d<GRAY16> &in,Image3d<GRAY16> &out);
template I3D_DLLEXPORT void CompassSobel3D(const Image3d<BINARY> &in,Image3d<BINARY> &out);
template I3D_DLLEXPORT void CompassSobel3D(const Image3d<float> &in,Image3d<float> &out);
template I3D_DLLEXPORT void Prewitt3D(const Image3d<GRAY8> &in,Image3d<GRAY8> &out);
template I3D_DLLEXPORT void Prewitt3D(const Image3d<GRAY16> &in,Image3d<GRAY16> &out);
template I3D_DLLEXPORT void Prewitt3D(const Image3d<BINARY> &in,Image3d<BINARY> &out);
template I3D_DLLEXPORT void Prewitt3D(const Image3d<float> &in,Image3d<float> &out);
template I3D_DLLEXPORT void Kirsch3D(const Image3d<GRAY8> &in,Image3d<GRAY8> &out);
template I3D_DLLEXPORT void Kirsch3D(const Image3d<GRAY16> &in,Image3d<GRAY16> &out);
template I3D_DLLEXPORT void Kirsch3D(const Image3d<BINARY> &in,Image3d<BINARY> &out);
template I3D_DLLEXPORT void Kirsch3D(const Image3d<float> &in,Image3d<float> &out);
template I3D_DLLEXPORT void ShenCastan3D(const Image3d<GRAY8> &in,Image3d<GRAY8> &out,double p,int WinHalfSize);
template I3D_DLLEXPORT void ShenCastan3D(const Image3d<GRAY16> &in,Image3d<GRAY16> &out,double p,int WinHalfSize);
template I3D_DLLEXPORT void ShenCastan3D(const Image3d<float> &in,Image3d<float> &out,double p,int WinHalfSize);
template I3D_DLLEXPORT void ShenCastan2D(const Image3d<GRAY8> &in,Image3d<GRAY8> &out,double p,int WinHalfSize);
template I3D_DLLEXPORT void ShenCastan2D(const Image3d<GRAY16> &in,Image3d<GRAY16> &out,double p,int WinHalfSize);
template I3D_DLLEXPORT void ShenCastan2D(const Image3d<float> &in,Image3d<float> &out,double p,int WinHalfSize);
template I3D_DLLEXPORT void MarrHildreth3D(const Image3d<GRAY8> &in,Image3d<GRAY8> &out,double sigma1,double sigma2);
template I3D_DLLEXPORT void MarrHildreth3D(const Image3d<GRAY16> &in,Image3d<GRAY16> &out,double sigma1,double sigma2);
template I3D_DLLEXPORT void MarrHildreth2D(const Image3d<GRAY8> &in,Image3d<GRAY8> &out,double sigma1,double sigma2);
template I3D_DLLEXPORT void MarrHildreth2D(const Image3d<GRAY16> &in,Image3d<GRAY16> &out,double sigma1,double sigma2);
template I3D_DLLEXPORT void Laplace3D(const Image3d<GRAY8> &in,Image3d<GRAY8> &out);
template I3D_DLLEXPORT void Laplace3D(const Image3d<GRAY16> &in,Image3d<GRAY16> &out);
template I3D_DLLEXPORT void Laplace3D(const Image3d<BINARY> &in,Image3d<BINARY> &out);
template I3D_DLLEXPORT void Laplace3D(const Image3d<float> &in,Image3d<float> &out);
template I3D_DLLEXPORT void Roberts3D(const Image3d<GRAY8> &in,Image3d<GRAY8> &out);
template I3D_DLLEXPORT void Roberts3D(const Image3d<GRAY16> &in,Image3d<GRAY16> &out);
template I3D_DLLEXPORT void Roberts3D(const Image3d<BINARY> &in,Image3d<BINARY> &out);
template I3D_DLLEXPORT void Roberts3D(const Image3d<float> &in,Image3d<float> &out);
template I3D_DLLEXPORT void EstimateGradient1_3D(const Image3d<GRAY8> &in,Image3d<GRAY8> &out,bool clamp);
template I3D_DLLEXPORT void EstimateGradient1_3D(const Image3d<GRAY16> &in,Image3d<GRAY16> &out,bool clamp);
template I3D_DLLEXPORT void EstimateGradient1_3D(const Image3d<BINARY> &in,Image3d<BINARY> &out,bool clamp);
template I3D_DLLEXPORT void EstimateGradient2_3D(const Image3d<GRAY8> &in,Image3d<GRAY8> &out,bool clamp);
template I3D_DLLEXPORT void EstimateGradient2_3D(const Image3d<float> &in,Image3d<float> &out,bool clamp);
template I3D_DLLEXPORT void EstimateGradient2_3D(const Image3d<GRAY16> &in,Image3d<GRAY16> &out,bool clamp);
template I3D_DLLEXPORT void EstimateGradient2_3D(const Image3d<BINARY> &in,Image3d<BINARY> &out,bool clamp);
template I3D_DLLEXPORT void Sobel3D(const Image3d<GRAY8> &in,Image3d<GRAY8> &out);
template I3D_DLLEXPORT void Sobel3D(const Image3d<GRAY16> &in,Image3d<GRAY16> &out);
template I3D_DLLEXPORT void Sobel3D(const Image3d<BINARY> &in,Image3d<BINARY> &out);
template I3D_DLLEXPORT void Sobel3D(const Image3d<float> &in,Image3d<float> &out);
template I3D_DLLEXPORT void AnisoSobel3D(const Image3d<GRAY8> &in,Image3d<GRAY8> &out);
template I3D_DLLEXPORT void AnisoSobel3D(const Image3d<GRAY16> &in,Image3d<GRAY16> &out);
template I3D_DLLEXPORT void AnisoSobel3D(const Image3d<float> &in,Image3d<float> &out);
template I3D_DLLEXPORT void hystThres3D(Image3d<GRAY8> &data,unsigned short lowThreshold,unsigned short highThreshold);
template I3D_DLLEXPORT void hystThres3D(Image3d<GRAY16> &data,unsigned short lowThreshold,unsigned short highThreshold);
template I3D_DLLEXPORT void hystThres3D(Image3d<float> &data,unsigned short lowThreshold,unsigned short highThreshold);
template I3D_DLLEXPORT void hystThres2D(Image3d<GRAY8> &data,unsigned short lowThreshold,unsigned short highThreshold);
template I3D_DLLEXPORT void hystThres2D(Image3d<GRAY16> &data,unsigned short lowThreshold,unsigned short highThreshold);
template I3D_DLLEXPORT void hystThres2D(Image3d<float> &data,unsigned short lowThreshold,unsigned short highThreshold);

#ifdef WITH_FFTW
template I3D_DLLEXPORT void Canny3D(const Image3d<GRAY8> &in,Image3d<GRAY8> &out,float sigma,float scale);
template I3D_DLLEXPORT void Canny3D(const Image3d<GRAY16> &in,Image3d<GRAY16> &out,float sigma,float scale);
template I3D_DLLEXPORT void Canny3D(const Image3d<float> &in,Image3d<float> &out,float sigma,float scale);
template I3D_DLLEXPORT void Canny2D(const Image3d<GRAY8> &in,Image3d<GRAY8> &out,float sigma,float scale);
template I3D_DLLEXPORT void Canny2D(const Image3d<GRAY16> &in,Image3d<GRAY16> &out,float sigma,float scale);
template I3D_DLLEXPORT void Canny2D(const Image3d<float> &in,Image3d<float> &out,float sigma,float scale);
#endif //fftw

template I3D_DLLEXPORT void Sobel2D(const Image3d<GRAY8> &in,Image3d<GRAY8> &out);
template I3D_DLLEXPORT void Sobel2D(const Image3d<GRAY16> &in,Image3d<GRAY16> &out);
template I3D_DLLEXPORT void Sobel2D(const Image3d<BINARY> &in,Image3d<BINARY> &out);
template I3D_DLLEXPORT void Sobel2D(const Image3d<float> &in,Image3d<float> &out);
template I3D_DLLEXPORT void Laplace2D(const Image3d<GRAY8> &in,Image3d<GRAY8> &out);
template I3D_DLLEXPORT void Laplace2D(const Image3d<GRAY16> &in,Image3d<GRAY16> &out);
template I3D_DLLEXPORT void Laplace2D(const Image3d<BINARY> &in,Image3d<BINARY> &out);
template I3D_DLLEXPORT void Laplace2D(const Image3d<float> &in,Image3d<float> &out);
template I3D_DLLEXPORT void Kirsch2D(const Image3d<GRAY8> &in,Image3d<GRAY8> &out);
template I3D_DLLEXPORT void Kirsch2D(const Image3d<GRAY16> &in,Image3d<GRAY16> &out);
template I3D_DLLEXPORT void Kirsch2D(const Image3d<BINARY> &in,Image3d<BINARY> &out);
template I3D_DLLEXPORT void Kirsch2D(const Image3d<float> &in,Image3d<float> &out);
template I3D_DLLEXPORT void Prewitt2D(const Image3d<GRAY8> &in,Image3d<GRAY8> &out);
template I3D_DLLEXPORT void Prewitt2D(const Image3d<GRAY16> &in,Image3d<GRAY16> &out);
template I3D_DLLEXPORT void Prewitt2D(const Image3d<BINARY> &in,Image3d<BINARY> &out);
template I3D_DLLEXPORT void Prewitt2D(const Image3d<float> &in,Image3d<float> &out);
template I3D_DLLEXPORT void Robinson2D(const Image3d<GRAY8> &in,Image3d<GRAY8> &out);
template I3D_DLLEXPORT void Robinson2D(const Image3d<GRAY16> &in,Image3d<GRAY16> &out);
template I3D_DLLEXPORT void Robinson2D(const Image3d<BINARY> &in,Image3d<BINARY> &out);
template I3D_DLLEXPORT void Robinson2D(const Image3d<float> &in,Image3d<float> &out);
template I3D_DLLEXPORT void CompassSobel2D(const Image3d<GRAY8> &in,Image3d<GRAY8> &out);
template I3D_DLLEXPORT void CompassSobel2D(const Image3d<GRAY16> &in,Image3d<GRAY16> &out);
template I3D_DLLEXPORT void CompassSobel2D(const Image3d<BINARY> &in,Image3d<BINARY> &out);
template I3D_DLLEXPORT void CompassSobel2D(const Image3d<float> &in,Image3d<float> &out);
template I3D_DLLEXPORT void Roberts2D(const Image3d<GRAY8> &in,Image3d<GRAY8> &out);
template I3D_DLLEXPORT void Roberts2D(const Image3d<GRAY16> &in,Image3d<GRAY16> &out);
template I3D_DLLEXPORT void Roberts2D(const Image3d<BINARY> &in,Image3d<BINARY> &out);
template I3D_DLLEXPORT void Roberts2D(const Image3d<float> &in,Image3d<float> &out);
template I3D_DLLEXPORT void EstimateGradient1_2D(const Image3d<GRAY8> &in,Image3d<GRAY8> &out,bool clamp);
template I3D_DLLEXPORT void EstimateGradient1_2D(const Image3d<GRAY16> &in,Image3d<GRAY16> &out,bool clamp);
template I3D_DLLEXPORT void EstimateGradient1_2D(const Image3d<BINARY> &in,Image3d<BINARY> &out,bool clamp);
template I3D_DLLEXPORT void EstimateGradient1_2D(const Image3d<float> &in,Image3d<float> &out,bool clamp);
template I3D_DLLEXPORT void EstimateGradient2_2D(const Image3d<GRAY8> &in,Image3d<GRAY8> &out,bool clamp);
template I3D_DLLEXPORT void EstimateGradient2_2D(const Image3d<float> &in,Image3d<float> &out,bool clamp);
template I3D_DLLEXPORT void EstimateGradient2_2D(const Image3d<GRAY16> &in,Image3d<GRAY16> &out,bool clamp);
template I3D_DLLEXPORT void EstimateGradient2_2D(const Image3d<BINARY> &in,Image3d<BINARY> &out,bool clamp);

}
