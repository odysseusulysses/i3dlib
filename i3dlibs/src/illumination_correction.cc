/*
 * i3dlib - image manipulation library
 *
 * Copyright (C) 2000-2006   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

 /*
 * FILE: illumination_correction.cc, by Tom� �limar
 *
 * histogram manipulation
 *
 * TODO: needs to be amended and translated!!!
 * 
 */


#include <algorithm>
#include <cmath>
#include <cstdlib>
#include <fftw3.h>

#include "threshold.h"
#include "transform.h"

#include "illumination_correction.h"


#ifdef WITH_LAPACK
// C++ header of function dgels_ located in the LAPACK library
extern "C" void dgels_(const char &trans, const int &m, const int &n, const int &nrhs, double *a, const int &lda, double *b, const int &ldb, double *work, const int &lwork, int &info);
#endif


namespace i3d
{

//	Pomocne funkce pro Gaussovo vyhlazeni

//	Aplikuje masku mask na bod i v histogramu data a vrati vypocitany bod
	unsigned long aplicateMask( const Histogram &data, const size_t i, const std::vector<double> &mask )
	{
		double sum=0;
		double maskSize=0;
		for( size_t j=0; j<mask.size(); j++ )
		{
			if( (i-mask.size()/2+j)>=0 && (i-mask.size()/2+j)<data.size() )
			{
				sum += data[i-mask.size()/2+j] * mask[j];
				maskSize+=mask[j];
			}
		}

		if (maskSize==0)
			 maskSize++;

		return (unsigned long)round(sum/maskSize);
	}

//	Vygeneruje hustotu normovaneho Gaussova rozdeleni a vrati jako vector
	std::vector<double> generateGaussMask( const double sigma )
	{
		std::vector<double> mask, tmp;
		// Protoze Gaussovo rozdeleni je definovano od minus nekonecna do nekonecna je potreba nejaky limit
		const double limit=getGauss( 0, sigma ) / 1000;
		double sum=0;
		size_t i=0;

		// Vypocita rozdeleni pro pravou polovinu krivky
		while( getGauss( i, sigma ) > limit )
		{
			tmp.push_back( getGauss( i, sigma ) );
			i++;
		}

		// Zvetsi vektor a zkopiruje pravou polovinu krivky nalevo
		mask.resize( tmp.size()*2 -1 );
		for( size_t i=1; i<tmp.size(); i++ )
		{
			mask[i+tmp.size()-1] = tmp[i];
			mask[tmp.size()-i-1] = tmp[i];
		}
		mask[tmp.size()-1]=tmp[0];

		// Normuje
		for( size_t i=0; i<mask.size(); i++ ) sum+=mask[i];
		for( size_t i=0; i<mask.size(); i++ ) mask[i]/=sum;

		return mask;
	}

#ifdef WITH_LAPACK
///// Pomocne funkce pro funkci correctIlluminationBsplineAprox()

//  Aproximuje bspline plochu na pozadi obrazku img a do surface ulozi kontrolni body teto plochy. Bspline plochu nepocita.
//  Pozadi obrazku je definovano binarnim obrazkem background - do aproximace se pocitaji pouze body, ktere jsou v
//	background nastaveny na backgroudSign. Img, background a surface museji mit stejnou velikost.
	template<class T> bool fitBspline( const Image3d<T> &img, bSplineSurface &surface, const Image3d<BINARY> &background, BINARY backgroundSign )
	{

		if( img.GetSizeZ() != 1 || background.GetSizeZ() != 1 ) return false; // Fitovat pouze jednu vrstvu //

		const size_t sizeX=img.GetSizeX();
		const size_t sizeY=img.GetSizeY();
		const size_t size=img.GetSliceSize();
		size_t backgroundSize=0, bg=0, j=0;

		if( sizeX != background.GetSizeX() || sizeY != background.GetSizeY() ) return false;
		if( sizeX != surface.getSizeX() || sizeY != surface.getSizeY() ) return false;

		const size_t cpCountX=surface.getCpCountX();
		const size_t cpCountY=surface.getCpCountY();
		const size_t degree=surface.getDegree();

		const double incrementX = surface.computeIncrementX();
		const double incrementY = surface.computeIncrementY();
		const double *knotVector = surface.computeKnotVector( degree );

//	Vypocita, kolik bodu se bude aproximovat pro vypocet velikosti matic
		for( size_t i=0; i<size; i++ )
			if( background.GetVoxel(i) == backgroundSign ) backgroundSize++;

//  Nastaveni parametru pro funkci dgels z knihovny LAPACK. Vcetne originalni dokumentace.

// 1. If TRANS = 'N' and m >= n:  find the least squares solution of
//    an overdetermined system, i.e., solve the least squares problem
//                 minimize || B - A*X ||.
//
// 2. If TRANS = 'N' and m < n:  find the minimum norm solution of
//    an underdetermined system A * X = B.
//
// 3. If TRANS = 'T' and m >= n:  find the minimum norm solution of
//    an undetermined system A**T * X = B.
//
// 4. If TRANS = 'T' and m < n:  find the least squares solution of
//    an overdetermined system, i.e., solve the least squares problem
//                 minimize || B - A**T * X ||.
//
		char trans = 'N';
//
// M       (input) INTEGER
//         The number of rows of the matrix A.  M >= 0.
//
		int m=backgroundSize;
//
// N       (input) INTEGER
//         The number of columns of the matrix A.  N >= 0.
//
		int n=cpCountX*cpCountY;
//
// NRHS    (input) INTEGER
//         The number of right hand sides, i.e., the number of
//         columns of the matrices B and X. NRHS >=0.
//
		int nrhs=1;
//
// A       (input/output) DOUBLE PRECISION array, dimension (LDA,N)
//         On entry, the M-by-N matrix A.
//         On exit,
//           if M >= N, A is overwritten by details of its QR
//                      factorization as returned by DGEQRF;
//           if M <  N, A is overwritten by details of its LQ
//                      factorization as returned by DGELQF.
//
		double *a=NULL;
//
// LDA     (input) INTEGER
//         The leading dimension of the array A.  LDA >= max(1,M).
//
		int lda=std::max(1,m);
//
// B       (input/output) DOUBLE PRECISION array, dimension (LDB,NRHS)
//         On entry, the matrix B of right hand side vectors, stored
//         columnwise; B is M-by-NRHS if TRANS = 'N', or N-by-NRHS
//         if TRANS = 'T'.
//         On exit, if INFO = 0, B is overwritten by the solution
//         vectors, stored columnwise:
//         if TRANS = 'N' and m >= n, rows 1 to n of B contain the least
//         squares solution vectors; the residual sum of squares for the
//         solution in each column is given by the sum of squares of
//         elements N+1 to M in that column;
//         if TRANS = 'N' and m < n, rows 1 to N of B contain the
//         minimum norm solution vectors;
//         if TRANS = 'T' and m >= n, rows 1 to M of B contain the
//         minimum norm solution vectors;
//         if TRANS = 'T' and m < n, rows 1 to M of B contain the
//         least squares solution vectors; the residual sum of squares
//         for the solution in each column is given by the sum of
//         squares of elements M+1 to N in that column.
//
		double *b=NULL;
//
// LDB     (input) INTEGER
//         The leading dimension of the array B. LDB >= MAX(1,M,N).
//
		int ldb=std::max(1,std::max(m,n));
//
// WORK    (workspace/output) DOUBLE PRECISION array, dimension (MAX(1,LWORK))
//         On exit, if INFO = 0, WORK(1) returns the optimal LWORK.
//
		double *work;
		double optimalLWORK[1];
//
// LWORK   (input) INTEGER
//         The dimension of the array WORK.
//         LWORK >= max( 1, MN + max( MN, NRHS ) ).
//         For optimal performance,
//         LWORK >= max( 1, MN + max( MN, NRHS )*NB ).
//         where MN = min(M,N) and NB is the optimum block size.
//
//         If LWORK = -1, then a workspace query is assumed; the routine
//         only calculates the optimal size of the WORK array, returns
//         this value as the first entry of the WORK array, and no error
//         message related to LWORK is issued by XERBLA.
//
		int lwork=-1;
//
// INFO    (output) INTEGER
//         = 0:  successful exit
//         < 0:  if INFO = -i, the i-th argument had an illegal value
//         > 0:  if INFO =  i, the i-th diagonal element of the
//               triangular factor of A is zero, so that A does not have
//               full rank; the least squares solution could not be
//               computed.
//
		int info;

//	Vypocet optimalni velikosti pole WORK. Parametr lwork je nastaven na -1
		dgels_(trans, m, n, nrhs, a, lda, b, ldb, optimalLWORK, lwork, info );

//	Alokace pameti
		lwork = optimalLWORK[0];
		work = new double[lwork];
		a = new double[lda*n];
		b = new double[ldb*nrhs];

//	Naplneni matice a
		for( int cp=0; cp<cpCountX*cpCountY; cp++ )
		{
			for( int point=0, bg=0; point<size; point++ )
			{
				if( background.GetVoxel( point ) == backgroundSign ) a[cp*backgroundSize+(bg++)]=
					surface.baseFce( degree, incrementY*(point/sizeX), cp/cpCountX, knotVector )* 
					surface.baseFce( degree, incrementX*(point%sizeX), cp%cpCountY, knotVector );
			}
		}

//	Naplneni matice b
		for( size_t i=0; i<size; i++ )
		{
			if( background.GetVoxel( i ) == backgroundSign )
				b[j++]=img.GetVoxel(i);
		}


//	Vlastni aproximace plochy a vypocet kontrolnich bodu. Ty budou ulozeny v matici b
		dgels_(trans, m, n, nrhs, a, lda, b, ldb, work, lwork, info );

//		for( int i=0; i<cpCountX*cpCountY; i++)
//			std::cout<<b[i]<<" - ";
//		std::cout<<std::endl;

//	Jestlize funkce dgels_ probehla bez chyb nastavime vypocitane kontrolni body jako kontrolni body plochy surface.
		if( !info ) surface.setControlPoints( b );


		delete[] a;
		delete[] b;
		delete[] work;
		delete[] knotVector;

		return info?false:true;
	}


#else
	template<class T> bool fitBspline( const Image3d<T> &img, bSplineSurface &surface, const Image3d<BINARY> &background, BINARY backgroundSign )
	{
		i3d::InternalException("Function fitBspline needs LAPACK!");
		return false; // dummy code :-)
	}
#endif

	template <class T> double getRMS( T *data, size_t size )
	{
		unsigned long tmp=0;
		for( size_t i=0; i<size; i++ )
			tmp+=data[i]*data[i];
		return sqrt((double)tmp);
	}


	template <class T> size_t getThreshold( Image3d<T> &img )
	{
		bool useOtsu = false;
		Histogram hist;
		IntensityHist( img, hist );
	
		Image3d<GRAY8> s;
		s.MakeRoom(2000,200, 1);
		if( useOtsu )
		{
			return FindOtsuThreshold( hist );
		}
		else
		{
			std::valarray<unsigned long> data( hist.size() );
			std::valarray<double> data2( hist.size() );

			double sum=0;
			unsigned long hmax=hist.max();
			for(size_t i=0; i<hist.size(); i++ )
				data2[i]=hist[i]/(double)hmax;


			for(size_t i=0; i<data2.size(); i++ )
				sum+=data2[i];
			double prum=sum/data2.size();
			sum=0;
			for(size_t i=0; i<data2.size(); i++ )
			{
				sum+=((double)data2[i]-prum)*((double)data2[i]-prum);
			}

			double standardDeviation = std::sqrt(sum/data2.size());
			double sigma = 1.06 * standardDeviation * std::pow((double)data.size(), double(-1/5));

			std::vector<double> mask = generateGaussMask( sigma );


			for( size_t i=0; i<hist.size(); i++ )
			{
				data[i]=aplicateMask( hist, i, mask );
			}

			std::valarray<unsigned long> derivative( data.size() );
			for( size_t i=0; i<data.size()-1; i++ ) derivative[i]=(data[i+1] - data[i]);
			for( size_t i=0; i<data.size()-2; i++ ) derivative[i]=(derivative[i+1] - derivative[i]);
			derivative[derivative.size()-1]=0;
			derivative[derivative.size()-2]=0;

			unsigned long max=0;
			size_t maxi=0;
			for( size_t i=0; i<data.size(); i++ )
			{
				if( max<data[i] )
				{
					max=data[i];
					maxi=i;
				}
			}
			unsigned long lmin=max;
			size_t lmini=maxi;
			for( size_t i = maxi; i<derivative.size(); i++ )
			{
				if( lmin<=derivative[i] ) break;
				else
				{
					lmin=derivative[i];
					lmini=i;
				}
			}
			unsigned long lmax=0;
			size_t lmaxi=lmini;
			for( size_t i = lmini; i<derivative.size(); i++ )
			{
				if( lmax>=derivative[i] ) break;
				else
				{
					lmax=derivative[i];
					lmaxi=i;
				}
			}
			s.SaveImage("gauss.tif");
			return lmaxi;
		}
	}

//	
	template <class T> void thresholdBackground( Image3d<T> &in, Image3d<BINARY> &out )
	{
		const size_t threshold = getThreshold( in );
		for(size_t i=0; i<out.GetImageSize(); i++ )
		{
			out.SetVoxel( i, in.GetVoxel(i)<threshold?(BINARY)0:(BINARY)1 );
		}
	}

//	Pokusi se zjistit nerovnomerne podsviceni v obrazku o a toto vrati jako obrazek background. Zbyle parametry urcuji vlastnosti bspline
//	plochy - BSplineCpCountX pocet kontrolnich bodu v ose X, bSplineCpCountY v ose Y a bSplineDegree urcuje stupen plochy.
//	Jestlize vse probehlo v poradku vraci true, jinak false
	template <class T> bool estimateBackground( const Image3d<T> &o, Image3d<T> &background, int bSplineCpCountX, int bSplineCpCountY, int bSplineDegree  )
	{
		Image3d<T> t(o); // Pomocny obrazek. Uchovava prubezny stav aproximace pro prahovani
		Image3d<BINARY> d; // Binarni obrazek urcujici pozadi obrazku. Behem kazdeho prahovani se vytvari znova.
		Image3d<float> tmpBackground;

		const T voxelMax=std::numeric_limits<T>::max();
		const T voxelMin=std::numeric_limits<T>::min();

//		int min=voxelMax;

		const size_t sizeX=o.GetSizeX();
		const size_t sizeY=o.GetSizeY();
		const size_t size=sizeX*sizeY;

		double temp;

//	Delta urcuje maximalni rozdil mezi plochou vypocitanou v predchozim kroku a aktualni plochou. Jakmile tento rozdil bude mensi, vypocet se ukonci.
		const double delta = getRMS(o.GetFirstVoxelAddr(), o.GetImageSize() )/1000.0;

		bSplineSurface s( sizeX, sizeY, bSplineCpCountX, bSplineCpCountY, bSplineDegree );

		d.MakeRoom( sizeX, sizeY, 1 );
		t.MakeRoom( sizeX, sizeY, 1 );
		tmpBackground.MakeRoom( sizeX, sizeY, 1 );


//		0. Nulty krok algoritmu. Prvnim odhadem upraveneho obrazku t je cely puvodni obrazek o.
//			t_0(x) = o(x)
		for(size_t i=0; i<size; i++ )
		{
			d.SetVoxel( i, (BINARY)0 );
		}

		for( int k=0; k<20; k++ ) //if( sqrt((double)temp) < delta ) break;
		{
//			2.	Nafitovani uniformni B-spline plochy, metodou nejmensich
//				ctvercu, jednotlivych pixelu pozadi ( d ) v puvodnim
//				obrazku o. Kontrolni body vysledne plochy jsou ulozeny v s.
			if( !fitBspline( o, s, d ) ) return false;
			s.compute(); // Vypocet uniformni B-spline plochy

//			3.	Upraveni obrazku podle nafitovane plochy pozadi s
//				t(x) = o(x) - s(x)
			for( size_t j=0; j<size; j++ )
			{
				int p=(o.GetVoxel(j) - s.getPoint( j ));
				t.SetVoxel( j, p>voxelMax?voxelMax:p<voxelMin?voxelMin:p );
			}

//			Vypocet rozdilu mezi minulou plochou a novou plochou
			temp=0;
			for( size_t j=0; j<size; j++ )
			{
				float diff=(s.getPoint(j)-tmpBackground.GetVoxel(j));
				temp+=diff*diff;
			}

//			Uchovame si plochu pro pristi srovnani
			for( size_t j=0; j<size; j++ )
				tmpBackground.SetVoxel( j, s.getPoint( j ) );


//			4.	Pokud RMS(s_k - s_k-1) < delta tak ukoncime algoritmus
			if( sqrt((double)temp) < delta ) break;
			
//			1.	Odhad pozadi prahovanim opraveneho obrazku t.
//				Ziskame mnozinu d urcujici pozadi.
			thresholdBackground( t, d );
		}

//		Prevedeme spocitanou plochu na stupne sedi a ulozime do obrazku background
		FloatToGrayNoWeight( tmpBackground, background );

		return true;
	}



//	Funkce se pokusi odstranit nerovnomerne podsviceni momoci aproximace B-spline plochy na pixely
//	pozadi a nasledne upravy. Parametr in je puvodni obrazek, do parametru out se ulozi jiz opraveny.
//	Parametr previewPercent urcuje, o kolik procet se zmensi obrazek na kterem se budou provadet vypocty.
//	To umoznuje zvyseni rychlosti na ukor presnosti. Zbyle parametry urcuji vlastnosti plochy, ktera
//	reprezentuje pozadi. Pocet kontrolnich bodu v ose X a Y a stupen plochy.
	template < class T > bool correctIlluminationBsplineAprox(
		const Image3d<T> &in, Image3d<T> &out, int previewPercent,
		int bSplineCpCountX, int bSplineCpCountY, int bSplineDegree )
	{
		Image3d<T> background;
		float tmp;

		const T voxelMin=std::numeric_limits<T>::min();
		const T voxelMax=std::numeric_limits<T>::max();
		// int min = voxelMax;


//		Kontrola mezi
		if( bSplineDegree<1 || bSplineCpCountX<=bSplineDegree || bSplineCpCountY<=bSplineDegree ) return false;
		if( previewPercent < 1 ) previewPercent = 1;
		if( previewPercent > 100 ) previewPercent = 100;

		size_t newSizeX = in.GetSizeX()/(100/previewPercent);
		size_t newSizeY = in.GetSizeY()/(100/previewPercent);

		if( newSizeX<64 ) newSizeX=in.GetSizeX()<64?in.GetSizeX():64;
		if( newSizeY<64 ) newSizeY=in.GetSizeY()<64?in.GetSizeY():64;

		background.MakeRoom( in.GetSizeX(), in.GetSizeY(), 1 );

//		V pripade, ze chceme provest vypocet na celem obrazku
		if( previewPercent == 100 )
		{
			if( !estimateBackground( in, background, bSplineCpCountX, bSplineCpCountY, bSplineDegree )) return false;
		}
//		V pripade, ze chceme provest vypocet na zmensenem obrazku. Obrazek nejdrive zmesime, provedeme
//		vypocet a pote zvetsime na puvodni velikost.
		else
		{
			Image3d<T> resIn;
			Image3d<T> resBackground;
			Resize( in, resIn, newSizeX, newSizeY, in.GetSizeZ() );
			resBackground.MakeRoom( resIn.GetSizeX(), resIn.GetSizeY(), 1 );
			if(!estimateBackground( resIn, resBackground, bSplineCpCountX, bSplineCpCountY, bSplineDegree )) return false;
			Resize( resBackground, background, in.GetSizeX(), in.GetSizeY(), in.GetSizeZ() );
		}
		out.CopyMetaData( in );

//		Uprava obrazku podle ziskaneho pozadi
		for( size_t z=0; z<in.GetSizeZ(); z++ )
		{
			for( size_t x=0; x<in.GetSizeX(); x++ )
				for( size_t y=0; y<in.GetSizeY(); y++ )
				{
					tmp=(in.GetVoxel( x, y, z ) - background.GetVoxel( x, y, 0 ) );
					out.SetVoxel( x, y, z, (T)(tmp<voxelMin?voxelMin:tmp>voxelMax?voxelMax:tmp) );
				}
		}
		return true;
	}

	/////////////////////////////////////////////////////////////////
	//                                                             //
	//               Struct HomomorphicFilterParams                //
	//                                                             //
	/////////////////////////////////////////////////////////////////

/****************************************************************************/	

	HomomorphicFilterParams::HomomorphicFilterParams()
	{
		SetIdentity();
	}

/****************************************************************************/	

	HomomorphicFilterParams::~HomomorphicFilterParams()
	{
	}

/****************************************************************************/	

	void HomomorphicFilterParams::SetIdentity()
	{
		gamma_low = 1.0;
		gamma_high = 1.0;
		c = 1.0;
		D0 = 1.0;
	}

/****************************************************************************/	



//	Homomorfni filtrovani

	template <class T> void homFilter(Image3d<T> &in, 
												 Image3d<T> &out, 
												 const HomomorphicFilterParams &params)
	{
		out.CopyMetaData(in);

		size_t sizeX = in.GetSizeX();
		size_t sizeY = in.GetSizeY();

		if( sizeX%2 == 1 ) sizeX--;
		if( sizeY%2 == 1 ) sizeY--;

		const size_t sliceSize  = sizeX*sizeY;

		fftw_complex *fin, *fout;
		fftw_plan plan;
		double *H, D, dx, dy;

//	Alokace pameti

		fin=(fftw_complex*)  fftw_malloc(sizeof(fftw_complex) * sliceSize );
		fout=(fftw_complex*)   fftw_malloc(sizeof(fftw_complex) * sliceSize );
		H=(double*) fftw_malloc(sizeof(double) * sliceSize );

//	Prepare filter
		for(size_t y=0; y<sizeY; y++ )
			for(size_t x=0; x<sizeX; x++ )
			{
				dx=((double)x - (double)sizeX/2.0);
				dy=((double)y - (double)sizeY/2.0);
				D  = std::sqrt( dx*dx + dy*dy );
				H[y+sizeY*x]=(params.gamma_high-params.gamma_low)*
						  (1 - std::exp(-params.c*((D*D)/
							(params.D0*params.D0))))+params.gamma_low;
			}

//	Fourier Transform

		
		for(size_t z=0; z<in.GetSizeZ(); z++ )
		{

			plan = fftw_plan_dft_2d( sizeX, 
											 sizeY, 
											 (fftw_complex*)fin, 
											 (fftw_complex*)fout, 
											 FFTW_FORWARD, 
											 FFTW_ESTIMATE );

			for(size_t x=0; x<sizeX; x++ )
				for(size_t y=0; y<sizeY; y++ )
				{
					fin[y+sizeY*x][0] = (std::log((double)in.GetVoxel(x,y,z )+1.0)
												*std::pow(-1.0,(double)(x+y)));
					fin[y+sizeY*x][1] = 0.0;
				}

			fftw_execute( plan );

//	Inverse Fourier Transform

			plan = fftw_plan_dft_2d( sizeX, sizeY, 
											 (fftw_complex*)fin, 
											 (fftw_complex*)fout, 
											 FFTW_BACKWARD, 
											 FFTW_ESTIMATE );

			for(size_t x=0; x<sizeX; x++ )
				for(size_t y=0; y<sizeY; y++ )
				{
					fin[y+sizeY*x][0] = fout[y+sizeY*x][0]*H[y+sizeY*x];
					fin[y+sizeY*x][1] = fout[y+sizeY*x][1]*H[y+sizeY*x];
				}

			fftw_execute( plan );


//	Postprocessing

			for(size_t x=0; x<sizeX; x++ )
				for(size_t y=0; y<sizeY; y++ )
				{
					out.SetVoxel( x, y, z, 
								  (T)std::exp((fout[y+sizeY*x][0] / 
								(double)sliceSize)*std::pow(-1.0,(double)x+y) ));
				}

		}

		fftw_destroy_plan(plan);
		fftw_free(fin);
		fftw_free(fout);
		fftw_free(H);
	}

	template I3D_DLLEXPORT bool correctIlluminationBsplineAprox( 
		const Image3d<GRAY8> &in, Image3d<GRAY8> &out, 
		int previewPercent,	int bSplineCpCountX, int bSplineCpCountY, int bSplineDegree );
	template I3D_DLLEXPORT bool correctIlluminationBsplineAprox( 
		const Image3d<GRAY16> &in, Image3d<GRAY16> &out,
		int previewPercent,	int bSplineCpCountX, int bSplineCpCountY, int bSplineDegree );

	template I3D_DLLEXPORT void homFilter(Image3d<GRAY16> &in, Image3d<GRAY16> &out, const HomomorphicFilterParams &params);
	template I3D_DLLEXPORT void homFilter(Image3d<GRAY8> &in, Image3d<GRAY8> &out, const HomomorphicFilterParams &params);

}

