/*
 * i3dlib - image manipulation library
 *
 * Copyright (C) 2000-2006   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 * FILE: shape.cc
 *
 * implementation of functions from shape.h
 *
 * Zdenek Hrncir, 2004 (60514@mail.muni.cz)
 */


#ifdef __GNUG__
#pragma implementation
#endif

#include <iostream>
#ifndef _MSC_VER
#include <cmath>
#endif
#include <climits>

#include "image3d.h"
#include "neighbours.h"
#include "shape.h"

#ifdef WITH_FFTW
  #include <fftw3.h>
#endif

#ifdef WITH_LAPACK
	// C++ header of ssyev_ function located in the LAPACK library
	extern "C" void ssyev_(const char &jobz, const char &uplo, const int &n, float *a, const int &lda, float *w, 
						   float *work, const int &lwork, int &info);
#endif

using namespace std;

#ifdef I3D_DEBUG
  using std::cout;
  using std::cerr;
  using std::endl;
  #include <iomanip>
#endif

#define PI 3.14159

namespace i3d {

  template <class LABEL>
  Shape<LABEL>::Shape(const Image3d<LABEL> *_img, LABEL _label) :
	  img(_img),label(_label)
  {
     SwitchAllIsIndicatorsOff();
     is3D = ( img->GetNumSlices() > 1 );
     Resolution res = img->GetResolution();
     if ( is3D ) {
       isIsometric = ( res.GetX()==res.GetY() && res.GetX()==res.GetZ() );
     } else {
       isIsometric = (res.GetX()==res.GetY());
     }
     #ifdef I3D_DEBUG
       if (isIsometric) {
         cout << "Shape constructor: Isometric image. " ;
       } else {
         cout << "Shape constructor: NOT isometric image. " ;
       }
     #endif

   }

  template <class LABEL>
  void Shape<LABEL>::SwitchAllIsIndicatorsOff()
  {
     isVolume= isSurface= isSurfaceUm= isCentroid= isBoundary= isCurvature=
     isPrincipalAxes= isRectangularity= isPrincipalSymmetry= isNFDs2d=
     isBendingE= false;
     curvatureDist=0;
   }


  template <class LABEL>
  void Shape<LABEL>::EraseValues()
	{
     SwitchAllIsIndicatorsOff();
     volume = surface = 0;
     surfaceUm = 0;
     centroid = Vector3d<size_t>(0,0,0);
     boundary.clear();
     curvatureDist=0;
     curvature.clear();
     statMoments.clear();
     cStatMoments.clear();
     principalAxes.val1 = principalAxes.val2 = principalAxes.val3 = 0;
     rectangularity = symmetry1 = symmetry2 = symmetry3 = 0;
     fF = nEnergy = 0;
     bendingE=0;


     is3D = ( img->GetNumSlices() > 1 );
     Resolution res = img->GetResolution();
     if ( is3D ) {
       isIsometric = ( res.GetX()==res.GetY() && res.GetX()==res.GetZ() );
     } else {
       isIsometric = (res.GetX()==res.GetY());
     }
   }

  
  template <class LABEL>
  void Shape<LABEL>::SetImage(const Image3d<LABEL> *_img)
  {
     img = _img;
     EraseValues();
   }

  
  template <class LABEL>
  void Shape<LABEL>::SetLabel(const LABEL _label){
     label = _label;
     EraseValues();
  }

  /* computes volume in voxels (usable also for voxel area) */
  template <class LABEL>
  unsigned long Shape<LABEL>::Volume(){

    if (isVolume) {
      #ifdef I3D_DEBUG
        cout << "Volume ... using earlier calculated value(s)" << endl;
      #endif
      return volume;
    }
    unsigned long vol=0; //voxel sum
    size_t imgSize=img->GetImageSize();

    if (imgSize == 0) { return 0; } //image is empty

    const LABEL *first = img->GetFirstVoxelAddr();
    for( size_t i=0 ; i < imgSize; i++) {
      vol += (*(first+i)) == label ? 1 : 0;
    }

    isVolume=true;
    volume=vol;
    return vol;
  }

  /*computes area of 2D image in pixels*/
  template <class LABEL>
  unsigned long Shape<LABEL>::Area(){
    if (is3D) throw InternalException ("Shape::Area: "
                     "This is 2D function for one slice images. "
                     "Try calling Volume (or Surface if you ment that).");
    return Volume(); //area and volume are in voxels similar
  }


  /* computes area of 2D image in micrometers (um)  */
  template <class LABEL>
  float Shape<LABEL>::AreaUm(){
    if (is3D) throw InternalException ("Shape::AreaUm: "
          "This is 2D function for one slice images. "
          "Try calling VolumeUm (or SurfaceUm if you ment that).");
    Resolution res = img->GetResolution();
    return Volume() / res.GetX() / res.GetY() ;
  }

  /* computes volume of binary image in micrometers^3 (um),
    for 2D images it is area in micrometers^2 (calls Area) */
  template <class LABEL>
  float Shape<LABEL>::VolumeUm(){
    if (is3D) {
      Resolution res = img->GetResolution();
      return Volume() / res.GetX() / res.GetY() / res.GetZ() ;

    } else {
      #ifdef I3D_DEBUG
        cout << "VolumeUm: calling 2D version (area)" << endl;
      #endif
      return AreaUm();
    }
  }

  template <class LABEL>
  float Shape<LABEL>::Roundness(){
     float surf=SurfaceUm();
     float vol = VolumeUm();
	 float round = 0;
   if (is3D) {
     round = vol*vol*36*PI/(surf*surf*surf);
   } else {
     //return surf*surf/vol;
     round = vol*4*PI/(surf*surf);
   }
   round = (round>1.0)? 1:round; 
   return round;
  }

//****************** Finding Boundary *****************************************


  /* 2D_4 clockwise neighbourhood*/
  const int nb2D_4Cw[] = { 1, 0, 0,    0, 1, 0,   -1, 0, 0,    0, -1, 0};
  /* 2D_8 clockwise neighbourhood*/
  const int nb2D_8Cw[] = { 1, 0, 0,    1, 1, 0,    0, 1, 0,   -1, 1, 0,
                          -1, 0, 0,   -1, -1, 0,   0, -1, 0,   1, -1, 0};


  /* returns boundary, for 2D images the boundary sequence is in chain,
     i.e. bdr[i+1] and bdr[i] are neighbours - works only for nb2D_4, nb2D_8
  */
  template <class LABEL>
  Boundary Shape<LABEL>::GetBoundary2d(const Neighbourhood& nbh){
    if (is3D) {
      throw InternalException ("Shape::GetBoundary2d: "
                                "This is 2D version of GetBoundary usable "
                                "only for one slices images.");
    }
    Boundary bdr;
    size_t imgSize=img->GetImageSize();
    if ( imgSize==0 ) { return bdr; }

    // settings for implicit nb2D_4 neighbourhood (= 2D_8 connected
    // boundary - using 2D_8 clockwise NBhood ):
    bool isNb2D_4Cw = false;     // using  2D_4 clockwise NBhood ? NO
    //actually used clockwise boudary connection neighbourhood:
    const int *nb2DCw = nb2D_8Cw;
    //number of neighbours in  actually used clockwise neighbourhood:
    short nb2DCwSize=8;
    //increment in neighbourhood for new direction setting:
    short nb2DCwInc=5;

    // conditioned settings for nb2D_8 neighbourhood ( = 2D_4 connected
    // boundary - using 2D_4 clockwise NBhood ):
    if ( nbh.offset == nb2D_8.offset ) {
      isNb2D_4Cw = true;
      nb2DCw=nb2D_4Cw;
      nb2DCwSize=4;
      nb2DCwInc=3;
    }
      // check wheather it is one of nb2D_4 and nb2D_8 (=nb2D_4Cw)
    if ( !isNb2D_4Cw  &&  (nbh.offset != nb2D_4.offset) ){
      throw InternalException ("Shape::GetBoundary2d: "
                              "Boundary of 2D images can be processed"
                             "only with nb2D_4 and nb2D_8 neighbourhoods");
    }
    if (isBoundary && !isNb2D_4Cw) {
      #ifdef I3D_DEBUG
        cout << "GetBoundary 2D version... using earlier calculated value(s)"
             << endl;
      #endif
      return boundary;
    }

      /*najdu prvni hranicni voxel (vzhledem k usporadani Image3d
      je to prvni na ktery sekvencne narazim) */
    size_t i=0;      //tested voxel index
    while ( i < imgSize && !(img->GetVoxel(i) == label) ) { i++; }
    if ( !(i < imgSize) ) { return bdr; } // zadny objekt tam neni - konec

      //first and end boundary voxel:
    Vector3d<size_t> firstEnd(img->GetX(i), img->GetY(i), img->GetZ(i));
    bdr.push_back(firstEnd);

    /*  budu hledat ve smeru hodinovych rucicek sousedy a pvni na ktereho
        narazim prohlasim za dalsi
        hranicni a aktualni voxel, pokud na zadny nenarazim ,
        tj. je tam jen jeden voxel,
        pak podminka cyklu bude teke nevyhovujici a konec
    */
    Vector3d<size_t> actVox=firstEnd;  // aktualne zkoumany voxel
    int startNbIdx=0; //index of start neighbour in neighbourhood
    do {
      for ( int n = 0; n < nb2DCwSize; n++){
        int actNbIdx = (n+startNbIdx) % nb2DCwSize;  // index of actual neighour
            //actual neighbour:
        Vector3d<size_t> actNbr( actVox.x+nb2DCw[3*actNbIdx],
                           actVox.y+nb2DCw[3*actNbIdx+1], actVox.z );

        if ( img->Include(actNbr.x, actNbr.y, actNbr.z) &&
                               img->GetVoxel(actNbr) == label ) {
             // I found neighbour on border
          bdr.push_back(actNbr);
          actVox = actNbr;
          startNbIdx= (actNbIdx + nb2DCwInc) % nb2DCwSize;
          break;
        }
      }
    } while ( actVox !=firstEnd );
    if (bdr.size()>1) { bdr.pop_back(); } //nechci mit zacatek ,t.j konec 2x
    if (!isNb2D_4Cw) { //saving boundary
      isBoundary=true;
      boundary=bdr;
    }
    return bdr;
  }


  /* returns boundary, for 3D images
  */
  template <class LABEL>
  Boundary Shape<LABEL>::GetBoundary3d(const Neighbourhood& nbh){
    if ( !is3D) {
      throw InternalException ("Shape::GetBoundary3d: "
                                "This is 3D version of GetBoundary usable"
                                "only for images with several slices.");
    }

    bool isNb3D_6 = (nbh.offset == nb3D_6.offset);
    if (isBoundary && isNb3D_6) {
      #ifdef I3D_DEBUG
        cout << "GetBoundary 3D version... using earlier calculated value(s)"
             << endl;
      #endif
      return boundary;
    }
    Boundary bdr;
    vector<const LABEL *> nbs;  //neighbours
    typedef typename vector<const LABEL *>::iterator NbsIt; //iterator - neighbour
    NbsIt nb;

    if ( isNb3D_6 ){  // 3D_6 neighbourhood
      #ifdef I3D_DEBUG
        cout << "GetBoundary 3D version... using quick "
                "calculation of 3D_6 neighbourhood" << endl;
      #endif
      size_t sizeX = img->GetSizeX();
      size_t sizeY = img->GetSizeY();
      size_t sizeZ = img->GetSizeZ();
      for (size_t x=0 ; x < sizeX; x++) {
       for (size_t y=0 ; y < sizeY; y++) {
        for (size_t z=0 ; z < sizeZ; z++) {
          if ( img->GetVoxel(x,y,z) == label) {  //voxel objektu, pocitam sousedy:
            if ( img->OnBorder(x,y,z) ||
                 img->GetVoxel(x,y+1,z) != label ||
                 img->GetVoxel(x+1,y,z) != label ||
                 img->GetVoxel(x,y,z+1) != label ||
                 img->GetVoxel(x,y-1,z) != label ||
                 img->GetVoxel(x-1,y,z) != label ||
                 img->GetVoxel(x,y,z-1) != label
                ) {
              bdr.push_back( Vector3d<size_t>(x, y, z));
            }
          }
        }
       }
      }
    } else {  // generic neighbourhood
      #ifdef I3D_DEBUG
        cout << "GetBoundary 3D version... "
                "using version for generic neighbourhood" << endl;
      #endif
      size_t  valid=0, // jak dlouho mohu pouzivat MoveWindow
              shift=0, //o kolik musim posunout okno;
              nbhSize = nbh.size(); // number of neighbours in neighbourhood
      //sousedstvi voxelu hledam zrychlenou funkci MoveWindow,
      // jen pro nezbytne voxely:
      size_t imgSize=img->GetImageSize();
      for (size_t i=0 ; i < imgSize; i++) {
        if ( img->GetVoxel(i) == label ) {  //voxel objektu, zkoumam sousedy...
          if ( valid <=1  ) { // musim vypocitat sousedy znovu:
            valid = GetWindow( *img, img->GetX(i), img->GetY(i),
                               img->GetZ(i), nbh, nbs);
          } else { // staci posunout okno:
            MoveWindow(nbs, shift);
            valid--;
          }
           // ...a taky sousedy vyhodnotim:
          if (nbs.size() != nbhSize ) {
            bdr.push_back( Vector3d<size_t>(img->GetX(i), img->GetY(i),
                                                          img->GetZ(i)) );
          } else {
            nb = nbs.begin();
            while ( nb != nbs.end() && (**nb) == label){
              nb++;
            }
            if ( nb != nbs.end() ) {
              bdr.push_back( Vector3d<size_t>(img->GetX(i), img->GetY(i),
                                                            img->GetZ(i)) );
            }
          }
          shift=1; //v kazdem pripade bude eventualni pristi posun okna == 1
        } else { //nepatri-li voxel objektu, jdeme dal.
          if (valid) valid--; // ale zmensi se validita..
          shift++; // ..a zvetsi pristi posun okna.
        }
      }
    }
    if (isNb3D_6) {
      isBoundary=true;
      boundary=bdr;
    }
    return bdr;
  }

  /* public GetBoundary finding with implicit neighbourhoods - very quick
     nb2D_4 for 2D images and
     nb3D_6 for 3D images*/
     template <class LABEL>
  Boundary Shape<LABEL>::GetBoundary(){
    if (is3D) {
      #ifdef I3D_DEBUG
        cout << "GetBoundary: calling 3D version" << endl;
      #endif
      return GetBoundary3d(nb3D_6);
    } else {
      #ifdef I3D_DEBUG
        cout << "GetBoundary: calling 2D version" << endl;
      #endif
      return GetBoundary2d(nb2D_4);
    }
  }

  /* public GetBoundary finding with given neighbourhood
     nb2D_8 and nb2D_4 for 2D images allowed
     whatever neighbourhood for 3D images allowed*/
  template <class LABEL>
  Boundary Shape<LABEL>::GetBoundary(const Neighbourhood& nbh){
    if (is3D) {
      #ifdef I3D_DEBUG
        cout << "GetBoundary: calling 3D version" << endl;
      #endif
      return GetBoundary3d(nbh);
    } else {
      #ifdef I3D_DEBUG
        cout << "GetBoundary: calling 2D version" << endl;
      #endif
      return GetBoundary2d(nbh);
    }
  }

  /*returns Net Boundary for 3D images - it is graph -
    nodes are boundary voxels and edges are
    between neighbouring voxels */
  template <class LABEL>
  NetBoundary Shape<LABEL>::NetBoundary3d(){
    NetBoundary netBdr;
    Boundary bdr = GetBoundary();
    typedef Boundary::iterator BoundaryIt;
    typedef NetBoundary::iterator NetBoundaryIt;
    typedef VectContainer::const_iterator NeighboursIt;
    typedef Vector3d<size_t> V3d;

     // fill map with keys (boundary points):
    for (BoundaryIt it = bdr.begin(); it!=bdr.end(); it++) {
      NetBoundaryNode bdrNode;//boundary neighbours of boundary point- now empty
      pair < V3d, NetBoundaryNode > bdrPair( *it, bdrNode );
      pair<NetBoundaryIt, bool> succes = netBdr.insert( bdrPair );
    }

    //find neighbours of each boundary point:
    //vector<const LABEL *> nbs;  //3D_18 neighbours of boundary point
    //vector<const LABEL *>::iterator nb; //iterator - neighbour
    for (NetBoundaryIt bdrPair = netBdr.begin();
                       bdrPair != netBdr.end(); bdrPair++){
      V3d  vox = bdrPair->first;  //actual boundary voxel
        //boundary neighbours of boundary point
      BoundaryNeighbours *bdrNbs = &(bdrPair->second.boundaryNeighbours);

      //GetWindow( *img, vox.x, vox.y, vox.z, nb3D_18, nbs);
      if ( !img->OnBorder(vox.x, vox.y, vox.z) ){ // voxel isn't on image border
          // get through all neighbours in nb3D_18 neighbourhood:
        for (NeighboursIt nbs = nb3D_18.offset.begin();
                                         nbs != nb3D_18.offset.end(); nbs++){
          V3d neighbour(vox.x + nbs->x, vox.y + nbs->y, vox.z + nbs->z);
          if ( img->GetVoxel(neighbour) == label){
            //boundary neighbour node:
            NetBoundaryIt nbrPair = netBdr.find( neighbour );
            if ( nbrPair != netBdr.end() ) {
              bdrNbs->push_back( &(nbrPair->first) );
              #ifdef I3D_DEBUG
                //cout << "NetBoundary: inserting neighbour" << endl;
              #endif
            }
          }
          //secti vox s *nbsIt, zjisti jestli ten vektor ukazuje na objekt,
          // jestli je na hranici (v nBdr) a pokud ano, jeho adresu (z nBdr)
          //  zapis na konec vektoru hranicnich sousedu (bdrNbs)
        }
      } else {  // voxel on border, I must check neighbours validity first
        vector<bool> valid;
        GetNbh(*img, vox.x, vox.y, vox.z, nb3D_18, valid);

        size_t i;
        NeighboursIt nbs;
        for ( nbs = nb3D_18.offset.begin(), i = 0;
              nbs != nb3D_18.offset.end();   nbs++, ++i){
          if ( valid[i] ) {
            V3d neighbour(vox.x + nbs->x, vox.y + nbs->y, vox.z + nbs->z);
            if ( img->GetVoxel(neighbour) == label){
              NetBoundaryIt nbrPair = netBdr.find( neighbour );
	      if ( nbrPair != netBdr.end() ) {
	        bdrNbs->push_back( &(nbrPair->first) );
	      }
	    }
          }
        }
      }
    }

    return netBdr;
  }


//****************** calculating surface/perimeter *****************************************

  /* returns perimeter constructed by given neighbourhood
    (implicitly nb2D_4), only for 2D images*/
  template <class LABEL>
  size_t Shape<LABEL>::Perimeter(const Neighbourhood& nbh){
    if (is3D) throw InternalException ("Shape::Perimeter: "
                                "This is 2D function for one slice images. "
				"Try calling Surface.");
    Boundary bdr = GetBoundary(nbh);
    size_t surf = bdr.size();
    // clear non-implicit 2D boundary
    if (nbh.offset != nb2D_4.offset) bdr.clear();
    return surf;
  }

  /* computes surface/perimeter of binary image in voxels by boundary
     constructor implicit neighbourhoods - quick
     nb2D_4 for 2D images and
     nb3D_6 for 3D images     */
  template <class LABEL>
  size_t Shape<LABEL>::Surface(){
    Boundary bdr = GetBoundary();
    size_t surf = bdr.size();
    return surf;
  }


  // computes surface of binary image in voxels by given neighbourhood
  // calls boundary constructor
  template <class LABEL>
  size_t Shape<LABEL>::Surface(const Neighbourhood& nbh){
    if ( is3D ) { //3D
      Boundary bdr = GetBoundary(nbh);
      size_t surf = bdr.size();
      // clear non-implicit 3D boundary
      if (nbh.offset != nb3D_6.offset) bdr.clear();
      return surf;
    } else { //2D
      return Perimeter(nbh);
    }
  }







  /* This is internal function.
     v 3d-6 neighborhood pro binarni obraz je 64 moznosti pro okoli 1 voxelu.
     fkce VoxelSurfaces pro vsech 64 sousedstvi aproximuje povrch voxelu a
     to v mikrometrech v zaislosti na rozliseni Resolution.
     Je-li rada sousednich voxelu v 3d-6 neighborhood nasledujici:
     (1, 0, 0), (0, 1, 0), (0, 0, 1), (-1, 0, 0), (0, -1, 0), (0, 0, -1),
     pak VoxSurf[0] je povrxh voxelu bez sousedu,
     VoxSurf[1] je povrxh voxelu se sousedem (1, 0, 0),
     VoxSurf[2] je povrxh voxelu se sousedem            (0, 1, 0),
     VoxSurf[3] je povrxh voxelu se sousedy (1, 0, 0) a (0, 1, 0) atd.
     cislovani v komentarich:
               ___
              /__/| 4
            _| 3| |__
           /_|__|/_ /|
          |5/__/| 2| |
          |_| 1||__|/
            |__/||
             |6_|/
      */
  #define SQR(x) ((x)*(x))


  void VoxelSurfaces(const Resolution r, float (& VoxSurf)[64]){
    // rozmery jednoho voxelu podle rozliseni
    float x=1.0/r.GetX();
    float y=1.0/r.GetY();
    float z=1.0/r.GetZ();

    VoxSurf[0]=2*x*y + 2*x*z + 2*y*z; //osamoceny voxel - to snad ani nema smysl

    // konfigurace s jednim sousednim voxelem:
    //V1,V4 (V1 znaci konfiguraci,kdy v sousedstvi jen voxel c.1 patri objektu):
    VoxSurf[1]=VoxSurf[8]= sqrt(SQR(x)+SQR(z/2)) * y + sqrt(SQR(x)+SQR(y/2))*z;
    // V2, V5:
    VoxSurf[2]=VoxSurf[16]= sqrt(SQR(y)+SQR(z/2)) * x + sqrt(SQR(y)+SQR(x/2))*z;
    // V3, V6:
    VoxSurf[4]=VoxSurf[32]= sqrt(SQR(z)+SQR(y/2)) * x + sqrt(SQR(z)+SQR(x/2))*y;

    //kofigurace se 2 sousednimi voxely:
    //V12, V15, V45, V24:
    VoxSurf[3]=VoxSurf[17]=VoxSurf[24]=VoxSurf[10]= sqrt(SQR(x) + SQR(y)) * z;
    //V13, V16, V34, V46:
    VoxSurf[5]=VoxSurf[33]=VoxSurf[12]=VoxSurf[40]= sqrt(SQR(x) + SQR(z)) * y;
    //V23, V35, V26, V56:
    VoxSurf[6]=VoxSurf[20]=VoxSurf[34]=VoxSurf[48]= sqrt(SQR(y) + SQR(z)) * x;

    //konfigurace se 2 sousednimy voxely naproti sobe:
    //V14:
    VoxSurf[9]= x * PI * ( 3*(y+z)/4 - sqrt(y*z)/2 );
    //V25:
    VoxSurf[18]= y * PI * ( 3*(x+z)/4 - sqrt(x*z)/2 );
    //V36:
    VoxSurf[36]= z * PI * ( 3*(y+x)/4 - sqrt(y*x)/2 );

    //konfigurace se 3 sousednimi voxely v jedne rovine:
    //V124 ,V145 :
    VoxSurf[11]= VoxSurf[25]= 2*x * sqrt( SQR(z)/4 + SQR(y) );
    //V125 ,V245 :
    VoxSurf[19]= VoxSurf[26]= 2*y * sqrt( SQR(z)/4 + SQR(x) );
    //V236 ,V356 :
    VoxSurf[38]= VoxSurf[52]= 2*z * sqrt( SQR(x)/4 + SQR(y) );
    //V134 ,V146 :
    VoxSurf[13]= VoxSurf[41]= 2*x * sqrt( SQR(y)/4 + SQR(z) );
    //V235 ,V265 :
    VoxSurf[22]= VoxSurf[50]= 2*y * sqrt( SQR(x)/4 + SQR(z) );
    //V136 ,V346 :
    VoxSurf[37]= VoxSurf[44]= 2*z * sqrt( SQR(y)/4 + SQR(x) );

    //konfigurace se 3 sous. voxely majici spolecny 1 vrchol kvadru
    //vsechny povrchy jsou stejne:
    //V123 ,V126 ,V156 ,V135 :
    float a=sqrt( SQR(x) +SQR(y) );
    float b=sqrt( SQR(x) +SQR(z) );
    float c=sqrt( SQR(y) +SQR(z) );
    float s=(a+b+c)/2;
    float S=2*sqrt( s*(s-a)*(s-b)*(s-c) );// + x*y/4 + y*z/4 + x*z/4;

    VoxSurf[7]=VoxSurf[35]=VoxSurf[49]=VoxSurf[21]= S;
    //V234 ,V246 ,V345 ,V456 :
    VoxSurf[14]=VoxSurf[42]=VoxSurf[28]=VoxSurf[56]= S;

    //konfigurace se 4 sous. voxely, ktere nejsou v rovine:
    //V1236, V1536, V4536, V2436:
    VoxSurf[39]=VoxSurf[46]=VoxSurf[53]=VoxSurf[60]= sqrt(SQR(x) + SQR(y)) * z;
    //V1352, V1652, V3452, V4652:
    VoxSurf[23]=VoxSurf[51]=VoxSurf[30]=VoxSurf[58]= sqrt(SQR(x) + SQR(z)) * y;
    //V2314, V3514, V2614, V5614:
    VoxSurf[15]=VoxSurf[43]=VoxSurf[29]=VoxSurf[57]= sqrt(SQR(y) + SQR(z)) * x;

    //konfigurace se 4 sous. voxely, ktere jsou v rovine:
    //V1245:
    VoxSurf[27]= 2*x*y;
    //V2356:
    VoxSurf[54]= 2*y*z;
    //V1346
    VoxSurf[45]= 2*x*z;

    //konfigurace s 5 sous. voxely:
    //V12345, V12456:
    VoxSurf[31]=VoxSurf[59]= x*y;
    //V12346, V13456:
    VoxSurf[47]=VoxSurf[61]= x*z;
    //V12356, V23456:
    VoxSurf[55]=VoxSurf[62]= y*z;

    //voxel je "obklicen" 6 sousednimi voxely => neni na povrchu
    //V123456:
    VoxSurf[63]=0;
  }


/* computes surface of binary image in micro meters (um) by 3D-6 neighbourhood
   also sets number of 3D-6 surface voxels */
  template <class LABEL>
  float Shape<LABEL>::SurfaceUm3d(){

    if (isSurfaceUm) {
      #ifdef I3D_DEBUG
        cout << "SurfaceUm 3D version ... using earlier calculated value(s)"
             << endl;
      #endif
      return surfaceUm;
    }

    size_t imgSize=img->GetImageSize();
    if (imgSize == 0) { return 0; } //image is empty


    float surf=0;         //total surface
    float VoxSurf[64];    //surface of one voxel depending on neighbours
    size_t VoxSum[64];    //sum of voxels in theyr neighbors categories
    for (int i=0; i<64; i++) { VoxSum[i]=0; }
    VoxelSurfaces(img->GetResolution(), VoxSurf);

    Boundary bdr=GetBoundary();
    size_t bdrSize = bdr.size();
    size_t sizeX = img->GetSizeX();
    size_t sizeY = img->GetSizeY();
    size_t sizeZ = img->GetSizeZ();

    for (size_t i = 0; i < bdrSize; i++ ){ // iterating all boundary voxels
      size_t x,y,z;
      x=boundary[i].x;
      y=boundary[i].y;
      z=boundary[i].z;
      //deskriptor okoli voxelu - index do pole VoxSum a VoxSurf
      unsigned char nbsDesc=0;
      if ( (y+1) < sizeY  &&  img->GetVoxel(x,y+1,z) == label) nbsDesc++;
      nbsDesc <<= 1;
      if ( (x+1) < sizeX  &&  img->GetVoxel(x+1,y,z) == label) nbsDesc++;
      nbsDesc <<= 1;
      if ( (z+1) < sizeZ  &&  img->GetVoxel(x,y,z+1) == label) nbsDesc++;
      nbsDesc <<= 1;
      if ( y > 0  &&  img->GetVoxel(x,y-1,z) == label) nbsDesc++;
      nbsDesc <<= 1;
      if ( x > 0  &&  img->GetVoxel(x-1,y,z) == label) nbsDesc++;
      nbsDesc <<= 1;
      if ( z > 0  &&  img->GetVoxel(x,y,z-1) == label) nbsDesc++;
      VoxSum[nbsDesc]++;
    }

    #ifdef I3D_DEBUG
    size_t voxSurf=0;
    cout << "SurfaceUm: Number of voxels and voxel surfaces "
            "for all 3D-6 neighbourhoods:"
         << endl;
    for (int i=0; i<64;i++) {
      cout << "    " << i << " : " << VoxSum[i] << " : " << VoxSurf[i]<<endl;
      if ( i>0 && i<63 ) voxSurf += VoxSum[i];
    }
    #endif

    for (int i=0; i<64;i++) {
      surf += VoxSum[i] * VoxSurf[i];
    }

    isSurfaceUm=true;
    surfaceUm=surf;
    return surf;
  }

  template <class LABEL>
  float Shape<LABEL>::PerimeterUm(){
    if (is3D) throw InternalException ("Shape::PerimeterUm: "
                                "This is 2D function for one slice images. "
                                "Try calling SurfaceUm.");
    if (isSurfaceUm) {
      #ifdef I3D_DEBUG
        cout << "SurfaceUm 2D version (perimeter) ... "
                "using earlier calculated value(s)" << endl;
      #endif
      return surfaceUm;
    }

    size_t imgSize=img->GetImageSize();
    if (imgSize == 0) { return 0; } //image is empty

    Resolution res = img->GetResolution();
    //distances betwen 2 points in x, y, and diagonal direction:
    float xDist = 1.0 / res.GetX();
    float yDist = 1.0 / res.GetY();
    float dDist = sqrt( xDist*xDist + yDist*yDist );
    float per= 2*xDist + 2*yDist;  //total perimeter - initialized for one pixel correction
    
    //calling 2d version of boundary (it will be connected):
    Boundary bdr=GetBoundary();
    size_t bdrSize = bdr.size();

    if (bdrSize == 1) { //one voxel object
      return per;
    }

    for (size_t i = 0; i < bdrSize-1; i++ ){ // iterating all boundary voxels
      size_t x0,y0,x1,y1;  //neighbour voxels (0 and 1)
      x0=boundary[i].x;
      y0=boundary[i].y;
      x1=boundary[i+1].x;
      y1=boundary[i+1].y;

      if ( x0 != x1 && y0 != y1){ // lying diagonally
        per += dDist;
      } else {
        if ( x0 == x1 ) per += yDist; //same column
        if ( y0 == y1 ) per += xDist; //same row
      }
    }

    // finally check first and last boundary points
    size_t x0,y0,x1,y1;
    x0=boundary[0].x;
    y0=boundary[0].y;
    x1=boundary[bdrSize-1].x;
    y1=boundary[bdrSize-1].y;
    if ( x0 != x1 && y0 != y1){ // lying diagonally
      per += dDist;
    } else {
      if ( x0 == x1 ) per += yDist; //same column
      if ( y0 == y1 ) per += xDist; //same row
    }

    isSurfaceUm=true;
    surfaceUm=per;
    return per;
 }

  template <class LABEL>
  float Shape<LABEL>::SurfaceUm(){
    if (is3D) {
      #ifdef I3D_DEBUG
        cout << "SurfaceUm: calling 3D version" << endl;
      #endif
      return SurfaceUm3d();
    } else {
      #ifdef I3D_DEBUG
        cout << "SurfaceUm: calling 2D version (perimeter)" << endl;
      #endif
      return PerimeterUm();
    }
  }


  /*  computes curvature of every boundary voxel of 2D BINARY object.
      Curvature(img).size() == GetBoundary(img).size()
      and Curvature(img)[i] is curvature of voxel GetBoundary(img)[i]
      curvature is from interval <-1, 1>;
      -1 stands for straight boundary;
      1 stands for total tips (convex or concave);
      distance must be >1 and it means how much the curvature measure
      is sensitive to noise, the higher the distance,
      the less sensitivity to noise but also to little details.
      Please note that calling curvature2d several times
      with differend 'dist' will cause overwriting values in vector.
  */
  template <class LABEL>
  vector< float > Shape<LABEL>::Curvature2d( unsigned int dist){
    if (is3D) throw InternalException ("Shape::Curvature2d: "
                     "This is 2D function for one slice images.");

    if (dist==0) {
      throw InternalException ("Shape::Curvature2d: "
                                "distance can NOT be zero "
                                );
    }
    if (curvatureDist==dist) {
      #ifdef I3D_DEBUG
        cout << "Curvature2d ... using earlier calculated value(s)" << endl;
      #endif
      return  curvature;
    }

    Boundary bdr=GetBoundary();
    unsigned int bdrSize = bdr.size();
    if (dist > bdrSize/4) {
      dist = bdrSize/4;
      if (dist==0) dist=1;  // for too small objects
      #ifdef I3D_DEBUG
        cout << "Shape::Curvature2d: " <<
            "distance is higher than quarter of shape boundary length - " <<
            "that's not suitable. I'm setting it for acceptable maximum: " <<
            dist << endl;
      #endif
    }

    //Im computing with another distance or for the first time:
    if ( curvatureDist !=dist ) {
      curvature.clear();
    }
    vector< float > curvatures(bdrSize, 0);

    if (isIsometric) {
      for (unsigned int i = 0; i < bdrSize; i++ ){
        Vector3d<int> ba(bdr[i]);
        Vector3d<int> b1(bdr[ ( i<dist )? bdrSize+i-dist:(i-dist)  ]);
        Vector3d<int> b2(bdr[ (i+dist) % bdrSize ]);


        Vector3d<float> v1 = static_cast< Vector3d<float> >(b1 - ba);
        Vector3d<float> v2 = static_cast< Vector3d<float> >(b2 - ba);
        float v1Norm = sqrt(v1.x*v1.x + v1.y*v1.y);
        float v2Norm = sqrt(v2.x*v2.x + v2.y*v2.y);
        float curv = (v1Norm==0 || v2Norm==0) ? 1:
                        (v1.x*v2.x + v1.y*v2.y) / (v1Norm*v2Norm);
        curvatures[i]=curv;
      }
    } else {
      Resolution res = img->GetResolution();
      float dx = 1.0/ res.GetX();
      float dy = 1.0/ res.GetY();
      for (unsigned int i = 0; i < bdrSize; i++ ){
        Vector3d<int> iba(bdr[i]);
        Vector3d<int> ib1(bdr[ ( i<dist )? bdrSize+i-dist:(i-dist)  ]);
        Vector3d<int> ib2(bdr[ (i+dist) % bdrSize ]);
          // recomputation points to um
        Vector3d<float> ba ( iba.x *dx, iba.y *dy, 0);
        Vector3d<float> b1 ( ib1.x *dx, ib1.y *dy, 0);
        Vector3d<float> b2 ( ib2.x *dx, ib2.y *dy, 0);

        Vector3d<float> v1 = static_cast< Vector3d<float> >(b1 - ba);
        Vector3d<float> v2 = static_cast< Vector3d<float> >(b2 - ba);
        float v1Norm = sqrt(v1.x*v1.x + v1.y*v1.y);
        float v2Norm = sqrt(v2.x*v2.x + v2.y*v2.y);
        float curv = (v1Norm==0 || v2Norm==0) ? 1:
                        (v1.x*v2.x + v1.y*v2.y) / (v1Norm*v2Norm);
        curvatures[i]=curv;
      }
    }
    curvature=curvatures;
    curvatureDist = dist;
    return curvatures;
  }

  /*  Alternative curvature estimation by substituing boundary with B-Splines.
      Take care, this approach doesn't take resolution into account
      i suggest you should use it just for isometric images with same resolution,
      it doesn't store results in 'Shape' object.
  */
  template <class LABEL>
  vector< float > Shape<LABEL>::BSCurvature2d(){
     if (is3D) throw InternalException ("Shape::BSCurvature2d: "
                     "This is 2D function for one slice images.");
     Boundary bdr=GetBoundary();
      unsigned int bdrSize = bdr.size();
       if (  bdrSize < 5) {
        throw InternalException ("Shape::Curvature: "
                                "Too short boundary,"
                                "computing curvature doesn't make sense"
                                 );
      }
      #ifdef I3D_DEBUG
          if (!isIsometric) cout << "Warning: You are using BScurvature2d for non isometric"
                                    " image, values won't be resolution-precise." << endl;
      #endif


      vector< float > curvatures(bdrSize, 0);
      Resolution res = img->GetResolution();
      for (unsigned int i = 0; i < bdrSize; i++ ){
        Vector3d<int> p = bdr[i];
        Vector3d<int> p1 = bdr[ (i+1) % bdrSize ];
        Vector3d<int> p2 = bdr[ (i+2) % bdrSize ];
        Vector3d<int> p_1 = bdr[ ( i<1 )? bdrSize+i-1:(i-1)  ];
        Vector3d<int> p_2 = bdr[ ( i<2 )? bdrSize+i-2:(i-2)  ];

        float b1 = ( p_2.x + p2.x + 2*(p_1.x + p1.x ) - 6*p.x ) / 12.0;
        float b2 = ( p_2.y + p2.y + 2*(p_1.y + p1.y ) - 6*p.y ) / 12.0;
        float c1 = ( p2.x - p_2.x + 4*(p1.x - p_1.x)) / 12.0;
        float c2 = ( p2.y - p_2.y + 4*(p1.y - p_1.y)) / 12.0;

        float cc = c1*c1 + c2*c2;
        float curv;
        if (cc==0) {
          curv = 10;      //   +
                          //   +
                          //   +
                          //pripad "jehly", krivost by vychazela nekonecna,
                          //nastaveno na fixni hodnotu vypoctenou pomoci
                          //modelu zahrnujiciho velikost pixelu
        } else {
          if (  ( curv = 2*(c1*b2 - c2*b1)/sqrt( cc*cc*cc) ) > 10  ) {
            curv = 8.5;   // +         +        +
                          // +         ++       ++
                          // ++         +       +
                          //dalsi 3 spec. pripady, kdy krivost je
                          //nesmyslne velika nezapocitanim rozmeru pixelu
          }
        }
        curvatures[i]=curv;
        /*#ifdef I3D_DEBUG
          cout << "p ( " << p.x << ", " << p.y << " ): " << curv << endl;
        #endif*/

      }
      return curvatures;
  }


  /* Curature is written directly in NetBoundary it is defined as N/N1,
     where N is number of object voxels in elipsoidal neighbourhood
     and N1 is voxel volume of neighbourhood, therefore it is
     in interval (0, 1),  0 - convex verticle, 1 - concave pit
     cca 0.5 - flat land */
  /* its is too slow
    template <class LABEL>
    NetBoundary Shape<LABEL>::Curvature3d( unsigned int dist){


    NetBoundary netBdr = NetBoundary3d();
    typedef NetBoundary::iterator NetBoundaryIt;
    typedef Vector3d<size_t> V3d;

    double r = static_cast<double>(dist);
    Neighbourhood nbh(r,r,r);
    //no. of voxels in neighbourhood:
    float nbhSize = static_cast<float>(nbh.offset.size()/3);

    //for each boundary voxel search his circular neighbourhood:
    for (NetBoundaryIt bdrPair = netBdr.begin();
                                bdrPair != netBdr.end(); bdrPair++){
      V3d  vox = bdrPair->first;  //actual boundary voxel
      vector<const LABEL *> nbs;  //neighbours
      vector<const LABEL *>::iterator nb; //iterator - neighbour
      GetWindow( *img, vox.x, vox.y, vox.z, nbh, nbs);
      size_t voxSum = 0;
      for( nb = nbs.begin(); nb != nbs.end(); nb++ ){ //searching neighbourhood:
        if ( (**nb) == label) {
	  voxSum++;
	}

      }
      bdrPair->second.curvature = voxSum/nbhSize;
    }
    return netBdr;
  }*/


/* normalized bending energy - but too slow*/
/*  template <class LABEL>
    double Shape<LABEL>::NBendingEnergy3d(){
    double bendingE=0;
    if (isIsometric){
      float minR, maxR, meanR; //only mean radius is important so far
      Radius(minR, maxR, meanR);
      Resolution res = img->GetResolution();
      float ax = res.GetX();
      float ay = res.GetY();
      float az = res.GetZ();

      int dist = static_cast<int> ( meanR / sqrt(ax*ax +  ay*ay + az*az) );

      NetBoundary netBdr = Curvature3d( dist<2 ? 2: dist );
      typedef NetBoundary::iterator NetBoundaryIt;
      for (NetBoundaryIt bdrPair = netBdr.begin();
                                      bdrPair != netBdr.end(); bdrPair++){
        const NetBoundaryNode netBdrNode = bdrPair->second;
           // shifting curvature from interval (0,1) to interval (-1,1) :
        float shiftCurv = ( netBdrNode.curvature * 2.0 ) - 1.0;
        bendingE += shiftCurv * shiftCurv;
      }
      bendingE /= netBdr.size();
    } else {
      throw InternalException ("Shape::Curvature: "
          "3d curvature for NONisometric images not implemented yet.");
    }
  }
*/
  template <class LABEL>
  double Shape<LABEL>::BendingEnergy2d(){
    if (is3D) throw InternalException ("Shape::BendingEnergy2d: "
                     "This is 2D function for one slice images.");

    if (isBendingE) {
      #ifdef I3D_DEBUG
        cout << "BendingEnergy2d ... using earlier calculated value(s)"
             << endl;
      #endif
      return bendingE;
    }

    double bendE=0;
    vector< float > curvature = BSCurvature2d();
    for (vector<float>::iterator cit = curvature.begin();
                                     cit!=curvature.end(); cit++){
      bendE += (*cit) * (*cit);
    }
    //bendE /= PerimeterUm(); // presumes curvature in microns
    bendE /= curvature.size();

    curvature.clear();
    isBendingE=true;
    bendingE=bendE;
    return bendE;
  }

  /*this does not work
  template <class LABEL>
  float Shape<LABEL>::NBendingEnergy2d(){
    float perimeter = PerimeterUm();
    return BendingEnergy2d()*perimeter*perimeter;
  }
  */

//*************** STATISTICAL MOMENTS together with CENTROID*******************


/* overloaded math functions pow(), used by StatMoment()*/
  inline unsigned long pow(const size_t base, const unsigned short exp){
    unsigned long mul = 1;
    for (unsigned short i = 0; i < exp ; i++){
      mul *= base;
    }
    return mul;
  }

  inline unsigned long pow(const long base, const unsigned short exp){
    long mul = 1;
    for (unsigned short i = 0; i < exp ; i++){
      mul *= base;
    }
    return mul;
  }

  inline float pow( const float base, const unsigned short exp){
    float mul = 1;
    for (unsigned short i = 0; i < exp ; i++){
      mul *= base;
    }
    return mul;
  }



   /*Computes statistical moment.
     ex, ey, ez  - are exponent coeficients
     moment for 3D is defined as  Sum(x^ex * y^ey * z^ez * img3D(x,y,z))
         for all x,y,z values combinations
   */
  template <class LABEL>
  unsigned long Shape<LABEL>::StatMoment( unsigned short ex, unsigned short ey,
                                                      unsigned short ez){

      // finding wheather this moment wasn't already calculated
    if ( ex<10 && ey<10 && ez<10 ) {
      typedef map<int, unsigned long>::iterator statMomIt;
      statMomIt it = statMoments.find(ex*100 + ey*10 + ez);
      if ( it != statMoments.end() ) {
        #ifdef I3D_DEBUG
          cout << "StatMoment ... using earlier calculated value(s)" << endl;
        #endif
        return ( (*it).second );
      }
    }

    size_t szX = img->GetSizeX();
    size_t szY = img->GetSizeY();
    size_t szZ = img->GetSizeZ();
    unsigned long M=0;

    for (size_t z = 0; z < szZ; z++){
      for (size_t y = 0; y < szY; y++){
        for (size_t x = 0; x < szX; x++){
          if (img->GetVoxel(x,y,z) == label){
            M += pow(x,ex) * pow(y,ey) * pow(z,ez); // * value == 1 for bin img
          }
        }
      }
    }

    if ( ex<10 && ey<10 && ez<10 ) {
      statMoments.insert( pair<int, unsigned long> (ex*100 + ey*10 + ez, M) );
    }
    return M;
  }

 /* statistical moments but in micrometers*/
 template <class LABEL>
 float Shape<LABEL>::StatMomentUm( unsigned short ex, unsigned short ey,
                   unsigned short ez){
    // ax,ay,az - distances between voxels in all axes:
    Resolution res= img->GetResolution();
    float ax = 1.0 / res.GetX();
    float ay = 1.0 / res.GetY();
    float az = 1.0 / res.GetZ();
    return StatMoment(ex,ey,ez) * pow(ax,ex) * pow(ay,ey) * pow(az,ez);
  }

  template <class LABEL>
  Vector3d<size_t> Shape<LABEL>::Centroid(){
    if (isCentroid) {
      #ifdef I3D_DEBUG
        cout << "Centroid ... using earlier calculated value(s)" << endl;
      #endif
      return centroid;
    }
    Vector3d<size_t> center;
    double vol = static_cast<double>(Volume());
    center.x = static_cast<size_t>( StatMoment( 1, 0, 0) / vol);
    center.y = static_cast<size_t>(StatMoment( 0, 1, 0) / vol);
    center.z = is3D ? static_cast<size_t>(StatMoment( 0, 0, 1) / vol) : 0;

    isCentroid = true;
    centroid = center;
    return center;
  }



  /*Computes central statistical moment.
    these are important to obtain translation invariance -
    useful for calculating eigenaxis and symmetry
    ex, ey, ez  - are exponent coeficients
    moment for 3D is defined as  Sum((x -x')^ex * (y-y')^ey *
                                     (z-z')^ez * img3D(x,y,z))
    for all x,y,z values combinations, x',y',z' denotes x,y,z center coord.
   */
  template <class LABEL>
  long Shape<LABEL>::CStatMoment(unsigned short ex, unsigned short ey,
                   unsigned short ez){

    // finding wheather this moment wasn't already calculated
    if ( ex<10 && ey<10 && ez<10 ) {
      typedef map<int, long>::iterator cStatMomIt;
      cStatMomIt it = cStatMoments.find(ex*100 + ey*10 + ez);
      if ( it != cStatMoments.end() ) {
        #ifdef I3D_DEBUG
          cout << "CStatMoment ... using earlier calculated value(s)" << endl;
        #endif
        return ( (*it).second );
      }
    }

    //first obtain the center:
    Vector3d<long> c = Centroid();

    size_t szX = img->GetSizeX();
    size_t szY = img->GetSizeY();
    size_t szZ = img->GetSizeZ();
    long M=0;

    for (long z = 0; static_cast<size_t>(z) < szZ; z++){
      for (long y = 0; static_cast<size_t>(y) < szY; y++){
        for (long x = 0; static_cast<size_t>(x) < szX; x++){
          if (img->GetVoxel(x,y,z) == label){
            M += pow(x - c.x, ex) * pow(y - c.y, ey) * pow(z - c.z ,ez);
          }
        }
      }
    }
    if ( ex<10 && ey<10 && ez<10 ) {
      cStatMoments.insert( pair<int, long> (ex*100 + ey*10 + ez, M) );
    }
    return M;
  }


  /*Computes central statistical moment in micrometers, takes into account
    resolution. These are important to obtain translation invariance -
    useful for calculating eigenaxes and symmetry.
    ex, ey, ez  - are exponent coeficients
    moment for 3D is defined as  Sum((x -x')^ex * (y-y')^ey * (z-z')^ez *
                                                               img3D(x,y,z))
       for all x,y,z values combinations, x',y',z' denotes x,y,z center coordinates
   */
  template <class LABEL>
  float Shape<LABEL>::CStatMomentUm( unsigned short ex, unsigned short ey,
                   unsigned short ez){

    // ax,ay,az - distances between voxels in all axes:
    Resolution res= img->GetResolution();
    float ax = 1.0 / res.GetX();
    float ay = 1.0 / res.GetY();
    float az = 1.0 / res.GetZ();
    return CStatMoment(ex,ey,ez) * pow(ax,ex) * pow(ay,ey) * pow(az,ez);
  }



//****************************** RADIUS **************************************
  template <class LABEL>
  void Shape<LABEL>::Radius(float &min, float &max, float &mean){
    Boundary bdr = GetBoundary();
    size_t bdrSize = bdr.size();
    Vector3d<float> ctr = static_cast<Vector3d<float> > (Centroid());
    //Vector3d<size_t> ctr =  Centroid();
    double distSum = 0;
    min = std::numeric_limits<float>::max();
    max = 0;

    Resolution res=img->GetResolution();
    float ax= 1.0/res.GetX();
    float ay= 1.0/res.GetY();
    float az= 1.0/res.GetZ();

    for (size_t i = 0; i < bdrSize; i++ ){ // iterating all boundary voxels
      Vector3d<float> bdrVox = static_cast<Vector3d<float> > (boundary[i]);
      float dx = (bdrVox.x - ctr.x) *ax;
      float dy = (bdrVox.y - ctr.y) *ay;
      float dz = (bdrVox.z - ctr.z) *az;
      float dist = sqrt( dx*dx + dy*dy + dz*dz );
      if ( dist > max) max = dist;
      if ( dist < min) min = dist;
      distSum += dist;
    }
    mean = distSum / bdrSize;
  }



//****************************** EIGENAXES *************************************

  /* computes principal axes and their values
     using LAPACK ssyev (single(real) - symmetric - eigen vectors) function
     If input image isn't isometric (i.e. resolution  x ,y ,z aren't the same ),
     than resolution is taken into account and statistical moments
     and eigen axes/values are computed in micrometers.
     */
  template <class LABEL>
  PrincipalAxes Shape<LABEL>::GetPrincipalAxes() {
   #ifndef WITH_LAPACK
      cerr << "GetPrincipalAxes: you cannot use this function without "
                   "linking LAPACK library."
         << endl;
      exit(1);
	
		// return any value
		return principalAxes;
	#else

    if ( isPrincipalAxes ) {
      #ifdef I3D_DEBUG
        cout << "GetPrincipalAxes ... using earlier calculated value(s)"
             << endl;
      #endif
      return principalAxes;
    }

    const int N=3;            // no. of columns
    const int LDA=3;          // no. of rows
    float a[LDA*N];    // input/output  matrix
    float w[N];        // eigenvalues output
    const int LWORK=50;       // length of work array (set to optimal length)
    float work[LWORK]; // work array
    int info=0;         // info 0 == OK

    // filling upper triangle of matrix with central statistical moments:
    if ( isIsometric ) { // isometric image
      #ifdef I3D_DEBUG
        cout << "GetPrincipalAxes: isometric resolution case - "
                    "using CStatMoment" << endl;
      #endif
      a[0 + LDA*0]=CStatMoment( 2,0,0);
      a[0 + LDA*1]=CStatMoment( 1,1,0);
      a[0 + LDA*2]=CStatMoment( 1,0,1);
      a[1 + LDA*1]=CStatMoment( 0,2,0);
      a[1 + LDA*2]=CStatMoment( 0,1,1);
      a[2 + LDA*2]=CStatMoment( 0,0,2);
    } else {   //non isometric image
      #ifdef I3D_DEBUG
        cout << "GetPrincipalAxes: nonisometric resolution case "
                   "- using CStatMomentUm" << endl;
      #endif
      a[0 + LDA*0]=CStatMomentUm( 2,0,0);
      a[0 + LDA*1]=CStatMomentUm( 1,1,0);
      a[0 + LDA*2]=CStatMomentUm( 1,0,1);
      a[1 + LDA*1]=CStatMomentUm( 0,2,0);
      a[1 + LDA*2]=CStatMomentUm( 0,1,1);
      a[2 + LDA*2]=CStatMomentUm( 0,0,2);
    }

	ssyev_('V', 'U', N, &a[0], LDA, &w[0], &work[0], LWORK, info);

    if (info != 0) throw InternalException ("Shape::GetPrincipalAxes: "
                                "unable to compute eigenvectors, "
                                "LAPACK function returned error");

    PrincipalAxes pas;

    pas.val3=w[0];
    pas.val2=w[1];
    pas.val1=w[2];

    pas.axe3.x=a[0];
    pas.axe3.y=a[1];
    pas.axe3.z=a[2];

    pas.axe2.x=a[3];
    pas.axe2.y=a[4];
    pas.axe2.z=a[5];

    pas.axe1.x=a[6];
    pas.axe1.y=a[7];
    pas.axe1.z=a[8];

    isPrincipalAxes=true;
    principalAxes = pas;
    return pas;
	#endif
  }


  /* Elongations */
  template <class LABEL>
  void Shape<LABEL>::Elongation( float& majorElongation, float& minorElongation){
    PrincipalAxes pas = GetPrincipalAxes();
    majorElongation = pas.val1 / pas.val2;
    minorElongation = pas.val2 / pas.val3;
  }

  template <class LABEL>
  void Shape<LABEL>::Elongation2d( float& elongation){
    PrincipalAxes pas = GetPrincipalAxes();
    elongation = pas.val1 / pas.val2;
  }

  template <class LABEL>
  void Shape<LABEL>::Elongation3d( float& majorElongation, float& minorElongation){
    PrincipalAxes pas = GetPrincipalAxes();
    majorElongation = pas.val1 / pas.val2;
    minorElongation = pas.val2 / pas.val3;
  }

/*multiplying only 4x4 matrices*/
  void Mat4x4Mutiply(float *m1, float *m2, float *r) {
     for (int n=0 ; n<4; n++){
       for (int m=0 ; m<4; m++){
         r[m + 4*n] = m1[m + 4*0]*m2[0 + 4*n] + m1[m + 4*1]*m2[1 + 4*n] +
                      m1[m + 4*2]*m2[2 + 4*n] + m1[m + 4*3]*m2[3 + 4*n];
       }
     }
  }

/*composition of affine transformations, result is stored in m1*/
  void AffineCompose(float *m1, const float *m2) {
     float r[16];  //result
     for (int n=0 ; n<4; n++){
       for (int m=0 ; m<4; m++){
         r[m + 4*n] = m1[m + 4*0]*m2[0 + 4*n] + m1[m + 4*1]*m2[1 + 4*n] +
                      m1[m + 4*2]*m2[2 + 4*n] + m1[m + 4*3]*m2[3 + 4*n];
       }
     }
     for (int n=0 ; n<16; n++){
       m1[n]=r[n];
     }
  }

/*multiplying only 1*4 X 4x4 matrices*/
  void AffinePerform(float *v, float *m2, float *r) {
       for (int m=0 ; m<4; m++){
         int col = 4*m;
         r[m] = v[0]*m2[0 + col] + v[1]*m2[1 + col] +
                v[2]*m2[2 + col] + v[3]*m2[3 + col];
       }
  }

/*affine transform of Vectror3d point by 4x4 affine matrix*/
  inline Vector3d<int> AffinePerform(const Vector3d<size_t> &v, float *m2) {
       Vector3d<float> r;
       for (int m=0 ; m<4; m++){
         r.x = v.x*m2[0] + v.y*m2[1] + v.z*m2[2] + 1*m2[3];
         r.y = v.x*m2[4] + v.y*m2[5] + v.z*m2[6] + 1*m2[7];
         r.z = v.x*m2[8] + v.y*m2[9] + v.z*m2[10] + 1*m2[11];
       }
    return Vector3d<int>(static_cast<int>(r.x), static_cast<int>(r.y),
                                                 static_cast<int>(r.z) );
  }



  /* symType == 'P' ...
     symmetry with respect to plane given by normal vector and one point
     symType == 'R' ...
     symmetry with respect to ray given by its direction vector nad one point
     symmetry is defined as ratio  Sym / ( Sym + 2*Asym), where
     Sym .... sum of voxels having their symmetric mirrors in object
     Asym ... sum of voxels without their symmetric mirrors in object
  */
  template <class LABEL>
  float  Shape<LABEL>::Symmetry( Vector3d<float> normVect, Vector3d<size_t> center,
                                                               char symType){

    // vypocet uhlu pro srovnani dane roviny s rovinou yz
    // (tj, srovnani normVect s osou x)
    // alpha ... rotace okolo osy z - srovnani normaly do  roviny xz,
    // beta rotace okolo y - srovnani s osou x
    // Ca , Cb, Sa, Sb .. cosinus/sinus alpha/beta

    float Ca,Cb;
    float xx_yy=normVect.x*normVect.x + normVect.y*normVect.y;
    //normala je || s osou z - nerotuj okolo z:
    if ( fabs(normVect.x)<1e-15 && fabs(normVect.y)<1e-15 ) {
      Ca=1;
    } else {
      Ca = normVect.x / sqrt ( xx_yy );
    }
    Cb =  sqrt ( ( xx_yy ) / ( xx_yy + normVect.z*normVect.z ) );

    float alpha = acos( Ca );
    float beta =  acos( Cb );
    // urceni smeru rotace (tj. znamenko uhlu)
    if ( normVect.y > 0 ) { alpha = -alpha; }
    if ( normVect.z > 0 ) { beta = -beta; }
    float Sa = sin (alpha);
    float Sb = sin (beta);

      //center in microns:
    Resolution res = img->GetResolution();
    float Px = center.x / res.GetX();
    float Py = center.y / res.GetY();
    float Pz = center.z / res.GetZ();

    //matrices of basic affinite transformations
    float aff0[16],aff1[16], aff2[16], aff3[16], aff4[16],
                   aff5[16], aff6[16], aff7[16], aff8[16];

    // scaling
    aff0[ 0 + 4*0 ] = 1.0/res.GetX();
    aff0[ 1 + 4*0 ] = 0;
    aff0[ 2 + 4*0 ] = 0;
    aff0[ 3 + 4*0 ] = 0;

    aff0[ 0 + 4*1 ] = 0;
    aff0[ 1 + 4*1 ] = 1.0/res.GetY();
    aff0[ 2 + 4*1 ] = 0;
    aff0[ 3 + 4*1 ] = 0;

    aff0[ 0 + 4*2 ] = 0;
    aff0[ 1 + 4*2 ] = 0;
    aff0[ 2 + 4*2 ] = 1.0/res.GetZ();
    aff0[ 3 + 4*2 ] = 0;

    aff0[ 0 + 4*3 ] = 0;
    aff0[ 1 + 4*3 ] = 0;
    aff0[ 2 + 4*3 ] = 0;
    aff0[ 3 + 4*3 ] = 1;


    // shift to 0,0,0
    aff1[ 0 + 4*0 ] = 1;
    aff1[ 1 + 4*0 ] = 0;
    aff1[ 2 + 4*0 ] = 0;
    aff1[ 3 + 4*0 ] = -Px;

    aff1[ 0 + 4*1 ] = 0;
    aff1[ 1 + 4*1 ] = 1;
    aff1[ 2 + 4*1 ] = 0;
    aff1[ 3 + 4*1 ] = -Py;

    aff1[ 0 + 4*2 ] = 0;
    aff1[ 1 + 4*2 ] = 0;
    aff1[ 2 + 4*2 ] = 1;
    aff1[ 3 + 4*2 ] = -Pz;

    aff1[ 0 + 4*3 ] = 0;
    aff1[ 1 + 4*3 ] = 0;
    aff1[ 2 + 4*3 ] = 0;
    aff1[ 3 + 4*3 ] = 1;


    // rotation around axis z
    aff2[ 0 + 4*0 ] = Ca;
    aff2[ 1 + 4*0 ] = -Sa;
    aff2[ 2 + 4*0 ] = 0;
    aff2[ 3 + 4*0 ] = 0;

    aff2[ 0 + 4*1 ] = Sa;
    aff2[ 1 + 4*1 ] = Ca;
    aff2[ 2 + 4*1 ] = 0;
    aff2[ 3 + 4*1 ] = 0;

    aff2[ 0 + 4*2 ] = 0;
    aff2[ 1 + 4*2 ] = 0;
    aff2[ 2 + 4*2 ] = 1;
    aff2[ 3 + 4*2 ] = 0;

    aff2[ 0 + 4*3 ] = 0;
    aff2[ 1 + 4*3 ] = 0;
    aff2[ 2 + 4*3 ] = 0;
    aff2[ 3 + 4*3 ] = 1;


    // rotation around axis y
    aff3[ 0 + 4*0 ] = Cb;
    aff3[ 1 + 4*0 ] = 0;
    aff3[ 2 + 4*0 ] = -Sb;
    aff3[ 3 + 4*0 ] = 0;

    aff3[ 0 + 4*1 ] = 0;
    aff3[ 1 + 4*1 ] = 1;
    aff3[ 2 + 4*1 ] = 0;
    aff3[ 3 + 4*1 ] = 0;

    aff3[ 0 + 4*2 ] = Sb;
    aff3[ 1 + 4*2 ] = 0;
    aff3[ 2 + 4*2 ] = Cb;
    aff3[ 3 + 4*2 ] = 0;

    aff3[ 0 + 4*3 ] = 0;
    aff3[ 1 + 4*3 ] = 0;
    aff3[ 2 + 4*3 ] = 0;
    aff3[ 3 + 4*3 ] = 1;


    //mirrororing around plane yz in case of plane symmetry or
    //axis x in case of ray symm
    aff4[ 0 + 4*0 ] = (symType == 'P')? -1 : 1;
    aff4[ 1 + 4*0 ] = 0;
    aff4[ 2 + 4*0 ] = 0;
    aff4[ 3 + 4*0 ] = 0;

    aff4[ 0 + 4*1 ] = 0;
    aff4[ 1 + 4*1 ] = (symType == 'P')? 1 : -1;
    aff4[ 2 + 4*1 ] = 0;
    aff4[ 3 + 4*1 ] = 0;

    aff4[ 0 + 4*2 ] = 0;
    aff4[ 1 + 4*2 ] = 0;
    aff4[ 2 + 4*2 ] = (symType == 'P')? 1 : -1;
    aff4[ 3 + 4*2 ] = 0;

    aff4[ 0 + 4*3 ] = 0;
    aff4[ 1 + 4*3 ] = 0;
    aff4[ 2 + 4*3 ] = 0;
    aff4[ 3 + 4*3 ] = 1;


    //backward rotation around axis y
    aff5[ 0 + 4*0 ] = Cb;
    aff5[ 1 + 4*0 ] = 0;
    aff5[ 2 + 4*0 ] = Sb;
    aff5[ 3 + 4*0 ] = 0;

    aff5[ 0 + 4*1 ] = 0;
    aff5[ 1 + 4*1 ] = 1;
    aff5[ 2 + 4*1 ] = 0;
    aff5[ 3 + 4*1 ] = 0;

    aff5[ 0 + 4*2 ] = -Sb;
    aff5[ 1 + 4*2 ] = 0;
    aff5[ 2 + 4*2 ] = Cb;
    aff5[ 3 + 4*2 ] = 0;

    aff5[ 0 + 4*3 ] = 0;
    aff5[ 1 + 4*3 ] = 0;
    aff5[ 2 + 4*3 ] = 0;
    aff5[ 3 + 4*3 ] = 1;


    //backward rotation around axis z
    aff6[ 0 + 4*0 ] = Ca;
    aff6[ 1 + 4*0 ] = Sa;
    aff6[ 2 + 4*0 ] = 0;
    aff6[ 3 + 4*0 ] = 0;

    aff6[ 0 + 4*1 ] = -Sa;
    aff6[ 1 + 4*1 ] = Ca;
    aff6[ 2 + 4*1 ] = 0;
    aff6[ 3 + 4*1 ] = 0;

    aff6[ 0 + 4*2 ] = 0;
    aff6[ 1 + 4*2 ] = 0;
    aff6[ 2 + 4*2 ] = 1;
    aff6[ 3 + 4*2 ] = 0;

    aff6[ 0 + 4*3 ] = 0;
    aff6[ 1 + 4*3 ] = 0;
    aff6[ 2 + 4*3 ] = 0;
    aff6[ 3 + 4*3 ] = 1;


    //shift to P
    aff7[ 0 + 4*0 ] = 1;
    aff7[ 1 + 4*0 ] = 0;
    aff7[ 2 + 4*0 ] = 0;
    aff7[ 3 + 4*0 ] = Px;

    aff7[ 0 + 4*1 ] = 0;
    aff7[ 1 + 4*1 ] = 1;
    aff7[ 2 + 4*1 ] = 0;
    aff7[ 3 + 4*1 ] = Py;

    aff7[ 0 + 4*2 ] = 0;
    aff7[ 1 + 4*2 ] = 0;
    aff7[ 2 + 4*2 ] = 1;
    aff7[ 3 + 4*2 ] = Pz;

    aff7[ 0 + 4*3 ] = 0;
    aff7[ 1 + 4*3 ] = 0;
    aff7[ 2 + 4*3 ] = 0;
    aff7[ 3 + 4*3 ] = 1;

    // scaling back
    aff8[ 0 + 4*0 ] = res.GetX();
    aff8[ 1 + 4*0 ] = 0;
    aff8[ 2 + 4*0 ] = 0;
    aff8[ 3 + 4*0 ] = 0;

    aff8[ 0 + 4*1 ] = 0;
    aff8[ 1 + 4*1 ] = res.GetY();
    aff8[ 2 + 4*1 ] = 0;
    aff8[ 3 + 4*1 ] = 0;

    aff8[ 0 + 4*2 ] = 0;
    aff8[ 1 + 4*2 ] = 0;
    aff8[ 2 + 4*2 ] = res.GetZ();
    aff8[ 3 + 4*2 ] = 0;

    aff8[ 0 + 4*3 ] = 0;
    aff8[ 1 + 4*3 ] = 0;
    aff8[ 2 + 4*3 ] = 0;
    aff8[ 3 + 4*3 ] = 1;

    AffineCompose(aff0,aff1);
    AffineCompose(aff0,aff2);
    AffineCompose(aff0,aff3);
    AffineCompose(aff0,aff4);
    AffineCompose(aff0,aff5);
    AffineCompose(aff0,aff6);
    AffineCompose(aff0,aff7);
    AffineCompose(aff0,aff8);

    #ifdef I3D_DEBUG
      cout << "In Symmetry... " << endl;
      cout << "  Center: " << center.x << ", " << center.y << ", "
           << center.z << endl;
      cout << "  Alpha: " << alpha << ", beta: " << beta << endl;

      cout << "  Affine matrix: " << endl;
      for (int row=0; row<4; row++) {
        for (int col=0; col<4; col++) {
         cout << "  "<< setw(10) << aff0[row+4*col];
        }
        cout << endl;
      }

      Vector3d<size_t>  pnt(0, 0, 0);
      Vector3d<long> pnto = AffinePerform( pnt ,   aff0 );
      cout << "  transform of 0, 0, 0 point: " << pnto.x << ", "
           << pnto.y << ", " << pnto.z << endl;
      pnt.x=  center.x;
      pnt.y= center.y;
      pnt.z= center.z;
      pnto = AffinePerform(pnt, aff0 );
      cout << "  transform of center: " << pnto.x << ", "
           << pnto.y << ", " << pnto.z << endl;
      cout << "  calculating symmetry ..." << endl;
    #endif

    size_t szX = img->GetSizeX();
    size_t szY = img->GetSizeY();
    size_t szZ = img->GetSizeZ();

    unsigned long sym=0;
    unsigned long asym=0;
    for (size_t z = 0; z < szZ; z++){
      for (size_t y = 0; y < szY; y++){
        for (size_t x = 0; x < szX; x++){
          if (img->GetVoxel(x,y,z) == label){
            Vector3d<int> sP = AffinePerform (Vector3d<size_t>(x,y,z), aff0);
            if ( img->Include(sP.x, sP.y, sP.z) &&
                 img->GetVoxel(sP.x,sP.y,sP.z) == label){
	      sym++;
	    } else {
	      asym++;
	    }
	  }
        }
      }
    }

    #ifdef I3D_DEBUG
      cout << "Leaving Symmetry ..." << endl << endl;
    #endif
    return static_cast<float>(sym) / (sym+2*asym);
  }


  template <class LABEL>
  void  Shape<LABEL>::PrincipalSymmetry( float &sym1, float &sym2, float &sym3){

    if ( isPrincipalSymmetry ) {
        #ifdef I3D_DEBUG
          cout << "PrincipalSymmetry ... using earlier calculated value(s)"
               << endl;
        #endif
        sym1 = symmetry1;
        sym2 = symmetry2;
        sym3 = symmetry3;
        return;
    }

    //spocti center, principal axes a pak to hod funkci nahore
    Vector3d<size_t> center = Centroid();

    PrincipalAxes pas=GetPrincipalAxes();
    sym1 = Symmetry( pas.axe1, center, 'P');
    sym2 = Symmetry( pas.axe2, center, 'P');
    if (is3D) {
      sym3 = Symmetry( pas.axe3, center, 'P');
    } else {
      sym3 = 1;
    }

    isPrincipalSymmetry = true;
    symmetry1 = sym1;
    symmetry2 = sym2;
    symmetry3 = sym3;
    return;
  }


  /* Rectangularity is based on recognizing MEP (minimum enclosing prism),
     MEP can be defined as minimal prism containing whole object, having
     edges perpendicular with object main axes
  */
  template <class LABEL>
  float Shape<LABEL>::Rectangularity3d(){

    if (isRectangularity) {
      #ifdef I3D_DEBUG
        cout << "Rectangularity 3d version... using earlier calculated value(s)"
             << endl;
      #endif
      return rectangularity;
    }
     //first I must obtain MEP
    PrincipalAxes pa = GetPrincipalAxes ();
    // maximal distance in axe1, axe2, axe3 same/opposite direction
    float dist1same, dist1opp, dist2same, dist2opp, dist3same, dist3opp;
    dist1same= dist1opp= dist2same= dist2opp= dist3same= dist3opp=0;

    //obecna rovnice roviny: a*x + b*y + c*z + e = 0
    //pro rovinu danou normalou a bodem (Px, Py, Pz): e = -a*Px -b*Py -c*Pz
    // a, b, c ... odpovida x, y, z jednotlivich hlavnich os
    Resolution res = img->GetResolution();
    float ax = 1.0 / res.GetX(); // distances between vectors
    float ay = 1.0 / res.GetY();
    float az = 1.0 / res.GetZ();
    Vector3d<size_t> center = Centroid();
    float Px, Py, Pz; //center position
    if (isIsometric) {
      Px = center.x;
      Py = center.y;
      Pz = center.z;
    } else {
      Px = center.x * ax;
      Py = center.y * ay;
      Pz = center.z * az;
    }

    // e coeficients for all three main planes
    float e1 = -pa.axe1.x*Px - pa.axe1.y*Py -pa.axe1.z*Pz;
    float e2 = -pa.axe2.x*Px - pa.axe2.y*Py -pa.axe2.z*Pz;
    float e3 = -pa.axe3.x*Px - pa.axe3.y*Py -pa.axe3.z*Pz;

    //
    float divider1 = sqrt( pa.axe1.x*pa.axe1.x + pa.axe1.y*pa.axe1.y
                                               + pa.axe1.z*pa.axe1.z );
    float divider2 = sqrt( pa.axe2.x*pa.axe2.x + pa.axe2.y*pa.axe2.y
                                               + pa.axe2.z*pa.axe2.z );
    float divider3 = sqrt( pa.axe3.x*pa.axe3.x + pa.axe3.y*pa.axe3.y
                                               + pa.axe3.z*pa.axe3.z );

    size_t szX = img->GetSizeX();
    size_t szY = img->GetSizeY();
    size_t szZ = img->GetSizeZ();

    for (size_t z = 0; z < szZ; z++){
      for (size_t y = 0; y < szY; y++){
        for (size_t x = 0; x < szX; x++){
          if (img->GetVoxel(x,y,z) == label){
            float Vx, Vy, Vz;  //vector from center to checked voxel
            float d1, d2, d3;  //distances from center
            if (isIsometric) {
              Vx = x-Px;
              Vy = y-Py;
              Vz = z-Pz;

              d1 = fabs( x*pa.axe1.x + y*pa.axe1.y + z*pa.axe1.z + e1 ) / divider1;
              d2 = fabs( x*pa.axe2.x + y*pa.axe2.y + z*pa.axe2.z + e2 ) / divider2;
              d3 = fabs( x*pa.axe3.x + y*pa.axe3.y + z*pa.axe3.z + e3 ) / divider3;
            } else {
              float muX = x*ax; //recompute voxel to micron coordinates
              float muY = y*ay;
              float muZ = z*az;
              Vx = muX-Px;
              Vy = muY-Py;
              Vz = muZ-Pz;

              d1 = fabs( muX*pa.axe1.x + muY*pa.axe1.y + muZ*pa.axe1.z + e1 ) / divider1;
              d2 = fabs( muX*pa.axe2.x + muY*pa.axe2.y + muZ*pa.axe2.z + e2 ) / divider2;
              d3 = fabs( muX*pa.axe3.x + muY*pa.axe3.y + muZ*pa.axe3.z + e3 ) / divider3;
            }

            if ( (Vx*pa.axe1.x + Vy*pa.axe1.y + Vz*pa.axe1.z) >0 ) {
              if ( d1 > dist1same ) dist1same = d1;
            } else {
              if ( d1 > dist1opp ) dist1opp = d1;
            }

            if ( (Vx*pa.axe2.x + Vy*pa.axe2.y + Vz*pa.axe2.z) >0 ) {
              if ( d2 > dist2same ) dist2same = d2;
            } else {
              if ( d2 > dist2opp ) dist2opp = d2;
            }

            if ( (Vx*pa.axe3.x + Vy*pa.axe3.y + Vz*pa.axe3.z) >0 ) {
              if ( d3 > dist3same ) dist3same = d3;
            } else {
              if ( d3 > dist3opp ) dist3opp = d3;
            }
          }
        }
      }
    }

    #ifdef I3D_DEBUG
      cout << "Rectangularity 3D version - max distances in principal"
              "axes directions:" << endl
           << "  axe 1 direction: " << dist1same << ", oposite direction: "
           << dist1opp<< endl
           << "  axe 2 direction: " << dist2same << ", oposite direction: "
           << dist2opp<< endl
           << "  axe 3 direction: " << dist3same << ", oposite direction: "
           << dist3opp<< endl;
    #endif
    if (isIsometric) {
      rectangularity = Volume() / ( (dist1same+dist1opp+1) *
                          (dist2same+dist2opp+1) * (dist3same+dist3opp+1) );
    } else {
      rectangularity = VolumeUm() / ( (dist1same+dist1opp) *
                          (dist2same+dist2opp) * (dist3same+dist3opp) );
    }
    isRectangularity=true;
    return rectangularity;
  }

  template <class LABEL>
  float Shape<LABEL>::Rectangularity2d(){
    if (isRectangularity) {
      #ifdef I3D_DEBUG
        cout << "Rectangularity 2d version... using earlier calculated value(s)"
             << endl;
      #endif
      return rectangularity;
    }
     //first I must obtain MEP
    PrincipalAxes pa = GetPrincipalAxes ();
    // maximal distance in axe1, axe2 same/opposite direction
    float dist1same, dist1opp, dist2same, dist2opp;
    dist1same= dist1opp= dist2same= dist2opp=0;

      //obecna rovnice primky: a*x + b*y + e = 0
      //pro primku danou normalovym vektorem
      //a bodem (Px, Py, Pz): e = -a*Px -b*Py
      // a, b ... odpovida x, y jednotlivich hlavnich os
    Resolution res = img->GetResolution();
    float ax = 1.0 / res.GetX(); // distances between vectors
    float ay = 1.0 / res.GetY();
    Vector3d<size_t> center = Centroid();
    float Px, Py; //center position
    if (isIsometric) {
      Px = center.x;
      Py = center.y;
    } else {
      Px = center.x * ax;
      Py = center.y * ay;
    }

    // e coeficients for main axes
    float e1 = -pa.axe1.x*Px - pa.axe1.y*Py;
    float e2 = -pa.axe2.x*Px - pa.axe2.y*Py;

    //
    float divider1 = sqrt( pa.axe1.x*pa.axe1.x + pa.axe1.y*pa.axe1.y);
    float divider2 = sqrt( pa.axe2.x*pa.axe2.x + pa.axe2.y*pa.axe2.y);

    size_t szX = img->GetSizeX();
    size_t szY = img->GetSizeY();

    for (size_t y = 0; y < szY; y++){
      for (size_t x = 0; x < szX; x++){
        if (img->GetVoxel(x,y,0) == label){
          float Vx, Vy;      // vector of tested point from center
          float d1, d2;      //distances of tested point from center
          if (isIsometric) {
            Vx = x-Px;
            Vy = y-Py;

            d1 = fabs( x*pa.axe1.x + y*pa.axe1.y + e1 ) / divider1;
            d2 = fabs( x*pa.axe2.x + y*pa.axe2.y + e2 ) / divider2;
          } else {
            float muX = x*ax;   //recompute voxel to micron coordinates
            float muY = y*ay;
            Vx = muX-Px;
            Vy = muY-Py;

            d1 = fabs( muX*pa.axe1.x + muY*pa.axe1.y + e1 ) / divider1;
            d2 = fabs( muX*pa.axe2.x + muY*pa.axe2.y + e2 ) / divider2;
          }

          if ( (Vx*pa.axe1.x + Vy*pa.axe1.y ) >0 ) {
            if ( d1 > dist1same ) dist1same = d1;
          } else {
            if ( d1 > dist1opp ) dist1opp = d1;
          }

          if ( (Vx*pa.axe2.x + Vy*pa.axe2.y ) >0 ) {
            if ( d2 > dist2same ) dist2same = d2;
          } else {
            if ( d2 > dist2opp ) dist2opp = d2;
          }
        }
      }
    }


    #ifdef I3D_DEBUG
      cout << "Rectangularity 2D version - max distances"
              "in principal axes directions:" << endl
           << "  axe 1 direction: " << dist1same << ", opposite direction: "
           << dist1opp<< endl
           << "  axe 2 direction: " << dist2same << ", opposite direction: "
           << dist2opp<< endl;
    #endif
    if (isIsometric) {
      rectangularity = Volume() / ( (dist1same+dist1opp+1) *
                                    (dist2same+dist2opp+1));
    } else {
      rectangularity = VolumeUm() / ( (dist1same+dist1opp) *
                                      (dist2same+dist2opp));
    }
    isRectangularity=true;
    return rectangularity;
  }

  template <class LABEL>
  float Shape<LABEL>::Rectangularity(){
    if (is3D) {
      #ifdef I3D_DEBUG
        cout << "Rectangularity: calling 3D version (prismaty)" << endl;
      #endif
      return Rectangularity3d();
    } else {
      #ifdef I3D_DEBUG
        cout << "Rectangularity: calling 2D version" << endl;
      #endif
      return Rectangularity2d();
    }
   }

   /*energy of first koefNumber koeficients obtained by Fast Fourier Transform,
     it means that only low frequencies are assumed, it is always assumed resolution and computed in Um*/
   template <class LABEL>
   float Shape<LABEL>::LPEnergy2d(unsigned koefNumber){
    #ifdef WITH_FFTW
      if (is3D) throw InternalException ("Shape::LPEnergy2d: "
          "This is 2D function for one slice images. ");
      fftw_complex *in, *out;
      fftw_plan plan;
      Boundary bdr = GetBoundary();

      #ifdef I3D_DEBUG
        cout << "Shape::LPEnergy2D: FFT creating plan..." << endl;
      #endif
      size_t n = bdr.size();
      in = static_cast< fftw_complex *> (fftw_malloc(sizeof(fftw_complex)*n));
      out = static_cast< fftw_complex *> (fftw_malloc(sizeof(fftw_complex)*n));
      plan = fftw_plan_dft_1d(n, in, out, FFTW_FORWARD, FFTW_ESTIMATE);

      Resolution res = img->GetResolution();
      double dx = 1.0/res.GetX();
      double dy = 1.0/res.GetY();
      Vector3d<double> ctr = static_cast< Vector3d<double> > (Centroid());
      ctr.x *= dx;
      ctr.y *= dy;
      Boundary::iterator bdrVox;
      size_t i;
      for ( bdrVox = bdr.begin(), i=0; bdrVox != bdr.end(); bdrVox++, i++){
        in[i][0] = static_cast<double> (bdrVox->x * dx) - ctr.x;
        in[i][1] = static_cast<double> (bdrVox->y * dy) - ctr.y;
      }

      #ifdef I3D_DEBUG
        cout << "Shape::LFEnergy2D: computing FFT..." << endl;
      #endif
      fftw_execute(plan);

      for (size_t i = 0; i<n; i++){ //normalizing output
        out[i][0] /=n;
        out[i][1] /=n;
      }

      //fft transform out:
      double energy=0;
      koefNumber = koefNumber < (n-n/2) ? koefNumber : (n-n/2);
      for (size_t i = 0; (i < koefNumber) ; i++){
        energy += out[i][0]*out[i][0] + out[i][1] * out[i][1];//=absolutValue^2
      }

      #ifdef I3D_DEBUG
        cout << "destroing FFT plan and output, erasing boundary input..."
             << endl;
      #endif
      fftw_destroy_plan(plan);
      fftw_free(in);
      fftw_free(out);

      return energy;
    #else //WITH_FFTW
       cerr << "LPEnergy2d: you cannot use this function "
              "without linking FFTW library."
       << endl;
      exit(1);
		// return any value
		return 0.0;
    #endif //WITH_FFTW
  }

   /*Normalized Fourier Descriptors : FF and NEnergy*/
  template <class LABEL>
  void Shape<LABEL>::NFDs2d(float& FF, float& NEnergy){
    #ifndef WITH_FFTW
      cerr << "NFDs2d: you cannot use this function "
              " without linking FFTW library."
       << endl;
      exit(1);
    #else
      if (is3D) throw InternalException ("Shape::NFDs2d: "
          "This is 2D function for one slice images. ");
      if (isNFDs2d) {
      #ifdef I3D_DEBUG
        cout << "NFDs2d... using earlier calculated value(s)" << endl;
      #endif
      FF = fF;
      NEnergy = nEnergy;
      return;
      }

      fftw_complex *in, *out;
      fftw_plan plan;
      Boundary bdr = GetBoundary();
      size_t n = bdr.size();
      size_t shift = n/2;
      if (n==1) {
        isNFDs2d = true;
        fF = 1;
        nEnergy = 1;
        return;
      }
      #ifdef I3D_DEBUG
        cout << "Shape::FF2d: FFT creating plan..." << endl;
      #endif
      in = static_cast< fftw_complex *> (fftw_malloc(sizeof(fftw_complex)*n));
      out = static_cast< fftw_complex *> (fftw_malloc(sizeof(fftw_complex)*n));
      plan = fftw_plan_dft_1d(n, in, out, FFTW_FORWARD, FFTW_ESTIMATE);
      Boundary::iterator bdrVox;
      size_t i;
      if (isIsometric) {
        Vector3d<size_t> ctr = Centroid();
        for ( bdrVox = bdr.begin(), i=0; bdrVox != bdr.end(); bdrVox++, i++){
          in[i][0] = static_cast<double> (bdrVox->x)-static_cast<double>(ctr.x);
          in[i][1] = static_cast<double> (bdrVox->y)-static_cast<double>(ctr.y);
        }
      } else {
        Resolution res = img->GetResolution();
        double dx = 1.0/res.GetX();
        double dy = 1.0/res.GetY();
        Vector3d<double> ctr = static_cast< Vector3d<double> >(Centroid());
        ctr.x *= dx;
        ctr.y *= dy;
        for ( bdrVox = bdr.begin(), i=0; bdrVox != bdr.end(); bdrVox++, i++){
          in[i][0] = static_cast<double> (bdrVox->x *dx) - ctr.x;
          in[i][1] = static_cast<double> (bdrVox->y *dy) - ctr.y;
        }
      }
      #ifdef I3D_DEBUG
        cout << "Shape::FF2d: performing FFT..." << endl;
      #endif
      fftw_execute(plan);

      //normalizing output: 0th koef =0, 1st koef=1,
      //others are divided by abs(1st koef)
      out[0][0]=0;
      out[0][1]=0;
      fftw_complex k1;
      k1[0]= out[1][0];
      k1[1]= out[1][1];
      double divider=sqrt(k1[0]*k1[0] + k1[1]*k1[1]);
      for (size_t i = 1; i<n; i++){
        out[i][0] /=divider;
        out[i][1] /=divider;
      }

      double cit=0, jmen=0; //FF sumations
      NEnergy=0;
      // s goes from N/2 to N-N/2-1
      for (long s = -shift; s < static_cast<long>(n-shift); s++){
        long corr = ( n + s ) % n;
        double absVal = sqrt(out[corr][0]*out[corr][0] +
                             out[corr][1]*out[corr][1]);
        if (s!=0) cit += absVal/ (s<0 ? -s:s );
        jmen += absVal;
        NEnergy += absVal * absVal;
      }
      FF = cit/jmen;

      #ifdef I3D_DEBUG
        cout << "destroing FFT plan and output, erasing boundary input field..."
             << endl;
      #endif
      fftw_destroy_plan(plan);
      fftw_free(in);
      fftw_free(out);

      isNFDs2d = true;
      fF = FF;
      nEnergy = NEnergy;
      return;
    #endif //WITH_FFTW
  }
  
  // explicit instantiations:
  template class I3D_DLLEXPORT Shape<size_t>;
  template class I3D_DLLEXPORT Shape<BINARY>;
  template class I3D_DLLEXPORT Shape<unsigned short>;

}//namespace


