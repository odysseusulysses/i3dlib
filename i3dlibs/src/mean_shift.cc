/*
 * i3dlib - image manipulation library
 *
 * Copyright (C) 2000-2006   Centre for Biomedical Image Analysis (CBIA)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 * mean_shift.cc
 *
 * Mean shift algorithms
 *
 * Martin Ma�ka (xmaska@fi.muni.cz) 2009
 */

#include "mean_shift.h"

	/////////////////////////////////////////////////////////////////
	//                                                             //
	//                      Class MeanShift                        //
	//                                                             //
	/////////////////////////////////////////////////////////////////

	template <class T> class MeanShift 
	{
		private:
			/** Spatial kernel size. */
			double m_spatial_kernel_size;
			/** Range kernel size. */
			double m_range_kernel_size;
			/** Feature space dimensionality. */
			int m_dim;
			/** Number of feature vectors in the feature space. */
			int m_num;
			/** Kernel central point. */
			double *m_centre;
			/** Mean shift vector. */
			double *m_mean_shift;
			/** Maximal number of iterations for finding a mode. */
			int m_iter_limit;
			/** Threshold magnitude of the mean shift vector. If the magnitude of the mean shift vector is below
			  * this threshold, the procedure finding a mode will be over. */
			double m_ms_thres; 
			/** Member-wise stored feature vectors. */
			double *m_vectors;
			/** Bucket data structure for efficient searching of feature vectors. */
			int *m_buckets;
			int *m_bucket_list;
			/** A number of buckets in each dimension. */
			int *m_bucket_num;
			/** A list of bucket indices for each dimension. */ 
			int *m_bucket_index;
			/** A list of shifts to neighbouring buckets. */
			int *m_bucket_neigh;
			/** A number of neighbouring buckets. */
			int m_bucket_neigh_num;
			/** A minimal intensity. */
			double m_bucket_min_intensity;

		public:
			/** Constructor. */
			MeanShift(const i3d::Image3d<T> &img, double spatial_kernel_size, double range_kernel_size);
			/** Destructor. */
			~MeanShift();
			/** Mean shift filtering. */
			void Filter(i3d::Image3d<T> &img, bool optimized);

		private:
			/** Initialize bucket data structure. */
			void PrepareBuckets(const i3d::Image3d<T> &img);
			/** Clear bucket data structure. */
			void ClearBuckets();
			/** Get index of a bucket. */
			int GetBucketIndex() const;
			/** Find index of a bucket which given feature vector belongs to. */
			int FindBucketIndex(double *feature_vector);
			/** Compute the mean shift vector. */
			void ComputeMeanShiftVector();
			/** Get the magnitude of the mean shift vector. */
			double GetMeanShiftVectorMagnitude() const;
			/** Shift kernel according to the mean shift vector. */
			void ShiftKernel();
			/** Mean shift filtering without any optimization. */
			void NonOptimizedFilter(i3d::Image3d<T> &img);
			/** Optimized mean shift filtering. */
			void OptimizedFilter(i3d::Image3d<T> &img);
			
	};

	template <class T> 
		MeanShift<T>::MeanShift(const i3d::Image3d<T> &img, double spatial_kernel_size, double range_kernel_size)
			: m_spatial_kernel_size(spatial_kernel_size), m_range_kernel_size(range_kernel_size), m_dim(0), 
			  m_num(img.GetImageSize()), m_centre(NULL), m_mean_shift(NULL), m_iter_limit(100), m_ms_thres(0.01),
			  m_vectors(NULL), m_buckets(NULL), m_bucket_list(NULL), m_bucket_num(NULL), m_bucket_index(NULL), 
			  m_bucket_neigh(NULL), m_bucket_neigh_num(0), m_bucket_min_intensity(std::numeric_limits<double>::max())
	{
		if (img.GetSizeZ() == 1)
		{
			m_dim = 3;
			m_bucket_neigh_num = 27;
		}
		else
		{
			m_dim = 4;
			m_bucket_neigh_num = 81;
		}

		m_centre = new double[m_dim];
		m_mean_shift = new double[m_dim];

		PrepareBuckets(img);
	}

	template <class T>		
		MeanShift<T>::~MeanShift()
	{
		if (m_centre)
		{
			delete [] m_centre;
		}

		if (m_mean_shift)
		{
			delete [] m_mean_shift;
		}

		ClearBuckets();
	}

	template <class T> 
		void MeanShift<T>::Filter(i3d::Image3d<T> &img, bool optimized)
	{
		if (optimized)
		{
			OptimizedFilter(img);
		}
		else
		{
			NonOptimizedFilter(img);
		}
	}

	template <class T> 
		void MeanShift<T>::PrepareBuckets(const i3d::Image3d<T> &img)
	{
		int i, index, bucket_index, bucket_num = 1;
		T img_min, img_max;
		i3d::Vector3d<size_t> img_size = img.GetSize();
		const T *img_ptr = img.GetFirstVoxelAddr();

		// allocate memory for feature vectors
		m_vectors = new double[m_num * m_dim];

		// create feature vectors from original image 
		if (m_dim == 3)
		{
			for(i = 0, index = 0; i < m_num; i++, img_ptr++)
			{
				m_vectors[index++] = (i % img_size.x) / m_spatial_kernel_size;
				m_vectors[index++] = (i / img_size.x) / m_spatial_kernel_size;
				m_vectors[index++] = *img_ptr / m_range_kernel_size;
			}
		}
		else
		{
			for(i = 0, index = 0; i < m_num; i++, img_ptr++)
			{
				m_vectors[index++] = (i % img_size.x) / m_spatial_kernel_size;
				m_vectors[index++] = ((i / img_size.x) % img_size.y) / m_spatial_kernel_size;
				m_vectors[index++] = ((i / img_size.x) / img_size.y) / m_spatial_kernel_size;
				m_vectors[index++] = *img_ptr / m_range_kernel_size;
			}
		}

		// find minimal and maximal image intensity
		img.GetRange(img_min, img_max);
		m_bucket_min_intensity = img_min / m_range_kernel_size;
		double max = img_max / m_range_kernel_size;

		// create bucket data structure for efficient searching of feature vectors
		m_bucket_num = new int[m_dim];
		m_bucket_index = new int[m_dim];

		for (i = 0; i < m_dim - 1; i++)
		{
			m_bucket_num[i] = static_cast<int>(img_size[i] / m_spatial_kernel_size + 3);
			bucket_num *= m_bucket_num[i];
		}

		m_bucket_num[m_dim - 1] = static_cast<int>(max - m_bucket_min_intensity + 3);
		bucket_num *= m_bucket_num[m_dim - 1];

		m_bucket_list = new int[m_num];
		m_buckets = new int[bucket_num];
		memset(m_buckets, -1, bucket_num * sizeof(int));

		for(i = 0, index = 0; i < m_num; i++, index += m_dim)
		{
			// find a bucket for the i-th feature vector
			bucket_index = FindBucketIndex(&m_vectors[index]);

			//std::cout << "Index " << i << " bucket_index " << bucket_index << std::endl;

			m_bucket_list[i] = m_buckets[bucket_index];
			m_buckets[bucket_index] = i;
		}

		// create a list of shifts to neighbouring buckets
		m_bucket_neigh = new int[m_bucket_neigh_num];

		if (m_dim == 3)
		{
			for (index = 0, m_bucket_index[0] = -1; m_bucket_index[0] <= 1; m_bucket_index[0]++)
			{
				for (m_bucket_index[1] = -1; m_bucket_index[1] <= 1; m_bucket_index[1]++)
				{
					for (m_bucket_index[2] = -1; m_bucket_index[2] <= 1; m_bucket_index[2]++)
					{
						m_bucket_neigh[index++] = GetBucketIndex();
					}
				}
			}
		}
		else
		{
			for (index = 0, m_bucket_index[0] = -1; m_bucket_index[0] <= 1; m_bucket_index[0]++)
			{
				for (index = 0, m_bucket_index[1] = -1; m_bucket_index[1] <= 1; m_bucket_index[1]++)
				{
					for (m_bucket_index[2] = -1; m_bucket_index[2] <= 1; m_bucket_index[2]++)
					{
						for (m_bucket_index[3] = -1; m_bucket_index[3] <= 1; m_bucket_index[3]++)
						{
							m_bucket_neigh[index++] = GetBucketIndex();
						}
					}
				}
			}
		}
	}

	template <class T>
		void MeanShift<T>::ClearBuckets()
	{
		if (m_vectors)
		{
			delete [] m_vectors;
		}

		if (m_buckets) 
		{
			delete [] m_buckets;
		}

		if (m_bucket_list)
		{
			delete [] m_bucket_list;
		}

		if (m_bucket_num)
		{
			delete [] m_bucket_num;
		}

		if (m_bucket_index)
		{
			delete [] m_bucket_index;
		}

		if (m_bucket_neigh)
		{
			delete [] m_bucket_neigh;
		}
	}

	template <class T>
		inline int MeanShift<T>::GetBucketIndex() const
	{
		int index = 0;

		for (int i = m_dim - 1; i > 0; i--)
		{
			index += m_bucket_index[i];
			index *= m_bucket_num[i - 1];
		}
	
		return index + m_bucket_index[0];
	}

	template <class T>
		inline int MeanShift<T>::FindBucketIndex(double *feature_vector)
	{
		for (int i = 0; i < m_dim - 1; i++)
		{
			m_bucket_index[i] = static_cast<int>(feature_vector[i]) + 1;
		}

		m_bucket_index[m_dim - 1] = static_cast<int>(feature_vector[m_dim - 1] - m_bucket_min_intensity) + 1;

		return GetBucketIndex();
	}

	template <class T>
		inline void MeanShift<T>::ComputeMeanShiftVector()
	{
		int j, k, pos, index, bucket_index, count = 0;
		double diff, tmp, thres = 80.0 / m_range_kernel_size;

		// initialize the mean shift vector
		for(j = 0; j < m_dim; j++)
		{
			m_mean_shift[j] = 0.0;
		}

		// find bucket index
		bucket_index = FindBucketIndex(m_centre);

		for (j = 0; j < m_bucket_neigh_num; j++)
		{
			pos = m_buckets[bucket_index + m_bucket_neigh[j]];

			while (pos >= 0)
			{
				index = m_dim * pos;

				// determine if inside search window
				diff = 0.0;
				for (k = 0; k < m_dim - 1; k++)
				{
					tmp = m_vectors[index + k] - m_centre[k];
					diff += tmp * tmp;
				}

				if (diff < 1.0)
				{
					tmp = m_vectors[index + m_dim - 1] - m_centre[m_dim - 1];

					if (m_centre[m_dim - 1] > thres)
					{
						diff = 4 * tmp * tmp;
					}
					else
					{
						diff = tmp * tmp;
					}

					if (diff < 1.0)
					{
						for (k = 0; k < m_dim; k++)
						{
							m_mean_shift[k] += m_vectors[index + k];
						}

						count++;
					}
				}
				
				pos = m_bucket_list[pos];
			}
		}
		
		if (count > 0)
		{
			for(j = 0; j < m_dim; j++)
			{
				m_mean_shift[j] = m_mean_shift[j] / count - m_centre[j];
			}
		}
	}

	template <class T>
		inline double MeanShift<T>::GetMeanShiftVectorMagnitude() const
	{
		double mag = m_mean_shift[m_dim - 1] * m_mean_shift[m_dim - 1] * m_range_kernel_size * m_range_kernel_size;

		double tmp = 0.0;
		for (int i = 0; i < m_dim - 1; i++)
		{
			tmp += m_mean_shift[i] * m_mean_shift[i];
		}

		mag += tmp * m_spatial_kernel_size * m_spatial_kernel_size;

		return mag;
	}

	template <class T>
		inline void MeanShift<T>::ShiftKernel()
	{
		for (int j = 0; j < m_dim; j++)
		{
			m_centre[j] += m_mean_shift[j];
		}
	}

	template <class T> 
		void MeanShift<T>::NonOptimizedFilter(i3d::Image3d<T> &img)
	{
		int j, iter;
		double ms_mag;

		// find a mode for each feature vector
		T *img_ptr = img.GetFirstVoxelAddr();
		for(int i = 0, index = 0; i < m_num; i++, index += m_dim, img_ptr++)
		{
			// set the kernel centre to the i-th feature vector and initialize the mean shift vector
			for (j = 0; j < m_dim; j++)
			{
				m_centre[j] = m_vectors[index + j];
				m_mean_shift[j] = 0.0;
			}

			// set iteration counter to 0
			iter = 0;

			// keep shifting a kernel center according to the mean shift vector until
			// its magnitude is below a specified threshold (m_ms_thres) or the number
			// of iterations reaches a specified threshold (m_iter_limit)
			do
			{
				// shift kernel according to the mean shift vector
				ShiftKernel();

				// compute the mean shift vector
				ComputeMeanShiftVector();

				// compute the magnitude of the mean shift vector
				ms_mag = GetMeanShiftVectorMagnitude();

				// increment iteration counter
				iter++;

			} while((ms_mag >= m_ms_thres) && (iter < m_iter_limit));

			// shift kernel according to the mean shift vector
			ShiftKernel();

			// copy a mode intensity to the output image
			*img_ptr = static_cast<T>(m_centre[m_dim - 1] * m_range_kernel_size + 0.5);
		}
	}
	
	template <class T>
		void MeanShift<T>::OptimizedFilter(i3d::Image3d<T> &img)
	{
		int j, iter, candidate, basin_size;
		double	ms_mag, dist;
		bool basin_found;
		T mode_intensity;
		T *img_ptr = img.GetFirstVoxelAddr();
		
		int size_x = img.GetSizeX();

		// initialize a mode table used for a basin of attraction
		unsigned char *modes = new unsigned char[m_num];
		memset(modes, 0, m_num * sizeof(unsigned char));

		// initialize a basin attraction
		int *basin = new int[m_num];

		// find a mode for each feature vector
		for (int i = 0, index = 0; i < m_num; i++, index += m_dim)
		{
			if (modes[i] != 1)
			{
				// any mode has not been found yet for this feature vector

				// initialize the basin of attraction
				basin[0] = i;
				basin_size = 1;
				basin_found = false;

				// set the kernel centre to the i-th feature vector
				for (j = 0; j < m_dim; j++)
				{
					m_centre[j] = m_vectors[index + j];
				}

				// compute the mean shift vector
				ComputeMeanShiftVector();

				// compute the magnitude of the mean shift vector
				ms_mag = GetMeanShiftVectorMagnitude();

				// set iteration counter to 1
				iter = 1;

				// keep shifting a kernel center according to the mean shift vector until
				// its magnitude is below a specified threshold (m_ms_thres) or the number
				// of iterations reaches a specified threshold (m_iter_limit)
				while((ms_mag >= m_ms_thres) && (iter < m_iter_limit))
				{	
					// shift kernel according to the mean shift vector
					ShiftKernel();

					// find a grid point that could have the same mode as the i-th feature vector
					candidate = static_cast<int>(m_spatial_kernel_size * m_centre[0] + 0.5) +
								static_cast<int>(m_spatial_kernel_size * m_centre[1] + 0.5) * size_x;

					if ((candidate != i) && (modes[candidate] != 2))
					{
						// compute a distance between the grid point and a mode
						dist = m_vectors[m_dim * (candidate + 1) - 1] - m_centre[m_dim - 1];
						dist *= dist;
						
						// check whether the grid point belongs to the basin of attraction
						if (dist < 0.5)
						{
							if (modes[candidate] == 0)
							{
								// any mode has not been found yet for this feature vector

								basin[basin_size++] = candidate;
								modes[candidate]	= 2;
							}
							else
							{
								// indicate that a mode has already been found
								basin_found = true;

								break;
							}
						}
					}

					// compute the mean shift vector
					ComputeMeanShiftVector();

					// compute the magnitude of the mean shift vector
					ms_mag = GetMeanShiftVectorMagnitude();

					// increment iteration counter
					iter++;
				}

				if (basin_found)
				{
					// a mode has already been found for this feature vector

					// get its intensity
					mode_intensity = *(img_ptr + candidate);
				}
				else
				{
					// any mode has not been found yet for this feature vector

					// shift kernel according to the mean shift vector
					ShiftKernel();

					// get a mode intensity
					mode_intensity = static_cast<T>(m_centre[m_dim - 1] * m_range_kernel_size + 0.501);
				}

				// set the mode intensity for each feature vector in the basin of attraction
				for (j = 0; j < basin_size; j++)
				{
					// get the j-th feature vector in the basin of attraction
					candidate = basin[j];

					// update the mode table
					modes[candidate] = 1;

					// copy the mode intensity to the output image
					*(img_ptr + candidate) = mode_intensity;
				}
			}
		}
	
		// deallocate memory
		delete [] modes;
		delete [] basin;
	}

namespace i3d
{
	/////////////////////////////////////////////////////////////////
	//                                                             //
	//                     MeanShiftFiltering                      //
	//                                                             //
	/////////////////////////////////////////////////////////////////

	template <class T> void MeanShiftFiltering(const i3d::Image3d<T> &in, i3d::Image3d<T> &out, 
		                                       double spatial_kernel_size, double range_kernel_size, bool optimized)
	{
		out.CopyMetaData(in);

		MeanShift<T> ms(in, spatial_kernel_size, range_kernel_size);
		ms.Filter(out, optimized);
	}

	/* Explicit instantiations */
	template I3D_DLLEXPORT void MeanShiftFiltering(const i3d::Image3d<i3d::GRAY8> &in, i3d::Image3d<i3d::GRAY8> &out,
		                                           double spatial_kernel_size, double range_kernel_size, bool optimized);
	template I3D_DLLEXPORT void MeanShiftFiltering(const i3d::Image3d<i3d::GRAY16> &in, i3d::Image3d<i3d::GRAY16> &out,
		                                           double spatial_kernel_size, double range_kernel_size, bool optimized);

} // namespace i3d
