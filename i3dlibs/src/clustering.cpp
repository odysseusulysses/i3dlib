/*
 * clustering.cpp
 *
 * k-Means clustering
 *
 * Pavel Matula (pam@fi.muni.cz) 2007
 *
 */

#include <ctime>
#include <cstdlib>
#include "histogram.h"
#include "threshold.h"
#include "clustering.h"

namespace i3d {

	template <class VOXEL> void ComputeMeans(const i3d::Image3d<VOXEL> &imgin, const i3d::Image3d<i3d::GRAY8> &imgout, std::vector<float> &means)
	{
		std::vector<long> sums;
		sums.resize(means.size(), 0);
		means.resize(sums.size(), 0.0);
        GRAY8 i;

		const VOXEL *px = imgin.GetFirstVoxelAddr();
		const GRAY8 *pxout = imgout.GetFirstVoxelAddr();

		for (size_t j = 0; j < imgin.GetImageSize(); j++) {
			i = *pxout;

			means[i] += *px;
			sums[i]++;

			pxout++; px++;
		}

		for (size_t j = 0; j < means.size(); j++) {
			means[j] /= sums[j];
			//wxLogMessage("mean #%d = %g", j, means[j]);
		}
	}


	template <class VOXEL> void CreateEqualMassClusters(const Image3d<VOXEL> &img, Image3d<GRAY8> &imgout, GRAY8 k)
	{
		/* Compute histogram ----------------------------------- */	
		std::vector<i3d::GRAY16> dividers;
		dividers.resize(k-1, std::numeric_limits<i3d::GRAY16>::max());

		i3d::Histogram hist;
		i3d::IntensityHist(img, hist);

		long fin = (img.GetImageSize()/k) * (k - 1);
		long sum = 0;
		size_t i = 0;
		long j = 1;
		while (sum < fin) {
			sum += hist[i];
			if (sum >= (long(img.GetImageSize())/long(k)) * j) {
				dividers[j-1] = i;
				++j;
			}
			++i;
		}

		/* Creating initial partioning ----------------------------------- */	
		const VOXEL *px = img.GetFirstVoxelAddr(); 
		GRAY8 *pxout = imgout.GetFirstVoxelAddr(); 
		for (size_t j = 0; j < img.GetImageSize(); j++) {
			GRAY8 i = 0;
			while (dividers[i] < *px) ++i;
			*pxout = i;

			++px; ++pxout;
		}
	}

	template <> void CreateEqualMassClusters(const Image3d<float> &img, Image3d<GRAY8> &imgout, GRAY8 k)
	{
		throw InternalException("CreateEqualMassClusters not implemented for float images.");
	}

/******************************************************************************************/\

	template <class VOXEL> void CreateOtsuClusters(const Image3d<VOXEL> &img, Image3d<GRAY8> &imgout, GRAY8 k)
	{
		i3d::Histogram hist;
		i3d::IntensityHist(img, hist);

		std::vector<int> thresholds;

		if (ComputeMultilevelOtsuThresholds(hist, k, thresholds)) {
			DoMultilevelThresholding(img, imgout, thresholds);
		}
	}
	
	template <> void CreateOtsuClusters(const Image3d<float> &img, Image3d<GRAY8> &imgout, GRAY8 k)
	{
		throw InternalException("CreateOtsuClusters not implemented for float images.");
	}

/******************************************************************************************/\

	template <class VOXEL> bool kMeans(const Image3d<VOXEL> &img, Image3d<GRAY8> &imgout, GRAY8 k, InitClusterMethod method, int max_iters, std::vector<float> *fin_means)
	{
		if (img.GetImageSize() != imgout.GetImageSize()) {
			imgout.MakeRoom(img.GetSize());
			imgout.SetResolution(img.GetResolution());
			imgout.SetOffset(img.GetOffset());
		}

		switch (method) {
			case ClusterRandom: 
				{
					srand((unsigned)time(0));
					GRAY8 *pxout = imgout.GetFirstVoxelAddr(); 
					for (size_t j = 0; j < img.GetImageSize(); j++) {
                        *pxout = rand() % k;
						++pxout;
					}
				}
				break;
			case ClusterEqualMass:
				{
					CreateEqualMassClusters(img, imgout, k);
				}
				break;
			case ClusterAlternating:
				{
					GRAY8 *pxout = imgout.GetFirstVoxelAddr(); 
					for (size_t j = 0; j < img.GetImageSize(); j++) {
						*pxout = j % k;
						++pxout;
					}
				}
				break;
			case ClusterMask:
				// nothing to do... we just take imgout as the initial partitioning
				break;
			case ClusterMultilevelOtsu:
				{
					CreateOtsuClusters(img, imgout, k);
					return true; // no Lloyd follows!
					break;
				}
			default:
				throw InternalException("unknown initialisation method in kMeans.");
				break;
		}

		/* BEGIN Lloyd Algorithm*/
		std::vector<float> means;
		means.resize(k);

		bool has_changed = true;
		while (has_changed) {
			ComputeMeans(img, imgout, means);

			has_changed = false;

			const VOXEL *px    = img.GetFirstVoxelAddr();
			GRAY8 *pxout = imgout.GetFirstVoxelAddr();
			i3d::GRAY8 closest;

			for (size_t i = 0; i < img.GetImageSize(); ++i) {
				/* find closest group */
				closest = *pxout;
				float mclo = means[closest];
				float min_dist = fabs(mclo - *px);
				for (int j = 0; j < k; ++j) {
					float m = means[j];
					if (fabs(m - *px) < min_dist) {
						has_changed = true;
						closest = j; 
						min_dist = fabs(m - *px);
					}
				}
				if (*pxout != closest) {
					*pxout = closest;
				}

				pxout++; px++;
			}

			if (has_changed && !max_iters--) {
				//wxLogMessage("Maximal number of iterations reached...");
				has_changed = false;
			}
		}
		/* END Lloyd Algorithm*/

		if (fin_means) {
			*fin_means = means;
		}

		return max_iters != -1;
	}


/******************************************************************************************/

template  I3D_DLLEXPORT bool kMeans(const Image3d<GRAY8> &img, Image3d<GRAY8> &imgout, GRAY8 k, InitClusterMethod method, int max_iters, std::vector<float> *means);
template  I3D_DLLEXPORT bool kMeans(const Image3d<GRAY16> &img, Image3d<GRAY8> &imgout, GRAY8 k, InitClusterMethod method, int max_iters, std::vector<float> *means);
template  I3D_DLLEXPORT bool kMeans(const Image3d<float> &img, Image3d<GRAY8> &imgout, GRAY8 k, InitClusterMethod method, int max_iters, std::vector<float> *means);
}
