#include "colocalization.h"
#include <map>
#include <vector>


//using namespace std;
using namespace i3d; 




namespace i3d {

//--------------------------------------------------------------------------
template <class VOXEL>
void Manders(const Image3d<VOXEL>& i1, const Image3d<VOXEL>& i2, Image3d<GRAY16>& o1, unsigned int threshold)
{
	if (i1.GetImageSize()!=i2.GetImageSize()) 
	{
		std::cerr << "Images of different sizes." << std::endl;
		o1.MakeRoom(1,1,1);
		return;
	}
		
	Image3d<float> o;
	o.MakeRoom(i1.GetSize());
	o1.CopyMetaData(i1);

	double gr16max=65535;
	double sumPic1=0,min=gr16max,max=-gr16max,vox,k;
	for(unsigned int i=0;i<i1.GetImageSize();i++)
		sumPic1+=i1.GetVoxel(i);
	for(unsigned int i=0;i<i1.GetImageSize();i++)
	{		
		if (i2.GetVoxel(i)>threshold) 
		{	
			vox = i1.GetVoxel(i)/sumPic1;
		}
		else 
		{	
			vox=0;
		}
		o.SetVoxel(i,vox); 
		if (vox<min) min=vox;
		if (vox>max) max=vox;
	}
	k = gr16max/(max-min);
	for(unsigned int i=0;i<i1.GetImageSize();i++)
		o1.SetVoxel(i,(unsigned int)((o.GetVoxel(i)-min)*k));

};
//--------------------------------------------------------------------------

template <class VOXEL>
void Pearson(const Image3d<VOXEL> &i1, const Image3d<VOXEL> &i2, Image3d<GRAY16> &o1)
{	
	if (i1.GetImageSize()!=i2.GetImageSize()) 
	{
		std::cerr << "Images of different sizes." << std::endl;
		o1.MakeRoom(1,1,1);
		return;
	}
	double gr16max=65535;
	double avPic1=0,avPic2=0,sqPic1=0,sqPic2=0,sqs=0,min=gr16max,max=-gr16max,vox,k;
	
	Image3d<float> o;
	o.MakeRoom(i1.GetSize());
	o1.CopyMetaData(i1);
	
	for(unsigned long i=0;i<i1.GetImageSize();i++)
	{
		avPic1+=i1.GetVoxel(i);
		avPic2+=i2.GetVoxel(i);
	}
	avPic1/=i1.GetImageSize();
	avPic2/=i2.GetImageSize();
	for(unsigned long i=0;i<i1.GetImageSize();i++)
	{
		sqPic1+=(i1.GetVoxel(i)-avPic1)*(i1.GetVoxel(i)-avPic1);
		sqPic2+=(i2.GetVoxel(i)-avPic2)*(i2.GetVoxel(i)-avPic2);
	}
	sqs=sqrt(sqPic1*sqPic2);
	for(unsigned long i=0;i<i1.GetImageSize();i++)
	{
		vox = (i1.GetVoxel(i)-avPic1)*(i2.GetVoxel(i)-avPic2)/sqs;
		if (vox<min) min=vox;
		if (vox>max) max=vox;
		o.SetVoxel(i, vox);
	}
	k = gr16max/(max-min);
	for(unsigned long i=0;i<i1.GetImageSize();i++)
	{
		o1.SetVoxel(i,(unsigned int)((o.GetVoxel(i)-min)*k));
	}
	
};

//--------------------------------------------------------------------------
template <class VOXEL>
void Overlap(const Image3d<VOXEL> &i1, const Image3d<VOXEL> &i2, Image3d<GRAY16> &o1)
{
	if (i1.GetImageSize()!=i2.GetImageSize()) 
	{
		std::cerr << "Images of different sizes." << std::endl;
		o1.MakeRoom(1,1,1);
		return;
	}
	double gr16max=65535;
	double sqSumPic1=0,sqSumPic2=0,sqs=0,min=gr16max,max=-gr16max,vox,k;
	
	Image3d<float> o;
	o.MakeRoom(i1.GetSize());
	o1.CopyMetaData(i1);
	
	for(unsigned long i=0;i<i1.GetImageSize();i++)
	{
		sqSumPic1+=i1.GetVoxel(i)*i1.GetVoxel(i);
		sqSumPic2+=i2.GetVoxel(i)*i2.GetVoxel(i);
	}
	sqs=sqrt(sqSumPic1*sqSumPic2);
	for(unsigned long i=0;i<i1.GetImageSize();i++)
	{
		vox = i1.GetVoxel(i)*i2.GetVoxel(i)/sqs;
		if (vox<min) min=vox;
		if (vox>max) max=vox;
		o.SetVoxel(i, vox);
	}
	k = gr16max/(max-min);
	for(unsigned int i=0;i<i1.GetImageSize();i++)
		o1.SetVoxel(i,(unsigned int)((o.GetVoxel(i)-min)*k));
	
};

//--------------------------------------------------------------------------
template <class VOXEL>
double PearsonC(const Image3d<VOXEL> &i1, const Image3d<VOXEL> &i2, const LabeledImage3d<unsigned short, BINARY> &mask, int label)
{
	double avPic1=0,avPic2=0,sqPic1=0,sqPic2=0,sqs=0, coef=0, surface=0;
	
	if  (i1.GetImageSize()!=i2.GetImageSize())
		return 0;
	
	for(unsigned long i=0;i<i1.GetImageSize();i++)
	{
		if (label==-1 || label==mask.GetVoxel(i))
		{
			avPic1+=i1.GetVoxel(i);
			avPic2+=i2.GetVoxel(i);
			surface++;
		}
	}
	if (surface>0)
	{	avPic1/=surface;
		avPic2/=surface;
	}
	for(unsigned long i=0;i<i1.GetImageSize();i++)
	{
		if (label==-1 || label==mask.GetVoxel(i))
		{
			sqPic1+=(i1.GetVoxel(i)-avPic1)*(i1.GetVoxel(i)-avPic1);
			sqPic2+=(i2.GetVoxel(i)-avPic2)*(i2.GetVoxel(i)-avPic2);
		}
	}
	sqs=sqrt(sqPic1*sqPic2);
	for(unsigned long i=0;i<i1.GetImageSize();i++)
	{
		if (label==-1 || label==mask.GetVoxel(i))
			coef+=(i1.GetVoxel(i)-avPic1)*(i2.GetVoxel(i)-avPic2)/sqs;
	}
	
	return coef;
}

//--------------------------------------------------------------------------
template <class VOXEL>
double MandersC(const Image3d<VOXEL> &i1, const Image3d<VOXEL> &i2, const  LabeledImage3d<unsigned short, BINARY> &mask, int label, unsigned int threshold)
{
	if  (i1.GetImageSize()!=i2.GetImageSize())
		return 0;
	

	double sumPic1=0, coef=0;
	for(unsigned long i=0;i<i1.GetImageSize();i++)
		if (label==-1 || label==mask.GetVoxel(i)) sumPic1+=i1.GetVoxel(i);
	for(unsigned long i=0;i<i1.GetImageSize();i++)
	{
		if (label==-1 || label==mask.GetVoxel(i))
			if (i2.GetVoxel(i)>threshold) 
				coef+=(i1.GetVoxel(i)/sumPic1); 
		
	}
	return coef;
}	 

//--------------------------------------------------------------------------
template <class VOXEL>
double OverlapC(const Image3d<VOXEL> &i1, const Image3d<VOXEL> &i2, const LabeledImage3d<unsigned short, BINARY> &mask, int label)
{
	if  (i1.GetImageSize()!=i2.GetImageSize())
		return 0;
	double sqSumPic1=0,sqSumPic2=0,sqs=0, coef=0;
	
	
	for(unsigned long i=0;i<i1.GetImageSize();i++)
	{
		if (label==-1 || label==mask.GetVoxel(i))
		{	
			sqSumPic1+=i1.GetVoxel(i)*i1.GetVoxel(i);
			sqSumPic2+=i2.GetVoxel(i)*i2.GetVoxel(i);
		}
	}
	sqs=sqrt(sqSumPic1*sqSumPic2);
	for(unsigned long i=0;i<i1.GetImageSize();i++)
	{
		if (label==-1 || label==mask.GetVoxel(i))
			coef+=i1.GetVoxel(i)*i2.GetVoxel(i)/sqs; 
	}
	return coef;
}
//--------------------------------------------------------------------------
template <class VOXEL>
double PearsonC(const Image3d<VOXEL> &i1, const Image3d<VOXEL> &i2)
{
	double avPic1=0,avPic2=0,sqPic1=0,sqPic2=0,sqs=0, coef=0;
	
	if  (i1.GetImageSize()!=i2.GetImageSize())
		return 0;
	
	for(unsigned long i=0;i<i1.GetImageSize();i++)
	{
		avPic1+=i1.GetVoxel(i);
		avPic2+=i2.GetVoxel(i);
	}
		avPic1/=i1.GetImageSize();
		avPic2/=i1.GetImageSize();
	for(unsigned long i=0;i<i1.GetImageSize();i++)
	{
		sqPic1+=(i1.GetVoxel(i)-avPic1)*(i1.GetVoxel(i)-avPic1);
		sqPic2+=(i2.GetVoxel(i)-avPic2)*(i2.GetVoxel(i)-avPic2);
	}
	sqs=sqrt(sqPic1*sqPic2);
	for(unsigned long i=0;i<i1.GetImageSize();i++)
	{
		coef+=(i1.GetVoxel(i)-avPic1)*(i2.GetVoxel(i)-avPic2)/sqs;
	}
	
	return coef;
}

//--------------------------------------------------------------------------
template <class VOXEL>
double MandersC(const Image3d<VOXEL> &i1, const Image3d<VOXEL> &i2, unsigned int threshold)
{
	if  (i1.GetImageSize()!=i2.GetImageSize())
		return 0;
	

	double sumPic1=0, coef=0;
	for(unsigned long i=0;i<i1.GetImageSize();i++)
		sumPic1+=i1.GetVoxel(i);
	for(unsigned long i=0;i<i1.GetImageSize();i++)
	{
		if (i2.GetVoxel(i)>threshold) 
			coef+=(i1.GetVoxel(i)/sumPic1); 
		
	}
	return coef;
}	 

//--------------------------------------------------------------------------
template <class VOXEL>
double OverlapC(const Image3d<VOXEL> &i1, const Image3d<VOXEL> &i2)
{
	if  (i1.GetImageSize()!=i2.GetImageSize())
		return 0;
	double sqSumPic1=0,sqSumPic2=0,sqs=0, coef=0;
	
	
	for(unsigned long i=0;i<i1.GetImageSize();i++)
	{
		sqSumPic1+=i1.GetVoxel(i)*i1.GetVoxel(i);
		sqSumPic2+=i2.GetVoxel(i)*i2.GetVoxel(i);
	}
	sqs=sqrt(sqSumPic1*sqSumPic2);
	for(unsigned long i=0;i<i1.GetImageSize();i++)
	{
		coef+=i1.GetVoxel(i)*i2.GetVoxel(i)/sqs; 
	}
	return coef;
}
//--------------------------------------------------------------------------
template <class VOXEL>
std::map<unsigned short,double> PearsonM(const Image3d<VOXEL> &i1, const Image3d<VOXEL> &i2, const LabeledImage3d<unsigned short, BINARY> &mask)
{
	
	std::map<unsigned short, double> coefs;
	std::set<unsigned short> labels;
	std::set<unsigned short>::iterator it;
	unsigned short currentlabel, m;
	if  (i1.GetImageSize()!=i2.GetImageSize())
		return coefs;
	currentlabel=0;
	do //for each unique object label compute colocalization
	{
		double avPic1=0,avPic2=0,sqPic1=0,sqPic2=0,sqs=0, coef=0, surface=0;
		for(unsigned long i=0;i<i1.GetImageSize();i++)
		{
			m=mask.GetVoxel(i);
			if (m!=0 && labels.insert(m).second && currentlabel==0) currentlabel=m;
			if (currentlabel==m && m!=0)
			{
				avPic1+=i1.GetVoxel(i);
				avPic2+=i2.GetVoxel(i);
				surface++;
			}
		}
		if (surface>0)
		{	avPic1/=surface;
			avPic2/=surface;
		}
		for(unsigned long i=0;i<i1.GetImageSize();i++)
		{
			if (currentlabel==mask.GetVoxel(i))
			{
				sqPic1+=(i1.GetVoxel(i)-avPic1)*(i1.GetVoxel(i)-avPic1);
				sqPic2+=(i2.GetVoxel(i)-avPic2)*(i2.GetVoxel(i)-avPic2);
			}
		}
		sqs=sqrt(sqPic1*sqPic2);
		for(unsigned long i=0;i<i1.GetImageSize();i++)
		{
			if (currentlabel==mask.GetVoxel(i))
				coef+=(i1.GetVoxel(i)-avPic1)*(i2.GetVoxel(i)-avPic2)/sqs;
		}
		coefs.insert(std::pair<unsigned short, double>(currentlabel, coef));
		it=labels.insert(currentlabel).first;
		if (it!=labels.end() && labels.size()!=coefs.size()) 
		{	
			it++;
			currentlabel=*it;
		}
	} while (labels.size()!=coefs.size());
	
	return coefs;
}

//--------------------------------------------------------------------------
template <class VOXEL>
std::map<unsigned short,double> MandersM(const Image3d<VOXEL> &i1, const Image3d<VOXEL> &i2, const LabeledImage3d<unsigned short, BINARY> &mask, unsigned int threshold)
{
	
	std::map<unsigned short, double> coefs;
	std::set<unsigned short> labels;
	std::set<unsigned short>::iterator it;
	unsigned short currentlabel, m;
	if  (i1.GetImageSize()!=i2.GetImageSize())
		return coefs;
	currentlabel=0;
	do //for each unique object label compute colocalization
	{
		double sumPic1=0, coef=0;
		for(unsigned long i=0;i<i1.GetImageSize();i++)
		{
			m=mask.GetVoxel(i);
			if (m!=0 && labels.insert(m).second && currentlabel==0) currentlabel=m;
			if (currentlabel==m && m!=0)
			{
				sumPic1+=i1.GetVoxel(i);
			}
		}
		
		for(unsigned long i=0;i<i1.GetImageSize();i++)
		{
			if (currentlabel==mask.GetVoxel(i))
				if (i2.GetVoxel(i)>threshold) 
					coef+=(i1.GetVoxel(i)/sumPic1); 
		}
		coefs.insert(std::pair<unsigned short, double>(currentlabel, coef));
		it=labels.insert(currentlabel).first;
		if (it!=labels.end() && labels.size()!=coefs.size()) 
		{	
			it++;
			currentlabel=*it;
		}
	} while (labels.size()!=coefs.size());
	
	return coefs;
}

//--------------------------------------------------------------------------
template <class VOXEL>
std::map<unsigned short,double> OverlapM(const Image3d<VOXEL> &i1, const Image3d<VOXEL> &i2, const LabeledImage3d<unsigned short, BINARY> &mask)
{
	std::map<unsigned short, double> coefs;
	std::set<unsigned short> labels;
	std::set<unsigned short>::iterator it;
	unsigned short currentlabel, m;
	if  (i1.GetImageSize()!=i2.GetImageSize())
		return coefs;
	currentlabel=0;
	do {//for each unique object label compute colocalization

		double sqSumPic1=0,sqSumPic2=0,sqs=0, coef=0;
		for(unsigned long i=0;i<i1.GetImageSize();i++)
		{
			m=mask.GetVoxel(i);
			if (m!=0 && labels.insert(m).second && currentlabel==0) currentlabel=m;
			if (currentlabel==m && m!=0)
			{
				sqSumPic1+=i1.GetVoxel(i)*i1.GetVoxel(i);
				sqSumPic2+=i2.GetVoxel(i)*i2.GetVoxel(i);
			}
		}
				
		sqs=sqrt(sqSumPic1*sqSumPic2);
		for(unsigned long i=0;i<i1.GetImageSize();i++)
		{
			if (currentlabel==mask.GetVoxel(i))
				coef+=i1.GetVoxel(i)*i2.GetVoxel(i)/sqs; 
		}
		coefs.insert(std::pair<unsigned short, double>(currentlabel, coef));
		it=labels.insert(currentlabel).first;
		if (it!=labels.end() && labels.size()!=coefs.size()) 
		{	
			it++;
			currentlabel=*it;
		}
	} while (labels.size()!=coefs.size());
	
	return coefs;
}
//--------------------------------------------------------------------------


/* Explicit instantiations: */

template I3D_DLLEXPORT void Manders(const Image3d<GRAY8> &, const Image3d<GRAY8> &, Image3d<GRAY16> &, unsigned int);
template I3D_DLLEXPORT void Manders(const Image3d<GRAY16> &, const Image3d<GRAY16> &, Image3d<GRAY16> &, unsigned int);
template I3D_DLLEXPORT void Pearson(const Image3d<GRAY8> &, const Image3d<GRAY8> &, Image3d<GRAY16> &);
template I3D_DLLEXPORT void Pearson(const Image3d<GRAY16> &, const Image3d<GRAY16> &, Image3d<GRAY16> &);
template I3D_DLLEXPORT void Overlap(const Image3d<GRAY8> &, const Image3d<GRAY8> &, Image3d<GRAY16> &);
template I3D_DLLEXPORT void Overlap(const Image3d<GRAY16> &, const Image3d<GRAY16> &, Image3d<GRAY16> &);

template I3D_DLLEXPORT std::map<unsigned short,double> PearsonM(const Image3d<GRAY8> &i1, const Image3d<GRAY8> &i2, const LabeledImage3d<unsigned short, BINARY> &mask);
template I3D_DLLEXPORT std::map<unsigned short,double> MandersM(const Image3d<GRAY8> &i1, const Image3d<GRAY8> &i2, const LabeledImage3d<unsigned short, BINARY> &mask, unsigned int threshold);
template I3D_DLLEXPORT std::map<unsigned short,double> OverlapM(const Image3d<GRAY8> &i1, const Image3d<GRAY8> &i2, const LabeledImage3d<unsigned short, BINARY> &mask);
template I3D_DLLEXPORT std::map<unsigned short,double> PearsonM(const Image3d<GRAY16> &i1, const Image3d<GRAY16> &i2, const LabeledImage3d<unsigned short, BINARY> &mask);
template I3D_DLLEXPORT std::map<unsigned short,double> MandersM(const Image3d<GRAY16> &i1, const Image3d<GRAY16> &i2, const LabeledImage3d<unsigned short, BINARY> &mask, unsigned int threshold);
template I3D_DLLEXPORT std::map<unsigned short,double> OverlapM(const Image3d<GRAY16> &i1, const Image3d<GRAY16> &i2, const LabeledImage3d<unsigned short, BINARY> &mask);

template I3D_DLLEXPORT double OverlapC(const Image3d<GRAY8> &i1, const Image3d<GRAY8> &i2, const LabeledImage3d<unsigned short, BINARY> &mask, int label);
template I3D_DLLEXPORT double MandersC(const Image3d<GRAY8> &i1, const Image3d<GRAY8> &i2, const LabeledImage3d<unsigned short, BINARY> &mask, int label, unsigned int threshold);
template I3D_DLLEXPORT double PearsonC(const Image3d<GRAY8> &i1, const Image3d<GRAY8> &i2, const LabeledImage3d<unsigned short, BINARY> &mask, int label);
template I3D_DLLEXPORT double OverlapC(const Image3d<GRAY8> &i1, const Image3d<GRAY8> &i2);
template I3D_DLLEXPORT double MandersC(const Image3d<GRAY8> &i1, const Image3d<GRAY8> &i2, unsigned int threshold);
template I3D_DLLEXPORT double PearsonC(const Image3d<GRAY8> &i1, const Image3d<GRAY8> &i2);
template I3D_DLLEXPORT double OverlapC(const Image3d<GRAY16> &i1, const Image3d<GRAY16> &i2, const LabeledImage3d<unsigned short, BINARY> &mask, int label);
template I3D_DLLEXPORT double MandersC(const Image3d<GRAY16> &i1, const Image3d<GRAY16> &i2, const LabeledImage3d<unsigned short, BINARY> &mask, int label, unsigned int threshold);
template I3D_DLLEXPORT double PearsonC(const Image3d<GRAY16> &i1, const Image3d<GRAY16> &i2, const LabeledImage3d<unsigned short, BINARY> &mask, int label);
template I3D_DLLEXPORT double OverlapC(const Image3d<GRAY16> &i1, const Image3d<GRAY16> &i2);
template I3D_DLLEXPORT double MandersC(const Image3d<GRAY16> &i1, const Image3d<GRAY16> &i2, unsigned int threshold);
template I3D_DLLEXPORT double PearsonC(const Image3d<GRAY16> &i1, const Image3d<GRAY16> &i2);




}//namespace i3d




